# Oh_오션헌터 #

### 개발 환경 ###

* Unity 5.6.3p4
- [Windows](https://beta.unity3d.com/download/fbe8bd37d7fa/UnityDownloadAssistant-5.6.3p4.exe?_ga=2.255229000.1930992513.1510904190-537438461.1429066663)
- [Mac](https://beta.unity3d.com/download/fbe8bd37d7fa/UnityDownloadAssistant-5.6.3p4.dmg?_ga=2.255229000.1930992513.1510904190-537438461.1429066663)

### 주요 플러그인 ###

* UniRX - 5.5.0
* UniLinq - ???
* ICSharpCode.SharpZipLib(서버 통신시 데이터 압축) - ???
* Spine - runtime:2.5, tool:3.6.38
* Texture packer importer(customized, 텍스쳐 import) - ???
* File Browser(맵툴용) - ???
* UniWebView(웹뷰) - 2.8.0
* Sprite Mask(인게임 블럭 마스킹용) - 1.4