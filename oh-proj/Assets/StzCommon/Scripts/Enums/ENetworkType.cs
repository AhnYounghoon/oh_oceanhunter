﻿public class ENetworkType
{
    public const string NOT_CONNECTED = "NOT_CONNECTED";
    public const string WIFI = "WIFI";
    public const string MOBILE = "MOBILE";
}
