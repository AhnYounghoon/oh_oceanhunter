﻿using System;

public enum EPermissionResponseType
{
	GRANTED = 0,		//퍼미션 승인 됨.
	DENIED = -1,		//퍼미션 거부 됨.
}

