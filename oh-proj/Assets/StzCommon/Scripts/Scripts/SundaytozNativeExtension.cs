using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SimpleJSON;
using STZ_Common;

public class SundaytozNativeExtension : MonoBehaviour
{
    /* 인자없는 콜백 함수 */
    public delegate void NoParamCallback();

    /* 네트워크 상태 변화 */
    public delegate void OnChangedNetworkStatusHandler(string inNetworkType);

    /// <summary>
    /// 
    /// </summary>
    protected static SundaytozPluginBase plugin = null;

    /* 네트워크 상태 변화 */
    public static event OnChangedNetworkStatusHandler OnChangedNetworkStatus;

    /// <summary>
    /// 
    /// </summary>
    private static SundaytozNativeExtension _instance;
    public static SundaytozNativeExtension Instance
    {
        get
        {
            if(!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(SundaytozNativeExtension)) as SundaytozNativeExtension;
                if(!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "SundaytozNativeExtension";
                    _instance = container.AddComponent(typeof(SundaytozNativeExtension)) as SundaytozNativeExtension;
                    DontDestroyOnLoad(_instance);
                }
            }
 
            return _instance;
        }
    }

    public void Initialize(SundaytozResponseHandler.CompleteDelegate complete)
    {

        if(plugin == null)
        {
#if(!UNITY_EDITOR && UNITY_ANDROID)
            plugin = ScriptableObject.CreateInstance<SundaytozPluginAndroid>();
#elif (!UNITY_EDITOR && UNITY_IOS)
            plugin = ScriptableObject.CreateInstance<SundaytozPluginiOS>();
#else
            plugin = ScriptableObject.CreateInstance<SundaytozPluginForUnityEditor>();
#endif
        }

        // 네트워크 상태 변화를 감지하기 위해 
        SundaytozResponseHandler.Instance.AddHandler(StzNativeAction.CHANGED_NETWORK_STATUS, __OnChangedNetworkStatus, null, false /* always */);

        StzNativeCall call = new StzNativeCall(StzNativeAction.INITIALIZE);
        SundaytozResponseHandler.Instance.AddHandler(call.getAction(), complete);
        plugin.request(call);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inStatus"></param>
    /// <param name="inResult"></param>
    private void __OnChangedNetworkStatus(int inStatus, JSONNode inResult)
    {
        string networkType = inResult[StzNativeStringKeys.Params.networkType].Value;
        OnChangedNetworkStatus(networkType);
    }

    /// <summary>
    /// 로컬 푸시 메세지 등록 
    /// </summary>
    /// <param name="inId"></param>
    /// <param name="inTime"></param>
    /// <param name="inTitle"></param>
    /// <param name="inMessage"></param>
    /// <param name="inIcon"></param>
    /// <param name="inCounter"></param>
    /// <param name="inType"></param>
    public void AddLocalNotification(int inId, int inTime, string inTitle, string inMessage, int inCounter, int inType, string inBgImageName = "", string inTitleColor = "", string inMsgColor = "", int titleSize = 15, int msgSize = 15)
    {
        Debug.Log(string.Format("[Sundaytoz] add LocalNotification {0}: {1}", inId, inMessage));

        StzNativeCall call = new StzNativeCallAddLocalNotification(inId, inTime, inTitle, inMessage, inCounter, inType, inBgImageName, inTitleColor, inMsgColor, titleSize, msgSize);
        plugin.request(call);
    }

    public void SetTextToSystemClipboard(string inText)
    {
        Debug.Log(string.Format("[Sundaytoz] set Text to System clipboard {0}", inText));

        StzNativeCall call = new StzNativeCallSetTextToSystemClipboard(inText);
        plugin.request(call);
    }

    /// <summary>
    /// 등록된 로컬 푸시 메세지 취소 
    /// </summary>
    /// <param name="inId"></param>
    /// <param name="inType"></param>
    public void CancelLocalNotification(int inId, int inType)
    {
        Debug.Log(string.Format("[Sundaytoz] cancel LocalNotification {0}: {1}", inId, inType));

        StzNativeCall call = new StzNativeCallCancelLocalNotification(inId, inType);
        plugin.request(call);
    }

    /// <summary>
    /// 모든 로컬 푸시 메세지 취소 
    /// </summary>
    public void CancelLocalNotificationAll()
    {
        if(plugin == null)
        {
            return;
        }

        Debug.Log("[Sundaytoz] cancel LocalNotification ALL!!");

        StzNativeCall call = new StzNativeCall(StzNativeAction.LOCAL_NOTIFICATION_ALL_CANCEL);
        plugin.request(call);
    }

    /// <summary>
    /// 시스템 팝업 출력 
    /// </summary>
    /// <param name="inTitle"></param>
    /// <param name="inMessage"></param>
    /// <param name="inOkayLabel"></param>
    /// <param name="inCancelLabel"></param>
    /// <param name="inOnOkay"></param>
    /// <param name="inOnCancel"></param>
    public void ShowAlert(string inTitle, string inMessage, string inOkayLabel = null, string inCancelLabel = null, NoParamCallback inOnOkay = null, NoParamCallback inOnCancel = null)
    {
        Debug.Log("[Sundaytoz] Show alert...");

        SundaytozResponseHandler.CompleteDelegate completed = null;

        if(inOnOkay != null || inOnCancel != null)
        {
            completed = delegate (int inStatus, JSONNode inResult)
            {
                int buttonId = (inResult.ContainsKey("button") ? inResult["button"].AsInt : -1);

                if(buttonId == 0 && inOnOkay != null)
                {
                    inOnOkay();
                }
                else if(buttonId == 1 && inOnCancel != null)
                {
                    inOnCancel();
                }
            };
        }

        StzNativeCall call = new StzNativeCallShowAlert(inTitle, inMessage, (inOkayLabel != null && inOkayLabel.Length > 0 ? inOkayLabel : "확인"), (inCancelLabel != null && inCancelLabel.Length > 0 ? inCancelLabel : null));
        SundaytozResponseHandler.Instance.AddHandler(call.getAction(), completed);
        plugin.request(call);
    }

	public void GetPushId(SundaytozResponseHandler.CompleteDelegate complete, SundaytozResponseHandler.ErrorDelegate error)
	{
		if(plugin == null)
		{
			return;
		}
		
		StzNativeCall call = new StzNativeCall(StzNativeAction.GET_PUSH_ID);
        SundaytozResponseHandler.Instance.AddHandler(call.getAction(), complete, error);
        plugin.request(call);
	}

	/// <summary>
	/// 현재 네트워크 상태를 NOT_CONNECTED, WIFI, MOBILE로 구분해 반환 
	/// </summary>
	/// <returns></returns>
	public string GetNetworkStatus()
	{
		if(plugin == null)
		{
			return ENetworkType.NOT_CONNECTED;
		}

		return plugin.GetNetworkStatus();
	}

	/// <summary>
	/// 앱 인스톨 여부
	/// </summary>
	public bool IsInstalled( string packageName )
	{
		if(plugin == null)
		{
			return false;
		}

		return plugin.IsInstalled(packageName);
	}

	public EPermissionGrantType CheckPermission( string permission )
	{
		if(plugin == null)
		{
			return EPermissionGrantType.NONE;
		}
		Debug.Log(string.Format("[Sundaytoz] Check Permission {0}", permission ));

		return plugin.GetPermissionGrantStatus(permission);
	}


	public string CheckAllPermission()
	{
		Debug.Log("[Sundaytoz] CheckAllPermission");
		List<string> ret = new List<string>();
		if(plugin == null)
		{
			return String.Empty;
		}
		return plugin.GetAllPermissions();
	}

	public void RequestPermission( string permission, SundaytozResponseHandler.CompleteDelegate complete, SundaytozResponseHandler.ErrorDelegate error)
	{
		if (plugin == null) {
			return;
		}
		Debug.Log (string.Format ("SundaytozNativeExtension::RequestPermission()->permission {0}", permission));
		StzNativeCall call = new StzNativeCallRequestPermission ( permission );
		SundaytozResponseHandler.Instance.AddHandler (call.getAction (), complete, error);
		plugin.request (call);
	}

	public void showAppDetail()
	{
		plugin.showAppDetail ();
	}

    public void clearInstallReferrer()
    {
        StzNativeCall call = new StzNativeCall(StzNativeAction.CLEAR_INSTALL_REFERRER);
        plugin.request(call);
    }

#if UNITY_IOS
	public void StartLoading()
	{
		StzNativeCall call = new StzNativeCall(StzNativeAction.START_DEVICE_LOADING);
		plugin.request(call);
	}

	public void StopLoading()
	{
		StzNativeCall call = new StzNativeCall(StzNativeAction.STOP_DEVICE_LOADING);
		plugin.request(call);
	}
#endif

}
