using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.Collections.Generic;

public class SundaytozResponseHandler : MonoBehaviour
{
	private static SundaytozResponseHandler _instance;
    public static SundaytozResponseHandler Instance
    {
        get
        {
            if(_instance == null)
            {
				_instance = GameObject.FindObjectOfType(typeof(SundaytozResponseHandler)) as SundaytozResponseHandler;

                if(!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "SundaytozResponseHandler";
                    _instance = container.AddComponent(typeof(SundaytozResponseHandler)) as SundaytozResponseHandler;
					DontDestroyOnLoad(_instance);
                }
            }

            return _instance;
        }
    }
	
    /// <summary>
    /// 콜백 
    /// </summary>
	public delegate void CompleteDelegate(int inStatus, JSONNode inResult);
	public delegate void ErrorDelegate(int inStatus, string inMessage);

    /* 최근 상태코드 */
    public static int lastestStatus { get; private set; }

    /* 최근 오류내용 */
    public static string lastestError { get; private set; }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="result"></param>
	public static void __OnCompleted(string result)
    {
		_instance.OnSundaytozResponseComplete(result);
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="error"></param>
	public static void __OnError(string error)
    {
        _instance.OnSundaytozResponseError(error);
    }

    /// <summary>
    /// 
    /// </summary>
    private class ActionHandler
    {
        public bool once = true;
        public SundaytozResponseHandler.CompleteDelegate complete = null;
        public SundaytozResponseHandler.ErrorDelegate error = null;

        public void Clear()
        {
            complete = null;
            error = null;
        }
    }

    /* 핸들러맵 */
    private Dictionary<string, List<ActionHandler>> _handlers = new Dictionary<string, List<ActionHandler>>();

    /// <summary>
    /// 특정 액션에 대한 핸들러를 추가 
    /// 이 핸들러는 한 번 응답이후 제거된다 
    /// </summary>
    /// <param name="inAction"></param>
    /// <param name="inOnComplete"></param>
    /// <param name="inOnError"></param>
    /// <param name="inOnce"></param>
    public void AddHandler(StzNativeAction inAction, SundaytozResponseHandler.CompleteDelegate inOnComplete, SundaytozResponseHandler.ErrorDelegate inOnError = null, bool inOnce = true)
    {
        string action = inAction.ToString();
        List<ActionHandler> handlers = null;

        if(_handlers.ContainsKey(action))
        {
            handlers = _handlers[action];
        }
        else
        {
            handlers = new List<ActionHandler>();
            _handlers[action] = handlers;
        }

        ActionHandler handler = new ActionHandler();
        handler.complete = inOnComplete;
        handler.error = inOnError;
        handler.once = inOnce;

        handlers.Add(handler);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="result"></param>
    public void OnSundaytozResponseComplete(string inRAW)
    {
        JSONNode root = JSON.Parse(inRAW);
        string action = root[StzNativeStringKeys.Params.action];

        if(!_handlers.ContainsKey(action))
        {
            return;
        }

        List<ActionHandler> handlers = _handlers[action];

        JSONNode result = (root.ContainsKey(StzNativeStringKeys.Params.result) ? root[StzNativeStringKeys.Params.result] : null);
        int status = (result != null ? result[StzNativeStringKeys.Params.status].AsInt : -1);

        lastestStatus = status;
        lastestError = "";

        bool enableRemove = true;

        for(int i = 0; i < handlers.Count; ++i)
        {
            ActionHandler handler = handlers[i];

            if(handler.complete != null)
            {
                handler.complete(status, result);
            }

            if(handler.once)
            {
                handler.Clear();
            }
            else
            {
                enableRemove = false;
            }
        }

        if(enableRemove)
        {
            _handlers.Remove(action);
        }
    }
	
	/// <summary>
    /// 
    /// </summary>
    /// <param name="error"></param>
	public void OnSundaytozResponseError(string error)
    {
        JSONNode root = JSON.Parse(error);
        string action = root[StzNativeStringKeys.Params.action].Value;

        if(!_handlers.ContainsKey(action))
        {
            return;
        }

        List<ActionHandler> handlers = _handlers[action];

        int status = -1;
        string message = null;

        JSONNode failReason = root[StzNativeStringKeys.Params.error];

        if(failReason != null)
        {
            status = failReason[StzNativeStringKeys.Params.code].AsInt;
            message = failReason[StzNativeStringKeys.Params.message].Value;
        }

        lastestStatus = status;
        lastestError = message;

        bool enableRemove = true;

        for(int i = 0; i < handlers.Count; ++i)
        {
            ActionHandler handler = handlers[i];

            if(handler.error != null)
            {
                handler.error(status, message);
            }

            if(handler.once)
            {
                handler.Clear();
            }
            else
            {
                enableRemove = false;
            }
        }

        if(enableRemove)
        {
            _handlers.Remove(action);
        }
    }
}
