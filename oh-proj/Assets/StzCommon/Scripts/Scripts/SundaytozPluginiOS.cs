using System.Runtime.InteropServices;
using UnityEngine;
using STZ_Common;

#if UNITY_IOS
public class SundaytozPluginiOS : SundaytozPluginBase
{
    [DllImport("__Internal")]
    private static extern void sundaytozUnityExtension(string action, ResponseCallback callback);
    [DllImport("__Internal")]
    private static extern bool isIntalled(string appScheme);
    private delegate void ResponseCallback(bool hasError, string response);

    /// <summary>
    /// 
    /// </summary>
    public SundaytozPluginiOS()
    {
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hasError"></param>
    /// <param name="response"></param>
    [AOT.MonoPInvokeCallback(typeof(ResponseCallback))]
    private static void handleResponseCallback(bool hasError, string response)
    {
        if(hasError == false)
        {
            SundaytozResponseHandler.__OnCompleted(response);
        }
        else
        {
            SundaytozResponseHandler.__OnError(response);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCall"></param>
    public override void request(StzNativeCall inCall)
    {
        sundaytozUnityExtension(inCall.getParamString(), handleResponseCallback);
    }

    /// <summary>
    /// 현재 네트워크 상태 (ENetworkType)
    /// </summary>
    /// <returns></returns>
    public override string GetNetworkStatus()
    {
        string status = "";

        switch(UnityEngine.Application.internetReachability)
        {
            default:
            case UnityEngine.NetworkReachability.NotReachable:
            {
                status = ENetworkType.NOT_CONNECTED;
                break;
            }

            case UnityEngine.NetworkReachability.ReachableViaCarrierDataNetwork:
            {
                status = ENetworkType.MOBILE;
                break;
            }

            case UnityEngine.NetworkReachability.ReachableViaLocalAreaNetwork:
            {
                status = ENetworkType.WIFI;
                break;
            }
        }

        return status;
    }

	/// <summary>
	/// 현재 퍼미션 획득 상태 (EPermissionGrantType)
	/// </summary>
	/// <returns></returns>
	public override EPermissionGrantType GetPermissionGrantStatus( string permissionName )
	{
		return EPermissionGrantType.ALREADY_GRANTED;
	}

    public override string GetAllPermissions()
    {
        return string.Empty;
    }

    /// <summary>
	/// 앱 정보 화면으로 유도
	/// </summary>
	public override void showAppDetail ()
	{
	}

    /// <summary>
    /// 앱 인스톨 여부
    /// <?xml version="1.0" encoding="UTF-8"?>
    /// 아래의 값으로만 조회해야합니다 
//    kakao88905059458847504://사천성
//    kakao90933379376280272://애2
//    kakao16f9f9d613db3862b749a7ea0a81adce://애3
//    kakao94159215306305618://상하이 국내
//    fb1532140153779770://상하이 스매쉬
//    kakao0fe931d8d10842433c7911a8c67269bf://터치 국내
//    fb245310342622069://백야드
    /// </summary>
    public override bool IsInstalled(string urlScheme)
    {
        Debug.Log("SundaytozPluginiOS::IsInstalled()->");
        return isIntalled(urlScheme);
    }
}
#endif
