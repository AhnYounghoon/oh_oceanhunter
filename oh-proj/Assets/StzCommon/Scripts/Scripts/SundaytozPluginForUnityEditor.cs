using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using SimpleJSON;
using STZ_Common;

public class SundaytozPluginForUnityEditor : SundaytozPluginBase
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCall"></param>
    public override void request(StzNativeCall inCall)
    {
        Debug.Log("[SUNDAYTOZ] " + inCall.getParamString());

        StzNativeAction action = inCall.getAction();

        JSONClass json = new JSONClass();
        json[StzNativeStringKeys.Params.action] = action.ToString();

        JSONClass result = new JSONClass();
        result[StzNativeStringKeys.Params.status] = "0";

        json.Add(StzNativeStringKeys.Params.result, result);

        switch(action)
        {
            default:
            {
                SundaytozResponseHandler.Instance.OnSundaytozResponseComplete(json.ToString());
                break;
            }
        }
    }

    /// <summary>
    /// 현재 네트워크 상태 (ENetworkType)
    /// </summary>
    /// <returns></returns>
    public override string GetNetworkStatus()
    {
        string status = "";

        switch(UnityEngine.Application.internetReachability)
        {
            default:
            case UnityEngine.NetworkReachability.NotReachable:
            {
                status = ENetworkType.NOT_CONNECTED;
                break;
            }

            case UnityEngine.NetworkReachability.ReachableViaCarrierDataNetwork:
            {
                status = ENetworkType.MOBILE;
                break;
            }

            case UnityEngine.NetworkReachability.ReachableViaLocalAreaNetwork:
            {
                status = ENetworkType.WIFI;
                break;
            }
        }

        return status;
    }

	public override EPermissionGrantType GetPermissionGrantStatus (string permissionName)
	{
		return EPermissionGrantType.ALREADY_GRANTED;
	}

    public override string GetAllPermissions()
    {
        return string.Empty;
    }

    public override void showAppDetail ()
	{
	}

    public override bool IsInstalled(string packageName)
    {
        return true;
    }

}
