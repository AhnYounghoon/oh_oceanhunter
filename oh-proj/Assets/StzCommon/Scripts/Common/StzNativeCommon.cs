using System.Collections;

public enum StzNativeAction
{
	UNKNOWN,

    /* 초기화 */
    INITIALIZE,

    /* 네트워크 상태 변화 */
    CHANGED_NETWORK_STATUS,

    /* 로컬 푸시 메세지 등록 */
    LOCAL_NOTIFICATION_ADD,

    /* 로컬 푸시 메세지 취소 */
    LOCAL_NOTIFICATION_CANCEL,

    /* 전체 로컬 푸시 메세지 취소 */
    LOCAL_NOTIFICATION_ALL_CANCEL,

	/* 팝업창 출력 */
	SHOW_ALERT,

    /* 푸시토큰 조회 */
    GET_PUSH_ID,

    SET_TEXT_TO_SYSTEM_CLIPBOARD,

	REQUEST_PERMISSION,

#if UNITY_IOS
    START_DEVICE_LOADING,
	
	STOP_DEVICE_LOADING,
#endif

    CLEAR_INSTALL_REFERRER
};

public class StzNativeStringKeys
{
	public static string version = "1.0.1";

	public class Params
    {
		public const string action = "action";
		public const string result = "result";
		public const string error = "error";
		public const string code = "code";

        /* 상태 코드 */
        public const string status    = "status";

        /* 상태와 관련된 세부내용 */
        public const string statusMessage = "message";

        public const string type          = "type";
        public const string alarmId       = "alarmId";
        public const string time          = "time";
        public const string message       = "message";
        public const string title         = "title";
        public const string iconId        = "iconId";
        public const string counter       = "counter";
        public const string text          = "text";
        public const string okay          = "okay";
        public const string cancel        = "cancel";

        public const string bgImageName   = "bgImageName";
        public const string titleColor    = "titleColor";
        public const string msgColor      = "msgColor";        
	    public const string titleSize	  = "titleSize";        
	    public const string msgSize       = "msgSize";        

        /// <summary>
        /// 초기화 완료시 응답받은 데이터 
        /// </summary>
        public const string device_name	= "device_name";
		public const string carrier 		= "carrier";
		public const string country 		= "country";
		public const string os_version	    = "os_version";
		public const string app_params	    = "app_params";
        public const string timezone        = "timezone";
        public const string timeoffset      = "timeoffset";

		public const string registration_id = "registration_id";

        public const string networkType = "networkType";

		public const string largePictureUrl = "largePictureUrl";
		public const string largeImageUrl = "largeImageUrl";

		public const string permission = "permission";

        public const string install_referer      = "install_referer";
    }
}
