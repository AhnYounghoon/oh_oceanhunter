﻿using SimpleJSON;
using System;

namespace STZ_Common
{
	public class StzNativeCallAddLocalNotification : StzNativeCall
	{
		/* 아이템 고유코드 */
		private JSONClass _data;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="inId"></param>
		/// <param name="inTime"></param>
		/// <param name="inTitle"></param>
		/// <param name="inMessage"></param>
		/// <param name="inIcon"></param>
		/// <param name="inCounter"></param>
		/// <param name="inType">ELocalNotificationType</param>
		public StzNativeCallAddLocalNotification(int inId, int inTime, string inTitle, string inMessage, int inCounter, int inType, string inBgImageName, string inTitleColor, string inMsgColor, int titleSize, int msgSize) : base(StzNativeAction.LOCAL_NOTIFICATION_ADD)
		{
			_data = makeDefaultParam();

			_data[StzNativeStringKeys.Params.alarmId] = inId.ToString();
			_data[StzNativeStringKeys.Params.time] = inTime.ToString();
			_data[StzNativeStringKeys.Params.title] = inTitle;
			_data[StzNativeStringKeys.Params.message] = inMessage;
			_data[StzNativeStringKeys.Params.title] = inTitle;
			_data[StzNativeStringKeys.Params.counter] = inCounter.ToString();
			_data[StzNativeStringKeys.Params.type] = inType.ToString();

			_data[StzNativeStringKeys.Params.bgImageName] 	= inBgImageName;
			_data[StzNativeStringKeys.Params.titleColor] 	= inTitleColor;
			_data[StzNativeStringKeys.Params.msgColor] 		= inMsgColor;
			_data[StzNativeStringKeys.Params.titleSize] 	= titleSize.ToString();
			_data[StzNativeStringKeys.Params.msgSize] 		= msgSize.ToString();

		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string getParamString()
		{
			return _data.ToString();
		}
	};
}