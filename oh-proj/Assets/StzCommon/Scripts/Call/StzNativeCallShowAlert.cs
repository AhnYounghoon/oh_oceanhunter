﻿using SimpleJSON;
using System;

namespace STZ_Common
{
	public class StzNativeCallShowAlert : StzNativeCall
	{
		/*  */
		private JSONClass _data;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="inTitle"></param>
		/// <param name="inMessage"></param>
		/// <param name="inOkay"></param>
		/// <param name="inCancel"></param>
		public StzNativeCallShowAlert(string inTitle, string inMessage, string inOkay, string inCancel = null) : base(StzNativeAction.SHOW_ALERT)
		{
			_data = makeDefaultParam();

			_data[StzNativeStringKeys.Params.title] = inTitle;
			_data[StzNativeStringKeys.Params.message] = inMessage;
			_data[StzNativeStringKeys.Params.okay] = inOkay;

			if(inCancel != null)
			{
				_data[StzNativeStringKeys.Params.cancel] = inCancel;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string getParamString()
		{
			return _data.ToString();
		}
	};
}