﻿using SimpleJSON;
using System;

namespace STZ_Common
{
    public class StzNativeCallSetTextToSystemClipboard : StzNativeCall
    {
        private JSONClass _data;

        public StzNativeCallSetTextToSystemClipboard(string inText) : base(StzNativeAction.SET_TEXT_TO_SYSTEM_CLIPBOARD)
        {
            _data = makeDefaultParam();

            _data[StzNativeStringKeys.Params.text] = inText;
        }

        public override string getParamString()
        {
            return _data.ToString();
        }
    };
}

