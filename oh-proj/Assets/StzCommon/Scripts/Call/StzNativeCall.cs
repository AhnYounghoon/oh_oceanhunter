﻿using SimpleJSON;

namespace STZ_Common
{
	public class StzNativeCall
	{
		protected StzNativeAction _action;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="inAction"></param>
		public StzNativeCall(StzNativeAction inAction)
		{
			_action = inAction;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public StzNativeAction getAction()
		{
			return _action;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected JSONClass makeDefaultParam()
		{
			JSONClass json = new JSONClass();
			json[StzNativeStringKeys.Params.action] = _action.ToString();

			return json;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public virtual string getParamString()
		{
			JSONClass json = makeDefaultParam();
			return json.ToString();
		}
	}
}