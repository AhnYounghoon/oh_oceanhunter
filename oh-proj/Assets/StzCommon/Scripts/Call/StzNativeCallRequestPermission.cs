﻿using SimpleJSON;
using System;

namespace STZ_Common
{
	public class StzNativeCallRequestPermission : StzNativeCall
	{
		/* 아이템 고유코드 */
		private JSONClass _data;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="inPermission"></param>
		public StzNativeCallRequestPermission( string permission ) : base(StzNativeAction.REQUEST_PERMISSION)
		{
			_data = makeDefaultParam();

			_data[StzNativeStringKeys.Params.permission] = permission;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string getParamString()
		{
			return _data.ToString();
		}
	};
}