﻿using SimpleJSON;
using System;

namespace STZ_Common
{
	public class StzNativeCallCancelLocalNotification : StzNativeCall
	{
		/* 아이템 고유코드 */
		private JSONClass _data;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="inId"></param>
		/// <param name="inType">ELocalNotificationType</param>
		public StzNativeCallCancelLocalNotification(int inId, int inType) : base(StzNativeAction.LOCAL_NOTIFICATION_CANCEL)
		{
			_data = makeDefaultParam();
			
			_data[StzNativeStringKeys.Params.alarmId] = inId.ToString();
			_data[StzNativeStringKeys.Params.type] = inType.ToString();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string getParamString()
		{
			return _data.ToString();
		}
	}
}