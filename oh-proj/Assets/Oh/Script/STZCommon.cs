﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using System.Text.RegularExpressions;

public class STZCommon
{
	public const float SCREEN_WIDTH  = 1920f;
	public const float SCREEN_HEIGHT = 1080f;

    /// <summary>
    /// 스크린 크기에 맞게 화면 리사이즈
    /// </summary>
    public static void SetResolution(Camera inCamera)
    {
        if (Screen.width / Screen.height < SCREEN_WIDTH / SCREEN_HEIGHT)
        {
            float width = (SCREEN_HEIGHT * 0.5f) / SCREEN_HEIGHT * SCREEN_WIDTH;
            inCamera.orthographicSize = width / Screen.width * Screen.height;
        }
    }
	
	/// <summary>
	/// 해시 테이블 생성 함수
	/// </summary>
	/// <returns>파라메터로 생성된 해시 테이블.</returns>
	/// <param name="args">파라메터 나열(키, 벨류, 키, 벨류, ...).</param>
	
	public static Hashtable Hash(params object[] args){
		Hashtable hashTable = new Hashtable(args.Length/2);
		if (args.Length %2 != 0) {
			Debug.LogError("Hash requires an even number of arguments!"); 
			return null;
		}else{
			int i = 0;
			while(i < args.Length - 1) {
				hashTable.Add(args[i], args[i+1]);
				i += 2;
			}
			return hashTable;
		}
	}

    /// <summary>
    /// 해당 텍스처를 복사한다
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static Texture2D duplicateTexture(Texture2D source)
    {
        
        RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.wrapMode = TextureWrapMode.Clamp;
        readableText.Apply(true, true);
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }

    /// <summary>
    /// 같은 타입의 두 벨류를 상호 교환해주는 함수.
    /// </summary>

    public static void Swap<T>(ref T lhs, ref T rhs)
	{
		T temp;
		temp = lhs;
		lhs = rhs;
		rhs = temp;
	}

    public static float RotateTo(Vector2 a, Vector2 b)
    {
        float c = Mathf.Atan2(b.y - a.y, b.x - a.x) * 180.0f / Mathf.PI;
        return c;
    }

    public static string Truncate(string str, int startPos = 0, int length = 8)
	{
		if (str == null)
		{
			Debug.Assert(false, "[TRUNCATE] 입력된 string 값이 null 입니다.");
			return string.Empty;
		}
		
		if (str.Length <= startPos + length)
			return str;
		
		string result = str.Substring(0, length - 1);
		return result + "..";
	}
	
	/// <summary>
	/// List의 원소들을 콤마로 구분된 파라미터용 문자열로 변환 
	/// </summary>
	/// <param name="inUsers"></param>
	/// <returns></returns>
	public static string ListToString(System.Collections.Generic.List<string> inUsers)
	{
		if (inUsers == null)
		{
			return "";
		}
		
		return String.Join(",", inUsers.ToArray());
	}

    /// <summary>
    /// Split 데이터의 유무 판단
    /// </summary>
    /// <param name="inSplitArray"></param>
    /// <param name="inBaseValue"></param>
    /// <returns></returns>
    public static string GetSplitValue(string[] inSplitArray, int inIndex, string inBaseValue = "ERROR")
    {
        string value = inBaseValue;
        if (inSplitArray.Length > inIndex)
        {
            value = inSplitArray[inIndex];
        }
        return value;
    }

    protected static Regex _itemIDRegex = new Regex("(?<type>[0-9]+):(?<sub>[0-9]+):(?<count>[0-9]+)");

    /// <summary>
    /// x:y:z 형태의 아이템 값을 분리하는 함수.
    /// </summary>
    /// <param name="itemID">분리할 값들의 모음</param>
    /// <param name="type">타입 리턴</param>
    /// <param name="item">아이템 리턴</param>
    /// <param name="count">카운트 리턴</param>
    /// <returns>bool 성공 실패 여부</returns>

    public static bool ExtractItemID(string itemID, out int type, out int item, out long count)
    {
        var match = _itemIDRegex.Match(itemID);
        if (match.Success)
        {
            type = int.Parse(match.Groups["type"].Value);
            item = int.Parse(match.Groups["sub"].Value);
            count = long.Parse(match.Groups["count"].Value);
            return true;
        }
        else
        {
            type = 0;
            item = 0;
            count = 0;
            return false;
        }
    }


    // 개 -> 를,  냥 -> 을
    public static string Josa(string s, string t)
    {
        // 원본 문구가 없을때는 빈 문자열 반환
        if (s.IsNullOrEmpty())
            return string.Empty;

        // http://taegon.kim/archives/24
        int code = (int)s[s.Length - 1] - 44032;
        //int cho = 19, jung = 21, jong = 28;
        //int i1, i2, code1, code2;

        // 한글이 아닐때
        if (code < 0 || code > 11171)
            return t;

        if (code % 28 == 0)
            return GetJosa(t, false);
        else
            return GetJosa(t, true);
    }

    private static string GetJosa(string josa, bool jong)
    {
        // jong : true면 받침있음, false면 받침없음

        if (josa == "을" || josa == "를")
            return (jong ? "을" : "를");
        if (josa == "이" || josa == "가")
            return (jong ? "이" : "가");
        if (josa == "은" || josa == "는")
            return (jong ? "은" : "는");
        if (josa == "와" || josa == "과")
            return (jong ? "와" : "과");

        // 알 수 없는 조사
        return josa;
    }

    /// <summary>
    /// 현재 날짜가 이용 가능한지 여부.
    /// </summary>
    /// <param name="inCurrentTime"></param>
    /// <param name="inStartTime"></param>
    /// <param name="inEndTime"></param>
    /// <returns></returns>
    public static bool CheckEnableDate(DateTime inCurrentTime, DateTime inStartTime, DateTime inEndTime)
    {
        return !(inCurrentTime < inStartTime || inCurrentTime > inEndTime);
    }

    /// <summary>
    /// 현재 버전과 타겟 버젼과의 비교
    /// </summary>
    /// <param name="inCurrentVersion"></param>
    /// <param name="inTargetVersion"></param>
    /// <returns>
    /// -1: 현재버전이 타겟버전보다 작다. 
    /// 0:현재버전 = 타겟버전 
    /// 1:현재버전이 타겟버전보다 크다. 
    /// </returns>
    public static int CheckVersion(string inCurrentVersion, string inTargetVersion)
    {
        string[] curVerArr = inCurrentVersion.Split('.');
        string[] targetVerArr = inTargetVersion.Split('.');

        double curVer = curVerArr[0].IntValue() * System.Math.Pow(10, 6) + curVerArr[1].IntValue() * System.Math.Pow(10, 3) + curVerArr[2].IntValue();
        double targetVer = targetVerArr[0].IntValue() * System.Math.Pow(10, 6) + targetVerArr[1].IntValue() * System.Math.Pow(10, 3) + targetVerArr[2].IntValue();
        if (targetVer < curVer)
        {
            return 1;
        }
        else if (targetVer > curVer)
        {
            return -1;
        }

        return 0;
    }

    public static bool CheckEnableVersionWithCheckSign(string inCurrentVersion, string inTargetVersion, string inCheckSign)
    {
        int checkVer = CheckVersion(inCurrentVersion, inTargetVersion);
        if (inCheckSign == "*"
            || (inCheckSign == ">>" && (checkVer == 1))
            || (inCheckSign == ">" && (checkVer == 1 || checkVer == 0))
            || (inCheckSign == "=" && checkVer == 0)
            || (inCheckSign == "<" && (checkVer == -1 || checkVer == 0))
            || (inCheckSign == "<<" && (checkVer == -1)))
        {
            return true;
        }

        return false;
    }

    public static System.DateTime Time1970 { get { return new DateTime(1970, 1, 1, 0, 0, 0, 0); } }

    public static bool CheckEnableVersion(string inCurrentVersion, string inStartVersion, string inEndVersion)
    {
        int lowVal = CheckVersion(inCurrentVersion, inStartVersion);
        int highVal = CheckVersion(inCurrentVersion, inEndVersion);

        return lowVal >= 0 && highVal <= 0;
    }

    public static string osKey
    {
        get
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE
#if UNITY_ANDROID
            // NOTE @minjun SundaytozDebug/EditTestOs 셋팅을 통해 IOS/AOS/OSX/WIN 데이터를 이용해 작업할 수 있다.
            TextAsset asset = UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/LocalData/TestOsData.txt");
            if (asset != null)
            {
                var data = SimpleJSON.JSONNode.Parse(asset.text);
                if (data.ContainsKey("os"))
                {
                    return data["os"];
                }
            }
#endif
            return "win";
#elif UNITY_EDITOR_OSX
            return "osx";
#elif UNITY_ANDROID
            return "aos";
#elif UNITY_IOS
            return "ios";
#else
            return null;
#endif
        }
    }

    public static string serverOsKey
    {
        get
        {
#if UNITY_ANDROID
            return "a";
#elif UNITY_IOS
            return "i";
#else
            return "a";
#endif
        }
    }

//    public static string marketId
//    {
//        get
//        {
//#if UNITY_ANDROID
//            if (Sundaytoz.StzPaymentSettings.AndroidMarket == Sundaytoz.StzPaymentSettings.ANDROID_MARKET.ONE_STORE)
//            {
//                return "onestore";
//            }
//            else
//            {
//                return "google";
//            }
//#elif UNITY_IOS
//            return "ios";
//#else
//            return "google";
//#endif
//        }
//    }

    public static bool isEnoughMaxCache
    {
        get
        {
#if UNITY_IOS
            if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3G
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3GS
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4S
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad1Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad2Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadMini1Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen
                || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen
                )
            {
                return false;
            }
#endif

            return true;
        }
    }

    /// <summary>
    /// 0 ~ 255 색상 체계를 0 ~ 1까지 유니티 컬러로 변환한다
    /// </summary>
    /// <param name="r"></param>
    /// <param name="g"></param>
    /// <param name="b"></param>
    /// <param name="a"></param>
    /// <returns></returns>
    public static Color ToUnityColor(float r, float g, float b, float a = 255)
    {
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCurrentDate"></param>
    /// <param name="inLatestDate"></param>
    /// <param name="resetTime"></param>
    /// <returns></returns>
    public static bool PassedResetTime(DateTime inCurrentDate, DateTime inLatestDate, string resetTime)
    {
        DateTime resetDate = DateTime.Parse(StringHelper.Format("{0}-{1}-{2} {3}",
                                                                inLatestDate.Year, inLatestDate.Month, inLatestDate.Day,
                                                                resetTime));

        if (resetDate <= inLatestDate)
        {
            resetDate = resetDate.AddDays(1);
        }

        return (inCurrentDate >= resetDate);
    }

    public static bool IsValidCharacter(int inCharNum)
    {
        if (inCharNum >= 0x0000 && inCharNum <= 0x007F) // C0 Controls and Basic Latin 
        {
            return true;
        }
        else if (inCharNum >= 0x1100 && inCharNum <= 0x11FF) // Hangul Jamo
        {
            return true;
        }
        else if (inCharNum >= 0x3130 && inCharNum <= 0x318F) // Hangul Compatibility Jamo
        {
            return true;
        }
        else if (inCharNum >= 0xAC00 && inCharNum <= 0xD7AF) // Hangul Syllables
        {
            return true;
        }

        return false;
    }

    public static System.DateTime ConvertToLocalTime(System.DateTime inUTCTime)
    {
        return inUTCTime.AddHours(9);
    }

    public static System.DateTime ConvertToUTCTime(System.DateTime inLocalTime)
    {
        return inLocalTime.AddHours(-9);
    }

    /// <summary>
    /// 한글인가? 
    /// </summary>
    /// <param name="inChar"></param>
    /// <returns></returns>
    public static bool IsKorean(char inChar)
    {
        //( 한글자 || 자음 , 모음 )
        if ((0xAC00 <= inChar && inChar <= 0xD7A3) || (0x3131 <= inChar && inChar <= 0x318E))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

public class STZMath
{
    public static float BetweenAngle(Vector2 a,Vector2 b)
    {
        return Mathf.Atan2(b.y - a.y, b.x - a.x) * 180.0f / Mathf.PI;
    }

    public static long Max(long a, long b)
    {
        return a > b ? a : b;
    }
    
    public static long Min(long a, long b)
    {
        return a < b ? a : b;
    }
}