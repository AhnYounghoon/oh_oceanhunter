using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using SimpleJSON;

[Serializable]
public class LocalStageData
{
    public int index = 0;
    public string tag = string.Empty;
    public int tileSize_h = 18;
    public int tileSize_v = 10;
    public int subMissionCount = 0;
    //public GoalPatternInfo goalInfo = new GoalPatternInfo();

    public List<int> layer_block = new List<int>();
    public Dictionary<int, int> layer_block_special = null;

    public List<SeaRoute> seaRouteList = null;
    public List<TileData> tileList = null;
    public List<TokenData> tokenList = null;
    //public List<CharacterData> characterList = null;
    //public List<BoardData> boardList = null;
    //public List<TreasureData> treasureList = null;
    //public Dictionary<int, List<ObstacleData>> obstacle_Dic = null;
    //public List<HiddenStage> hiddenStageList = null;
    //public ScrollMapView scrollViewInfo = null;
    //public Dictionary<int, List<TriggerData>> trigger_Dic = null;
    //public List<GroundWaterData> groundWaterList = null;
    //public List<BiscuitTilePath> biscuitTileList = null;
    //public List<BiscuitTilePath> toastTileList = null;
    //public List<BoardData> waterFragmentMissionList = null;

    /// <summary>
    /// 맵데이터 메모리에 올리기
    /// static stage sheet의 턴, 점수 변경
    /// </summary>
    /// <param name="inNode"></param>
    /// <param name="inIsUseStatic">스태틱의 stage 시트 참조 여부</param>
    /// <returns></returns>
    public bool Bind(JSONNode inNode, bool inIsUseStatic = true)
    {
        index = inNode["stage_index"].AsInt;
        //STZDebug.Log("stage Data bind start : " + index);

        tag = inNode["stage_tag"].Value;
        tileSize_h = inNode["tile_size_h"].AsInt;
        tileSize_v = inNode["tile_size_v"].AsInt;

        //goalInfo.gGoalCount = inNode["goal"]["mission_info"][EMissionInfo.MAIN_COUNT.ToString()].AsInt;
        //goalInfo.gSubMission1 = inNode["goal"]["mission_info"][EMissionInfo.SUB_TYPE1.ToString()].AsInt;
        //goalInfo.gSubCount1 = inNode["goal"]["mission_info"][EMissionInfo.SUB_COUNT1.ToString()].AsInt;

        //int count = inType == StageDataManager.EStageDataType.Season_2 ? Statics.Instance.stage_s2.Count : Statics.Instance.stage.Count;
        //// 스태틱 데이터가 없거나 스태틱을 의도적으로 사용하지 않거나 맵툴에서는 맵데이터의 값을 사용
        //if (count <= 0 || SceneManager.GetActiveScene().name == GeneralDataManager.ESceneName.MapTool.ToString() || !inIsUseStatic)
        //{
        //    goalInfo.gScore_Star1 = inNode["goal"]["scores"]["step1"].AsInt;
        //    goalInfo.gScore_Star2 = inNode["goal"]["scores"]["step2"].AsInt;
        //    goalInfo.gScore_Star3 = inNode["goal"]["scores"]["step3"].AsInt;
        //    goalInfo.gMainType = inNode["goal"]["mission_info"][EMissionInfo.MAIN_TYPE.ToString()].AsInt;
        //}
        //else
        //{
        //    bool contain = inType == StageDataManager.EStageDataType.Season_2 ? Statics.Instance.stage_s2.ContainsKey(index) : Statics.Instance.stage.ContainsKey(index);
        //    if (!contain)
        //    {
        //        SimplePopup.Alert(StringHelper.Format("{0}스테이지 스태틱 데이터가 없습니다.", index));
        //        //Debug.Assert(Statics.Instance.stage.ContainsKey(index));
        //        return false;
        //    }

        //    ServerStatic.Stage stageData = inType == StageDataManager.EStageDataType.Season_2 ? Statics.Instance.stage_s2[index].CopyToStage() : Statics.Instance.stage[index];

        //    goalInfo.gScore_Star1 = stageData.star1_score;
        //    goalInfo.gScore_Star2 = stageData.star2_score;
        //    goalInfo.gScore_Star3 = stageData.star3_score;
        //    goalInfo.gMainType = stageData.mission_type;
        //}

        // 항로 정보
        JSONArray sea_routes = inNode["sea_routes"].AsArray;
        for (int i = 0; i < sea_routes.Count; i++)
        {
            if (seaRouteList == null)
                seaRouteList = new List<SeaRoute>();

            JSONNode srJson = sea_routes[i];
            SeaRoute sr = new SeaRoute();
            sr.Bind(srJson);

            seaRouteList.Add(sr);
        }

        // 타일 정보
        JSONArray tiles = inNode["tiles"].AsArray;

        for (int i = 0; i < tiles.Count; i++)
        {
            if (tileList == null)
                tileList = new List<TileData>();

            JSONNode tileInfo = tiles[i];

            TileData tileData = new TileData();
            tileData.Bind(tileInfo);
            tileList.Add(tileData);
        }

        // 토큰 정보
        JSONArray tokens = inNode["tokens"].AsArray;

        for (int i = 0; i < tokens.Count; i++)
        {
            if (tokenList == null)
                tokenList = new List<TokenData>();

            JSONNode tokenInfo = tokens[i];

            TokenData tokenData = new TokenData();
            tokenData.Bind(tokenInfo);
            tokenList.Add(tokenData);
        }

        //// 맵 array binding
        //{
        //    JSONNode temp = inNode["shape"]["LAYER_BLOCK"];

        //    JSONArray array = temp.AsArray;
        //    List<int> tempList = new List<int>();
        //    for (int j = 0; j < array.Count; j++)
        //    {
        //        tempList.Add(array[j].AsInt);
        //    }

        //    layer_block = tempList;
        //}

        //// 블럭 상세(특수블럭 정보등)
        //JSONArray blocks = inNode["blocks"].AsArray;
        //for (int i = 0; i < blocks.Count; i++)
        //{
        //    if (layer_block_special == null)
        //        layer_block_special = new Dictionary<int, int>();

        //    JSONNode blockInfo = blocks[i];

        //    BlockData blockData = new BlockData();
        //    blockData.Bind(blockInfo);
        //    layer_block_special.Add(blockData.index, blockData.special_type);
        //}

        //// 캐릭터 정보
        //JSONArray characters = inNode["characters"].AsArray;
        //for (int i = 0; i < characters.Count; i++)
        //{
        //    if (characterList == null)
        //        characterList = new List<CharacterData>();

        //    JSONNode charInfo = characters[i];

        //    CharacterData charData = new CharacterData();
        //    charData.Bind(charInfo);
        //    characterList.Add(charData);
        //}

        //// 유물 정보
        //JSONArray treasures = inNode["treasures"].AsArray;

        //for (int i = 0; i < treasures.Count; i++)
        //{

        //    if (treasureList == null)
        //        treasureList = new List<TreasureData>();

        //    JSONNode treasureInfo = treasures[i];

        //    TreasureData treasureData = new TreasureData();
        //    treasureData.Bind(treasureInfo);
        //    treasureList.Add(treasureData);
        //}

        //// 장애물 속성 binding
        //List<ObstacleData> obstacleList = null;
        //JSONArray obstacles = inNode["obstacles"].AsArray;

        //for (int i = 0; i < obstacles.Count; i++)
        //{
        //    if (obstacleList == null)
        //        obstacleList = new List<ObstacleData>();

        //    ObstacleData obData;
        //    JSONNode obstacleJson = obstacles[i];

        //    int obstacle_type = obstacleJson.ContainsKey("type") ? obstacleJson["type"].AsInt : -1;
        //    if (Obstacle.IsBoss(obstacle_type))
        //    {
        //        obData = new BossData();
        //    }
        //    else
        //    {
        //        obData = new ObstacleData();
        //    }

        //    obData.bind(obstacleJson);
        //    obstacleList.Add(obData);
        //}

        //if (obstacleList != null)
        //{
        //    // 동일한 인덱스별로 dictionary에 저장
        //    for (int i = 0; i < obstacleList.Count; i++)
        //    {
        //        ObstacleData obData = obstacleList[i];
        //        List<ObstacleData> resList = new List<ObstacleData>();
        //        resList.Add(obData);
        //        obstacleList.RemoveAt(0);
        //        i--;

        //        for (int j = 0; j < obstacleList.Count; j++)
        //        {
        //            if (obData.index == obstacleList[j].index)
        //            {
        //                resList.Add(obstacleList[j]);
        //                obstacleList.RemoveAt(j);
        //                j--;
        //            }
        //        }

        //        if (resList.Count > 0)
        //        {
        //            if (obstacle_Dic == null) obstacle_Dic = new Dictionary<int, List<ObstacleData>>();
        //            obstacle_Dic.Add(obData.index, resList);
        //        }
        //    }

        //    obstacleList = null;
        //}

        //// 트리거 바인딩
        //List<TriggerData> triggerList = null;
        //JSONArray triggers = inNode["triggers"].AsArray;
        //TriggerData td;
        //for (int i = 0; i < triggers.Count; i++)
        //{
        //    if (triggerList == null)
        //        triggerList = new List<TriggerData>();

        //    JSONNode triggerInfo = triggers[i];
        //    int trigger_type = triggerInfo.ContainsKey("type") ? triggerInfo["type"].AsInt : -1;

        //    if (trigger_type == (int)ETrigger.TRIGGER_BLOCK_CREATOR)
        //        td = new BlockCreatorData();
        //    else
        //        td = new TriggerData();

        //    td.Bind(triggerInfo);
        //    triggerList.Add(td);
        //}

        //if (triggerList != null)
        //{
        //    // 동일한 인덱스별로 dictionary에 저장
        //    for (int i = 0; i < triggerList.Count; i++)
        //    {
        //        TriggerData triggerData = triggerList[i];

        //        if (triggerData == null)
        //            continue;

        //        List<TriggerData> resList = new List<TriggerData>();
        //        resList.Add(triggerData);
        //        triggerList.RemoveAt(0);
        //        i--;

        //        for (int j = 0; j < triggerList.Count; j++)
        //        {
        //            if (triggerData.index == triggerList[j].index)
        //            {
        //                resList.Add(triggerList[j]);
        //                triggerList.RemoveAt(j);
        //                j--;
        //            }
        //        }

        //        if (resList.Count > 0)
        //        {
        //            if (trigger_Dic == null) trigger_Dic = new Dictionary<int, List<TriggerData>>();
        //            trigger_Dic.Add(triggerData.index, resList);
        //        }
        //    }

        //    triggerList = null;
        //}

        //// 숨겨진 스테이지 정보
        //JSONArray hidden_stages = inNode["hidden_stages"].AsArray;
        //for (int i = 0; i < hidden_stages.Count; i++)
        //{
        //    if (hiddenStageList == null)
        //        hiddenStageList = new List<HiddenStage>();

        //    JSONNode hiddenInfoJson = hidden_stages[i];
        //    HiddenStage hStage = new HiddenStage();
        //    hStage.Bind(hiddenInfoJson);

        //    hiddenStageList.Add(hStage);
        //}

        //// 스크롤 뷰 정보
        //if (inNode.ContainsKey("scroll_map_view"))
        //{
        //    JSONNode scroll_views = inNode["scroll_map_view"];
        //    scrollViewInfo = new ScrollMapView();
        //    scrollViewInfo.Bind(scroll_views);
        //}

        //// 지하수 정보
        //JSONArray ground_waters = inNode["ground_waters"].AsArray;
        //for (int i = 0; i < ground_waters.Count; i++)
        //{
        //    if (groundWaterList == null)
        //        groundWaterList = new List<GroundWaterData>();

        //    JSONNode gwInfoJson = ground_waters[i];
        //    GroundWaterData gw = new GroundWaterData();
        //    gw.Bind(gwInfoJson);

        //    groundWaterList.Add(gw);
        //}

        //// 수로 조각
        //JSONArray water_fragments = inNode["water_fragments"].AsArray;
        //for (int i = 0; i < water_fragments.Count; i++)
        //{
        //    if (waterFragmentMissionList == null)
        //        waterFragmentMissionList = new List<BoardData>();

        //    JSONNode wfInfoJson = water_fragments[i];
        //    BoardData wf = new BoardData();
        //    wf.Bind(wfInfoJson);

        //    waterFragmentMissionList.Add(wf);
        //}

        //// 비스킷 타일
        //JSONArray biscuit_tiles = inNode["biscuit_tiles"].AsArray;
        //for (int i = 0; i < biscuit_tiles.Count; i++)
        //{
        //    if (biscuitTileList == null)
        //        biscuitTileList = new List<BiscuitTilePath>();

        //    JSONNode ytInfoJson = biscuit_tiles[i];
        //    BiscuitTilePath o = new BiscuitTilePath();
        //    o.Bind(ytInfoJson);

        //    biscuitTileList.Add(o);
        //}

        //// 토스트 타일
        //JSONArray toast_tiles = inNode["toast_tiles"].AsArray;
        //for (int i = 0; i < toast_tiles.Count; i++)
        //{
        //    if (toastTileList == null)
        //        toastTileList = new List<BiscuitTilePath>();

        //    JSONNode ytInfoJson = toast_tiles[i];
        //    BiscuitTilePath o = new BiscuitTilePath();
        //    o.Bind(ytInfoJson);

        //    toastTileList.Add(o);
        //}

        //STZDebug.Log("stage Data bind end : " + index);

        return true;
    }
    
    public JSONClass Export()
    {
        JSONClass result = new JSONClass();

        // 기본 정보
        result.Add("stage_index", index.ToString());
        result.Add("stage_tag", tag);
        result.Add("tile_size_h", tileSize_h.ToString());
        result.Add("tile_size_v", tileSize_v.ToString());

        JSONClass newOne = null;

        // 항로 정보
        if (seaRouteList != null)
        {
            JSONArray srArray = new JSONArray();

            for (int i = 0; i < seaRouteList.Count; i++)
            {
                newOne = new JSONClass();
                SeaRoute sr = seaRouteList[i];

                newOne = sr.Export();
                srArray.Add(newOne);
            }

            result.Add("sea_routes", srArray);
        }

        // 타일 정보
        if (tileList != null)
        {
            JSONArray tileArray = new JSONArray();
            for (int i = 0; i < tileList.Count; i++)
            {
                TileData b = tileList[i];
                newOne = b.Export();

                tileArray.Add(newOne);
            }
            result.Add("tiles", tileArray);
        }

        // 토큰 정보
        if (tokenList != null)
        {
            JSONArray tokenArray = new JSONArray();
            for (int i = 0; i < tokenList.Count; i++)
            {
                TokenData b = tokenList[i];
                newOne = b.Export();

                tokenArray.Add(newOne);
            }
            result.Add("tokens", tokenArray);
        }

        /*
        // 미션, 점수
        JSONClass mission = new JSONClass();
        mission.Add("MAIN_TYPE", goalInfo.gMainType.ToString());
        mission.Add("MAIN_COUNT", goalInfo.gGoalCount.ToString());
        mission.Add("SUB_TYPE1", goalInfo.gSubMission1.ToString());
        mission.Add("SUB_COUNT1", goalInfo.gSubCount1.ToString());

        JSONClass score = new JSONClass();
        score.Add("step1", goalInfo.gScore_Star1.ToString());
        score.Add("step2", goalInfo.gScore_Star2.ToString());
        score.Add("step3", goalInfo.gScore_Star3.ToString());

        JSONClass newOne = new JSONClass();
        newOne.Add("mission_info", mission);
        newOne.Add("scores", score);

        result.Add("goal", newOne);

        JSONArray newArray;
        JSONArray newArraySub;

        // 캐릭터 정보 export
        if (characterList != null)
        {
            JSONArray charListArray = new JSONArray();

            for (int i = 0; i < characterList.Count; i++)
            {
                newOne = new JSONClass();
                CharacterData c = characterList[i];
                
                newOne = c.Export();
                charListArray.Add(newOne);
            }

            result.Add("characters", charListArray);
        }

        // 히든 스테이지 정보 export
        if (hiddenStageList != null)
        {
            JSONArray hiddenStageArray = new JSONArray();

            for (int i = 0; i < hiddenStageList.Count; i++)
            {
                newOne = new JSONClass();
                HiddenStage h = hiddenStageList[i];

                newOne = h.Export();
                hiddenStageArray.Add(newOne);
            }

            result.Add("hidden_stages", hiddenStageArray);
        }
        
        // 스크롤뷰 정보 export
        if (scrollViewInfo != null)
        {
            newOne = new JSONClass();
            newOne = scrollViewInfo.Export();

            result.Add("scroll_map_view", newOne);
        }

        newOne = new JSONClass();

        // 오브젝트 배치 정보
        for (int i = 0; i < (int)EStageShape.COUNT; i++)
        {
            newArray = new JSONArray();
            List<int> tempList = new List<int>();

            switch (i)
            {
                case (int)EStageShape.LAYER_BLOCK:
                    tempList = layer_block;
                    break;
            }

            for (int j = 0; j < tempList.Count; j++)
            {
                newArray.Add(tempList[j].ToString());
            }

            switch (i)
            {
                case (int)EStageShape.LAYER_BLOCK:
                    newOne.Add("LAYER_BLOCK", newArray);
                    break;
            }
        }

        result.Add("shape", newOne);

        // 특수블럭 정보
        if(layer_block_special != null)
        {
            JSONArray specialBlockArray = new JSONArray();
            foreach(int key in layer_block_special.Keys)
            {
                BlockData o = new BlockData();
                o.index = key;
                o.special_type = layer_block_special[key];

                newOne = o.Export();
                specialBlockArray.Add(newOne);
            }

            result.Add("blocks", specialBlockArray);
        }

        // 유물 정보
        if (treasureList != null)
        {
            JSONArray treasureArray = new JSONArray();
            for (int i = 0; i < treasureList.Count; i++)
            {
                TreasureData t = treasureList[i];
                newOne = t.Export();

                treasureArray.Add(newOne);
            }

            result.Add("treasures", treasureArray);
        }

        // 수로 조각 미션 정보
        if (waterFragmentMissionList != null)
        {
            JSONArray fragmentArray = new JSONArray();
            for (int i = 0; i < waterFragmentMissionList.Count; i++)
            {
                BoardData t = waterFragmentMissionList[i];
                newOne = t.Export();

                fragmentArray.Add(newOne);
            }

            result.Add("water_fragments", fragmentArray);
        }

        // 수로 정보
        if (groundWaterList != null && groundWaterList.Count != 0)
        {
            JSONArray groundWaterArray = new JSONArray();
            for (int i = 0; i < groundWaterList.Count; i++)
            {
                GroundWaterData g = groundWaterList[i];
                newOne = g.Export();

                groundWaterArray.Add(newOne);
            }
            result.Add("ground_waters", groundWaterArray);
        }

        // 과자길 정보
        if (biscuitTileList != null)
        {
            JSONArray biscuitTileArray = new JSONArray();
            for (int i = 0; i < biscuitTileList.Count; i++)
            {
                BiscuitTilePath y = biscuitTileList[i];
                newOne = y.Export();

                biscuitTileArray.Add(newOne);
            }
            result.Add("biscuit_tiles", biscuitTileArray);
        }

        // 토스트길 정보
        if (toastTileList != null)
        {
            JSONArray toastTileArray = new JSONArray();
            for (int i = 0; i < toastTileList.Count; i++)
            {
                BiscuitTilePath y = toastTileList[i];
                newOne = y.Export();

                toastTileArray.Add(newOne);
            }
            result.Add("toast_tiles", toastTileArray);
        }

        // 장애물 정보
        if (obstacle_Dic != null)
        {
            JSONArray obstacleArray = new JSONArray();
            foreach (int key in obstacle_Dic.Keys)
            {
                List<ObstacleData> obList = obstacle_Dic[key];

                foreach (ObstacleData o in obList)
                {
                    newOne = new JSONClass();

                    newOne.Add("index", o.index.ToString());
                    newOne.Add("type", o.type.ToString());

                    if (o.size != null && o.size.Count > 0)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.size.Count; j++)
                            newArraySub.Add(o.size[j].ToString());
                        newOne.Add("size", newArraySub);
                    }

                    if (o.createIndex != null && o.createIndex.Count > 0)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.createIndex.Count; j++)
                            newArraySub.Add(o.createIndex[j].ToString());
                        newOne.Add("createIndex", newArraySub);
                    }

                    if (o.groupIndex != null && o.groupIndex.Count > 0)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.groupIndex.Count; j++)
                            newArraySub.Add(o.groupIndex[j].ToString());
                        newOne.Add("groupIndex", newArraySub);
                    }

                    // 책장은 특별히 hp가 0으로도 저장되게 처리
                    if (o.hp > 0 || (Obstacle.IsBookShelf(o.type) && o.hp == 0))
                        newOne.Add("hp", o.hp.ToString());
                    if (o.target_lock > -1)
                        newOne.Add("target_lock", o.target_lock.ToString());
                    if (o.dir > EDirection.DIR_NONE)
                        newOne.Add("dir", ((int)o.dir).ToString());

                    if (o.rotate > 0)
                        newOne.Add("rotate", o.rotate.ToString());
                    if (o.count > 0)
                        newOne.Add("count", o.count.ToString());
                    if (o.break_type > 0)
                        newOne.Add("break_type", o.break_type.ToString());
                    if (o.shape > 0)
                        newOne.Add("shape", o.shape.ToString());

                    // FIXME 서브미션 고민 필요
                    if (o.mission_info != null)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.mission_info.Count; j++)
                        {
                            JSONClass sub_mission = new JSONClass();
                            MissionData m = o.mission_info[j];

                            // NOTE @minjun target_type은 구름 오브젝트에서 사용중
                            sub_mission.Add("target_type",m.target_type.ToString());
                            sub_mission.Add("m_type", m.m_type.ToString());
                            sub_mission.Add("count", m.count.ToString());

                            newArraySub.Add(sub_mission);
                        }

                        newOne.Add("mission", newArraySub);
                    }

                    if (o.ribbon_info != null)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.ribbon_info.Count; j++)
                        {
                            JSONClass ribbon_path = new JSONClass();
                            BiscuitTilePath g = o.ribbon_info[j];
                            ribbon_path = g.Export();

                            newArraySub.Add(ribbon_path);
                        }

                        newOne.Add("ribbon_info", newArraySub);
                    }

                    if (o.rail_info != null)
                    {
                        newArraySub = new JSONArray();
                        for (int j = 0; j < o.rail_info.Count; j++)
                        {
                            JSONClass rail_path = new JSONClass();
                            BiscuitTilePath g = o.rail_info[j];
                            rail_path = g.Export();

                            newArraySub.Add(rail_path);
                        }

                        newOne.Add("rail_info", newArraySub);
                    }

                    if (Obstacle.IsBoss(o.type))
                    {
                        BossData bossData = (BossData)o;

                        if (bossData.boss_type > 0)
                            newOne.Add("boss_type", bossData.boss_type.ToString());

                        newArraySub = new JSONArray();
                        for (int j = 0; j < bossData.position.Count; j++)
                            newArraySub.Add(bossData.position[j].ToString());
                        newOne.Add("position", newArraySub);

                        if (bossData.normal_skill > 0)
                            newOne.Add("normal_skill", bossData.normal_skill.ToString());
                        if (bossData.normal_create_count > 0)
                            newOne.Add("normal_create_count", bossData.normal_create_count.ToString());

                        newArraySub = new JSONArray();
                        for (int j = 0; j < bossData.normal_create_position.Count; j++)
                            newArraySub.Add(bossData.normal_create_position[j].ToString());
                        newOne.Add("normal_create_position", newArraySub);

                        //newArraySub = new JSONArray();
                        //for (int j = 0; j < bossData.normal_create_type.Count; j++)
                        //    newArraySub.Add(bossData.normal_create_type[j].ToString());
                        //newOne.Add("normal_create_type", newArraySub);

                        if (bossData.special_skill > 0)
                            newOne.Add("special_skill", bossData.special_skill.ToString());

                        //newArraySub = new JSONArray();
                        //for (int j = 0; j < bossData.special_create_type.Count; j++)
                        //    newArraySub.Add(bossData.special_create_type[j].ToString());
                        //newOne.Add("special_create_type", newArraySub);
                    }

                    obstacleArray.Add(newOne);
                }
            }
            result.Add("obstacles", obstacleArray);
        }

        // 트리거 정보
        if (trigger_Dic != null)
        {
            JSONArray triggerArray = new JSONArray();
            foreach (int key in trigger_Dic.Keys)
            {
                List<TriggerData> triggerList = trigger_Dic[key];

                foreach (TriggerData t in triggerList)
                {
                    if (t is BlockCreatorData)
                    {
                        BlockCreatorData bcd = t as BlockCreatorData;
                        newOne = bcd.Export();
                    }
                    else
                    {
                        newOne = t.Export();
                    }

                    triggerArray.Add(newOne);
                }
            }
            result.Add("triggers", triggerArray);
        }*/

        return result;
    }
    /*
    public int GetStartCount(int inScore)
    {
        int starCount = 0;
        if (inScore >= goalInfo.gScore_Star3)
            starCount = 3;
        else if (inScore >= goalInfo.gScore_Star2)
            starCount = 2;
        else if (inScore >= goalInfo.gScore_Star1)
            starCount = 1;
        else
            starCount = 0;

        GameInfo.SCORE_STAR_CNT = starCount;

        return starCount;
    }*/

    /// <summary>
    /// 맴버 데이터 초기화
    /// </summary>
    public void ClearData()
    {
        index = 0;
        tileSize_h = 18;
        tileSize_v = 10;
        subMissionCount = 0;

        layer_block = null;
        layer_block_special = null;

        //characterList = null;
        //boardList = null;
        //treasureList = null;
        //obstacle_Dic = null;
        //hiddenStageList = null;
        seaRouteList = null;
        //scrollViewInfo = null;
        //trigger_Dic = null;
        //groundWaterList = null;
        //biscuitTileList = null;
        //toastTileList = null;
        //waterFragmentMissionList = null;
    }
}