﻿using System.Collections.Generic;
using UniLinq;

/// <summary>
/// 함대 데이터
/// </summary>
public class FleetData
{
    public ServerStatic.ShipSkin        shipSkin;
    public ServerStatic.BattleObject    shipData;
    public int index;

    /// <summary>
    /// 함대 이름 (함대 이름은 배의 이름을 따른다)
    /// </summary>
    public string Name
    {
        get
        {
            return shipData.name;
        }
    }

    /// <summary>
    /// 전투력 (배와 캐릭터 무기의 데미지 총합)
    /// </summary>
    public float BattleAbility
    {
        get
        {
            float shipDamage      = shipData.damage;
            float characterDamage = 0;
            float weaponDamage    = 0;
            float total           = 0;
            total = shipDamage + characterDamage + weaponDamage;
            return total;
        }
    }

    /// <summary>
    /// 특성 포인트
    /// TODO @minjun.ha 특성 개발 후 작성해야함
    /// </summary>
    /// <returns></returns>
    public int AbilityPoint
    {
        get
        {
            return 1;
        }
    }
}
