﻿using System;
using System.Collections;
using System.Collections.Generic;

public class SortController
{
    public enum EActionType
    {
        None,
    }

    public enum ESortType
    {
        None,
    }

    //public static ServerStatic.Weapon[] SortWeaponList(List<ServerStatic.Weapon> inList, 
    //    EActionType inActionType = EActionType.None, ESortType inSortType = ESortType.None)
    //{
    //    ServerStatic.Weapon[] dataArr = inList.ToArray();
    //    Array.Sort(dataArr, new WeaponSorter(inActionType, inSortType));
    //    return dataArr;
    //}

    private class WeaponSorter : Sorter, IComparer
    {
        public WeaponSorter(EActionType inActionType, ESortType inHeartBoxSortingType) 
            : base(inActionType, inHeartBoxSortingType)
        {
        }

        protected override uint GetPriorityValue(EPriority inPriority)
        {
            uint value = 0;
            switch (inPriority)
            {
                case EPriority.Ascending:   value = 1 << 0; break;
                case EPriority.Korean:      value = 1 << 1; break;
                default:
                    break;
            }
            return value;
        }

        public int Compare(object x, object y)
        {
            return 0;
            //ServerStatic.Weapon xValue = (ServerStatic.Weapon)x;
            //ServerStatic.Weapon yValue = (ServerStatic.Weapon)y;
            //
            //KoreanCompare(xValue.name, yValue.name);
            //AscendCompare(xValue.name, yValue.name);
            //
            //if (xResult == yResult)
            //{
            //    return 0;
            //}
            //else
            //{
            //    return xResult > yResult ? -1 : 1;
            //}
        }
    }

    /// <summary>
    /// 각각의 비교대상을 비교하고 그 값을 EPriority를 bit연산에 이용하여 정렬 순위에 기준값을 계산하여 정렬 
    /// </summary>
    private abstract class Sorter
    {
        private EActionType actionType;
        private ESortType   sortingType;

        protected enum EPriority
        {
            Ascending,
            Korean,
        }

        protected uint xResult;
        protected uint yResult;

        public Sorter(EActionType inActionType, ESortType inHeartBoxSortingType)
        {
            xResult     = 0U;
            yResult     = 0U;
            actionType  = inActionType;
            sortingType = inHeartBoxSortingType;
        }

        /// <summary>
        /// 해당 enum의 우선 순위 값을 반환
        /// </summary>
        /// <param name="inPriority"></param>
        /// <returns></returns>
        protected virtual uint GetPriorityValue(EPriority inPriority)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// 한글 우선 
        /// </summary>
        protected void KoreanCompare(string xValue, string yValue)
        {
            uint koreanPriority = GetPriorityValue(EPriority.Korean);

            bool xKorean = STZCommon.IsKorean(xValue[0]);
            bool yKorean = STZCommon.IsKorean(yValue[0]);

            if (xKorean != yKorean)
            {
                if (xKorean)
                {
                    xResult |= koreanPriority;
                }
                else
                {
                    yResult |= koreanPriority;
                }
            }
        }

        /// <summary>
        /// 가나다 순 
        /// </summary>
        protected void AscendCompare(string xValue, string yValue)
        {
            uint ascendingPriority = GetPriorityValue(EPriority.Ascending);

            if (!xValue.Equals(yValue))
            {
                if (xValue.CompareTo(yValue) < 0)
                {
                    xResult |= ascendingPriority;
                }
                else
                {
                    yResult |= ascendingPriority;
                }
            }
        }
    }
}
