﻿using System;
using System.Collections.Generic;
using SimpleJSON;

[Serializable]
public class SeaRoute
{
    public int index = 0;
    public List<int> sea_route = new List<int>();
    public int open_type = 0;
    public List<int> open_value = new List<int>();

    public bool open = false;

    public void Bind(JSONNode inNode)
    {
        index = inNode.ContainsKey("index") ? inNode["index"].AsInt : -1;

        JSONArray array = null;
        if (inNode.ContainsKey("sea_route_value"))
        {
            array = inNode["sea_route_value"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                sea_route.Add(array[i].AsInt);
            }
        }

        open_type = inNode.ContainsKey("open_type") ? inNode["open_type"].AsInt : 0;

        if (inNode.ContainsKey("open_value"))
        {
            array = inNode["open_value"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                open_value.Add(array[i].AsInt);
            }
        }
    }

    public JSONClass Export()
    {
        JSONClass newOne = new JSONClass();
        JSONArray newArraySub = null;

        newOne.Add("index", index.ToString());

        newArraySub = new JSONArray();
        for (int j = 0; j < sea_route.Count; j++)
            newArraySub.Add(sea_route[j].ToString());
        newOne.Add("sea_route_value", newArraySub);

        if (open_type > 0)
        {
            newOne.Add("open_type", open_type.ToString());

            newArraySub = new JSONArray();
            for (int j = 0; j < open_value.Count; j++)
                newArraySub.Add(open_value[j].ToString());
            newOne.Add("open_value", newArraySub);
        }

        return newOne;
    }

    public void Clear()
    {
        open = false;
    }
}
