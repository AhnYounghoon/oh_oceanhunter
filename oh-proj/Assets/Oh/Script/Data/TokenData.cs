﻿using System;
using System.Collections.Generic;
using SimpleJSON;

public enum ETokenType
{
    TOKEN_NONE = 0,
    TOKEN_MOVE = 1,
    TOKEN_FIX = 1,
    TOKEN_CONDITION = 1,

    TOTAL
};

[Serializable]
public class TokenData
{
    public int index = -1;
    public int type = 0;
    public int appear_rating = 100;
    public int appear_condition = 0;
    public int appear_position = -1;
    public string wave_info = string.Empty;
    public List<int> moving_path = null;

    public bool active = true;

    public int hp = 0;
    public int target_lock = -1;
    public EDirection dir = EDirection.DIR_NONE;
    public int rotate = 0;
    public int count = 0;
    public int break_type = 0;
    public int shape = 0;

    public List<MissionData> mission_info = null;
    //public List<BiscuitTilePath> rail_info = null;

    virtual public void Bind(JSONNode inNode)
    {
        index = inNode.ContainsKey("index") ? inNode["index"].AsInt : -1;
        type = inNode.ContainsKey("type") ? inNode["type"].AsInt : 0;
        appear_rating = inNode.ContainsKey("appear_rating") ? inNode["appear_rating"].AsInt : 100;
        appear_condition = inNode.ContainsKey("appear_condition") ? inNode["appear_condition"].AsInt : 0;
        appear_position = inNode.ContainsKey("appear_position") ? inNode["appear_position"].AsInt : -1;
        wave_info = inNode.ContainsKey("wave_info") ? inNode["wave_info"].Value : string.Empty;

        //hp = inNode.ContainsKey("hp") ? inNode["hp"].AsInt : -1;
        //target_lock = inNode.ContainsKey("target_lock") ? inNode["target_lock"].AsInt : -1;
        //dir = inNode.ContainsKey("dir") ? (EDirection)inNode["dir"].AsInt : EDirection.DIR_NONE;
        //rotate = inNode.ContainsKey("rotate") ? inNode["rotate"].AsInt : 0;
        //count = inNode.ContainsKey("count") ? inNode["count"].AsInt : 0;
        //break_type = inNode.ContainsKey("break_type") ? inNode["break_type"].AsInt : -1;
        //shape = inNode.ContainsKey("shape") ? inNode["shape"].AsInt : 0;

        JSONArray array = null;

        if (inNode.ContainsKey("moving_path"))
        {
            moving_path = new List<int>();

            array = inNode["moving_path"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                moving_path.Add(array[i].AsInt);
            }
        }        

        //if (inNode.ContainsKey("mission"))
        //{
        //    mission_info = new List<MissionData>();

        //    array = inNode["mission"].AsArray;

        //    for (int i = 0; i < array.Count; i++)
        //    {
        //        MissionData o = new MissionData();
        //        o.bind(array[i]);
        //        o.target_index = index;
        //        o.target_type = (Obstacle.IsCloud(type) || Obstacle.IsMagicPot(type)) ? array[i].ContainsKey("target_type") ? array[i]["target_type"].AsInt : -1 : type;
        //        mission_info.Add(o);
        //    }
        //}

        //if (inNode.ContainsKey("rail_info"))
        //{
        //    rail_info = new List<BiscuitTilePath>();

        //    array = inNode["rail_info"].AsArray;

        //    for (int i = 0; i < array.Count; i++)
        //    {
        //        BiscuitTilePath o = new BiscuitTilePath();
        //        o.Bind(array[i]);
        //        rail_info.Add(o);
        //    }
        //}
    }

    public JSONClass Export()
    {
        JSONClass newOne = new JSONClass();
        JSONArray newArraySub = null;

        newOne.Add("index", index.ToString());
        newOne.Add("type", type.ToString());
        if (appear_rating < 100)
            newOne.Add("appear_rating", appear_rating.ToString());
        if (appear_condition > 0)
            newOne.Add("appear_condition", appear_condition.ToString());
        if (appear_position > -1)
            newOne.Add("appear_position", appear_position.ToString());
        if (!wave_info.IsNullOrEmpty())
            newOne.Add("wave_info", wave_info.ToString());

        if (moving_path != null)
        {
            newArraySub = new JSONArray();
            for (int i = 0; i < moving_path.Count; i++)
                newArraySub.Add(moving_path[i].ToString());
            newOne.Add("moving_path", newArraySub);
        }

        return newOne;
    }

    public TokenData Clone()
    {
        TokenData newOne = new TokenData();

        newOne.index = index;
        newOne.type = type;
        newOne.appear_rating = appear_rating;
        newOne.appear_condition = appear_condition;
        newOne.appear_position = appear_position;
        newOne.wave_info = wave_info;

        if (moving_path != null)
        {
            newOne.moving_path = new List<int>();
            newOne.moving_path.AddRange(moving_path);
        }

        return newOne;
    }

    public TokenData(TokenData oldOne)
    {
        this.index = oldOne.index;
        this.type = oldOne.type;
        this.hp = oldOne.hp;
        this.target_lock = oldOne.target_lock;
        this.break_type = oldOne.break_type;
        this.dir = oldOne.dir;
        this.rotate = oldOne.rotate;
        this.count = oldOne.count;
        this.shape = oldOne.shape;

        if (oldOne.moving_path != null)
        {
            this.moving_path = new List<int>();
            this.moving_path.AddRange(oldOne.moving_path);
        }

        if (oldOne.mission_info != null)
        {
            this.mission_info = new List<MissionData>();

            foreach (MissionData m in oldOne.mission_info)
            {
                MissionData newOne = m.Clone();
                this.mission_info.Add(newOne);
            }
        }

        //if (oldOne.rail_info != null)
        //{
        //    this.rail_info = new List<BiscuitTilePath>();

        //    foreach (BiscuitTilePath o in oldOne.rail_info)
        //    {
        //        BiscuitTilePath newOne = o.Clone();
        //        this.rail_info.Add(newOne);
        //    }
        //}
    }

    public TokenData()
    {

    }
}

[Serializable]
public class BossData : TokenData
{
    public int boss_type = 0;
    public List<int> position = new List<int>();
    public int normal_skill = 0;
    public int normal_create_count = 0;
    public List<int> normal_create_position = new List<int>();
    //public List<int> normal_create_type = new List<int>();

    public int special_skill = 0;
    //public List<int> special_create_type = new List<int>();

    override public void Bind(JSONNode inNode)
    {
        base.Bind(inNode);

        boss_type = inNode.ContainsKey("boss_type") ? inNode["boss_type"].AsInt : 0;

        JSONArray array;
        array = inNode["position"].AsArray;
        for (int i = 0; i < array.Count; i++)
        {
            position.Add(array[i].AsInt);
        }

        normal_skill = inNode.ContainsKey("normal_skill") ? inNode["normal_skill"].AsInt : 0;
        normal_create_count = inNode.ContainsKey("normal_create_count") ? inNode["normal_create_count"].AsInt : 0;

        array = inNode["normal_create_position"].AsArray;
        for (int i = 0; i < array.Count; i++)
        {
            normal_create_position.Add(array[i].AsInt);
        }

        //array = inNode["normal_create_type"].AsArray;
        //for (int i = 0; i < array.Count; i++)
        //{
        //    normal_create_type.Add(array[i].AsInt);
        //}

        special_skill = inNode.ContainsKey("special_skill") ? inNode["special_skill"].AsInt : 0;

        //array = inNode["special_create_type"].AsArray;
        //for (int i = 0; i < array.Count; i++)
        //{
        //    special_create_type.Add(array[i].AsInt);
        //}
    }

    public BossData()
    {

    }

    public BossData(BossData oldOne) : base(oldOne)
    {
        this.boss_type = oldOne.boss_type;

        if (oldOne.position != null)
        {
            this.position = new List<int>();
            this.position.AddRange(oldOne.position);
        }

        this.normal_skill = oldOne.normal_skill;
        this.normal_create_count = oldOne.normal_create_count;
        if (oldOne.normal_create_position != null)
        {
            this.normal_create_position = new List<int>();
            this.normal_create_position.AddRange(oldOne.normal_create_position);
        }

        //if (oldOne.normal_create_type != null)
        //{
        //    this.normal_create_type = new List<int>();
        //    this.normal_create_type.AddRange(oldOne.normal_create_type);
        //}

        this.special_skill = oldOne.special_skill;
        //if (oldOne.special_create_type != null)
        //{
        //    this.special_create_type = new List<int>();
        //    this.special_create_type.AddRange(oldOne.special_create_type);
        //}
    }
}

[Serializable]
public class MissionData
{
    public int m_type = 0;
    public int count = 0;
    public int unit = 0;
    // 서브 미션 대상이되는 오브젝트 인덱스
    public int target_index = -1;
    public int target_type = 0;

    public bool isCompleted = false;
    public int charged_count = 0;

    public void bind(JSONNode inNode)
    {
        m_type = inNode.ContainsKey("m_type") ? inNode["m_type"].AsInt : -1;
        count = inNode.ContainsKey("count") ? inNode["count"].AsInt : 0;
        unit = inNode.ContainsKey("action_unit") ? inNode["action_unit"].AsInt : -1;
    }

    public MissionData Clone()
    {
        MissionData newOne = new MissionData();

        newOne.m_type = m_type;
        newOne.count = count;
        newOne.unit = unit;
        newOne.target_index = target_index;
        newOne.target_type = target_type;

        return newOne;
    }
}
