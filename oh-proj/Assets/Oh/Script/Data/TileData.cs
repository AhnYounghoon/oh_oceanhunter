﻿using UnityEngine;
using System;
using System.Collections;
using SimpleJSON;

[Serializable]
public class TileData
{
    public int index = -1;
    public int id = 0;
    public int type = 0;
    public int shape = 0;
    public int event_type = 0;
    public int event_amount = 1;
    public int reward_type = 0;
    public string reward_value = string.Empty;
    public int appear_rating = 100;
    public string sailing_pass = string.Empty;

    // 이벤트 수행 여부 정보
    public bool event_occur = false;
    public bool active = true;

    public void Bind(JSONNode inNode)
    {
        index = inNode.ContainsKey("index") ? inNode["index"].AsInt : -1;
        id = inNode.ContainsKey("id") ? inNode["id"].AsInt : 0;
        type = inNode.ContainsKey("type") ? inNode["type"].AsInt : 0;
        shape = inNode.ContainsKey("shape") ? inNode["shape"].AsInt : 0;
        event_type = inNode.ContainsKey("event_type") ? inNode["event_type"].AsInt : 0;
        event_amount = inNode.ContainsKey("event_amount") ? inNode["event_amount"].AsInt : 1;
        reward_type = inNode.ContainsKey("reward_type") ? inNode["reward_type"].AsInt : 0;
        reward_value = inNode.ContainsKey("reward_value") ? inNode["reward_value"].Value : string.Empty;
        appear_rating = inNode.ContainsKey("appear_rating") ? inNode["appear_rating"].AsInt : 100;
        sailing_pass = inNode.ContainsKey("sailing_pass") ? inNode["sailing_pass"].Value : string.Empty;
    }

    public JSONClass Export()
    {
        JSONClass newOne = new JSONClass();

        newOne.Add("index", index.ToString());
        if (id > 0)
            newOne.Add("id", id.ToString());
        newOne.Add("type", type.ToString());
        
        if (type == (int)ETile.TILE_EVENT)
        {
            if (shape > 0)
                newOne.Add("shape", shape.ToString());
            if (event_type > 0)
            {
                newOne.Add("event_type", event_type.ToString());
                newOne.Add("event_amount", event_amount.ToString());
            }
            if (reward_type > 0)
                newOne.Add("reward_type", reward_type.ToString());
            if (!reward_value.IsNullOrEmpty())
                newOne.Add("reward_value", reward_value);
        }

        if (appear_rating < 100)
            newOne.Add("appear_rating", appear_rating.ToString());

        if (!sailing_pass.IsNullOrEmpty())
            newOne.Add("sailing_pass", sailing_pass);

        return newOne;
    }

    public TileData Clone()
    {
        TileData newOne = new TileData();

        newOne.index = index;
        newOne.id = id;
        newOne.type = type;
        newOne.shape = shape;
        newOne.event_type = event_type;
        newOne.event_amount = event_amount;
        newOne.reward_type = reward_type;
        newOne.reward_value = reward_value;
        newOne.appear_rating = appear_rating;
        newOne.sailing_pass = sailing_pass;

        return newOne;
    }
}
