﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSimulationTool
{
    public class SimulationData : MonoBehaviour
    {
        private void Start()
        {
            InputField inputField = this.GetComponent<InputField>();
            Button button = this.GetComponent<Button>();
            if (inputField != null)
            {
                if (SimulationDataGroup.it.ContainKey(name))
                {
                    inputField.text = SimulationDataGroup.it[name];
                }
                SaveData(inputField.text);
                inputField.onEndEdit.AddListener(delegate { SaveData(inputField.text); });
            }
            else if (button != null)
            {
                button.onClick.AddListener(delegate { ExcuteCallback(); });
            }
        }

        public void ExcuteCallback()
        {
            SimulationDataGroup.it.ExcuteCallback(this.name);
        }

        public void SaveData(string data)
        {
            SimulationDataGroup.it.SetData(this.name, data);
        }
    }
}
