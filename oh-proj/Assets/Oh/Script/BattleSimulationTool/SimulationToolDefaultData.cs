﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSimulationTool
{
    public class SimulationToolDefaultData : MonoBehaviour
    {
        private static string DEFAULT_STATIC_LOAD_PATH
        {
            get
            {
#if !UNITY_EDITOR
                return Application.dataPath + "/Statics.txt";
#endif
                return string.Format("Statics/{0}", "Statics");
            }
        }

        [System.Serializable]
        public class DataGroup
        {
            public string key;
            public string value;
        }

        [System.Serializable]
        public class DebugRangeData
        {
            public string key;
            public Color  value;
        }

        [SerializeField]
        List<DataGroup> _dataList = new List<DataGroup>();

        [SerializeField]
        List<DebugRangeData> _debugList = new List<DebugRangeData>();
        
        private void Awake()
        {
            foreach (var data in _dataList)
            {
                SimulationDataGroup.it.SetData(data.key, data.value);
            }
            SimulationDataGroup.it.SetData("StaticLoadPath", DEFAULT_STATIC_LOAD_PATH);
        }

        public void ShowDebugRange(string key)
        {
            foreach (var data in _debugList)
            {
                if (data.key == key)
                {
                    if (BattleController.it.debugList.ContainsKey(key))
                    {
                        BattleController.it.debugList.Remove(key);
                    }
                    else
                    {
                        BattleController.it.debugList.Add(data.key, data.value);
                    }
                    break;
                }
            }
        }
    }
}