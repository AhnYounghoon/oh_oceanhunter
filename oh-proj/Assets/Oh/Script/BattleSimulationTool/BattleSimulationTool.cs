﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace BattleSimulationTool
{
    public class BattleSimulationTool : GameEngine
    {
        public readonly Color DISABLE_COLOR = STZCommon.ToUnityColor(137, 137, 137);
        public readonly Color ABLE_COLOR    = STZCommon.ToUnityColor(255, 255, 255);

        [SerializeField]
        GameObject toolUI;

        [SerializeField]
        SimulationFileBrowser fileBrowser;

        [SerializeField]
        SimulationNotification notification;

        private List<PlayData> _playerDataList = new List<PlayData>();  // 플레이어 데이터
        private List<PlayData> _enemyDataList  = new List<PlayData>();  // 적 데이터

        private Image _selectedMainButton;  // 선택 중인 최상위 버튼을 표시
        private SimulationPanel[] _panel;   // 세부 패널 (상위 타이틀/서브 타이틀/패널)
        private SimulationPage[] _page;     // 상위 페이지 (상위 타이틀 페이지)
        private Button[] _mainButton;       // 상위 타이틀 버튼
        private Button[] _subButton;        // 세부 타이틀 버튼 

        private bool _showToolUI;           // 툴 표시 여부
        private bool _bStaticLoad;          // Static 로드 여부
        private bool _bPlaying;             // 게임 진행중 여부
        private string _staticLoadPath;     // Static 로드 위치

        private string _currentPage;        // 현재 표시중인 페이지 Static 로드 경로

        private void Awake()
        {
            GeneralDataManager.it.currentSceneName = GeneralDataManager.ESceneName.BattleSimulationTool;
            LoadOnAwake();
        }

        private void Start()
        {
            _selectedMainButton = toolUI.transform.Find("Top/SelectBar").GetComponent<Image>();
            _mainButton = toolUI.transform.Find("Top/Button").GetComponentsInChildren<Button>();
            _page = toolUI.transform.Find("Page").GetComponentsInChildren<SimulationPage>();

            // Top Button Callback
            SimulationDataGroup.it.RegistCallback("BattleInfo", () => { SelectMainButton("BattleInfo"); });
            SimulationDataGroup.it.RegistCallback("Setting",    () => { SelectMainButton("Setting"); });

            // Battle Info Sub Button Callback
            SimulationDataGroup.it.RegistCallback("System", () => { SelectSubButton("System"); });
            SimulationDataGroup.it.RegistCallback("Range", () => { SelectSubButton("Range"); });
            SimulationDataGroup.it.RegistCallback("Character", () => { SelectSubButton("Character"); });

            // Panel Button Callback
            SimulationDataGroup.it.RegistCallback("OnSimulation", OnSimulation);
            SimulationDataGroup.it.RegistCallback("OnStop", OnStop);
            SimulationDataGroup.it.RegistCallback("OnLoadStatics", OnLoadStatics);
            SimulationDataGroup.it.RegistCallback("OnFindPath", OnFindPath);

            SelectMainButton("BattleInfo");
            SetToolUI(true);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SetToolUI(!_showToolUI);
            }
        }

        private void OnFindPath()
        {
            fileBrowser.Show();
        }

        private void OnSimulation()
        {
            if (_bPlaying)
            {
                notification.ShowNotification("이미 게임이 실행중입니다!", "Stop 후 재시작해주세요", SimulationNotification.ELevel.Error);
                return;
            }

            if (!_bStaticLoad)
            {
                notification.ShowNotification("Static 에러!", "Static 로드를 먼저해주세요", SimulationNotification.ELevel.Error);
                return;
            }

            SimulationDataGroup data = SimulationDataGroup.it;
            int startBattleDistance = data["StartBattleDistance"].IntValue();
            int limitDistance       = data["LimitDistance"].IntValue();
            int backgroundID        = data["BackgroundID"].IntValue();
            string playerID         = data["PlayerID"];
            string enemyID          = data["EnemyID"];
            float timeLimit         = data["TimeLimit"].FloatValue();
            float gameSpeed         = data["GameSpeed"].FloatValue();

            string[] splitPlayer = playerID.Split(',');
            string[] splitEnemy  = enemyID.Split(',');

            // set player
            foreach (var player in splitPlayer)
            {
                PlayData playerData = new PlayData();
                playerData.BaseOn(Statics.Instance.battle_object[player.IntValue()]);
                playerData.startPosition.x = -(startBattleDistance / 2);
                playerData.startScale.x = 1;
                playerData.tag = ETag.Player;
                _playerDataList.Add(playerData);
            }

            // set enemy
            foreach (var enemy in splitEnemy)
            {
                PlayData enemyData = new PlayData();
                enemyData.BaseOn(Statics.Instance.battle_object[enemy.IntValue()]);
                enemyData.startPosition.x = (startBattleDistance / 2);
                enemyData.startScale.x = -1;
                enemyData.tag = ETag.Enermy;
                _enemyDataList.Add(enemyData);
            }

            Time.timeScale = gameSpeed;

            _bPlaying = true;
            SetToolUI(false);
            GameInit(_playerDataList,_enemyDataList, backgroundID);
        }

        private void OnStop()
        {
            Time.timeScale = 1;

            base.Clear();
            _bPlaying = false;
        }

        private void OnLoadStatics()
        {
            _staticLoadPath = SimulationDataGroup.it["StaticLoadPath"];
            string output = string.Empty;
#if UNITY_EDITOR
            TextAsset ta = Resources.Load<TextAsset>(_staticLoadPath);
            output = ta.text;
#else
            if (System.IO.File.Exists(_staticLoadPath))
            {
                output = System.IO.File.ReadAllText(_staticLoadPath);
            }
            else
            {
                // File Not Exist
                notification.ShowNotification("Statics 로드 실패!", "해당 경로에 파일이 있는지 확인해주세요", SimulationNotification.ELevel.Error);
                return;
            }
#endif
            if (output.IsNullOrEmpty())
            {
                notification.ShowNotification("Statics 파싱에러!", "json 형식에 어긋나는 파일이에요", SimulationNotification.ELevel.Error);
                return;
            }

            var json = SimpleJSON.JSONNode.Parse(output) as SimpleJSON.JSONClass;
            Statics.Instance.Parse(json);

            _bStaticLoad = true;
            notification.ShowNotification("Statics 로드 성공!", _staticLoadPath);
        }

        /// <summary>
        /// 최상위 페이지를 선택
        /// </summary>
        /// <param name="name"></param>
        private void SelectMainButton(string name)
        {
            // 같은 페이지는 선택할 수 없다
            if (_currentPage == name)
            {
                return;
            }

            _currentPage = name;

            if (_panel != null)
            {
                foreach (var panel in _panel)
                {
                    panel.gameObject.SetActive(true);
                }
            }
     
            // top button able/ disable
            foreach (var button in _mainButton)
            {
                bool b = button.name == name;
                button.transform.Find("Text").GetComponent<Text>().color = b ? ABLE_COLOR : DISABLE_COLOR;
                if (b)
                {
                    _selectedMainButton.transform.DOLocalMoveX(button.transform.localPosition.x - 30, 0.2f);
                }
            }

            // page active
            GameObject targetPage = null;
            foreach (var page in _page)
            {
                bool b = page.name == name;
                page.gameObject.SetActive(b);
                if (b)
                {
                    targetPage = page.gameObject;
                }
            }

            if (targetPage != null)
            {
                // find sub button and panel
                var a = toolUI.transform.Find(StringHelper.Format("Page/{0}/ButtonList", name));
                var b = toolUI.transform.Find(StringHelper.Format("Page/{0}/Panel", name));
                if (a != null)
                {
                    _subButton = toolUI.transform.Find(StringHelper.Format("Page/{0}/ButtonList", name)).GetComponentsInChildren<Button>();
                }
                else
                {
                    _subButton = null;
                }

                if (b != null)
                {
                    _panel     = toolUI.transform.Find(StringHelper.Format("Page/{0}/Panel", name)).GetComponentsInChildren<SimulationPanel>();
                    foreach (var panel in _panel)
                    {
                        panel.gameObject.SetActive(false);
                    }
                }
                else
                {
                    _panel = null;
                }

                SelectSubButton(_subButton[0].name);
            }
        }

        /// <summary>
        /// 하위 패널을 선택
        /// </summary>
        /// <param name="name"></param>
        private void SelectSubButton(string name)
        {
            // sub button able/disable
            if (_subButton != null)
            {
                foreach (var button in _subButton)
                {
                    bool b = button.name == name;
                    button.transform.Find("Text").GetComponent<Text>().color = b ? ABLE_COLOR : DISABLE_COLOR;
                }
            }

            // panel able/disable
            if (_panel != null)
            {
                foreach (var panel in _panel)
                {
                    panel.gameObject.SetActive(panel.name == name);
                }
            }
        }

        /// <summary>
        /// 툴 UI 조정 
        /// </summary>
        /// <param name="value">활성화 / 비활성화</param>
        private void SetToolUI(bool value)
        {
            if (_showToolUI == value)
                return;

            _showToolUI = value;
            Vector3 StartPos = _showToolUI ? new Vector3(-640, 0, 0) : new Vector3(0, 0, 0);
            Vector3 ArrivePos = _showToolUI ? new Vector3(0, 0, 0) : new Vector3(-640, 0, 0);
            toolUI.transform.localPosition = StartPos;
            toolUI.gameObject.transform.DOLocalMove(ArrivePos, 0.4f);
        }
    }
}

