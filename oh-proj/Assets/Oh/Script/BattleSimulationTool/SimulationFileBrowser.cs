﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimulationFileBrowser : MonoBehaviour
{
    [SerializeField]
    GUISkin[] skins;

    [SerializeField]
    Texture2D file, folder, back, drive;

    [SerializeField]
    InputField pathField;

    private FileBrowser _fileBroswer = null;
    private bool drawGUI = false;

    private void Start()
    {
        InitFileBrowser();
    }

    private void InitFileBrowser()
    {
#if UNITY_EDITOR
        _fileBroswer = new FileBrowser(Application.dataPath + "/Resources/Json", 1);
#else
        _fileBroswer = new FileBrowser(1);
#endif

        //fileBroswer.guiSkin = skins[0];
        _fileBroswer.guiSkin = skins[1];

        _fileBroswer.fileTexture = file;
        _fileBroswer.directoryTexture = folder;
        _fileBroswer.backTexture = back;
        _fileBroswer.driveTexture = drive;
        //show the search bar
        _fileBroswer.showSearch = true;
        //search recursively (setting recursive search may cause a long delay)
        _fileBroswer.searchRecursively = false;
    }

    public void Show()
    {
        drawGUI = true;
    }

    void OnGUI()
    {
        if (!drawGUI)
            return;

        if (_fileBroswer.draw())
        {
            if (_fileBroswer.outputFile == null)
            {
                drawGUI = false;
            }
            else
            {
                pathField.text = _fileBroswer.outputFile.FullName;
                drawGUI = false;
            }
        }
    }
}
