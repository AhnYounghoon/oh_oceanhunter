﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BattleSimulationTool
{
    public class SimulationDataGroup
    {
        static SimulationDataGroup _instance = null;
        public static SimulationDataGroup it
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SimulationDataGroup();
                }
                return _instance;
            }
        }

        public string this[string key]
        {
            get
            {
                if (!_dataGroup.ContainsKey(key))
                {
                    Debug.LogError(StringHelper.Format("{0} not contain key", key));
                    return string.Empty;
                }
                return _dataGroup[key];
            }
        }

        Dictionary<string, System.Action> _callbackGroup = new Dictionary<string, System.Action>();
        Dictionary<string, string>        _dataGroup     = new Dictionary<string, string>();

        public void SetData(string key, string value)
        {
            if (_dataGroup.ContainsKey(key))
            {
                _dataGroup[key] = value;
                return;
            }

            _dataGroup.Add(key, value);
        }

        public void RegistCallback(string key, System.Action callback)
        {
            if (_callbackGroup.ContainsKey(key))
            {
                Debug.LogError(StringHelper.Format("{0} is already contain!", key));
                return;
            }

            _callbackGroup.Add(key, callback);
        }

        public void ExcuteCallback(string key)
        {
            if (!_callbackGroup.ContainsKey(key))
            {
                Debug.LogError(StringHelper.Format("{0} not exist!", key));
                return;
            }

            System.Action callback = _callbackGroup[key];
            if (callback != null)
            {
                callback();
            }
        }

        public bool ContainKey(string key)
        {
            if (_dataGroup.ContainsKey(key))
            {
                return true;
            }
            return false;
        }
    }
}
