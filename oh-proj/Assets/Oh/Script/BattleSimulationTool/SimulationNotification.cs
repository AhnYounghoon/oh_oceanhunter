﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

public class SimulationNotification : MonoBehaviour
{
    public readonly Color DEBUG_COLOR = STZCommon.ToUnityColor(255, 255, 255);
    public readonly Color WARNING_COLOR = STZCommon.ToUnityColor(255, 220, 0);
    public readonly Color ERROR_COLOR = STZCommon.ToUnityColor(255, 0, 0);

    public enum ELevel
    {
        Debug,
        Warning,
        Error,
    }

    private List<GameObject> notificationList = new List<GameObject>();

    public void ShowNotification(string inTitle, string inMessage = "",ELevel inLevel = ELevel.Debug)
    {
        Color targetColor = DEBUG_COLOR;
        if (inLevel == ELevel.Error)
            targetColor = ERROR_COLOR;
        else if (inLevel == ELevel.Warning)
            targetColor = WARNING_COLOR;

        int index = notificationList.Count;

        GameObject go = Instantiate(Resources.Load<GameObject>("Prefabs/SimulationNotification"));
        go.transform.Find("Title").GetComponent<Text>().text = inTitle;
        go.transform.Find("Title").GetComponent<Text>().color = targetColor;
        go.transform.Find("Message").GetComponent<Text>().text = inMessage;
        go.transform.SetParent(transform);
        go.GetComponent<Button>().onClick.AddListener(() => { RemoveNotification(go); });
        go.transform.SetScale(1);
        go.transform.SetPositionX(845);
        go.transform.SetPositionY(308 - index * 100);
        go.transform.DOLocalMoveX(442, 0.3f)
            .OnComplete(() => 
        {
        });
        notificationList.Add(go);
    }

    public void RemoveNotification(GameObject obj)
    {
        var noti = notificationList.FindIndex(x => x == obj);
        notificationList.RemoveAt(noti);
        obj.transform.DOLocalMoveX(845, 0.3f)
          .OnComplete(() =>
          {
              Destroy(obj);
          });
        SortNotification();
    }

    private void SortNotification()
    {
        int i = 0;
        foreach (var noti in notificationList)
        {
            float y = 308 - i * 100;
            if ((int)noti.transform.localPosition.y != (int)y)
            {
                noti.transform.DOLocalMoveY(y, 0.3f);
            }
            i++;
        }
    }
}
