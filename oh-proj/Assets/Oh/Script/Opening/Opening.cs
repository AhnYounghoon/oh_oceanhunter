﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Opening : MonoBehaviour
{
    [SerializeField]
    Talk talk;

    [SerializeField]
    GameObject rain;

    [SerializeField]
    GameObject up;

    [SerializeField]
    Image logo;

    [SerializeField]
    Camera cameraA;

    void Start ()
    {
        GeneralDataManager.it.LoadData();
        SingletonController.Get<SoundManager>().Load("JSON/SoundData");

        SingletonController.Get<SoundManager>().Play("BGM_opening", "bgm");
        talk.AddEventFunction("@FadeInMonster", (System.Action inEndCallback) =>
        {
            cameraA.transform.DOMove(new Vector3(0, 19, -170), 10.0f);
            rain.gameObject.SetActive(true);
            up.gameObject.SetActive(true);
            up.transform.DOMoveZ(294, 5.0f).OnComplete(() => { inEndCallback(); });

        });

        talk.AddEventFunction("@Logo", (System.Action inEndCallback) =>
        {
            logo.transform.DOScale(1, 3.0f);
            logo.DOFade(1, 3.0f).OnComplete(() => { inEndCallback(); });
        });


        talk.Parse("Data/opening_1", End);
    }

    private void End()
    {
        GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.Opening;
        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.FLOW, string.Empty, 1);
        UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.LobbyScene.ToString());
    }
}
