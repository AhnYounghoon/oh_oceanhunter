﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// UNITY에서 MonoBehaviour의 gameObject, transform, Component를 호출하기 위해서는
/// C++ 단위에서 호출해야해서 속도가 느리다.
/// 때문에 한 번 호출한 객체는 메모리에 캐싱하여 다시 부를 때 접근 속도를 빠르게 하기 위해 클래스를 제작하였다.
/// </summary>

public class STZBehaviour : MonoBehaviour, ISTZBehaviour
{
    Transform _transform;
    GameObject _gameObject;

    /* 컴포넌트를 캐싱하기 위한 클래스 */
    Dictionary<Type, Component> _componentDic = new Dictionary<Type, Component>();

    /// <summary>
    /// transform 불러오기.
    /// </summary>
    public new Transform transform
    {
        get
        {
            if (_transform == null)
                _transform = base.transform;

            return _transform;
        }
    }

    /// <summary>
    /// gameObject 불러오기.
    /// </summary>
    public new GameObject gameObject
    {
        get
        {
            if (_gameObject == null)
                _gameObject = base.gameObject;

            return _gameObject;
        }
    }

    /// <summary>
    /// string을 이용해 Component 가져오기.
    /// </summary>
    /// <param name="typeName">컴포넌트 타입 이름.</param>
    /// <returns></returns>
    public new Component GetComponent(string typeName)
    {
        System.Type type = System.Type.GetType(typeName);
        return GetComponent(type);
    }

    /// <summary>
    /// type를 이용해 Component 가져오기.
    /// </summary>
    /// <param name="type">컴포넌트 타입.</param>
    /// <returns></returns>
    public new Component GetComponent(System.Type type)
    {
        SetComponent(type);
        return _componentDic[type];
    }

    /// <summary>
    /// Generic을 활용한 Component 가져오기.
    /// </summary>
    /// <typeparam name="T">Component 종류</typeparam>
    /// <returns></returns>
    public new T GetComponent<T>() where T : Component
    {
        Component c = GetComponent(typeof(T));
        if (c != null)
            return (T)c;

        return null;
    }

    /// <summary>
    /// Generic을 활용한 Component 추가.
    /// </summary>
    /// <typeparam name="T">추가될 Component 종류.</typeparam>
    /// <returns>추가된 Component</returns>
    public T AddComponent<T>() where T : Component
    {
        System.Type type = typeof(T);
        if (_componentDic.ContainsKey(type) && _componentDic[type] != null)
        {
            return (T)_componentDic[type];
        }

        Component component = gameObject.AddComponent<T>();
        if (component != null)
        {
            SetComponent(type);
            return (T)component;
        }

        return null;
    }

    /// <summary>
    /// Generic을 활용한 Component 제거.
    /// </summary>
    /// <typeparam name="T">제거될 Component 종류.</typeparam>
    public void RemoveComponent<T>() where T : Component
    {
        System.Type type = typeof(T);
        Destroy(GetComponent(type));
        _componentDic.Remove(type);
    }

    /// <summary>
    /// Component 캐싱을 위한 함수.
    /// </summary>
    /// <param name="type">캐싱할 Component 타입.</param>

    private void SetComponent(Type type)
    {
        if (!_componentDic.ContainsKey(type))
        {
            _componentDic.Add(type, base.GetComponent(type));
        }
        else if (_componentDic[type] == null)
        {
            _componentDic[type] = base.GetComponent(type);
        }
    }
}

public interface ISTZBehaviour
{
    T GetComponent<T>() where T : Component;
}