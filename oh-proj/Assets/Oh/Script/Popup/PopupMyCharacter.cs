﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[Popup(prefab_path = "Prefabs/Popup/PopupMyCharacter")]
public class PopupMyCharacter : CustomPopup, IHashReceiver
{
    [SerializeField]
    CharacterTableView tableView;

    [SerializeField]
    GameObject emptyView;

    [SerializeField]
    GameObject notifiyOpenBoxArea;

    [SerializeField]
    Toggle[] btnToggle;

    [SerializeField]
    Button btnGotoOpen;

    [SerializeField]
    Text txtComment;

    private bool isHero;
    int currGoldBoxCount;
    int currSilverBoxCount;
    int _toggleIndex = 1;

    protected override void PrevOpen()
    {
        ObserverManager.it.AddEventListener(this, EObserverEventKey.CHAR_UPGRADE);
        MainWealthUI.it.Show(true);
        currGoldBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.GOLD_BOX);
        currSilverBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.SILVER_BOX);
        isHero = true;

        if (_param.ContainsKey("Type"))
        {
            isHero = (int)_param["Type"] == (int)ServerStatic.Character.ECatetory.HERO;
            SetComponet();
        }
        else
        {
            if (currGoldBoxCount > 0)
                isHero = true;
            else if (currSilverBoxCount > 0)
                isHero = false;

            SetComponet();
        }

        btnToggle[0].isOn = isHero;
        btnToggle[1].isOn = !isHero;
    }

    protected override void PrevClose()
    {
        ObserverManager.it.RemoveEventListener(this, EObserverEventKey.CHAR_UPGRADE);
        MainWealthUI.it.Rewind();
    }

    public void ReceiveEvent(EObserverEventKey inEventKey, Hashtable inParam)
    {
        if (inEventKey == EObserverEventKey.CHAR_UPGRADE)
        {
            SetComponet();
        }
    }

    void SetComponet()
    {
        List<int> heroList = GeneralDataManager.it.GetHeroList();
        List<int> sailorList = GeneralDataManager.it.GetSailorList();
        List<ServerStatic.Character> myList = null;

        currGoldBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.GOLD_BOX);
        currSilverBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.SILVER_BOX);

        if (isHero)
            myList = Statics.Instance.character.Values.Where(x => heroList.Contains(x.id)).OrderBy(x => x.category).ToList();
        else
        {
            if (sailorList != null)
                myList = Statics.Instance.character.Values.Where(x => sailorList.Contains(x.id)).OrderBy(x => x.category).ToList();
        }

        if (myList != null)
        {
            tableView.gameObject.SetActive(true);
            tableView.Init(myList);
            emptyView.SetActive(false);
        }
        else
        {
            tableView.gameObject.SetActive(false);
            emptyView.SetActive(true);
        }

        notifiyOpenBoxArea.SetActive(false);
        if (currGoldBoxCount > 0 || currSilverBoxCount > 0)
        {
            notifiyOpenBoxArea.SetActive(true);
            txtComment.text = StringHelper.Format("열수 있는 {0}상자가 {1}개 있습니다.\n열어보시겠습니까?", isHero ? "황금" : "실버", isHero ? currGoldBoxCount: currSilverBoxCount);
            if (txtComment.text.Contains("0개"))
                txtComment.text = "상자를 얻어 캐릭터를 얻으세요!";
        }
        else
            txtComment.text = "상자를 얻어 캐릭터를 얻으세요!";
    }

    public void OnToggle(int inIndex)
    {
        if (_toggleIndex == inIndex)
            return;

        _toggleIndex = inIndex;
        isHero = inIndex == (int)ServerStatic.Character.ECatetory.HERO;
        SetComponet();
    }

    public void OnOpenBox()
    {
        if (_toggleIndex == (int)ServerStatic.Character.ECatetory.HERO && currGoldBoxCount <= 0)
            return;

        if (_toggleIndex == (int)ServerStatic.Character.ECatetory.SAILOR && currSilverBoxCount <= 0)
            return;

        PopupMagicBox.Show(isHero ? (int)ServerStatic.Character.ECatetory.HERO : (int)ServerStatic.Character.ECatetory.SAILOR, (param) => { SetComponet(); });
    }

    public static void Show(Callback inCallback)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupMyCharacter), inCallback);
    }
}
