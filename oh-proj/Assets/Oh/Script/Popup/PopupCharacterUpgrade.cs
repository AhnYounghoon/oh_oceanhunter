﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

[Popup(prefab_path = "Prefabs/Popup/PopupCharacterUpgrade")]
public class PopupCharacterUpgrade : BasePopUp
{
    [SerializeField]
    Text[] txtCurrStats;

    [SerializeField]
    Text[] txtAddStats;

    [SerializeField]
    CharacterTableViewCell charInfo;

    [SerializeField]
    Button BtnUpgrade;
    [SerializeField]
    Button BtnConfirm;

    ServerStatic.Character data = null;
    bool isUpgrade = false;

    protected override void PrevOpen()
    {
        data = (ServerStatic.Character)_param["data"];
        isUpgrade = (bool)_param["isUpgrade"];

        charInfo.Reset(data);
        ShowCharacterStats();
    }

    /// <summary>
    /// 레벨에 증가분을 더해서 보여줌
    /// </summary>
    void ShowCharacterStats()
    {
        CharacterLevelInfo personLevelInfo = GeneralDataManager.it.GetPersonLevelInfo(data.category, data.id);
        int currLevel = personLevelInfo.level;

        txtCurrStats[0].text = (data.hp + Mathf.RoundToInt(data.hp * ((currLevel - 1) * 0.1f))).ToString();
        txtCurrStats[1].text = (data.damage + Mathf.RoundToInt(data.damage * ((currLevel - 1) * 0.1f))).ToString();
        txtCurrStats[2].text = (data.defense + Mathf.RoundToInt(data.defense * ((currLevel - 1) * 0.1f))).ToString();

        txtAddStats[0].gameObject.SetActive(isUpgrade);
        txtAddStats[1].gameObject.SetActive(isUpgrade);
        txtAddStats[2].gameObject.SetActive(isUpgrade);

        if (isUpgrade)
        {
            txtAddStats[0].text = StringHelper.Format("+{0}", Mathf.RoundToInt(data.hp * (currLevel * 0.1f)));
            txtAddStats[1].text = StringHelper.Format("+{0}", Mathf.RoundToInt(data.damage * (currLevel * 0.1f)));
            txtAddStats[2].text = StringHelper.Format("+{0}", Mathf.RoundToInt(data.defense * (currLevel * 0.1f)));
        }

        BtnUpgrade.gameObject.SetActive(isUpgrade);
        BtnConfirm.gameObject.SetActive(!isUpgrade);
    }

    public void OnUpgrade()
    {
        CharacterLevelInfo personLevelInfo = GeneralDataManager.it.GetPersonLevelInfo(data.category, data.id);
        personLevelInfo.exp -= personLevelInfo.level * 10;
        personLevelInfo.level++;

        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, data.category == (int)ServerStatic.Character.ECatetory.HERO ? GeneralDataManager.EParam.HERO.ToString() : GeneralDataManager.EParam.SAILOR.ToString(), 1, data.id, null, personLevelInfo);
        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.SILVER.ToString(), -200);

        isUpgrade = false;
        charInfo.Reset(data);
        ShowCharacterStats();

        ObserverManager.it.Dispatch(EObserverEventKey.CHAR_UPGRADE, null);
    }

    public void OnConfirm()
    {
        Exit();
    }

    public static void Show(Callback inCallback, ServerStatic.Character inData, bool isUpgrade = false)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupCharacterUpgrade), inCallback, STZCommon.Hash("data", inData, "isUpgrade", isUpgrade));
    }
}
