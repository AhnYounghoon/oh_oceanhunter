﻿using UnityEngine;

public class ShipPopupAnimation : MonoBehaviour
{
    private enum EShowMode
    {
        All,
        Weapon,
        Ship,
    }

    [SerializeField]
    CustomAnimation _ship;

    [Header("Mode All")]
    [SerializeField]
    CustomAnimation _floorPlanPanel;

    [SerializeField]
    CustomAnimation[] _weaponPanel;

    [Header("Mode Weapon")]
    [SerializeField]
    CustomAnimation _weaponInfoPanel;

    [SerializeField]
    CustomAnimation _weaponListPanel;

    [Header("Mode Ship")]
    [SerializeField]
    CustomAnimation _shipInfoPanel;

    [SerializeField]
    CustomAnimation _shipListPanel;

    EShowMode _currentMode = EShowMode.All;

    private void ChangeShowMode(EShowMode inMode)
    {
        if (_currentMode == inMode)
        {
            return;
        }

        _currentMode = inMode;
        switch (_currentMode)
        {
            case EShowMode.All:
                ShowAll();
                break;
            case EShowMode.Weapon:
                ShowWeapon();
                break;
            case EShowMode.Ship:
                ShowShip();
                break;
            default:
                break;
        }
    }

    public void ChangeAllMove()
    {
        ChangeShowMode(EShowMode.All);
    }

    public void ChangeWeaponMode()
    {
        ChangeShowMode(EShowMode.Weapon);
    }

    public void ChangeShipMode()
    {
        ChangeShowMode(EShowMode.Ship);
    }

    private void ShowAll()
    {
        _floorPlanPanel.Play("Open");
        for (int i = 0; i < _weaponPanel.Length; i++)
        {
            _weaponPanel[i].Play("Open");
        }

        _weaponInfoPanel.Play("Close");
        _weaponListPanel.Play("Close");
        _shipInfoPanel.Play("Close");
        _shipListPanel.Play("Close");

        _ship.Play("Open");
    }

    private void ShowWeapon()
    {
        _floorPlanPanel.Play("Close");
        for (int i = 0; i < _weaponPanel.Length; i++)
        {
            _weaponPanel[i].Play("Close");
        }

        _weaponInfoPanel.Play("Show");
        _weaponListPanel.Play("Show");

        _ship.Play("Weapon", false);
    }

    private void ShowShip()
    {
        _floorPlanPanel.Play("Close");
        for (int i = 0; i < _weaponPanel.Length; i++)
        {
            _weaponPanel[i].Play("Close");
        }

        _shipInfoPanel.Play("Show");
        _shipListPanel.Play("Show");

        _ship.Play("Weapon", false);
    }
}
