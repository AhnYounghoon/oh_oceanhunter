﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniLinq;

[Popup(prefab_path = "Prefabs/Popup/PopupShip")]
public class PopupShip : CustomPopup
{
    public enum EParam
    {
        C_FLEET_DATA
    }

    [Tooltip("배 정보")]
    [SerializeField]
    FleetUI _shipUI;

    [Tooltip("무기 패널")]
    [SerializeField]
    WeaponPanelUI _weaponPanelUI;

    [Tooltip("스킨 착용 UI")]
    [SerializeField]
    SkinUI _skinUI;

    [Tooltip("무기 착용 UI")]
    [SerializeField]
    WeaponUI[] _weaponUI;

    FleetData _data;

    protected override void PrevOpen()
    {
        _data = _param.ContainsKey(EParam.C_FLEET_DATA) ? (FleetData)_param[EParam.C_FLEET_DATA] : null;
        //_shipUI.Refresh(_data, FleetUI.EShowMode.ShowWeapon);

        RefreshWeaponUI();
        RefreshSkinUI();
    }

    private void RefreshSkinUI()
    {
        _skinUI.Refresh(_data.shipSkin);
    }

    private void RefreshWeaponUI()
    {
        //for (int i = 0; i < _weaponUI.Length; i++)
        //{
        //    ServerStatic.Weapon weapon = _data.weaponList.ElementAtOrDefault(i);
        //    _weaponUI[i].Refresh(weapon);
        //}
    }

    public void OnMain()
    {
        //_shipUI.FocusTarget(BattleEnum.ETarget.Player);
        RefreshWeaponUI();
        RefreshSkinUI();
    }

    public void OnSelectWeapon(int inLocationIdx)
    {
        //_shipUI.FocusTarget(SkillEnum.ETarget.Player_Weapon, inLocationIdx);
        _weaponPanelUI.Refresh(_data, _shipUI,(ELocation)inLocationIdx);
    }
}
