﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Popup(prefab_path = "Prefabs/Popup/PopupViewCharacter")]
public class PopupViewCharacter : CustomPopup
{
    public enum EParam
    {
        EHero,
        ESailor,
    }

    [Header(" ==== Character ==== ")]
    [SerializeField] Image _imgCharacter;
    [SerializeField] Text  _txtName;

    [Header(" ==== Info ==== ")]
    [SerializeField] Text  _txtHp;
    [SerializeField] Text  _txtDamage;
    [SerializeField] Text  _txtDefense;
    [SerializeField] Text  _txtAttackRange;
    [SerializeField] Text  _txtAttackSpeed;
    [SerializeField] Text  _txtMoveSpeed;
    [SerializeField] Text  _txtLeaderShip;
    [SerializeField] Image _imgLeaderShip;

    [Header(" ==== Info Plus ==== ")]
    [SerializeField] Text _txtHpPlus;
    [SerializeField] Text _txtDamagePlus;
    [SerializeField] Text _txtDefensePlus;
    [SerializeField] Text _txtAttackRangePlus;
    [SerializeField] Text _txtAttackSpeedPlus;
    [SerializeField] Text _txtMoveSpeedPlus;

    [Header(" ==== Sailor ==== ")]
    [SerializeField] FleetCharacterUI _characterUI;
    [SerializeField] Text _txtSailorHp;
    [SerializeField] Text _txtSailorDamage;
    [SerializeField] Text _txtSailorDefense;

    private ServerStatic.Character _heroData;
    private ServerStatic.Character _sailorData;

    protected override void PrevOpen()
    {
        MainWealthUI.it.Show(true);

        _heroData   = _param.ContainsKey(EParam.EHero)   ? _param[EParam.EHero]   as ServerStatic.Character : null;
        _sailorData = _param.ContainsKey(EParam.ESailor) ? _param[EParam.ESailor] as ServerStatic.Character : null;

        /// Set Character
        _txtName.text = _heroData.name;
        _imgCharacter.sprite = Resources.Load<Sprite>(_heroData.illust_path);

        /// Set Info
        _txtHp.text      = _heroData.hp.ToString();
        _txtDamage.text  = _heroData.damage.ToString();
        _txtDefense.text = _heroData.defense.ToString();
        _txtAttackRange.text = _heroData.attack_range.ToString();
        _txtAttackSpeed.text = "10"; // 이건 수치가 없네 띠용
        _txtMoveSpeed.text = _heroData.speed.ToString();
        _txtLeaderShip.text = "500 / 1000"; // Static 갱신해야함
        _imgLeaderShip.transform.SetScaleX(0.5f); // 실제 수치로 변경해야함

        /// Set Plus
        bool sailorExist = _sailorData != null;
        if (sailorExist)
        {
            _characterUI.Reset(_sailorData);
            _txtHpPlus.text = StringHelper.Format("+{0}", _sailorData.hp);
            _txtDamagePlus.text = StringHelper.Format("+{0}", _sailorData.damage);
            _txtDefensePlus.text = StringHelper.Format("+{0}", _sailorData.defense);
            _txtAttackRangePlus.text = StringHelper.Format("+{0}", _sailorData.attack_range);
            _txtAttackSpeedPlus.text = StringHelper.Format("+{0}", 10); // 수치 없음
            _txtMoveSpeedPlus.text = StringHelper.Format("+{0}", _sailorData.speed);
            _txtSailorHp.text = StringHelper.Format("+{0}", _sailorData.hp);
            _txtSailorDamage.text = StringHelper.Format("+{0}", _sailorData.damage);
            _txtSailorDefense.text = StringHelper.Format("+{0}", _sailorData.defense);
        }

        _characterUI.gameObject.SetActive(sailorExist);
        _txtHpPlus.gameObject.SetActive(sailorExist);
        _txtDamagePlus.gameObject.SetActive(sailorExist);
        _txtDefensePlus.gameObject.SetActive(sailorExist);
        _txtAttackRangePlus.gameObject.SetActive(sailorExist);
        _txtAttackSpeedPlus.gameObject.SetActive(sailorExist);
        _txtMoveSpeedPlus.gameObject.SetActive(sailorExist);
        _txtSailorHp.gameObject.SetActive(sailorExist);
        _txtSailorDamage.gameObject.SetActive(sailorExist);
        _txtSailorDefense.gameObject.SetActive(sailorExist);
    }

    protected override void PrevClose()
    {
        MainWealthUI.it.Rewind();
    }
}
