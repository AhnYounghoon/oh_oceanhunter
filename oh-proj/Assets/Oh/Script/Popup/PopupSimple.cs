﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[Popup(prefab_path = "Prefabs/Popup/PopupSimple")]
public class PopupSimple : BasePopUp
{
    public enum EButtonType
    {
        OK,
        OK_Exit,
        Yes_No,
        Only_OK
    }

    public void OnYes()
    {
        if (!_param.ContainsKey("result"))
            _param.Add("result", true);
        Exit();
    }

    public void OnNo()
    {
        if (!_param.ContainsKey("result"))
            _param.Add("result", false);
        Exit();
    }

    protected override void PrevOpen()
    {
        base.PrevOpen();

        GameObject BtnYes = transform.Find("Background/BtnYes").gameObject;
        GameObject BtnNo = transform.Find("Background/BtnNo").gameObject;
        GameObject BtnOK = transform.Find("Background/BtnOk").gameObject;
        GameObject BtnExit = transform.Find("Background/BtnExit").gameObject;
        GameObject TitleGO = transform.Find("Background/Title").gameObject;

        Text btnYesText = BtnYes.transform.Find("Text").GetComponent<Text>();
        Text btnNoText = BtnNo.transform.Find("Text").GetComponent<Text>();
        Text titleText = TitleGO.transform.Find("Text").GetComponent<Text>();

        if (_param.ContainsKey("text"))
        {
            this.Get<Text>("Background/Text").text = (string)_param["text"];
        }

        // 버튼 종류에 따른 가시화 정렬 
        BtnYes.SetActive(false);
        BtnNo.SetActive(false);
        BtnOK.SetActive(false);

        EButtonType btnType = (EButtonType)_param["buttonType"];
        switch (btnType)
        {
            case EButtonType.OK:
                BtnOK.SetActive(true);
                break;

            case EButtonType.OK_Exit:
                BtnOK.SetActive(true);
                break;

            case EButtonType.Yes_No:
                BtnYes.SetActive(true);
                BtnNo.SetActive(true);
                break;

            case EButtonType.Only_OK:
                BtnOK.SetActive(true);
                BtnExit.SetActive(false);
                break;
        }

        // init title 
        {
            const string titleKey = "titleText";
            if (_param.ContainsKey(titleKey) && _param[titleKey] != null)
            {
                TitleGO.SetActive(true);
                titleText.text = (string)_param[titleKey];
            }
            else
            {
                TitleGO.SetActive(false);
            }
        }

        // set button text 
        {
            const string yesBtnKey = "yesButtonText";
            string rYesText = string.Empty;

            // 예 
            if (_param.ContainsKey(yesBtnKey) && _param[yesBtnKey] != null)
            {
                rYesText = (string)_param[yesBtnKey];
            }
            else
            {
                rYesText = "BT_CONFIRM".Locale();
            }
            btnYesText.text = rYesText;

            const string noBtnKey = "noButtonText";
            string rNoText = string.Empty;

            // 아니오 
            if (_param.ContainsKey(noBtnKey) && _param[noBtnKey] != null)
            {
                rNoText = (string)_param[noBtnKey];
            }
            else
            {
                rNoText = "BT_DECLINE".Locale();
            }
            btnNoText.text = rNoText;
        }
    }

    public override void OnAndroidBackbutton()
    {
        EButtonType btnType = (EButtonType)_param["buttonType"];

        const string useBackButtonKey = "useBackButton";
        if (_param.ContainsKey(useBackButtonKey) && (bool)_param[useBackButtonKey] != true)
        {
            btnType = EButtonType.Yes_No;
        }
        switch (btnType)
        {
            case EButtonType.OK:
            case EButtonType.OK_Exit:
            case EButtonType.Only_OK:
                if (!_param.ContainsKey("result"))
                    _param.Add("result", true);
                break;

            case EButtonType.Yes_No:
                if (!_param.ContainsKey("result"))
                    _param.Add("result", false);
                break;
        }

        base.OnAndroidBackbutton();
    }
}
