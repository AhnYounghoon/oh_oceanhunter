﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[Popup(prefab_path = "Prefabs/Popup/PopupMagicBox")]
public class PopupMagicBox : BasePopUp
{
    [SerializeField]
    MagicBox treasureBox;

    [SerializeField]
    Text txtDesc;

    [SerializeField]
    GameObject btnOK;

    protected override void PrevOpen()
    {
        base.PrevOpen();

        int goldBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.GOLD_BOX);
        int silverBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.SILVER_BOX);
        int gachaType = (int)_param["gachaType"];
        if (goldBoxCount + silverBoxCount <= 0)
        {
            txtDesc.text = StringHelper.Format("가지고 있는 상자가 없습니다.\n전투를 해서 상자를 얻으세요.");
            treasureBox.gameObject.SetActive(false);
            btnOK.SetActive(true);
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                treasureBox.Get<GameObject>(StringHelper.Format("Box/Box{0}", i)).SetActive(false);
            }

            treasureBox.Get<GameObject>(StringHelper.Format("Box/Box{0}", gachaType - 1)).SetActive(true);
            treasureBox.gameObject.SetActive(true);

            txtDesc.text = StringHelper.Format("상자를 열어보세요!!");
        }
    }

    protected override void PostOpen()
    {
        bool isHero = false;
        int rnd = Random.Range(0, 100) + 1;
        int gachaType = (int)_param["gachaType"];
        isHero = gachaType == (int)ServerStatic.Character.ECatetory.HERO;
        List<ServerStatic.Character> gachaList = null;
        int gachaResult = 1;

        if (isHero)
        {
            int alreadyGachaCount = GeneralDataManager.it.GetGachaCount(GeneralDataManager.EParam.GOLD_GACHA_COUNT);
            switch (alreadyGachaCount)
            {
                case 0:
                    gachaResult = 17;
                    break;
                case 1:
                    gachaResult = 18;
                    break;
                case 2:
                    gachaResult = 19;
                    break;
                default:
                    gachaResult = Random.Range(14, 20);  // 14 ~ 19
                    break;
            }
        }
        else
        {
            int clearedStageId = GeneralDataManager.it.GetStageClearData();
            switch (clearedStageId)
            {
                case 0:
                    gachaResult = Random.Range(20, 25);  // 20 ~ 24
                    break;
                case 1:
                    gachaResult = Random.Range(20, 28);  // 20 ~ 27
                    break;
                case 2:
                    gachaResult = Random.Range(20, 31);  // 20 ~ 30
                    break;
                default:
                    gachaResult = Random.Range(20, 34);  // 20 ~ 33
                    break;
            }
        }

        gachaList = ServerStatic.Character.GetListByCategory(isHero ? (int)ServerStatic.Character.ECatetory.HERO : (int)ServerStatic.Character.ECatetory.SAILOR);
        ServerStatic.Character selectedChar = /*gachaList[gachaResult]*/ServerStatic.Character.GetById(gachaResult);

        string item_id = string.Empty;
        RewardData rd = new RewardData(StringHelper.Format("{0}:{1}:1", (int)ERewardType.CHARACTER, selectedChar.id));
        treasureBox.SetReward(rd, () =>
        {
            GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, isHero ? ERewardType.GOLD_BOX.ToString() : ERewardType.SILVER_BOX.ToString(), -1);

            CharacterLevelInfo ownCharacter = GeneralDataManager.it.GetPersonLevelInfo(isHero ? (int)ServerStatic.Character.ECatetory.HERO : (int)ServerStatic.Character.ECatetory.SAILOR, selectedChar.id);

            if (ownCharacter == null)
            {
                // 신규
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, isHero ? GeneralDataManager.EParam.HERO.ToString() : GeneralDataManager.EParam.SAILOR.ToString(), 1, selectedChar.id, null, new CharacterLevelInfo(1, 0));
            }
            else
            {
                // 중복
                ownCharacter.exp += 20;
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, isHero ? GeneralDataManager.EParam.HERO.ToString() : GeneralDataManager.EParam.SAILOR.ToString(), 1, selectedChar.id, null, ownCharacter);
            }

            GeneralDataManager.it.UpdateData(isHero ? GeneralDataManager.EParam.GOLD_GACHA_COUNT : GeneralDataManager.EParam.SILVER_GACHA_COUNT, null);
            btnOK.SetActive(true);
        });

        treasureBox.Open(isHero);
    }

    public override void OnAndroidBackbutton()
    {
        // 백 버튼의 사용을 막음
    }

    public static void Show(int inType, Callback inCallback)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupMagicBox), inCallback, STZCommon.Hash("gachaType", inType));
    }
}
