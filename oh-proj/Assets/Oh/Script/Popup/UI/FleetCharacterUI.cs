﻿using ServerStatic;
using UnityEngine;
using UnityEngine.UI;

public class FleetCharacterUI : CharacterTableViewCell
{
    [SerializeField]
    GameObject _cellImage;

    [SerializeField]
    Button _plusButton;

    [SerializeField]
    GameObject _imgActive;

    [SerializeField]
    GameObject _buttonList;

    public System.Action<Character> inSelectCallback { get; set; }
    public System.Action<Character> inAddCallback    { get; set; }
    public System.Action<Character> inInfoCallback   { get; set; }

    public bool addMode
    {
        get
        {
            return _imgActive.activeSelf;
        }
    }

    public int id
    {
        get
        {
            if (_data == null)
            {
                return 0;
            }
            return _data.id;
        }
    }

    public Character Data
    {
        private set
        {
            if (_buttonList != null)
            {
                _buttonList.gameObject.SetActive(false);
            }
            _data = value;
            _imgActive.gameObject.SetActive(false);
            _plusButton.gameObject.SetActive(value == null);
            _cellImage.gameObject.SetActive(!(value == null));
            SetButtonGroup();
        }
        get
        {
            return _data;
        }
    }

    public override void Reset(Character inData, DragObject inDrag = null, int inHaveCount = 0)
    {
        base.Reset(inData, inDrag, inHaveCount);
        Data = inData;
    }

    private void SetButtonGroup()
    {
        if (_data != null)
        {
            if (_data.category == 1)
            {
                SetHeroButtonGroup();
            }
            else if (_data.category == 2)
            {
                SetSailorButtonGroup();
            }
        }
    }

    private void SetSailorButtonGroup()
    {
        if (_buttonList == null)
            return;

        Transform btn_1 = _buttonList.transform.Find("Btn_1");
        Transform btn_2 = _buttonList.transform.Find("Btn_2");
        Transform btn_3 = _buttonList.transform.Find("Btn_3");

        if (btn_1 != null && btn_2 != null && btn_3 != null)
        {
            btn_1.gameObject.SetActive(true);
            btn_2.gameObject.SetActive(true);
            btn_3.gameObject.SetActive(true);

            Text txt1 = btn_1.Find("Text").GetComponent<Text>();
            Text txt2 = btn_2.Find("Text").GetComponent<Text>();
            Text txt3 = btn_3.Find("Text").GetComponent<Text>();

            txt1.text = "강화 1000G";
            txt2.text = "배치 해제";
            txt3.text = "선원 정보";

            btn_1.GetComponent<Button>().onClick.RemoveAllListeners();
            btn_2.GetComponent<Button>().onClick.RemoveAllListeners();
            btn_3.GetComponent<Button>().onClick.RemoveAllListeners();

            btn_1.GetComponent<Button>().onClick.AddListener(OnUpgrade);
            btn_2.GetComponent<Button>().onClick.AddListener(OnRemove);
            btn_3.GetComponent<Button>().onClick.AddListener(OnInfo);
        }
        else if (btn_1 != null && btn_2 != null)
        {
            btn_1.gameObject.SetActive(true);
            btn_2.gameObject.SetActive(true);

            Text txt1 = btn_1.Find("Text").GetComponent<Text>();
            Text txt2 = btn_2.Find("Text").GetComponent<Text>();

            txt1.text = "강화 1000G";
            txt2.text = "선원 정보";

            btn_1.GetComponent<Button>().onClick.RemoveAllListeners();
            btn_2.GetComponent<Button>().onClick.RemoveAllListeners();

            btn_1.GetComponent<Button>().onClick.AddListener(OnUpgrade);
            btn_2.GetComponent<Button>().onClick.AddListener(OnInfo);
        }
    }

    private void SetHeroButtonGroup()
    {
        if (_buttonList == null)
            return;

        Transform btn_1 = _buttonList.transform.Find("Btn_1");
        Transform btn_2 = _buttonList.transform.Find("Btn_2");

        if (btn_1 != null && btn_2 != null)
        {
            btn_1.gameObject.SetActive(true);
            btn_2.gameObject.SetActive(true);

            Text txt1 = btn_1.Find("Text").GetComponent<Text>();
            Text txt2 = btn_2.Find("Text").GetComponent<Text>();

            txt1.text = "배치 해제";
            txt2.text = "선장 관리";

            btn_1.GetComponent<Button>().onClick.RemoveAllListeners();
            btn_2.GetComponent<Button>().onClick.RemoveAllListeners();

            btn_1.GetComponent<Button>().onClick.AddListener(OnRemove);
            btn_2.GetComponent<Button>().onClick.AddListener(OnInfo);
        }
        else if (btn_1 != null)
        {
            btn_1.gameObject.SetActive(true);

            Text txt1 = btn_1.Find("Text").GetComponent<Text>();
            txt1.text = "선장 관리";

            btn_1.GetComponent<Button>().onClick.RemoveAllListeners();
            btn_1.GetComponent<Button>().onClick.AddListener(OnInfo);
        }
    }

    public void OnSelect()
    {
        OnPointerDown(null);

        if (_buttonList == null)
            return;

        _buttonList.gameObject.SetActive(!_buttonList.activeSelf);
        if (_buttonList.activeSelf)
        {
            if (inSelectCallback != null)
            {
                inSelectCallback(Data);
            }
        }
    }

    public void OnUpgrade()
    {

    }

    public void OnAdd()
    {
        _imgActive.gameObject.SetActive(!_imgActive.activeSelf);
        if (_imgActive.activeSelf)
        {
            if (inAddCallback != null)
            {
                inAddCallback(Data);
            }
        }
    }

    public void OnRemove()
    {
        Data = null;
    }

    public void OnInfo()
    {
        if (_buttonList != null)
        {
            _buttonList.gameObject.SetActive(false);
        }

        if (inInfoCallback != null)
        {
            inInfoCallback(Data);
        }
        else
        {
            PopupCharacterUpgrade.Show(null, _data);
        }
    }
}