﻿using UniLinq;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPanelUI : MonoBehaviour
{
    [Tooltip("무기 리스트")]
    [SerializeField]
    WeaponTableView _weaponTableView;

    [Tooltip("타이틀")]
    [SerializeField]
    Text _txtTitle;

    [Tooltip("무기 이름")]
    [SerializeField]
    Text _txtWeapon;

    [Tooltip("무기 정보 창")]
    [SerializeField]
    CompareAbilityUI _compareAbilityUI;

   // ServerStatic.Weapon _equipmentWeapon;
   // ServerStatic.Weapon _selectedWeapon;

    ELocation _location;
    FleetData _data;
    FleetUI   _fleetUI;

    public void Refresh(FleetData inData, FleetUI inFleetUI, ELocation inLocation)
    {
        _data    = inData;
        _fleetUI = inFleetUI;
        _location = inLocation;

        //_weaponTableView.Init(Statics.Instance.weapon.Values.ToList());
        _weaponTableView.SetCellsBySortingType(SortController.EActionType.None, SortController.ESortType.None);
        //_weaponTableView.SelectCallback = OnSelect;

        /// 착용 중인 무기 셋팅
        //_equipmentWeapon = _data.weaponList.ElementAtOrDefault((int)inLocation);
        //_txtTitle.text = inLocation.ToString() + "\nLocation";
        //_compareAbilityUI.Compare(_equipmentWeapon, _equipmentWeapon);
        //if (_equipmentWeapon != null)
        //{
        //    _txtWeapon.text = _equipmentWeapon.name;
        //    _weaponTableView.equipmentWeaponId = _equipmentWeapon.id;
        //}
        //else
        //{
        //    _txtWeapon.text = "None";
        //}
    }

    //private void OnSelect(ServerStatic.Weapon inWeapon)
    //{
    //    _selectedWeapon = inWeapon;
    //    _txtWeapon.text = _selectedWeapon.name;
    //    //_compareAbilityUI.Compare(_equipmentWeapon, _selectedWeapon);
    //}

    public void OnSort(int inSortIdx)
    {

    }

    public void OnShowMode(int inModeIdx)
    {

    }

    //public void OnInfo(ServerStatic.Weapon inWeapon)
    //{

    //}

    public void OnEquipment()
    {
        //_data.ChangeWeapon(_selectedWeapon, (int)_location);
        //_fleetUI.RefreshWeapon();
        //_weaponTableView.equipmentWeaponId = _selectedWeapon.id;
        //_compareAbilityUI.Compare(_equipmentWeapon, _selectedWeapon);
    }
}
