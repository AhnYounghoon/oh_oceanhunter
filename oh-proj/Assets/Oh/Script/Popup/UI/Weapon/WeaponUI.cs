﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WeaponUI : MonoBehaviour
{
    [Tooltip("무기 이름")]
    [SerializeField]
    Text _txtName;

    [Tooltip("무기 데미지")]
    [SerializeField]
    Text _txtDamage;

    [Tooltip("무기 이미지")]
    [SerializeField]
    Image _imgWeapon;

    //ServerStatic.Weapon _data;

    //public void Refresh(ServerStatic.Weapon inData)
    //{
    //    _data = inData;

    //    if (_data == null)
    //    {
    //        _txtName.text     = "EMPTY";
    //        if (_txtDamage != null)
    //        {
    //            _txtDamage.text   = "Damage 0";
    //        }
    //    }
    //    else
    //    {
    //        _txtName.text     = _data.name;

    //        if (_imgWeapon != null)
    //        {
    //            _imgWeapon.sprite = Resources.Load<Sprite>(_data.resourcePath);
    //            _imgWeapon.color = STZCommon.ToUnityColor(255, 255, 255);
    //            _imgWeapon.gameObject.SetActive(true);
    //        }

    //        if (_txtDamage != null)
    //        {
    //            _txtDamage.text   = StringHelper.Format("Damage {0}", _data.damage);
    //        }
    //    }
    //}
}
