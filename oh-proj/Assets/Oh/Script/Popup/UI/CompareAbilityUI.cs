﻿using UnityEngine;
using UnityEngine.UI;

public class CompareAbilityUI : MonoBehaviour
{
    [Tooltip("전투력")]
    [SerializeField]
    Text _txtBattleAbility;

    [Tooltip("전투력 변경값")]
    [SerializeField]
    Text _txtBattleAbilityChange;

    [Tooltip("공격력")]
    [SerializeField]
    Text _txtDamage;

    [Tooltip("공격력 변경값")]
    [SerializeField]
    Text _txtDamageChange;

    [Tooltip("공격 속도")]
    [SerializeField]
    Text _txtAttackSpeed;

    [Tooltip("공격 속도 변경값")]
    [SerializeField]
    Text _txtAttackSpeedChange;

    [Tooltip("명중률")]
    [SerializeField]
    Text _txtAccuracy;

    [Tooltip("명중률 변경값")]
    [SerializeField]
    Text _txtAccuracyChange;

    //public void Compare(ServerStatic.Weapon inFrom, ServerStatic.Weapon inTo)
    //{
    //    if (inFrom == null)
    //    {
    //        /// Set Value
    //        _txtBattleAbility.text = "0";
    //        _txtDamage.text = "0";
    //        _txtAttackSpeed.text = "0";
    //        _txtAccuracy.text = "0";

    //        if (inTo == null)
    //        {
    //            _txtBattleAbilityChange.text = "";
    //            _txtDamageChange.text = "";
    //            _txtAttackSpeedChange.text = "";
    //            _txtAccuracyChange.text = "";
    //        }
    //        else
    //        {
    //            /// Set Difference Value
    //            _txtBattleAbilityChange.text = inTo.damage.ToString();
    //            _txtDamageChange.text = inTo.damage.ToString();
    //            _txtAttackSpeedChange.text = inTo.speed.ToString();
    //            _txtAccuracyChange.text = ("90").ToString();
    //        }
    //    }
    //    else
    //    {
    //        /// Set Value
    //        _txtBattleAbility.text = inFrom.damage.ToString();
    //        _txtDamage.text = inFrom.damage.ToString();
    //        _txtAttackSpeed.text = inFrom.speed.ToString();
    //        _txtAccuracy.text = "90";

    //        /// Set Difference Value
    //        _txtBattleAbilityChange.text = (inTo.damage - inFrom.damage).ToString();
    //        _txtDamageChange.text = (inTo.damage - inFrom.damage).ToString();
    //        _txtAttackSpeedChange.text = (inTo.speed - inFrom.speed).ToString();
    //        _txtAccuracyChange.text = ("10").ToString();
    //    }

    //    /// Add +,- PostFix
    //    AddPostfix(_txtBattleAbilityChange);
    //    AddPostfix(_txtDamageChange);
    //    AddPostfix(_txtAttackSpeedChange);
    //    AddPostfix(_txtAccuracyChange);
    //}

    private void AddPostfix(Text inText)
    {
        if (inText.text.IsNullOrEmpty())
            return;

        Color plus  = STZCommon.ToUnityColor(0, 0, 255);
        Color minus = STZCommon.ToUnityColor(255, 0, 0);
        Color equal = STZCommon.ToUnityColor(0, 255, 0);
        float value = float.Parse(inText.text);

        if (value == 0)
        {
            inText.text = value.ToString();
            inText.color = equal;
        } 
        else if (value < 0)
        {
            inText.text = "-" + value.ToString();
            inText.color = minus;
        }
        else if (value > 0)
        {
            inText.text = "+" + value.ToString();
            inText.color = plus;
        }
    }
}
