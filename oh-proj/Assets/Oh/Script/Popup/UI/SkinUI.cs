﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinUI : MonoBehaviour
{
    [Tooltip("스킨 이름")]
    [SerializeField]
    Text _txtSkinName;

    [Tooltip("스킨 이미지")]
    [SerializeField]
    Image _imgSkin;

    ServerStatic.ShipSkin _data;

    public void Refresh(ServerStatic.ShipSkin inData)
    {
        _data = inData;

        if (_data == null)
        {
            _txtSkinName.text = "None";
            _imgSkin.gameObject.SetActive(false);
        }
        else
        {
            _txtSkinName.text = _data.name;
            _imgSkin.gameObject.SetActive(true);
            _imgSkin.sprite = Resources.Load<Sprite>("Texture/Skin/" + _data.id);
        }
    }
}
