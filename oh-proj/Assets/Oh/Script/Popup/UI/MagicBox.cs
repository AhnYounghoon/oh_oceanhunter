﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class MagicBox : MonoBehaviour
{
    [SerializeField]
    Image[] _imgBox;

    [SerializeField]
    Image _imgIcon;

    [SerializeField]
    Sprite _goldSprite;

    [SerializeField]
    Text _txtRewardCount;

    //[SerializeField]
    //Sprite[] _boxSpritePack;

    private System.Action _inEndCallback = null;
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void SetReward(RewardData inData, System.Action inEndCallback)
    {
        _inEndCallback          = inEndCallback;
        //_txtRewardCount.text    = StringHelper.Format("x{0}", inData.reward_count);
        _txtRewardCount.text = inData.name;
        _imgIcon.sprite         = Resources.Load<Sprite>(inData.iconPath);
        _imgIcon.GetComponent<RectTransform>().sizeDelta = _imgIcon.sprite.bounds.size;

        foreach (var o in _imgBox)
            o.gameObject.SetActive(false);

        // 보상에 맞게 상자 이미지 셋팅
        //ServerStatic.RouletteInfiniteReward reward = Statics.Instance.roulette_infinite_reward.Values.Where(x => x.item_ids == inData.itemId).ToList().FirstOrDefault();
        //if (reward != null)
        //{
        //    //_imgBox.sprite = _boxSpritePack[reward.group - 1];
        //    _imgBox[reward.group - 1].gameObject.SetActive(true);
        //}
        _imgBox[0].gameObject.SetActive(true);
    }

    private void EndOpen()
    {
        if (_inEndCallback != null)
        {
            _inEndCallback();
        }
    }

    private void BoxOpenSound()
    {
        SingletonController.Get<SoundManager>().Play("UI_FortuneOpen", "sfx_ui");
    }

    public void Open(bool inIsGold)
    {
        if (_animator == null)
            return;

        _animator.Rebind();
        if (inIsGold)
            _animator.Play("ani_Magic_box_select");
        else
            _animator.Play("ani_silver_box_select");
    }
}
