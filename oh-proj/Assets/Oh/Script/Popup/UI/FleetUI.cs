﻿using UnityEngine;

public class FleetUI : MonoBehaviour
{
    [SerializeField] FleetCharacterUI hero;
    [SerializeField] FleetCharacterUI sailor;

    public void Init(int inHeroId, int inSailorId)
    {
        ServerStatic.Character heroData    = Statics.Instance.character.ContainsKey(inHeroId) ? Statics.Instance.character[inHeroId] : null;
        ServerStatic.Character sailorData  = Statics.Instance.character.ContainsKey(inSailorId) ? Statics.Instance.character[inSailorId] : null;

        if (heroData != null)
        {
            hero.Reset(heroData);
            hero.inInfoCallback = (ServerStatic.Character data) => 
            {
                SingletonController.Get<PopupManager>().Show(typeof(PopupViewCharacter), null, STZCommon.Hash(
                    PopupViewCharacter.EParam.EHero, data, PopupViewCharacter.EParam.ESailor, sailorData));
            };
        }
        if (sailorData != null)
        {
            sailor.Reset(sailorData);
        }
    }

    public void SetCharacter(int inId)
    {
        ServerStatic.Character character = Statics.Instance.character[inId];
        if (character != null)
        {
            if (character.category == 1 && hero.addMode)
            {
                hero.Reset(character);
            }
            else if (character.category == 2 && sailor.addMode)
            {
                sailor.Reset(character);
            }
        }
    }

    public string ToDataType()
    {
        if (hero.id == 0)
        {
            return string.Empty;
        }

        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        builder.Append(hero.id.ToString());
        builder.Append(":");
        builder.Append(sailor.id.ToString());
        builder.Append(",");
        return builder.ToString();
    }
}
