﻿using STZFramework;
using UnityEngine;
using System.Collections.Generic;

public class QuestUI : MonoBehaviour
{
    [SerializeField]
    QuestSelectTableView tableView;

    public void SetQuest()
    {
        FileMap questFM = GeneralDataManager.it.GetQuestData();

        if (questFM == null)
            return;

        List<int> questIds = new List<int>();
        int totalQuestCount = questFM.Get<int>(GeneralDataManager.EParam.COUNT.ToString());
        for (int i = 1; i <= totalQuestCount; i++)
        {
            // 미완료된 퀘스트만 추출
            if (questFM.Get<int>(i.ToString()) <= 0)
                questIds.Add(i);
        }

        tableView.Reset(questIds);
    }
}
