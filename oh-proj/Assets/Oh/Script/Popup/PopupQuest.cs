﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Popup(prefab_path = "Prefabs/Popup/PopupQuest")]
public class PopupQuest : CustomPopup
{
    [SerializeField]
    Text txtTitle;
    [SerializeField]
    Text txtDesc;
    [SerializeField]
    Text txtAction;
    [SerializeField]
    Image[] rewards;

    private int selectedEpisode;

    protected override void PrevOpen()
    {
        ServerStatic.Quest quest = (ServerStatic.Quest)_param["data"];
        txtTitle.text = quest.title;
        txtDesc.text = quest.desc_1;
        txtAction.text = quest.desc_2;
        RewardData[] parsedReward = RewardData.ParseItemIDs(quest.reward);

        for (int i = 0; i < rewards.Length; i++)
        {
            rewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < parsedReward.Length; i++)
        {
            rewards[i].gameObject.SetActive(true);
            rewards[i].sprite = Resources.Load<Sprite>(parsedReward[i].iconPath);
        }
    }

    public void OnQuest()
    {
        PopupStageSelection.Show(null);
    }

    public static void Show(Callback inCallback, ServerStatic.Quest inData)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupQuest), inCallback, STZCommon.Hash("data", inData));
    }
}
