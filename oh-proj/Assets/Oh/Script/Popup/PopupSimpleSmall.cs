﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[Popup(prefab_path = "Prefabs/Popup/PopupSimpleSmall")]
public class PopupSimpleSmall : BasePopUp
{
    public void OnYes()
    {
        Exit();
    }

    public void OnNo()
    {
        Exit();
    }

    protected override void PrevOpen()
    {
        base.PrevOpen();

        GameObject BtnOK = transform.Find("Background/BtnOk").gameObject;
        GameObject TitleGO = transform.Find("Background/Title").gameObject;

        Text titleText = TitleGO.transform.Find("Text").GetComponent<Text>();

        if (_param.ContainsKey("text"))
        {
            this.Get<Text>("Background/Text").text = (string)_param["text"];
        }

        // init title 
        {
            const string titleKey = "titleText";
            if (_param.ContainsKey(titleKey) && _param[titleKey] != null)
            {
                TitleGO.SetActive(true);
                titleText.text = (string)_param[titleKey];
            }
            else
            {
                TitleGO.SetActive(false);
            }
        }
    }
}
