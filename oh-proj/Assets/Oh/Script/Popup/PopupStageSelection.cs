﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Popup(prefab_path = "Prefabs/Popup/PopupStageSelection")]
public class PopupStageSelection : CustomPopup
{
    [SerializeField]
    StageSelectTableView tableView;
    [SerializeField]
    Text txtEpisodeName;
    [SerializeField]
    Text txtEpisodeDesc;
    [SerializeField]
    GameObject[] stages;

    private List<StageData> _stageDatas;
    private int selectedEpisode;

    protected override void PrevOpen()
    {
        selectedEpisode = 1;

        ChangedEpisode(selectedEpisode);
    }

    public void OnChangeEpisode(int inEpisodeId)
    {
        if (selectedEpisode != inEpisodeId)
        {
            selectedEpisode = inEpisodeId;
            ChangedEpisode(selectedEpisode);
        }
    }

    void ChangedEpisode(int inEpisodeId)
    {
        ServerStatic.SailingCatogory o = ServerStatic.SailingCatogory.GetListByEpisodeId(inEpisodeId)[0];
        txtEpisodeName.text = StringHelper.Format("<color=#FFC000>제 {0} 장</color> {1}", inEpisodeId, o.episode_desc);
        //txtEpisodeDesc.text = StringHelper.Format("에피소드 {0:D2} - 설명", inEpisodeId);
        //tableView.Reset(inEpisodeId);

        StageSetting();
    }

    void StageSetting()
    {
        List<ServerStatic.SailingCatogory> stageList = ServerStatic.SailingCatogory.GetListByEpisodeId(selectedEpisode);

        if (_stageDatas == null)
            _stageDatas = new List<StageData>();

        for (int i = 0; i < stageList.Count; i++)
        {
            if (i + 1 > GeneralDataManager.it.GetStageClearData() + 1)
            {
                stages[i].Get<Image>("deactivate").gameObject.SetActive(true);
                stages[i].Get<Image>("activate").gameObject.SetActive(false);
            }
            else
            {
                stages[i].Get<Image>("deactivate").gameObject.SetActive(false);
                stages[i].Get<Image>("activate").gameObject.SetActive(true);
            }

            // 현재 진행중인 해협에 화살표 표시
            stages[i].Get<Image>("Arrow").gameObject.SetActive(i + 1 == GeneralDataManager.it.GetStageClearData() + 1 ? true : false);

            stages[i].Get<Text>("Text").text = stageList[i].stage_desc;
            StageData newData = new StageData();
            newData.data = stageList[i];
            _stageDatas.Add(newData);
        }
    }

    public void OnSelect(int inId)
    {
        StageData targetData = _stageDatas[inId - 1];

        if (targetData.data.stage_id > GeneralDataManager.it.GetStageClearData() + 1)
        {
            return;
        }

        if (StageDataManager.it.SelectStage(targetData.data.stage_id) == null)
        {
            SimplePopup.ForceConfirm(StringHelper.Format("해당 ({0})스테이지 데이터가 없습니다.", targetData.data.stage_id));
            return;
        }

        PopupStageInfo.Show(null, targetData.data);
    }

    public static void Show(Callback inCallback)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupStageSelection), inCallback);
    }
}

public class StageData
{
    public ServerStatic.SailingCatogory data;
}
