﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx;
using UniLinq;

/// <summary>
/// 테스트 클래스 
/// </summary>
public class CustomPopup : BasePopUp
{
    [SerializeField]
    bool _animationTest = false;

    List<CustomAnimation> _animationList;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _canvas   = GetComponent<Canvas>();

        _animationList = this.GetComponentsInChildren<CustomAnimation>().ToList();
        if (_animationTest)
        {
            AnimateOpen();
        }
    }

    protected override void AnimateOpen()
    {
        if (_animationList.Count == 0)
        {
            LastOpen();
        }
        else
        {
            foreach (var animation in _animationList)
            {
                if (animation == null)
                {
                    continue;
                }
                if (animation.name == "Background")
                {
                    animation.Play("Open", false, LastOpen);
                }
                else
                {
                    animation.Play("Open", false, null);
                }
            }
        }
    }

    protected override void AnimateClose()
    {
        if (_animationList.Count == 0)
        {
            LastClose();
        }
        else
        {
            foreach (var animation in _animationList)
            {
                if (animation == null)
                {
                    continue;
                }
                if (animation.name == "Background")
                {
                    animation.Play("Close", false, LastClose);
                }
                else
                {
                    animation.Play("Close", false, null);
                }
            }
        }
    }

    public override void Close(bool isCallbackEnabled = true, bool isAnimationEnabled = true, bool isCanceledNext = false)
    {
        /* 팝업이 닫혀 있으면 해당 함수를 수행하지 않는다. */
        if (_isClosed)
            return;

        if (_canvasGroup != null)
        {
            _canvasGroup.interactable = false;
        }

        /* 콜백을 사용하지 않는다면 등록되어 있는 콜백 제거 */
        if (!isCallbackEnabled)
            _callback = null;

        _isClosed = true;
        _animating = true;

        PrevClose();
        AnimateClose();

        PlaySound(_closeSound);
    }
}
