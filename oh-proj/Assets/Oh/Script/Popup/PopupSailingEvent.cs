﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[Popup(prefab_path = "Prefabs/Popup/PopupSailingEvent")]
public class PopupSailingEvent : BasePopUp
{
    public enum EParam
    {
        EVENT_TYPE,
        EVENT_AMOUNT
    }

    [SerializeField]
    Image scene_image;

    [SerializeField]
    GameObject resultArea;
    [SerializeField]
    Text txtResult;

    [SerializeField]
    GameObject statusArea;
    [SerializeField]
    Text txtStatus;

    Queue<ServerStatic.SailingEvent> event_queue;
    ServerStatic.SailingEvent targetEvent;

    protected override void PrevOpen()
    {
        base.PrevOpen();

        var list = ServerStatic.SailingEvent.GetListByType((int)_param[EParam.EVENT_TYPE]);
        if (list == null || list.Length <= 0)
            return;

        for (int i = 0; i < (int)_param[EParam.EVENT_AMOUNT]; i++)
        {
            int sum = 0;
            int random = Random.Range(0, 10000) + 1;
            foreach (ServerStatic.SailingEvent o in list)
            {
                sum += o.rate;
                if (random <= sum)
                {
                    if (event_queue == null) event_queue = new Queue<ServerStatic.SailingEvent>();
                    event_queue.Enqueue(o);
                    break;
                }
            }
        }

        targetEvent = event_queue.Dequeue();
        StartCoroutine(SetEvent());

        GameObject TitleGO = transform.Find("Background/Title").gameObject;
        Text titleText = TitleGO.transform.Find("Text").GetComponent<Text>();

        // init title 
        {
            const string titleKey = "titleText";
            if (_param.ContainsKey(titleKey) && _param[titleKey] != null)
            {
                TitleGO.SetActive(true);
                titleText.text = (string)_param[titleKey];
            }
            else
            {
                TitleGO.SetActive(false);
            }
        }        
    }

    public void ChangeScene()
    {

    }

    public void SetRewardComment()
    {

    }

    IEnumerator SetEvent()
    {
        if (targetEvent == null)
            yield break;

        resultArea.SetActive(false);
        txtStatus.text = "탐사중..";

        yield return new WaitForSeconds(2.0f);

        StartCoroutine(ShowEventResult());
    }

    IEnumerator ShowEventResult()
    {
        txtStatus.text = targetEvent.text;

        if (!targetEvent.event_reward.IsNullOrEmpty())
            yield return new WaitForSeconds(1.0f);
        else
            yield return null;

        //targetEvent.event_reward = "8";
        string[] eventRewardArr = targetEvent.event_reward.Split(',');
        int random = Random.Range(0, eventRewardArr.Length);
        var list = ServerStatic.SailingEventReward.GetListById(eventRewardArr[random].IntValue());
        foreach (ServerStatic.SailingEventReward o in list)
        {
            resultArea.SetActive(true);
            string resultText = string.Empty;
            if (o.category.Equals("goods"))
            {
                ServerStatic.Goods targetGoods = ServerStatic.Goods.GetItemById(o.value.IntValue());
                resultText = StringHelper.Format("{0} {1}를 획득했다.", targetGoods.name.Locale(), Random.Range(o.min, o.max + 1));
            }
            else
            {
                string rewardName = StringHelper.Format("{0}_{1}", o.category, o.value).ToUpper();
                int randomValue = Random.Range(o.min, o.max + 1);
                float percentage = 1.0f;
                if (o.plus_minus.Equals("plus"))
                    percentage += (float)randomValue / 100;
                else
                    percentage -= (float)randomValue / 100;

                resultText = StringHelper.Format("{0}가 {1}% {2}했다.", rewardName.Locale(), randomValue, o.plus_minus.Equals("plus") ? "증가" : "감소");
                if (o.value.Equals("damage"))
                {
                    GeneralDataManager.it.playerData[0].ChangeStat(BattleEnum.EProperty.Damage, percentage);
                }
                else if (o.value.Equals("defense"))
                {
                    GeneralDataManager.it.playerData[0].ChangeStat(BattleEnum.EProperty.Defense, percentage);
                }
                else if (o.value.Equals("hp"))
                {
                    GeneralDataManager.it.playerData[0].ChangeStat(BattleEnum.EProperty.Hp, percentage);
                }

            }

            txtResult.text = resultText;
        }

        yield return new WaitForSeconds(2.0f);

        OnNextEvent();
    }

    public void OnNextEvent()
    {
        if (event_queue.Count == 0)
        {
            Exit();
            return;
        }

        targetEvent = event_queue.Dequeue();
        StartCoroutine(SetEvent());
    }

    public static void Show(Callback inCallback, int inEventType, int inEventAmount)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupSailingEvent), inCallback, STZCommon.Hash(EParam.EVENT_TYPE, inEventType, EParam.EVENT_AMOUNT, inEventAmount));
    }
}
