﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

[Popup(prefab_path = "Prefabs/Popup/PopupFleet")]
public class PopupFleet : CustomPopup
{
    [SerializeField]
    Image _selectLine;

    [SerializeField]
    Image[] _selectButton;

    [SerializeField]
    Sprite[] _selectButtonSprite;

    [SerializeField]
    CharacterTableView _tableView;

    [SerializeField]
    FleetUI[] _fleetUI;

    protected override void PrevOpen()
    {
        MainWealthUI.it.Show(true);
        SetComponent();
    }

    protected override void PrevClose()
    {
        MainWealthUI.it.Rewind();

        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach (var ui in _fleetUI)
        {
            builder.Append(ui.ToDataType());
        }
        builder.Remove(builder.Length - 1, 1);
        Debug.Log(builder.ToString());
        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.FLEET, string.Empty, 1, 1, builder.ToString());
    }

    private void SetComponent()
    {
        // 테이블
        {
            OnHeroTab();
            _tableView.OnTouchEvent = OnTouchCharacter;
        }

        int selectIdx = 1;
        Dictionary<int, string> fleetData = GeneralDataManager.it.GetFleetData();
        string[] fleet = fleetData[selectIdx].Split(',');
        for (int i = 0; i < fleet.Length; i++)
        {
            string[] spliter = fleet[i].Split(':');
            int hero   = spliter[0].IntValue();
            int sailor = spliter[1].IntValue();
            _fleetUI[i].Init(hero, sailor);
        }
    }

    private void OnTouchCharacter(int inId)
    {
        foreach (var ui in _fleetUI)
        {
            ui.SetCharacter(inId);
        }
    }

    public void OnHeroTab()
    {
        List<int> heroList = GeneralDataManager.it.GetHeroList();
        List<ServerStatic.Character> list = Statics.Instance.character.Values.Where(x => heroList.Contains(x.id)).OrderBy(x => x.category).ToList();
        _tableView.Init(list);

        _selectLine.transform.SetParent(_selectButton[0].transform);
        _selectLine.transform.SetPosition(new Vector2(0, 27.5f));

        _selectButton[0].sprite = _selectButtonSprite[0];
        _selectButton[1].sprite = _selectButtonSprite[1];
    }

    public void OnSailorTab()
    {
        List<int> sailorList = GeneralDataManager.it.GetSailorList();

        List<ServerStatic.Character> list = new List<ServerStatic.Character>();
        if (sailorList != null && sailorList.Count > 0)
        {
            list = Statics.Instance.character.Values.Where(x => sailorList.Contains(x.id)).OrderBy(x => x.category).ToList();
        }
        _tableView.Init(list);

        _selectLine.transform.SetParent(_selectButton[1].transform);
        _selectLine.transform.SetPosition(new Vector2(0, 27.5f));

        _selectButton[0].sprite = _selectButtonSprite[1];
        _selectButton[1].sprite = _selectButtonSprite[0];
    }
}