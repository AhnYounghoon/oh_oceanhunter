﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

[Popup(prefab_path = "Prefabs/Popup/PopupQuestReward")]
public class PopupQuestReward : BasePopUp
{
    [SerializeField]
    Text txtStageId;
    [SerializeField]
    Text txtDesc;

    [SerializeField]
    Image[] default_rewards;

    [SerializeField]
    Image[] add_rewards;

    [SerializeField]
    Button BtnConfirm;

    ServerStatic.Quest data = null;

    protected override void PrevOpen()
    {
        data = (ServerStatic.Quest)_param["data"];

        for (int i = 0; i < default_rewards.Length; i++)
        {
            default_rewards[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < add_rewards.Length; i++)
        {
            add_rewards[i].gameObject.SetActive(false);
        }

        RewardData[] parsedReward = RewardData.ParseItemIDs(data.reward);
        //for (int i = 0; i < parsedReward.Length; i++)
        //{
        //    default_rewards[i].gameObject.SetActive(true);
        //    default_rewards[i].sprite = Resources.Load<Sprite>(parsedReward[i].iconPath);
        //}

        //for (int i = 0; i < parsedReward.Length; i++)
        //{
        //    add_rewards[i].gameObject.SetActive(true);
        //    add_rewards[i].sprite = Resources.Load<Sprite>(parsedReward[i].iconPath);
        //}
        default_rewards[0].gameObject.SetActive(true);
        default_rewards[0].sprite = Resources.Load<Sprite>(parsedReward[0].iconPath);
        add_rewards[0].gameObject.SetActive(true);
        add_rewards[0].sprite = Resources.Load<Sprite>(parsedReward[1].iconPath);

        GeneralDataManager.it.AcquireReward(data.reward);
        GeneralDataManager.it.AcquireQuest(data.id);
    }

    public void OnConfirm()
    {
    }

    public static void Show(Callback inCallback, ServerStatic.Quest inData)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupQuestReward), inCallback, STZCommon.Hash("data", inData));
    }
}
