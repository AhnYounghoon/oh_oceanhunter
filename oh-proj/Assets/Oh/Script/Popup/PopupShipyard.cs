﻿using System.Collections.Generic;
using UniLinq;
using UnityEngine;

[Popup(prefab_path = "Prefabs/Popup/PopupShipyard")]
public class PopupShipyard : CustomPopup
{
    [Tooltip("함대 배열")]
    [SerializeField]
    FleetUI[] fleetArray;

    protected override void PrevOpen()
    {
        Dictionary<int, string> fleetData = GeneralDataManager.it.GetFleetData();

        /// Set Fleet Data
        foreach (var data in GeneralDataManager.it.playerFleetList)
        {
            if (fleetArray.Length <= data.index)
            {
                continue;
            }

            FleetUI cell = fleetArray[data.index - 1];
            //cell.Refresh(data, FleetUI.EShowMode.ShowCell);
            //cell.OnEquipment = OnEquipment;
            //cell.OnUpgrade   = OnUpgrade;
        }

        /// Lock Cell
        foreach (var cell in fleetArray)
        {
            //if (!cell.DataUsable)
            //{
            //    cell.Refresh(null, FleetUI.EShowMode.ShowCell);
            //}
        }
    }

    private void OnEquipment(FleetData inData)
    {
        ShowPopupShip(inData);
    }

    private void OnUpgrade(FleetData inData)
    {

    }

    public void ShowPopupShip(FleetData inData)
    {
        // NOTE @minjun.ha 기존 Callback을 유지하기 위함
        Callback lastCallback = _callback;
        _callback = (_) =>
        {
            SingletonController.Get<PopupManager>().Show(typeof(PopupShip), param =>
            {
                PopupShipyard.Show(lastCallback);
            }, STZCommon.Hash(PopupShip.EParam.C_FLEET_DATA, inData));
        };
        Exit();
    }

    public static void Show(Callback inCallback)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupShipyard), inCallback);
    }
}
