﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;

[Popup(prefab_path = "Prefabs/Popup/PopupOrganization")]
public class PopupOrganization : CustomPopup
{
    public enum EPanel
    {
        Weapon,
        Character
    }

    [SerializeField]
    ShipUI shipUI;

    [SerializeField]
    WeaponTableView weaponTableView;

    [SerializeField]
    CharacterTableView characterTableView;
    
    [SerializeField]
    GameObject []panel;

    [SerializeField]
    Image[] shipGroupImage;

    [SerializeField]
    Image[] panelButtonImage;

    private readonly EPanel defaultPanel = EPanel.Character;

    private List<ServerStatic.BattleObject>     playerShipList   = null;
    //private List<ServerStatic.Weapon>           playerWeaponList = null;
    private List<ServerStatic.Character>        playerCharacterList = null;

    protected override void PrevOpen()
    {
        // TODO @minjun 테스트 코드
        {
            playerShipList   = new List<ServerStatic.BattleObject>() { Statics.Instance.battle_object[3], Statics.Instance.battle_object[1] };
            //playerWeaponList = new List<ServerStatic.Weapon>() { Statics.Instance.weapon[1], Statics.Instance.weapon[3] };
            playerCharacterList = new List<ServerStatic.Character>() { Statics.Instance.character[1], Statics.Instance.character[2], Statics.Instance.character[3], Statics.Instance.character[4], Statics.Instance.character[5], Statics.Instance.character[6] };
        }

        //weaponTableView.Init(playerWeaponList);
        characterTableView.Init(playerCharacterList);

        ActiveShipGroup(1);
    }

    /// <summary>
    /// 해당 함선을 활성화
    /// </summary>
    /// <see cref="Connected Button Inspector"/>
    /// <param name="idx">0 ~ 해금된 함선 수</param>
    public void ActiveShipGroup(int idx)
    {
        ServerStatic.BattleObject ship = playerShipList.ElementAtOrDefault(idx);
        if (ship == null)
        {
            // TODO @minjun.ha 해당 함선이 없음 상점으로 이동하는 플로우 추가?
            return;
        }

        // TODO @minjun.ha 임시로 마지막에 본 배를 메인 함선으로 설정
        GeneralDataManager.it.targetBattleObject = ship;

        for (int i = 0; i < shipGroupImage.Length; i++)
        {
            shipGroupImage[i].color = (i == idx) ? STZCommon.ToUnityColor(255, 132, 0, 175) : STZCommon.ToUnityColor(165, 165, 165, 175);
        }
        shipUI.Refresh(ship);

        // Default Panel
        ActivePanel((int)defaultPanel);
    }

    /// <summary>
    /// 내부 패널 활성화
    /// </summary>
    /// <see cref="EPanel"/>
    /// <see cref="Connected Button Instpector"/>
    /// <param name="idx"></param>
    public void ActivePanel(int idx)
    {
        for (int i = 0; i < panelButtonImage.Length; i++)
        {
            panelButtonImage[i].color = (i == idx) ? STZCommon.ToUnityColor(255, 132, 0, 175) : STZCommon.ToUnityColor(165, 165, 165, 175);
        }
        for (int i = 0; i < panel.Length;i++)
        {
            panel[i].gameObject.SetActive(idx == i);
        }
        shipUI.SetDropMode((ShipUI.EShowType)(idx + 1));
    }
}
