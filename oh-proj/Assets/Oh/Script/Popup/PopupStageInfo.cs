﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

[Popup(prefab_path = "Prefabs/Popup/PopupStageInfo")]
public class PopupStageInfo : BasePopUp
{
    [SerializeField]
    Text txtStageId;
    [SerializeField]
    Text txtDesc;

    [SerializeField]
    Image[] rewards;

    [SerializeField]
    CharacterTableViewCell[] charInfo;

    [SerializeField]
    Button BtnSelectShip;
    [SerializeField]
    Button BtnSailingMode;
    [SerializeField]
    Button BtnStart;

    ServerStatic.SailingCatogory data = null;

    protected override void PrevOpen()
    {
        data = (ServerStatic.SailingCatogory)_param["data"];
        //txtStageId.text = StringHelper.Format("{0}-{1}", data.episode_id, data.stage_id);
        txtStageId.text = data.episode_desc;
        //txtDesc.text = data.episode_desc;

        Dictionary<int, string> fleetInfo = GeneralDataManager.it.GetFleetData();
        string value = fleetInfo[1];
        string[] shipArr = value.Split(',');
        for (int i = 0; i < shipArr.Length; i++)
        {
            string[] personArr = shipArr[i].Split(':');
            var charStatic = Statics.Instance.character.Values.Where(x => x.id == personArr[0].IntValue()).FirstOrDefault();
            charInfo[i].Reset(charStatic, null);
        }

        RewardData[] parsedReward = RewardData.ParseItemIDs(data.clear_reward);
        for (int i = 0; i < rewards.Length; i++)
        {
            rewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < parsedReward.Length; i++)
        {
            rewards[i].gameObject.SetActive(true);
            rewards[i].sprite = Resources.Load<Sprite>(parsedReward[i].iconPath);
        }
    }

    public void OnSailingMode()
    {
        BtnSailingMode.Get<Text>("Text").text = "";
    }

    public void OnStart()
    {
        if (data != null)
            GeneralDataManager.it.prevStage = data.stage_id;

        GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.LobbyScene;
        UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.BattleOceanScene.ToString());
    }

    public void OnEditFleet()
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupFleet));
    }

    public static void Show(Callback inCallback, ServerStatic.SailingCatogory inData)
    {
        SingletonController.Get<PopupManager>().Show(typeof(PopupStageInfo), inCallback, STZCommon.Hash("data", inData));
    }
}
