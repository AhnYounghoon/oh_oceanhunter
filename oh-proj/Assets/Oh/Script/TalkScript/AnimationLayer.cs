﻿using System.Collections.Generic;
using UnityEngine;

namespace TalkScript
{
    public class AnimationLayer : MonoBehaviour
    {
        private Dictionary<string, GameObject> _animationDic = new Dictionary<string, GameObject>();

        public void AddAnimation(string name)
        {
            GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/TalkScript/Animation/" + name));
            go.transform.SetParent(this.transform);
            go.name = name;
            _animationDic.Add(name, go);
        }

        public void RemoveAnimation(string inName)
        {
            if (!_animationDic.ContainsKey(inName))
            {
                return;
            }

            MonoBehaviour.Destroy(_animationDic[inName].gameObject);
            _animationDic.Remove(inName);
        }
    }
}
