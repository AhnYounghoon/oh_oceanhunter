﻿using UnityEngine;

namespace TalkScript
{
    public class CameraBehavior : Behavior
    {
        private GameCamera _gameCamera = null;

        private void Awake()
        {
            loadKey = "CAMERA";

            _gameCamera = Camera.main.GetComponent<GameCamera>();
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("TARGET"))
            {
                string target = inData.hashTable.StringValue("Target");
                if (target == "Player")
                {
                    //if (_gameCamera != null)
                    //    _gameCamera.FocusPlayer(int.Parse(inData.hashTable.StringValue("Size")));
                }
                else if (target == "Enemy")
                {
                    //if (_gameCamera != null)
                    //    _gameCamera.FocusEnemy(int.Parse(inData.hashTable.StringValue("Size")));
                }
                else if (target == "Default")
                {
                    //if (_gameCamera != null)
                    //    _gameCamera.SetTargetDefault();
                }
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain CameraBehavior", inData.form));
            }
        }
    }
}
