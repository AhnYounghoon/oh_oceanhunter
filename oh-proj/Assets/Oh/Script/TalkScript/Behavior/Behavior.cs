﻿using UnityEngine;
using System.Collections;

namespace TalkScript
{
    public class Behavior : MonoBehaviour
    {
        /// <summary>
        /// 스크립트가 진행중인지를 반환
        /// </summary>
        [HideInInspector]
        public bool working;

        /// <summary>
        /// 다음으로 넘어가는 콜백
        /// </summary>
        [HideInInspector]
        public System.Action<bool> nextCallback;

        protected string loadKey = string.Empty;

        public bool ExcuteBehavior(BehaviorData inData)
        {
            if (inData.ContainForm(loadKey))
            {
                Excute(inData);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 진행중인 스크립트를 넘김
        /// <!-- 대화 스킵 정도만 작동 -->
        /// </summary>
        virtual public void SkipWork() { }

        virtual protected void Excute(BehaviorData inData) { }
    }
}
