﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

namespace TalkScript
{
    public class DialogueBehavior : Behavior
    {
        private CharacterLayer _characterLayer = null;
        private Text _txtName = null;
        private Text _txtText = null;

        private string waitText = string.Empty;

        private void Awake()
        {
            loadKey = "DIALOGUE";

            _txtName = this.transform.Find("UI/Name").GetComponent<Text>();
            _txtText = this.transform.Find("UI/Text").GetComponent<Text>();
            _characterLayer = this.transform.Find("CharacterLayer").GetComponent<CharacterLayer>();
        }

        private IEnumerator TypingText(string inText, float inTypeSpeed = 0.05f)
        {
            string copyText = string.Empty;
            waitText        = inText;
            _txtText.text    = string.Empty;

            working = true;
            for (int i = 0;i < inText.Length;i++)
            {
                copyText = inText.Substring(0, i);
                _txtText.text = copyText;
                yield return new WaitForSeconds(inTypeSpeed);
            }
            working = false;
            _txtText.text = inText;
        }

        public override void SkipWork()
        {
            StopAllCoroutines();
            working      = false;
            _txtText.text = waitText;
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("SPEAKER"))
            {
                _txtName.text = inData.speaker;

                if (_txtName.text.Contains(","))
                {
                    string []splitArray = _txtName.text.Split(',');
                    _characterLayer.FocusCharacter(splitArray);
                }
                else
                {
                    _characterLayer.FocusCharacter(_txtName.text);
                }
            }
            else if (inData.ContainForm("TALK"))
            {
                string talk = inData.talk.Replace("\\n", "\n");
                StartCoroutine(TypingText(talk));
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain DialogueBehavior", inData.form));
            }
        }
    }
}
