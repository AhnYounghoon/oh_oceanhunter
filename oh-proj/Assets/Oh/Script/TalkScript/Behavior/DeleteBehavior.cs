﻿using UnityEngine;

namespace TalkScript
{
    public class DeleteBehavior : Behavior
    {
        private CharacterLayer      _characterLayer  = null;
        private ImageLayer          _imageLayer      = null;
        private AnimationLayer      _animationLayer  = null;

        private void Awake()
        {
            loadKey = "DELETE";

            _characterLayer  = this.transform.Find("CharacterLayer").GetComponent<CharacterLayer>();
            _imageLayer      = this.transform.Find("ImageLayer").GetComponent<ImageLayer>();
            _animationLayer  = this.transform.Find("AnimationLayer").GetComponent<AnimationLayer>();
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("CHARACTER"))
            {
                _characterLayer.RemoveCharacter(inData.name);
            }
            else if (inData.ContainForm("IMAGE"))
            {
                _imageLayer.RemoveImage(inData.name);
            }
            else if (inData.ContainForm("ANIMATION"))
            {
                _animationLayer.RemoveAnimation(inData.name);
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain LoadBehavior", inData.form));
            }
        }
    }
}
