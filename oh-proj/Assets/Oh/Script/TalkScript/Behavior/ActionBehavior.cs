﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace TalkScript
{
    public class ActionBehavior : Behavior
    {
        private ImageLayer  _imageLayer  = null;
        private Image       _fadeBox     = null;

        private void Awake()
        {
            loadKey = "ACTION";

            _fadeBox     = this.transform.Find("FadeBox").GetComponent<Image>();
            _imageLayer  = this.transform.Find("ImageLayer").GetComponent<ImageLayer>();
        }

        private void EndAction()
        {
            working = false;
            if (nextCallback != null)
            {
                nextCallback(true);
            }
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("SCENE_FADEIN"))
            {
                working = true;
                _fadeBox.GetComponent<Canvas>().overrideSorting = true;
                _fadeBox.color = STZCommon.ToUnityColor(0, 0, 0, 0);
                _fadeBox.DOFade(1, inData.time).OnComplete(EndAction);
            }
            else if (inData.ContainForm("SCENE_FADEOUT"))
            {
                working = true;
                _fadeBox.GetComponent<Canvas>().overrideSorting = true;
                _fadeBox.color = STZCommon.ToUnityColor(0, 0, 0);
                _fadeBox.DOFade(0, inData.time).OnComplete(EndAction);
            }
            else if (inData.ContainForm("ACTION_DESOLVE"))
            {
                working = true;
                _imageLayer.Desolve(EndAction, inData.hashTable.StringValue("TargetA"), inData.hashTable.StringValue("TargetB"), inData.time);
            }
            else if (inData.ContainForm("ACTION_SCENE_SHAKE"))
            {
                working = true;
                Camera.main.DOShakePosition(inData.time, 30, 20).OnComplete(EndAction);
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain ActionBehavior", inData.form));
            }
        }
    }

}

