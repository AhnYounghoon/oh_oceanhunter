﻿using System.Collections.Generic;
using UnityEngine;

namespace TalkScript
{
    public class LoadBehavior : Behavior
    {
        private CharacterLayer      _characterLayer  = null;
        private ImageLayer          _imageLayer      = null;
        private AnimationLayer      _animationLayer  = null;

        private void Awake()
        {
            loadKey = "LOAD";

            _characterLayer  = this.transform.Find("CharacterLayer").GetComponent<CharacterLayer>();
            _imageLayer      = this.transform.Find("ImageLayer").GetComponent<ImageLayer>();
            _animationLayer = this.transform.Find("AnimationLayer").GetComponent<AnimationLayer>();
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("CHARACTER"))
            {
                _characterLayer.AddCharacter(inData.name, inData.position);
            }
            else if (inData.ContainForm("IMAGE"))
            {
                _imageLayer.AddImage(inData.name, inData.position);
            }
            else if (inData.ContainForm("ANIMATION"))
            {
                _animationLayer.AddAnimation(inData.name);
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain LoadBehavior", inData.form));
            }
        }
    }
}
