﻿using UniRx;
using UnityEngine;

namespace TalkScript
{
    public class SystemBehavior : Behavior
    {
        private CharacterLayer _characterLayer   = null;
        private ImageLayer _imageLayer               = null;
        private GameObject _animationLayer           = null;
        private GameObject _ui                       = null;
        private Talk       _talkParser               = null;

        System.IDisposable timer = null;
        private void Awake()
        {
            loadKey = "SYSTEM";

            _characterLayer  = this.transform.Find("CharacterLayer").GetComponent<CharacterLayer>();
            _imageLayer      = this.transform.Find("ImageLayer").GetComponent<ImageLayer>();
            _animationLayer  = this.transform.Find("AnimationLayer").gameObject;
            _ui              = this.transform.Find("UI").gameObject;
            _talkParser      = this.GetComponent<Talk>();
        }

        private void OnDestroy()
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
        }

        protected override void Excute(BehaviorData inData)
        {
            if (inData.ContainForm("WAIT"))
            {
                working = true;
                timer = Observable.Interval(System.TimeSpan.FromSeconds(inData.time)).Subscribe(TabBlock =>
                {
                    if (timer != null)
                    {
                        timer.Dispose();
                        timer = null;
                    }
                    working = false;
                    nextCallback(true);
                });
            }
            else if (inData.ContainForm("SHOW_UI"))
            {
                _characterLayer.gameObject.SetActive(true);
                _imageLayer.gameObject.SetActive(true);
                _animationLayer.gameObject.SetActive(true);
                _ui.gameObject.SetActive(true);
            }
            else if (inData.ContainForm("HIDE_UI"))
            {
                _characterLayer.gameObject.SetActive(false);
                _imageLayer.gameObject.SetActive(false);
                _animationLayer.gameObject.SetActive(false);
                _ui.gameObject.SetActive(false);
            }
            else if (inData.ContainForm("EVENT"))
            {
                working = true;
                _talkParser.CallEvent(inData.hashTable.StringValue("Function"), () =>
                {
                    working = false;
                    nextCallback(true);
                });
            }
            else
            {
                Debug.LogError(StringHelper.Format("[{0}] is not contain SystemBehavior", inData.form));
            }
        }
    }
}
