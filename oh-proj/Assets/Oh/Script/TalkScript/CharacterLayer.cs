﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TalkScript
{
    public class CharacterLayer : MonoBehaviour
    {
        private Dictionary<string, Image> _characterDic = new Dictionary<string, Image>();

        public void AddCharacter(string name, Vector2 position)
        {
            if (_characterDic.ContainsKey(name))
            {
                _characterDic[name].transform.localPosition = position;
                return;
            }

            Image image = ObjectCreator.it.CreateObj<Image>(this.transform, "Prefabs/TalkScript/Character", position);
            image.sprite = Resources.Load<Sprite>("Texture/TalkScript/Character/" + name);
            image.Get<RectTransform>().sizeDelta = image.Get<Image>().sprite.bounds.size;
            image.transform.localScale = new Vector3(1, 1, 1);
            image.name = name;

            _characterDic.Add(name, image);
            DisableCharacter(name);
        }

        public void RemoveCharacter(string inName)
        {
            if (!_characterDic.ContainsKey(inName))
            {
                return;
            }
            MonoBehaviour.Destroy(_characterDic[inName].gameObject);
            _characterDic.Remove(inName);
        }

        public void DisableCharacter(string inName)
        {
            if (!_characterDic.ContainsKey(inName))
            {
                return;
            }

            _characterDic[inName].color = STZCommon.ToUnityColor(75, 75, 75);
            _characterDic[inName].GetComponent<Canvas>().overrideSorting = true;
            _characterDic[inName].GetComponent<Canvas>().sortingOrder = 0;
            _characterDic[inName].transform.DOScale(0.9f, 0.5f);
        }

        public void AbleCharacter(string inName)
        {
            if (!_characterDic.ContainsKey(inName))
            {
                return;
            }

            _characterDic[inName].color = STZCommon.ToUnityColor(255, 255, 255);
            _characterDic[inName].GetComponent<Canvas>().overrideSorting = true;
            _characterDic[inName].GetComponent<Canvas>().sortingOrder = 1;
            _characterDic[inName].transform.DOScale(1.0f, 0.5f);
        }

        public void FocusCharacter(params string[] inNameArray)
        {
            if (inNameArray.Length == 1 && !_characterDic.ContainsKey(inNameArray[0]))
            {
                return;
            }

            List<string> nameList = new List<string>(inNameArray);
            foreach (var ch in _characterDic)
            {
                if (nameList.Contains(ch.Key))
                {
                    AbleCharacter(ch.Key);
                }
                else
                {
                    DisableCharacter(ch.Key);
                }
            }
        }
    }
}

