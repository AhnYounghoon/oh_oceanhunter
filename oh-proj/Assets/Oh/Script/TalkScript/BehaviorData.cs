﻿using System.Collections;
using UnityEngine;

namespace TalkScript
{
    public class BehaviorData
    {
        public string form { get { return hashTable.StringValue("Form"); } }
        public string name { get { return hashTable.StringValue("Name"); } }
        public string speaker { get { return hashTable.StringValue("Speaker"); } }
        public float time { get { return hashTable.FloatValue("Time"); } }
        public string talk { get { return hashTable.StringValue("Talk"); } }
        public Vector2 position
        {
            get
            {
                if (!hashTable.ContainsKey("Position"))
                {
                    return default(Vector2);
                }
                string[] posString = hashTable["Position"].ToString().Split(',');
                return new Vector2(int.Parse( posString[0]),int.Parse( posString[1]));
            }
        }

        public Hashtable hashTable = null;
        public BehaviorData()
        {
            if (hashTable == null)
            {
                hashTable = new Hashtable();
            }
        }

        /// <summary>
        /// 스킵 가능한지 여부를 반환 
        /// <!-- 대화만 스킵 가능하도록 설정 -->
        /// </summary>
        /// <returns></returns>
        public bool AbleSkip()
        {
            return string.IsNullOrEmpty(talk);
        }

        /// <summary>
        /// 명령어를 확인할때 사용
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ContainForm(string value)
        {
            return form.Contains(value);
        }
    }
}
