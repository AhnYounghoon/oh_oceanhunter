﻿using System.Collections.Generic;
using UnityEngine;

namespace TalkScript
{
    public abstract class Parser
    {
        public enum EErrorType
        {
            UnexpectedScript,       // 정의 되지 않은 스크립트
            GetOutOfEventName,      // @로 시작하지 않은 이벤트 함수
            MinusValue,             // 반드시 양수가 되어야하는 값 
            CameraTargetError,      // Player/Enemy/Default 이외의 값이 있을때
            UnRegistEvent,          // 등록되지 않은 이벤트 함수
            SupportBattleScene,     // 해당 씬에서는 지원하지 않음
        }

        public List<BehaviorData> behaviorList  = new List<BehaviorData>();

        // 등록되지 않은 이벤트 리스트
        public List<string> unregistEventList   = new List<string>();

        // 현재 읽어들인 라인
        protected string readLine = string.Empty;

        // ' '로 분리된 string 배열
        protected string[] splitArray = null;

        // 파일 경로
        private string _path = string.Empty;

        // 줄 번호
        private int _lineNumber = 0;

        // 에러 개수 
        private int _errorCount = 0;

        private readonly string colorCode = "45C9B0FF";

        protected void Log(string inMessage)
        {
            Debug.Log(StringHelper.Format("<color=#{0}>{1}</color>", colorCode, inMessage));
        }

        protected void ErrorLog(EErrorType inType)
        {
            string prefix  = "<color=#{0}>[Talk Script] {1}(line.{2}):error code[{3}] ";
            string postfix = " '{4}'</color>";
            string message = string.Empty;

            switch (inType)
            {
                case EErrorType.UnexpectedScript:
                    message = "Unexpected script";
                    break;
                case EErrorType.GetOutOfEventName:
                    message = "event must start '@' name";
                    break;
                case EErrorType.MinusValue:
                    message = "must plus number";
                    break;
                case EErrorType.CameraTargetError:
                    message = "camera target must (Player or Enemy or Default)";
                    break;
                case EErrorType.UnRegistEvent:
                    message = "unregist event";
                    break;
                case EErrorType.SupportBattleScene:
                    message = "support only BattleScene";
                    break;
                default:
                    break;
            }
            Debug.Log(StringHelper.Format(prefix + message + postfix, colorCode, _path, _lineNumber, (int)inType, readLine));
            _errorCount++;
        }

        /// <summary>
        /// 스크립트 빌드
        /// </summary>
        /// <param name="text"></param>
        public void BuildScript(string text)
        {
            _lineNumber = 0;
            _errorCount = 0;

            string[] data = text.Split('\n');
            for (int i = 0; i < data.Length; i++)
            {
                readLine = data[i].Replace("\r", "");
                if (readLine == "")
                {
                    LineEmpty();
                    continue;
                }
                _lineNumber = i + 1;

                splitArray = readLine.Split(' ');
                Parse();
            }
        }

        /// <summary>
        /// 해당 경로의 파일을 로드
        /// </summary>
        /// <param name="inPath"></param>
        /// <returns></returns>
        virtual public bool LoadData(string inPath)
        {
            _path = inPath;
            TextAsset file = Resources.Load(inPath) as TextAsset;
            if (file == null)
            {
                Log(StringHelper.Format("{0} is not exist!", inPath));
                return false;
            }

            Log(StringHelper.Format("------ Talk Script Build started: {0} ------", _path));
            BuildScript(file.text);
            Log(StringHelper.Format("------ Talk Script Build: succeeded, {0} failed ------", _errorCount));
            unregistEventList.Clear();
            return true;
        }

        /// <summary>
        /// 파싱 메소드 실행
        /// </summary>
        /// <param name="text"></param>
        virtual public void Parse()
        {
        }

        /// <summary>
        /// 빈 라인일때
        /// </summary>
        virtual public void LineEmpty()
        {

        }
    }
}
