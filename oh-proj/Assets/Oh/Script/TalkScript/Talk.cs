﻿/******************************************************************************
 * 대화 스크립트
 *
 * 명령어 시트
 * - https://drive.google.com/open?id=13U3AcxHhaq1k3VgYz8bRgWsC3wUKKdnPWsIopuoo2z4
 *
 * 구현된 행동 타입
 * - #System
 * - #Camera
 * - #Action
 * - #Load
 * - [Speaker]
 * 
 *****************************************************************************/

using System.Collections.Generic;
using TalkScript;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Talk : MonoBehaviour
{
    public delegate void TalkEventHandler(System.Action inEndEvent);
    public Dictionary<string, System.Delegate> eventFunctionDic = new Dictionary<string, System.Delegate>();

    System.Action       _endCallback  = null;
    System.IDisposable  _clickUpdate  = null;
    List<Behavior>      _behaviorList = null;
    Parser              _parser       = null;
    int                 _currentIdx   = 0;

	private void Awake()
    {
        _behaviorList = new List<Behavior>();

        // 파싱할 행동 스크립트를 받아옴
        var componentArray = this.GetComponentsInChildren(typeof(Behavior));
        foreach (var component in componentArray)
        {
            Behavior behavior = (Behavior)component;
            behavior.nextCallback = Next;
            _behaviorList.Add(behavior);
        }

        _parser = new TalkParser();
    }

    private void OnDestroy()
    {
        Clear();
    }

    private void Clear()
    {
        _currentIdx  = 0;
        _endCallback = null;
        if (_clickUpdate != null)
        {
            _clickUpdate.Dispose();
            _clickUpdate = null;
        }
    }

    public bool Parse(string inPath, System.Action inEndCallback)
    {
        Clear();
        _endCallback = inEndCallback;

        if (!_parser.LoadData(inPath))
        {
            return false;
        }

        // 클릭 콜백을 등록
        _clickUpdate = this.UpdateAsObservable()
            .Select(_ => (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space)))
            .DistinctUntilChanged()
            .Where(x => x)
            .Subscribe(Next);

        Next(true);
        return true;
    }

    /// <summary>
    /// 등록된 이벤트 함수를 호출
    /// <!-- 에러시 바로 스킵 -->
    /// </summary>
    /// <param name="inKey"></param>
    /// <param name="inCallback"></param>
    public void CallEvent(string inKey, System.Action inCallback)
    {
        if (!eventFunctionDic.ContainsKey(inKey))
        {
            Debug.LogError(StringHelper.Format("[{0}] is not contain eventFunctionDic", inKey));
            inCallback();
            return;
        }
        TalkEventHandler handler = eventFunctionDic[inKey] as TalkEventHandler;
        if (handler != null)
        {
            handler(inCallback);
        }
    }

    /// <summary>
    /// 이벤트 등록
    /// </summary>
    /// <param name="inKey"></param>
    /// <param name="inEvent"></param>
    public void AddEventFunction(string inKey, TalkEventHandler inEvent)
    {
        if (eventFunctionDic.ContainsKey(inKey))
        {
            eventFunctionDic[inKey] = (TalkEventHandler)eventFunctionDic[inKey] + inEvent;
        }
        else
        {
            // 등록되지 않은 이벤트 리스트중 등록되는 함수가 있다면 삭제한다
            if (_parser != null)
            {
                _parser.unregistEventList.Add(inKey);
            }
            eventFunctionDic.Add(inKey, inEvent);
        }
    }

    /// <summary>
    /// 다음 스크립트로 넘어감
    /// </summary>
    /// <param name="v"></param>
    private void Next(bool v)
    {
        // 작동중인 스크립트를 다시 터치했을 때 스킵하도록 설정
        Behavior workBehavior = CheckWorkBehavior();
        if (workBehavior != null)
        {
            workBehavior.SkipWork();
            return;
        }

        // 모든 스크립트를 끝냈을 때
        if (_parser.behaviorList.Count <= _currentIdx)
        {
            _clickUpdate.Dispose();
            if (_endCallback != null)
            {
                _endCallback();
            }
            return;
        }

        BehaviorData data = _parser.behaviorList[_currentIdx];
        _currentIdx++;
        ExcuteBehavior(data);

        // 바로 다음 스크립트로 넘어갈 수 있다면 재귀
        if (data.AbleSkip())
        {
            Next(v);
        }
    }

    /// <summary>
    /// 작동중인 스크립트를 받아옴
    /// </summary>
    /// <returns></returns>
    private Behavior CheckWorkBehavior()
    {
        Behavior workBehavior = null;
        foreach (var behavior in _behaviorList)
        {
            if (behavior.working)
            {
                workBehavior = behavior;
            }
        }
        return workBehavior;
    }

    /// <summary>
    /// 해당 데이터의 스크립트를 실행
    /// </summary>
    /// <param name="data"></param>
    private void ExcuteBehavior(BehaviorData data)
    {
        foreach (var behavior in _behaviorList)
        {
            bool b = behavior.ExcuteBehavior(data);
            if (b) break;
        }
    }
}
