﻿using System.Text;

namespace TalkScript
{
    public class TalkParser : Parser
    {
        public override bool LoadData(string path)
        {
            bool value = base.LoadData(path);
            return value;
        }

        public override void Parse()
        {
            base.Parse();

            switch (splitArray[0])
            {
                case "#Load": ParseLoad(); break;
                case "#Action": ParseAction(); break;
                case "#System": ParseSystem(); break;
                case "#Camera": ParseCamera(); break;
                case "#Delete": ParseDelete(); break;
                default:
                    if (!ParseDialogue())
                    {
                        ErrorLog(EErrorType.UnexpectedScript);
                    }
                    break;
            }
        }

        /// <summary>
        /// 삭제 스크립트
        /// </summary>
        private void ParseDelete()
        {
            string value = STZCommon.GetSplitValue(splitArray, 1);
            if (value == "Character")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "DELETE_CHARACTER", "Name", STZCommon.GetSplitValue(splitArray, 2))
                });
            }
            else if (value == "Image")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "DELETE_IMAGE", "Name", STZCommon.GetSplitValue(splitArray, 2))
                });
            }
            else if (value == "Animation")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "DELETE_ANIMATION", "Name", STZCommon.GetSplitValue(splitArray, 2))
                });
            }
            else
            {
                ErrorLog(EErrorType.UnexpectedScript);
            }
        }

        /// <summary>
        /// 카메라 스크립트
        /// </summary>
        private void ParseCamera()
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "BattleScene")
            {
                ErrorLog(EErrorType.SupportBattleScene);
            }
            else if (STZCommon.GetSplitValue(splitArray, 1) == "Target")
            {
                string target = STZCommon.GetSplitValue(splitArray, 2);
                string size = STZCommon.GetSplitValue(splitArray, 3, "640");

                if (target != "Player" && target != "Enemy" && target != "Default")
                {
                    ErrorLog(EErrorType.CameraTargetError);
                }
                else if (size.Contains("-"))
                {
                    ErrorLog(EErrorType.MinusValue);
                }
                else
                {
                    behaviorList.Add(new BehaviorData()
                    {
                        hashTable = STZCommon.Hash("Form", "CAMERA_TARGET", "Target", target, "Size",size)
                    });
                }
            }
            else
            {
                ErrorLog(EErrorType.UnexpectedScript);
            }
        }

        /// <summary>
        /// 시스템 스크립트
        /// </summary>
        private void ParseSystem()
        {
            string value = STZCommon.GetSplitValue(splitArray, 1);
            if (value == "Wait")
            {
                string time = STZCommon.GetSplitValue(splitArray, 2);
                if (time.Contains("-"))
                {
                    ErrorLog(EErrorType.MinusValue);
                }
                else
                {
                    behaviorList.Add(new BehaviorData()
                    {
                        hashTable = STZCommon.Hash("Form", "SYSTEM_WAIT", "Time", STZCommon.GetSplitValue(splitArray, 2))
                    });
                }
            }
            else if (value == "HideUI")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "SYSTEM_HIDE_UI")
                });
            }
            else if (value == "ShowUI")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "SYSTEM_SHOW_UI")
                });
            }
            else if (value == "Event")
            {
                string function = STZCommon.GetSplitValue(splitArray, 2);
                if (function[0] != '@')
                {
                    ErrorLog(EErrorType.GetOutOfEventName);
                }
                else if (!unregistEventList.Contains(function))
                {
                    ErrorLog(EErrorType.UnRegistEvent);
                }
                else
                {
                    behaviorList.Add(new BehaviorData()
                    {
                        hashTable = STZCommon.Hash("Form", "SYSTEM_EVENT", "Function", STZCommon.GetSplitValue(splitArray, 2))
                    });
                }
            }
            else
            {
                ErrorLog(EErrorType.UnexpectedScript);
            }
        }

        /// <summary>
        /// 로드 스크립트
        /// </summary>
        private void ParseLoad()
        {
            string value = STZCommon.GetSplitValue(splitArray, 1);
            if (value == "Character")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "LOAD_CHARACTER", "Name", STZCommon.GetSplitValue(splitArray, 2), "Position", STZCommon.GetSplitValue(splitArray, 3))
                });
            }
            else if (value == "Image")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "LOAD_IMAGE", "Name", STZCommon.GetSplitValue(splitArray, 2), "Position", STZCommon.GetSplitValue(splitArray, 3))
                });
            }
            else if (value == "Animation")
            {
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "LOAD_ANIMATION", "Name", STZCommon.GetSplitValue(splitArray, 2))
                });
            }
            else
            {
                ErrorLog(EErrorType.UnexpectedScript);
            }
        }

        /// <summary>
        /// 액션 스크립트
        /// </summary>
        private void ParseAction()
        {
            string value = STZCommon.GetSplitValue(splitArray, 1);
            if (value == "Screen")
            {
                BehaviorData data = new BehaviorData();

                string action = STZCommon.GetSplitValue(splitArray, 2);
                if (action == "FadeIn")
                {
                    data.hashTable.Add("Form", "ACTION_SCENE_FADEIN");
                }
                else if (action == "FadeOut")
                {
                    data.hashTable.Add("Form", "ACTION_SCENE_FADEOUT");
                }
                else if (action == "Shake")
                {
                    data.hashTable.Add("Form", "ACTION_SCENE_SHAKE");
                }
                else if (action == "Desolve")
                {
                    data.hashTable.Add("Form", "ACTION_DESOLVE");
                    data.hashTable.Add("TargetA", STZCommon.GetSplitValue(splitArray, 3));
                    data.hashTable.Add("TargetB", STZCommon.GetSplitValue(splitArray, 4));
                }
                else
                {
                    ErrorLog(EErrorType.UnexpectedScript);
                    return;
                }

                string time = STZCommon.GetSplitValue(splitArray, splitArray.Length - 1, "1.0");
                if (time.Contains("-"))
                {
                    ErrorLog(EErrorType.MinusValue);
                }
                else
                {
                    data.hashTable.Add("Time", time);
                    behaviorList.Add(data);
                }
            }
            else
            {
                ErrorLog(EErrorType.UnexpectedScript);
            }
        }

        /// <summary>
        /// 대화 스크립트
        /// </summary>
        private bool ParseDialogue()
        {
            bool parse = false;
            if (readLine[0].CompareTo('[') == 0)
            {
                StringBuilder speaker = new StringBuilder();
                for (int i = 0; i < readLine.Length - 1; i++)
                {
                    if (readLine[i] != '[' && readLine[i] != ']')
                    {
                        speaker.Append(readLine[i].ToString());
                    }
                }

                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "DIALOGUE_SPEAKER", "Speaker", speaker.ToString())
                });
                parse = true;
            }
            else if (readLine[0].CompareTo('@') == 0)
            {
                string talk = readLine.Substring(1, readLine.Length - 1);
                behaviorList.Add(new BehaviorData()
                {
                    hashTable = STZCommon.Hash("Form", "DIALOGUE_TALK", "Talk", talk)
                });
                parse = true;
            }
            else if (readLine[0].CompareTo('/') == 0)
            {
                // 주석이라고 판단
                parse = true;
            }
            return parse;
        }
    }
}
