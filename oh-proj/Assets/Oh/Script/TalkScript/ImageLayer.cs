﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TalkScript
{
    public class ImageLayer : MonoBehaviour
    {
        private Dictionary<string, Image> _imageDic = new Dictionary<string, Image>();

        public void AddImage(string name, Vector2 position)
        {
            if (_imageDic.ContainsKey(name))
            {
                _imageDic[name].transform.localPosition = position;
                return;
            }

            Image image = ObjectCreator.it.CreateObj<Image>(this.transform, "Prefabs/TalkScript/Image", position);
            image.sprite = Resources.Load<Sprite>("Texture/TalkScript/Image/" + name);
            image.transform.localScale = new Vector3(0.01f, 0.01f, 1);
            image.name = name;
            image.SetNativeSize();
            _imageDic.Add(name, image);
        }

        public void RemoveImage(string inName)
        {
            if (!_imageDic.ContainsKey(inName))
            {
                return;
            }

            MonoBehaviour.Destroy(_imageDic[inName].gameObject);
            _imageDic.Remove(inName);
        }

        public void Desolve(System.Action callback,string targetA, string targetB,float time)
        {
            if (!_imageDic.ContainsKey(targetA) || !_imageDic.ContainsKey(targetB))
            {
                return;
            }

            _imageDic[targetA].color = STZCommon.ToUnityColor(255, 255, 255,255);
            _imageDic[targetB].color = STZCommon.ToUnityColor(255, 255, 255, 0);

            _imageDic[targetA].DOFade(0, time);
            _imageDic[targetB].DOFade(1, time).OnComplete(() => 
            {
                if (callback != null)
                {
                    callback();
                }
            }); 
        }
    }
}
