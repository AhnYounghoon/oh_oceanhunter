﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    public enum EState
    {
        Zoom,           // 타겟을 기준으로 줌
        Move,           // 타겟을 기준으로 카메라 이동
        Shake,          // 카메라를 흔든다
    }

    public enum EUse
    {
        One_shot,       // 한번만 적용되고 사라짐
        Loop,           // 매 프레임 적용됨
    }

    [SerializeField]
    Camera mainCamera;

    public float cameraOrthosize { get { return mainCamera.orthographicSize; } }

    List<KeyValuePair<EState, EUse>> stateList = new List<KeyValuePair<EState, EUse>>();

    readonly float ORTHOGRAPHIC_SIZE_OFFSET = 200;      // 값 + Offset
    readonly float DEFAULT_ORTHOSIZE        = 120;      // 기초 사이즈
    readonly float DISTANCE_LIMIT           = 50;       // 타겟 A와 B의 한계치
    readonly float SMOOTH_SPEED             = 0.025f;   // lerp 값
    readonly float MAX_ORTHOSIZE            = 120;      // MAX 사이즈 

    Transform   _target;
    int         _zoomSize;

    private void Awake()
    {
        Clear();
    }

    public void Clear()
    {
        //mainCamera.orthographicSize         = DEFAULT_ORTHOSIZE;
        mainCamera.transform.localPosition  = new Vector3(0, 191, -188);
        stateList.Clear();
        _zoomSize = 120;
        _target = null;
    }

    public void SetOrthographic(bool inOrthographic)
    {
        mainCamera.orthographic = inOrthographic;
    }

    public void SetTarget(Transform inTarget)
    {
        _target = inTarget;
    }

    public void SetZoomSize(int inSize)
    {
        _zoomSize = inSize;
    }

    /// <summary>
    /// 카메라 State를 추가한다
    /// StateList를 업데이트하며 실행됨
    /// </summary>
    /// <param name="inState">행동 패턴</param>
    /// <param name="inUse">사용 후 State 처리</param>
    public void AddState(EState inState, EUse inUse)
    {
        KeyValuePair<EState, EUse> value = new KeyValuePair<EState, EUse>(inState, inUse);
        if (stateList.Contains(value))
        {
            Debug.LogError(StringHelper.Format("{0} is already exist!", inState.ToString()));
            return;
        }
        stateList.Add(value);
    }

    private void FixedUpdate()
    {
        if (stateList == null)
        {
            return;
        }

        for (int i = 0; i < stateList.Count;i++)
        {
            var state = stateList[i];
            if (state.Key == EState.Move)
            {
                Move();
            }
            else if (state.Key == EState.Zoom)
            {
                Zoom();
            }
            else if (state.Key == EState.Shake)
            {
                Shake();
            }

            if (state.Value == EUse.One_shot)
            {
                stateList.Remove(state);
            }
        }
    }

    /// <summary>
    /// 카메라 쉐이크
    /// </summary>
    private void Shake()
    {
        mainCamera.DOShakePosition(1,7);
    }

    /// <summary>
    /// 타겟을 기준으로 확대
    /// <!-- 타깃 A,B가 모두 존재할 때는 타겟의 중심을 기준으로 확대한다 -->
    /// </summary>
    private void Zoom()
    {
        if (_target != null)
        {
            float targetValue = _zoomSize;
            mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, targetValue, SMOOTH_SPEED);
        }
        else
        {
            mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 120, SMOOTH_SPEED);
        }
    }

    /// <summary>
    /// 타겟을 기준으로 이동
    /// <!-- 타깃 A,B가 모두 존재할 때는 타겟의 중심을 기준으로 이동한다 -->
    /// </summary>
    private void Move()
    {
        if (_target != null)
        {
            Vector3 targetPosition;
            if (mainCamera.orthographic)
            {
                targetPosition = new Vector3(_target.position.x, mainCamera.transform.localPosition.y, mainCamera.transform.localPosition.z);
                Vector3 smoothPosition = Vector3.Lerp(mainCamera.transform.position, targetPosition, SMOOTH_SPEED);
                mainCamera.transform.position = smoothPosition;
            }
            else
            {
                targetPosition = new Vector3(_target.position.x + 60, _target.position.y + 40, _target.position.z - 120);
                Vector3 smoothPosition = Vector3.Lerp(mainCamera.transform.position, targetPosition, SMOOTH_SPEED);
                mainCamera.transform.position = smoothPosition;
                mainCamera.transform.LookAt(_target.transform);
            }
        }
        else
        {
            Vector3 targetPosition = new Vector3(0,191, -188);
            Vector3 smoothPosition = Vector3.Lerp(mainCamera.transform.position, targetPosition, SMOOTH_SPEED);
            mainCamera.transform.position = smoothPosition;
            mainCamera.transform.localRotation = Quaternion.Euler(45, 0, 0);
        }
    }
}
