﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BossUI : MonoBehaviour
{
    [SerializeField]
    Image _imgBossThumnail;

    [SerializeField]
    Text _txtBossName;

    [SerializeField]
    HpBar _hpBar;

    ServerStatic.BattleObject data;
    bool work = true;

    public void Init(ServerStatic.BattleObject inData)
    {
        this.gameObject.SetActive(true);

        CanvasGroup group = this.GetComponent<CanvasGroup>();
        group.alpha = 0;
        group.DOFade(1.0f, 0.5f);

        data = inData;
        _txtBossName.text = inData.name;
        _hpBar.SetHp(inData.hp, inData.hp);

        _imgBossThumnail.sprite = Resources.Load<Sprite>(StringHelper.Format("Texture/Monster/{0}", inData.id));
    }

    public void Clear()
    {

    }

    public void ChangeHpEvent(float inBefore, float inAfter, float inMax)
    {
        if (!work)
            return;

        if (inAfter <= 0)
        {
            work = false;
            CanvasGroup group = this.GetComponent<CanvasGroup>();
            group.alpha = 1;
            group.DOFade(0, 0.5f);
        }
        else
        {
            _hpBar.SetHp(inAfter, inMax);
        }
    }
}
