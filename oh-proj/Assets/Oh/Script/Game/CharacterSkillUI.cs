﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CharacterSkillUI : MonoBehaviour
{
    [SerializeField]
    Image _background;

    [SerializeField]
    Image _character;

    [SerializeField]
    Text _skillText;

    public void Active(ServerStatic.Skill inSkill, ServerStatic.Character inCharacter)
    {
        _character.sprite = Resources.Load<Sprite>(inCharacter.illust_path);
        Time.timeScale = 1.0f;

        _skillText.text = inSkill.name;
        _skillText.color  = STZCommon.ToUnityColor(255, 255, 255, 0);
        _character.color  = STZCommon.ToUnityColor(255, 255, 255, 0);
        _background.color = STZCommon.ToUnityColor(0, 0, 0, 0);

        _skillText.DOFade(1, 0.5f);
        _background.DOFade(0.6f, 0.5f);
        _character.DOFade(1, 1.0f);
        _character.transform.SetPositionX(890);
        _character.transform.DOLocalMoveX(-258, 1.5f).OnComplete(() =>
        {
            Time.timeScale = 1.0f;
            _character.DOFade(0, 0.5f);
            _background.DOFade(0, 0.5f);
            _skillText.DOFade(0, 0.5f);
            _character.transform.DOLocalMoveX(-800, 0.5f).OnComplete(() =>
            {
                Time.timeScale = 1.0f;
            });
        });
    }
}
