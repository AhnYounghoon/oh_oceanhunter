﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// Tweener를 Inspector상에서 제어해 애니메이션 할 수 있는 스크립트
/// </summary>
public class CustomAnimation : MonoBehaviour
{
    public enum EAnimation
    {
        Move,
        Rotate,
        Scale,
        Alpha,
    }

    [System.Serializable]
    public class AnimationGroup
    {
        public string key = string.Empty;
        public bool loop = false;
        public bool autoDestory = false;
        public List<AnimationList> animationList = new List<AnimationList>();

        [System.Serializable]
        public class AnimationList
        {
            // 해당 애니메이션은 시퀀스 형태로 진행된다
            public List<Animation> animationList = new List<Animation>();

            [System.Serializable]
            public class Animation
            {
                public EAnimation animation = EAnimation.Move;
                public Ease ease = Ease.Unset;
                public Vector3 value1 = default(Vector3);
                public float value2 = 0;
                public float time = 1;
                public float delay = 0;
            }
        }
    }

    [Tooltip("Awake시 애니메이션")]
    [SerializeField]
    string _startAnimationKey = string.Empty;

    [Tooltip("Awake 애니메이션 루프 여부")]
    [SerializeField]
    bool _startAnimationLoop = false;

    [SerializeField]
    List<AnimationGroup> _animationGroupList = new List<AnimationGroup>();

    System.Action  _callback;
    AnimationGroup _currentPlayAnimation;
    Sequence       _currentSequence;
    int            _currentPlayIdx;

    private void Awake()
    {
        if (!_startAnimationKey.IsNullOrEmpty())
        {
            Play(_startAnimationKey, _startAnimationLoop);
        }
    }

    /// <summary>
    /// 애니메이션을 재생
    /// </summary>
    /// <param name="inAnimationKey"></param>
    public void Play(string inAnimationKey, bool inLoop = false, System.Action inCallback = null, bool inAutoDestory = false)
    {
        bool find = false;

        // _currentPlayAnimation = _animationGroupList.Where(x => x.key == inAnimationKey).FirstOrDefault();
        // NOTE @minjun.ha for문이 쿼리문 보다 빠를 것 같아 변경
        for (int i = 0; i < _animationGroupList.Count;i++)
        {
            if (_animationGroupList[i].key == inAnimationKey)
            {
                find = true;
                _currentPlayAnimation = _animationGroupList[i];
                break;
            }
        }

        if (!find)
        {
            return;
        }
        else
        {
            _currentPlayIdx                   = 0;
            _currentSequence                  = null;
            _callback                         = inCallback;
            _currentPlayAnimation.loop        = inLoop;
            _currentPlayAnimation.autoDestory = inAutoDestory;
            PlayAnimation();
        }
    }

    /// <summary>
    /// 애니메이션 정지
    /// </summary>
    public void Stop()
    {
        if (_currentSequence != null && _currentSequence.IsActive())
        {
            _currentSequence.Kill();
        }
    }

    private void PlayAnimation()
    {
        // 플레이 도중 오브젝트가 삭제되었을 수 있기 때문에 처리
        if (this == null || this.gameObject == null)
        {
            return;
        }

        // 애니메이션 끝 도달
        if (_currentPlayAnimation.animationList.Count <= _currentPlayIdx)
        {
            if (_callback != null)
            {
                _callback();
            }
            if (_currentPlayAnimation.loop)
            {
                _currentPlayIdx = 0;
                _currentSequence.Kill();
            }
            else
            {
                if (_currentPlayAnimation.autoDestory)
                {
                    MonoBehaviour.Destroy(this.gameObject);
                }
                return;
            }
        }

        // 지정된 시퀀스대로 애니메이션을 실행
        AnimationGroup.AnimationList sequence = _currentPlayAnimation.animationList.ElementAtOrDefault(_currentPlayIdx);
        if (sequence != null)
        {
            _currentPlayIdx++;
            _currentSequence = PlayNextSequenceAnimation(_currentPlayAnimation.key, sequence);
        }
    }

    /// <summary>
    /// 한번에 실행되야 할 애니메이션을 추가/실행 한다
    /// </summary>
    /// <param name="inAnimationList"></param>
    private Sequence PlayNextSequenceAnimation(string inAnimationKey, AnimationGroup.AnimationList inAnimationList)
    {
        Sequence sequence = DOTween.Sequence();
        float time = 0;
        foreach (var animation in inAnimationList.animationList)
        {
            sequence.SetAs(GetAnimation(animation));
            time = animation.delay + animation.time;
        }
        sequence.SetDelay(time);
        return sequence.AppendCallback(PlayAnimation);
    }

    private Tweener GetAnimation(AnimationGroup.AnimationList.Animation inAnimation)
    {
        Tweener tweener = null;
        switch (inAnimation.animation)
        {
            case EAnimation.Move:
                tweener = DoMove(inAnimation.value1, inAnimation.ease, inAnimation.time, inAnimation.delay);
                break;
            case EAnimation.Rotate:
                tweener = DoRotate(inAnimation.value1, inAnimation.ease, inAnimation.time, inAnimation.delay);
                break;
            case EAnimation.Scale:
                tweener = DoScale(inAnimation.value1, inAnimation.ease, inAnimation.time, inAnimation.delay);
                break;
            case EAnimation.Alpha:
                tweener = DoFade(inAnimation.value2, inAnimation.ease, inAnimation.time, inAnimation.delay);
                break;
            default:
                break;
        }
        return tweener;
    }

    private Tweener DoMove(Vector3 value, Ease ease, float time, float delay)
    {
        return this.transform.DOLocalMove(value, time)
            .SetDelay(delay)
            .SetEase(ease);
    }

    private Tweener DoRotate(Vector3 value, Ease ease, float time, float delay)
    {
        return this.transform.DOLocalRotate(value, time)
            .SetDelay(delay)
            .SetEase(ease);
    }

    private Tweener DoScale(Vector3 value, Ease ease, float time, float delay)
    {
        return this.transform.DOScale(value, time)
            .SetDelay(delay)
            .SetEase(ease);
    }

    private Tweener DoFade(float value, Ease ease, float time, float delay)
    {
        Tweener tweener = null;
        if (this.GetComponent<SpriteRenderer>())
        {
            SpriteRenderer renderer = this.GetComponent<SpriteRenderer>();
            tweener = renderer.DOFade(value, time);
        }
        else if (this.GetComponent<Image>())
        {
            CanvasGroup cg = this.GetComponent<CanvasGroup>();
            if (cg != null)
            {
                tweener = cg.DOFade(value, time);
            }
            else
            {
                Image image = this.GetComponent<Image>();
                tweener = image.DOFade(value, time);
            }
        }
        else
        {
            CanvasGroup cg = this.GetComponent<CanvasGroup>();
            if (cg != null)
            {
                tweener = cg.DOFade(value, time);
            }
        }
        return tweener
            .SetDelay(delay)
            .SetEase(ease);
    }

#if UNITY_EDITOR
    [ContextMenu("Stop Animation")]
    public void EditorStopAnimation()
    {
        if (Application.isPlaying)
        {
            Stop();
        }
    }
#endif
}
