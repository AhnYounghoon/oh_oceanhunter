﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CustomCollider : MonoBehaviour
{
    public enum EDebugMode
    {
        None,
        Circle,
        Line,
    }

    public System.Action<Collider> enterCallback { set { _enterCallback = value; } }
    public System.Action<Collider> stayCallback  { set { _stayCallback = value; } }
    public System.Action<Collider> exitCallback  { set { _exitCallback = value; } }
    public string TargetTag { set { _targetTag = value; } }

    System.Action<Collider> _enterCallback;
    System.Action<Collider> _stayCallback;
    System.Action<Collider> _exitCallback;

    Collider    _collider;
    string      _targetTag;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    public void SetSize(float inSize, Vector3 inOffset = default(Vector3))
    {
        if (_collider is SphereCollider)
        {
            (_collider as SphereCollider).radius = inSize;
            (_collider as SphereCollider).center = inOffset;
        }
        else if (_collider is BoxCollider)
        {
            (_collider as BoxCollider).size = new Vector3(inSize, inSize, inSize);
            (_collider as BoxCollider).center = inOffset;
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (_targetTag.IsNullOrEmpty() || collision.CompareTag(_targetTag))
        {
            if (_enterCallback != null)
            {
                _enterCallback(collision);
            }
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (_targetTag.IsNullOrEmpty() || collision.CompareTag(_targetTag))
        {
            if (_stayCallback != null)
            {
                _stayCallback(collision);
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (_targetTag.IsNullOrEmpty() || collision.CompareTag(_targetTag))
        {
            if (_exitCallback != null)
            {
                _exitCallback(collision);
            }
        }
    }
}
