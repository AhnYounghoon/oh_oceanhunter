﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class CustomParticle : MonoBehaviour
{
    [SerializeField]
    bool _autoDestroy = false;

    private ParticleSystem _particle;
    private System.Action  _callback;

    private void Awake()
    {
        _particle = this.GetComponent<ParticleSystem>();
    }

    private void Start () {
        this.UpdateAsObservable()
            .Select(_ => !_particle.isPlaying)
            .DistinctUntilChanged()
            .Where(x => x)
            .Subscribe(_ => 
            {
                if (_callback != null)
                {
                    _callback();
                }

                if (_autoDestroy)
                    MonoBehaviour.Destroy(this.gameObject);
            });
    }

    public void SetAutoDestroy(bool inAutoDestroy)
    {
        _autoDestroy = inAutoDestroy;
    }

    public void RegistEndCallback(System.Action inCallback)
    {
        _callback = inCallback;
    }
}
