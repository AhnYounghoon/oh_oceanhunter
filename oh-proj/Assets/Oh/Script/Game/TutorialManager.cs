﻿using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    static TutorialManager _instance = null;
    public static TutorialManager it
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject();
                go.name = typeof(TutorialManager).Name;
                _instance = go.AddComponent<TutorialManager>();
            }

            return _instance;
        }
    }

    public bool Active
    {
        get
        {
            return _talkScript != null;
        }
    }

    System.Action   _endCallback;
    string          _tutorialPath;
    Talk            _talkScript;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Clear()
    {
        _endCallback = null;
        if (_talkScript != null)
        {
            MonoBehaviour.Destroy(_talkScript.gameObject);
        }
    }

    public void SkipTutorial()
    {
        if (Active)
        {
            if (_endCallback != null)
            {
                _endCallback();
            }
            Clear();
        }
    }

    /// <summary>
    /// 튜토리얼 로드
    /// </summary>
    /// <param name="inPath"></param>
    /// <param name="inParent">파서 상위 오브젝트</param>
    public void SetTutorial(string inPath, Transform inParent = null)
    {
        Clear();

        _tutorialPath = inPath;
        _talkScript = ObjectCreator.it.CreateObj<Talk>(this.transform, "Prefabs/TalkScript/TalkScript");
        SetParent(inParent);
    }

    /// <summary>
    /// 스크립트를 파싱하고 튜토리얼 시작
    /// </summary>
    /// <param name="inClearCallback">스크립트 종료 콜백</param>
    public void StartTutorial(System.Action inClearCallback = null)
    {
        if (Active)
        {
            _endCallback = inClearCallback;
            if (!_talkScript.Parse(_tutorialPath, inClearCallback))
            {
                Debug.LogError(StringHelper.Format("{0} Parsing Error !!", _tutorialPath));
                MonoBehaviour.Destroy(_talkScript.gameObject);
            }
        }
    }

    /// <summary>
    /// Script 파일 이벤트 처리 함수를 등록
    /// <see cref="#System Event (Function)"/>
    /// </summary>
    /// <param name="inKey">ex) @HelloWorld</param>
    /// <param name="inCallback">해당 콜백을 호출해줘야 다음 스크립트로 넘어간다</param>
    public void AddEvent(string inKey, Talk.TalkEventHandler inCallback)
    {
        if (!Active)
        {
            return;
        }
        _talkScript.AddEventFunction(inKey, inCallback);
    }

    /// <summary>
    /// 스크립트 파싱 오브젝트를 GameUI Child로 등록
    /// NOTE @minjun.ha 관련 Order가 꼬이는 부분이 있어 상태 초기화
    /// </summary>
    /// <param name="inGameUI"></param>
    private void SetParent(Transform inParent)
    {
        _instance.transform.SetParent(inParent);
        _instance.transform.localPosition   = default(Vector3);
        _instance.transform.localScale      = default(Vector3);

        _talkScript.transform.localPosition  = default(Vector3);
        _talkScript.transform.localScale     = new Vector3(1, 1, 1);

        _instance.transform.localPosition   = default(Vector3);
        _instance.transform.localScale      = new Vector3(1, 1, 1);
    }
}
