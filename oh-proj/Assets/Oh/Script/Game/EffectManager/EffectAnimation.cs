﻿using UnityEngine;
using Spine.Unity;

[RequireComponent(typeof(Animator))]
/// <summary>
/// 이펙트 애니메이션을 관리하기 위해 이펙트 프리펩에 적용해야 하는 클래스.
/// </summary>
public class EffectAnimation : STZBehaviour
{
    /// <summary>
    /// 레이져팡 스파인이 한바퀴도는데 5.5초로 제작되어 있음 
    /// 60프레임 기준 1프레임당 360도 / 5.5f 만큼 회전함
    /// </summary>
    private readonly float ROTATION_PER_FRAME = 65.4545f;
    
    // 현재 루프 횟수.
    private int _loopCount = 0;
    // 현재 애니메이션 스테이트 이름.
    private string _stateName;
    // 애니메이션을 관리하는 애니메이터.
    private Animator _animator;

    // 최종 루프 횟수.
    public int maxLoopCount { get; set; }
    public string prefabPath { get; set; }
    // 데이터 캐싱 유무.
    public bool isRemovingCashingData { get; set; }
    public bool isRotate { get; set; }
    public bool isMain { get; set; }
    public float zRotation { get; set; }
    public bool isPlay { get; set; }
    // 이펙트가 종료될 때 호출할 콜백.
    public EffectManager.Callback callback { get; set; }
    public EffectManager.OnEvent onEvent { get; set; }
    public bool autoDestroy { get; set; }

    /// <summary>
    /// 초기화.
    /// </summary>
    public void Awake()
    {
        _animator = gameObject.GetComponent<Animator>();
        _animator.StopPlayback();
        callback = null;
        onEvent = null;

        autoDestroy = false;
    }

    public void Clear()
    {
        enabled = false;
        isPlay = false;
        zRotation = 0;
        isMain = false;
        this.transform.rotation = Quaternion.Euler(0, 0, 0);

        // NOTE scale을 무조건 1로 하면 스케일이 1이 아닌 리소스들의 재사용시 문제가 됨
        //this.transform.localScale = Vector2.one;
        Vector2 scale = this.transform.localScale;
        if (scale.x < 0)
            scale.x *= -1;
        if (scale.y < 0)
            scale.y *= -1;
        this.transform.localScale = scale;
    }

    public void SetSpineData(string animation_skinName)
    {
        SetSpineData(animation_skinName, false);
    }

    public void SetSpineData(string animation_skinName, bool loop)
    {
        string[] str = animation_skinName.Split('@');
        SkeletonAnimation sa = GetComponent<SkeletonAnimation>();
        sa.skeleton.SetSkin(str[1]);
        sa.initialSkinName = str[1];
        sa.skeleton.Update(1);
        sa.skeleton.UpdateWorldTransform();
        sa.LateUpdate();
        sa.Play(str[0], false);
    }

    public void SetSpineAnimation(string animationName)
    {
        SkeletonAnimation sa = GetComponent<SkeletonAnimation>();
        sa.Play(animationName, false);
    }

    /// <summary>
    /// 애니메이션 스테이트 설정.
    /// </summary>
    /// <param name="stateName">애니메이션 스테이트 이름.</param>
    public void SetStateName(string stateName)
    {
        isPlay = true;
        gameObject.SetActive(true);
        enabled = true;

        _stateName = stateName;
        _animator.Play(_stateName);
    }

    /// <summary>
    /// 애니메이션이 1회 끝났을 때 호출.
    /// 애니메이션 데이터의 마지막에 꼭 "EndFrame"이라는 이벤트 테그를 달아 주어야 한다.
    /// </summary>
    public void EndFrame()
    {
        if (maxLoopCount == -1)
        {
            return;
        }

        _loopCount++;

        if (_loopCount >= maxLoopCount)
        {
            if (autoDestroy)
                DestroyEffect();
            else
            {
                //gameObject.SetActive(false);
                Clear();
                EffectManager.it.ReturnPool(this);
            }
        }
    }

    /// <summary>
    /// 애니메이션중 특정 프레임에서의 이벤트 발생을 처리하고 싶을때 해당 태그를 달아주면 된다
    /// </summary>
    public void OnTriggerEvent()
    {
        if (onEvent != null)
        {
            onEvent();
        }
    }

    /// <summary>
    /// 이펙트 파괴.
    /// </summary>
    public void DestroyEffect()
    {
        EffectManager.it.DestroyEffect(this);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="soundKey">Sound key.</param>
    public void PlaySound(string soundKey)
    {
        //STZFramework.STZDebug.Log("[SOUND]" + soundKey);
        string[] values = soundKey.Split(',');

        for (int i = 0; i < values.Length; i++)
        {
            string[] ids = values[i].Split('@');
            if (ids.Length == 1)
            {
                SingletonController.Get<SoundManager>().Play(ids[0]);
            }
            else if (ids.Length == 2)
            {
                SingletonController.Get<SoundManager>().Play(ids[0], ids[1]);
            }
        }
    }
}