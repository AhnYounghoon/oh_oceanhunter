﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 이펙트를 즉각적으로 생성하고 파괴하는 것을 관리하기 위한 클래스
/// </summary>
public class EffectManager
{
    private static EffectManager _instance = null;
    public static EffectManager it { get { if (_instance == null) _instance = new EffectManager(); return _instance; } }

    public delegate void Callback();    // 이펙트 종료시 콜백을 위한 델리게이트.
    public delegate void OnEvent();    // 이펙트 이벤트 발생을 위한 델리게이트.

    private List<EffectAnimation> _effectList = new List<EffectAnimation>();       // 재생 중인 이펙트가 저장되어 있는 리스트.

    /// <summary>
    /// 이펙트 생성.
    /// </summary>
    /// <param name="callback">이펙트가 파괴될 때 받을 콜백.</param>
    /// <param name="prefabPath">이펙트 프리펩 위치.</param>
    /// <param name="parent">이펙트를 설치할 부모.</param>
    /// <param name="position">이펙트 위치.</param>
    /// <param name="stateName">애니메이션 스테이트 이름.</param>
    /// <param name="loopCount">반복 횟수(-1이면 무한대).</param>
    public void Create(Callback callback, string prefabPath, Transform parent, Vector3 position, string stateName = "Play", 
        int loopCount = 1, bool isXFlip = false/*bool isRemovingCashingData = false*/, int inRotateDegree = -1, OnEvent onEvent = null)
    {
        bool ret = GetFromPool(callback, prefabPath, parent, position, stateName, isXFlip, inRotateDegree, onEvent);

        if (!ret)
            CreateEffect(callback, prefabPath, parent, position, stateName, loopCount, isXFlip, inRotateDegree, onEvent);
    }

    private bool GetFromPool(Callback callback, string prefabPath, Transform parent,
        Vector3 position, string stateName, bool isXFlip, int inRotateDegree, OnEvent onEvent)
    {
        for (int i = 0; i < _effectList.Count; i++)
        {
            if (!_effectList[i].isPlay && _effectList[i].prefabPath == prefabPath)
            {
                if (stateName.IndexOf("(main)") >= 0)
                {
                    stateName = stateName.Replace("(main)", "");
                    _effectList[i].isMain = true;
                }

                _effectList[i].SetStateName(stateName);
                _effectList[i].transform.localPosition = position;
                _effectList[i].callback = callback;
                _effectList[i].onEvent = onEvent;
                _effectList[i].isRotate = inRotateDegree > -1 ? true : false;
                if (inRotateDegree > -1)
                {
                    //if(inRotateDegree == 0) _effectList[i].isMain = true;
                    _effectList[i].transform.localRotation = Quaternion.Euler(0, 0, inRotateDegree);
                    _effectList[i].zRotation = _effectList[i].transform.localEulerAngles.z;
                    _effectList[i].enabled = true;
                }
                if (isXFlip)
                    _effectList[i].transform.localScale = new Vector2(-1, 1);
                return true;
            }
        }

        return false;
    }

    public void ReturnPool(EffectAnimation effectAnimation)
    {
        if (effectAnimation.callback != null)
        {
            effectAnimation.callback();
        }

        //effectAnimation.isPlay = false;
        effectAnimation.Clear();
        effectAnimation.gameObject.SetActive(false);
        //_effectList.Add(effectAnimation);
    }

    public EffectAnimation CreateEffect(Callback callback, string prefabPath, Transform parent, Vector3 position, 
        string stateName, int loopCount = 1, bool isXFlip = false/*bool isRemovingCashingData = false*/, int inRotateDegree = -1, OnEvent onEvent = null)
    {
        EffectAnimation effectAnimation = null;

        effectAnimation = (MonoBehaviour.Instantiate(Resources.Load(prefabPath)) as GameObject).GetComponent<EffectAnimation>();
        effectAnimation.transform.localPosition = position;
        effectAnimation.maxLoopCount = loopCount;
        effectAnimation.prefabPath = prefabPath;
        effectAnimation.callback = callback;
        effectAnimation.onEvent = onEvent;
        //effectAnimation.isRemovingCashingData = isRemovingCashingData;
        if (stateName.IndexOf("(main)") >= 0)
        {
            stateName = stateName.Replace("(main)", "");
            effectAnimation.isMain = true;
        }
        effectAnimation.SetStateName(stateName);
        effectAnimation.transform.SetParent(parent, false);
        effectAnimation.isRotate = inRotateDegree > -1 ? true : false;

        if (inRotateDegree > -1)
        {
            //if (inRotateDegree == 0) effectAnimation.isMain = true;
            effectAnimation.transform.localRotation = Quaternion.Euler(0, 0, inRotateDegree);
            effectAnimation.zRotation = effectAnimation.transform.localEulerAngles.z;
            effectAnimation.enabled = true;
        }

        if (isXFlip)
            effectAnimation.transform.localScale = new Vector2(-1, 1);

        _effectList.Add(effectAnimation);

        return effectAnimation;
    }

    /// <summary>
    /// 해당 프리팹에 이펙트를 모두 풀로 반환
    /// </summary>
    /// <param name="prefabPath"></param>
    public void DestroyEffects(string prefabPath)
    {
        if (_effectList == null)
            return;

        for (int i = 0; i < _effectList.Count; i++)
        {
            if (_effectList[i] == null)
            {
                _effectList.RemoveAt(i);
                i--;
                continue;
            }

            if (_effectList[i].prefabPath == prefabPath)
            {
                ReturnPool(_effectList[i]);
            }
        }
    }

    /// <summary>
    /// 이펙트 파괴.
    /// </summary>
    /// <param name="effectAnimation">현재 재생이 완료된 이펙트.</param>
    /// <param name="removingCashData">값이 <c>true</c> 면 캐싱 데이터 제거.</param>
    public void DestroyEffect(EffectAnimation effectAnimation)
    {
        if (effectAnimation == null)
        {
            _effectList.Remove(effectAnimation);
            return;
        }

        if (effectAnimation.callback != null)
        {
            effectAnimation.callback = null;
        }

        if (effectAnimation.onEvent != null)
        {
            effectAnimation.onEvent = null;
        }

        if (effectAnimation.isRemovingCashingData)
        {
            Resources.UnloadAsset(effectAnimation.gameObject);
        }

        _effectList.Remove(effectAnimation);

        if (effectAnimation.gameObject != null)
            MonoBehaviour.Destroy(effectAnimation.gameObject);
    }

    /// <summary>
    /// 모든 이펙트 및 캐싱 데이터 파괴.
    /// </summary>
    /// <param name="removingCashingData">값이 <c>true</c> 면 캐싱 데이터 제거.</param>
    public void DestroyAll(bool isRemovingCashingData = false)
    {
        while (_effectList.Count > 0)
        {
            if (isRemovingCashingData)
            {
                Resources.UnloadAsset(_effectList[0].gameObject);
            }

            DestroyEffect(_effectList[0]);
        }

        _effectList.Clear();
    }

    public void AddEffect(EffectAnimation inEffectAnimation)
    {
        _effectList.Add(inEffectAnimation);
    }
}