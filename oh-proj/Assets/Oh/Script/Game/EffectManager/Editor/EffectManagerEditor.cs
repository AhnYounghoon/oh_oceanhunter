using UnityEngine;
using UnityEditor;
using System.IO;

[InitializeOnLoad]
public class EffectManagerEditor
{
	[MenuItem("Assets/Effect Manager/Convert to Spine Effect")]
	static void ConvertToSpineEffect() {

        GameObject anim = Selection.activeGameObject;

        if (anim == null)
        {
            EditorUtility.DisplayDialog("변경 실패", "파일이 제대로 선택되었는지 다시 한 번 확인해 주세요.", "확인");
            return;
        }

		if (anim.GetComponent<Animator>())
		{
			if (!anim.GetComponent<EffectAnimation>())
			{
				anim.AddComponent<EffectAnimation>();
			}
		}

		TurnOffShadowOptions(anim);

		string path = AssetDatabase.GetAssetPath(anim.GetInstanceID());

		EditorUtility.DisplayDialog("변경 완료", "\"" + path + "\" 파일이 이펙트 파일로 변경되었습니다.", "확인");
	}

	static void TurnOffShadowOptions(GameObject g)
	{
		MeshRenderer mr = g.GetComponent<MeshRenderer>();

		if (mr != null)
		{
			mr.receiveShadows = false;
			mr.castShadows = false;
		}

		foreach (Transform t in g.transform)
		{
			TurnOffShadowOptions(t.gameObject);
		}
	}

	[MenuItem("Assets/Effect Manager/Create New Unity Effect Dummy")]
	static void CreateUnityEffectDummy() 
	{
		Object obj = Selection.activeObject;

        if (obj == null)
        {
            EditorUtility.DisplayDialog("생성 실패", "폴더가 제대로 선택 되었는지 다시 한 번 확인해 주세요.", "확인");
            return;
        }

		bool isCreation = false;

        if (obj.GetType() == typeof(Object))
		{
            string path = AssetDatabase.GetAssetPath(obj.GetInstanceID());

			string folderName ="EffectDummy";
			string folderPath = path + "/" + folderName;

			folderPath = AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder(path, folderName));

			GameObject anim = new GameObject("TempPrefab");
			anim.AddComponent<Animator>();
			anim.AddComponent<EffectAnimation>();
			anim.AddComponent<SpriteRenderer>();

			string ctrlPath = folderPath + "/" + "TempControl" + ".controller";
			UnityEditor.Animations.AnimatorController ctrl = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(ctrlPath);
			anim.GetComponent<Animator>().runtimeAnimatorController = ctrl;

			AnimationClip clip = new AnimationClip();
			clip.name = "TempAnimation";
			clip.wrapMode = WrapMode.Loop;
#if !UNITY_5
			AnimationUtility.SetAnimationType(clip, ModelImporterAnimationType.Generic);
#endif

			AssetDatabase.CreateAsset(clip, folderPath + "/" + "Effect" + ".anim");
			AssetDatabase.SaveAssets();

			AnimationUtility.SetAnimationEvents(clip, new AnimationEvent[0]);

#if !UNITY_5
			UnityEditor.Animations.AnimatorController.AddAnimationClipToController(ctrl, clip);
#endif

			PrefabUtility.CreatePrefab(folderPath + "/" + anim.name + ".prefab", anim);
			MonoBehaviour.DestroyImmediate(anim);

			isCreation = true;

			EditorUtility.DisplayDialog("생성 완료", "\"" + folderPath + "\"  경로에 이펙트 더미가 생성되었습니다.", "확인");
		}

		if (!isCreation)
			EditorUtility.DisplayDialog("생성 실패", "폴더가 제대로 선택 되었는지 다시 한 번 확인해 주세요.", "확인");
	}
}
