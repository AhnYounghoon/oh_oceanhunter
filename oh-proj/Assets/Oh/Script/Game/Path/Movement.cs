﻿using DG.Tweening;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Movement
{
    public float speed { get; set; }
    public bool stop { get { return _movePositionList == null; } }
    public bool auto { get; set; }

    List<Vector3>  _movePositionList;
    Vector3        _moveNormalizeVector;
    PathFinder     _pathFinder;
    Vector3        _pastPosition;

    GameObject  _moveTarget;
    GameObject  _rotateTarget;
    int         _currentMoveIdx;
    float       _moveDelay;

    public Movement(GameObject inMoveTarget,GameObject inRotateTarget, float inSpeed, PathFinder inPathFinder)
    {
        auto = false;

        speed           = inSpeed;
        _moveTarget     = inMoveTarget;
        _rotateTarget   = inRotateTarget;
        _pathFinder     = inPathFinder;
    }

    public void UpdateMove()
    {
        if (_moveDelay > 0)
        {
            _moveDelay -= Time.fixedDeltaTime;
        }
        if (CalculationMove())
        {
            Vector3 tar = _moveTarget.transform.position;
            tar.x += _moveNormalizeVector.x * speed;
            tar.z += _moveNormalizeVector.z * speed;

            _moveTarget.transform.position = new Vector3(tar.x, tar.y, tar.z);
        }
    }

    /// <summary>
    /// 이동을 멈춤
    /// </summary>
    public void StopMove()
    {
        auto = false;
        _movePositionList = null;
        _currentMoveIdx   = 0;
        _pathFinder.RemoveReachPath();
    }

    public void RemovePastPath()
    {
        _pastPosition = Vector3.zero;
    }

    /// <summary>
    /// 목적지 까지 길 경로를 갱신
    /// </summary>
    public void RefreshPath()
    {
        if (_pastPosition == Vector3.zero || _moveDelay > 0)
        {
            return;
        }
        bool pastAuto = auto;
        StopMove();
        auto = pastAuto;
        _movePositionList = _pathFinder.FindPath(_moveTarget.transform.position, _pastPosition);

        if (CalculationMove() && _movePositionList.Count > 0)
        {
            Look(_moveTarget.transform.position, _movePositionList.FirstOrDefault());
        }
        else
        {
            _moveDelay = 0.3f;
        }
    }

    /// <summary>
    /// 해당 지점으로 이동
    /// </summary>
    /// <param name="inPosition"></param>
    public void Move(Vector3 inPosition, bool inAuto = false)
    {
        if (inAuto && _moveDelay > 0)
        {
            return;
        }

        auto            = inAuto;
        _pastPosition   = inPosition;
        _currentMoveIdx = 0;

        _movePositionList = _pathFinder.FindPath(_moveTarget.transform.position, inPosition);
        if (_movePositionList == null)
        {
            // TODO @minjun.ha 길을 찾지 못한 경우 오브젝트가 서로 겹쳐진 경우 ....
            _moveDelay = 1.0f;
        }
        else
        {
            if (CalculationMove() && _movePositionList.Count > 0)
            {
                Look(_moveTarget.transform.position, _movePositionList.FirstOrDefault());
            }
            else
            {
                _moveDelay = 1.0f;
            }
        }
    }

    /// <summary>
    /// 다음 목적지의 방향과 거리를 구함
    /// <returns>이동 불가시 반환</returns>
    /// </summary>
    private bool CalculationMove()
    {
        if (_movePositionList == null || _movePositionList.Count <= _currentMoveIdx)
        {
            StopMove();
            _pastPosition = Vector3.zero;
            Quaternion last = _rotateTarget.transform.localRotation;
            _rotateTarget.transform.DOKill();
            _rotateTarget.transform.localRotation = last;
            return false;
        }

        Vector3 from = _moveTarget.transform.position;
        Vector3 to   = _movePositionList[_currentMoveIdx];
        to.y = 0;
        from.y = 0;

        var walkDistance = to - from;
        var distance = walkDistance.magnitude;
        if (distance != 0)
        {
            // 벡터를 정규와 시킴
            _moveNormalizeVector = walkDistance / distance;
        }

        // 가까운 위치에 있는 거리는 타깃으로 잡지 않음
        if ((int)distance <= 5f)
        {
            _currentMoveIdx++;
        }
        Look(from, to);
        return true;
    }

    public void Look(Vector3 inTo)
    {
        Look(_moveTarget.transform.position, inTo);
    }

    public void LookDefault()
    {
        _rotateTarget.transform.DORotate(Vector3.zero, 1.0f);
    }

    /// <summary>
    /// 해당 위치를 바라봄
    /// TODO @minjun.ha 성능상의 이슈가 있어 삼각함수를 벡터 내적으로 변경해야함
    /// </summary>
    /// <param name="inFrom"></param>
    /// <param name="inTo"></param>
    private void Look(Vector3 inFrom, Vector3 inTo)
    {
        inFrom.y = 0;
        inTo.y   = 0;
        float result = Mathf.Atan2(inTo.z - inFrom.z, inTo.x - inFrom.x) * 180.0f / Mathf.PI;

        Quaternion rotation = Quaternion.Euler(0, -result, 0);
        Quaternion smoothRotation = Quaternion.Lerp(_rotateTarget.transform.rotation, rotation, 0.125f);
        _rotateTarget.transform.rotation = smoothRotation;
    }
}
