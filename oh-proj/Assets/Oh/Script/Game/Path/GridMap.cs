﻿using UnityEngine;
using System.Collections.Generic;
using UniLinq;

public class GridMap : MonoBehaviour
{
    public  static GridMap it { get { return _instance; } }
    private static GridMap _instance;

    [Tooltip("충돌 감지 영역")]
    [SerializeField]
    LayerMask _collisionMask;

    [Tooltip("그리드 맵 크기")]
    [SerializeField]
    Vector2 _gridWorldSize;

    [Tooltip("각 그리드 크기")]
    [SerializeField]
    float _nodeRadius;

    [Tooltip("기즈모")]
    [SerializeField]
    bool _showMap;

    [SerializeField]
    Vector2 _select;

    public float nodeSize { get; set; }

    Vector3 _mapStartPosition;
    Vector2 _mapSize;

    Node[,] _grid;
    int _xSize = 0;
    int _ySize = 0;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        _instance = this;
    }

    public void Clear()
    {
        _grid           = null;
        _xSize          = 0;
        _ySize          = 0;
        nodeSize        = 0;
        _mapStartPosition = Vector2.zero;
    }

    public Vector3 ChangeWorldPosition(Vector2 size)
    {
        return ChangeWorldPosition(size.x, size.y);
    }

    /// <summary>
    /// 인덱스 위치를 월드 위치로 변환
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private Vector3 ChangeWorldPosition(float x, float y)
    {
        return _mapStartPosition + Vector3.right * (x * nodeSize + _nodeRadius) + Vector3.forward * (y * nodeSize + _nodeRadius);
    }

    /// <summary>
    /// 그리드 생성
    /// </summary>
    public void CreateGrid()
    {
        nodeSize = _nodeRadius * 2;
        _xSize = Mathf.RoundToInt(_gridWorldSize.x / nodeSize);
        _ySize = Mathf.RoundToInt(_gridWorldSize.y / nodeSize);

        _grid = new Node[_xSize, _ySize];

        // 월드의 왼쪽 하단 좌표를 구함
        _mapStartPosition = transform.position - Vector3.right * _gridWorldSize.x / 2 - Vector3.forward * _gridWorldSize.y / 2;

        Vector3 worldPoint = Vector3.zero;
        // 그리드를 생성하며 해당 위치에 오브젝트가 있는 지 확인
        for (int x = 0; x < _xSize; x++)
        {
            for (int y = 0; y < _ySize; y++)
            {
                // 충돌 오브젝트를 확인
                worldPoint = ChangeWorldPosition(x, y);
                bool collision = !(Physics.CheckSphere(worldPoint, _nodeRadius, _collisionMask));
                _grid[x, y] = new Node(collision, worldPoint, x, y);
            }
        }
    }

    /// <summary>
    /// 충돌할 수 없는 지역 업데이트
    /// </summary>
    public void RefreshBlockArea()
    {
        for (int x = 0; x < _xSize; x++)
        {
            for (int y = 0; y < _ySize; y++)
            {
                bool collision = !(Physics.CheckSphere(_grid[x, y].worldPosition, _nodeRadius, _collisionMask));
                _grid[x, y].collision = collision;
            }
        }
    }

    /// <summary>
    /// 3*3으로 인접해있는 노드 반환
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    public List<Node> GetApproachNode(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for (int x=-1;x<=1;++x)
        {
            for (int y = -1; y <= 1; ++y)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.x + x;
                int checkY = node.y + y;
                if (checkX >= 0 && checkX < _xSize && checkY >= 0 && checkY < _ySize)
                    neighbours.Add(_grid[checkX, checkY]);
            }
        }
        return neighbours;
    }

    /// <summary>
    /// 월드 좌표를 그리드 노드로 변환
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + _gridWorldSize.x/2) /_gridWorldSize.x;
        float percentY = (worldPosition.z + _gridWorldSize.y/2) /_gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((_xSize - 1) * percentX);
        int y = Mathf.RoundToInt((_ySize - 1) * percentY);
        return _grid[x, y];
    }

    public Vector2 FindEmptyIndex(Vector2 inSize)
    {
        return FindEmptyIndex((int)inSize.x / _xSize + 1, (int)inSize.y / _ySize + 1);
    }

    public Vector2 FindEmptyIndex(float inSize, bool boss)
    {
        Vector2 index = FindEmptyIndex((int)inSize / _xSize + 1, (int)inSize / _ySize + 1);
        int targetStart = boss ? 10 : 18;
        int targetValue = boss ? 15 : 40;
        if (index.x < targetStart || index.x > targetValue)
        {
            return FindEmptyIndex(inSize, boss);
        }
        return index;
    }

    /// <summary>
    /// 비어 있는 인덱스를 찾음
    /// </summary>
    /// <param name="inXSize"></param>
    /// <param name="inYSize"></param>
    /// <returns></returns>
    public Vector2 FindEmptyIndex(int inXSize, int inYSize)
    {
        RefreshBlockArea();
        Vector2 index = new Vector2(-1,-1);
        List<Vector2> emptyIndexList = new List<Vector2>();
        for (int x = 0; x < _xSize; x++)
        {
            for (int y = 0; y < _ySize; y++)
            {
                if (!_grid[x,y].collision ||  CheckBlockArea(x, y, inXSize, inYSize))
                {
                    continue;
                }
                emptyIndexList.Add(new Vector2(x, y));
            }
        }
        
        if (emptyIndexList.Count > 0)
        {
            index = emptyIndexList[Random.Range(0, emptyIndexList.Count - 1) ];
        }

        for (int x = 0; x < _xSize; x++)
        {
            for (int y = 0; y < _ySize; y++)
            {
                int realX = (int)index.x + x;
                int realY = (int)index.y + y;
                if (realX >= _xSize || realY >= _ySize)
                {
                    continue;
                }
                _grid[realX, realY].collision = false;
            }
        }
        index = new Vector2(index.x + inXSize / 2.0f, index.y + inYSize / 2.0f);
        return index;
    }

    /// <summary>
    /// 해당 영역 내 장애물 판단유무를 가져옴
    /// </summary>
    /// <param name="inX"></param>
    /// <param name="inY"></param>
    /// <param name="inXSize"></param>
    /// <param name="inYSize"></param>
    /// <returns></returns>
    public bool CheckBlockArea(int inX, int inY, int inXSize, int inYSize)
    {
        bool block = false;
        for (int x = 0; x < inXSize; x++)
        {
            for (int y = 0; y < inYSize; y++)
            {
                int realX = inX + x;
                int realY = inY + y;
                if (realX >= _xSize || realY >= _ySize || !_grid[realX, realY].collision)
                {
                    block = true;
                    break;
                }
            }
            if (block)
                break;
        }
        return block;
    }

    private void OnDrawGizmos()
    {
        if(_showMap && _grid != null)
        {
            foreach (Node node in _grid)
            {
                if (node.x == _select.x && node.y == _select.y)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeSize - 0.1f));
                }
                else if (!node.collision)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawCube(node.worldPosition, Vector3.one * (nodeSize - 0.1f));
                }
                else
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawWireCube(node.worldPosition, Vector3.one * (nodeSize - 0.1f));
                }
            }
        }
    }
}
