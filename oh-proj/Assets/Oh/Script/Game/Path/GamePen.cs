﻿using System.Collections.Generic;
using UnityEngine;
using UniRx.Triggers;
using UniRx;

/// <summary>
/// 리펙토링 필요
/// TODO @minjun.ha
/// </summary>
public class GamePen : MonoBehaviour
{
    public enum EMode
    {
        PathDraw,
        SelectSkillArea,
    }

    [Header(" ===== Path Draw Field ===== ")]
    [SerializeField]
    Transform _cursor;

    [SerializeField]
    Transform _selectFromMark;

    [SerializeField]
    GameObject _selectTargetMark;

    [SerializeField]
    GameObject _attackRange;

    List<Vector3>   _drawPositionList;
    LineRenderer    _lineRenderer;
    Collider        _selectTarget;
    Transform       _selected;
    PathFinder      _pathFinder;

    [Header(" ==== Skill Area Field ==== ")]
    [SerializeField]
    MeshRenderer _magicCircle;

    [SerializeField]
    Light _magicCircleLight;

    [SerializeField]
    MeshRenderer _ableRange;

    [SerializeField]
    Collider _magicCircleCollider;

    BattleObject targetObject = null;

    System.Action inSkillCallback = null;
    System.Action inCallback = null;

    public EMode mode { get; set; }
    private bool skillCheckOnce = false;

    private void Awake()
    {
        mode          = EMode.PathDraw;
        _drawPositionList = new List<Vector3>();
        _lineRenderer = this.GetComponent<LineRenderer>();
        _pathFinder   = this.GetComponent<PathFinder>();

        System.Func<Unit, bool> selector  = (Unit _) => { return Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0); };

        this.UpdateAsObservable()
            .Select(selector)
            .Where(x => x)
            .Subscribe(CursorAction);
    }

    private void Clear()
    {
        _selectTarget   = null;
        _selected       = null;
        _lineRenderer.positionCount = 0;
        _drawPositionList.Clear();
    }

    public void DisableAttackRange()
    {
        _attackRange.gameObject.SetActive(false);
    }

    public void SetActiveSkill(BattleObject inBattleObject, System.Action inCallback, System.Action inSkillCallback)
    {
        int id = inBattleObject.Data.skill_id.IntValue();
        if (!Statics.Instance.skill.ContainsKey(id))
        {
            return;
        }

        this.inSkillCallback = inSkillCallback;
        this.inCallback = inCallback;
        targetObject = inBattleObject;

        _attackRange.gameObject.SetActive(false);
        _selectFromMark.gameObject.SetActive(false);
        _magicCircle.gameObject.SetActive(false);
        mode = EMode.SelectSkillArea;
        skillCheckOnce = false;

        ServerStatic.Skill skill = Statics.Instance.skill[id];
        _ableRange.gameObject.SetActive(true);
        _ableRange.transform.SetParent(inBattleObject.transform);
        _ableRange.transform.SetPosition(Vector3.zero);
        _ableRange.transform.SetScaleX(skill.range + 20);
        _ableRange.transform.SetScaleZ(skill.range + 20);

        _magicCircleCollider.OnTriggerStayAsObservable()
            .Subscribe(_ => 
            {
                if (_.CompareTag("AbleRange"))
                {
                    _magicCircleLight.color = Color.green;
                    _ableRange.material.SetColor("_TintColor", Color.green);
                }
            })
            .AddTo(this.gameObject);

        _magicCircleCollider.OnTriggerExitAsObservable()
            .Subscribe(_ => 
            {
                if (_.CompareTag("AbleRange"))
                {
                    _magicCircleLight.color = Color.red;
                    _ableRange.material.SetColor("_TintColor", Color.red);
                }
            })
            .AddTo(this.gameObject);
    }

    private void SelectBattleObject(Transform inSelect, Color inColor)
    {
        BattleObject battleObject = inSelect.parent.GetComponent<BattleObject>();

        _attackRange.SetActive(true);
        _attackRange.transform.SetParent(inSelect);
        _attackRange.transform.localPosition = new Vector3(0, -5, 0);
        _attackRange.transform.localScale    = new Vector3(battleObject.Data.attack_range * 2, 5, battleObject.Data.attack_range * 2);
        _attackRange.GetComponent<MeshRenderer>().material.color = inColor;
    }

    private void CursorAction(bool b)
    {
        if (mode == EMode.PathDraw)
        {
            UpdateDraw();
        }
        else if (mode == EMode.SelectSkillArea)
        {
            UpdateSkillArea();
        }
    }

    /// <summary>
    /// 스킬 영역을 지정
    /// </summary>
    /// <param name="b"></param>
    private void UpdateSkillArea()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hit = Physics.RaycastAll(ray);
        for (int i = 0; i < hit.Length; i++)
        {
            GameObject hitObject = hit[i].collider.gameObject;
            if (hitObject.CompareTag(ETag.Ocean.ToString()) && hit[i].point.y < 3)
            {
                _magicCircle.gameObject.SetActive(true);
                _magicCircle.transform.position = hit[i].point;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (skillCheckOnce && _magicCircleLight.color == Color.green)
            {
                if (inCallback != null)
                {
                    inCallback();
                }

                _ableRange.gameObject.SetActive(false);
                _ableRange.transform.SetParent(this.transform);
                _magicCircle.gameObject.SetActive(false);
                targetObject.UseSkill(null, inSkillCallback);
            }
            else
            {
                skillCheckOnce = true;
            }
        }
    }

    /// <summary>
    /// 바다에 이동 경로를 그림
    /// </summary>
    /// <param name="b"></param>
    private void UpdateDraw()
    {
        // TODO @minjun.ha 나중에 레이어로 구분지어서 전체를 다 raycast 안해도 될 듯 함.
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hit;
        hit = Physics.RaycastAll(ray);
        for (int i = 0; i < hit.Length; i++)
        {
            GameObject hitObject = hit[i].collider.gameObject;
            if (Input.GetMouseButtonDown(0))
            {
                if (hitObject.CompareTag(ETag.Player.ToString()))
                {
                    _selected = hit[i].collider.transform;
                    _cursor.position = _selected.position;

                    SelectBattleObject(_selected, STZCommon.ToUnityColor(0, 255, 0, 100));

                    _selectFromMark.gameObject.SetActive(true);
                    _selectFromMark.SetParent(_selected);
                    _selectFromMark.localPosition = Vector3.zero;
                }
                else if (hitObject.CompareTag(ETag.Enermy.ToString()))
                {
                    var select = hit[i].collider.transform;
                    SelectBattleObject(select, STZCommon.ToUnityColor(255, 0, 0, 100));
                }
            }
            if (_selected != null)
            {
                // 바다를 raycast하여 바다위에 경로를 그림
                if (hit[i].collider.gameObject.tag == ETag.Ocean.ToString())
                {
                    if (hit[i].point.y < 3)
                    {
                        _cursor.position = hit[i].point;
                    }
                }
                else if ((hitObject.tag == ETag.Enermy.ToString() ||
                          hitObject.tag == ETag.Player.ToString()) && (_selected != hit[i].collider.transform))
                {
                    float scale = hitObject.transform.parent.GetComponent<BattleObject>().Data.size;

                    _selectTarget = hit[i].collider;
                    _selectTargetMark.SetActive(true);
                    _selectTargetMark.transform.SetParent(_selectTarget.transform.parent);
                    _selectTargetMark.transform.localScale = new Vector3(scale, 5, scale);
                    _selectTargetMark.transform.localPosition = Vector3.zero;
                }
            }
        }

        if (_selected != null)
        {
            Vector3 pos = new Vector3((int)_cursor.position.x, (int)_cursor.position.y, (int)_cursor.position.z);
            if (Input.GetMouseButton(0) && !_drawPositionList.Contains(pos))
            {
                // 최단 거리의 경로를 유저에게 보여줌
                var list = _pathFinder.FindPath(_selected.transform.position, pos);
                if (list != null)
                {
                    _lineRenderer.positionCount = list.Count;
                    for (int i = 0; i < list.Count;i++)
                    {
                        _lineRenderer.SetPosition(i, list[i]);
                    }
                }
                _drawPositionList.Add(pos);
            }

            // 손을 땟을때 해당 경로 까지 이동함
            if (Input.GetMouseButtonUp(0))
            {
                if (_selectTargetMark != null)
                {
                    _selectTargetMark.SetActive(false);
                }

                BattleObject battleObject = _selected.parent.GetComponent<BattleObject>();
                if (_selectTarget == null)
                {
                    battleObject.MoveTo(_drawPositionList[_drawPositionList.Count - 1], false);
                }
                else
                {
                    IProperty target = _selectTarget.transform.parent.GetComponent<BattleObject>() as IProperty;
                    battleObject.SetChaseTarget(target);
                }
                Clear();
            }
        }
    }
}
