﻿using UnityEngine;
using System.Collections;

public class Node
{
    public int TotalScore { get { return pastTotalScore + distanceScore; } }
    public int x { get; private set; }
    public int y { get; private set; }

    public Vector3 worldPosition { get; private set; }

    public Node parent;
    public bool collision;
    public int  pastTotalScore;
    public int  distanceScore;

    public Node(bool inCollision, Vector3 inWorldPosition, int inX, int inY)
    {
        x = inX;
        y = inY;
        collision     = inCollision;
        worldPosition = inWorldPosition;
    }
}
