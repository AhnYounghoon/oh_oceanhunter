﻿using UnityEngine;
using System.Collections.Generic;
using UniLinq;

public class PathFinder : MonoBehaviour
{
    List<Vector3>   _movePath;

    /// <summary>
    /// 길을 찾음
    /// </summary>
    /// <param name="startPos"></param>
    /// <param name="targetPos"></param>
    /// <returns>null이면 길을 찾지 못한 경우</returns>
    public List<Vector3> FindPath(Vector3 startPos, Vector3 targetPos)
    {
        GridMap.it.RefreshBlockArea();
        List<Node> openList     = new List<Node>();
        HashSet<Node> closeList = new HashSet<Node>();

        Node startNode  = GridMap.it.NodeFromWorldPoint(startPos);
        Node targetNode = GridMap.it.NodeFromWorldPoint(targetPos);

        // NOTE @minjun.ha 물체 사이즈에 막혀 길 찾기가 안되는 경우가 있어 시작값은 항상 collision 를 초기화
        startNode.collision = false;

        openList.Add(startNode);
        while(openList.Count > 0)
        {
            Node currentNode = openList[0];
            for(int i = 1; i < openList.Count; i++)
            {
                // 이전 노드의 총 점이 현재 노드 보다 작고 인접 점수가 작다면 이전노드를 현재노드로 사용
                if (openList[i].TotalScore < currentNode.TotalScore || 
                    (openList[i].TotalScore == currentNode.TotalScore && openList[i].distanceScore < currentNode.distanceScore))
                {
                    currentNode = openList[i];
                }
            }
            openList.Remove(currentNode);
            closeList.Add(currentNode);

            List<Node> approachNodeList = GridMap.it.GetApproachNode(currentNode);
            for (int i = 0; i < approachNodeList.Count;i++)
            {
                Node node = approachNodeList[i];
                if (!node.collision || closeList.Contains(node))
                {
                    continue;
                }

                // 오픈 리스트에 포함되어 있지 않거나 이전 스코어 보다 낮다면 점수 갱신
                int pastTotalScore = currentNode.pastTotalScore + GetDistance(currentNode, node);
                if(pastTotalScore < node.pastTotalScore || !openList.Contains(node))
                {
                    node.parent         = currentNode;
                    node.pastTotalScore = pastTotalScore;
                    node.distanceScore  = GetDistance(node, targetNode);

                    if (!openList.Contains(node))
                    {
                        openList.Add(node);
                    }
                }
            }
            if (currentNode == targetNode)
            {
                EndMovePath(startNode, targetNode);
                break;
            }
        }
        return _movePath;
    }

    /// <summary>
    /// 도착지를 선정
    /// </summary>
    /// <param name="startNode"></param>
    /// <param name="endNode"></param>
    void EndMovePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
        _movePath = path.Select(x => x.worldPosition).ToList();
    }

    /// <summary>
    /// 휴리스틱 추정값에 의한 타깃까지 거리
    /// </summary>
    /// <param name="nodeA"></param>
    /// <param name="nodeB"></param>
    /// <returns></returns>
    int GetDistance(Node nodeA, Node nodeB)
    {
        int lenX = Mathf.Abs(nodeA.x - nodeB.x);
        int lenY = Mathf.Abs(nodeA.y - nodeB.y);

        if (lenX > lenY)
        {
            return 14 * lenY + 10 * (lenX - lenY);
        }
        return 14 * lenX + 10 * (lenY - lenX);
    }

    /// <summary>
    /// 저장된 도착길 정보를 지움
    /// </summary>
    public void RemoveReachPath()
    {
        if (_movePath != null)
        {
            _movePath.Clear();
        }
    }

    void OnDrawGizmos()
    {
        if (_movePath != null)
        {
            Gizmos.color = Color.black;
            foreach (Vector3 n in _movePath)
            {
                Gizmos.DrawCube(n, Vector3.one * (GridMap.it.nodeSize - 0.1f));
            }
        }
    }
}

