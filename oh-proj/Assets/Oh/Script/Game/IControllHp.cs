﻿using UnityEngine;

public interface IControllHp
{
    /// <summary>
    /// 데미지 / 힐 
    /// </summary>
    /// <param name="inValue"></param>
    /// <param name="inPosition">적용 위치</param>
    /// <returns></returns>
    bool GetDamage(float inValue, Vector3 inPosition = default(Vector3));
    bool GetHeal(float inValue, Vector3 inPosition = default(Vector3));

    /// <summary>
    /// 최대 체력 반환
    /// </summary>
    /// <returns></returns>
    float GetMaxHp();
}
