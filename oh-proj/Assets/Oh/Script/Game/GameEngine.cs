﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using DG.Tweening;

public class GameEngine : MonoBehaviour
{
    [SerializeField]
    GameCamera _gameCamera;

    [SerializeField]
    GameLight _gameLight;

    [SerializeField]
    GameUI _gameUI;

    [SerializeField]
    GamePen _gamePen;

    List<BattleObject> _playerList = new List<BattleObject>();
    List<BattleObject> _enemyList  = new List<BattleObject>();

    List<PlayData> _playerDataList = new List<PlayData>();
    List<PlayData> _enemyDataList  = new List<PlayData>();

    Transform       _backgroundLayer; 
    Transform       _shipLayer;
    int             _currentWave;

    // TEST VALUE ====================
    private bool _stageClear = false;
    private bool _startGame  = false;

    PlayData boss = null;

    private void Awake()
    {
        SingletonController.Get<SoundManager>().StopGroup("bgm");
        SingletonController.Get<SoundManager>().Play("BGM_ingame", "bgm");

        Clear();
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "BattleSimulationTool")
        {
            LoadOnAwake();
        }
    }

    private void Start()
    {
        AnimationController.it.Clear();
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "BattleSimulationTool")
        {
            GameInit();
        }
    }

    protected void LoadOnAwake()
    {
        _backgroundLayer = this.transform.Find("BackgroundLayer");
        _shipLayer       = this.transform.Find("BattleObjectLayer");
        Statics.Instance.Parse(Statics.LoadFromLocal());
    }

    private void OnDestroy()
    {
        // NOTE @minjun.ha 게임 씬이 삭제될 때 애니메이션 컨트롤러 하위 객체도 삭제
        AnimationController.it.Clear();
        GridMap.it.Clear();
    }

    public void GameInit(List<PlayData> inPlayerData = null, List<PlayData> inEnemyData = null, int backgroundID = 1)
    {
        if (inPlayerData != null)
        {
            _playerDataList = inPlayerData;
        }
        if (GeneralDataManager.it.playerData != null)
            _playerDataList = GeneralDataManager.it.playerData;

        if (inEnemyData != null)
        {
            _enemyDataList = inEnemyData;
        }
        if (GeneralDataManager.it.enemyData != null)
            _enemyDataList = GeneralDataManager.it.enemyData;

        _gamePen.gameObject.SetActive(true);
        GridMap.it.CreateGrid();
        GameSetting();
        _gameUI.SetGameStartUI();
        ShowStartAction();
    }

    void FixedUpdate ()
    {
        if (!_startGame)
            return;

        for (int i = 0; i < _playerList.Count; i++)
        {
            if (_playerList[i].gameObject.activeSelf)
            {
                _playerList[i].UpdateMovement();
            }
        }
        for (int i = 0; i < _enemyList.Count;i++)
        {
            if (_enemyList[i].gameObject.activeSelf)
            {
                _enemyList[i].UpdateMovement();
            }
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            _gameCamera.SetOrthographic(false);
            _gameCamera.SetTarget(_playerList[0].transform);
            _gameCamera.SetZoomSize(50);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            _gameCamera.SetTarget(null);
            _gameCamera.SetOrthographic(true);
            _gameCamera.SetZoomSize(120);
        }

        if (BattleController.it.GroupAllDisabled(ETag.Enermy))
        {
            // 0.2 배로 넘어가는 버그 수정 (임시)
            Time.timeScale = 1.0f;

            if (CreateEnemy(_currentWave + 1))
            {
                _gameUI.ShowWave(_currentWave + 1);
                ReadyToNextWave(() => 
                {
                    StartWave(_currentWave + 1);
                });
            }
            else
            {
                GameClear();
            }
        }
        if (BattleController.it.GroupAllDisabled(ETag.Player))
        {
            GameOver();
        }
    }

    /// <summary>
    /// sailing_category 시트를 참조하여 보상 제공
    /// </summary>
    void GetStageClearReward()
    {
        var reward = Statics.Instance.sailing_category.Values.Where(x => x.stage_id == GeneralDataManager.it.prevStage).FirstOrDefault();
        GeneralDataManager.it.AcquireReward(reward.clear_reward);
    }

    public void GameClear()
    {
        if (_stageClear)
            return;

        _stageClear = true;
        _gameUI.ShowStageClear(() =>
        {
            GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.GOLD.ToString()  , 50);
            GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.SILVER.ToString(), 100);
            if (GeneralDataManager.it.isBoss)
            {
                GetStageClearReward();
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.STAGE_CLEAR, null, GeneralDataManager.it.prevStage);
                StageDataManager.it.SaveOceanData();
                UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.LobbyScene.ToString());
                return;
            }
            // 전투 클리어 보상
            else
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.SILVER_BOX.ToString(), 1);

            GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.BattleScene;
            GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.INGAME_WIN, null, 1);
            // 해당위치의 토큰 삭제후 스테이지 로컬 캐싱
            StageDataManager.it.RemoveTokenAtOceanData(GeneralDataManager.it.tokenIndex);
            StageDataManager.it.SaveOceanData();
            UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.BattleOceanScene.ToString());
        });
    }

    public void GameOver()
    {
        _gameUI.ShowStageFail(() =>
        {
            GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.BattleScene;
            GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.INGAME_WIN, null, 0);
            // 스테이지 로컬 캐싱
            StageDataManager.it.SaveOceanData();
            UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.LobbyScene.ToString());
        });
    }

    private void ShowStartAction()
    {
        /// 연출
        /// 1. 배가 먼저 옴
        /// 2. 적이 나타나고
        /// 3. UI 표시
        /// 
        _gameCamera.SetOrthographic(false);
        _gameCamera.SetTarget(_playerList[_playerList.Count/ 2].transform);
        _gameCamera.SetZoomSize(50);

        Tweener endTweener = null;
        for (int i = 0; i < _playerList.Count;i++)
        {
            Vector3 to   = _playerDataList[i].startPosition;
            Vector3 from = new Vector3(to.x - 500, to.y, to.z);
            _playerList[i].gameObject.SetActive(true);
            _playerList[i].transform.position = from;
            endTweener = _playerList[i].transform.DOMove(to, 2.0f + i * 0.4f);
        }

        endTweener.OnComplete(() =>
        {
            _gameCamera.SetTarget(null);
            _gameCamera.SetOrthographic(true);
            _gameCamera.SetZoomSize(120);

            CreateEnemy(_currentWave);
            StartWave(_currentWave, () => 
            {
                _stageClear = false;
                _gameUI.FadeInUI();
                _gameUI.ShowNoti(StringHelper.Format("Wave {0}", _currentWave));
            });
        });
    }

    private void ReadyToNextWave(System.Action inEndCallback)
    {
        _startGame = false;

        _gamePen.DisableAttackRange();
        _gameCamera.SetOrthographic(false);
        _gameCamera.SetTarget(_playerList[_playerList.Count / 2].transform);
        _gameCamera.SetZoomSize(50);

        Tweener tweener = null;
        foreach (var player in _playerList)
        {
            player.ReadyToNextWave();
            tweener = player.transform.DOMoveX(player.transform.position.x + 400, 2.0f)
                .SetDelay(1.0f);
        }
        _gameUI.FadeIn(1.0f);

        tweener.OnComplete(() => 
        {
            _gameUI.FadeOut();
            _gameUI.ShowNoti(boss != null ? "Boss" : StringHelper.Format("Wave {0}", _currentWave + 1));
            if (boss != null)
            {
                _gameUI.bossUI.Init(boss);
            }

            Tweener endTweener = null;
            for (int i = 0; i < _playerList.Count; i++)
            {
                if (_playerList[i].CheckCondition(BattleEnum.EProperty.Hp, BattleEnum.ESplitProperty.Value, BattleEnum.EValueType.Compare, 0) == 1)
                {
                    continue;
                }
                Vector3 to = _playerDataList[i].startPosition;
                Vector3 from = new Vector3(to.x - 500, to.y, to.z);
                _playerList[i].gameObject.SetActive(true);
                _playerList[i].transform.position = from;
                endTweener = _playerList[i].transform.DOMove(to, 2.0f + i * 0.4f);
            }
            endTweener.OnComplete(() => 
            {
                _gameCamera.SetTarget(null);
                _gameCamera.SetOrthographic(true);
                _gameCamera.SetZoomSize(120);

                inEndCallback();
            });
        });
    }

    private bool CreateEnemy(int inWave)
    {
        _enemyList.Clear();

        boss = null;
        foreach (var data in _enemyDataList)
        {
            if (data.wave != inWave)
            {
                continue;
            }

            BattleObject enemy = ObjectCreator.it.CreateObj<BattleObject>(_shipLayer, "Prefabs/BattleScene/BattleObject/" + data.prefab_path);
            enemy.gameObject.SetActive(false);
            enemy.Data = data;
            _enemyList.Add(enemy);
            if (data.boss)
            {
                enemy.changeHpEventHandler += _gameUI.bossUI.ChangeHpEvent;
                boss = data;
            }
        }

        if (_enemyList.Count == 0)
        {
            return false;
        }
        return true;
    }

    public void StartWave(int inWave, System.Action inEndCallback = null)
    {
        _currentWave    = inWave;
        _startGame      = false;

        // Set Battle Controller
        BattleController.it.SetData(_playerList, _enemyList);

        Tweener endTweener = null;
        for (int i = 0; i < _enemyList.Count; i++)
        {
            Vector3 pos  = GridMap.it.ChangeWorldPosition(GridMap.it.FindEmptyIndex(_enemyDataList[i].size * 2, _enemyDataList[i].boss));
            Vector3 to   = Vector3.zero;
            Vector3 from = Vector3.zero; 

            if (_enemyDataList[i].id == 11 || _enemyDataList[i].id == 12 || _enemyDataList[i].id == 14 || _enemyDataList[i].id == 15 || _enemyDataList[i].id == 16 || _enemyDataList[i].id == 17 || _enemyDataList[i].id == 20 || _enemyDataList[i].id == 21 || _enemyDataList[i].id == 25 || _enemyDataList[i].id == 26 || _enemyDataList[i].id == 27 || _enemyDataList[i].id == 30)
            {
                from = new Vector3(pos.x + 800, pos.y, pos.z);
                to = new Vector3(pos.x, pos.y, pos.z);
            }
            else
            {
                from = new Vector3(pos.x, pos.y - 300, pos.z);
                to = new Vector3(pos.x, pos.y, pos.z);
            }
            _enemyList[i].gameObject.SetActive(true);
            _enemyList[i].transform.position = from;
            endTweener = _enemyList[i].transform.DOMove(to, 1.0f + i * 0.05f);
        }

        endTweener.OnComplete(() =>
        {
            _startGame = true;
            if (inEndCallback != null)
            {
                inEndCallback();
            }
        });
    }

    public void Clear()
    {
        _currentWave = 1;
        _startGame = false;

        _playerDataList.Clear();
        _enemyDataList.Clear();

        _playerList.Clear();
        _enemyList.Clear();

        _gameUI.Clear();
        AnimationController.it.Clear();
        if (_shipLayer != null)
        {
            _shipLayer.transform.DestroyChildren();
        }
    }

    /// <summary>
    /// 스킬 사용
    /// </summary>
    /// <param name="data"></param>
    private void UseSkillPlayer(PlayData data)
    {
        BattleObject target = null;
        for (int i = 0; i < _playerDataList.Count; i++)
        {
            if (_playerDataList[i].heroData.id == data.heroData.id)
            {
                target = _playerList[i];
                break;
            }
        }

        if (target == null)
        {
            Debug.LogError("Use Skill Player not exist !!!");
            return;
        }

        int skillId = target.Data.skill_id.IntValue();
        _gameLight.SetSkillMode(target.transform, true, () => { Time.timeScale = 0.2f; });
        _gamePen.SetActiveSkill(target, () => 
        {
            _gamePen.mode = GamePen.EMode.PathDraw;
            Time.timeScale = 1.0f;
            _gameLight.SetSkillMode(target.transform, false);

            _gameCamera.SetOrthographic(false);
            _gameCamera.SetTarget(target.transform);
            _gameCamera.SetZoomSize(50);

            _gameUI.skillUI.Active(Statics.Instance.skill[skillId], target.Data.heroData);
        }, ()=> 
        {
            _gameCamera.SetTarget(null);
            _gameCamera.SetOrthographic(true);
            _gameCamera.SetZoomSize(120);
            _gameLight.SetSkillMode(target.transform, false);
        });
    }

    /// <summary>
    /// 게임 시작 전 셋팅
    /// </summary>
    private void GameSetting()
    {
        // 시뮬레이팅을 위해 해당 사항에서 추가 
        if (_playerDataList.Count == 0)
        {
            PlayerPlayData p2 = new PlayerPlayData(Statics.Instance.character[16], Statics.Instance.character[30]);
            p2.hp = 100000;
            p2.currentHp = 100000;
            p2.startPosition = new Vector3(-150, -5, 100);
            _playerDataList.Add(p2);

            PlayerPlayData p1 = new PlayerPlayData(Statics.Instance.character[17], Statics.Instance.character[31]);
            p1.startPosition = new Vector3(-150, -5, 0);
            _playerDataList.Add(p1);

            PlayerPlayData p3 = new PlayerPlayData(Statics.Instance.character[18], Statics.Instance.character[32]);
            p3.startPosition = new Vector3(-150, -5, -100);
            _playerDataList.Add(p3);

            //PlayerPlayData p4 = new PlayerPlayData(Statics.Instance.character[19], Statics.Instance.character[33]);
            //p4.startPosition = new Vector3(-150, -5, -150);
            //_playerDataList.Add(p4);
        }
        if (_enemyDataList.Count == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                EnemyPlayData d1 = new EnemyPlayData();
                d1.wave = 1;
                d1.hp = 100000;
                d1.currentHp = 100000;
                _enemyDataList.Add(d1);
            }

            EnemyPlayData d2 = new EnemyPlayData();
            d2.wave = 2;
            d2.boss = true;
            _enemyDataList.Add(d2);

            EnemyPlayData d3 = new EnemyPlayData();
            d3.wave = 3;
            d3.boss = true;
            _enemyDataList.Add(d3);
        }

        // Create Players
        for (int i = 0; i < _playerDataList.Count;i++)
        {
            PlayData data = _playerDataList[i];
            BattleObject player = ObjectCreator.it.CreateObj<BattleObject>(_shipLayer, "Prefabs/BattleScene/BattleObject/" + data.prefab_path);
            player.transform.localPosition = Vector3.zero;
            player.gameObject.SetActive(false);
            player.changeHpEventHandler += _gameUI.GetHpUIEvent(i);
            player.Data = data;
            _playerList.Add(player);
        }

        // Set Characters
        _gameUI.SetCharacterData(_playerDataList);
        _gameUI.UseSkillEvent += UseSkillPlayer;

        // Set Camera
        _gameCamera.AddState(GameCamera.EState.Move, GameCamera.EUse.Loop);
        _gameCamera.AddState(GameCamera.EState.Zoom, GameCamera.EUse.Loop);
    }

    public void __TEST_SKIP_WAVE()
    {
        foreach(var enemy in _enemyList)
        {
            enemy.Data.currentHp = 0;
            enemy.gameObject.SetActive(false);
        }

        // 0.2 배로 넘어가는 버그 수정 (임시)
        Time.timeScale = 1.0f;

        if (CreateEnemy(_currentWave + 1))
        {
            _gameUI.ShowWave(_currentWave + 1);
            ReadyToNextWave(() =>
            {
                StartWave(_currentWave + 1);
            });
        }
        else
        {
            GameClear();
        }
    }

    public void __TEST_POWER_UP()
    {
        foreach (var data in _playerDataList)
        {
            data.damage = 100000;
        }
    }
}
