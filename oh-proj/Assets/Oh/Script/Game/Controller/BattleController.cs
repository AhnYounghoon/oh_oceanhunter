﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ServerStatic;
using BattleEnum;

public class BattleController : Singleton<BattleController>
{
    public Dictionary<string, Color> debugList = new Dictionary<string, Color>();

    private class Data
    {
        public List<Character>  characterList;
        public BattleObject     battleObject;
        public PlayData         playData;
    }

    List<Skill> _verificationSkillList = new List<Skill>();

    List<IProperty>  _targetAllList;        // Value 별로 자르지 않고 범위에 의한 스플 데미지를 위해 보존
    List<IProperty>  _playerBattleObject;
    List<IProperty>  _enemyBattleObject;
                     
    public void SetData(List<BattleObject> inPlayerList, List<BattleObject> inEnemyList)
    {
        Clear();

        var playerList  = ConvertDataList(inPlayerList);
        var enemyList   = ConvertDataList(inEnemyList);

        GatherList(playerList, enemyList);

        // Set Data시 모든 스킬 데이터를 검증해도 괜찮을듯함?
    }

    public void Clear()
    {
        _verificationSkillList.Clear();

        _playerBattleObject =null;
        _enemyBattleObject = null;
        _targetAllList = null;
    }

    public float GetCoolTime(int inSkillId)
    {
        if (!Statics.Instance.skill.ContainsKey(inSkillId))
        {
            return 2.0f;    /// 스킬 쿨타임 디폴트 값
        }
        return Statics.Instance.skill[inSkillId].cool_time;
    }

    /// <summary>
    /// 가장 가까운 타겟의 위치를 가져옴
    /// </summary>
    /// <param name="inTargetTag"></param>
    /// <param name="inPosition"></param>
    /// <returns></returns>
    public IProperty GetNearestTarget(ETag inTargetTag, Vector3 inPosition, IProperty inIgnoreTarget = null)
    {
        List<IProperty> targetList = inTargetTag == ETag.Enermy ? _enemyBattleObject : _playerBattleObject;
        targetList = targetList
            .Where(x => x != inIgnoreTarget && x.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) > 0)
            .OrderBy(x => Vector3.Distance(inPosition, x.GetHitAblePosition()))
            .ToList();
        return targetList.FirstOrDefault();
    }

    /// <summary>
    /// Hp가 가장 낮은 타깃을 가져옴
    /// </summary>
    /// <param name="inTargetTag"></param>
    /// <param name="inIgnoreTarget"></param>
    /// <returns></returns>
    public IProperty GetLowHpTarget(ETag inTargetTag, IProperty inIgnoreTarget = null)
    {
        List<IProperty> targetList = inTargetTag == ETag.Enermy ? _enemyBattleObject : _playerBattleObject;
        targetList = targetList
            .Where(x => x != inIgnoreTarget && x.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) > 0)
            .OrderBy(x => x.CheckCondition(EProperty.Hp, ESplitProperty.Percent, EValueType.None, 0))
            .ToList();
        return targetList.FirstOrDefault();
    }

    /// <summary>
    /// 그룹이 모두 상태 불능인지 판단
    /// </summary>
    /// <param name="inTargetTag"></param>
    /// <returns></returns>
    public bool GroupAllDisabled(ETag inTargetTag)
    {
        bool disable = true;
        List<IProperty> targetList = inTargetTag == ETag.Enermy ? _enemyBattleObject : _playerBattleObject;
        if (targetList == null)
        {
            // NOTE @minjun.ha 셋팅 전이라 판단하고 살아있는걸로 취급
            return false;
        }

        for (int i = 0; i < targetList.Count; i++)
        {
            if (targetList[i].CheckCondition(EProperty.Arrive, ESplitProperty.Value, EValueType.Compare, 0) != -1)
            {
                disable = false;
                break;
            }
        }
        return disable;
    }

    /// <summary>
    /// 타겟 그룹에 상태 이상을 적용시킨다
    /// </summary>
    /// <param name="inTarget"></param>
    /// <param name="inProperty"></param>
    public void ApplyCondition(ETarget inTarget, ESplitProperty inProperty)
    {
        if (inTarget == ETarget.Player)
        {
            foreach (var player in _playerBattleObject)
            {
                player.ApplyCondition(inProperty);
            }
        }
    }

    /// <summary>
    /// 값 반환 형식을 가져옴
    /// </summary>
    /// <param name="inValueType"></param>
    /// <param name="inValue"></param>
    /// <param name="inData"></param>
    /// <returns></returns>
    public int GetConditionValue(BattleEnum.EValueType inValueType, float inValue, float inData)
    {
        int value = (int)inData;
        if (inValueType == BattleEnum.EValueType.Ascending ||
            inValueType == BattleEnum.EValueType.Descending)
        {
            value = (int)inData;
        }
        else if (inValueType == BattleEnum.EValueType.Compare &&
            inValue == inData)
        {
            value = 1;
        }
        return value;
    }

    /// <summary>
    /// 스킬 사용 여부를 확인하고 스킬 사용
    /// </summary>
    /// <param name="inSkill">스킬 정보</param>
    /// <param name="inUseObj">스킬 사용 요청을 한 오브젝트</param>
    /// <returns>스킬의 사용 여부를 반환</returns>
    public bool ActiveSkill(int inSkillId, GameObject inUseObj, IProperty inUseProperty, ETag inTag, System.Action inEndCallback = null)
    {
        // 스킬 스태틱 검증
        if (!Statics.Instance.skill.ContainsKey(inSkillId))
        {
            return false;
        }

        Skill skill = Statics.Instance.skill[inSkillId];
        int condition_id = skill.excute_condition_id;
        int effect_id = skill.excute_effect_id;
        int target_id = skill.excute_target_id;

        // 스킬 데이터 검증
        if (!VerificationSkill(skill))
        {
            return false;
        }

        // 스킬 발동 조건 검증
        if (!CheckCondition(Statics.Instance.skill_condition[condition_id], inTag))
        {
            return false;
        }

        // 타겟 대상을 받아옴
        List<IProperty> targetList = GetTargetList(Statics.Instance.skill_target[target_id], inTag, inUseProperty, skill.range);
        if (targetList == null || targetList.Count == 0)
        {
            return false;
        }
        ExcuteSkillEffect(Statics.Instance.skill_effect[effect_id], inUseObj, inUseProperty,targetList, _targetAllList, Statics.Instance.skill_target[target_id].range, inEndCallback);
        return true;
    }

    /// <summary>
    /// 애니메이션 모션을 실행한다
    /// </summary>
    /// <param name="inEffect"></param>
    /// <param name="inTargetList"></param>
    private void ExcuteSkillEffect(SkillEffect inEffect, GameObject inUseObj,IProperty inUseProperty, List<IProperty> inTargetList, List<IProperty> inTargetBaseList,float inRange, System.Action inEndCallback)
    {
        foreach (var target in inTargetList)
        {
            AnimationController.it.CharacterSkill(inEffect, inUseObj, target,
            (Vector3 position) => /// Effect 
            {
                ApplyAbility(target, inTargetBaseList, inUseProperty, inEffect, position, inRange);
            },
            () => /// End
            {
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });
        }
    }

    /// <summary>
    /// 타겟에 해당 수치를 반영한다
    /// </summary>
    /// <param name="inTarget"></param>
    /// <param name="inEffect"></param>
    private void ApplyAbility(IProperty inTarget, List<IProperty> inTargetList, IProperty inUseProperty, SkillEffect inEffect, Vector2 inPosition, float inRange)
    {
        EEffectType     type     = inEffect.GetEffectType();
        ESplitProperty property = inEffect.GetSplitProperty();

        foreach (var target in inTargetList)
        {
            // 충돌 및 사용 오브젝트의 명중률 체크
            if (inUseProperty.CheckCondition(EProperty.Hit, ESplitProperty.Value, EValueType.Ascending, 0) == 1 &&
               (inTarget == target || (inRange > 0 && target.ContainBoundBox(inPosition, inRange))))
            {
                /// Attack (GetDamage)
                if (type == EEffectType.Attack)
                {
                    if (!(target is IControllHp))
                    {
                        // 해당 타겟은 HP 속성이 없어 공격 받을 수 없음
                        return;
                    }

                    float value = 0;
                    if (property == ESplitProperty.Percent)
                    {
                        value = (target as IControllHp).GetMaxHp() * (inEffect.value / 100.0f);
                        value *= Random.Range(0, inEffect.correct_value);
                    }
                    else if (property == ESplitProperty.Value)
                    {
                        value = inEffect.value;
                        value += Random.Range(-inEffect.correct_value, inEffect.correct_value);
                    }
                    else if (property == ESplitProperty.Stat)
                    {
                        value = inUseProperty.CheckCondition(EProperty.Damage, ESplitProperty.Value, EValueType.Ascending, 0);
                        value *= inEffect.value;
                        value += Random.Range(-inEffect.correct_value, inEffect.correct_value);
                    }

                    // 크리티컬 데미지, 관통 데미지 추가
                    value += inUseProperty.CheckCondition(EProperty.Critical_Damage, ESplitProperty.Value, EValueType.Ascending, 0);
                    value += inUseProperty.CheckCondition(EProperty.Penetrate, ESplitProperty.Value, EValueType.Ascending, 0);
                    (target as IControllHp).GetDamage(value);
                }
                /// Heal (GetHeal)
                else if (type == EEffectType.Heal)
                {
                    if (!(target is IControllHp))
                    {
                        // 해당 타겟은 HP 속성이 없어 힐 받을 수 없음
                        return;
                    }

                    float value = 0;
                    if (property == ESplitProperty.Percent)
                    {
                        value = (target as IControllHp).GetMaxHp() * (inEffect.value / 100.0f);
                    }
                    else if (property == ESplitProperty.Value)
                    {
                        value = inEffect.value;
                    }
                    else if (property == ESplitProperty.Stat)
                    {
                        value = inUseProperty.CheckCondition(EProperty.Damage, ESplitProperty.Value, EValueType.Ascending, 0);
                        value *= inEffect.value;
                        value += Random.Range(-inEffect.correct_value, inEffect.correct_value);
                    }
                    (target as IControllHp).GetHeal(value);
                }
            }
        }
    }

    /// <summary>
    /// 타겟 오브젝트 리스트를 받아옴
    /// 
    /// 1. 타겟 오브젝트 그룹 선정
    /// 2. 대상 ID, Range 비교
    /// 3. 전투상태,대상 ID,범위 비교
    /// 4. 분류 속성 비교
    /// 5. 대상 수 받아옴
    /// 
    /// </summary>
    /// <param name="inTarget">타겟 리스트</param>
    /// <returns></returns>
    private List<IProperty> GetTargetList(SkillTarget inTarget, ETag inTag, IProperty InUseObj, float inRange)
    {
        // 섞이면 안되는 리스트기 때문에 삭제, 정렬 후 마지막에 결과 리스트에 합친다
        List<IProperty> targetAList = new List<IProperty>();
        List<IProperty> targetBList = new List<IProperty>();

        List<IProperty>[] allListArray = new List<IProperty>[] { targetAList, targetBList };
        List<IProperty> targetResultList = null;

        // 타겟 오브젝트 그룹 선정
        {
            ETarget target = inTarget.GetTarget();
            if (target == ETarget.All)
            {
                targetAList.AddRange(_playerBattleObject);
                targetAList.AddRange(_enemyBattleObject);
            }
            else
            {
                if (inTag == ETag.Player)
                {
                    switch (target)
                    {
                        case ETarget.Player: targetAList.AddRange(_playerBattleObject); break;
                        case ETarget.Enemy: targetAList.AddRange(_enemyBattleObject); break;
                    }
                }
                else if (inTag == ETag.Enermy)
                {
                    switch (target)
                    {
                        case ETarget.Player: targetAList.AddRange(_enemyBattleObject); break;
                        case ETarget.Enemy: targetAList.AddRange(_playerBattleObject); break;
                    }
                }
            }

            if (targetAList.Count == 0 && targetBList.Count == 0)
            {
                return null; /// 타겟이 없을때
            }
        }

        // 전투상태,대상 ID,범위 비교
        {
            Vector3 position = InUseObj.GetHitAblePosition();
            position.y = 0;
            foreach (var list in allListArray)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].CheckCondition(EProperty.Arrive, ESplitProperty.Value, EValueType.Compare, 0) == -1)
                    {
                        list.Remove(list[i]);
                        i--;
                    }
                    else if (!list[i].ContainBoundBox(position, inRange))
                    {
                        list.Remove(list[i]);
                        i--;
                    }

                    // TODO @minjun.ha 타겟 아이디는 이렇게 쓰는게 아님
                    //else if (inTarget.target_id != "None" && list[i].CheckCondition(EProperty.Id, EValueType.Compare, inTarget.target_id.FloatValue()) == -1)
                    //{
                    //    list.Remove(list[i]);
                    //    i--;
                    //}
                }
            }

            if (targetAList.Count == 0 && targetBList.Count == 0)
            {
                return null; /// 타겟이 없을때
            }
        }

        // 분류 속성 비교
        {
            EValueType eValueType = inTarget.GetValueType();
            EProperty eProperty   = inTarget.GetProperty();
            ESplitProperty eSplitProperty = inTarget.GetSplitProperty();

            if (eProperty == EProperty.Random)
            {
                if (targetAList.Count >= 1)
                {
                    targetAList.ShuffleList(ref targetAList);
                }
                if (targetBList.Count >= 1)
                {
                    targetBList.ShuffleList(ref targetBList);
                }
            }
            else
            {
                /// 랜덤인 경우에는 굳이 정렬할 필요없음
                if (eValueType == EValueType.Ascending)
                {
                    if (targetAList.Count != 0)
                    {
                        targetAList = targetAList.OrderBy(x => (int)x.CheckCondition(eProperty, eSplitProperty, EValueType.Ascending, 0, InUseObj)).ToList();
                    }
                    if (targetBList.Count != 0)
                    {
                        targetBList = targetBList.OrderBy(x => (int)x.CheckCondition(eProperty, eSplitProperty, EValueType.Ascending, 0, InUseObj)).ToList();
                    }
                }
                else if (eValueType == EValueType.Descending)
                {
                    if (targetAList.Count != 0)
                    {
                        targetAList = targetAList.OrderByDescending(x => (int)x.CheckCondition(eProperty, eSplitProperty, EValueType.Descending, 0, InUseObj)).ToList();
                    }
                    if (targetBList.Count != 0)
                    {
                        targetBList = targetBList.OrderByDescending(x => (int)x.CheckCondition(eProperty, eSplitProperty, EValueType.Descending, 0, InUseObj)).ToList();
                    }
                }
            }
        }

        // 대상 범위는 스킬의 콜라이더 크기
        // 대상수 선정
        {
            targetResultList = targetBList.Count == 0 ? targetAList : targetAList.Union(targetBList).ToList();

            // 이후 충돌 범위에 사용되기 때문에 타겟을 자르면 안됨
            _targetAllList = targetResultList;

            int range = inTarget.value;
            if (range > targetResultList.Count)
            {
                range = targetResultList.Count;
            }
            targetResultList = targetResultList.GetRange(0, range);
        }

        return targetResultList;
    }

    /// <summary>
    /// 스킬이 발동될 수 있는 조건인지 체크
    /// 
    /// 1. 발동 확률 체크
    /// 2. 타겟 오브젝트 그룹 선정
    /// 3. 조건 비교
    /// 
    /// </summary>
    /// <param name="inCondition"></param>
    /// <param name="inTag"></param>
    /// <returns></returns>
    private bool CheckCondition(SkillCondition inCondition, ETag inTag)
    {
        List<IProperty> targetList = new List<IProperty>();

        // 발동 확률 체크
        {
            float percent = Random.Range(0, 100);
            if (inCondition.excute_percent < percent)
            {
                return false; /// 미발동
            }
        }

        // 타겟 오브젝트 그룹 선정
        {
            ETarget target = inCondition.GetTarget();
            if (target == ETarget.All)
            {
                targetList.AddRange(_playerBattleObject);
                targetList.AddRange(_enemyBattleObject);
            }
            else
            {
                if (inTag == ETag.Player)
                {
                    switch (target)
                    {
                        case ETarget.Player: targetList.AddRange(_playerBattleObject); break;
                        case ETarget.Enemy: targetList.AddRange(_enemyBattleObject); break;
                    }
                }
                else if (inTag == ETag.Enermy)
                {
                    switch (target)
                    {
                        case ETarget.Player: targetList.AddRange(_enemyBattleObject); break;
                        case ETarget.Enemy: targetList.AddRange(_playerBattleObject); break;
                    }
                }
            }
         
            if (targetList.Count == 0)
            {
                return false; /// 타겟이 없을때
            }
        }

        // 조건 비교
        {
            EProperty eProperty   = inCondition.GetProperty();
            EValueType eValueType = inCondition.GetValueType();

            bool isSuccess = false;
            for (int i = 0; i < targetList.Count;i++)
            {
                if (targetList[i].CheckCondition(eProperty, ESplitProperty.Value, eValueType, inCondition.value) != -1)
                {
                    // 1조건만 일치해도 가능하므로 더이상 반복문을 실행할 필요없음
                    isSuccess = true;
                    break;
                }
            }
            if (!isSuccess)
            {
                return false; /// 조건 일치가 없을때
            }
        }
        return true;
    }

    /// <summary>
    /// 스킬 데이터 검증
    /// 
    /// - Contain Key
    /// - Enum Parsing
    /// - Percent Range
    /// 
    /// </summary>
    /// <param name="inSkill"></param>
    /// <returns></returns>
    private bool VerificationSkill(Skill inSkill)
    {
        if (_verificationSkillList.Contains(inSkill))
            return true;

        int condition_id = inSkill.excute_condition_id;
        int effect_id = inSkill.excute_effect_id;
        int target_id = inSkill.excute_target_id;

        bool complete = true;

        if (_playerBattleObject == null || _enemyBattleObject == null)
        {
            complete = false;
        }
        else if (!Statics.Instance.skill_condition.ContainsKey(condition_id))
        {
            Debug.LogError(StringHelper.Format("excute_condition {0} not exist id", condition_id));
            complete = false;
        }
        else
        {
            SkillCondition condition = Statics.Instance.skill_condition[condition_id];
            if (condition.GetConditionType() == EConditionType.None)
            {
                Debug.LogError(StringHelper.Format("condition_type {0} parsing error", condition.condition_type));
                complete = false;
            }
            if (condition.GetTarget() == ETarget.None)
            {
                Debug.LogError(StringHelper.Format("condition_target {0} parsing error", condition.target));
                complete = false;
            }
            if (condition.GetProperty() ==  EProperty.None)
            {
                Debug.LogError(StringHelper.Format("condition_property {0} parsing error", condition.property));
                complete = false;
            }
            if (condition.GetValueType() == EValueType.None)
            {
                Debug.LogError(StringHelper.Format("condition_value_type {0} parsing error", condition.value_type));
                complete = false;
            }
            if (condition.excute_percent < 0 || condition.excute_percent > 100)
            {
                Debug.LogError(StringHelper.Format("condition_excute_percent {0} out of range must be 0 ~ 100 value", condition.excute_percent));
                complete = false;
            }
        }

        if (!Statics.Instance.skill_effect.ContainsKey(effect_id))
        {
            Debug.LogError(StringHelper.Format("excute_effect {0} not exist id", effect_id));
            complete = false;
        }
        else
        {
            SkillEffect effect = Statics.Instance.skill_effect[effect_id];
            if (effect.GetEffectType() ==  EEffectType.Null)
            {
                Debug.LogError(StringHelper.Format("effect_type {0} parsing error", effect.effect_type));
                complete = false;
            }
            if (effect.GetSplitProperty() == ESplitProperty.Null)
            {
                Debug.LogError(StringHelper.Format("effect_split_property {0} parsing error", effect.split_property));
                complete = false;
            }
        }

        if (!Statics.Instance.skill_target.ContainsKey(target_id))
        {
            Debug.LogError(StringHelper.Format("excute_target {0} not exist id", target_id));
            complete = false;
        }
        else
        {
            SkillTarget target = Statics.Instance.skill_target[target_id];
            if (target.GetTarget() == ETarget.None)
            {
                Debug.LogError(StringHelper.Format("target_type {0} parsing error", target.target));
                complete = false;
            }
            if (target.GetProperty() == EProperty.None)
            {
                Debug.LogError(StringHelper.Format("target_property {0} parsing error", target.property));
                complete = false;
            }
            if (target.GetSplitProperty() == ESplitProperty.Null)
            {
                Debug.LogError(StringHelper.Format("target_split_property {0} parsing error", target.split_property));
                complete = false;
            }
            if (target.GetValueType() == EValueType.None)
            {
                Debug.LogError(StringHelper.Format("target_condition_type {0} parsing error", target.value_type));
                complete = false;
            }
        }

        // 검증된 스킬을 다시 검증할 필요는 없기 때문에 등록
        if (complete && !_verificationSkillList.Contains(inSkill))
        {
            _verificationSkillList.Add(inSkill);
        }
        return complete;
    }

    /// <summary>
    /// BattleObject List의 데이터를 Skill Controller 전용 Data List로 변환한다 
    /// </summary>
    /// <param name="inBattleList">배틀 오브젝트 리스트</param>
    /// <returns></returns>
    private List<Data> ConvertDataList(List<BattleObject> inBattleList)
    {
        List<Data> list = new List<Data>();
        for (int i = 0; i < inBattleList.Count; i++)
        {
            BattleObject battleObject = inBattleList.ElementAt(i);
            if (battleObject == null)
            {
                // 해당 데이터는 있지만 생성이 되지 않은 경우 (죽었나..?)
                continue;
            }

            Data data = new Data();
            data.battleObject = battleObject;
            data.playData = battleObject.Data;
            list.Add(data);
        }
        return list;
    }

    /// <summary>
    /// 타겟별로 관련 오브젝트를 모은다
    /// </summary>
    /// <param name="inPlayerList"></param>
    /// <param name="inEnemyList"></param>
    private void GatherList(List<Data> inPlayerList, List<Data> inEnemyList)
    {
        _playerBattleObject = new List<IProperty>();
        _enemyBattleObject  = new List<IProperty>();

        foreach (var data in inPlayerList)
        {
            _playerBattleObject.Add(data.battleObject);
        }

        foreach (var data in inEnemyList)
        {
            _enemyBattleObject.Add(data.battleObject);
        }
    }
}
