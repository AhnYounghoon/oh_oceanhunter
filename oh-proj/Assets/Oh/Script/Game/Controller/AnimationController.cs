﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;

public class AnimationController : Singleton<AnimationController>
{
    public void Clear()
    {
        this.transform.DestroyChildren();
        EffectManager.it.DestroyEffects("");
    }

    public void ShowTutorialWave(Vector3 inPosition, System.Action inWaveAction)
    {
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/TutorialWave"));
        go.transform.SetParent(this.transform);
        go.transform.position = inPosition;

        System.IDisposable stream = null;
        stream = Observable.Interval(System.TimeSpan.FromSeconds(1.2f))
            .Subscribe(_ =>
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
                if (inWaveAction != null)
                {
                    inWaveAction();
                }
            })
            .AddTo(this);
    }

    public void ShowKrakenAttack(Vector3 inPosition)
    {
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/KrakenSkill_1"));
        go.transform.SetParent(this.transform);
        go.transform.position = inPosition;

        System.IDisposable stream = null;
        stream = Observable.Interval(System.TimeSpan.FromSeconds(1.0f))
            .Subscribe(_ =>
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
                ShowDamageScore(new Vector3(inPosition.x + 100, inPosition.y, inPosition.z), 1);
                ShowDamageScore(new Vector3(inPosition.x - 100, inPosition.y, inPosition.z), 1);
                ShowDamageScore(inPosition, 1);
            })
            .AddTo(this);
    }

    public void ShowStageClear(System.Action inCallback = null)
    {
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/stageClear"));
        go.transform.SetParent(this.transform);
        go.transform.DOScale(new Vector3(2, 2, 1), 1.5f)
            .SetEase(Ease.OutCubic)
            .SetAs(go.GetComponent<SpriteRenderer>().DOFade(1f, 1.5f))
            .OnComplete(() =>
            {
                go.GetComponent<SpriteRenderer>().DOFade(0, 1.5f)
                .OnComplete(() => 
                {
                    MonoBehaviour.Destroy(go);
                    if (inCallback != null)
                    {
                        inCallback();
                        inCallback = null;
                    }
                });
            })
            .SetAutoKill();
    }

    public void ShowDamageScore(Vector3 inStartPosition, float inDamage)
    {
        inStartPosition.y += 50;

        float value = Random.Range(1, 3) > 1 ? -4 : 4;

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/ScoreType_1"));
        TextMeshPro pro = go.GetComponent<TextMeshPro>();
        pro.transform.SetParent(this.transform);
        pro.transform.position = inStartPosition;
        pro.GetComponent<TextMeshPro>().text = inDamage.ToString();
        pro.transform.DOLocalMoveY(65, 0.5f)
            .SetEase(Ease.OutBack)
            .SetAs(pro.DOFade(0, 0.5f).SetDelay(0.5f))
            .OnComplete(() =>
            {
                MonoBehaviour.Destroy(pro.gameObject);
            })
            .SetAutoKill();
    }

    public void ShowMiss(Vector3 inStartPosition)
    {
        inStartPosition.y += 50;

        float value = Random.Range(1, 3) > 1 ? -4 : 4;

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/ScoreType_3"));
        TextMeshPro pro = go.GetComponent<TextMeshPro>();
        pro.transform.SetParent(this.transform);
        pro.transform.position = inStartPosition;
        pro.transform.DOLocalMoveY(65, 0.5f)
            .SetEase(Ease.OutBack)
            .SetAs(pro.DOFade(0, 0.5f).SetDelay(0.5f))
            .OnComplete(() =>
            {
                MonoBehaviour.Destroy(pro.gameObject);
            })
            .SetAutoKill();
    }

    public void ShowHealScore(Vector3 inStartPosition, float inValue)
    {
        inStartPosition.y += 50;

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/ScoreType_2"));
        go.transform.SetParent(this.transform);
        go.transform.position = inStartPosition;
        go.GetComponent<TextMeshPro>().text = inValue.ToString();
        go.transform.DOLocalMoveY(65, 0.5f)
            .SetEase(Ease.OutBack)
            .OnComplete(() =>
            {
                MonoBehaviour.Destroy(go);
            })
            .SetAutoKill();
    }

    public void ShowExplodeEffect(Vector3 inPosition)
    {
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/ExplodeEffect"));
        go.transform.SetParent(this.transform);
        go.transform.position = inPosition;
    }

    public void CharacterSkill(ServerStatic.SkillEffect inEffect, GameObject inUseObj, IProperty inTargetProperty, System.Action<Vector3> inEffectCallback, System.Action inCompleteCallback)
    {
        if (inEffect.resource_path == "Fire")
        {
            StartCoroutine(Flamethrow(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty));
        }
        else if (inEffect.resource_path == "Heal")
        {
            StartCoroutine(HealEffect(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty));
        }
        else if (inEffect.resource_path == "LaunchFire")
        {
            StartCoroutine(LaunchFire(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, new Vector3(1, 1, 1)));
        }
        else if (inEffect.resource_path == "LaunchBlue")
        {
            StartCoroutine(LaunchFireBlue(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, new Vector3(1, 1, 1)));
        }
        else if (inEffect.resource_path == "TestBomb")
        {
            StartCoroutine(ShootCannon(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty));
        }
        else if (inEffect.resource_path == "TestMagic")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 1));
        }
        else if (inEffect.resource_path == "TestMagic_1")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 2));
        }
        else if (inEffect.resource_path == "TestMagic_2")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 3));
        }
        else if (inEffect.resource_path == "TestMagic_3")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 4));
        }
        else if (inEffect.resource_path == "TestMagic_4")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 5));
        }
        else if (inEffect.resource_path == "TestMagic_5")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 6));
        }
        else if (inEffect.resource_path == "TestMagic_6")
        {
            StartCoroutine(ShootFireBall(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty, 7));
        }
        else if (inEffect.resource_path == "ShootFire")
        {
            StartCoroutine(ShootFire(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty));
        }
        else
        {
            StartCoroutine(ShootCannon(inEffect, inEffectCallback, inCompleteCallback, inUseObj, inTargetProperty));
            Debug.LogError(StringHelper.Format("Cannot Define {0} animation!", inEffect.resource_path));
        }
    }

    IEnumerator ShootCannon(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty)
    {
        Vector3 inStartPosition = inUseObj.transform.position;
        Vector3 inEndPosition   = inTargetProperty.GetHitAblePosition();

        inStartPosition.y += 20;

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/CannonBall"));
        go.transform.SetParent(this.transform);
        go.transform.position = inStartPosition;
        go.transform.DOMove(inEndPosition, 1.0f)
            .SetEase(Ease.Linear)
            .OnComplete(() => 
            {
                Destroy(go);
                ShowExplodeEffect(inEndPosition);
                if (inEffectCallback != null)
                {
                    inEffectCallback(inEndPosition);
                }
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });
        yield break;
    }

    IEnumerator ShootFireBall(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty, int inIndex)
    {
        Vector3 inStartPosition = inUseObj.transform.position;
        Vector3 inEndPosition = inTargetProperty.GetHitAblePosition();

        inStartPosition.y += 20;

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/FireBall_" + inIndex));
        go.transform.SetParent(this.transform);
        go.transform.position = inStartPosition;
        go.transform.DOMove(inEndPosition, 1.5f)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                Destroy(go);
                ShowExplodeEffect(inEndPosition);
                if (inEffectCallback != null)
                {
                    inEffectCallback(inEndPosition);
                }
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });
        yield break;
    }

    IEnumerator ShootFire(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty)
    {
        Vector3 inStartPosition = inUseObj.transform.position;
        Vector3 inEndPosition   = inTargetProperty.GetHitAblePosition();

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/FireShoot"));
        go.transform.SetParent(this.transform);
        go.transform.position = inEndPosition;
        go.transform.DOMove(inEndPosition + Vector3.down, 1.5f)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                Destroy(go);
                ShowExplodeEffect(inEndPosition);
                if (inEffectCallback != null)
                {
                    inEffectCallback(inEndPosition);
                }
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });
        yield break;
    }

    IEnumerator LaunchFire(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty, Vector3 inScale)
    {
        Vector3 startPosition = inUseObj.transform.position;
        Vector3 inEndPosition = inTargetProperty.GetHitAblePosition();

        GameObject go = null;
        var update = inUseObj.transform
            .ObserveEveryValueChanged(x => x.position)
            .DistinctUntilChanged()
            .Subscribe(_ =>
            {
                if (go != null)
                {
                    Vector3 position = inUseObj.transform.position;
                    go.transform.position = position;
                }
            });

        float interval = inEffect.time / inEffect.count;
        var stream = Observable.Interval(System.TimeSpan.FromSeconds(interval))
            .Subscribe(_ =>
            {
                go.transform.LookAt(inTargetProperty.GetHitAblePosition());
                if (inEffectCallback != null)
                {
                    inEffectCallback(inEndPosition);
                }
            })
            .AddTo(this);

        var timer = Observable.Timer(System.TimeSpan.FromSeconds(3.0f))
            .Subscribe(_ =>
            {
                MonoBehaviour.Destroy(go);

                update.Dispose();
                stream.Dispose();
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });

        go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/FireEffect"));
        go.transform.SetParent(this.transform);
        go.transform.position = startPosition;
        go.transform.localScale = inScale;
        go.transform.LookAt(inTargetProperty.GetHitAblePosition());
        yield break;
    }

    IEnumerator LaunchFireBlue(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty, Vector3 inScale)
    {
        Vector3 startPosition = inUseObj.transform.position;
        Vector3 inEndPosition = inTargetProperty.GetHitAblePosition();

        GameObject go = null;
        var update = inUseObj.transform
            .ObserveEveryValueChanged(x => x.position)
            .DistinctUntilChanged()
            .Subscribe(_ =>
            {
                if (go != null)
                {
                    Vector3 position = inUseObj.transform.position;
                    go.transform.position = position;
                }
            });

        float interval = inEffect.time / inEffect.count;
        var stream = Observable.Interval(System.TimeSpan.FromSeconds(interval))
            .Subscribe(_ =>
            {
                go.transform.LookAt(inTargetProperty.GetHitAblePosition());
                if (inEffectCallback != null)
                {
                    inEffectCallback(inEndPosition);
                }
            })
            .AddTo(this);

        var timer = Observable.Timer(System.TimeSpan.FromSeconds(3.0f))
            .Subscribe(_ =>
            {
                MonoBehaviour.Destroy(go);

                update.Dispose();
                stream.Dispose();
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            });

        go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/FireEffectBlue"));
        go.transform.SetParent(this.transform);
        go.transform.position = startPosition;
        go.transform.localScale = inScale;
        go.transform.LookAt(inTargetProperty.GetHitAblePosition());
        yield break;
    }


    IEnumerator HealEffect(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty)
    {
        Vector3 targetPosition = inTargetProperty.GetHitAblePosition();
        if (inEffectCallback != null)
        {
            inEffectCallback(targetPosition);
        }

        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/Heal"));
        go.transform.SetParent(this.transform);
        go.transform.position = targetPosition;
        go.GetComponent<CustomParticle>().RegistEndCallback(() => 
        {
            Destroy(go);
            if (inEndCallback != null)
            {
                inEndCallback();
            }
        });
        yield break;
    }

    IEnumerator ShootBullet(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty)
    {
        Vector3 inStartPosition = inUseObj.transform.position;
        Vector3 inEndPosition = inTargetProperty.GetHitAblePosition();

        inStartPosition.y += 100;
        GameObject go = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/BattleScene/Effect/bullet"));
        go.transform.SetParent(this.transform);
        go.transform.position = inStartPosition;
        go.transform.localScale = new Vector2(0.2f, 0.2f);
        go.transform.DOMove(inEndPosition, inEffect.time)
            .SetEase(Ease.OutQuad)
            .OnComplete(() =>
            {
                if (inEffectCallback != null)
                {
                    MonoBehaviour.Destroy(go);
                    inEffectCallback(inEndPosition);
                }
                if (inEndCallback != null)
                {
                    inEndCallback();
                }
            })
            .SetAutoKill();
        yield break;
    }

    IEnumerator Flamethrow(ServerStatic.SkillEffect inEffect, System.Action<Vector3> inEffectCallback, System.Action inEndCallback, GameObject inUseObj, IProperty inTargetProperty)
    {
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(LaunchFire(inEffect, inEffectCallback, inEndCallback, inUseObj, inTargetProperty, new Vector3(4, 4, 4)));
    }
}
