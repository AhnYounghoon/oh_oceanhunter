﻿public static class EnumExtensionMethod
{
    public static ETag GetTargetTag(this ETag inTag)
    {
        ETag tag = ETag.Enermy;
        if (inTag == ETag.Enermy)
        {
            tag = ETag.Player;
        }
        return tag;
    }
}

public enum ELocation
{ 
    Front,
    Middle,
    Back,
}

public enum ETag
{
    Player,
    Enermy,
    Miss,
    Ocean
}

public enum EDirection
{
    DIR_NONE = -1,
    DIR_TOP,
    DIR_TR,
    DIR_RIGHT,
    DIR_BR,
    DIR_BOTTOM,
    DIR_BL,
    DIR_LEFT,
    DIR_TL,
    COUNT
};

/// <summary>
/// 선박 이동 이벤트
/// </summary>
public enum EMovingEvent
{
    SUCCESS,
    NOT_ENOUGH_FUEL,
    DO_NOT_FIND_ROUTE,
    ARRIVED_AT_GOAL,
    ARRIVED_AT_HARBOR
}

/// <summary>
/// 해양이 변경되는 상황
/// </summary>
public enum EOceanChange
{
    TILE_APPEAR = 1,
    TOKEN_APPEAR,
    SEA_ROUTE_APPEAR
}

public enum EBattleObjectType
{
    None,
    Ship,
    Tutorial,
    Monster,
}

public enum ECharacterType
{
    Attack = 1,
    Magic,
    Support,
    Defense
}

namespace BattleEnum
{
    public enum EBattleType
    {
        Attack,
        Support,
        Magic,
        Defense,
    }

    /// <summary>
    /// 값 분류 조건
    /// </summary>
    public enum EValueType
    {
        None = 0,
        Compare,
        Ascending,
        Descending,
    }

    /// <summary>
    /// 조건 타입
    /// </summary>
    public enum EConditionType
    {
        None = 0,
        Stat,
        Behavior,
        Exist,
        All,
    }

    /// <summary>
    /// 배틀 씬 타겟
    /// </summary>
    public enum ETarget
    {
        None = 0,
        Player,
        Enemy,
        All
    }

    /// <summary>
    /// 오브젝트 속성
    /// </summary>
    public enum EProperty
    {
        None = 0,
        Id,
        Damage,
        Hp,
        Distance,
        Arrive,
        Random,
        Critical_Damage,
        Hit,
        Penetrate,               // 관통 데미지
        Defense,
    }

    public enum EEffectType
    {
        Null = 0,
        None,
        Attack,
        Buff,
        DeBuff,
        Heal,
    }

    public enum ESplitProperty
    {
        Null = 0,
        None,
        Stat,
        Stun,
        Percent,
        Value,
    }
}
