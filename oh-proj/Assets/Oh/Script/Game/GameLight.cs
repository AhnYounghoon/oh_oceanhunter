﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameLight : MonoBehaviour
{
    [SerializeField]
    Light _directionalLight;

    [SerializeField]
    Transform _skillLight;

    [SerializeField]
    Light _spotLight;

    [SerializeField]
    MeshRenderer _showSpotModel;

    /// <summary>
    /// 스킬 모드용 라이팅 셋팅
    /// </summary>
    /// <param name="inTransform"></param>
    /// <param name="isActive"></param>
    /// <param name="inEndCallback"></param>
    public void SetSkillMode(Transform inTransform, bool isActive, System.Action inEndCallback = null)
    {
        float from = isActive ? 0 : 1;
        float to   = isActive ? 1 : 0;
        float time = 0.5f;

        _skillLight.SetParent(inTransform);
        _skillLight.localPosition = Vector3.zero;
        _showSpotModel.gameObject.SetActive(true);

        DOTween.To(value => _directionalLight.intensity = value, to  , from, time);
        DOTween.To(value => _spotLight.intensity = value,        from * 45 , to * 45, time);
        DOTween.To(value => 
        {
            _showSpotModel.material.SetColor("_Color", STZCommon.ToUnityColor(255,255,255, value));
        }, from * 100, to * 100, time)
            .OnComplete(() => 
            {
                if (inEndCallback != null)
                {
                    inEndCallback();
                }

                if (!isActive)
                {
                    _showSpotModel.gameObject.SetActive(false);
                }
            });
    }
}