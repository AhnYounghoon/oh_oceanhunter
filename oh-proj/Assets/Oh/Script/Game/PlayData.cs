﻿using System.Collections.Generic;
using UnityEngine;

public class PlayData : ServerStatic.BattleObject
{
    private readonly Vector3 DEFAULT_POSITION   = new Vector3(-150, 0, 55);
    private readonly Vector3 DEFAULT_SCALE      = new Vector3(1, 1, 1);
    private readonly Quaternion DEFAULT_ROTATION = Quaternion.Euler(0, 0, 0);

    public ETag         tag;
    public Vector3      startScale;
    public Vector3      startPosition;
    public Quaternion   startRotation;
    public int          wave;
    public int          characterId;
    public bool         boss;
    public float        currentHp;

    public ServerStatic.Character heroData;
    public ServerStatic.Character sailorData;

    public PlayData()
    {
        startScale    = DEFAULT_SCALE;
        startPosition = DEFAULT_POSITION;
        startRotation = DEFAULT_ROTATION;
        wave          = 1;
        characterId   = 1;
        boss          = false;
    }

    public PlayData(ServerStatic.BattleObject obj)
    {
        startScale    = DEFAULT_SCALE;
        startPosition = DEFAULT_POSITION;
        startRotation = DEFAULT_ROTATION;
        BaseOn(obj);
    }

    /// <summary>
    /// 플레이 데이터에 스탯을 적용한다
    /// </summary>
    /// 
    /// <!-- 해당 데이터만 적용 가능 -->
    /// - Damage
    /// - Hp
    /// - Defense
    /// 
    /// <param name="inProperty">스탯</param>
    /// <param name="percent">기준은 1.0f</param>
    public void ChangeStat(BattleEnum.EProperty inProperty, float percent)
    {
        if (inProperty == BattleEnum.EProperty.Hp)
        {
            hp *= percent;
        }
        else if (inProperty == BattleEnum.EProperty.Defense)
        {
            defense *= percent;
        }
        else if (inProperty == BattleEnum.EProperty.Damage)
        {
            damage *= percent;
        }
    }

    /// <summary>
    /// 해당 스태틱에 기초하여 데이터를 셋팅한다
    /// </summary>
    /// <param name="obj"></param>
    public void BaseOn(ServerStatic.BattleObject obj)
    {
        id          = obj.id;
        name        = obj.name;
        action_type = obj.action_type;
        prefab_path = obj.prefab_path;
        resource_path = obj.resource_path;
        skin_id     = obj.skin_id;
        level       = obj.level;
        max_level   = obj.level;
        hp          = obj.hp;
        defense     = obj.defense;
        speed       = obj.speed;
        fuel        = obj.fuel;
        inventory   = obj.inventory;
        ability     = obj.ability;
        size        = obj.size;
        offset      = obj.offset;
        attack_range = obj.attack_range;
        attack_id   = obj.attack_id;
        skill_id    = obj.skill_id;
        data        = obj.data;
        damage      = obj.damage;
        currentHp   = obj.hp;

        float value = size / 30.0f;
        startScale = new Vector3(value, value, value); 
    }
}

public class PlayerPlayData : PlayData
{
    private readonly Vector3 START_POSITION = new Vector3(-150, 10, 55);
    private readonly Vector3 INIT_SCALE     = new Vector3(1,1,1);

    public PlayerPlayData(int inHeroId, int inSailorId)
    {
        var hero   = Statics.Instance.character.ContainsKey(inHeroId)   ? Statics.Instance.character[inHeroId]   : null;
        var sailor = Statics.Instance.character.ContainsKey(inSailorId) ? Statics.Instance.character[inSailorId] : null;
        Init(hero, sailor);
    }

    public PlayerPlayData(ServerStatic.Character inHeroData, ServerStatic.Character inSailorData)
    {
        Init(inHeroData, inSailorData);
    }

    private void Init(ServerStatic.Character inHeroData, ServerStatic.Character inSailorData)
    {
        tag = ETag.Player;
        startPosition = START_POSITION;
        startScale = INIT_SCALE;
        heroData = inHeroData;
        sailorData = inSailorData;
        BaseOn(Statics.Instance.battle_object[heroData.battle_object_id.IntValue()]);
        ApplyHeroStat();
        ApplySailorStat();
    }

    private void ApplyHeroStat()
    {
        this.name = heroData.name;
        this.hp += heroData.hp;
        this.defense += heroData.defense;
        this.speed += heroData.speed;
        this.attack_range += heroData.attack_range;
        this.damage += heroData.damage;
        this.attack_id = heroData.attack_id;
        this.skill_id = heroData.skill_id;
        this.action_type = heroData.type;
    }

    private void ApplySailorStat()
    {
        if (sailorData == null)
            return;

        int loop = (int)(heroData.leadership / sailorData.cost);
        for (int i = 0; i < loop;i++)
        {
            this.hp += sailorData.hp;
            this.attack_range += sailorData.attack_range;
            this.defense += sailorData.defense;
            this.speed += sailorData.speed;
            this.damage += sailorData.damage;
        }
        currentHp = this.hp;
    }
}

public class EnemyPlayData : PlayData
{
    private readonly Vector3 INIT_POSITION = new Vector3(150, 0, 85);
    private readonly Vector3 INIT_SCALE     = new Vector3(1, 1, 1);
    private readonly Quaternion INIT_ROTATION = Quaternion.Euler(0, 180, 0);

    public EnemyPlayData()
    {
        tag            = ETag.Enermy;
        startPosition  = INIT_POSITION;
        startScale     = INIT_SCALE;
        startRotation  = INIT_ROTATION;
        BaseOn(Statics.Instance.battle_object[12]);
    }
}
