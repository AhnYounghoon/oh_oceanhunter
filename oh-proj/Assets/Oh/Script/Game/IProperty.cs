﻿using UnityEngine;

public interface IProperty
{
    /// <summary>
    /// 해당 조건에 맞는 지 반환
    /// </summary>
    /// <param name="inProperty">속성</param>
    /// <param name="inValueType">반환값</param>
    /// <param name="inValue">값</param>
    /// <returns>
    /// 
    /// <see cref="BattleEnum.EValueType.Compare"/>
    /// - Compare일때 -1은 불일치 나머지는 일치로 판단한다
    /// 
    /// <see cref="BattleEnum.EValueType.Ascending"/>
    /// <see cref="BattleEnum.EValueType.Descending"/>
    /// - 해당 값을 반환한다
    /// 
    /// </returns>
    int CheckCondition(BattleEnum.EProperty inProperty, BattleEnum.ESplitProperty inSplitType, 
        BattleEnum.EValueType inValueType, float inValue, IProperty inTarget = null);

    /// <summary>
    /// 상태이상을 적용한다
    /// </summary>
    /// <param name="inProperty"></param>
    void ApplyCondition(BattleEnum.ESplitProperty inProperty);

    /// <summary>
    /// 해당 오브젝트의 충돌 여부
    /// </summary>
    /// <param name="inPosition"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    bool ContainBoundBox(Vector3 inPosition, float size);

    void SetBodyLayer(int layer);

    /// <summary>
    /// 타격 가능한 위치를 가져옴
    /// </summary>
    /// <returns></returns>
    Vector3 GetHitAblePosition();
}
