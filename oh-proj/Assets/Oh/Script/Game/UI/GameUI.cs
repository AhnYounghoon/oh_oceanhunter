﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class GameUI : MonoBehaviour
{
    [Tooltip("자동 플레이 텍스트 색상")]
    [SerializeField]
    Text _txtAuto;

    [Tooltip("자동 플레이 버튼")]
    [SerializeField]
    Button _btnAuto;

    [Tooltip("캐릭터 스킬 아이콘")]
    [SerializeField]
    CharacterSkillIcon[] _characterSkill;

    [SerializeField]
    Text _txtStageClear;

    [SerializeField]
    Text _txtStageFail;

    [SerializeField]
    Text _txtWave;

    [SerializeField]
    Transform _battleNoti;
    
    [SerializeField]
    Text _txtBattleNoti;

    [SerializeField]
    CanvasGroup _imgFadeBox;

    [SerializeField]
    GameObject _bottomUI;

    public CharacterSkillUI skillUI;
    public BossUI           bossUI;

    public event CharacterSkillIcon.UseSkillEventHandler UseSkillEvent
    {
        add
        {
            foreach (var skill in _characterSkill)
            {
                if (skill == null)
                    continue;

                skill.useSkillHandler += value;
            }
        }
        remove
        {
            foreach (var skill in _characterSkill)
            {
                if (skill == null)
                    continue;

                skill.useSkillHandler -= value;
            }
        }
    }

    private void Start()
    {
        // 맵툴 해상도로 세팅
        if (GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.MapTool)
            this.Get<CanvasScaler>().referenceResolution = new Vector2(MapToolManager.MAPTOOL_WIDTH, MapToolManager.MAPTOOL_HEIGHT);

        Clear();
    }

    public void Clear()
    {
        foreach (var skill in _characterSkill)
        {
            skill.Clear();
        }
    }

    public BattleObject.ChangeHpEventHandler GetHpUIEvent(int inIndex)
    {
        if (_characterSkill.Length <= inIndex)
            return null;

        return _characterSkill[inIndex].ChangeHpEvent;
    }

    /// <summary>
    /// 게임 시작시 호출되는 함수
    /// </summary>
    public void SetGameStartUI()
    {
        if (_characterSkill == null)
        {
            return;
        }

        foreach (var character in _characterSkill)
        {
            if (character == null)
            {
                continue;
            }
            character.SetGameStartUI();
        }

        GameInfo.isAuto = false;
        OnAutoPlay();
        this.GetComponent<CanvasGroup>().alpha = 0;
    }

    public void ShowNoti(string inText)
    {
        _battleNoti.localPosition = new Vector2(-730, 306);
        _battleNoti.DOLocalMoveX(-550, 0.5f);
        _txtBattleNoti.text = inText;
    }

    public void FadeInUI()
    {
        CanvasGroup group = this.GetComponent<CanvasGroup>();
        group.DOFade(1, 0.8f);
    }

    public void FadeIn(float inDelay)
    {
        _imgFadeBox.alpha = 0;
        _imgFadeBox.DOFade(1, 2.0f).SetDelay(inDelay).OnComplete(() => 
        {
        });
    }

    public void FadeOut()
    {
        _imgFadeBox.alpha = 1;
        _imgFadeBox.DOFade(0, 2.0f).OnComplete(() =>
        {
        });
    }

    /// <summary>
    /// 캐릭터 데이터를 기반으로 UI를 셋팅
    /// </summary>
    /// <param name="inDataList"></param>
    public void SetCharacterData(List<PlayData> inDataList)
    {
        int count = 0;

        float[] position = new float[] { 317, 243, 163, 89, 0 };
        _bottomUI.transform.SetPositionX(position[inDataList.Count - 1]);
        for (int i = 0; i < _characterSkill.Length; i++)
        {
            if (inDataList.Count <= i)
            {
                _characterSkill[i].gameObject.SetActive(false);
                continue;
            }
            PlayData data = inDataList[i];
            if (data == null)
            {
                continue;
            }
            _characterSkill[count].Init(data);
            count++;
        }
    }

    public void ShowStageClear(System.Action inEndCallback)
    {
        _txtStageClear.color = STZCommon.ToUnityColor(255, 255, 255, 0);
        _txtStageClear.transform.localScale = new Vector3(4, 4, 4);
        _txtStageClear.DOFade(1, 0.5f).SetDelay(2.0f);
        _txtStageClear.transform.DOScale(1, 0.5f)
            .SetDelay(2.0f)
            .OnComplete(() => 
        {
            if (inEndCallback != null)
            {
                inEndCallback();
            }
        });
    }

    public void ShowStageFail(System.Action inEndCallback)
    {
        _txtStageFail.color = STZCommon.ToUnityColor(255, 0, 0, 0);
        _txtStageFail.transform.localScale = new Vector3(4, 4, 4);
        _txtStageFail.DOFade(1, 0.5f);
        _txtStageFail.transform.DOScale(1, 0.5f).OnComplete(() =>
        {
            if (inEndCallback != null)
            {
                inEndCallback();
            }
        });
    }

    public void ShowWave(int inWave, System.Action inEndCallback = null)
    {
        _txtWave.text = StringHelper.Format("{0} WAVE", inWave);
    }

    public void OnAutoPlay()
    {
        GameInfo.isAuto = !GameInfo.isAuto;
        if (GameInfo.isAuto)
        {
            for (int i = 0; i < _characterSkill.Length; i++)
            {
                //_characterSkill[i].ActiveSkill();
            }

            _txtAuto.color = STZCommon.ToUnityColor(54, 96, 255);
            _btnAuto.transform.localRotation = Quaternion.Euler(0, 0, 0);
            _btnAuto.transform.DOLocalRotate(new Vector3(0, 0, -180), 1)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Incremental);
        }
        else
        {
            _txtAuto.color = STZCommon.ToUnityColor(255, 255, 255);
            _btnAuto.transform.DOKill();
        }
    }
}
