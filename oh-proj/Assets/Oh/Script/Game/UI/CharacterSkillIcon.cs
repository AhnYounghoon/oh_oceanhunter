﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CharacterSkillIcon : MonoBehaviour
{
    public delegate void UseSkillEventHandler(PlayData data);
    public event UseSkillEventHandler useSkillHandler;

    [SerializeField]
    Image _icon;

    [SerializeField]
    Image _imgCoolTime;

    [SerializeField]
    Text _txtCoolTime;

    [SerializeField]
    Image _imgType;

    [SerializeField]
    HpBar _hpBar;

    System.IDisposable  _skillTimer;
    CustomAnimation     _customAnimation;
    PlayData            _data;
    bool                _chargeSkill;
    float               _leftTime;

    public void Init(PlayData inData)
    {
        _data = inData;
        _icon.sprite     = Resources.Load<Sprite>(_data.heroData.icon_path);
        _imgType.sprite  = Resources.Load<Sprite>("Texture/CharacterType/" + _data.heroData.type);
        _customAnimation = _icon.transform.parent.GetComponent<CustomAnimation>();
        ChangeHpEvent(_data.hp, _data.hp, _data.hp);
    }

    public void SetGameStartUI()
    {
        if (_data == null)
            return;

        RegistSkillTimer();
    }

    public void Clear()
    {
        _leftTime    = 0;
        _chargeSkill = false;

        if (useSkillHandler != null)
            useSkillHandler = null;
            
        if (_data == null)
            return;

        _data = null;
        SetSkillCharge(false);
        if (_skillTimer != null)
        {
            _skillTimer.Dispose();
            _skillTimer = null;
        }
    }

    private void OnDestroy()
    {
        Clear();
    }

    public void ChangeHpEvent(float inBefore, float inAfter, float inMax)
    {
        if (inAfter <= 0)
        {
            Clear();
            _icon.gameObject.SetActive(false);
        }
        _hpBar.SetHp(inAfter, inMax);
    }

    /// <summary>
    /// 스킬 완충
    /// </summary>
    /// <param name="b"></param>
    public void SetSkillCharge(bool b)
    {
        _chargeSkill = b;
        if (_chargeSkill)
        {
            if (_customAnimation != null)
            {
                _customAnimation.Play("Bounds", true);
            }
        }
        else
        {
            if (_customAnimation != null)
            {
                _customAnimation.Stop();
            }
            this.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    /// <summary>
    /// 스킬 사용
    /// event handler에 등록된 모든 콜백 호출
    /// [컷씬, 카메라 처리, 캐릭터 스킬 처리]
    /// </summary>
    public void ActiveSkill()
    {
        if (_chargeSkill)
        {
            SetSkillCharge(false);
            useSkillHandler(_data);
            RegistSkillTimer();
            TutorialActiveSkill();
        }
    }

    /// <summary>
    /// 캐릭터 데이터를 기반으로 스킬 타이머를 등록
    /// </summary>
    private void RegistSkillTimer()
    {
        _imgCoolTime.gameObject.SetActive(true);
        _txtCoolTime.gameObject.SetActive(true);
        _customAnimation.transform.localScale = new Vector2(1, 1);

        _leftTime   = BattleController.it.GetCoolTime(_data.skill_id.IntValue());
        _skillTimer = Observable.Interval(System.TimeSpan.FromSeconds(1))
            .Subscribe(_ => 
            {
                _leftTime--;
                _txtCoolTime.text = _leftTime.ToString();

                if (_leftTime <= 0)
                {
                    if (_skillTimer != null)
                    {
                        _skillTimer.Dispose();
                        _skillTimer = null;
                    }
                    SetSkillCharge(true);

                    if (GameInfo.isAuto)
                    {
                        //ActiveSkill();
                    }
                }
            });

        _txtCoolTime.text = _leftTime.ToString();
        DOTween.To(value => _imgCoolTime.fillAmount = value, 1, 0, _leftTime)
            .SetEase(Ease.Linear)
            .OnComplete(() => 
            {
                _txtCoolTime.gameObject.SetActive(false);
                _imgCoolTime.gameObject.SetActive(false);
            });
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ActiveSkill();
        }
    }
#endif

    #region Tutorial
    private System.Action tutorialCallback = null;
    private void TutorialActiveSkill()
    {
        if (tutorialCallback != null)
        {
            tutorialCallback();
            tutorialCallback = null;

            // NOTE @minjun 튜토리얼때는 스킬 재사용 타이머 등록을 하지 않는다.
            if (_skillTimer != null)
            {
                _skillTimer.Dispose();
                _skillTimer = null;
            }
        }
    }
    private void TutorialActiveSkillCheck(System.Action inEndCallback, int inCharacterId)
    {
        tutorialCallback = inEndCallback;
        if (_data.id == inCharacterId)
        {
            SetSkillCharge(true);
        }
    }
    private void TutorialActiveSkill_1(System.Action inEndCallback)
    {
        TutorialActiveSkillCheck(inEndCallback, 1);
    }
    private void TutorialActiveSkill_2(System.Action inEndCallback)
    {
        TutorialActiveSkillCheck(inEndCallback, 6);
    }
    private void TutorialActiveSkill_3(System.Action inEndCallback)
    {
        TutorialActiveSkillCheck(inEndCallback, 5);
    }
    private void TutorialActiveSkill_4(System.Action inEndCallback)
    {
        TutorialActiveSkillCheck(inEndCallback, 3);
    }
    #endregion
}
