﻿using UnityEngine;

public class HpBar : MonoBehaviour
{
    [SerializeField]
    Transform _gauge;

    [SerializeField]
    Transform _gaugeBack;

    [SerializeField]
    Vector2 _gaugeSize;

    [SerializeField]
    TMPro.TextMeshPro _gaugeTextMeshPro;

    [SerializeField]
    UnityEngine.UI.Text _gaugeTextUI;

    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        _gauge.transform.localScale     = _gaugeSize;
        _gaugeBack.transform.localScale = _gaugeSize;
    }

    public void SetHp(float hp, float max)
    {
        float ratio = hp / max * _gaugeSize.x;
        if (ratio < 0)
        {
            ratio = 0;
        }

        if (_gaugeTextMeshPro != null)
        {
            _gaugeTextMeshPro.text = StringHelper.Format("{0}/{1}", hp.ToString(), max);
        }
        if (_gaugeTextUI != null)
        {
            _gaugeTextUI.text = hp.ToString();
        }
        if (_gauge != null)
        {
            _gauge.transform.SetScaleX(ratio);
        }
    }
}
