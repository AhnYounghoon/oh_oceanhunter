﻿using System.Collections.Generic;
using UnityEngine;
using BattleEnum;

/// <summary>
/// 몬스터, 해적선, 배 등 모든 싸울 수 있는 오브젝트
/// <!-- 해당 클래스만 별도로 사용 불가 -->
/// </summary>
public abstract class BattleObject : STZBehaviour, IProperty, IControllHp
{
    // 체력 변경시 호출되는 Event Handler
    public delegate void ChangeHpEventHandler(float before, float after, float max);
    public event ChangeHpEventHandler changeHpEventHandler;

    public PlayData Data
    {
        set
        {
            _data = value;
            DataSetting();
        }
        get
        {
            return _data;
        }
    }

    protected GameObject model;
    protected HpBar      hpBar;
    protected Movement   movement;
    protected IProperty  activeTarget;
    protected IProperty  chaseTarget;

    ESplitProperty  _condition;
    PlayData        _data;
    CustomCollider  _detectCollider;

    protected virtual void DataSetting()
    {
        name       = StringHelper.Format("({0}) {1}", _data.id, _data.tag);
        tag        = _data.tag.ToString();

        // NOTE @minjun.ha 체력은 매판 풀피로!
        _data.currentHp = _data.hp;

        /// Create Model
        var modelTransform = this.transform.Find("Model");
        model = Instantiate(Resources.Load<GameObject>("Model/" + _data.resource_path));
        model.transform.SetParent(modelTransform);
        model.transform.localPosition = Vector3.zero;
        model.transform.localRotation = Quaternion.Euler(0,0,0);

        /// Set Hp Bar
        hpBar = this.transform.Find("Canvas").GetComponent<HpBar>();
        hpBar.SetHp(_data.currentHp, _data.hp);
        hpBar.transform.SetPositionY(_data.size < 45 ? 45 : _data.size);

        /// Set dect, active collider range
        _detectCollider = this.transform.Find("DetectCollider").GetComponent<CustomCollider>();
        _detectCollider.SetSize(_data.size, _data.GetOffset());
        _detectCollider.tag       = _data.tag.ToString();
        //_detectCollider.TargetTag = _data.tag.ToString();
        _detectCollider.enterCallback = DetectTriggerEnter;

        CustomCollider senseCollider = this.transform.Find("SenseCollider").GetComponent<CustomCollider>();
        senseCollider.SetSize(_data.attack_range);
        senseCollider.TargetTag = _data.GetBattleType() == EBattleType.Support ? _data.tag.ToString() : _data.tag.GetTargetTag().ToString() ;
        senseCollider.stayCallback = SenseTriggerStay;
        senseCollider.exitCallback = SenseTriggerExit;

        /// Attach Move System
        var pathFinder = this.transform.Find("PathFinder").GetComponent<PathFinder>();
        movement = new Movement(this.gameObject, modelTransform.gameObject, _data.speed, pathFinder);

        /// Set Transform
        transform.localPosition       = _data.startPosition;
        modelTransform.localScale     = _data.startScale;
        modelTransform.localRotation  = _data.startRotation;
    }

    protected virtual void SenseTriggerStay(Collider other)
    {
        if (chaseTarget != null || activeTarget == null || activeTarget.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) < 0)
        {
            activeTarget = other.transform.parent.GetComponent<BattleObject>() as IProperty;

            if (chaseTarget != null && chaseTarget == activeTarget)
            {
                chaseTarget = null;
                movement.StopMove();
                movement.RemovePastPath();
            }
            else if (movement.auto)
            {
                movement.StopMove();
                movement.RemovePastPath();
            }
        }
    }

    protected virtual void SenseTriggerExit(Collider other)
    {
        // 적용을 받고 있던 대상이 타깃 범위에서 사라졌을때
        if (activeTarget == (other.transform.parent.GetComponent<BattleObject>() as IProperty))
        {
            activeTarget = null;
        }
    }

    private void DetectTriggerEnter(Collider other)
    {
        BattleObject battleObject = other.transform.parent.GetComponent<BattleObject>();

        // 장애물이 있다면 이동을 멈춤
        if (battleObject != null && battleObject.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) >= 0)
        {
            movement.StopMove();
            battleObject.SetBodyLayer(0);
            SetBodyLayer(0);
            movement.RefreshPath();
            SetBodyLayer(8);
            battleObject.SetBodyLayer(8);
        }
    }

    public void SetBodyLayer(int layer)
    {
        // 오브젝트 크기 때문에 길찾기를 수행할때만 layer를 바꾸는 방법을 사용하고 있지만
        // 더 좋은 방법을 찾아야 될 것 같다 TODO @minjun.ha
        _detectCollider.gameObject.layer = layer;
    }

    protected virtual bool ChangeHp(float value)
    {
        // 체력이 0이하이면 체력 변경을 제한한다
        if (Data.currentHp <= 0)
        {
            if (changeHpEventHandler != null)
            {
                changeHpEventHandler(0, 0, _data.hp);
            }
            return false;
        }

        Data.currentHp += value;

        // 최대, 최소값 제한
        if (Data.currentHp <= 0)
        {
            Data.currentHp = 0;
            DieBattleObject();
        }
        else if (Data.currentHp > _data.hp)
        {
            Data.currentHp = _data.hp;
        }

        hpBar.SetHp(Data.currentHp, _data.hp);
        if (changeHpEventHandler != null)
        {
            changeHpEventHandler(Data.currentHp - value, Data.currentHp, _data.hp);
        }
        return true;
    }

    protected virtual void DieBattleObject()
    {
        this.gameObject.SetActive(false);
    }

    public virtual void UseSkill(System.Action inEvent, System.Action inEndCallback)
    {

    }

    public virtual void UpdateMovement()
    {
        // 스턴일때 이동 불가
        if (_condition == BattleEnum.ESplitProperty.Stun)
            return;

        if (movement.stop && (_data.tag == ETag.Enermy || (GameInfo.isAuto && _data.tag == ETag.Player)))
        {
            ETag targetTag = _data.GetBattleType() == EBattleType.Support ? _data.tag : _data.tag.GetTargetTag();
            if (_data.GetBattleType() == EBattleType.Support)
            {
                var target = BattleController.it.GetLowHpTarget(targetTag, this);
                if (activeTarget == null || activeTarget != target)
                {
                    activeTarget = null;
                    chaseTarget  = target;
                    MoveTo(target, true);
                }
            }
            else
            {
                // 활성범위안에 다른 오브젝트가 존재하지 않는다면
                // 가장 가까운 거리의 오브젝트를 찾아 이동
                if (chaseTarget != null)
                {
                    MoveTo(chaseTarget, true);
                }
                else if (activeTarget == null || activeTarget.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) <= 0)
                {
                    activeTarget = null;
                    var target = BattleController.it.GetNearestTarget(targetTag, this.transform.position, this);
                    if (target != null)
                    {
                        MoveTo(target, true);
                    }
                }
            }
        }
        movement.UpdateMove();
    }

    public void ReadyToNextWave()
    {
        activeTarget = null;
        chaseTarget  = null;
        movement.StopMove();
        movement.LookDefault();
    }

    public void SetChaseTarget(IProperty inTarget)
    {
        // 이미 타겟팅이된 목표물이라면 실행하지 않음
        if (activeTarget != inTarget && inTarget != null)
        {
            chaseTarget = inTarget;
            MoveTo(chaseTarget, false);
        }
    }

    public void MoveTo(IProperty inTarget, bool inAuto)
    {
        inTarget.SetBodyLayer(0);
        MoveTo(inTarget.GetHitAblePosition(), inAuto);
        inTarget.SetBodyLayer(8);
    }

    public virtual void MoveTo(Vector3 inPosition, bool inAuto)
    {
        SetBodyLayer(0);
        movement.Move(inPosition, inAuto);
        SetBodyLayer(8);
    }

    private bool IsAvoid()
    {
        return Random.Range(0, 100) <= _data.avoid_percent;
    }

    /// <summary>
    /// IControllHp Interface - 데미지 입음
    /// </summary>
    /// <see cref="IControllHp.GetDamage(float, Vector3)"/>
    /// <param name="inValue"></param>
    /// <param name="inPosition"></param>
    /// <returns></returns>
    public bool GetDamage(float inValue, Vector3 inPosition = default(Vector3))
    {
        if (IsAvoid())
        {
            AnimationController.it.ShowMiss(this.transform.position);
            return false;
        }

        // 방어력에 의한 실제 데미지 계산
        // NOTE @minjun.ha 1보다 데미지가 작을 때는 1로 적용
        if (inValue - _data.defense <= 1)
        {
            inValue = 1;
        }
        else
        {
            inValue = inValue - _data.defense;
        }
        if (!ChangeHp(-inValue))
        {
            return false;
        }

        if (inPosition == default(Vector3))
        {
            inPosition = this.transform.position;
        }
        AnimationController.it.ShowDamageScore(inPosition, inValue);
        return true;
    }

    /// <summary>
    /// IControllHp Interface - 힐
    /// </summary>
    /// <see cref="IControllHp.GetHeal(float, Vector3)"/>
    /// <param name="inValue"></param>
    /// <param name="inPosition"></param>
    /// <returns></returns>
    public bool GetHeal(float inValue, Vector3 inPosition = default(Vector3))
    {
        if (!ChangeHp(inValue))
        {
            return false;
        }

        AnimationController.it.ShowHealScore(transform.position, inValue);
        return true;
    }

    /// <summary>
    /// IControllHp Interface - 최대 체력
    /// </summary>
    /// <see cref="IControllHp.GetMaxHp()"/>
    /// <returns></returns>
    public float GetMaxHp()
    {
        return _data.hp;
    }

    /// <summary>
    /// IProperty Interface - 충돌 여부
    /// </summary>
    /// <see cref="IProperty.GetHitAblePosition()"/>
    /// <param name="inPosition"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    public bool ContainBoundBox(Vector3 inPosition, float size)
    {
        Vector3 currentPosition = this.transform.position;

        // 점과 점 사이의 거리를 구함
        float betweenDistance = Vector3.Distance(inPosition, currentPosition);
        if (_data.size + size >= betweenDistance)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// IProperty Interface - 타격 가능한 위치를 가져옴
    /// </summary>
    /// <see cref="IProperty.ContainBoundBox(Vector3, float)"/>
    /// <returns></returns>
    public Vector3 GetHitAblePosition()
    {
        Vector3 pos = this.transform.position;
        pos.y += _data.size  - 10;
        return pos;
    }

    /// <summary>
    /// IProperty Interface - 상태 이상 적용
    /// </summary>
    /// <see cref="IProperty.ApplyCondition(ESplitProperty)"/>
    /// <param name="inProperty"></param>
    public void ApplyCondition(BattleEnum.ESplitProperty inProperty)
    {
        _condition = inProperty;
    }

    /// <summary>
    /// IProperty Interface - 데이터 정보를 확인
    /// </summary>
    /// <see cref="IProperty.CheckCondition(EProperty, EValueType, float)"/>
    /// <param name="inProperty"></param>
    /// <param name="inValueType"></param>
    /// <param name="inValue"></param>
    /// <returns></returns>
    public int CheckCondition(EProperty inProperty, ESplitProperty inSplitProperty, EValueType inValueType, float inValue, IProperty inTarget = null)
    {
        BattleController controller = BattleController.it;
        int value = -1;
        switch (inProperty)
        {
            case EProperty.Id: value = controller.GetConditionValue(inValueType, inValue, _data.id); break;
            case EProperty.Damage: value = controller.GetConditionValue(inValueType, inValue, _data.damage); break;
            case EProperty.Hp: value = controller.GetConditionValue(inValueType, inValue, inSplitProperty == ESplitProperty.Percent ? (Data.currentHp / Data.hp) * 100 : Data.currentHp); break;
            case EProperty.Distance: value = controller.GetConditionValue(inValueType, inValue, Vector3.Distance(inTarget.GetHitAblePosition(), transform.position)); break;
            case EProperty.Arrive: value = Data.currentHp > 0 ? 1 : -1; break;
            case EProperty.Critical_Damage: value = 0; break;
            case EProperty.Hit: value = 1; break;
            case EProperty.Penetrate: value = 0; break;
        }
        return value;
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (_data != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(this.transform.position + _data.GetOffset(), _data.size);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(this.transform.position, _data.attack_range);
        }
    }
#endif
}
