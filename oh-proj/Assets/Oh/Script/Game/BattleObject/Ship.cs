﻿using BattleEnum;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class Ship : BattleObject
{
    GameObject  _skillNotice;
    Image       _skillIcon;
    float       _attackDelay;

    protected override void DataSetting()
    {
        base.DataSetting();

        // Skill Noti Mapping
        _skillNotice = this.transform.Find("Canvas/SkillNoti").gameObject;
        _skillNotice.gameObject.SetActive(false);

        // Set Name
        Text txtName = this.transform.Find("Canvas/txtName").GetComponent<Text>();
        txtName.text = Data.heroData.name;

        // Skill Icon
        _skillIcon = _skillNotice.transform.Find("Mask/imgCharacter").GetComponent<Image>();
        _skillIcon.sprite = Resources.Load<Sprite>(Data.heroData.icon_path);
        
        // Set Character Profile
        var profile = this.transform.Find("Canvas/imgType/imgCharacter").GetComponent<Image>();
        profile.sprite = Resources.Load<Sprite>(Data.heroData.icon_path);
    }

    protected override void DieBattleObject()
    {
        this.transform.DOMoveY(-100, 1.0f).OnComplete(() => { base.DieBattleObject(); });
    }

    public override void UpdateMovement()
    {
        base.UpdateMovement();
        if (_attackDelay > 0)
        {
            _attackDelay -= Time.fixedDeltaTime;
        }
    }

    public override void UseSkill(System.Action inEvent, System.Action inEndCallback)
    {
        base.UseSkill(inEvent, inEndCallback);

        Observable.Timer(System.TimeSpan.FromSeconds(2.0f))
             .Subscribe(_ =>
             {
                 if (inEndCallback != null)
                 {
                     inEndCallback();
                 }

                 BattleController.it.ActiveSkill(Data.skill_id.IntValue(), this.gameObject, this, Data.tag, () =>
                 {

                 });
             }).AddTo(this.gameObject);
    }

    /// <summary>
    /// 액티브 범위 안에 들었을때
    /// </summary>
    /// <param name="other"></param>
    protected override void SenseTriggerStay(Collider other)
    {
        base.SenseTriggerStay(other);
        if (chaseTarget != null || activeTarget == null || activeTarget.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) < 0) { }
        else if (_attackDelay <= 0)
        {
            if (BattleController.it.ActiveSkill(Data.attack_id.IntValue(), this.gameObject, this, Data.tag, null))
            {
                _attackDelay = Statics.Instance.skill[Data.attack_id.IntValue()].cool_time;
            }
        }
    }

#if UNITY_EDITOR
    /// <summary>
    /// 선박 내부 데이터를 json 형태로 저장
    /// 
    /// <data>
    /// </data>
    /// </summary>
    [ContextMenu("Save Data")]
    public void SaveData()
    {
        SimpleJSON.JSONClass json = new SimpleJSON.JSONClass();

        /// JSON ADD

        string output = json.SaveToCompressedBase64();
        Debug.Log(json.ToString());
        Debug.Log(output);
        System.Windows.Forms.Clipboard.SetText(output);
    }
#endif
}
