﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterAnimation : MonoBehaviour
{
    public static readonly string IDLE   = "Idle";
    public static readonly string SUMMON = "Summon";
    public static readonly string DAMAGE = "Damage";
    public static readonly string ATTACK = "Attack_{0}";
    public static readonly string SKILL  = "Skill_{0}";
    public static readonly string DIE    = "Die";
    public static readonly string MOVE   = "Move";

    Dictionary<string, Action> _triggerEndCallback;
    System.IDisposable _onStateExit;

    public class ReserveAnimation
    {
        public string animationName;
        public bool   loop;
        public Action callback;
    }

    private ReserveAnimation _reserveAnimation;
    private string _prevAnimation;
    private Animator _animator;
    private bool _prevStart;

    private void Awake()
    {
        _prevStart = true;
        _animator  = this.GetComponent<Animator>();
        _triggerEndCallback = new Dictionary<string, Action>();
    }

    public void EndCallback(AnimatorStateInfo info)
    {
        foreach (var callback in _triggerEndCallback)
        {
            if (info.IsName(callback.Key))
            {
                //if (info.IsName(_prevAnimation))
                //{
                //    _prevAnimation = string.Empty;
                //}
                if (callback.Value != null)
                {
                    callback.Value();
                }
                break;
            }
        }
    }

    private void Start()
    {
        _prevStart = false;
        if (_reserveAnimation != null)
        {
            PlayAnimation(_reserveAnimation.animationName, _reserveAnimation.loop , _reserveAnimation.callback);
            _reserveAnimation = null;
        }
    }

    public void PlayAnimation(string inAnimation, bool isLoop = false, System.Action inEndCallback = null)
    {
        string name = inAnimation;

        if (_prevStart)
        {
            _reserveAnimation = new ReserveAnimation()
            {
                animationName = inAnimation,
                callback      = inEndCallback,
                loop          = isLoop
            };
            return;
        }
        if (name == _prevAnimation)
        {
            return;
        }

        _animator.Rebind();
        _animator.Play(name);
        _prevAnimation = name;

        if (isLoop)
        {
            inEndCallback = () => 
            {
                _prevAnimation = string.Empty;
                PlayAnimation(inAnimation, isLoop);
            };
        }

        if (_triggerEndCallback.ContainsKey(inAnimation))
        {
            _triggerEndCallback[inAnimation] = inEndCallback;
        }
        else
        {
            _triggerEndCallback.Add(inAnimation, inEndCallback);
        }
    }
}
