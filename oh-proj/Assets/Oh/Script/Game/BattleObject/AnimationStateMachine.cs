﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateMachine : StateMachineBehaviour
{
    private MonsterAnimation _animation;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
   
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_animation == null)
        {
            _animation = animator.GetComponent<MonsterAnimation>();
        }
        _animation.EndCallback(stateInfo);
    }
}
