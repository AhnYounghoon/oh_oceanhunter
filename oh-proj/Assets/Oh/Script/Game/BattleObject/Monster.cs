﻿using BattleEnum;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using ReserveSkill = System.Collections.Generic.KeyValuePair<int, int>;

public class Monster : BattleObject
{
    Queue<ReserveSkill> _reserveSkillQueue = new Queue<ReserveSkill>(); 
    List<IDisposable>   _skillTimerList    = new List<IDisposable>();

    MonsterAnimation _animation;
    State            _state;
    float            _attackDelayTime;

    protected override void DataSetting()
    {
        base.DataSetting();

        /// Set Animation
        _animation = model.transform.Find("Model").GetComponent<MonsterAnimation>();

        /// Set State
        _state = new State();
        _state.changeStateEventHandler += ChangeStateEvent;
        _state.ChangeState(State.EState.Appear);

        changeHpEventHandler += ChangeHpEvent;
        RegistSkillTimer();
    }

    private void OnDestroy()
    {
        for (int i = 0; i < _skillTimerList.Count; i++)
        {
            if (_skillTimerList[i] != null)
            {
                _skillTimerList[i].Dispose();
                _skillTimerList[i] = null;
            }
        }
        _skillTimerList.Clear();
    }

    public override void UpdateMovement()
    {
        base.UpdateMovement();

        if (_attackDelayTime > 0)
        {
            _attackDelayTime -= Time.deltaTime;
        }

        // 몬스터는 항상 활성화 캐릭터를 바라봄
        if (activeTarget != null)
        {
            movement.Look(activeTarget.GetHitAblePosition());
        }
    }

    public override void MoveTo(Vector3 inPosition, bool inAuto)
    {
        base.MoveTo(inPosition, inAuto);

        // TODO @minjun.ha 몬스터 걷기는 일단 보류 ㅠㅠ
        //_animation.PlayAnimation(MonsterAnimation.MOVE, true);
    }

    /// <summary>
    /// State 변경에 따른 이벤트
    /// 같은 State로 변경에는 호출 되지 않는다
    /// </summary>
    /// <param name="state"></param>
    private void ChangeStateEvent(State.EState state, int idx)
    {
        if (_animation == null)
            return;

        ServerStatic.BattleObject.ParseData data = Data.GetParseData();
        switch (state)
        {
            case State.EState.None:
                _animation.PlayAnimation(movement.stop ? MonsterAnimation.IDLE : MonsterAnimation.MOVE, false);
                break;
            case State.EState.Appear:
                _animation.PlayAnimation(MonsterAnimation.SUMMON, false, () => { _state.ChangeState(State.EState.None); });
                break;
            case State.EState.Damage:
                _animation.PlayAnimation(MonsterAnimation.DAMAGE, false, () => { _state.ChangeState(State.EState.None); });
                break;
            case State.EState.Attack:
                _animation.PlayAnimation(StringHelper.Format(MonsterAnimation.ATTACK, 1), false, () => { _animation.PlayAnimation(MonsterAnimation.IDLE); });
                break;
            case State.EState.Skill:
                _animation.PlayAnimation(StringHelper.Format(MonsterAnimation.SKILL, idx + 1), false, () => { _animation.PlayAnimation(MonsterAnimation.IDLE); });
                break;
            case State.EState.Die:
                _animation.PlayAnimation(MonsterAnimation.DIE, false, base.DieBattleObject);
                if (Data.tag == ETag.Enermy)
                {
                    var quests = ServerStatic.Quest.GetListByBattleObjectId(Data.id);
                    foreach (ServerStatic.Quest quest in quests)
                    {
                        if (GeneralDataManager.it.IsCompleteQuest(quest.id.ToString()))
                            continue;

                        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.QUEST, quest.id.ToString());
                    }
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// 스킬 타이머를 등록
    /// </summary>
    private void RegistSkillTimer()
    {
        string[] skillArray = Data.skill_id.Split(',');
        for (int i = 0; i < skillArray.Length; i++)
        {
            int idx     = i;
            int skillId = skillArray[idx].IntValue();
            float coolTime = BattleController.it.GetCoolTime(skillId);
            var timer = Observable.Interval(TimeSpan.FromSeconds(coolTime)).Subscribe(TabBlock =>
            {
                ReserveSkill skillData = new ReserveSkill();
                if (!_reserveSkillQueue.Contains(skillData))
                {
                    _reserveSkillQueue.Enqueue(new ReserveSkill(skillId, idx));
                }
            });
            _skillTimerList.Add(timer);
        }
    }

    /// <summary>
    /// HP 변경 사항이 있을 때 호출됨
    /// 받은 데미지가 일정 비율 보다 높다면 데미지 애니메이션을 실행 
    /// </summary>
    /// <param name="before"></param>
    /// <param name="after"></param>
    /// <param name="max"></param>
    private void ChangeHpEvent(float before, float after, float max)
    {
        //if (before - after >= (max / 20))
        //{
        //    _state.ChangeState(State.EState.Damage);
        //}
    }

    /// <summary>
    /// 몬스터는 죽는 애니메이션 뒤 Disable 되야함
    /// </summary>
    protected override void DieBattleObject()
    {
        hpBar.gameObject.SetActive(false);
        _state.ChangeState(State.EState.Die);
    }

    /// <summary>
    /// 스킬 사용
    /// 타이머에 의해 등록된 스킬이 있다면 검증 후 사용
    /// </summary>
    /// <returns></returns>
    private bool Skill()
    {
        if (_reserveSkillQueue.Count == 0 || !_state.AbleChangeState(State.EState.Skill))
        {
            return false;
        }

        ReserveSkill reserve = _reserveSkillQueue.Dequeue();
        bool isRun = false;

        if (BattleController.it.ActiveSkill(reserve.Key, this.gameObject,this, Data.tag, () => 
        {
            _state.ChangeState(State.EState.None);
        }))
        {
            _state.ChangeState(State.EState.Skill, reserve.Value);
            isRun = true;
        }
        return isRun;
    }

    /// <summary>
    /// 공격
    /// </summary>
    /// <returns></returns>
    private bool Attack()
    {
        if (!_state.AbleChangeState(State.EState.Attack) || _attackDelayTime > 0)
        {
            return false;
        }

        bool isRun = false;
        if (BattleController.it.ActiveSkill(Data.attack_id.IntValue(), this.gameObject, this, Data.tag, () => 
        {
            _state.ChangeState(State.EState.None);
        }))
        {
            _attackDelayTime = Statics.Instance.skill[Data.attack_id.IntValue()].cool_time;
            _state.ChangeState(State.EState.Attack);
            isRun = true;
        }
        return isRun;
    }

    /// <summary>
    /// 공격 범위에 포함 (Attack, Skill)
    /// </summary>
    /// <param name="coll"></param>
    protected override void SenseTriggerStay(Collider other)
    {
        base.SenseTriggerStay(other);
        if (chaseTarget != null || activeTarget == null || activeTarget.CheckCondition(EProperty.Hp, ESplitProperty.Value, EValueType.None, 0) < 0) { }
        else
        {
            Skill();
            Attack();
        }
    }

    /// <summary>
    /// 공격 범위에서 벗어남
    /// </summary>
    /// <param name="coll"></param>
    protected override void SenseTriggerExit(Collider other)
    {
        base.SenseTriggerExit(other);
        if (_state.IsState( State.EState.Skill))
        {
            return;
        }
        _state.ChangeState(State.EState.None);
    }

    /// <summary>
    /// 상태 변화 및 제어 클래스
    /// </summary>
    private class State
    {
        public enum EState
        {
            None,
            Appear,
            Damage,
            Attack,
            Skill,
            Die,
        }

        public delegate void ChangeStateEvent(EState state, int idx);
        public event ChangeStateEvent changeStateEventHandler;

        private EState _state;

        public void ChangeState(EState inState, int idx = 0)
        {
            if (!AbleChangeState(_state, inState))
            {
                return;
            }

            _state = inState;
            changeStateEventHandler(_state, idx);
        }

        public bool IsState(EState inState)
        {
            return _state == inState;
        }

        public bool AbleChangeState(EState inState)
        {
            return AbleChangeState(_state, inState);
        }

        private bool AbleChangeState(EState inBefore, EState inAfter)
        {
            // 전투 불능 상태는 예외로 항상 허용
            if (inAfter == EState.Die)
            {
                return true;
            }

            bool able = true;

            // 같은 상태로의 변경은 기본적으로 제한
            if (inBefore == inAfter)
            {
                able = false;
            }

            // 몬스터가 전투 불능일때 더이상 State의 변화를 주면 안된다
            if (inBefore == EState.Die)
            {
                able = false;
            }

            // 스킬은 None으로 변경만 허용한다
            if (inBefore == EState.Skill && inAfter != EState.None)
            {
                able = false;
            }

            // Appear은 None으로 변경만 허용한다
            if (inBefore == EState.Appear && inAfter != EState.None)
            {
                able = false;
            }

            // Attack에서 Skill로 갈 수는 없다
            if (inBefore == EState.Attack && inAfter == EState.Skill)
            {
                able = false;
            }

            // Damage에서 Attack이나 Damage로 갈 수 없다
            if (inBefore == EState.Damage && (inAfter == EState.Attack || inAfter == EState.Damage))
            {
                able = false;
            }

            // Attack이나 Skill 상태에서 데미지 상태로 변경될 수는 없다
            if ((inBefore == EState.Attack || inBefore == EState.Skill) && inAfter == EState.Damage)
            {
                able = false;
            }
            return able;
        }
    }

#if UNITY_EDITOR
    [Header("------ Editor Only ------")]
    [Tooltip("idle 애니메이션 키")]
    [SerializeField]
    string idleAnimation;

    [Tooltip("summon 애니메이션 키")]
    [SerializeField]
    string summonAnimation;

    [Tooltip("damage 애니메이션 키")]
    [SerializeField]
    string damageAnimation;

    [Tooltip("die 애니메이션 키")]
    [SerializeField]
    string dieAnimation;

    [Tooltip("attack 애니메이션 키")]
    [SerializeField]
    List<string> _attackAnimation;

    [Tooltip("skill 애니메이션 키")]
    [SerializeField]
    List<string> _skillAnimation;

    /// <summary>
    /// 몬스터 내부 데이터를 json 형태로 저장
    /// 
    /// <data>
    ///  - 공격 애니메이션 키
    ///  - 스킬 애니메이션 키
    /// </data>
    /// </summary>
    [ContextMenu("Save Data")]
    public void SaveData()
    {
        SimpleJSON.JSONClass json = new SimpleJSON.JSONClass();
        json.Add("idle_animation", idleAnimation);
        json.Add("summon_animation", summonAnimation);
        json.Add("damage_animation", damageAnimation);
        json.Add("die_animation", dieAnimation);

        if (_attackAnimation != null)
        {
            SimpleJSON.JSONArray attack = new SimpleJSON.JSONArray();
            foreach (string name in _attackAnimation)
            {
                attack.Add(name);
            }
            json.Add("attack_animation", attack);
        }

        if (_skillAnimation != null)
        {
            SimpleJSON.JSONArray skill = new SimpleJSON.JSONArray();
            foreach (string name in _skillAnimation)
            {
                skill.Add(name);
            }
            json.Add("skill_animation", skill);
        }

        string output = json.SaveToCompressedBase64();
        Debug.Log(json.ToString());
        Debug.Log(output);
        System.Windows.Forms.Clipboard.SetText(output);
    }
#endif
}
