﻿using UnityEngine;
using System.Collections;

public static class STZExtension
{
    public static T Get<T>(this MonoBehaviour inMonoBehavior, string inChildPath = "")
    {
        return Get<T>(inMonoBehavior.transform, inChildPath);
    }

    public static T Get<T>(this GameObject inGameObject, string inChildPath = "")
    {
        return Get<T>(inGameObject.transform, inChildPath);
    }

    public static T Get<T>(this Transform inTransform, string inChildPath = "")
    {
        System.Type type = typeof(T);
        Transform tf = inTransform.Find(inChildPath);
        if (tf == null)
            return default(T);

        if (type == typeof(Transform))
        {
            object value = tf;
            if (value == null)
                Debug.LogWarning("Transform 할당 실패");
            return (T)value;
        }
        else if (type == typeof(GameObject))
        {
            object value = tf.gameObject;
            if (value == null)
                Debug.LogWarning("GameObject 할당 실패");
            return (T)value;
        }
        else
        {
            T value = tf.GetComponent<T>();
            if (value == null)
                Debug.LogWarning("Component 할당 실패");
            return value;
        }
    }

    public static string Locale(this string o)
    {
        string result = o;
        if (Statics.Instance != null
           && Statics.Instance.locale != null)
        {
            result = Statics.Instance.locale.GetText(o);
        }
        return result;
    }

    const string postposition = "2459가갸거겨고교구규그기개걔게계과괘궈궤괴귀긔까꺄꺼껴꼬꾜꾸뀨끄끼깨꺠께꼐꽈꽤꿔꿰꾀뀌끠나냐너녀노뇨누뉴느니내냬네녜놔놰눠눼뇌뉘늬다댜더뎌도됴두듀드디대댸데뎨돠돼둬뒈되뒤듸따땨떠>뗘또뚀뚜뜌뜨띠때떄떼뗴똬뙈뚸뛔뙤뛰띄라랴러려로료루류르리래럐레례롸뢔뤄뤠뢰뤼릐마먀머며모묘무뮤므미매먜메몌뫄뫠뭐뭬뫼뮈믜바뱌버벼보뵤부뷰브비배뱨베볘봐봬붜붸뵈뷔븨빠뺘뻐뼈뽀뾰뿌쀼쁘삐빼뺴뻬뼤뽜뽸뿨쀄뾔쀠쁴사샤서셔소쇼수슈스시새섀세셰솨쇄숴쉐쇠쉬싀싸쌰써쎠쏘쑈쑤쓔쓰씨쌔썌쎄쎼쏴쐐쒀쒜쐬쒸씌아야어여오요우유으이애얘에예와왜워웨외위의자쟈저져조죠주쥬즈지재쟤제졔좌좨줘줴죄쥐즤짜쨔쩌쪄쪼쬬쭈쮸쯔찌째쨰쩨쪠쫘쫴쭤쮀쬐쮜쯰차챠처쳐초쵸추츄츠치채챼체쳬촤쵀춰췌최취츼카캬커켜코쿄쿠큐크키캐컈케켸콰쾌쿼퀘쾨퀴킈타탸터텨토툐투튜트티태턔테톄톼퇘퉈퉤퇴튀틔파퍄퍼펴포표푸퓨프피패퍠페폐퐈퐤풔풰푀퓌픠하햐허혀호효후휴흐히해햬헤혜화홰훠훼회휘희";
    public static string ConvertPostPosition(this string str)
    {
        int lastIndex = 0;
        lastIndex = str.LastIndexOf("[ro_euro]");
        if (lastIndex != -1)
        {
            if (postposition.LastIndexOf(str[lastIndex - 1]) != -1)
            {
                return str.Replace("[ro_euro]", ("KR_RO").Locale());
            }
            else
            {
                return str.Replace("[ro_euro]", ("KR_EURO").Locale());
            }
        }
        lastIndex = str.LastIndexOf("[i_ga]");
        if (lastIndex != -1)
        {
            if (postposition.LastIndexOf(str[lastIndex - 1]) != -1)
            { // 받침 없음
                return str.Replace("[i_ga]", ("KR_GA").Locale());
            }
            else
            {
                return str.Replace("[i_ga]", ("KR_I").Locale());
            }
        }
        lastIndex = str.LastIndexOf("[eun_neun]");
        if (lastIndex != -1)
        {
            if (postposition.LastIndexOf(str[lastIndex - 1]) != -1)
            {
                return str.Replace("[eun_neun]", ("KR_NEUN").Locale());
            }
            else
            {
                return str.Replace("[eun_neun]", ("KR_EUN").Locale());
            }
        }
        lastIndex = str.LastIndexOf("[eul_reul]");
        if (lastIndex != -1)
        {
            if (postposition.LastIndexOf(str[str.Length - 1]) != -1)
            {
                return str.Replace("[eul_reul]", ("KR_REUL").Locale());
            }
            else
            {
                return str.Replace("[eul_reul]", ("KR_EUL").Locale());
            }
        }
        lastIndex = str.LastIndexOf("[wa_gwa]");
        if (lastIndex != -1)
        {
            if (postposition.LastIndexOf(str[lastIndex - 1]) != -1)
            {
                return str.Replace("[wa_gwa]", ("KR_WA").Locale());
            }
            else
            {
                return str.Replace("[wa_gwa]", ("KR_GWA").Locale());
            }
        }

        return str;
    }

    public static string StringValue(this Hashtable inHashTable, string inKey)
    {
        if (!inHashTable.ContainsKey(inKey))
        {
            return string.Empty;
        }
        return inHashTable[inKey].ToString();
    }

    public static int IntValue(this Hashtable inHashTable, string inKey)
    {
        if (!inHashTable.ContainsKey(inKey))
        {
            return 0;
        }

        int result = 0;
        int.TryParse(inHashTable[inKey].ToString(), out result);
        return result;
    }

    public static float FloatValue(this Hashtable inHashTable, string inKey)
    {
        if (!inHashTable.ContainsKey(inKey))
        {
            return 0;
        }

        float result = 0;
        float.TryParse(inHashTable[inKey].ToString(), out result);
        return result;
    }

    /// <summary>
    /// 카메라 해상도 맞추기
    /// </summary>
    /// <param name="inCamera">조정할 카메라</param>
    /// <param name="inStandardWidth">가로 기준 해상도</param>
    /// <param name="inStandardHeight">세로 기준 해상도</param>
    public static void SetResolution(this Camera inCamera, float inStandardWidth = 640f, float inStandardHeight = 1136f)
    {
        if (Screen.width / Screen.height < inStandardWidth / inStandardHeight)
        {
            float width = (inStandardHeight * 0.5f) / inStandardHeight * inStandardWidth;
            inCamera.orthographicSize = width / Screen.width * Screen.height;
        }
    }

    /// <summary>
    /// RectTRansform 해상도 맞추기
    /// </summary>
    /// <param name="inTransform">조정할 RectTransform</param>
    /// <param name="inStandardWidth">가로 기준 해상도</param>
    /// <param name="inStandardHeight">세로 기준 해상도</param>
    public static void SetResolution(this RectTransform inTransform, float inStandardWidth = 640f, float inStandardHeight = 1136f)
    {
        if (Screen.width / Screen.height < inStandardWidth / inStandardHeight)
        {
            float width = (inStandardHeight * 0.5f) / inStandardHeight * inStandardWidth;
            float height = width / Screen.width * Screen.height * 2;

            inTransform.sizeDelta = new Vector2(width, height);   
        }
    }
}
