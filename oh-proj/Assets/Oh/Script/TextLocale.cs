﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextLocale : MonoBehaviour
{
	void Start()
    {
        Text txt = this.Get<Text>();
        txt.text = txt.text.Locale();
	}
}
