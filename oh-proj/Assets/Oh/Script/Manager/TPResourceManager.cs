﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using STZFramework;

public class TPResourceManager : MonoBehaviour
{
    static GameObject _container;
    static TPResourceManager _instance;

    static private bool _ready = false;

    Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();

    public static TPResourceManager it
    {
        get
        {
            if (_instance == null)
            {
                _container = new GameObject();
                _container.name = "ThemeResourceManager";
                _instance = _container.AddComponent(typeof(TPResourceManager)) as TPResourceManager;
                _instance.LoadResources();
                DontDestroyOnLoad(_container);
                _ready = true;
            }

            return _instance;
        }
    }

    public Sprite GetSprite(string name)
    {
        if (_sprites.ContainsKey(name) == false)
        {
            Debug.LogError(string.Format("({0}) sprite name not exist", name));
            return null;
        }

        return _sprites[name];
    }

    public void LoadResources()
    {
        if (_ready)
            return;

        LoadImages("Texture/OceanObject");

        if (SceneManager.GetActiveScene().name == GeneralDataManager.ESceneName.MapTool.ToString())
        {
            LoadImages("Texture/MapTool");
        }
    }

//    public void LoadEpisodeThemeResource()
//    {
//#if !UNITY_STANDALONE
//        int currStage = StageDataManager.it.GetStageIndex();
//        string themeKey = string.Empty;
//        int currEpId = Statics.Instance.GetCurrentEpisodeId(currStage);
//        if (GeneralDataManager.IsSeason1())
//        {
//            themeKey = StringHelper.Format("theme_{0}", Statics.Instance.episode[currEpId].theme_id);

//            if (Statics.Instance.episode[currEpId].theme_id >= 7)
//                return;
//        }
//        else
//        {
//            themeKey = StringHelper.Format("theme_{0}", Statics.Instance.episode_s2[currEpId].theme_id);

//            if (Statics.Instance.episode_s2[currEpId].theme_id >= 7)
//                return;
//        }
//#else
//        string themeKey = "theme_1";
//#endif

//        if (!_sprites.ContainsKey(StringHelper.Format("{0}_tileBg_01", themeKey)))
//            LoadImages(StringHelper.Format("TexturePack/Tile/{0}", themeKey), themeKey);
//    }

    void LoadImage(string imageName)
    {
        Sprite texture = Resources.Load<Sprite>(imageName);
        if (texture == null)
            return;

        string key = texture.name;
        _sprites[key] = texture;
    }

    void LoadImages(string imagesName, string prefix = null)
    {
        Sprite[] textures = Resources.LoadAll<Sprite>(imagesName);
        if (textures == null || textures.Length <= 0)
            return;

        string key = "";
        for (int i = 0; i < textures.Length; i++)
        {
            key = textures[i].name;
            if (!prefix.IsNullOrEmpty())
                key = prefix + "_" + key;

            _sprites[key] = textures[i];
        }
    }
}
