using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using SimpleJSON;
using STZFramework;

public class StageDataManager
{
    private static StageDataManager _instance = null;
    public static StageDataManager it { get { if (_instance == null) { _instance = new StageDataManager(); } return _instance; } }

    // 기본 월드맵 스테이지 파일 [시즌 1]
    private Dictionary<int, LocalStageData> _stageDic = new Dictionary<int, LocalStageData>();

    private Dictionary<int, LocalStageData> currentStageDic
    {
        get
        {
            return _stageDic;
        }
        set
        {
            _stageDic = value;
        }
    }

    private LocalStageData _tempStageData;
    private LocalStageData _targetStageData;
    private bool _ready;
    private string _filePath;
    bool _isUseStatic = true;
    public bool useStatic { get { return _isUseStatic; } }

    private Dictionary<int, List<TileData>> _tile_dic;
    private Dictionary<int, List<TokenData>> _token_dic;
    //private Dictionary<int, List<ObstacleData>> _obstacle_multiInfo;

    private List<int> _end_point;
    private int[] _hidden_index;
    //private Dictionary<int, List<BlockCreatorData>> _blockCreatorTrigger;
    //private List<MissionData> sub_mission;
    //private List<BoardData> _waterFragments;

    public enum EStageDataType
    {
        Season_1,
        Season_2
    }

    /// <summary>
    /// 패키징된 스테이지 파일 경로를 가져온다
    /// </summary>
    /// <param name="inStageDataType"></param>
    /// <returns></returns>
    public string GetPackagedStageFilePath(EStageDataType inStageDataType)
    {
        string path = string.Empty;
        switch (inStageDataType)
        {
            case EStageDataType.Season_1:
                path = "Json/StageDataCompressed_v0097";
                break;
            case EStageDataType.Season_2:
                path = "Json/StageDataCompressed_v1002";
                break;
            default:
                break;
        }
        return path;
    }

    /// <summary>
    /// 레벨 디자인 테스트를 위한 파일 경로 이름
    /// </summary>
    /// <param name="inStageDataType"></param>
    /// <returns></returns>
    public string GetPlayTestPath(EStageDataType inStageDataType)
    {
        string path = string.Empty;
        switch (inStageDataType)
        {
            case EStageDataType.Season_1:
                path = "/Play_StageData.txt";
                break;
            case EStageDataType.Season_2:
                path = "/Play_StageData_s2.txt";
                break;
            default:
                break;
        }
        return path;
    }

    #region stage file merge
    public void FileMerge(string file1Path, string file2Path, string outputFilePath)
    {
        JSONNode jsonNode;
        Dictionary<int, LocalStageData> mergeStageDic = new Dictionary<int, LocalStageData>();

        // file1 load
        string data1 = File.ReadAllText(file1Path);
        if (file1Path.Contains("Compressed"))
            jsonNode = JSONNode.LoadFromCompressedBase64(data1);
        else
            jsonNode = JSONNode.Parse(data1);

        jsonNode = jsonNode["stage"];
        JSONArray jsonArray = jsonNode.AsArray;

        for (int i = 0; i < jsonArray.Count; i++)
        {
            LocalStageData sd = new LocalStageData();

            if (sd.Bind(jsonArray[i], false))
            {
                if (mergeStageDic.ContainsKey(sd.index))
                {
                    //Debug.Log(string.Format("({0}) stage already in _stageDic", sd.index));
                }
                else
                    mergeStageDic.Add(sd.index, sd);
            }
        }

        // file2 load
        string data2 = File.ReadAllText(file2Path);
        if (file2Path.Contains("Compressed"))
            jsonNode = JSONNode.LoadFromCompressedBase64(data2);
        else
            jsonNode = JSONNode.Parse(data2);

        jsonNode = jsonNode["stage"];

        for (int i = 0; i < jsonArray.Count; i++)
        {
            LocalStageData sd = new LocalStageData();

            if (sd.Bind(jsonArray[i], false))
            {
                if (mergeStageDic.ContainsKey(sd.index))
                {
                    Debug.Log(StringHelper.Format("file2 ({0}) stage already in mergeStageDic [index changed ({0}) => ({1})]", sd.index, mergeStageDic.Count + 1));
                    sd.index = mergeStageDic.Count + 1;
                    mergeStageDic.Add(sd.index, sd);
                }
                else
                    mergeStageDic.Add(sd.index, sd);
            }
        }

        // write file
#if UNITY_EDITOR
        string fileName = "MergeData.txt";
        //filePath = Application.dataPath + "/Resources/Json/" + fileName;
#else
		System.DateTime dt = System.DateTime.Now;
		string fileName = string.Format ("StageData_{0}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}.txt", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
		//filePath = Application.dataPath + "/" + fileName;
#endif

        outputFilePath += "/" + fileName;
        LocalStageData mergeData;

        JSONArray root_array = new JSONArray();
        JSONClass root = new JSONClass();

        for (int i = 1; i <= mergeStageDic.Count; i++)
        {
            mergeData = mergeStageDic[i];
            root_array.Add(ExportStageToJson(mergeData));
        }

        root.Add("stage", root_array);

        //if (inIsCompress)
        //{
        //    filePath = filePath.Replace("MergeData", "MergeDataDataCompressed");
        //    File.WriteAllText(filePath, root.SaveToCompressedBase64());
        //}
        //else
        {
            JsonFormatter format = new JsonFormatter(root.ToString());
            File.WriteAllText(outputFilePath, format.Format());
        }

        Debug.Log("MergeDataFile : " + outputFilePath);
    }
    #endregion

    #region Stage File I/O
    public bool LoadFilePathData(string filePath, bool inIsCompress)
    {
        if (File.Exists(filePath) == false)
            return false;

        if (_filePath == filePath)
            return false;
        else
            _filePath = filePath;

        if (filePath.Contains("Compressed"))
            inIsCompress = true;

        Parse(File.ReadAllText(filePath), inIsCompress);
        return true;
    }

    /// <summary>
    /// 패키지내의 포함된 파일 로딩
    /// </summary>
    public void LoadPackagedStageFile(string inFilePath)
    {
        string filePath = "Json/StageData";

        if (!inFilePath.IsNullOrEmpty())
            filePath = inFilePath;

        TextAsset ta = Resources.Load<TextAsset>(filePath);

        if (ta == null)
        {
            Debug.LogWarning(inFilePath + " file dont exist!!");
            return;
        }

        Parse(ta.text, filePath.Contains("Compressed"));

        Resources.UnloadAsset(ta);
    }

    public bool LoadCachedStageFile(int inSlotIndex)
    {
        string filePath = Application.temporaryCachePath + StringHelper.Format("/OceanData_{0}.txt", inSlotIndex);
        if (!File.Exists(filePath))
        {
            Debug.LogWarning(filePath + " file dont exist!!");
            return false;
        }

        Parse(File.ReadAllText(filePath), filePath.Contains("Compressed"), true);

        return true;
    }

    public LocalBattleOceanData localData { get; private set; }
    public int slotIndex { get; set; }
    /// <summary>
    /// 임시 스테이지와 배 정보 설정
    /// </summary>
    public void SetLocalOceanData(int posIndex, int fuelAmount)
    {
        localData = new LocalBattleOceanData();

        // 선박 정보
        {
            localData.shipIndex = posIndex;
            localData.shipFuel = fuelAmount;
        }

        // 타일 & 토큰 정보
        {
            localData.stageInfo = GetTempStage();
        }
    }

    public void SaveOceanData()
    {
        if (localData == null)
            return;

        string filePath;
#if UNITY_EDITOR || !UNITY_STANDALONE
        //filePath = Application.dataPath + "/Resources/Json/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
        filePath = Application.temporaryCachePath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#elif UNITY_STANDALONE
        filePath = Application.dataPath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#else
        filePath = Application.temporaryCachePath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#endif        

        JSONClass root = localData.Export();
        JsonFormatter format = new JsonFormatter(root.ToString());

        File.WriteAllText(filePath, format.Format());
    }

    public void LoadOceanData()
    {
        // 이미 깬 스테이지 다시 하면 중간 데이터 로딩 안함
        if (GeneralDataManager.it.GetStageClearData() >= slotIndex)
        {
            if (localData != null) localData = null;
            return;
        }

        string filePath;
#if UNITY_EDITOR || !UNITY_STANDALONE
        //filePath = Application.dataPath + "/Resources/Json/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
        filePath = Application.temporaryCachePath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#elif UNITY_STANDALONE
        filePath = Application.dataPath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#else
        filePath = Application.temporaryCachePath + "/" + StringHelper.Format("OceanData_{0}.txt", slotIndex);
#endif

        if (!System.IO.File.Exists(filePath))
        {
            // 안한 스테이지면 이전 로컬 데이터 초기화(1스테이지 클리어후 2스테이지 할때)
            if (localData != null) localData = null;
            return;
        }

        JSONNode jsonNode = JSONNode.Parse(File.ReadAllText(filePath));

        localData = new LocalBattleOceanData();
        localData.Bind(jsonNode);

        SaveTempStageData(localData.stageInfo);
    }

    public LocalBattleOceanData GetLocalBattleOceanData()
    {
        return localData;
    }

    public void RemoveTokenAtOceanData(int inIndex)
    {
        List<TokenData> tokenDatas = localData.stageInfo.tokenList;
        for (int i = 0; i < tokenDatas.Count; i++)
        {
            if (tokenDatas[i].index == inIndex)
            {
                tokenDatas.Remove(tokenDatas[i]);
                break;
            }
        }
    }

    /// <summary>
    /// 맵데이터 파싱해서 로딩
    /// </summary>
    /// <param name="inData"></param>
    /// <param name="inIsCompress">압축 여부</param>
    /// <param name="inIsInfinityTower">무한의 탑 데이터인지 여부</param>
    public void Parse(string inData, bool inIsCompress, bool inTempStage = false)
    {
#if UNITY_STANDALONE
        if (_stageDic != null)
            _stageDic.Clear();
#else
        if (_ready)
            return;
#endif

        JSONNode jsonNode;

        if (inIsCompress)
            jsonNode = JSONNode.LoadFromCompressedBase64(inData);
        else
            jsonNode = JSONNode.Parse(inData);

        jsonNode = jsonNode["stage"];
        JSONArray jsonArray = jsonNode.AsArray;

        // 밸런싱플레이를 위한 맵데이터를 로딩했다면 static 값은 참조하지 않음
        if (_filePath != null && _filePath.Contains("Play_"))
            _isUseStatic = false;

        // 개별 맵에 저장한다
        Dictionary<int, LocalStageData> targetStageDic = null;
        targetStageDic = _stageDic;

        for (int i = 0; i < jsonArray.Count; i++)
        {
            LocalStageData sd = new LocalStageData();
            
            // 밸런싱플레이를 위한 맵데이터를 로딩했다면 static 값은 참조하지 않음
            if (_filePath != null && _filePath.Contains("Play_"))
                _isUseStatic = false;

            if (sd.Bind(jsonArray[i], _isUseStatic))
            {
                if (targetStageDic.ContainsKey(sd.index))
                    Debug.Log(string.Format("({0}) stage already in _stageDic", sd.index));
                else
                {
                    if (!inTempStage)
                        targetStageDic.Add(sd.index, sd);
                    else
                        _tempStageData = sd;
                }
            }
        }

        _ready = true;
    }

    /// <summary>
    /// 맵툴에서 파일 저장 용도로 사용 중
    /// </summary>
    /// <param name="filePath"></param>
    /// <param name="inIsCompress"></param>
    public void SaveDataFile(string filePath, bool inIsCompress)
    {
        int i;
        LocalStageData stageData;

        JSONArray root_array = new JSONArray();
        JSONClass root = new JSONClass();

        for (i = 1; i <= currentStageDic.Count; i++)
        {
            stageData = currentStageDic[i];
            root_array.Add(ExportStageToJson(stageData));
        }

        root.Add("stage", root_array);

        if (inIsCompress)
        {
            filePath = filePath.Replace("StageData", "StageDataCompressed.txt");
            File.WriteAllText(filePath, root.SaveToCompressedBase64());
        }
        else
        {
            filePath += ".txt";
            JsonFormatter format = new JsonFormatter(root.ToString());
            File.WriteAllText(filePath, format.Format());
        }

        Debug.Log("SaveDataFile : " + filePath);
    }

    public JSONClass ExportStageToJson(LocalStageData data)
    {
        JSONClass result = new JSONClass();

        result = data.Export();

        return result;
    }

    public void SaveStageData(LocalStageData inNewOne)
    {
        if (currentStageDic.ContainsKey(inNewOne.index))
            currentStageDic.Remove(inNewOne.index);

        currentStageDic.Add(inNewOne.index, inNewOne);

        if (currentStageDic.Count > 0)
            _targetStageData = currentStageDic[inNewOne.index];

        _ready = true;
    }

    public void SaveTempStageData(LocalStageData inNewOne)
    {
        _tempStageData = inNewOne;
    }
    #endregion

#if UNITY_STANDALONE
    /// <summary>
    /// 통계를 위함
    /// </summary>
    /// <returns></returns>
    public Dictionary<int, LocalStageData> GetCurrentStageDic()
    {
        return _stageDic;
    }
#endif

    public int GetStageCount()
    {
        return currentStageDic.Count;
    }

    public LocalStageData GetStage()
    {
        if (_targetStageData == null || !_ready)
            return null;

        return _targetStageData;
    }

    public LocalStageData GetTempStage()
    {
        if (_tempStageData == null)
            return null;

        return _tempStageData;
    }

    /// <summary>
    /// 특정 스테이지 선택
    /// </summary>
    /// <param name="inStageIndex"></param>
    /// <returns></returns>
    public LocalStageData SelectStage(int inStageIndex)
    {
        if (!currentStageDic.ContainsKey(inStageIndex))
        {
            Debug.Assert(false, string.Format("해당 ({0})스테이지가 없습니다.", inStageIndex));
            return null;
        }

#if UNITY_STANDALONE
        _targetStageData = currentStageDic[inStageIndex];
#else
        //if (Statics.Instance.stage.ContainsKey(inStageIndex))
        //    _targetStageData = currentStageDic[Statics.Instance.stage[inStageIndex].map_id];
        //else
            _targetStageData = currentStageDic[inStageIndex];

        _targetStageData.index = inStageIndex;
#endif

        if (_tempStageData != null)
        {
            _targetStageData = _tempStageData;
            _tempStageData = null;
        }

        SetStageInfo();

        return _targetStageData;
    }

    public int GetStageIndex()
    {
        return _targetStageData.index;
    }

    public List<SeaRoute> GetAutoSeaRoute()
    {
        return _targetStageData.seaRouteList;
    }

    public SeaRoute GetSeaRoute(int inIndex)
    {
        return _targetStageData.seaRouteList[inIndex - 1];
    }

    /*
    public List<HiddenStage> GetHiddenStage()
    {
        return _targetStageData.hiddenStageList;
    }

    public List<int> GetHiddenIndexs()
    {
        List<int> result = new List<int>();

        for(int i = 0; i < _hidden_index.Length; i++)
        {
            if (_hidden_index[i] == 1)
                result.Add(i);
        }

        return result;
    }

    public void UpdateHiddenIndexs(List<int> inIndex)
    {
        foreach (int index in inIndex)
        {
            _hidden_index[index] = 0;
        }
    }

    public List<ScrollStage> GetScrollStage()
    {
        return _targetStageData.scrollStageList;
    }

    public ScrollMapView GetScrollView()
    {
        return _targetStageData.scrollViewInfo;
    }

    public List<GroundWaterData> GetGroundWaterInfo()
    {
        return _targetStageData.groundWaterList;
    }

    public List<BiscuitTilePath> GetBiscuitTileInfo()
    {
        return _targetStageData.biscuitTileList;
    }

    public List<BiscuitTilePath> GetToastTileInfo()
    {
        return _targetStageData.toastTileList;
    }

    public List<BiscuitTilePath> GetCarpetTileInfo()
    {
        return _targetStageData.carpetTileList;
    }

    /// <summary>
    /// 수로조각이 정해진 위치에 배치되었는지 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    /// <param name="inIsInit"></param>
    /// <returns></returns>
    public bool IsMatchWaterFragment(int inIndex, int inType, bool inIsInit)
    {
        int counting = 0;

        foreach (BoardData t in _waterFragments)
        {
            if (t.type == inType && t.index == inIndex)
            {
                if (!inIsInit)
                    SingletonController.Get<SoundManager>().Play("SE_StonePiece", "sfx_ingame");
                _waterFragments.RemoveAt(counting);
                GameInfo.NEED_COUNT_MISSION--;
                return true;
            }

            counting++;
        }

        return false;
    }

    public List<BoardData> GetWaterFragmentMission()
    {
        return _waterFragments;
    }

    public int GetWaterFragmentMissionCount()
    {
        return _waterFragments != null ? _waterFragments.Count : 0;
    }*/

    private void SetStageInfo()
    {
        BattleOceanController.HORIZONTAL = _targetStageData.tileSize_h;
        BattleOceanController.VERTICAL = _targetStageData.tileSize_v;
        BattleOceanController.TILE_TOTAL = BattleOceanController.HORIZONTAL * BattleOceanController.VERTICAL;

        BattleOceanController.SCROLL_X = 0;
        BattleOceanController.SCROLL_Y = 0;
        BattleOceanController.HORIZONTAL_GAP = BattleOceanController.HORIZONTAL - BattleOceanController.REGULAR_SIZE_X;
        BattleOceanController.VERTICAL_GAP = BattleOceanController.VERTICAL - BattleOceanController.REGULAR_SIZE_Y;
        BattleOceanController.SCROLL_MAX = BattleOceanController.HORIZONTAL_GAP > BattleOceanController.VERTICAL_GAP ? BattleOceanController.HORIZONTAL_GAP : BattleOceanController.VERTICAL_GAP;

        // NOTE 실제 중심 영역으로 지정한다면 Coordinate.GetPositionInGridIndex()와 같이 변경 필요
        //GameEngine.START_POS_Y = GameEngine.BLOCK_RANGE_Y + GameEngine.BLOCK_HEIGHT * (GameEngine.REGULAR_SIZE - 1);

        // 타일 정보 셋팅
        if (_tile_dic != null)
            _tile_dic.Clear();

        if (_targetStageData.tileList != null)
        {
            _tile_dic = new Dictionary<int, List<TileData>>();

            foreach (TileData bData in _targetStageData.tileList)
            {
                List<TileData> tile_datas = new List<TileData>();

                if (_tile_dic.ContainsKey(bData.index))
                {
                    tile_datas.AddRange(_tile_dic[bData.index]);
                    _tile_dic.Remove(bData.index);
                    tile_datas.Add(bData.Clone());
                    _tile_dic.Add(bData.index, tile_datas);
                }
                else
                {
                    tile_datas.Add(bData.Clone());
                    _tile_dic.Add(bData.index, tile_datas);
                }
            }
        }

        // 토큰 정보 셋팅
        if (_token_dic != null)
            _token_dic.Clear();

        if (_targetStageData.tokenList != null)
        {
            _token_dic = new Dictionary<int, List<TokenData>>();

            foreach (TokenData bData in _targetStageData.tokenList)
            {
                List<TokenData> token_datas = new List<TokenData>();

                if (_token_dic.ContainsKey(bData.index))
                {
                    token_datas.AddRange(_token_dic[bData.index]);
                    _token_dic.Remove(bData.index);
                    token_datas.Add(bData.Clone());
                    _token_dic.Add(bData.index, token_datas);
                }
                else
                {
                    token_datas.Add(bData.Clone());
                    _token_dic.Add(bData.index, token_datas);
                }
            }
        }

        // 스크롤맵 정보 초기화
        //List<ScrollStage> scrollStages = _targetStageData.scrollStageList;
        //if (scrollStages != null)
        //{
        //    foreach (ScrollStage sStage in scrollStages)
        //    {
        //        sStage.Clear();
        //    }
        //}      
    }

    /*
    public List<MissionData> GetSubMissionInfo()
    {
        return sub_mission;
    }

    /// <summary>
    /// 블럭 색상자물쇠 타입 조회
    /// </summary>
    /// <returns></returns>
    public List<int> GetBlockLockObstacle()
    {
        List<int> lockList = null;

        if (sub_mission == null)
            return null;

        foreach (MissionData m in sub_mission)
        {
            if (m.m_type <= (int)EBlockType.BLOCK_MAX)
            {
                if (lockList == null)
                    lockList = new List<int>();

                lockList.Add(m.m_type);
            }
        }

        return lockList;
    }

    public List<MissionData> GetSubMissionInfo(int inIndex)
    {
        if (sub_mission == null)
            return null;

        List<MissionData> missionDataList = new List<MissionData>();

        foreach (MissionData m in sub_mission)
        {
            if (m.target_index == inIndex)
                missionDataList.Add(m);
        }

        return missionDataList;
    }

    public bool SetSubMissionInfo(int inType, int inTargetIndex = -1)
    {
        if (sub_mission == null)
            return false;

        foreach (MissionData m in sub_mission)
        {
            if (it.IsHiddenIndex(m.target_index))
                continue;

            if (m.isCompleted)
                continue;

            if (m.m_type == inType && m.count > 0)
            {
                if (inTargetIndex > 0)
                {
                    if (m.target_index == inTargetIndex)
                    {
                        m.count--;

                        if (m.count <= 0)
                            m.isCompleted = true;

                        return true;
                    }
                }
                else
                {
                    m.count--;

                    if (m.count <= 0)
                        m.isCompleted = true;

                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inIndex"></param>
    public void RemoveSubMissionInfo(int inIndex)
    {
        if (sub_mission == null)
            return;

        for (int i = 0; i < sub_mission.Count; i++)
        {
            MissionData m = sub_mission[i];

            if (m != null && m.target_index == inIndex)
            {
                sub_mission.Remove(m);
                i--;
            }
        }
    }

    public List<CharacterData> GetCharacterData()
    {
        return _targetStageData.characterList;
    }

    public CharacterData GetCharacterDataByIndex(int inIndex)
    {
        if (_targetStageData == null || _targetStageData.characterList == null)
            return null;

        return _targetStageData.characterList.Find(
            delegate (CharacterData cd)
            {
                return cd.index == inIndex;
            });
    }

    public List<int> GetCharacterIndex()
    {
        List<int> res = new List<int>();

        if (_targetStageData.characterList == null)
            return null;

        for (int i = 0; i < _targetStageData.characterList.Count; i++)
        {
            res.Add(_targetStageData.characterList[i].index);
        }

        return res;
    }

    public EMainGoalInfo GetMissionType()
    {
        return (EMainGoalInfo)_targetStageData.goalInfo.gMainType;
    }

    /// <summary>
    /// 미션 종료 체크 턴이 남은 여부에 따라서 결과가 상이함
    /// </summary>
    /// <returns><c>true</c>, if completed mission was ised, <c>false</c> otherwise.</returns>
    /// <param name="inIsLeftTurn">If set to <c>true</c> in is left turn.</param>
    public bool IsCompletedMission(bool inIsLeftTurn)
    {
        if (GetMissionType() == EMainGoalInfo.GOAL_FIND_TREASURE || GetMissionType() == EMainGoalInfo.GOAL_MOVE_TREASURE ||
            GetMissionType() == EMainGoalInfo.GOAL_REACH_TREASURE || GetMissionType() == EMainGoalInfo.GOAL_BOSS)
        {
            if (GameInfo.MAIN_MISSION_COUNT <= 0)
                return true;
            else
                return false;
        }
        else if (GetMissionType() == EMainGoalInfo.GOAL_SCORE)
        {
            if (false == inIsLeftTurn && GameInfo.INDEX_SCORE >= _targetStageData.goalInfo.gScore_Star1)
                return true;
            else
                return false;
        }
        else if (GetMissionType() == EMainGoalInfo.GOAL_ICE)
        {
            if (false == HaveIce())
                return true;
            else
                return false;
        }
        else if (GetMissionType() == EMainGoalInfo.GOAL_RESCUE_FRIEND)
        {
            if (GameInfo.MAIN_MISSION_COUNT <= 0)
                return true;
            else
                return false;
        }
        else if (GetMissionType() == EMainGoalInfo.GOAL_ENERGY)
        {
            if (GameInfo.MAIN_MISSION_COUNT <= 0)
                return true;
            else
                return false;
        }
        else if (GetMissionType() == EMainGoalInfo.GOAL_SANDBOX)
        {
            int stoneGoalCount = _targetStageData.goalInfo.gGoalCount;
            // 맵 데이터에 정의된 개수가 없는 경우는 맵에 배치된 상자가 다 제거되어야 한다.
            if (stoneGoalCount <= 0)
            {
                if (false == it.HaveStoneBox())
                    return true;
                else
                    return false;
            }
            else
            {
                if (GameInfo.MAIN_MISSION_COUNT == 0)
                    return true;
                else
                    return false;
            }
        }

        return false;
    }

#region board info
    public bool IsStageExist(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_board_dic == null)
            return true;

        if (_board_dic.ContainsKey(inIndex))
        {
            List<BoardData> datas = _board_dic[inIndex];

            for(int i = 0; i < datas.Count; i++)
            {
                if (!Board.IsBlockOnBoard(datas[i].type))
                    return false;
            }
        }

        return true;
    }

    /// <summary>
    /// 수로 타일과의 슬라이딩 구분을 위함
    /// </summary>
    /// <param name="inIndex"></param>
    /// <returns></returns>
    public bool IsPossibleNormalSliding(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_board_dic == null)
            return true;

        if (_board_dic.ContainsKey(inIndex))
        {
            List<BoardData> datas = _board_dic[inIndex];

            for (int i = 0; i < datas.Count; i++)
            {
                if (!Board.IsNonGroundWaterTile(datas[i].type))
                    return false;
            }
        }

        return true;
    }*/

    public void RemoveTileData(int inIndex, TileData inData)
    {
        if (_tile_dic == null)
            return;

        if (_tile_dic.ContainsKey(inIndex))
        {
            _tile_dic[inIndex].Remove(inData);

            if (_tile_dic[inIndex].Count <= 0)
            {
                _tile_dic.Remove(inIndex);
            }
        }
    }

    public List<TileData> GetTileDatas(int inIndex)
    {
        if (_tile_dic == null)
            return null;

        if (_tile_dic.ContainsKey(inIndex))
            return _tile_dic[inIndex];
        else
            return null;
    }

    public void SetTileData(int inIndex, List<TileData> inData)
    {
        if (_tile_dic == null)
            _tile_dic = new Dictionary<int, List<TileData>>();

        if (!_tile_dic.ContainsKey(inIndex))
            _tile_dic.Add(inIndex, inData);
    }

    public void SetTileData(int inIndex, TileData inData)
    {
        if (_tile_dic == null)
            _tile_dic = new Dictionary<int, List<TileData>>();

        if (!_tile_dic.ContainsKey(inIndex))
        {
            List<TileData> newDataList = new List<TileData>();
            newDataList.Add(inData);
            _tile_dic.Add(inIndex, newDataList);
        }
        else
        {
            _tile_dic[inIndex].Add(inData);
        }
    }

    public void RemoveTokenData(int inIndex, TokenData inData)
    {
        if (_token_dic == null)
            return;

        if (_token_dic.ContainsKey(inIndex))
        {
            _token_dic[inIndex].Remove(inData);

            if (_token_dic[inIndex].Count <= 0)
            {
                _token_dic.Remove(inIndex);
            }
        }
    }

    public List<TokenData> GetTokenDatas(int inIndex)
    {
        if (_token_dic == null)
            return null;

        if (_token_dic.ContainsKey(inIndex))
            return _token_dic[inIndex];
        else
            return null;
    }

    public void SetTokenData(int inIndex, List<TokenData> inData)
    {
        if (_token_dic == null)
            _token_dic = new Dictionary<int, List<TokenData>>();

        if (!_token_dic.ContainsKey(inIndex))
            _token_dic.Add(inIndex, inData);
    }

    public void SetTokenData(int inIndex, TokenData inData)
    {
        if (_token_dic == null)
            _token_dic = new Dictionary<int, List<TokenData>>();

        if (!_token_dic.ContainsKey(inIndex))
        {
            List<TokenData> newDataList = new List<TokenData>();
            newDataList.Add(inData);
            _token_dic.Add(inIndex, newDataList);
        }
        else
        {
            _token_dic[inIndex].Add(inData);
        }
    }

    /*
    /// <summary>
    /// 특정 타일이 존재하고 있는지 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    /// <returns></returns>
    public bool HaveSomethingTile(int inIndex, int inType = (int)EBoard.BOARD_ICE_TILE)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_board_dic == null)
            return false;

        if (_board_dic.ContainsKey(inIndex))
        {
            List<BoardData> boardData = _board_dic[inIndex];

            for (int i = 0; i < boardData.Count; i++)
            {
                if (Board.IsIce(inType) && Board.IsIce(boardData[i].type))
                    return true;

                if (Board.IsRibbonTile(inType) && Board.IsRibbonTile(boardData[i].type))
                    return true;
            }
        }

        return false;
    }

    public bool HaveWaterTile(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_board_dic == null)
            return false;

        if (_board_dic.ContainsKey(inIndex))
        {
            List<BoardData> boardData = _board_dic[inIndex];

            for (int i = 0; i < boardData.Count; i++)
            {
                if (Board.IsGroundWater(boardData[i].type))
                    return true;
            }
        }

        return false;
    }
    #endregion

    public int GetTotalIceCount()
    {
        int iceCount = 0;

        return iceCount;
    }

    public bool HaveIce()
    {
        return false;
    }

    public int GetTotalStoneBoxCount()
    {
        int stoneBoxCount = 0;

        foreach (int key in _obstacle_multiInfo.Keys)
        {
            foreach (ObstacleData data in _obstacle_multiInfo[key])
            {
                if (data.type == (int)EObstacle.OBSTACLE_STONEBOX)
                    stoneBoxCount++;
            }
        }

        return stoneBoxCount;
    }

    public bool HaveStoneBox()
    {
        foreach (int key in _obstacle_multiInfo.Keys)
        {
            foreach (ObstacleData data in _obstacle_multiInfo[key])
            {
                if (data.type == (int)EObstacle.OBSTACLE_STONEBOX)
                    return true;
            }
        }

        return false;
    }

#region obstacle info
    /// <summary>
    /// 해당 인덱스의 해당 방향에 벽 장애물이 있는가?
    /// </summary>
    /// <returns><c>true</c>, if wall obstacle exist was ised, <c>false</c> otherwise.</returns>
    /// <param name="inIndex">In index.</param>
    /// <param name="inDir">In dir.</param>
    public bool IsWallObstacleExist(int inIndex, EDirection inDir)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (Obstacle.IsWallObstacle(obData[i].type) && obData[i].dir == inDir)
                    return true;
            }
        }

        return false;
    }

    public bool HaveWallObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (Obstacle.IsWallObstacle(obData[i].type))
                    return true;
            }
        }

        return false;
    }

    public bool HaveOnlyWallObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsWallObstacle(obData[i].type))
                    return false;
            }
        }

        return true;
    }

    public bool HaveOnlyTreasureObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (obData[i].type != (int)EObstacle.OBSTACLE_RELICBIT)
                    return false;
            }
        }

        return true;
    }

    public bool HaveOnlyTrainStopObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsTrainStop(obData[i].type))
                    return false;
            }
        }

        return true;
    }

    public bool HaveOnlyBalloonObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (obData[i].type != (int)EObstacle.OBSTACLE_BALLOON)
                    return false;
            }
        }

        return true;
    }

    public bool HaveOnlyJailObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsRestrictObstacle(obData[i].type))
                    return false;
            }

            return true;
        }

        return false;
    }

    public bool HaveOnlyPangObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsPang(obData[i].type))
                    return false;
            }

            return true;
        }

        return false;
    }

    public bool HaveOnlyJellyObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsJelly(obData[i].type))
                    return false;
            }

            return true;
        }

        return false;
    }

    public bool HaveOnlyBubbleObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (!Obstacle.IsBubble(obData[i].type))
                    return false;
            }

            return true;
        }

        return false;
    }

    public bool IsCharacterExistOnObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return true;

        bool isPass = true;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (obData[i].type != (int)EObstacle.OBSTACLE_RELICBIT && obData[i].type != (int)EObstacle.OBSTACLE_BALLOON && obData[i].type != (int)EObstacle.OBSTACLE_SPRING)
                {
                    isPass = false;
                    break;
                }
            }
        }

        return isPass;
    }

    public bool HaveOnlySpringObstacle(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            List<ObstacleData> obData = _obstacle_multiInfo[inIndex];

            for (int i = 0; i < obData.Count; i++)
            {
                if (obData[i].type != (int)EObstacle.OBSTACLE_SPRING)
                    return false;
            }
        }

        return true;
    }

    public bool IsObstacleExist(int inIndex)
    {
        if (inIndex < 0 || inIndex >= GameEngine.BLOCK_TOTAL)
            return false;

        if (_obstacle_multiInfo == null)
            return false;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
            return true;
        else
            return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inData"></param>
    /// <param name="inIsForceAdd"></param>
    public void SetObstacleData(int inIndex, List<ObstacleData> inData, bool inIsForceAdd = false)
    {
        if (_obstacle_multiInfo == null)
            _obstacle_multiInfo = new Dictionary<int, List<ObstacleData>>();

        if (!_obstacle_multiInfo.ContainsKey(inIndex))
            _obstacle_multiInfo.Add(inIndex, inData);
        else
        {
            if (inIsForceAdd)
            {
                List<ObstacleData> o = _obstacle_multiInfo[inIndex];
                o.AddRange(inData);
            }
        }
    }

    public List<ObstacleData> GetObstacleData(int inIndex)
    {
        if (_obstacle_multiInfo == null)
            return null;

        if (_obstacle_multiInfo.ContainsKey(inIndex))
            return _obstacle_multiInfo[inIndex];
        else
            return null;
    }

    public void RemoveObstacleData(int inIndex, ObstacleData inData)
    {
        if (_obstacle_multiInfo.ContainsKey(inIndex))
        {
            _obstacle_multiInfo[inIndex].Remove(inData);

            if (_obstacle_multiInfo[inIndex].Count <= 0)
            {
                _obstacle_multiInfo.Remove(inIndex);
            }
        }
    }

    public bool RemoveTreasureObstacle(int inIndex)
    {
        foreach (TreasureData t in _targetStageData.treasureList)
        {
            if (t.index != inIndex)
                continue;

            if (t.type != (int)ETreasureType.TYPE_AS_OBSTACLE)
                continue;

            int treasureIndex = t.index;
            List<ObstacleData> obList;

            // size 가로
            for (int i = 0; i < t.size[0]; i++)
            {
                // size 세로
                for (int j = 0; j < t.size[1]; j++)
                {
                    treasureIndex = t.index + i + (j * GameEngine.HORIZONTAL);

                    obList = GetObstacleData(treasureIndex);

                    RemoveObstacleData(obList[0].index, obList[0]);
                }
            }
        }

        return true;
    }
    #endregion

    /// <summary>
    /// 해당지점 트리거 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    /// <returns></returns>
    public List<TriggerData> GetTriggerData(int inIndex, int inType = (int)ETrigger.TRIGGER_NONE)
    {
        if (_targetStageData.trigger_Dic == null)
            return null;

        if (_targetStageData.trigger_Dic.ContainsKey(inIndex))
            return _targetStageData.trigger_Dic[inIndex];
        else
            return null;
    }

    /// <summary>
    /// 해당 지점에 있는 블럭 생성기 리스트 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <returns></returns>
    public List<BlockCreatorData> GetBlockCreatorData(int inIndex)
    {
        if (_blockCreatorTrigger == null)
            return null;

        if (_blockCreatorTrigger.ContainsKey(inIndex))
            return _blockCreatorTrigger[inIndex];
        else
            return null;
    }

    public List<TreasureData> GetTreasureData()
    {
        return _targetStageData.treasureList;
    }

    public TreasureData GetTreasureData(int inIndex)
    {
        if (_targetStageData.treasureList == null)
            return null;

        foreach (TreasureData td in _targetStageData.treasureList)
        {
            if (td.index == inIndex)
            {
                return td;
            }
        }

        return null;
    }

    public bool IsEndPoint(int inIndex)
    {
        if (_end_point == null)
            return false;

        return _end_point.IndexOf(inIndex) > -1 ? true : false;
    }

    public List<int> GetEndPoint()
    {
        return _end_point;
    }

    /// <summary>
    /// 미션 클리어 체크를 위해 필요한 통나무의 갯수를 계산
    /// </summary>
    /// <returns></returns>
    public int NeedLogCount()
    {
        if (_end_point == null)
            return -1;

        int totalCount = 0;

        for (int i = 0; i < _end_point.Count; i++)
        {
            int endPoint = _end_point[i];

            // 맨 하단이 아닌 도착지점은 제외
            if ((int)(endPoint / GameEngine.HORIZONTAL) != GameEngine.VERTICAL - 1)
                continue;

            if (_targetStageData.layer_block[endPoint] != (int)EBlockType.BLOCK_LOGWOOD)
                totalCount++;
        }

        return totalCount;
    }

    public bool IsHiddenIndex(int inIndex)
    {
        if (_hidden_index == null)
            return false;

        if (inIndex < 0 || inIndex >= _hidden_index.Length)
        {
            //STZDebug.LogWarning("!!!!");
            return false;
        }

        return _hidden_index[inIndex] == 1 ? true : false;
    }

#region water info
    public BoardData GetGroundWaterData(int inIndex)
    {
        if (_targetStageData == null || _targetStageData.groundWaterList == null)
            return null;

        return null;
    }

    public EDirection GetNextDir(int inIndex)
    {
        EDirection result = EDirection.DIR_NONE;

        for (int i = 0; i < _targetStageData.groundWaterList.Count; i++)
        {
            GroundWaterData o = _targetStageData.groundWaterList[i];

            List<int> full_path = o.full_path;
            int pos = full_path.IndexOf(inIndex);

            if (pos < 0)
                continue;

            // 끝 블럭은 제외
            if (pos == 0)
                continue;

            int result_index = full_path[pos - 1];

            if (Mathf.Abs(inIndex - result_index) == GameEngine.HORIZONTAL)
            {
                if (inIndex > result_index)
                    result = EDirection.DIR_TOP;
                else
                    result = EDirection.DIR_BOTTOM;
            }
            else
            {
                if (inIndex > result_index)
                    result = EDirection.DIR_LEFT;
                else
                    result = EDirection.DIR_RIGHT;
            }

            return result;
        }

        return result;
    }

    private List<int> groundWaterHeadIndexs = null;
    public List<int> GetGroundWaterHeadIndex()
    {
        if (groundWaterHeadIndexs == null)
            groundWaterHeadIndexs = new List<int>();

        //groundWaterHeadIndexs.Clear();
        else
            return groundWaterHeadIndexs;

        foreach (GroundWaterData o in _targetStageData.groundWaterList)
        {
            groundWaterHeadIndexs.Add(o.full_path[o.full_path.Count - 1]);
        }

        return groundWaterHeadIndexs;
    }
#endregion

    public bool HasStage(int inStageId)
    {
        return _stageDic_s1.ContainsKey(inStageId);
    }

    #region Map Tool
    public void SwitchStageData()
    {
        var copyDic  = InfinityTowerManager.it.IsInfinityTower() ? _stageDic_s1 : _infinityStageDic;
        currentStageDic = copyDic;
    }
    #endregion
    */
}