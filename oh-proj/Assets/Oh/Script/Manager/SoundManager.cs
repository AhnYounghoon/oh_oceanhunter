﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using STZFramework;

public class SoundManager : STZBehaviour 
{
    public enum ESoundMethod
    {
        UNIQUE = 0,
        OVERWRITE,
        OVERLAY,
        COUNT
    }

    public class SoundOption
    {
        public ESoundMethod    method      = ESoundMethod.UNIQUE;
        public float           volume      = 1f;
        public float           pan         = 0;
        public float           pitch       = 1;
        public bool            isLoop      = false;
        public bool            isAutoLoad  = false;
        public bool            isIncluePackage = true;
    }

    public class SoundClip
    {
        public SoundGroup      parentGroup      = null;
        public string          id               = string.Empty;
        public string          src              = string.Empty;
        public SoundOption     options          = new SoundOption();

        protected float        _defaultVolume   = 1;

        protected AudioClip    _loadedAudioClip = null;
        public AudioClip       audioClip { get { if (_loadedAudioClip == null) _loadedAudioClip = Resources.Load<AudioClip>(src); return _loadedAudioClip; }  }

        public SoundClip(JSONNode node, SoundOption defaultOption, SoundGroup inParentSoundGroup)
        {
            Parse(node, defaultOption, inParentSoundGroup);
        }

        public void Parse(JSONNode node, SoundOption defaultOption, SoundGroup inParentSoundGroup)
        {
            parentGroup         = inParentSoundGroup;

            src                 = node["src"].Value;
            id                  = node.ContainsKey("id")        ? node["id"].Value                              : src;
            options.volume      = node.ContainsKey("volume")    ? Mathf.Clamp(node["volume"].AsFloat, 0, 1)     : defaultOption.volume;
            options.pan         = node.ContainsKey("pan")       ? Mathf.Clamp(node["pan"].AsFloat, -1, 1)       : defaultOption.pan;
            options.pitch       = node.ContainsKey("pitch")     ? node["pitch"].AsFloat                         : defaultOption.pitch;
            options.isLoop      = node.ContainsKey("loop")      ? node["loop"].AsBool                           : defaultOption.isLoop;
            options.isAutoLoad  = node.ContainsKey("autoload")  ? node["autoload"].AsBool                       : defaultOption.isAutoLoad;
            options.isIncluePackage = node.ContainsKey("include_package") ? node["include_package"].AsBool      : defaultOption.isIncluePackage;

            _defaultVolume = options.volume;

            bool bAutoLoad = options.isAutoLoad;
#if UNITY_EDITOR
            bool bDoCheck = false;
            bAutoLoad = bAutoLoad || bDoCheck;
#endif
            if (bAutoLoad)
            {
                var clip = Resources.Load<AudioClip>(src);
                if (clip == null)
                {
                    UnityEngine.Debug.LogErrorFormat("[SOUND] FAILED IN LOADING ({0})", src);
                }
            }

            if (node.ContainsKey("method"))
            {
                string s = node["method"].Value;
                if (s.Equals("unique"))
                {
                    options.method = ESoundMethod.UNIQUE;
                }
                else if (s.Equals("overwrite"))
                {
                    options.method = ESoundMethod.OVERWRITE;
                }
                else if (s.Equals("overlay"))
                {
                    options.method = ESoundMethod.OVERLAY;
                }
            }
            else
            {
                options.method = defaultOption.method;
            }
        }

        public void SetDefaultVolume()
        {
            options.volume = _defaultVolume;
        }
    }

    public class SoundGroup
    {
        public string                           id               = string.Empty;
        public SoundOption                      defaultOptions   = new SoundOption();
        public Dictionary<string, SoundClip>    clipDic          = new Dictionary<string, SoundClip>();
        public float                            groupVolume      = 1;

        public float                            defaultGroupVolume = 1;

        public SoundGroup(JSONNode node)
        {
            Parse(node);
        }

        public void Parse(JSONNode node)
        {
            id = node["id"].Value;
            if (id.Equals(string.Empty))
            {
                Debug.LogError("[SOUND_MANAGER] 그룹의 아이디가 존재하지 않습니다.");
                return;
            }
            
            if (node.ContainsKey("default_method"))
            {
                string s = node["default_method"].Value;
                if (s.Equals("unique"))
                {
                    defaultOptions.method = ESoundMethod.UNIQUE;
                }
                else if (s.Equals("overwrite"))
                {
                    defaultOptions.method = ESoundMethod.OVERWRITE;
                }
                else if (s.Equals("overlay"))
                {
                    defaultOptions.method = ESoundMethod.OVERLAY;
                }
            }
            
            if (node.ContainsKey("default_volume"))
            {
                defaultOptions.volume = Mathf.Clamp(node["default_volume"].AsFloat, 0, 1);
            }
            
            if (node.ContainsKey("default_pan"))
            {
                defaultOptions.pan = Mathf.Clamp(node["default_pan"].AsFloat, -1, 1);
            }
            
            if (node.ContainsKey("default_pitch"))
            {
                defaultOptions.pitch = node["default_pitch"].AsFloat;
            }
            
            if (node.ContainsKey("default_loop"))
            {
                defaultOptions.isLoop = node["default_loop"].AsBool;
            }
            
            if (node.ContainsKey("default_autoload"))
            {
                defaultOptions.isAutoLoad = node["default_autoload"].AsBool;
            }

            if (node.ContainsKey("group_volume"))
            {
                groupVolume = Mathf.Clamp(node["group_volume"].AsFloat, 0, 1);
                defaultGroupVolume = groupVolume;
            }

            if (node.ContainsKey("include_package"))
            {
                defaultOptions.isIncluePackage = node["include_package"].AsBool;
            }

            JSONArray clipArr = node["clip"].AsArray;
            for (int i = 0 ; i < clipArr.Count ; i++)
            {
                SoundClip clip = new SoundClip(clipArr[i], defaultOptions, this);
                
                if (clipDic.ContainsKey(clip.id))
                {
                    Debug.LogError(string.Format("[SOUND_MANAGER] 같은 ID({0})의 클립이 존재합니다.", clip.id));
                }
                else
                {
                    clipDic.Add(clip.id, clip);
                }
            }
        }

        public void SetDefaultGroupVolume()
        {
            groupVolume = defaultGroupVolume;
        }
    }

    protected Dictionary<string, SoundGroup>    _groupDic = new Dictionary<string, SoundGroup>();

    protected List<AudioSource>                 _speakerList = new List<AudioSource>();
    protected List<SoundClip>                   _playingList = new List<SoundClip>();
    protected List<int>                         _pauseSpeakerList = new List<int>();
    protected List<string>                      _muteGroupList = new List<string>();

    protected Dictionary<int, AudioSource>      _customSpeakerDic = new Dictionary<int, AudioSource>();
    protected Dictionary<int, SoundClip>        _customPlayingDic = new Dictionary<int, SoundClip>();
    protected Stack<AudioSource>                _customSpeakerPool = new Stack<AudioSource>();
    protected List<int>                         _pauseCustomSpeakerList = new List<int>();
    protected int                               _customSpeakerID = 0;

    protected const int CHANNEL_COUNT = 32;
    protected       int _channelCount = 0;

    protected   bool _isDirty = false;

    protected const float DEFAULT_MASTER_VOLUME = 1;
    protected       float _masterVolume = DEFAULT_MASTER_VOLUME;

    protected   bool _mute = false;

    /// <summary>
    /// 음소거.
    /// </summary>
    /// <value><c>true</c> 음소거 ON; <c>false</c>음소거 OFF</value>

    public      bool mute 
    {
        set 
        {
            _mute = value;

            for (int i = 0 ; i < _channelCount ; i++)
            {
                _speakerList[i].mute = _mute;
            }

            foreach (AudioSource source in _customSpeakerDic.Values)
            {
                source.mute = _mute;
            }
        }
        get
        {
            return _mute;
        }
    }

    /// <summary>
    /// 마스터 볼륨 설정(0.0 ~ 1.0).
    /// </summary>
    /// <param name="volume">마스터 볼륨.</param>

    public void SetMasterVolume(float volume)
    {
        _masterVolume = Mathf.Clamp(volume, 0, 1);
        _isDirty = true;
    }

    /// <summary>
    /// 마스터 볼륨 값.
    /// </summary>
    /// <returns>마스터 볼륨값.</returns>

    public float GetMasterVolume()
    {
        return _masterVolume;
    }

    /// <summary>
    /// 마스터 볼륨을 기본 값으로 설정.
    /// </summary>

    public void SetDefaultMasterVolume()
    {
        _masterVolume = DEFAULT_MASTER_VOLUME;
    }

    /// <summary>
    /// 그룹 볼륨 설정(0.0~1.0).
    /// </summary>
    /// <param name="volume">그룹 볼륨.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void SetGroupVolume(float volume, string groupID = "no_group")
    {
        if (!_groupDic.ContainsKey(groupID))
        {
            return;
        }

        _groupDic[groupID].groupVolume = Mathf.Clamp(volume, 0, 1);
        _isDirty = true;
    }

    /// <summary>
    /// 그룹 볼륨 값.
    /// </summary>
    /// <returns>그룹 볼륨 값.</returns>
    /// <param name="groupID">그룹 아이디.</param>

    public float GetGroupVolume(string groupID = "no_group")
    {
        if (!_groupDic.ContainsKey(groupID))
        {
            return -1;
        }

        return _groupDic[groupID].groupVolume;
    }

    /// <summary>
    /// 그룹 볼륨을 기본값으로 설정.
    /// </summary>
    /// <param name="groupID">그룹 볼륨값.</param>

    public void SetDefaultGroupVolume(string groupID = "no_group")
    {
        if (_groupDic.ContainsKey(groupID))
        {
            _groupDic[groupID].SetDefaultGroupVolume();
        }

        _isDirty = true;
    }

    /// <summary>
    /// 클립 볼륨(0.0 ~ 1.0).
    /// </summary>
    /// <param name="volume">클립 볼륨값.</param>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void SetClipVolume(float volume, string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);

        if (clip == null)
            return;

        clip.options.volume = Mathf.Clamp(volume, 0, 1);
        _isDirty = true;
    }

    /// <summary>
    /// 클립 볼륨값.
    /// </summary>
    /// <returns>클립 볼륨값.</returns>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public float GetClipVolume(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        
        if (clip == null)
            return -1;

        return clip.options.volume;
    }

    /// <summary>
    /// 클립 볼륨 기본값으로 설정.
    /// </summary>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void SetDefaultClipVolume(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        
        if (clip != null)
        {
            clip.SetDefaultVolume();
        }
    }

    /// <summary>
    /// 사운드 정보가 기록된 json 파일 로드.
    /// </summary>
    /// <param name="src">파일 위치.</param>

    public void Load(string src)
    {
        try
        {
            StopAll();
            _groupDic.Clear();
            _playingList.Clear();
            _pauseSpeakerList.Clear();

            TextAsset text = Resources.Load<TextAsset>(src);

            if (text == null)
            {
                Debug.LogError(string.Format("[SOUND_MANAGER] {0} 파일이 없습니다.", src));
                return;
            }

            //Resources.UnloadAsset(text);

            JSONNode node = JSONNode.Parse(text.text);
            JSONArray groupArr = node["sound_manager"]["group"].AsArray;

            for (int i = 0; i < groupArr.Count; i++)
            {
                SoundGroup group = new SoundGroup(groupArr[i]);

                if (_groupDic.ContainsKey(group.id))
                {
                    Debug.LogError(string.Format("[SOUND_MANAGER] 같은 ID({0})의 그룹이 존재합니다.", group.id));
                }
                else
                {
                    _groupDic.Add(group.id, group);
                }
            }

            _channelCount = node["sound_manager"].ContainsKey("channel_count") ? node["sound_manager"]["channel_count"].AsInt : CHANNEL_COUNT;
            _masterVolume = node["sound_manager"].ContainsKey("master_volume") ? node["sound_manager"]["master_volume"].AsInt : 1;

            for (int i = 0; i < _speakerList.Count; i++)
            {
                Destroy(_speakerList[i].gameObject);
            }

            _speakerList.Clear();
            _playingList.Clear();

            for (int i = 0; i < _channelCount; i++)
            {
                GameObject speaker = new GameObject();
                speaker.AddComponent<AudioSource>();
                speaker.name = "speaker" + i.ToString();
                speaker.transform.SetParent(this.transform, false);

                _speakerList.Add(speaker.GetComponent<AudioSource>());
                _playingList.Add(null);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);
        }
    }

    /// <summary>
    /// 사운드 클립 가져오기.
    /// </summary>
    /// <returns>사운드 클립.(없으면 null)</returns>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    protected SoundClip GetSoundClip(string clipID, string groupID)
    {
        SoundClip clip = null;
        if (_groupDic.ContainsKey(groupID))
        {
            SoundGroup group = _groupDic[groupID];
            
            if (group.clipDic.ContainsKey(clipID))
            {
                clip = group.clipDic[clipID];
            }
        }

        return clip;
    }

    /// <summary>
    /// 채널 사운드 재생.
    /// </summary>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>
    /// <param name="delay">딜레이.</param>

    public void Play(string clipID, string groupID = "no_group", ulong delay = 0)
    {
        /* 뮤트 되어 있는 그룹은 사운드를 재생하지 않는다. */
        if (_muteGroupList.Contains(groupID))
            return;

        SoundClip clip = GetSoundClip(clipID, groupID);

        if (clip == null)
        {
            Debug.LogWarning(string.Format("Cannot find soundclip [{0}] of group [{1}]", clipID, groupID));
            return;
        }

        bool enableToPlay = false;
        switch (clip.options.method)
        {
        case ESoundMethod.UNIQUE:
            enableToPlay = true;
            for (int i = 0 ; i < _channelCount ; i++)
            {
                if (_playingList[i] == clip && !_pauseSpeakerList.Contains(i))
                {
                    enableToPlay = false;
                    break;
                }
            }
            break;

        case ESoundMethod.OVERWRITE:
            enableToPlay = true;
            for (int i = 0 ; i < _channelCount ; i++)
            {
                if (_playingList[i] == clip && !_pauseSpeakerList.Contains(i))
                {
                    _speakerList[i].Stop();
                }
            }
            break;

        case ESoundMethod.OVERLAY:
            enableToPlay = true;
            break;
        }

        if (enableToPlay)
        {
            for (int i = 0 ; i < _channelCount ; i++)
            {
                if (!_speakerList[i].isPlaying)
                {
                    _playingList[i]         = clip;

                    _speakerList[i].clip    = clip.audioClip;
                    _speakerList[i].pitch   = clip.options.pitch;
                    _speakerList[i].loop    = clip.options.isLoop;
                    _speakerList[i].volume  = Mathf.Clamp(clip.options.volume * clip.parentGroup.groupVolume * _masterVolume, 0, 1);
                    _speakerList[i].panStereo     = clip.options.pan;

                    // 패키지내에 포함되지 않은 오디오클립이면 어셋번들 로딩
                    if (!clip.parentGroup.defaultOptions.isIncluePackage || !clip.options.isIncluePackage)
                    {
                    }
                    else
                        _speakerList[i].Play(delay);
                    break;
                }
            }
        }

        return;
    }

    public bool IsClipAvailable(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        return clip != null;
    }

    /// <summary>
    /// 채널 사운드 정지.
    /// </summary>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void StopClip(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        
        if (clip == null)
            return;

        for (int i = 0 ; i < _channelCount ; i++)
        {
            if (_playingList[i] == clip)
            {
                _speakerList[i].Stop();
                _playingList[i]         = null;
                _speakerList[i].clip    = null;

                _pauseSpeakerList.Remove(i);
            }
        }
    }

    /// <summary>
    /// 채널 내 특정 그룹 사운드 정지.
    /// </summary>
    /// <param name="groupID">그룹 아이디.</param>

    public void StopGroup(string groupID = "no_group")
    {
        if (!_groupDic.ContainsKey(groupID))
            return;

        SoundGroup group = _groupDic[groupID];
        foreach (SoundClip clip in group.clipDic.Values)
        {
            for (int i = 0 ; i < _channelCount ; i++)
            {
                if (_playingList[i] == clip)
                {
                    _speakerList[i].Stop();
                    _playingList[i]         = null;
                    _speakerList[i].clip    = null;
                    
                    _pauseSpeakerList.Remove(i);
                }
            }
        }
    }

    /// <summary>
    /// 모든 채널 사운드 정지.
    /// </summary>

    public void StopAll()
    {
        for (int i = 0 ; i < _channelCount ; i++)
        {
            _speakerList[i].Stop();
            _playingList[i]         = null;
            _speakerList[i].clip    = null;
        }

        _pauseSpeakerList.Clear();
    }

    /// <summary>
    /// 특정 채널 사운드 일시정지.
    /// </summary>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void Pause(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        
        if (clip == null)
            return;
        
        for (int i = 0 ; i < _channelCount ; i++)
        {
            if (_playingList[i] == clip && _speakerList[i].isPlaying)
            {
                _speakerList[i].Pause();

                if (!_pauseSpeakerList.Contains(i))
                    _pauseSpeakerList.Add(i);
            }
        }
    }

    /// <summary>
    /// 채널 내 특정 그룹 사운드 일시정지.
    /// </summary>
    /// <param name="groupID">그룹 아이디.</param>
    
    public void Pause(string groupID = "no_group")
    {
        if (!_groupDic.ContainsKey(groupID))
            return;
        
        SoundGroup group = _groupDic[groupID];
        foreach (SoundClip clip in group.clipDic.Values)
        {
            for (int i = 0 ; i < _channelCount ; i++)
            {
                if (_playingList[i] == clip && _speakerList[i].isPlaying)
                {
                    _speakerList[i].Pause();
                    
                    if (!_pauseSpeakerList.Contains(i))
                        _pauseSpeakerList.Add(i);
                }
            }
        }
    }

    /// <summary>
    /// 모든 채널 사운드 일시정지.
    /// </summary>

    public void PauseAll()
    {
        for (int i = 0 ; i < _channelCount ; i++)
        {
            if (!_speakerList[i].isPlaying)
                continue;

            _speakerList[i].Pause();

            if (!_pauseSpeakerList.Contains(i))
                _pauseSpeakerList.Add(i);
        }
    }

    /// <summary>
    /// 일시정지 중인 특정 채널 사운드 재생.
    /// </summary>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>

    public void Resume(string clipID, string groupID = "no_group")
    {
        SoundClip clip = GetSoundClip(clipID, groupID);
        
        if (clip == null)
            return;

        for (int i = 0 ; i < _pauseSpeakerList.Count ; i++)
        {
            int speakerIdx = _pauseSpeakerList[i];
            if (_playingList[speakerIdx] == clip)
            {
                _speakerList[speakerIdx].Play();
                _pauseSpeakerList.Remove(speakerIdx);
                i--;
            }
        }
    }

    /// <summary>
    /// 채널 내 일시정지 중인 특정 그룹 사운드 재생.
    /// </summary>
    /// <param name="groupID">그룹 아이디.</param>
    
    public void Resume(string groupID = "no_group")
    {
        if (!_groupDic.ContainsKey(groupID))
            return;
        
        SoundGroup group = _groupDic[groupID];
        foreach (SoundClip clip in group.clipDic.Values)
        {
            for (int i = 0 ; i < _pauseSpeakerList.Count ; i++)
            {
                int speakerIdx = _pauseSpeakerList[i];
                if (_playingList[speakerIdx] == clip)
                {
                    _speakerList[speakerIdx].Play();
                    _pauseSpeakerList.Remove(speakerIdx);
                    i--;
                }
            }
        }
    }

    /// <summary>
    /// 일시정지 중인 모든 채널 사운드 재생.
    /// </summary>

    public void ResumeAll()
    {
        for (int i = 0 ; i < _pauseSpeakerList.Count ; i++)
        {
            _speakerList[_pauseSpeakerList[i]].Play();
        }

        _pauseSpeakerList.Clear();
    }

    /// <summary>
    /// 커스텀 스피커 생성.
    /// </summary>
    /// <returns>커스텀 스피커 아이디.</returns>

    public int CreateCustomSpeaker()
    {
        _customSpeakerID++;
        GameObject speaker;

        if (_customSpeakerPool.Count > 0)
        {
            speaker = _customSpeakerPool.Pop().gameObject;
            speaker.gameObject.SetActive(true);
        }
        else
        {
            speaker = new GameObject();
            speaker.AddComponent<AudioSource>();
            speaker.name = "custom_speaker" + _customSpeakerID.ToString();
            speaker.transform.SetParent(transform, false);
        }

        speaker.GetComponent<AudioSource>().mute = _mute;

        _customSpeakerDic.Add(_customSpeakerID, speaker.GetComponent<AudioSource>());
        _customPlayingDic.Add(_customSpeakerID, null);

        return _customSpeakerID;
    }

    /// <summary>
    /// 커스텀 스피커 재생.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>
    /// <param name="clipID">클립 아이디.</param>
    /// <param name="groupID">그룹 아이디.</param>
    /// <param name="delay">딜레이.</param>

    public void PlayCustomSpeaker(int customSpeakerID, string clipID, string groupID = "no_group", ulong delay = 0)
    {
        if (!_customSpeakerDic.ContainsKey(customSpeakerID))
        {
            return;
        }

        AudioSource speaker = _customSpeakerDic[customSpeakerID];

        SoundClip clip = null;
        if (_groupDic.ContainsKey(groupID))
        {
            SoundGroup group = _groupDic[groupID];
            
            if (group.clipDic.ContainsKey(clipID))
            {
                clip = group.clipDic[clipID];
            }
        }
        
        if (clip == null)
            return;

        bool enableToPlay = false;
        if (clip == _customPlayingDic[customSpeakerID])
        {
            switch (clip.options.method)
            {
            case ESoundMethod.UNIQUE:
                enableToPlay = _customPlayingDic[customSpeakerID] != clip;
                break;
                
            case ESoundMethod.OVERWRITE:
                enableToPlay = true;
                if (_customPlayingDic[customSpeakerID] == clip)
                {
                    speaker.Stop();
                }
                break;
                
            case ESoundMethod.OVERLAY:
                enableToPlay = _customPlayingDic[customSpeakerID] != clip;
                break;
            }
        }
        else
        {
            enableToPlay = true;
            _customPlayingDic[customSpeakerID] = clip;
        }

        if (enableToPlay)
        {
            speaker.clip    = clip.audioClip;
            speaker.pitch   = clip.options.pitch;
            speaker.loop    = clip.options.isLoop;
            speaker.volume  = Mathf.Clamp(clip.options.volume * clip.parentGroup.groupVolume * _masterVolume, 0, 1);
            speaker.panStereo     = clip.options.pan;
            
            speaker.Play(delay);
        }
        
        return;
    }

    /// <summary>
    /// 커스텀 스피커 제거.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>

    public void RemoveCustomSpeaker(int customSpeakerID)
    {
        if (_customSpeakerDic.ContainsKey(customSpeakerID))
        {
            _customSpeakerDic[customSpeakerID].gameObject.SetActive(false);
            _customSpeakerPool.Push(_customSpeakerDic[customSpeakerID]);

            _customSpeakerDic.Remove(customSpeakerID);
            _customPlayingDic.Remove(customSpeakerID);
        }
    }

    /// <summary>
    /// 모든 커스텀 스피커 제거.
    /// </summary>

    public void ClearCustomSpeaker()
    {
        foreach (int customSpeakerID in _customSpeakerDic.Keys)
        {
            _customSpeakerDic[customSpeakerID].gameObject.SetActive(false);
            _customSpeakerPool.Push(_customSpeakerDic[customSpeakerID]);
        }

        _customSpeakerDic.Clear();
        _customPlayingDic.Clear();
        _pauseSpeakerList.Clear();
    }

    /// <summary>
    /// 커스텀 스피커 일시 정지.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>

    public void PauseCustomSpeaker(int customSpeakerID)
    {
        if (_customSpeakerDic.ContainsKey(customSpeakerID)
            && _customSpeakerDic[customSpeakerID].isPlaying)
        {
            _pauseCustomSpeakerList.Add(customSpeakerID);
            _customSpeakerDic[customSpeakerID].Pause();
        }

    }

    /// <summary>
    /// 모든 커스텀 스피커 일시 정지.
    /// </summary>

    public void PauseAllCustomSpeakers()
    {
        foreach (int customSpeakerID in _customSpeakerDic.Keys)
        {
            PauseCustomSpeaker(customSpeakerID);
        }
    }

    /// <summary>
    /// 일시 정지된 특정 커스텀 스피커 재생.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>

    public void ResumeCustomSpeaker(int customSpeakerID)
    {
        if (_pauseCustomSpeakerList.Contains(customSpeakerID))
        {
            _pauseCustomSpeakerList.Remove(customSpeakerID);
            _customSpeakerDic[customSpeakerID].Play();
        }
    }

    /// <summary>
    /// 일시 정지된 모든 커스텀 스피커 재생.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>

    public void ResumeAllCustomSpeakers()
    {
        for (int i = 0 ; i < _pauseCustomSpeakerList.Count ; i++)
        {
            ResumeCustomSpeaker(_pauseCustomSpeakerList[i]);
        }
    }

    /// <summary>
    /// 특정 커스텀 스피커 정지.
    /// </summary>
    /// <param name="customSpeakerID">커스텀 스피커 아이디.</param>

    public void StopCustomSpeaker(int customSpeakerID)
    {
        if (_pauseCustomSpeakerList.Contains(customSpeakerID))
        {
            _pauseCustomSpeakerList.Remove(customSpeakerID);
            _customSpeakerDic[customSpeakerID].Stop();
        }
    }

    /// <summary>
    /// 모든 커스텀 스피커 정지.
    /// </summary>

    public void StopAllCustomSpeakers()
    {
        foreach (int customSpeakerID in _customSpeakerDic.Keys)
        {
            ResumeCustomSpeaker(customSpeakerID);
        }
    }

    void Update()
    {
        if (!_isDirty)
            return;

        _isDirty = false;

        for (int i = 0 ; i < _channelCount ; i++)
        {
            if (_playingList[i] != null)
            {
                _speakerList[i].volume = Mathf.Clamp(_masterVolume * _playingList[i].options.volume * _playingList[i].parentGroup.groupVolume, 0, 1);
            }
        }

        foreach (int index in _customSpeakerDic.Keys)
        {
            if (_customPlayingDic[index] != null)
            {
                _customSpeakerDic[index].volume = Mathf.Clamp(_masterVolume * _customPlayingDic[index].options.volume * _customPlayingDic[index].parentGroup.groupVolume, 0, 1);
            }
        }
    }

    /// <summary>
    /// 그룹에 최초 설정한 볼륨 크기 가져오기.
    /// </summary>
    /// <param name="groupID">그룹 명.</param>
    /// <returns>볼륨 크기</returns>

    public float GetDefaultGroupVolume(string groupID = "no_group")
    {
        if (_groupDic.ContainsKey(groupID))
        {
            return _groupDic[groupID].defaultGroupVolume;
        }

        return 0;
    }

    public SoundGroup GetSoundGroup(string groupID = "no_group")
    {
        if (_groupDic.ContainsKey(groupID))
            return _groupDic[groupID];
        return null;
    }

    public void OnApplicationFocus(bool inFocused)
    {
        mute = inFocused ? false : true;
    }
}
