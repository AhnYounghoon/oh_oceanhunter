﻿using UnityEngine;
using System.Collections;

/// <summary>
/// MonoBehaviour를 상속받지 않는 클래스에서 코루틴을 사용하기 위한 매니저 클래스.
/// </summary>
public class CoroutineManager : MonoBehaviour
{
}
