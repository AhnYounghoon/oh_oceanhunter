﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public enum EObserverEventKey
{
    T = 0,
    TILE_CLICK = 1,
    TOKEN_MOVED = 2,

    QUEST_UPDATE = 3,
    GOODS_UPDATE = 4,
    CHAR_UPGRADE = 5,
}

public interface IHashReceiver
{
    void ReceiveEvent(EObserverEventKey inEventKey, Hashtable inParam);
}

public class ObserverManager
{
    static ObserverManager _instance;

    public static ObserverManager it { get { if (_instance == null) { _instance = new ObserverManager(); } return _instance;  } }

    List<IHashReceiver>[] _receiverList = new List<IHashReceiver>[System.Enum.GetValues(typeof(EObserverEventKey)).Length];

    ObserverManager()
    {
        for (int i = 0; i < _receiverList.Length; i++)
        {
            _receiverList[i] = new List<IHashReceiver>();
        }
    }

    public void AddEventListener(IHashReceiver inReceiver, EObserverEventKey inEventKey)
    {
        int idx = (int)inEventKey;
        var list = _receiverList[idx];
        if (!list.Contains(inReceiver))
            list.Add(inReceiver);
    }

    public void RemoveEventListener(IHashReceiver inReceiver)
    {
        for (int i = 0, length = _receiverList.Length; i < length; i++)
        {
            _receiverList[i].Remove(inReceiver);
        }
    }

    public void RemoveEventListener(IHashReceiver inReceiver, EObserverEventKey inEventKey)
    {
        _receiverList[(int)inEventKey].Remove(inReceiver);
    }

    public void RemoveAllEventListeners()
    {
        for (int i = 0, length = _receiverList.Length; i < length; i++)
        {
            _receiverList[i].Clear();
        }
    }

    public void Dispatch(EObserverEventKey inEventKey, Hashtable inParam)
    {
        var list = _receiverList[(int)inEventKey];
        for (int i = 0, length = list.Count; i < length; i++)
        {
            if (list[i] != null)
                list[i].ReceiveEvent(inEventKey, inParam);
        }
    }
}
