﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using STZFramework;

public class GeneralDataManager : SecurityContext
{
    public enum ESceneName
    {
        None = 0,
        Bridge,
        Title,
        BattleScene,
        BattleOceanScene,
        MapTool,
        Intro,
        Opening,
        LobbyScene,
        BattleSimulationTool,
    }

    enum EDataKey
    {
        I_PREV_SCENE,
        I_CURRENT_SCENE,
        I_PREV_STAGE,
        I_TOKEN_INDEX,
        B_BOSS,
        I_LAST_CLEAR_QUEST_ID,
    }

    /* 인스턴스 */
    static GeneralDataManager _instance;

    /// <summary>
    /// 인스턴스 가져오기
    /// </summary>
    public static GeneralDataManager it { get { if (_instance == null) _instance = new GeneralDataManager(); return _instance; } }

    /// <summary>
    /// 이전 스테이지 값
    /// </summary>
    public int prevStage { set { Set<int>(EDataKey.I_PREV_STAGE.ToString(), value); } get { return Get<int>(EDataKey.I_PREV_STAGE.ToString()); } }

    /// <summary>
    /// 전투를 할 토큰 인덱스
    /// </summary>
    public int tokenIndex { set { Set<int>(EDataKey.I_TOKEN_INDEX.ToString(), value); } get { return Get<int>(EDataKey.I_TOKEN_INDEX.ToString()); } }

    public bool isBoss { set { Set<bool>(EDataKey.B_BOSS.ToString(), value); } get { return Get<bool>(EDataKey.B_BOSS.ToString()); } }
    public int lastClearQuestID { set { Set<int>(EDataKey.I_LAST_CLEAR_QUEST_ID.ToString(), value); } get { return Get<int>(EDataKey.I_LAST_CLEAR_QUEST_ID.ToString()); } }

    /// <summary>
    /// 이전 스테이지 이름
    /// </summary>
    public ESceneName prevSceneName { /*protected*/ set { Set<int>(EDataKey.I_PREV_SCENE.ToString(), (int)value); } get { return (ESceneName)Get<int>(EDataKey.I_PREV_SCENE.ToString()); } }

    /// <summary>
    /// 최근 로드한 스테이지 이름
    /// </summary>
    public ESceneName currentSceneName { /*protected*/ set { Set<int>(EDataKey.I_CURRENT_SCENE.ToString(), (int)value); } get { return (ESceneName)Get<int>(EDataKey.I_CURRENT_SCENE.ToString()); } }

    /// <summary>
    /// 다음 씬으로 전환
    /// </summary>
    /// <param name="sceneName"></param>
    public void NextScene(ESceneName inSceneName)
    {
        prevSceneName = (ESceneName)System.Enum.Parse(typeof(ESceneName), SceneManager.GetActiveScene().name);
        currentSceneName = inSceneName;

        if (inSceneName == ESceneName.Title)
        {
            //ServerRequestManager.Instance.Initialize();
            //ServerModel.Instance.Clear();
            //Kakao.Friends.Instance.Clear();
        }

        SceneManager.LoadScene(ESceneName.Bridge.ToString());
    }

    public enum EParam
    {
        FLOW,
        GOODS,
        REWARD,
        QUEST,
        FLEET,  // 함대 정보
        PERSON, // 선장 & 선원 보유 정보
        HERO,
        SAILOR,
        STAGE_CLEAR,
        INGAME_WIN,
        GOLD_GACHA_COUNT,
        SILVER_GACHA_COUNT,

        VALUE,
        COUNT,
    }

    FileMap _fileMap = new FileMap();
    private string secretKey = "oh-proj";
    string cachingDataPath { get { return StringHelper.Format("{0}/{1}.dat", Application.temporaryCachePath, "data".MD5()); } }

    /// <summary>
    /// 로컬에 저장하는 정보 변경
    /// 증가 감소처리
    /// </summary>
    /// <param name="inDataType"></param>
    /// <param name="sub"></param>
    /// <param name="count"></param>
    public void UpdateData(EParam inDataType, string sub, int count = 1, int inType = 0, string inStrVal = null, CharacterLevelInfo inLevelInfo = null)
    {
        int value = 0;
        FileMap fm;

        switch (inDataType)
        {
            case EParam.FLEET:
                {
                    Dictionary<int, string> dicVal = _fileMap.Get<Dictionary<int, string>>(sub);
                    if (dicVal == null)
                        dicVal = new Dictionary<int, string>();

                    if (dicVal.ContainsKey(inType))
                    {
                        dicVal.Remove(inType);
                    }

                    dicVal[inType] = inStrVal;

                    _fileMap.Set<Dictionary<int, string>>(inDataType.ToString(), dicVal);
                }
                break;
            case EParam.PERSON:
                {
                    fm = _fileMap.Get<FileMap>(inDataType.ToString());
                    if (fm == null) fm = new FileMap();

                    Dictionary<int, CharacterLevelInfo> dicVal = fm.Get<Dictionary<int, CharacterLevelInfo>>(sub);
                    if (dicVal == null)
                        dicVal = new Dictionary<int, CharacterLevelInfo>();

                    if (dicVal.ContainsKey(inType))
                    {
                        //dicVal[inType] += count;
                        dicVal.Remove(inType);
                        dicVal[inType] = inLevelInfo;
                    }
                    else
                        dicVal.Add(inType, inLevelInfo);

                    fm.Set<Dictionary<int, CharacterLevelInfo>>(sub, dicVal);
                    _fileMap.Set<FileMap>(inDataType.ToString(), fm);
                }
                break;
            case EParam.INGAME_WIN:
            case EParam.STAGE_CLEAR:
            case EParam.FLOW:
                _fileMap.Set<int>(inDataType.ToString(), count);
                break;
            case EParam.GOLD_GACHA_COUNT:
            case EParam.SILVER_GACHA_COUNT:
                value = _fileMap.Get<int>(inDataType.ToString());
                value++;
                _fileMap.Set<int>(inDataType.ToString(), value);
                break;
            case EParam.QUEST:
                fm = _fileMap.Get<FileMap>(inDataType.ToString());
                if (fm == null) fm = new FileMap();
                value = fm.Get<int>(sub);
                value += count;

                // 신규 퀘스트 획득시 카운트 변경
                if (value == -1)
                {
                    int questCount = fm.Get<int>(EParam.COUNT.ToString());
                    fm.Set<int>(EParam.COUNT.ToString(), ++questCount);

                    fm.Set<int>(sub, value);
                    _fileMap.Set<FileMap>(inDataType.ToString(), fm);

                    // 퀘스트 이벤트 dispatch
                    ObserverManager.it.Dispatch(EObserverEventKey.QUEST_UPDATE, STZCommon.Hash("gain_quest", sub));
                }
                // 퀘스트 진행
                else if (value == 0)
                {
                    fm.Set<int>(sub, value);
                    _fileMap.Set<FileMap>(inDataType.ToString(), fm);

                    // 퀘스트 이벤트 dispatch
                    ObserverManager.it.Dispatch(EObserverEventKey.QUEST_UPDATE, STZCommon.Hash("progress_quest", sub));
                }
                else
                {
                    // 완료 체크후 보상 지급 및 다음 퀘스트 생성
                    ServerStatic.Quest targetQuest = ServerStatic.Quest.GetQuestById(sub.IntValue());
                    if (value == targetQuest.count)
                    {
                        value = 100000;
                        lastClearQuestID = sub.IntValue();

                        fm.Set<int>(sub, value);
                        _fileMap.Set<FileMap>(inDataType.ToString(), fm);

                        //AcquireReward(targetQuest.reward);
                        //AcquireQuest(sub.IntValue());
                        return;
                    }

                    fm.Set<int>(sub, value);
                    _fileMap.Set<FileMap>(inDataType.ToString(), fm);
                }                
                break;
            case EParam.GOODS:
                fm = _fileMap.Get<FileMap>(inDataType.ToString());
                if (fm == null) fm = new FileMap();
                value = fm.Get<int>(sub);
                value += count;
                fm.Set<int>(sub, value);
                _fileMap.Set<FileMap>(inDataType.ToString(), fm);

                // 재화 변경 dispatch
                ObserverManager.it.Dispatch(EObserverEventKey.GOODS_UPDATE, STZCommon.Hash("change_goods", sub));
                break;
        }

        SaveData();
    }

    public void AcquireReward(string inStrReward)
    {
        RewardData[] rewards = RewardData.ParseItemIDs(inStrReward);
        for (int i = 0; i < rewards.Length; i++)
        {
            switch ((ERewardType)rewards[i].reward_type)
            {
                case ERewardType.GOLD:
                    UpdateData(EParam.GOODS, ERewardType.GOLD.ToString(), rewards[i].reward_count);
                    break;
                case ERewardType.SILVER:
                    UpdateData(EParam.GOODS, ERewardType.SILVER.ToString(), rewards[i].reward_count);
                    break;
                case ERewardType.GOLD_BOX:
                    UpdateData(EParam.GOODS, ERewardType.GOLD_BOX.ToString(), rewards[i].reward_count);
                    break;
                case ERewardType.SILVER_BOX:
                    UpdateData(EParam.GOODS, ERewardType.SILVER_BOX.ToString(), rewards[i].reward_count);
                    break;
            }
        }
    }

    public void AcquireQuest(int currQuestId)
    {
        int nextQuestId = currQuestId + 1;

        ServerStatic.Quest targetQuest = ServerStatic.Quest.GetQuestById(nextQuestId);

        if (targetQuest == null)
            return;

        UpdateData(EParam.QUEST, nextQuestId.ToString(), -1);
    }

    public int GetGachaCount(EParam type)
    {
        return _fileMap.Get<int>(type.ToString());
    }

    public bool CheckFlowData(EParam type, int value)
    {
        return _fileMap.Get<int>(type.ToString()) >= value;
    }

    public FileMap GetQuestData()
    {
        return _fileMap.Get<FileMap>(EParam.QUEST.ToString());
    }

    public bool IsCompleteQuest(string sub)
    {
        return _fileMap.Get<FileMap>(EParam.QUEST.ToString()).Get<int>(sub) == int.MaxValue;
    }

    public int GetStageClearData()
    {
        return _fileMap.Get<int>(EParam.STAGE_CLEAR.ToString());
    }

    public bool GetLastBattleWin()
    {
        return _fileMap.Get<int>(EParam.INGAME_WIN.ToString()) > 0 ? true : false;
    }

    public int GetGoodsData(ERewardType inType = ERewardType.NONE)
    {
        if (_fileMap == null)
            return 0;

        if (_fileMap.Get<FileMap>(EParam.GOODS.ToString()) == null)
            return 0;

        return _fileMap.Get<FileMap>(EParam.GOODS.ToString()).Get<int>(inType.ToString());
    }

    /// <summary>
    /// 함대 정보 조회
    /// key는 함대 id, value는 선장:선원,선장:선원..
    /// </summary>
    /// <returns></returns>
    public Dictionary<int, string> GetFleetData()
    {
        if (_fileMap == null)
            return null;

        return _fileMap.Get<Dictionary<int, string>>(EParam.FLEET.ToString());
    }

    /// <summary>
    /// 사람 경험치 조회
    /// </summary>
    /// <param name="inType"></param>
    /// <param name="inId"></param>
    /// <returns></returns>
    public CharacterLevelInfo GetPersonLevelInfo(int inType, int inId)
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        EParam type = inType == (int)ServerStatic.Character.ECatetory.HERO ? EParam.HERO : EParam.SAILOR;
        Dictionary<int, CharacterLevelInfo> targetPerson = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(type.ToString());

        if (targetPerson == null || !targetPerson.ContainsKey(inId))
            return null;

        return targetPerson[inId];
    }

    public int GetPersonLevel(int inExp)
    {
        int levelRequireAmount = 0;

        for (int i = 1; i <= 100; i++)
        {
            levelRequireAmount = i * 10;

            if (inExp - levelRequireAmount < 0)
                return i;

            inExp -= levelRequireAmount;
        }

        return 0;
    }

    public int GetExpByLevel(int inExp)
    {
        int levelRequireAmount = 0;

        for (int i = 1; i <= 100; i++)
        {
            levelRequireAmount = i * 10;

            if (inExp - levelRequireAmount < 0)
                return inExp;

            inExp -= levelRequireAmount;
        }

        return 0;
    }

    public int GetMaxExpByLevel(int inLevel)
    {
        int result = 0;

        for (int i = 1; i <= inLevel; i++)
        {
            result += (i * 10);
        }

        return result;
    }

    /// <summary>
    /// 영웅 Dictionary 조회 - 보유한 영웅 별 개수
    /// </summary>
    /// <returns></returns>
    public Dictionary<int, CharacterLevelInfo> GetHeroDictionary()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> dicVal = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.HERO.ToString());
        return dicVal;
    }

    /// <summary>
    /// 영웅 List 조회 - 보유한 영웅 unique id 리스트
    /// </summary>
    /// <returns></returns>
    public List<int> GetHeroList()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> dicVal = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.HERO.ToString());

        List<int> result = new List<int>();
        foreach(int key in dicVal.Keys)
        {
            result.Add(key);
        }
        return result;
    }

    public List<int> GetAllPersionList()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> dicVal1 = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.HERO.ToString());
        Dictionary<int, CharacterLevelInfo> dicVal2 = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.SAILOR.ToString());

        List<int> result = new List<int>();
        foreach (int key in dicVal1.Keys)
        {
            result.Add(key);
        }
        foreach (int key in dicVal2.Keys)
        {
            result.Add(key);
        }

        return result;
    }

    public Dictionary<int, CharacterLevelInfo> GetAllPersionDictionary()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> result = new Dictionary<int, CharacterLevelInfo>();
        Dictionary<int, CharacterLevelInfo> dicVal1 = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.HERO.ToString());
        Dictionary<int, CharacterLevelInfo> dicVal2 = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.SAILOR.ToString());

        if (dicVal1 != null)
        {
            foreach (int key in dicVal1.Keys)
            {
                if (!result.ContainsKey(key))
                    result.Add(key, dicVal1[key]);
            }
        }

        if (dicVal2 != null)
        {
            foreach (int key in dicVal2.Keys)
            {
                if (!result.ContainsKey(key))
                    result.Add(key, dicVal2[key]);
            }
        }

        return result;
    }

    /// <summary>
    /// 선원 Dictionary 조회 - 보유한 선원 별 개수
    /// </summary>
    /// <returns></returns>
    public Dictionary<int, CharacterLevelInfo> GetSailorDictionary()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> dicVal = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.SAILOR.ToString());
        return dicVal;
    }

    /// <summary>
    /// 선원 List 조회 - 보유한 선원 unique id 리스트
    /// </summary>
    /// <returns></returns>
    public List<int> GetSailorList()
    {
        if (_fileMap == null)
            return null;

        if (_fileMap.Get<FileMap>(EParam.PERSON.ToString()) == null)
            return null;

        Dictionary<int, CharacterLevelInfo> dicVal = _fileMap.Get<FileMap>(EParam.PERSON.ToString()).Get<Dictionary<int, CharacterLevelInfo>>(EParam.SAILOR.ToString());
        if (dicVal == null)
            return null;

        List<int> result = new List<int>();
        foreach (int key in dicVal.Keys)
        {
            result.Add(key);
        }
        return result;
    }

    public void SaveData()
    {
        FileIOExtension.SaveAsFile(_fileMap, cachingDataPath, secretKey);
    }

    public void LoadData()
    {
        _fileMap = FileIOExtension.LoadFromFile<FileMap>(cachingDataPath, secretKey);
        if (_fileMap == null)
        {
            _fileMap = new FileMap();
        }
    }

    public void RemoveData()
    {
        FileIOExtension.RemoveFile(cachingDataPath);
    }

    public new void Clear()
    {
        base.Clear();
    }

    /// <summary>
    /// 잠시 여기 보관
    /// TODO @minjun 테스트 코드 삭제
    /// </summary>
    public System.Collections.Generic.List<ServerStatic.Character> characterList = new System.Collections.Generic.List<ServerStatic.Character>()
    {
        null,
        null,
        null,
        null,
        null,
    };

    /// <summary>
    /// 어디서든지 변경가능하게 하기 위함 추후 ServerModel로 대체되어야 함
    /// </summary>

    public System.Collections.Generic.List<FleetData> playerFleetList { get; set; }


    public System.Collections.Generic.List<PlayData> playerData { set; get; }
    public System.Collections.Generic.List<PlayData> enemyData { get; set; }

    public ServerStatic.BattleObject targetBattleObject = null;
}
