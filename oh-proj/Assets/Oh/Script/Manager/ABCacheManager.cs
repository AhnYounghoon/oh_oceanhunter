﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class ABCacheManager
{
    public enum EType
    {
    }

    Dictionary<string, CachedData> _loadedAssetBundleList = new Dictionary<string, CachedData>();
    List<ABCacheData> _requestABCachingDataList = new List<ABCacheData>();
    string _processingUrl = null;
    List<Coroutine> _loadAssets = new List<Coroutine>();

    class CachedData
    {
        public string url { get; private set; }
        public AssetBundle assetBundle { get; private set; }
        List<object> _linkedObjList = new List<object>();

        public CachedData(string inUrl, AssetBundle inAssetBundle)
        {
            url = inUrl;
            assetBundle = inAssetBundle;
        }

        public bool isEmpty
        {
            get
            {
                _linkedObjList = _linkedObjList.Where(x => x != null && !x.Equals(null)).ToList();
                return _linkedObjList.Count == 0;
            }
        }

        public void AddLinkedObj(object inLinkedObj)
        {
            if (!_linkedObjList.Contains(inLinkedObj))
                _linkedObjList.Add(inLinkedObj);
        }

        public void Release(object inLinkedObj)
        {
            _linkedObjList = _linkedObjList.Where(x => !x.Equals(inLinkedObj)).ToList();
        }
    }

    class ABCacheData
    {
        public EType type { get; private set; }
        public string url { get; private set; }
        public uint version { get; private set; }
        public object linkedObj { get; private set; }
        public System.Action<object> callback { get; private set; }
        public System.Type outputType { get; private set; }
        public string outputName { get; private set; }
        public bool isLocalResource { get; private set; }
        public bool sync { get; private set; }

        public ABCacheData(System.Action<object> inCallback, EType inType, string inUrl, uint inVersion, object inLinkedObj, System.Type inOutputType, string inOutputName, bool inSync)
        {
            callback = inCallback;
            version = inVersion;
            type = inType;
            linkedObj = inLinkedObj;
            outputType = inOutputType;
            outputName = inOutputName;
            sync = inSync;

            string tempUrl = inUrl.ToLower();
            isLocalResource = !(tempUrl.Contains("http://") || tempUrl.Contains("https://"));
            url = (isLocalResource) ? StringHelper.Format("{0}/{1}", inUrl, inOutputName) : inUrl;
        }
    }

    public ABCacheManager()
    {
        SingletonController.Get<CoroutineManager>().StartCoroutine(StartGC());
    }

    IEnumerator StartGC()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            GC();
        }
    }

    public void GC()
    {
        var list = _loadedAssetBundleList.Where(x => x.Value.isEmpty).ToList();
        for (int i = 0; i < list.Count; i++)
        {
            _loadedAssetBundleList[list[i].Key].assetBundle.Unload(true);
            _loadedAssetBundleList.Remove(list[i].Key);
        }
    }

	public void Get<T>(System.Action<object> inCallback, EType inType, string inUrl, uint inVersion, object inLinkedObj, string inOutputName, bool inSync = false)
    {
		ABCacheData data = new ABCacheData(inCallback, inType, inUrl, inVersion, inLinkedObj, typeof(T), inOutputName, inSync);
        _requestABCachingDataList.Add(data);
        _requestABCachingDataList = _requestABCachingDataList.OrderBy(x => (int)x.type).ToList();
        CheckLoader();
    }

    public void CheckLoader()
    {
        if (_processingUrl != null)
            return;

        bool isProcessing = false;
		for (int i = 0; i < _requestABCachingDataList.Count; i++)
        {
            ABCacheData data = _requestABCachingDataList[i];
			if (!Caching.IsVersionCached(data.url, (int)data.version))
			{
				continue;
			}

			_requestABCachingDataList.RemoveAt(i);
            _loadAssets.Add(SingletonController.Get<CoroutineManager>().StartCoroutine(LoadAsset(data)));
            isProcessing = true;
            break;
        }

		if (!isProcessing)
		{
			for (int i = 0; i < _requestABCachingDataList.Count;)
			{
				ABCacheData data = _requestABCachingDataList[i];
				_requestABCachingDataList.RemoveAt(i);
				if (data.linkedObj == null || data.linkedObj.Equals(null))
				{
					i++;
					continue;
				}

                _loadAssets.Add(SingletonController.Get<CoroutineManager>().StartCoroutine(LoadAsset(data)));
                break;
			}
		}
    }

    IEnumerator LoadAsset(ABCacheData inData)
    {
        _processingUrl = inData.url;
        AssetBundle assetBundle = null;
        object resultObj = null;

        string resourcePath = inData.isLocalResource ? inData.url : null;
        ResourceRequest resourcesRequest = null;
        if (resourcePath != null)
        {
            if (inData.sync)
            {
                resultObj = Resources.Load(resourcePath, inData.outputType);
            }
            else
            {
                resourcesRequest = Resources.LoadAsync(resourcePath, inData.outputType);
                while (!resourcesRequest.isDone)
                    yield return null;

                if (resourcePath != null && resourcesRequest != null && resourcesRequest.asset != null)
                {
                    resultObj = resourcesRequest.asset;
                }
            }
        }

        if (resultObj == null && !inData.isLocalResource)
        {
            UnityWebRequest request = null;
            if (_loadedAssetBundleList.ContainsKey(inData.url))
            {
                assetBundle = _loadedAssetBundleList[inData.url].assetBundle;
            }
            else
            {
                while (!Caching.ready)
                    yield return null;

                request = UnityWebRequest.GetAssetBundle(inData.url, inData.version);
                yield return request.Send();

                while (!request.isDone)
                    yield return null;

                if (!string.IsNullOrEmpty(request.error))
                {
                    Debug.Log(request.error + " : " + inData.url);
                }
                else
                {
                    assetBundle = DownloadHandlerAssetBundle.GetContent(request);
                }
            }

            if (assetBundle != null)
            {
                AddAssetBundle(inData.url, assetBundle, inData.linkedObj);

                if (inData.linkedObj.Equals(null) || inData.linkedObj == null)
                {
                    resultObj = null;
                }
                else if (inData.outputType == typeof(AssetBundle))
                {
                    resultObj = assetBundle;
                }
                else
                {
                    
                    if (inData.sync)
                    {
                       resultObj = inData.outputType == null ? assetBundle.LoadAsset(inData.outputName) : assetBundle.LoadAsset(inData.outputName, inData.outputType);
                    }
                    else
                    {
                        AssetBundleRequest requestAsync = inData.outputType == null ? assetBundle.LoadAssetAsync(inData.outputName) : assetBundle.LoadAssetAsync(inData.outputName, inData.outputType);
                        yield return requestAsync;
                        resultObj = requestAsync.asset;
                    }
                }
            }

            if (request != null)
                request.Dispose();

            request = null;
        }

        RebindShader(resultObj);

        if (!(inData.linkedObj.Equals(null) || inData.linkedObj == null))
        {
            inData.callback(resultObj);
        }

        _processingUrl = null;
        CheckLoader();
    }

    private void RebindShader(object inResultObj)
    {
        Renderer[] targetRenderers = null;

        if (inResultObj is GameObject)
        {
            GameObject go = inResultObj as GameObject;
            targetRenderers = go.GetComponentsInChildren<Renderer>(true);
        }
        else if (inResultObj is Component)
        {
            var cp = inResultObj as Component;
            targetRenderers = cp.GetComponentsInChildren<Renderer>(true);
        }

        if (targetRenderers == null)
        {
            return;
        }

        // NOTE @sangmoon shader link가 제대로 걸리지 않던 버그 픽스 (AssetBundle)
        foreach (var renderer in targetRenderers)
        {
            Material[] materials = renderer.sharedMaterials;
            for (int i = 0; i < materials.Length; i++)
            {
                if (materials[i] != null)
                {
                    string shaderName = materials[i].shader.name;
                    Shader newShader = Shader.Find(shaderName);

                    if (newShader != null)
                    {
                        materials[i].shader = newShader;
                    }
                }
            }
        }
    }

    public void Unload(string inUrl)
    {
        if (_loadedAssetBundleList.ContainsKey(inUrl) && _processingUrl != inUrl && _loadedAssetBundleList[inUrl].isEmpty)
        {
            _loadedAssetBundleList[inUrl].assetBundle.Unload(true);
            _loadedAssetBundleList.Remove(inUrl);
        }
    }

    public bool isDone { get { return _requestABCachingDataList.Count == 0 && _processingUrl == null; } }

    public void AddAssetBundle(string inUrl, AssetBundle inAssetBundle, object inLinkedObj)
    {
        if (!_loadedAssetBundleList.ContainsKey(inUrl))
            _loadedAssetBundleList.Add(inUrl, new CachedData(inUrl, inAssetBundle));

        _loadedAssetBundleList[inUrl].AddLinkedObj(inLinkedObj);
    }

    public void Release(object inLinkedObj)
    {
        foreach (var value in _loadedAssetBundleList.Values)
        {
            value.Release(inLinkedObj);
        }
    }

    public void Release(string inUrl)
    {
        if (_loadedAssetBundleList.ContainsKey(inUrl))
        {
            _loadedAssetBundleList[inUrl].assetBundle.Unload(true);
            _loadedAssetBundleList.Remove(inUrl);
        }
    }

    public void Release(string inUrl, object inLinkdedObj)
    {
        if (_loadedAssetBundleList.ContainsKey(inUrl))
        {
            _loadedAssetBundleList[inUrl].Release(inLinkdedObj);
        }
    }

    public void Clear(string inIgnore = "")
    {
        _processingUrl = null;
        _requestABCachingDataList.Clear();

        KeyValuePair<string, CachedData> data = new KeyValuePair<string, CachedData>();
        foreach (var value in _loadedAssetBundleList.Values)
        {
            if (value.url.Contains(inIgnore))
            {
                data = _loadedAssetBundleList.Where(x => x.Value == value).ToList().FirstOrDefault();
                continue;
            }

            value.assetBundle.Unload(true);
        }

        _loadedAssetBundleList.Clear();
        if (!string.IsNullOrEmpty(data.Key) && data.Value != null)
        {
            _loadedAssetBundleList.Add(data.Key, data.Value);
        }

        for (int i = 0, count = _loadAssets.Count; i < _loadAssets.Count; i++)
        {
            if (_loadAssets[i] == null || _loadAssets[i].Equals(null))
                continue;

            SingletonController.Get<CoroutineManager>().StopCoroutine(_loadAssets[i]);
        }

        _loadAssets.Clear();
    }
}
