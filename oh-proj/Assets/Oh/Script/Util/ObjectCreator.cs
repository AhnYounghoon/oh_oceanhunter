﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using STZFramework;

public class ObjectCreator : STZBehaviour
{
    static ObjectCreator _instance;
    public static ObjectCreator it
    {
        get
        {
            if (_instance == null)
            {
                GameObject obj = new GameObject("(Singleton)ObjectCreator");
                _instance = obj.AddComponent<ObjectCreator>();
                DontDestroyOnLoad(_instance);
            }

            return _instance;
        }
    }

    public delegate void Callback<T>(T target) where T : Component;

    /// <summary>
    /// 오브젝트 생성 (동기)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parent"></param>
    /// <param name="path"></param>
    /// <param name="pos"></param>
    /// <param name="visible"></param>
    /// <returns></returns>
    public T CreateObj<T>(Transform parent, string path, Vector3 pos = default(Vector3), bool visible = true, bool isLocal = true) where T : Component
    {
        GameObject resourceObj = Resources.Load<GameObject>(path);

        if (resourceObj == null)
        {
            return null;
        }

        return CreateObj<T>(resourceObj, parent, pos, visible, isLocal);
    }

    public T CreateObj<T>(GameObject sourceObj, Transform parent, Vector3 pos = default(Vector3), bool visible = true, bool isLocal = true) where T : Component
    {
        GameObject go = Instantiate(sourceObj);

        if (parent != null)
        {
            go.transform.SetParent(parent);
        }

        if(isLocal == true)
        {
            go.transform.localPosition = pos;
        }
        else
        {
            go.transform.position = pos;
        }

        go.SetActive(visible);

        T component = go.GetComponent<T>();

        if (component != null)
        {
            return component;
        }

        component = go.GetComponentInChildren<T>();

        return component;
    }

    /// <summary>
    /// 오브젝트 생성 (비동기)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="callback"></param>
    /// <param name="parent"></param>
    /// <param name="path"></param>
    /// <param name="pos"></param>
    /// <param name="visible"></param>
    public void CreateObj<T>(Callback<T> callback, Transform parent, string path, Vector3 pos = default(Vector3), bool visible = true) where T : Component
    {
        StartCoroutine(Co_CreateObj(callback, parent, path, pos, visible));
    }

    IEnumerator Co_CreateObj<T>(Callback<T> callback, Transform parent, string path, Vector3 pos = default(Vector3), bool visible = true) where T : Component
    {
        ResourceRequest request = Resources.LoadAsync(path);

        yield return request;

        GameObject go = GameObject.Instantiate(request.asset) as GameObject;

        if (parent != null)
        {
            go.transform.SetParent(parent);
        }

        go.transform.localPosition = pos;
        go.SetActive(visible);

        if (callback != null)
        {
            T component = go.GetComponent<T>();
            if (component == null)
            {
                component = go.GetComponentInChildren<T>();
            }

            if (component != null)
            {
                callback(component);
            }
        }
    }
}
