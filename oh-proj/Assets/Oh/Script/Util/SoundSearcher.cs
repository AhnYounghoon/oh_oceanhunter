﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class SoundSearcher
{
    private const char KOREAN_BEGIN_UNICODE = (char)44032;      // 가
    private const char KOREAN_LAST_UNICODE = (char)55203;       // 힣
    private const char KOREAN_BASE_UNIT = (char)588;            // 각 자음마다 가지는 글자수 

    private static readonly char[] INITIAL_SOUND = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' };

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inChar"></param>
    /// <returns></returns>
    private static bool IsIntialSound(char inChar)
    {
        return INITIAL_SOUND.Contains(inChar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inChar"></param>
    /// <returns></returns>
    private static char GetIntialSound(char inChar)
    {
        int koreanBegin = (inChar - KOREAN_BEGIN_UNICODE);
        int index = koreanBegin / KOREAN_BASE_UNIT;

        return INITIAL_SOUND[index];
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="inChar"></param>
    /// <returns></returns>
    private static bool IsKorean(char inChar)
    {
        return KOREAN_BEGIN_UNICODE <= inChar && inChar <= KOREAN_LAST_UNICODE;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCondition"></param>
    /// <param name="inTarget"></param>
    /// <returns></returns>
    public static bool MatchString(string inCondition, string inTarget)
    {
        int diffLength = inCondition.Length - inTarget.Length;
        int sLength = inTarget.Length;
        if (diffLength < 0)
        {
            return false;
        }

        for (int i = 0, offset = 0; i <= diffLength; ++i)
        {
            offset = 0;

            while (offset < sLength)
            {
                int currentIndex = i + offset;
                if (IsIntialSound(inTarget[offset]) && IsKorean(inCondition[currentIndex]))
                {
                    if (GetIntialSound(inCondition[currentIndex]) == inTarget[offset])
                    {
                        ++offset;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (inCondition[currentIndex] == inTarget[offset])
                    {
                        ++offset;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (offset == sLength)
            {
                return true;
            }
        }

        return false;
    }
}

