﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public static class AnimationMethod
{
    public static void Play(this SkeletonAnimation sk, string animationName, bool isLoop = false)
    {
        sk.loop = isLoop;
        sk.state.ClearTrack(0);
        sk.Skeleton.SetSlotsToSetupPose();
        sk.AnimationName = animationName;
    }

    public static void Play(this SkeletonGraphic sk, string animationName, bool isLoop = false)
    {
        sk.startingAnimation = animationName;
        sk.startingLoop = isLoop;
        sk.AnimationState.ClearTrack(0);
        sk.Initialize(true);
    }

    public static void Stop(this SkeletonAnimation sk)
    {
        sk.timeScale = 0;
    }

    public static void Resume(this SkeletonAnimation sk)
    {
        sk.timeScale = 1;
    }

    public static void Stop(this SkeletonGraphic sk)
    {
        sk.Clear();
        sk.AnimationState.ClearTrack(0);
        sk.Initialize(true);
    }

    public static void SetSkeletonDataAsset(this SkeletonAnimation sk, string objectName)
    {
        SkeletonDataAsset dataAsset = MonoBehaviour.Instantiate(Resources.Load(objectName)) as SkeletonDataAsset;
        sk.skeletonDataAsset = dataAsset;
        sk.initialSkinName = "default";
        sk.Initialize(true);
        sk.skeleton.Update(0.001f);
        sk.skeleton.UpdateWorldTransform();
        sk.LateUpdate();
        sk.AnimationState.ClearTrack(0);
    }

    public static void SetSkeletonDataAsset(this SkeletonGraphic sk, string objectName)
    {
        SkeletonDataAsset dataAsset = MonoBehaviour.Instantiate(Resources.Load(objectName)) as SkeletonDataAsset;
        sk.skeletonDataAsset = dataAsset;
        sk.initialSkinName = "default";
        sk.Initialize(true);
        sk.LateUpdate();
        sk.AnimationState.ClearTrack(0);
    }
}
