﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public static class UnityExtentionMethods
{
    public static void SetPositionX(this Transform transform, float x, bool isLocal = true)
    {
        Vector3 position = isLocal ? transform.localPosition : transform.position;
        position.x = x;

        transform.SetPosition(position, isLocal);
    }

    public static void SetPositionY(this Transform transform, float y, bool isLocal = true)
    {
        Vector3 position = isLocal ? transform.localPosition : transform.position;
        position.y = y;

        transform.SetPosition(position, isLocal);
    }

    public static void SetPositionZ(this Transform transform, float z, bool isLocal = true)
    {
        Vector3 position = isLocal ? transform.localPosition : transform.position;
        position.z = z;

        transform.SetPosition(position, isLocal);
    }

    public static void SetPosition(this Transform transform, Vector3 position, bool isLocal = true)
    {
        if (isLocal)
        {
            transform.localPosition = position;
        }
        else
        {
            transform.position = position;
        }
    }

    public static void SetAPositionX(this RectTransform transform, float x)
    {
        Vector2 position = transform.anchoredPosition;
        position.x = x;

        transform.SetAPosition(position);
    }

    public static void SetAPositionY(this RectTransform transform, float y)
    {
        Vector2 position = transform.anchoredPosition;
        position.y = y;

        transform.SetAPosition(position);
    }

    public static void SetAPosition(this RectTransform transform, Vector2 position)
    {
        transform.anchoredPosition = position;
    }

    public static Vector4 GetPosition4(this Transform transform, bool isLocal = true)
    {
        Vector4 position4 = isLocal ? transform.localPosition : transform.position;
        position4.w = 1.0f;

        return position4;
    }

    public static void SetScale(this Transform transform, float value)
    {
        transform.localScale = new Vector3(value, value, value);
    }

    public static void SetScale(this Transform transform, float x, float y, float z)
    {
        transform.localScale = new Vector3(x, y, z);
    }

    public static void SetScaleX(this Transform transform, float value)
    {
        transform.localScale = new Vector3(value, transform.localScale.y, transform.localScale.z);
    }

    public static void SetScaleZ(this Transform transform, float value)
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, value);
    }

    public static Rect GetPreferredRect(this Transform transform)
    {
        Rect preferredRect = Rect.zero;
        bool isFirst = true;

        foreach (Transform childTransform in transform)
        {
            RectTransform rectTransform = childTransform.GetComponent<RectTransform>();

            if (rectTransform != null && rectTransform.gameObject.activeSelf)
            {
                Rect rectToAdd = rectTransform.rect;
                Vector2 rectPosition = rectToAdd.position;

                rectPosition.x += rectTransform.localPosition.x;
                rectPosition.y += rectTransform.localPosition.y;

                rectToAdd.position = rectPosition;

                if (isFirst)
                {
                    isFirst = false;
                    preferredRect = rectToAdd;
                }
                else
                {
                    preferredRect = preferredRect.AddRect(rectToAdd);
                }
            }
        }

        return preferredRect;
    }

    public static void DestroyChildren(this Transform transform, List<Transform> exceptionChildren = null)
    {
        foreach (Transform child in transform)
        {
            if (exceptionChildren == null || !exceptionChildren.Contains(child))
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    public static Rect AddRect(this Rect rect, Rect rectToAdd)
    {
        rect.xMin = (rect.xMin < rectToAdd.xMin) ? rect.xMin : rectToAdd.xMin;
        rect.yMin = (rect.yMin < rectToAdd.yMin) ? rect.yMin : rectToAdd.yMin;
        rect.xMax = (rect.xMax > rectToAdd.xMax) ? rect.xMax : rectToAdd.xMax;
        rect.yMax = (rect.yMax > rectToAdd.yMax) ? rect.yMax : rectToAdd.yMax;

        return rect;
    }

    public static void SetWidth(this RectTransform rectTrans, float width)
    {
        rectTrans.SetSize(width, rectTrans.sizeDelta.y);
    }

    public static void SetHeight(this RectTransform rectTrans, float height)
    {
        rectTrans.SetSize(rectTrans.sizeDelta.x, height);
    }

    public static void SetSize(this RectTransform rectTrans, float width, float height)
    {
        rectTrans.SetSize(new Vector2(width, height));
    }

    public static void SetSize(this RectTransform rectTrans, Vector2 sizeDelta)
    {
        rectTrans.sizeDelta = sizeDelta;
    }

    public static void SetAlpha(this Image image, float alpha)
    {
        Color color = image.color;
        color.a = alpha;
        image.color = color;
    }

    public static void SetRenderTexture(this RawImage rawImage, Camera renderCamera, int width = 0, int height = 0)
    {
        int imgWidth = width == 0 ? Screen.width : width;
        int imgHeight = height == 0 ? Screen.height : height;

        RenderTexture renderTexture = new RenderTexture(imgWidth, imgHeight, 24);
        renderTexture.Create();

        renderCamera.targetTexture = renderTexture;
        renderCamera.Render();
        renderCamera.targetTexture = null;

        if (rawImage.texture == null)
        {
            rawImage.texture = new Texture2D(imgWidth, imgHeight, TextureFormat.RGB24, true);
        }

        RenderTexture.active = renderTexture;

        Texture2D rawImageTexture = rawImage.texture as Texture2D;
        rawImageTexture.ReadPixels(new Rect(0, 0, imgWidth, imgHeight), 0, 0);
        rawImageTexture.Apply();

        RenderTexture.active = null;

        GameObject.Destroy(renderTexture);
    }
}
