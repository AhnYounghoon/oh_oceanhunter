using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using STZFramework;

public static class Coordinate
{
	/// <summary>
    /// 특정 좌표의 그리드 인덱스 조회
    /// </summary>
    /// <param name="isX"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int GetPositionInGridIndex(bool isX, int value)
	{
		if (isX)
		{
			return (int)((value - BattleOceanController.START_POS_X + BattleOceanController.TILE_HALF_WIDTH) / BattleOceanController.TILE_WIDTH);
		}
		else
		{
            return (int)((value - BattleOceanController.START_POS_Y + BattleOceanController.TILE_HALF_WIDTH) / BattleOceanController.TILE_HEIGHT);
        }
    }

	public static int ToIdxNumber(int x, int y)
	{
		return (int)((x + BattleOceanController.SCROLL_X) + (y + BattleOceanController.SCROLL_Y) * BattleOceanController.HORIZONTAL);
	}

	public static int ToIdxNumber(float x, float y)
	{
		return (int)(x + y * BattleOceanController.HORIZONTAL);
	}

	/// <summary>
    /// 특정 인덱스의 게임 좌표 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <returns></returns>
    public static Vector2 ToPosition(int inIndex)
	{
		if (inIndex < 0 || inIndex >= BattleOceanController.TILE_TOTAL)
			return new Vector2(-1, -1);

		Vector2 retPos;

        retPos.x = BattleOceanController.START_POS_X + (int)(inIndex % BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_WIDTH;
        retPos.y = BattleOceanController.START_POS_Y + (int)(inIndex / BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_HEIGHT;

        return retPos;
	}

	/// <summary>
    /// 게임영역(18x10)인지 체크
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public static bool VaildationCoordinate(Vector2 pos)
	{
		if (pos.x <= -(BattleOceanController.BASE_GAME_WIDTH / 2) + BattleOceanController.TILE_RANGE_X || 
            pos.x > BattleOceanController.BASE_GAME_WIDTH / 2 - BattleOceanController.TILE_RANGE_X)
			return false;

        if (pos.y <= -(BattleOceanController.BASE_GAME_HEIGHT / 2) + BattleOceanController.TILE_RANGE_Y || 
            pos.y > BattleOceanController.BASE_GAME_HEIGHT / 2 - BattleOceanController.TILE_RANGE_Y)
            return false;

		return true;
	}
}

