﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UniRx.Triggers;
using UnityEngine;
using UniRx;

public class DragObject : MonoBehaviour
{
    enum EDragType
    {
        SpriteRenderer,
        Image,
    }

    [SerializeField]
    GameObject dragObject;

    [SerializeField]
    EDragType type;

    private System.Action<Vector3, Hashtable> touchUpCallback = null;
    private System.IDisposable touchUpChecker   = null;
    private System.IDisposable dragUpdater      = null;

    public System.Action<Vector3, Hashtable> TouchUpCallback
    {
        set
        {
            if (value != null)
            {
                touchUpCallback = value;
            }
        }
    }

    private void Clear()
    {
        if (touchUpChecker != null)
        {
            touchUpChecker.Dispose();
            touchUpChecker = null;
        }
        if (dragUpdater != null)
        {
            dragUpdater.Dispose();
            dragUpdater = null;
        }
    }

    private void OnDestroy()
    {
        Clear();
    }

    public void StartDrag(Sprite inSprite, Hashtable inData)
    {
        Clear();
        if (type == EDragType.Image)
        {
            dragObject.GetComponent<Image>().sprite = inSprite;
        }
        else if (type == EDragType.SpriteRenderer)
        {
            dragObject.GetComponent<SpriteRenderer>().sprite = inSprite;
        }
        dragObject.gameObject.SetActive(true);

        // 터치 위치의 변화가 있을 때만 콜백을 실행
        dragUpdater = this.ObserveEveryValueChanged(x => Input.mousePosition)
            .DistinctUntilChanged()
            .Subscribe(OnDrag);

        // 손을 땟을때 해당 업데이트 해제
        touchUpChecker = this.UpdateAsObservable()
            .Select(_ => Input.GetMouseButtonUp(0))
            .DistinctUntilChanged()
            .Where(x => x)
            .Subscribe(_ => 
            {
                StopDrag();

                // 마지막으로 위치했던 터치 위치를 파라미터로 콜백 반환
                if (touchUpCallback != null)
                {
                    touchUpCallback(dragObject.transform.position, inData);
                }
            });
    }

    public void StopDrag()
    {
        touchUpChecker.Dispose();
        touchUpChecker = null;

        dragUpdater.Dispose();
        dragUpdater = null;

        dragObject.gameObject.SetActive(false);
    }

    private void OnDrag(Vector3 inPosition)
    {
        if (dragObject != null && dragObject.activeSelf)
        {
            Vector2 screenPoint = Camera.main.ScreenToWorldPoint(inPosition);
            dragObject.transform.position = screenPoint;
        }
    }
}
