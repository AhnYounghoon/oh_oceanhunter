﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider2D))]
public class DropSlot : MonoBehaviour
{
    [SerializeField]
    Image _image;

    [SerializeField]
    Sprite _defaultSprite;

    bool _setImage = false;
    Collider2D _coll = null;

    public bool SetImage
    {
        get
        {
            return _setImage;
        }
    }

    private void Start()
    {
        _coll = this.GetComponent<Collider2D>();
    }

    public void ChangeImage(string inPath)
    {
        _setImage = true;
        _image.sprite = Resources.Load<Sprite>(inPath);
        _image.color = STZCommon.ToUnityColor(255, 255, 255);
    }

    public void SetDefaultImage()
    {
        _setImage = false;
        _image.sprite = _defaultSprite;
        _image.color = STZCommon.ToUnityColor(17, 193, 55, 123);
    }

    public bool ContainPoint(Vector3 inPosition)
    {
        if (_coll == null)
            return false;

        if (_coll.bounds.Contains(inPosition))
        {
            return true;
        }
        return false;
    }
}
