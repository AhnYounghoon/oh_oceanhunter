﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShipUI : MonoBehaviour
{
    public enum EShowType
    {
        None,
        Weapon,
        Character,
        ShowAll,
    }

    [SerializeField]
    Image _imgShip;

    [Header("생략할 수 있는 프로퍼티")]
    [Tooltip("선박 이름")]
    [SerializeField]
    Text _txtName;

    [Tooltip("선박 레벨")]
    [SerializeField]
    Text _txtLevel;

    [Tooltip("선박 설명")]
    [SerializeField]
    Text _txtMessage;

    [Header("드래그 드롭 (무기, 캐릭터 배치)")]
    [SerializeField]
    bool _useDragDrop;

    [SerializeField]
    DragObject _dragController;

    List<DropSlot> _dropWeaponSlot    = new List<DropSlot>();
    List<DropSlot> _dropCharacterSlot = new List<DropSlot>();

    ServerStatic.BattleObject _data;
    EShowType _dropMode;

    private void Awake()
    {
        if (_useDragDrop)
        {
            _dragController.TouchUpCallback = DragDropCallback;
        }
    }

    public void Refresh(ServerStatic.BattleObject inData)
    {
        if (inData == null)
        {
            Debug.LogError("ShipUI Ship Data is null!!");
            return;
        }

        _dropMode = EShowType.None;
        _data = inData;
        _imgShip.sprite = Resources.Load<Sprite>("Texture/BattleObject/" + inData.id);

        if (_txtName != null)
        {
            _txtName.text = "No.1 <color=#00F65DFF>Black Per</color>";
        }
        if (_txtMessage != null)
        {
            _txtMessage.text = "\"나름 튼튼한 배!\"";
        }
        if (_txtLevel != null)
        {
            _txtLevel.text = "Level 1";
        }

        RemoveDropArea();
        CreateDropArea();
    }

    public void SetDropMode(EShowType inDropMode)
    {
        if (!_useDragDrop)
            return;

        if (_dropMode == inDropMode)
            return;

        _dropMode = inDropMode;
        SetActiveCharacterArea(false);
        SetActiveWeaponArea(false);

        if (_dropMode == EShowType.Character)
        {
            SetActiveCharacterArea(true);
        }
        else if (_dropMode == EShowType.Weapon)
        {
            SetActiveWeaponArea(true);
        }
    }

    /// 드래그 드롭했을 때
    /// </summary>
    /// <param name="inPosition">드롭한 위치</param>
    /// <param name="inHashTable">드래그 관련 데이터</param>
    public void DragDropCallback(Vector3 inPosition, Hashtable inHashTable)
    {
        if (!_useDragDrop)
            return;

        if (!inHashTable.ContainsKey("weapon_data") && !inHashTable.ContainsKey("character_data"))
        {
            return;
        }

        //ServerStatic.Weapon weaponData       = (ServerStatic.Weapon)inHashTable["weapon_data"];
        ServerStatic.Character characterData = (ServerStatic.Character)inHashTable["character_data"];
        //if (weaponData == null && characterData == null)
        //{
        //    Debug.LogError("HashTable[data] is null!!");
        //    return;
        //}

        if (_dropMode == EShowType.Weapon)
        {
            for (int i = 0; i < _dropWeaponSlot.Count; i++)
            {
                if (_dropWeaponSlot[i].ContainPoint(inPosition))
                {
                    //LocateWeapon(i, weaponData);
                    break;
                }
            }
        }
        else if (_dropMode == EShowType.Character)
        {
            for (int i = 0; i < _dropCharacterSlot.Count; i++)
            {
                if (_dropCharacterSlot[i].ContainPoint(inPosition))
                {
                    LocateCharacter(i, characterData);
                    break;
                }
            }
        }
    }

    public void SetActiveWeaponArea(bool value)
    {
        for (int i = 0; i < _dropWeaponSlot.Count; i++)
        {
            _dropWeaponSlot[i].gameObject.SetActive(value);
        }
    }

    public void SetActiveCharacterArea(bool value)
    {
        for (int i = 0; i < _dropCharacterSlot.Count; i++)
        {
            _dropCharacterSlot[i].gameObject.SetActive(value);
        }
    }

    private void RemoveDropArea()
    {
        if (!_useDragDrop)
            return;

        foreach (var slot in _dropWeaponSlot)
        {
            Destroy(slot.gameObject);
        }
        foreach (var slot in _dropCharacterSlot)
        {
            Destroy(slot.gameObject);
        }
        _dropWeaponSlot.Clear();
        _dropCharacterSlot.Clear();
    }

    private void CreateDropArea()
    {
        GameObject go = Resources.Load<GameObject>("Prefabs/DropSlot");
        //foreach (var position in _data.GetParseData().weaponSlotList)
        //{
        //    GameObject area = Instantiate(go);
        //    area.transform.SetParent(_imgShip.transform);
        //    area.transform.localPosition = position;
        //    area.SetActive(false);
        //    _dropWeaponSlot.Add(area.GetComponent<DropSlot>());
        //}

        //foreach (var position in _data.GetParseData().characterSlotList)
        //{
        //    GameObject area = Instantiate(go);
        //    area.transform.SetParent(_imgShip.transform);
        //    area.transform.localPosition = position;
        //    area.SetActive(false);
        //    _dropCharacterSlot.Add(area.GetComponent<DropSlot>());
        //}
    }

    //public void LocateWeapon(int idx, ServerStatic.Weapon inData)
    //{
    //    // 이미 장착된 무기를 다른 슬롯에 대체할 때 기존 무기 리셋 후 변경
    //    var weaponList = GeneralDataManager.it.weaponList;
    //    for (int i = 0; i < weaponList.Count; i++)
    //    {
    //        ServerStatic.Weapon data = weaponList[i];
    //        if (data != null && data.id == inData.id)
    //        {
    //            weaponList[i] = null;
    //            _dropWeaponSlot[i].SetDefaultImage();
    //            break;
    //        }
    //    }

    //    weaponList[idx] = inData;
    //    _dropWeaponSlot[idx].ChangeImage(inData.resourcePath);
    //}

    public void LocateCharacter(int idx, ServerStatic.Character inData)
    {
        // 이미 장착된 무기를 다른 슬롯에 대체할 때 기존 무기 리셋 후 변경
        var characterList = GeneralDataManager.it.characterList;
        for (int i = 0; i < characterList.Count; i++)
        {
            ServerStatic.Character data = characterList[i];
            if (data != null && data.id == inData.id)
            {
                characterList[i] = null;
                _dropCharacterSlot[i].SetDefaultImage();
                break;
            }
        }

        characterList[idx] = inData;
        //_dropCharacterSlot[idx].ChangeImage(inData.resource_path);
    }
}
