﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using STZFramework;

public class LobbyUI : MonoBehaviour, IHashReceiver
{
    [SerializeField]
    GameObject _characterUI;

    [SerializeField]
    GameObject _rightUI;

    [SerializeField]
    QuestUI _questUI;

    [SerializeField]
    GameObject _bottomUI;

    [SerializeField]
    Button[] clearButton;

    [SerializeField]
    Button[] questSpot;

    void Awake()
    {
        ObserverManager.it.AddEventListener(this, EObserverEventKey.QUEST_UPDATE);

#if !UNITY_EDITOR
        for(int i = 0; i < clearButton.Length; i++)
        {
            clearButton[i].gameObject.SetActive(false);
        }
#endif
    }

    void OnDisable()
    {
        ObserverManager.it.RemoveEventListener(this, EObserverEventKey.QUEST_UPDATE);
    } 

    public void SetLobbyUI()
    {
        _questUI.SetQuest();
    }

    public void SetQuestSpot()
    {
        for (int i = 0; i < questSpot.Length; i++)
        {
            questSpot[i].gameObject.Get<Animator>().Play("Stop");
            questSpot[i].gameObject.Get<Image>("Arrow").gameObject.SetActive(false);
        }

        FileMap questFM = GeneralDataManager.it.GetQuestData();

        if (questFM == null)
            return;

        int questId = 0;
        int totalQuestCount = questFM.Get<int>(GeneralDataManager.EParam.COUNT.ToString());
        for (int i = 1; i <= totalQuestCount; i++)
        {
            // 미완료된 퀘스트만 추출
            if (questFM.Get<int>(i.ToString()) < 0)
            {
                questId = i;
                break;
            }
        }

        if (questId != 0)
        {
            questSpot[questId - 1].gameObject.Get<Animator>().Play("Play");
            questSpot[questId - 1].gameObject.Get<Image>("Arrow").gameObject.SetActive(true);
        }
    }

    public void ReceiveEvent(EObserverEventKey inEventKey, Hashtable inParam)
    {
        if (inEventKey == EObserverEventKey.QUEST_UPDATE)
        {
            _questUI.SetQuest();
        }
    }

    private void HideUIAnimation()
    {
        _characterUI.transform.DOMoveX(-1000, 0.5f).SetEase(Ease.OutSine);
        //_rightUI.transform.DOMoveX(470, 0.5f).SetDelay(0.1f).SetEase(Ease.OutSine);
        _bottomUI.transform.DOMoveY(-520, 0.5f).SetDelay(0.15f).SetEase(Ease.OutSine);
    }

    private void ShowUIAnimation()
    {
        _characterUI.transform.DOMoveX(-278, 0.5f).SetEase(Ease.OutSine);
        //_rightUI.transform.DOMoveX(0, 0.5f).SetDelay(0.1f).SetEase(Ease.OutSine);
        _bottomUI.transform.DOMoveY(-322, 0.5f).SetDelay(0.15f).SetEase(Ease.OutSine);
    }

    public void ShowPopup(BasePopUp inType)
    {
        if (inType == null)
        {
            Debug.LogError("Show Popup Parameter inType is null !!");
        }

        //HideUIAnimation();
        SingletonController.Get<PopupManager>().Show(inType.GetType(), param =>
        {
            //ShowUIAnimation();
        });
    }

    ///////////////////////// TEST /////////////////////
    public void StartGame()
    {        
        PopupStageSelection.Show((param) =>
        {
        });
    }

    public void OnQuest(bool inIsExpand)
    {
        if (inIsExpand)
        {
            _questUI.transform.DOMoveX(375, 0.5f).SetDelay(0.1f).SetEase(Ease.OutSine);
        }
        else
            _questUI.transform.DOMoveX(900, 0.5f).SetDelay(0.1f).SetEase(Ease.OutSine);
    }

    public Button GetQuestSpot(int inIndex)
    {
        return questSpot[inIndex - 1];
    }

    public void OnSelectQuestSpot(int inIndex)
    {
        FileMap questFM = GeneralDataManager.it.GetQuestData();
        if (questFM == null)
            return;

        int currCount = questFM.Get<int>(inIndex.ToString());
        if (currCount == -1)
        {
#if UNITY_EDITOR
            if (inIndex == 1)
                TutorialManager.it.SetTutorial(StringHelper.Format("Data/quest_{0}_progress_test", inIndex), this.Get<Canvas>().transform);
            else
                TutorialManager.it.SetTutorial(StringHelper.Format("Data/quest_{0}_progress", inIndex), this.Get<Canvas>().transform);
#else
            TutorialManager.it.SetTutorial(StringHelper.Format("Data/quest_{0}_progress", inIndex), this.Get<Canvas>().transform);
#endif
            TutorialManager.it.StartTutorial(() =>
            {
                TutorialManager.it.Clear();

                // 획득 -> 진행으로 변경
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.QUEST, inIndex.ToString());
                questSpot[inIndex - 1].gameObject.Get<Animator>().Play("Stop");
            });
        }
    }

    public void ResetData()
    {
        SimplePopup.Confirm("데이터를 초기화 하시겠습니까?", () => { GeneralDataManager.it.RemoveData(); }, null, null, "예", "아니요");        
    }

    public void ClearStage(int inIndex)
    {
        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.STAGE_CLEAR, null, inIndex);
        GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.QUEST, inIndex.ToString());
    }
}
