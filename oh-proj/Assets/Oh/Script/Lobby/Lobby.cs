﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviour
{
    [SerializeField]
    LobbyUI _lobbyUI;

    [SerializeField]
    Canvas _uiCanvas;

    private void Awake()
    {
        GeneralDataManager.it.LoadData();
        MainWealthUI.it.Show();

        if (GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.None)
            SingletonController.Get<SoundManager>().Load("JSON/SoundData");

        SingletonController.Get<SoundManager>().StopGroup("bgm");
        SingletonController.Get<SoundManager>().Play("BGM_lobby", "bgm");

#if UNITY_EDITOR
        TutorialManager.it.SetTutorial("Data/quest_1_start_test", _uiCanvas.transform);
#else
        TutorialManager.it.SetTutorial("Data/quest_1_start", _uiCanvas.transform);
#endif

        Statics.Instance.Parse(Statics.LoadFromLocal());
        StageDataManager.it.LoadPackagedStageFile(string.Empty);
    }

    public void Start()
    {
        if (!GeneralDataManager.it.CheckFlowData(GeneralDataManager.EParam.FLOW, 2))
        {
            TutorialManager.it.StartTutorial(() =>
            {
                TutorialManager.it.Clear();
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.FLOW, string.Empty, 2);
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.QUEST, "1", -1);
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.GOLD.ToString(), 1000);
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.SILVER.ToString(), 3000);
#if UNITY_EDITOR
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.GOODS, ERewardType.SILVER_BOX.ToString(), 10);
#endif

                // 선장3명
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, GeneralDataManager.EParam.HERO.ToString(), 1, 14, null, new CharacterLevelInfo(1, 0));
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, GeneralDataManager.EParam.HERO.ToString(), 1, 15, null, new CharacterLevelInfo(1, 0));
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.PERSON, GeneralDataManager.EParam.HERO.ToString(), 1, 16, null, new CharacterLevelInfo(1, 0));

                // 1함대에 3-pair 배치
                GeneralDataManager.it.UpdateData(GeneralDataManager.EParam.FLEET, string.Empty, 1, 1, "14:0,15:0,16:0");

                Button targetSpot = _lobbyUI.GetQuestSpot(1);
                if (targetSpot != null)
                {
                    targetSpot.gameObject.Get<Animator>().Play("Play");
                    targetSpot.gameObject.Get<Image>("Arrow").gameObject.SetActive(true);
                }
            });
        }
        else
        {
            _lobbyUI.SetLobbyUI();
            _lobbyUI.SetQuestSpot();
            _lobbyUI.Get<GachaNewIcon>("BottomUI/btnCharacter").CheckNewIcon();

            //GeneralDataManager.it.lastClearQuestID = 1;
            // 퀘스트 종료 문구
            if (GeneralDataManager.it.lastClearQuestID > 0)
            {
                TutorialManager.it.SetTutorial(StringHelper.Format("Data/quest_{0}_end", GeneralDataManager.it.lastClearQuestID), _uiCanvas.transform);
                TutorialManager.it.StartTutorial(() =>
                {
                    ServerStatic.Quest quest = ServerStatic.Quest.GetQuestById(GeneralDataManager.it.lastClearQuestID);
                    // 보상 지급
                    PopupQuestReward.Show((param) =>
                    {
                        PopupMyCharacter.Show((gachaParam) =>
                        {
                            _lobbyUI.SetQuestSpot();
                            _lobbyUI.Get<GachaNewIcon>("BottomUI/btnCharacter").CheckNewIcon();

                            if (GeneralDataManager.it.lastClearQuestID == 4)
                            {
                                TutorialManager.it.Clear();
                                GeneralDataManager.it.lastClearQuestID = 0;
                                return;
                            }

                            // 퀘스트 시작 문구
                            TutorialManager.it.SetTutorial(StringHelper.Format("Data/quest_{0}_start", GeneralDataManager.it.lastClearQuestID + 1), _uiCanvas.transform);
                            TutorialManager.it.StartTutorial(() =>
                            {
                                TutorialManager.it.Clear();
                                GeneralDataManager.it.lastClearQuestID = 0;
                            });
                        });
                    }, quest);
                });
            }
        }
    }
}
