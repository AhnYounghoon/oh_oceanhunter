﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelSeaRoute : STZBehaviour
{
    public TabDefault parent;

    /// <summary>
    /// 항로 데이터 관련
    /// </summary>    
    [SerializeField]
    public InputField SeaRouteValueInput;
    [SerializeField]
    public InputField SeaRouteOpenTypeInput;
    [SerializeField]
    public InputField SeaRouteOpenValueInput;
    [SerializeField]
    public Dropdown SeaRouteDropdown;
    [SerializeField]
    public Button SeaRouteAddButton;
    [SerializeField]
    public Button SeaRouteRemoveButton;

    private List<int> _seaRouteValue = null;
    private int _seaRouteOpenType = 0;
    private List<int> _seaRouteOpenValue = null;
    private Dictionary<int, SeaRoute> _seaRoutes = null;
    private int seaRoute_list_index = 1;

    public void Clear()
    {
        while (SeaRouteDropdown.options.Count > 1)
            SeaRouteDropdown.options.RemoveAt(SeaRouteDropdown.options.Count - 1);

        _seaRouteValue = null;
        _seaRouteOpenType = 0;
        _seaRouteOpenValue = null;

        SeaRouteValueInput.text = string.Empty;
        SeaRouteOpenTypeInput.text = string.Empty;
        SeaRouteOpenValueInput.text = string.Empty;

        _seaRoutes = null;
        seaRoute_list_index = 1;
    }

    public void Refresh()
    {
        Clear();

        LocalStageData targetStageData = StageDataManager.it.GetStage();

        List<SeaRoute> sr = targetStageData.seaRouteList;
        if (sr != null)
        {
            _seaRoutes = new Dictionary<int, SeaRoute>();

            foreach (SeaRoute o in sr)
            {
                _seaRoutes.Add(seaRoute_list_index, o);
                //parent.mapToolMain.Log(string.Format("맵 스크롤 리스트 ({0}) 추가됨", scroll_map_list_index));
                seaRoute_list_index++;

                List<Dropdown.OptionData> listItem = SeaRouteDropdown.options;
                Dropdown.OptionData newItem = new Dropdown.OptionData();

                newItem.text = (seaRoute_list_index).ToString();
                listItem.Add(newItem);
            }

            SelectSeaRouteListItem();
        }       
    }

    public void OnInputField(int inParam)
    {
        switch (inParam)
        {
            case 0:
                if (SeaRouteValueInput.text != "")
                {
                    _seaRouteValue = new List<int>();
                    string[] indexs = SeaRouteValueInput.text.Split(',');
                    for (int i = 0; i < indexs.Length; i++)
                    {
                        if (indexs[i] != null)
                        {
                            _seaRouteValue.Add(int.Parse(indexs[i]));
                        }
                    }
                }
                break;

            case 1:
                if (SeaRouteOpenTypeInput.text != "")
                    _seaRouteOpenType = int.Parse(SeaRouteOpenTypeInput.text);
                else
                    _seaRouteOpenType = 0;
                break;

            case 2:
                if (SeaRouteOpenValueInput.text != "")
                {
                    _seaRouteOpenValue = new List<int>();
                    string[] indexs = SeaRouteOpenValueInput.text.Split(',');
                    for (int i = 0; i < indexs.Length; i++)
                    {
                        if (indexs[i] != null)
                        {
                            _seaRouteOpenValue.Add(int.Parse(indexs[i]));
                        }
                    }
                }
                break;
        }
    }

    public void AddSeaRoute()
    {
        OnInputField(0);
        OnInputField(1);
        OnInputField(2);

        if (_seaRoutes == null)
            _seaRoutes = new Dictionary<int, SeaRoute>();

        seaRoute_list_index = int.Parse(SeaRouteDropdown.captionText.text);

        if (_seaRouteValue.Count < 2)
        {
            parent.mtm.Log("항로는 최소 2개 이상이어야 합니다.", LogLevel.WARNING);
            return;
        }

        SeaRoute newOne = new SeaRoute();
        if (_seaRouteValue != null)
            newOne.sea_route.AddRange(_seaRouteValue);
        newOne.index = seaRoute_list_index;
        newOne.open_type = _seaRouteOpenType;
        if (_seaRouteOpenValue != null)
            newOne.open_value.AddRange(_seaRouteOpenValue);

        // 갱신
        if (_seaRoutes.ContainsKey(seaRoute_list_index))
        {
            _seaRoutes.Remove(seaRoute_list_index);
            _seaRoutes.Add(seaRoute_list_index, newOne);

            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 수정됨", seaRoute_list_index));
        }
        else
        {
            _seaRoutes.Add(seaRoute_list_index, newOne);
            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 추가됨", seaRoute_list_index));
            seaRoute_list_index++;

            List<Dropdown.OptionData> listItem = SeaRouteDropdown.options;
            Dropdown.OptionData newItem = new Dropdown.OptionData();

            newItem.text = (seaRoute_list_index).ToString();
            listItem.Add(newItem);
        }
    }

    public void RemoveSeaRoute()
    {
        if (_seaRoutes == null)
            return;

        seaRoute_list_index = int.Parse(SeaRouteDropdown.captionText.text);

        // 삭제
        if (_seaRoutes.ContainsKey(seaRoute_list_index))
        {
            _seaRoutes.Remove(seaRoute_list_index);
            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 삭제됨", seaRoute_list_index));

            List<Dropdown.OptionData> listItem = SeaRouteDropdown.options;
            int changeKey = 0;
            for (int i = 0; i < listItem.Count; i++)
            {
                if (int.Parse(listItem[i].text) == seaRoute_list_index)
                {
                    listItem.Remove(listItem[i]);
                }
                changeKey = i + 1;
                listItem[i].text = changeKey.ToString();

                // Dicionary 내부 키 정렬
                if (changeKey >= seaRoute_list_index)
                {
                    if (_seaRoutes.ContainsKey(changeKey))
                    {
                        SeaRoute value = _seaRoutes[changeKey];
                        _seaRoutes.Remove(changeKey);
                        _seaRoutes.Add(i, value);
                    }
                }
            }

            SelectSeaRouteListItem();
            seaRoute_list_index--;
        }
    }

    public void SelectSeaRouteListItem()
    {
        if (_seaRoutes == null)
            return;

        SeaRouteValueInput.text = string.Empty;

        seaRoute_list_index = int.Parse(SeaRouteDropdown.captionText.text);

        if (_seaRoutes.ContainsKey(seaRoute_list_index))
        {
            SeaRoute targetScrollStage = _seaRoutes[seaRoute_list_index];
            string list = string.Empty;

            list = string.Empty;
            foreach (int index in targetScrollStage.sea_route)
            {
                if (list.Length == 0)
                    list = index.ToString();
                else
                    list += string.Format(",{0}", index);
            }
            SeaRouteValueInput.text = list;
            OnInputField(0);

            if (targetScrollStage.open_type > 0)
                OnInputField(1);

            if (targetScrollStage.open_value != null && targetScrollStage.open_value.Count > 0)
            {
                list = string.Empty;
                foreach (int index in targetScrollStage.open_value)
                {
                    if (list.Length == 0)
                        list = index.ToString();
                    else
                        list += string.Format(",{0}", index);
                }
                SeaRouteOpenValueInput.text = list;
                OnInputField(2);
            }
        }
    }

    public Dictionary<int, SeaRoute> GetParseSeaRouteData()
    {
        return _seaRoutes;
    }    
}