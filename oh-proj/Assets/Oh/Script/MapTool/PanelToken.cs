﻿using UnityEngine.UI;
using System.Collections.Generic;

public class PanelToken : STZBehaviour
{
    public TabToken parent;

    /// <summary>
    /// 토큰 데이터 관련
    /// </summary>
    public InputField tokenId;
    public InputField tokenAppearRating;
    public InputField tokenAppearCondition;
    public InputField tokenAppearPosition;
    public InputField tokenWaveInfo;
    public InputField tokenMovingRouteValueInput;

    public Dropdown tokenMovingRouteDropdown;
    public Button tokenMovingRouteAddButton;
    public Button tokenMovingRouteRemoveButton;

    private int _tokenId = 0;
    private int _tokenAppearRating = 100;
    private int _tokenAppearCondition = 0;
    private int _tokenAppearPosition = -1;
    private string _tokenWaveInfo = string.Empty;
    private List<int> _movingRoutes = null;
    //private Dictionary<int, SeaRoute> _movingRoutes = null;
    private int _movingRoute_list_index = 1;

    public void Clear()
    {
        while (tokenMovingRouteDropdown.options.Count > 1)
            tokenMovingRouteDropdown.options.RemoveAt(tokenMovingRouteDropdown.options.Count - 1);

        _movingRoutes = null;
        _movingRoute_list_index = 1;

        _tokenId = 0;
        _tokenAppearRating = 100;
        _tokenAppearCondition = 0;
        _tokenAppearPosition = -1;
        _tokenWaveInfo = string.Empty;

        tokenId.text = string.Empty;
        tokenAppearRating.text = string.Empty;
        tokenAppearCondition.text = string.Empty;
        tokenAppearPosition.text = string.Empty;
        tokenMovingRouteValueInput.text = string.Empty;
        tokenWaveInfo.text = string.Empty;
    }

    public void Refresh(TokenData inData = null)
    {
        Clear();

        if (inData == null)
            return;

        if (inData.type > 0)
        {
            tokenId.text = inData.type.ToString();
            OnInputField(0);
        }

        //if (inData.appear_rating < 100)
        {
            tokenAppearRating.text = inData.appear_rating.ToString();
            OnInputField(1);
        }

        if (inData.appear_condition > 0)
        {
            tokenAppearCondition.text = inData.appear_condition.ToString();
            OnInputField(2);
        }

        if (inData.appear_position > -1)
        {
            tokenAppearPosition.text = inData.appear_position.ToString();
            OnInputField(3);
        }

        if (!inData.wave_info.IsNullOrEmpty())
        {
            tokenWaveInfo.text = inData.wave_info;
            OnInputField(5);
        }

        if (inData.moving_path != null)
        {
            string list = string.Empty;
            foreach (int o in inData.moving_path)
            {
                if (list.Length == 0)
                    list = o.ToString();
                else
                    list += string.Format(",{0}", o);

                //parent.mapToolMain.Log(string.Format("수로 패스 리스트 ({0}) 추가됨", list_index));
                //_movingRoute_list_index++;

                //List<Dropdown.OptionData> listItem = tokenMovingRouteDropdown.options;
                //Dropdown.OptionData newItem = new Dropdown.OptionData();

                //newItem.text = (_movingRoute_list_index).ToString();
                //listItem.Add(newItem);
            }

            tokenMovingRouteValueInput.text = list;
            OnInputField(4);

            //SelectMovingRouteListItem();
        }
    }

    public void OnInputField(int inParam)
    {
        switch (inParam)
        {
            case 0:
                if (tokenId.text != "")
                    _tokenId = int.Parse(tokenId.text);
                else
                    _tokenId = 0;
                break;
            case 1:
                if (tokenAppearRating.text != "")
                    _tokenAppearRating = int.Parse(tokenAppearRating.text);
                else
                    _tokenAppearRating = 0;
                break;
            case 2:
                if (tokenAppearCondition.text != "")
                    _tokenAppearCondition = int.Parse(tokenAppearCondition.text);
                else
                    _tokenAppearCondition = 0;
                break;
            case 3:
                if (tokenAppearPosition.text != "")
                    _tokenAppearPosition = int.Parse(tokenAppearPosition.text);
                else
                    _tokenAppearPosition = 0;
                break;
            case 5:
                if (!tokenWaveInfo.text.IsNullOrEmpty())
                    _tokenWaveInfo = tokenWaveInfo.text;
                else
                    _tokenWaveInfo = string.Empty;
                break;
            case 4:
                if (tokenMovingRouteValueInput.text != "")
                {
                    _movingRoutes = new List<int>();
                    string[] indexs = tokenMovingRouteValueInput.text.Split(',');
                    for (int i = 0; i < indexs.Length; i++)
                    {
                        if (indexs[i] != null)
                        {
                            _movingRoutes.Add(int.Parse(indexs[i]));
                        }
                    }
                }
                else
                    _movingRoutes = null;
                break;
        }
    }

    //public void AddMovingRoute()
    //{
    //    if (_movingRoutes == null)
    //        _movingRoutes = new Dictionary<int, SeaRoute>();

    //    _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

    //    if (_movingRouteValue.Count < 2)
    //    {
    //        parent.mtm.Log("항로는 최소 2개 이상이어야 합니다.", LogLevel.WARNING);
    //        return;
    //    }

    //    SeaRoute newOne = new SeaRoute();
    //    newOne.sea_route.AddRange(_movingRouteValue);
    //    newOne.index = _movingRoute_list_index;

    //    // 갱신
    //    if (_movingRoutes.ContainsKey(_movingRoute_list_index))
    //    {
    //        _movingRoutes.Remove(_movingRoute_list_index);
    //        _movingRoutes.Add(_movingRoute_list_index, newOne);

    //        parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 수정됨", _movingRoute_list_index));
    //    }
    //    else
    //    {
    //        _movingRoutes.Add(_movingRoute_list_index, newOne);
    //        parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 추가됨", _movingRoute_list_index));
    //        _movingRoute_list_index++;

    //        List<Dropdown.OptionData> listItem = tokenMovingRouteDropdown.options;
    //        Dropdown.OptionData newItem = new Dropdown.OptionData();

    //        newItem.text = (_movingRoute_list_index).ToString();
    //        listItem.Add(newItem);
    //    }
    //}

    //public void RemoveMovingRoute()
    //{
    //    if (_movingRoutes == null)
    //        return;

    //    _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

    //    // 삭제
    //    if (_movingRoutes.ContainsKey(_movingRoute_list_index))
    //    {
    //        _movingRoutes.Remove(_movingRoute_list_index);
    //        parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 삭제됨", _movingRoute_list_index));

    //        List<Dropdown.OptionData> listItem = tokenMovingRouteDropdown.options;
    //        int changeKey = 0;
    //        for (int i = 0; i < listItem.Count; i++)
    //        {
    //            if (int.Parse(listItem[i].text) == _movingRoute_list_index)
    //            {
    //                listItem.Remove(listItem[i]);
    //            }
    //            changeKey = i + 1;
    //            listItem[i].text = changeKey.ToString();

    //            // Dicionary 내부 키 정렬
    //            if (changeKey >= _movingRoute_list_index)
    //            {
    //                if (_movingRoutes.ContainsKey(changeKey))
    //                {
    //                    SeaRoute value = _movingRoutes[changeKey];
    //                    _movingRoutes.Remove(changeKey);
    //                    _movingRoutes.Add(i, value);
    //                }
    //            }
    //        }

    //        SelectMovingRouteListItem();
    //        _movingRoute_list_index--;
    //    }
    //}

    //public void SelectMovingRouteListItem()
    //{
    //    if (_movingRoutes == null)
    //        return;

    //    tokenMovingRouteValueInput.text = string.Empty;

    //    _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

    //    if (_movingRoutes.ContainsKey(_movingRoute_list_index))
    //    {
    //        SeaRoute targetScrollStage = _movingRoutes[_movingRoute_list_index];
    //        string list = string.Empty;

    //        list = string.Empty;
    //        foreach (int index in targetScrollStage.sea_route)
    //        {
    //            if (list.Length == 0)
    //                list = index.ToString();
    //            else
    //                list += string.Format(",{0}", index);
    //        }
    //        tokenMovingRouteValueInput.text = list;
    //        OnInputField(0);
    //    }
    //}

    public TokenData GetParseData()
    {
        OnInputField(0);
        OnInputField(1);
        OnInputField(2);
        OnInputField(3);
        OnInputField(4);
        OnInputField(5);

        TokenData newOne = new TokenData();
        newOne.type = _tokenId;
        newOne.wave_info = _tokenWaveInfo;
        newOne.appear_rating = _tokenAppearRating;
        newOne.appear_condition = _tokenAppearCondition;
        newOne.appear_position = _tokenAppearPosition;

        if (_movingRoutes != null)
        {
            newOne.moving_path = new List<int>();
            newOne.moving_path.AddRange(_movingRoutes);
        }

        return newOne;
    }
}