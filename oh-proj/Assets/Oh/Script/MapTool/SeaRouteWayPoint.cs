﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SeaRouteWayPoint : STZBehaviour
{
    [SerializeField]
    public Image line;
    [SerializeField]
    public Image circle;
    [SerializeField]
    public Text infoText;

    /// <summary>
    /// 두 좌표간의 잇는 가이드 라인 생성
    /// </summary>
    /// <param name="inStep"></param>
    /// <param name="inIsFinal"></param>
    /// <param name="inAngle">라인 각도</param>
    /// <param name="lineLength">라인 길이</param>
    public void ShowWayPoint(int inStep, bool inIsFinal, Quaternion inAngle, float lineLength)
    {
        if (infoText != null)
            infoText.text = inStep.ToString();

        if (inStep > 0)
        {
            Color newColor = Color.white;
            if (inStep == 1)
                newColor = new Color(1, 0, 0, MapToolManager.GUIDE_TILE_ALPHA);
            else if (inStep == 2)
                newColor = new Color(0, 1, 0, MapToolManager.GUIDE_TILE_ALPHA);
            else if (inStep == 3)
                newColor = new Color(0, 0, 1, MapToolManager.GUIDE_TILE_ALPHA);
            else if (inStep == 4)
                newColor = new Color(1, 1, 0, MapToolManager.GUIDE_TILE_ALPHA);
            else if (inStep == 5)
                newColor = new Color(1, 0, 1, MapToolManager.GUIDE_TILE_ALPHA);
            else if (inStep == 6)
                newColor = new Color(0, 1, 1, MapToolManager.GUIDE_TILE_ALPHA);
            line.color = newColor;
        }

        Vector2 lineSize = line.rectTransform.sizeDelta;
        lineSize.x = lineLength;
        line.rectTransform.sizeDelta = lineSize;

        if (circle != null)
        {
            if (inIsFinal)
                circle.transform.localRotation = Quaternion.Inverse(inAngle);
            circle.gameObject.SetActive(inIsFinal);
        }
    }

    public void HideWayPoint()
    {
        this.gameObject.SetActive(false);
    }
}
