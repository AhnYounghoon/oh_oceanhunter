﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class MapToolInputField : MonoBehaviour
{
    enum InputType
    {
        INDEX,
        NUMBER
    }

    [SerializeField]    
    InputType inputType;
    bool isDownCtrl = false;
    bool isDownRightMouse = false;
    InputField inputField;

    void Start()
    {
        inputField = this.Get<InputField>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
            isDownCtrl = true;

        if (Input.GetKeyUp(KeyCode.LeftControl))
            isDownCtrl = false;

        if (Input.GetMouseButtonDown(1))
            isDownRightMouse = true;

        if (Input.GetMouseButtonUp(1))
            isDownRightMouse = false;

        if (inputType == InputType.NUMBER)
            SetInputField();

        if (inputType == InputType.INDEX)
        {
            if (Input.GetMouseButtonDown(0))
                AddAtInputField();

            if (Input.GetMouseButtonDown(1))
                RemoveAtInputField();
        }
    }

    public void AddAtInputField()
    {
        if (!isDownCtrl)
            return;

        if (MapToolManager.INPUT_INDEX < 0)
            return;

        string assignText = inputField.text;
        if (inputField.text.Length > 0)
            assignText += StringHelper.Format(",{0}", MapToolManager.INPUT_INDEX);
        else
            assignText = MapToolManager.INPUT_INDEX.ToString();

        inputField.text = assignText;
        inputField.caretPosition = inputField.text.Length;
    }

    public void RemoveAtInputField()
    {
        if (!isDownCtrl)
            return;

        string assignText = inputField.text;
        int lastTokenIndex = assignText.LastIndexOf(',');
        if (lastTokenIndex >= 0)
            assignText = assignText.Substring(0, lastTokenIndex);
        else
            assignText = string.Empty;

        inputField.text = assignText;
        inputField.caretPosition = inputField.text.Length;
    }

    public void SetInputField()
    {
        if (!isDownRightMouse)
            return;

        int value = -1;
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            value = KeyCode.Alpha0 - KeyCode.Alpha0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            value = KeyCode.Alpha1 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            value = KeyCode.Alpha2 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            value = KeyCode.Alpha3 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            value = KeyCode.Alpha4 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            value = KeyCode.Alpha5 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            value = KeyCode.Alpha6 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            value = KeyCode.Alpha7 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            value = KeyCode.Alpha8 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            value = KeyCode.Alpha9 - KeyCode.Alpha0;
        }
        else if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            inputField.text = string.Empty;
        }

        if (value >= 0)
        {
            string assignText = inputField.text;
            if (assignText.Length > 0)
                assignText += value.ToString();
            else
                assignText = value.ToString();

            inputField.text = assignText;
            inputField.caretPosition = inputField.text.Length;
        }
    }
}
