using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections.Generic;
using SimpleJSON;

public class TabMerge : STZBehaviour
{
    [SerializeField]
    public MapToolManager mtm;
    [SerializeField]
    InputField file1Input;
    [SerializeField]
    InputField file2Input;
    [SerializeField]
    InputField outfileInput;

    bool drawGUI = false;
    FileBrowser fileBroswer = null;
    string file1Path;
    string file2Path;
    string outputfilePath;
    bool bFile1;
    bool bFile2;
    bool bOutputFile;

    public void Init()
    {
        fileBroswer = mtm.fileBrowser;
    }

    void OnGUI()
    {
        if (!drawGUI)
            return;

        if (fileBroswer.draw())
        {
            if (fileBroswer.outputFile == null)
            {
                if (bOutputFile && fileBroswer.outputDir != null)
                {
                    drawGUI = false;

                    outputfilePath = fileBroswer.outputDir.FullName;
                    outfileInput.text = outputfilePath;
                    bOutputFile = false;

                    // 다른 폴더 선택시 초기화
                    //if (selectedFolderName == fileBrowser.outputDir.FullName)
                    //    return;

                    //fileBroswer.ClearAllFileList();
                    //fileBroswer.GetAllFileListInDirectory(fileBroswer.outputDir);
                    //mtm.Log(StringHelper.Format("Find ({0}) files in ({1}) Folder.", fileBroswer.all_files.Count, fileBroswer.outputDir.Name));
                    //selectedFolderName = fileBroswer.outputDir.FullName;
                }
                else
                    drawGUI = false;
            }
            else
            {
                if (bOutputFile && fileBroswer.outputDir != null)
                {
                    drawGUI = false;

                    outputfilePath = fileBroswer.outputDir.FullName;
                    outfileInput.text = outputfilePath;
                    bOutputFile = false;
                }
                else if (fileBroswer.outputFile.FullName.Contains(".txt"))
                {
                    //Debug.Log("LoadData : " + fileBroswer.outputFile.FullName);

                    if (bFile1)
                    {
                        file1Path = fileBroswer.outputFile.FullName;
                        file1Input.text = file1Path;
                        bFile1 = false;
                    }
                    else if (bFile2)
                    {
                        file2Path = fileBroswer.outputFile.FullName;
                        file2Input.text = file2Path;
                        bFile2 = false;
                    }

                    drawGUI = false;
                }
            }
        }
    }

    public void OnSelectFile1Path()
    {
        if (drawGUI)
            return;

        fileBroswer.outputFile = null;
        bFile1 = true;
        drawGUI = true;
    }

    public void OnSelectFile2Path()
    {
        if (drawGUI)
            return;

        fileBroswer.outputFile = null;
        bFile2 = true;
        drawGUI = true;
    }

    public void OnSelectOutputFilePath()
    {
        if (drawGUI)
            return;

        fileBroswer.outputFile = null;
        bOutputFile = true;
        drawGUI = true;
    }

    public void OnMerge()
    {
        if (file1Path.IsNullOrEmpty() || file2Path.IsNullOrEmpty() || outputfilePath.IsNullOrEmpty())
        {
            mtm.Log("파일 패스 오류!", LogLevel.WARNING);
            return;
        }

        StageDataManager.it.FileMerge(file1Path, file2Path, outputfilePath);
        mtm.Log("파일 Merge 성공");
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
