﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum LogLevel
{
    NORMAL,
    WARNING,
    ERROR
}
public enum TabIndex
{
    IDLE = 0,
    DEFAULT = 1,
    TILE = 2,
    TOKEN = 3,
    MERGE = 4
}

public class MapToolManager : MonoBehaviour
{
    public const int MAPTOOL_WIDTH = 1920;
    public const int MAPTOOL_HEIGHT = 720;
    public const float GUIDE_TILE_ALPHA = 0.3f;

    [SerializeField]
    GUISkin[] skins;
    [SerializeField]
    Texture2D file, folder, back, drive;

    [SerializeField]
    Canvas indexCanvas;
    [SerializeField]
    PanelComponentInfo infoPanel;
    [SerializeField]
    Text txtIndex;
    [SerializeField]
    Text txtKeyboard;
    [SerializeField]
    Text txtSplitIndex;
    [SerializeField]
    Text txtLog;

    [SerializeField]
    TabDefault tabDefault;
    [SerializeField]
    TabTile tabTile;
    [SerializeField]
    TabToken tabToken;
    [SerializeField]
    TabPlayer tabPlayer;
    [SerializeField]
    TabMerge tabMerge;

    [SerializeField]
    BattleOceanController oceanController;

    bool drawGUI = false;
    public FileBrowser fileBrowser = null;
    TabIndex currentTab;
    bool mouseDown = false;
    ComponentButton selectComponent;
    List<GameObject> gridsData = new List<GameObject>();

    public bool isPlayGame = false;
    public bool isDownCtrl = false;
    public int selectedStageNum = 1;
    /// <summary>
    /// MapToolInputField.cs에서 사용하기 위함
    /// </summary>
    static public int INPUT_INDEX = -1;

    List<GameObject> seaRouteGuide = new List<GameObject>();

    void Awake()
    {
        Application.targetFrameRate = 60;

        Statics.Instance.Parse(Statics.LoadFromLocal());
        //if (Screen.width != MAPTOOL_WIDTH)
        //{
        //    Debug.LogError("맵툴 기본해상도가 아닙니다.");
        //    Screen.SetResolution(MAPTOOL_WIDTH, MAPTOOL_HEIGHT, true);
        //    Camera.main.transform.position = new Vector2(Camera.main.transform.position.x + 320, Camera.main.transform.position.y);
        //}
    }

    // Use this for initialization
    void Start()
    {
        BattleOceanController.HORIZONTAL = BattleOceanController.REGULAR_SIZE_X;
        BattleOceanController.VERTICAL = BattleOceanController.REGULAR_SIZE_Y;
        BattleOceanController.TILE_TOTAL = BattleOceanController.HORIZONTAL * BattleOceanController.VERTICAL;

        BattleOceanController.SCROLL_X = 0;
        BattleOceanController.SCROLL_Y = 0;
        BattleOceanController.HORIZONTAL_GAP = BattleOceanController.HORIZONTAL - BattleOceanController.REGULAR_SIZE_X;
        BattleOceanController.VERTICAL_GAP = BattleOceanController.VERTICAL - BattleOceanController.REGULAR_SIZE_Y;

        InitFileBrowser();
        TPResourceManager.it.LoadResources();

        InitGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            txtKeyboard.text = KeyCode.F1.ToString();
            OnSelectTopButton((int)TabIndex.DEFAULT);
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            txtKeyboard.text = KeyCode.F2.ToString();
            OnSelectTopButton((int)TabIndex.TILE);
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            txtKeyboard.text = KeyCode.F3.ToString();
            OnSelectTopButton((int)TabIndex.TOKEN);
        }
        else if (Input.GetKeyDown(KeyCode.Tab))
        {
            //OnObjectRotate();
        }
        else if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            txtKeyboard.text = KeyCode.LeftControl.ToString();
            isDownCtrl = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            isDownCtrl = false;
        }
        else if (Input.GetKeyDown(KeyCode.Less))
        {
            txtKeyboard.text = KeyCode.Less.ToString();
        }
        else if (Input.GetKeyDown(KeyCode.Greater))
        {
            txtKeyboard.text = KeyCode.Greater.ToString();
        }

        if (!Input.anyKey)
            txtKeyboard.text = string.Empty;

        Vector3 pos = Input.mousePosition;
        Vector3 convertPos = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, 0.0f));

        bool isVaildArea = false;
        int selectItemX = -1;
        int selectItemY = -1;
        int target_index = -1;
        INPUT_INDEX = -1;

        if (!Coordinate.VaildationCoordinate(convertPos))
        {
            infoPanel.HideIndexInfo();
            txtIndex.text = string.Empty;
            isVaildArea = false;
            return;
        }
        else
        {
            isVaildArea = true;
        }

        if (isVaildArea)
        {
            selectItemX = Coordinate.GetPositionInGridIndex(true, (int)convertPos.x);
            selectItemY = Coordinate.GetPositionInGridIndex(false, (int)convertPos.y);
            target_index = Coordinate.ToIdxNumber(selectItemX, selectItemY);
        }

        //Debug.Log(string.Format("selectItemX({0}), selectItemY({1}), target_index({2})", selectItemX, selectItemY, target_index));

        if (selectItemX >= 0 && selectItemX < BattleOceanController.HORIZONTAL && selectItemY >= 0 && selectItemY < BattleOceanController.VERTICAL)
        {
            if (isPlayGame)
                infoPanel.HideIndexInfo();
            else
            {
                if (target_index != -1 && infoPanel.index != target_index/* && GameEngine.isMapSetting*/)
                    infoPanel.ShowIndexInfo(target_index);
                //txtIndex.text = string.Format("인덱스 : {0}", target_index);
                txtIndex.text = target_index.ToString();
                INPUT_INDEX = target_index;
            }
        }

        // 삭제
        if (Input.GetMouseButton(1))
        {
            if (mouseDown == true)
            {
                if (isDownCtrl)
                    return;

                switch (currentTab)
                {
                    case TabIndex.TILE:
                        RemoveTile(target_index);
                        break;
                    case TabIndex.TOKEN:
                        RemoveToken(target_index);
                        break;
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                mouseDown = true;
            }
        }

        // 추가
        if (Input.GetMouseButton(0))
        {
            if (selectComponent == null)
                return;

            if (mouseDown)
            {
                if (selectComponent.type == 99)
                    return;

                if (isDownCtrl)
                    return;

                switch (currentTab)
                {
                    case TabIndex.TILE:
                        SetTile(target_index);
                        break;
                    case TabIndex.TOKEN:
                        SetToken(target_index);
                        break;
                }
            }


            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;

                if (selectComponent.type == 99)
                    SelectObject(target_index);
            }
        }

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        {
            mouseDown = false;
        }
    }

    #region fileBrowser
    void InitFileBrowser()
    {
#if UNITY_EDITOR
        fileBrowser = new FileBrowser(Application.dataPath + "/Resources/Json", 1);
#else
        fileBrowser = new FileBrowser(1);
#endif

        //fileBrowser.guiSkin = skins[0];
        fileBrowser.guiSkin = skins[1];

        fileBrowser.fileTexture = file;
        fileBrowser.directoryTexture = folder;
        fileBrowser.backTexture = back;
        fileBrowser.driveTexture = drive;
        //show the search bar
        fileBrowser.showSearch = true;
        //search recursively (setting recursive search may cause a long delay)
        fileBrowser.searchRecursively = false;
    }

    void OnGUI()
    {
        if (!drawGUI)
            return;

        if (fileBrowser.draw())
        {
            if (fileBrowser.outputFile == null)
            {
                drawGUI = false;
            }
            else
            {
                if (fileBrowser.outputFile.FullName.Contains(".txt"))
                {
                    Debug.Log("LoadData : " + fileBrowser.outputFile.FullName);

                    bool ret = StageDataManager.it.LoadFilePathData(fileBrowser.outputFile.FullName, false);

                    if (ret)
                    {
                        selectedStageNum = 1;
                        Log(StringHelper.Format("{0} 데이터 로딩 성공", fileBrowser.outputFile.FullName));
                    }
                    else
                    {
                        Log(StringHelper.Format("{0} 데이터 로딩 실패!", fileBrowser.outputFile.FullName), LogLevel.ERROR);
                        return;
                    }

                    drawGUI = false;

                    if (StageDataManager.it.GetStageCount() > 0)
                    {
                        ShowStep();
                    }
                }
            }
        }
    }
    #endregion

    void InitGame()
    {
        tabTile.Init();
        tabToken.Init();
        tabPlayer.Init();
        InitGrid();

        OnSelectTopButton((int)TabIndex.DEFAULT);

        // 배틀씬에서 온 경우 처리
        if (GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.BattleScene)
        {
            ResetAndInit();
            PlayStage();
        }
    }

    void SetOceanObject()
    {
        LocalStageData targetStageData = StageDataManager.it.GetStage();

        if (targetStageData == null)
            return;

        oceanController.SetOcean();
    }

    public void ResetAndInit()
    {
        oceanController.ResetGame();

        //RemoveSeaRouteGuide(-1, true);

        // NOTE @minjun 이전에 작성중인 스테이지,캐릭터 데이터를 초기화시킨다
        tabDefault.writeStageData = null;

        // 스테이지 데이터 초기화 포함됨
        StageDataManager.it.SelectStage(selectedStageNum);

        //GameEngine.isMapSetting = false;
        SetOceanObject();

        // 항로 가이드 텍스쳐 생성
        //List<SeaRoute> seaRouteList = StageDataManager.it.GetSeaRoute();
        //if (seaRouteList != null)
        //{
        //    for (int i = 0; i < seaRouteList.Count; i++)
        //    {
        //        SeaRoute sr = seaRouteList[i];

        //        for (int j = 0; j < sr.sea_route.Count; j++)
        //        {
        //            if (j == 0)
        //                continue;

        //            AddSeaRouteGuide(sr.sea_route[j], sr.sea_route[j - 1], i + 1, j == sr.sea_route.Count - 1);
        //        }
        //    }
        //}

        //GameEngine.isMapSetting = true;
    }

    public void ShowStep()
    {
        OnSelectTopButton((int)TabIndex.DEFAULT);

        ResetAndInit();

        RefreshAll();
    }

    public void RefreshAll()
    {
        tabDefault.Refresh();
        tabTile.Refresh();
        tabToken.Refresh();
    }

    #region maptool guideImage
    void InitGrid()
    {
        gridsData.Clear();

        Vector3 pos = new Vector3();

        for (int y = 0; y < BattleOceanController.VERTICAL; y++)
        {
            for (int x = 0; x < BattleOceanController.HORIZONTAL; x++)
            {
                pos.x = BattleOceanController.START_POS_X + x * BattleOceanController.TILE_WIDTH;
                pos.y = BattleOceanController.START_POS_Y + y * BattleOceanController.TILE_HEIGHT;

                GameObject grid = Instantiate(Resources.Load("MapTool/MapGrid")) as GameObject;
                SpriteRenderer sr = grid.GetComponent<SpriteRenderer>();
                sr.sprite = TPResourceManager.it.GetSprite("grid");
                grid.name = string.Format("MapGrid_{0}_{1}", x, y);
                grid.transform.SetParent(this.transform);
                grid.transform.localPosition = pos;
                gridsData.Add(grid);
            }
        }
    }

    void OnOffGrid()
    {
        GameObject grid;
        for (int i = 0; i < gridsData.Count; i++)
        {
            grid = gridsData[i];
            grid.gameObject.SetActive(!grid.gameObject.activeSelf);
        }
    }

    void AddSeaRouteGuide(int inIndex, int inPrevIndex, int inStep, bool inIsFinal)
    {
        Vector3 pos = new Vector3();

        pos.x = BattleOceanController.START_POS_X + (inIndex % BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_WIDTH;
        pos.y = BattleOceanController.START_POS_Y + (inIndex / BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_HEIGHT;

        GameObject hts = Instantiate(Resources.Load("MapTool/SeaRouteWayPoint")) as GameObject;
        hts.name = string.Format("SeaRouteWayPoint_{0}_{1}", inIndex, inStep);
        SeaRouteWayPoint sr = hts.Get<SeaRouteWayPoint>();

        //Quaternion angle = Quaternion.Euler(0, 0, 0);
        //if (Mathf.Abs(inIndex - inPrevIndex) == 1)
        //{
        //    if (inIndex > inPrevIndex)
        //        angle = Quaternion.Euler(0, 0, 0);
        //    else
        //        angle = Quaternion.Euler(0, 0, 180);
        //}
        //else if (Mathf.Abs(inIndex - inPrevIndex) == BattleOceanController.REGULAR_SIZE_X)
        //
        //    if (inIndex > inPrevIndex)
        //        angle = Quaternion.Euler(0, 0, 90);
        //    else
        //        angle = Quaternion.Euler(0, 0, 270);
        //}
        //else
        //{
        //    if (inIndex > inPrevIndex)
        //    {
        //        if (inIndex % BattleOceanController.REGULAR_SIZE_X < inPrevIndex % BattleOceanController.REGULAR_SIZE_X)
        //            angle = Quaternion.Euler(0, 0, 135);
        //        else
        //            angle = Quaternion.Euler(0, 0, 45);
        //    }
        //    else
        //    {
        //        if (inIndex % BattleOceanController.REGULAR_SIZE_X < inPrevIndex % BattleOceanController.REGULAR_SIZE_X)
        //            angle = Quaternion.Euler(0, 0, 225);
        //        else
        //            angle = Quaternion.Euler(0, 0, 315);
        //    }
        //}
        Vector2 prevPos = Coordinate.ToPosition(inPrevIndex);
        Vector2 currPos = Coordinate.ToPosition(inIndex);

        float bAngle = STZMath.BetweenAngle(prevPos, currPos);
        Quaternion angle = Quaternion.Euler(0, 0, bAngle);

        Vector2 gapPos = currPos - prevPos;
        float lengthGap = Mathf.Sqrt(gapPos.x * gapPos.x + gapPos.y * gapPos.y);

        sr.ShowWayPoint(inStep, inIsFinal, angle, lengthGap);

        hts.transform.SetParent(indexCanvas.transform);
        hts.transform.localPosition = pos;
        hts.transform.localRotation = angle;
        seaRouteGuide.Add(hts);
    }

    void RemoveSeaRouteGuide(int inIndex, bool inIsAll = false)
    {
        if (seaRouteGuide == null || seaRouteGuide.Count <= 0)
            return;

        int i = 0;
        while (i < seaRouteGuide.Count)
        {
            GameObject o = seaRouteGuide[i];
            string[] arr = o.name.Split('_');

            if (int.Parse(arr[1]) == inIndex)
            {
                seaRouteGuide.Remove(o);
                Destroy(o);
                break;
            }
            else if (inIsAll)
            {
                seaRouteGuide.Remove(o);
                Destroy(o);
                i--;

                if (seaRouteGuide.Count == 0)
                    break;
            }

            i++;
        }
    }
    #endregion

    #region object interation
    public void OnSelectTile(ComponentButton button)
    {
        if (selectComponent != null)
            selectComponent.Unselect();

        selectComponent = button;
        selectComponent.Select();
    }

    public void OnSelectToken(ComponentButton button)
    {
        if (selectComponent != null)
            selectComponent.Unselect();

        selectComponent = button;
        selectComponent.Select();

        //tabToken.SetTokenPanel(.IsTrainHead(selectComponent.type) ? true : false);
    }

    void SelectObject(int inIndex)
    {
        if (isDownCtrl)
            return;

        switch (currentTab)
        {
            case TabIndex.TILE:
                List<Tile> targetTile = oceanController.oceanObjLayer.GetTile(inIndex);
                if (targetTile != null)
                {
                    foreach (Tile o in targetTile)
                    {
                        if (o.Data == null)
                            continue;

                        tabTile.tile_panel.Refresh(o.Data);
                        tabTile.SetTilePanel(true);
                    }
                }
                break;

            case TabIndex.TOKEN:
                List<Token> targetToken = oceanController.oceanObjLayer.GetToken(inIndex);
                if (targetToken != null)
                {
                    foreach (Token o in targetToken)
                    {
                        if (o.Data == null)
                            continue;

                        tabToken.token_panel.Refresh(o.Data);
                        tabToken.SetTokenPanel(true);
                    }
                }
                break;
        }
    }

    void SetTile(int inIndex)
    {
        if (selectComponent == null)
            return;

        List<Tile> targetTile = oceanController.oceanObjLayer.GetTile(inIndex);
        if (targetTile != null && targetTile.Count > 0)
        {
            return;
        }

        TileData newOne = new TileData();
        //if (Tile.IsEvent(selectComponent.type) || Tile.IsNormal(selectComponent.type))
            newOne = tabTile.tile_panel.GetParseData();

        newOne.index = inIndex;
        newOne.type = selectComponent.type;
        newOne.shape = selectComponent.shape;

        oceanController.oceanObjLayer.CreateTileHasData(newOne);
    }

    void RemoveTile(int inIndex)
    {
        if (selectComponent == null)
            return;

        List<Tile> targetTile = oceanController.oceanObjLayer.GetTile(inIndex);

        if (targetTile == null)
            return;

        oceanController.oceanObjLayer.RemoveTile(inIndex, selectComponent.type);
    }

    void SetToken(int inIndex)
    {
        if (selectComponent == null)
            return;

        List<Token> targetToken = oceanController.oceanObjLayer.GetToken(inIndex);
        if (targetToken != null && targetToken.Count > 0)
        {
            return;
        }

        TokenData newOne = new TokenData();
        newOne = tabToken.token_panel.GetParseData();
        newOne.index = inIndex;

        oceanController.oceanObjLayer.CreateTokenHasData(newOne);
    }

    void RemoveToken(int inIndex)
    {
        if (selectComponent == null)
            return;

        List<Token> targetToken = oceanController.oceanObjLayer.GetToken(inIndex);

        if (targetToken == null)
            return;

        oceanController.oceanObjLayer.RemoveToken(inIndex, selectComponent.type);
    }
    #endregion

    #region ButtonAction
    /// <summary>
    /// 인덱스 입력 작업이 필요한 부분에 연결 인덱스를 추가한다
    /// </summary>
    /// <param name="text"></param>
    public void AutoPathInput(InputField inputField)
    {
        // 제거, 자동 입력 중일때 제외
        // 키보드 입력을 통한 접근 제외 
        if (!Input.anyKeyDown || !Input.GetKey(KeyCode.Space) || inputField.text.Length == 1)
        {
            return;
        }

        // 연결 인덱스 체크
        if (txtSplitIndex.text.Length == 0)
        {
            Log("경로 인덱스 값을 채워주세요!", LogLevel.ERROR);
            return;
        }

        // 마지막 인덱스 중복 체크
        string assignText = inputField.text.Substring(0, inputField.text.Length - 1);
        if (inputField.text.Length >= 2 && txtSplitIndex.text.Length >= 1)
        {
            if (inputField.text[inputField.text.Length - 2] != txtSplitIndex.text[0])
            {
                assignText = StringHelper.Format("{0}{1}", assignText, txtSplitIndex.text);
            }
        }
        inputField.text = assignText;
        inputField.caretPosition = inputField.text.Length;
    }

    /// <summary>
    /// split 문자와 값의 개수가 일치한지 확인
    /// </summary>
    /// <param name="text"></param>
    public void IdentifyInputStandard(Text text)
    {
        string value = text.text;
        if (value == string.Empty)
            return;

        int valueCount = value.Split(',').Length;
        int splitCount = 0;
        for (int i = 0; i < value.Length; i++)
        {
            if (value[i] == ',')
            {
                splitCount++;
            }
        }

        if (valueCount != (splitCount + 1) || value[value.Length - 1] == ',')
        {
            Log("작성 규격과 일치하지 않습니다!", LogLevel.WARNING);
        }
    }

    public void LogClear()
    {
        txtLog.text = string.Empty;
    }

    public void Log(string inLog, LogLevel inLevel = LogLevel.NORMAL)
    {
        if (inLevel == LogLevel.WARNING)
            inLog = string.Format("<color=#E7C802FF>[WARNING] {0}</color>", inLog);
        if (inLevel == LogLevel.ERROR)
            inLog = string.Format("<color=#EC0000FF>[ERROR] {0}</color>", inLog);

        txtLog.text = inLog;
    }

    public void OnSelectTopButton(int inIndex)
    {
        if (drawGUI)
            return;

        if (selectComponent != null)
            selectComponent.Unselect();

        if (isPlayGame)
            return;

        HideAllTab();

        currentTab = (TabIndex)inIndex;

        switch (currentTab)
        {
            case TabIndex.DEFAULT:
                tabDefault.Show();
                break;

            case TabIndex.TILE:
                tabTile.Show();
                break;

            case TabIndex.TOKEN:
                tabToken.Show();
                break;

            case TabIndex.MERGE:
                tabMerge.Init();
                tabMerge.Show();
                break;
        }
    }

    public void HideAllTab()
    {
        tabDefault.Hide();
        tabTile.Hide();
        tabToken.Hide();
        tabPlayer.Hide();
        tabMerge.Hide();
    }

    public void OnSelectBottomButton(int index)
    {
        if (drawGUI)
            return;

        switch (index)
        {
            case 1:
                LoadData();
                break;
            case 2:
                ResetStage();
                break;
            case 3:
                SaveStage();
                break;
            case 4:
                SaveData(false);
                break;
            case 5:
                SaveData(true);
                break;
            case 6:
                // 플레이 전 세이브
                //if (toggleSavePlay.isOn)
                //{
                //    SaveStage();

                //    // NOTE @minjun 세이브후 데이터 셋팅 딜레이 적용.
                //    Invoke("PlayStage", 0.5f);
                //}
                //else
                {
                    PlayStage();
                }
                break;
            case 7:
                StopStage();
                break;
            //case 8:
            //    OnOffGuideTile();
            //    break;
        }
    }

    void LoadData()
    {
        if (isPlayGame)
        {
            Log("게임 플레이를 멈추고 로딩하세요!", LogLevel.ERROR);
            return;
        }

        if (drawGUI)
            return;

        drawGUI = true;
    }

    void ResetStage()
    {
        if (isPlayGame)
        {
            Log("게임 플레이를 멈추고 리셋하세요!", LogLevel.ERROR);
            return;
        }

        //SimplePopup.Confirm("자네 정말로 리셋할 생각인가?", () =>
        //{
        oceanController.ResetGame();

        //RemoveSeaWayGuide(-1, true);

        //// NOTE @minjun 작성중인 탭 정보 초기화
        //characterTab.panelInfo.data = null;
        LocalStageData targetStageData = StageDataManager.it.GetStage();
        if (targetStageData != null)
        {
            targetStageData.ClearData();
        }

        //stageTab.Clear();
        //boardTab.Refresh();
        //blockTab.Refresh();
        //obstacleTab.Refresh();
        //treasureTab.Refresh();
        //characterTab.Refresh();
        //triggerTab.Refresh();
        //infinityTowerTab.Refresh();

        //isResetStage = true;

        Log("스테이지 리셋!");
        //}, null, "경고", "리셋", "아니요");
    }

    /// <summary>
    /// 현재 배치된 토큰, 타일 정보등 메모리에 반영
    /// </summary>
    /// <param name="inIsBeforePlay">게임 플레이 전 저장 상황 여부</param>
    public void SaveStage(bool inIsBeforePlay = true)
    {
        if (!inIsBeforePlay)
            isPlayGame = false;

        if (isPlayGame)
        {
            Log("게임 플레이를 멈추고 저장하세요!", LogLevel.ERROR);
            return;
        }

        LocalStageData newStage = new LocalStageData();

        if (inIsBeforePlay)
        {
            if (tabDefault.stageInput.text.IsNullOrEmpty())
            {
                Log("스테이지 값 공백!", LogLevel.ERROR);
                newStage.index = 1;
            }
            else
                newStage.index = int.Parse(tabDefault.stageInput.text);

            if (!tabDefault.stageTagInput.text.IsNullOrEmpty())
            {
                newStage.tag = tabDefault.stageTagInput.text;
            }

            if (tabDefault.hSizeInput.text.IsNullOrEmpty())
            {
                Log(StringHelper.Format("보드 사이즈 가로값 공백 => {0}로 셋팅합니다!", BattleOceanController.REGULAR_SIZE_X), LogLevel.ERROR);
                newStage.tileSize_h = BattleOceanController.REGULAR_SIZE_X;
            }
            else
                newStage.tileSize_h = int.Parse(tabDefault.hSizeInput.text);

            if (tabDefault.vSizeInput.text.IsNullOrEmpty())
            {
                Log(StringHelper.Format("보드 사이즈 세로값 공백 => {0}로 셋팅합니다!", BattleOceanController.REGULAR_SIZE_Y), LogLevel.ERROR);
                newStage.tileSize_v = BattleOceanController.REGULAR_SIZE_Y;
            }
            else
                newStage.tileSize_v = int.Parse(tabDefault.vSizeInput.text);
        }
        else
        {
            newStage.index = selectedStageNum;
            newStage.tag = "BeforeIntoBattleScene";
        }

        for (int i = 0; i < (int)EBattleOceanShape.COUNT; i++)
        {
            for (int objIndex = 0; objIndex < newStage.tileSize_h * newStage.tileSize_v; objIndex++)
            {
                switch (i)
                {
                    case (int)EBattleOceanShape.LAYER_TILE:
                        List<Tile> tile = oceanController.oceanObjLayer.GetTile(objIndex);
                        if (tile != null)
                        {
                            if (newStage.tileList == null)
                                newStage.tileList = new List<TileData>();

                            foreach (Tile o in tile)
                            {
                                // 출현 확률에 의해서 사라진 타일은 제외
                                if (!o.gameObject.activeSelf)
                                    continue;

                                newStage.tileList.Add(o.Data);
                            }
                        }
                        break;

                    case (int)EBattleOceanShape.LAYER_TOKEN:
                        List<Token> token = oceanController.oceanObjLayer.GetToken(objIndex);
                        if (token != null)
                        {
                            if (newStage.tokenList == null)
                                newStage.tokenList = new List<TokenData>();

                            foreach (Token o in token)
                            {
                                // 출현 확률에 의해서 사라진 토큰은 제외
                                if (!o.gameObject.activeSelf)
                                    continue;

                                // 배틀씬 진입 전 저장상황에서 토큰이 이동했다면 저장
                                if (!inIsBeforePlay && o.Index != o.Data.index)
                                    o.Data.index = o.Index;

                                newStage.tokenList.Add(o.Data);
                            }
                        }
                        break;
                }
            }
        }

        Dictionary<int, SeaRoute> seaRouteInfo = tabDefault.panelSeaRoute.GetParseSeaRouteData();
        if (seaRouteInfo != null)
        {
            if (newStage.seaRouteList == null)
                newStage.seaRouteList = new List<SeaRoute>();

            foreach (int i in seaRouteInfo.Keys)
            {
                SeaRoute sr = seaRouteInfo[i];

                newStage.seaRouteList.Add(sr);
            }
        }

        if (inIsBeforePlay)
            StageDataManager.it.SaveStageData(newStage);
        else
            StageDataManager.it.SaveTempStageData(newStage);

        Log(StringHelper.Format("({0})스테이지가 저장되었습니다.", newStage.index));

        selectedStageNum = newStage.index;

        if (inIsBeforePlay)
        {
            OnSelectTopButton((int)TabIndex.DEFAULT);
            tabDefault.writeStageData = null;
            tabDefault.Refresh();

            ResetAndInit();
        }
        //isResetStage = false;
    }

    void SaveData(bool inIsCompress)
    {
        if (isPlayGame)
        {
            Log("게임 플레이를 멈추고 저장하세요!", LogLevel.ERROR);
            return;
        }

        string filePath;

#if UNITY_EDITOR
        string fileName = "StageData";
        filePath = Application.dataPath + "/Resources/Json/" + fileName;
#else
		System.DateTime dt = System.DateTime.Now;
		string fileName = string.Format ("StageData_{0}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
		filePath = Application.dataPath + "/" + fileName;
#endif
        if (StageDataManager.it.GetStageCount() <= 0)
        {
            Log("스테이지가 0개입니다. 저장 불가능!", LogLevel.WARNING);
            return;
        }

        StageDataManager.it.SaveDataFile(filePath, inIsCompress);

        Log(StringHelper.Format("{0} 데이터 저장 성공!", filePath));
    }

    void PlayStage()
    {
        if (isPlayGame)
            return;

        //if (isResetStage)
        //    return;

        if (StageDataManager.it.GetStage() == null)
        {
            Log("스테이지가 세팅되지 않았습니다", LogLevel.ERROR);
            return;
        }

        isPlayGame = true;
        //GameEngine.isMapSetting = false;

        OnOffGrid();
        tabPlayer.Show();
        //RemoveHiddenTileGuide(-1, true);
        //RemoveWaterTileGuide(-1, true);
        //RemoveBiscuitTileGuide(-1, true);
        //RemoveToastTileGuide(-1, true);

        Log(StringHelper.Format("{0} 스테이지 Play", selectedStageNum));

        BattleOceanController.SCROLL_X = 0;
        BattleOceanController.SCROLL_Y = 0;
        BattleOceanController.HORIZONTAL_GAP = BattleOceanController.HORIZONTAL - BattleOceanController.REGULAR_SIZE_X;
        BattleOceanController.VERTICAL_GAP = BattleOceanController.VERTICAL - BattleOceanController.REGULAR_SIZE_Y;

        oceanController.StartHunting();
    }

    public void StopStage()
    {
        if (!isPlayGame)
            return;

        isPlayGame = false;
        //GameEngine.isMapSetting = true;
        //toggleSeason2.isOn = false;
        //GeneralDataManager.it.currentWorldMapType = GeneralDataManager.EWorldMapType.S1_WorldMap;

        Log("게임 플레이 Stop");

        OnOffGrid();

        OnSelectTopButton((int)TabIndex.DEFAULT);
        ResetAndInit();

        //gameMain.gameEngine.enabled = false;
    }
    #endregion
}
