﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelTile : STZBehaviour
{
    public TabTile parent;

    [SerializeField]
    public InputField eventTypeInput;
    [SerializeField]
    public InputField eventAmountInput;
    [SerializeField]
    public InputField eventRewardType;
    [SerializeField]
    public InputField eventRewardValue;
    [SerializeField]
    public InputField tileAppearRating;
    [SerializeField]
    public InputField tileId;
    [SerializeField]
    public InputField tilePass;

    [SerializeField]
    public InputField tokenMovingRouteValueInput;
    [SerializeField]
    public Dropdown tokenMovingRouteDropdown;
    [SerializeField]
    public Button tokenMovingRouteAddButton;
    [SerializeField]
    public Button tokenMovingRouteRemoveButton;

    private int _event_type = 0;
    private int _event_amount = 0;
    private int _reward_type = 0;
    private string _reward_value = string.Empty;
    private int _tileAppearRating = 100;
    private int _tileId = 0;
    private string _tile_pass = string.Empty;

    private int _tokenId = 0;
    private List<int> _movingRouteValue = null;
    private Dictionary<int, SeaRoute> _movingRoutes = null;
    private int _movingRoute_list_index = 1;

    public void Clear()
    {
        //while (tokenMovingRouteDropdown.options.Count > 1)
        //    tokenMovingRouteDropdown.options.RemoveAt(tokenMovingRouteDropdown.options.Count - 1);

        //_movingRouteValue = null;
        //_moveingRoutes = null;
        //_movingRoute_list_index = 1;

        _event_type = 0;
        _event_amount = 0;
        _reward_type = 0;
        _reward_value = string.Empty;
        _tileAppearRating = 100;
        _tileId = 0;
        _tile_pass = string.Empty;

        eventTypeInput.text = string.Empty;
        eventAmountInput.text = string.Empty;
        eventRewardType.text = string.Empty;
        eventRewardValue.text = string.Empty;
        tileAppearRating.text = string.Empty;
        tileId.text = string.Empty;
        tilePass.text = string.Empty;
    }

    public void Refresh(TileData inData = null)
    {
        Clear();

        if (inData == null)
            return;

        if (inData.event_type > -1)
        {
            eventTypeInput.text = inData.event_type.ToString();
            OnInputField(0);
        }

        if (inData.event_amount > 0)
        {
            eventAmountInput.text = inData.event_amount.ToString();
            OnInputField(4);
        }

        if (inData.reward_type > 0)
        {
            eventRewardType.text = inData.reward_type.ToString();
            OnInputField(1);
        }

        if (!inData.reward_value.IsNullOrEmpty())
        {
            eventRewardValue.text = inData.reward_value;
            OnInputField(2);
        }

        tileAppearRating.text = inData.appear_rating.ToString();
        OnInputField(3);

        if (inData.id > 0)
        {
            tileId.text = inData.id.ToString();
            OnInputField(5);
        }

        if (!inData.sailing_pass.IsNullOrEmpty())
        {
            tilePass.text = inData.sailing_pass;
            OnInputField(6);
        }
    }

    public void OnInputField(int inParam)
    {
        switch (inParam)
        {
            case 0:
                if (eventTypeInput.text != "")
                    _event_type = int.Parse(eventTypeInput.text);
                else
                    _event_type = 0;
                break;

            case 1:
                if (eventRewardType.text != "")
                    _reward_type = int.Parse(eventRewardType.text);
                else
                    _reward_type = 0;
                break;

            case 2:
                if (eventRewardValue.text != "")
                    _reward_value = eventRewardValue.text;
                else
                    _reward_value = string.Empty;
                break;

            case 3:
                if (tileAppearRating.text != "")
                    _tileAppearRating = int.Parse(tileAppearRating.text);
                else
                    _tileAppearRating = 100;
                break;

            case 4:
                if (eventAmountInput.text != "")
                    _event_amount = int.Parse(eventAmountInput.text);
                else
                    _event_amount = 0;
                break;
            case 5:
                if (tileId.text != "")
                    _tileId = int.Parse(tileId.text);
                else
                    _tileId = 0;
                break;
            case 6:
                if (tilePass.text != "")
                    _tile_pass = tilePass.text;
                else
                    _tile_pass = string.Empty;
                break;
        }
    }

    public void AddMovingRoute()
    {
        if (_movingRoutes == null)
            _movingRoutes = new Dictionary<int, SeaRoute>();

        _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

        if (_movingRouteValue.Count < 2)
        {
            parent.mtm.Log("항로는 최소 2개 이상이어야 합니다.", LogLevel.WARNING);
            return;
        }

        SeaRoute newOne = new SeaRoute();
        newOne.sea_route.AddRange(_movingRouteValue);
        newOne.index = _movingRoute_list_index;

        // 갱신
        if (_movingRoutes.ContainsKey(_movingRoute_list_index))
        {
            _movingRoutes.Remove(_movingRoute_list_index);
            _movingRoutes.Add(_movingRoute_list_index, newOne);

            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 수정됨", _movingRoute_list_index));
        }
        else
        {
            _movingRoutes.Add(_movingRoute_list_index, newOne);
            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 추가됨", _movingRoute_list_index));
            _movingRoute_list_index++;

            List<Dropdown.OptionData> listItem = tokenMovingRouteDropdown.options;
            Dropdown.OptionData newItem = new Dropdown.OptionData();

            newItem.text = (_movingRoute_list_index).ToString();
            listItem.Add(newItem);
        }
    }

    public void RemoveMovingRoute()
    {
        if (_movingRoutes == null)
            return;

        _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

        // 삭제
        if (_movingRoutes.ContainsKey(_movingRoute_list_index))
        {
            _movingRoutes.Remove(_movingRoute_list_index);
            parent.mtm.Log(StringHelper.Format("항로 리스트 ({0}) 삭제됨", _movingRoute_list_index));

            List<Dropdown.OptionData> listItem = tokenMovingRouteDropdown.options;
            int changeKey = 0;
            for (int i = 0; i < listItem.Count; i++)
            {
                if (int.Parse(listItem[i].text) == _movingRoute_list_index)
                {
                    listItem.Remove(listItem[i]);
                }
                changeKey = i + 1;
                listItem[i].text = changeKey.ToString();

                // Dicionary 내부 키 정렬
                if (changeKey >= _movingRoute_list_index)
                {
                    if (_movingRoutes.ContainsKey(changeKey))
                    {
                        SeaRoute value = _movingRoutes[changeKey];
                        _movingRoutes.Remove(changeKey);
                        _movingRoutes.Add(i, value);
                    }
                }
            }

            SelectMovingRouteListItem();
            _movingRoute_list_index--;
        }
    }

    public void SelectMovingRouteListItem()
    {
        if (_movingRoutes == null)
            return;

        tokenMovingRouteValueInput.text = string.Empty;

        _movingRoute_list_index = int.Parse(tokenMovingRouteDropdown.captionText.text);

        if (_movingRoutes.ContainsKey(_movingRoute_list_index))
        {
            SeaRoute targetScrollStage = _movingRoutes[_movingRoute_list_index];
            string list = string.Empty;

            list = string.Empty;
            foreach (int index in targetScrollStage.sea_route)
            {
                if (list.Length == 0)
                    list = index.ToString();
                else
                    list += string.Format(",{0}", index);
            }
            tokenMovingRouteValueInput.text = list;
            OnInputField(0);
        }
    }

    public TileData GetParseData()
    {
        OnInputField(0);
        OnInputField(1);
        OnInputField(2);
        OnInputField(3);
        OnInputField(4);
        OnInputField(5);
        OnInputField(6);

        TileData newOne = new TileData();
        newOne.id = _tileId;
        newOne.event_type = _event_type;
        newOne.event_amount = _event_amount;
        newOne.reward_type = _reward_type;
        newOne.reward_value = _reward_value;
        newOne.appear_rating = _tileAppearRating;
        newOne.sailing_pass = _tile_pass;
        //newOne.rail_info = new List<BiscuitTilePath>();

        //foreach (int i in _railPaths.Keys)
        //{
        //    List<int> railPaths = _railPaths[i];

        //    BiscuitTilePath gw = new BiscuitTilePath();
        //    gw.index = i;
        //    gw.full_path.AddRange(railPaths);

        //    newOne.rail_info.Add(gw);
        //}

        return newOne;
    }
}