using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TabTile : STZBehaviour
{
    private const int TILE_GAP = 90;

    public MapToolManager mtm;
    public PanelTile tile_panel;

    Vector3 startPos = new Vector3(-250.0f, 150.0f, 0.0f);

    public void Init()
    {
        Vector3 pos = startPos;

        ComponentButton selector = CreateButton(pos, "hand");
        selector.type = 99;
        pos.x += TILE_GAP;

        ComponentButton harbor = CreateButton(pos, "harbor");
        harbor.type = (int)ETile.TILE_HARBOR;
        harbor.txt.text = "항구";
        pos.x += TILE_GAP;

        ComponentButton goalTile = CreateButton(pos, "goal");
        goalTile.type = (int)ETile.TILE_GOAL;
        goalTile.txt.text = "도착";
        pos.x += TILE_GAP;

        ComponentButton normalTile = CreateButton(pos, "normal_1");
        normalTile.type = (int)ETile.TILE_NORMAL;
        normalTile.txt.text = "일반";
        pos.x += TILE_GAP;

        ComponentButton conditionTile = CreateButton(pos, "event");
        conditionTile.type = (int)ETile.TILE_NORMAL;
        conditionTile.txt.text = "조건부";
        pos.x += TILE_GAP;

        pos.x = -250.0f;
        pos.y -= TILE_GAP;

        //ComponentButton eventTile = CreateButton(pos, "event");
        //eventTile.type = (int)ETile.TILE_EVENT;
        //eventTile.txt.text = "이벤트";
        //pos.x += TILE_GAP;

        ComponentButton eventFishingTile = CreateButton(pos, "fishing");
        eventFishingTile.type = (int)ETile.TILE_EVENT;
        eventFishingTile.shape = (int)ETileShape.SHAPE_FISHING;
        eventFishingTile.txt.text = "어군";
        pos.x += TILE_GAP;

        ComponentButton eventIslandTile = CreateButton(pos, "island");
        eventIslandTile.type = (int)ETile.TILE_EVENT;
        eventIslandTile.shape = (int)ETileShape.SHAPE_ISLAND;
        eventIslandTile.txt.text = "무인도";
        pos.x += TILE_GAP;

        ComponentButton eventRockTile = CreateButton(pos, "rock");
        eventRockTile.type = (int)ETile.TILE_EVENT;
        eventRockTile.shape = (int)ETileShape.SHAPE_ROCK;
        eventRockTile.txt.text = "암초";
        pos.x += TILE_GAP;
    }

    public void Refresh()
    {
        tile_panel.Refresh();
    }

    public void SetTilePanel(bool inEnable)
    {
        tile_panel.gameObject.SetActive(inEnable);
        if (!inEnable)
            tile_panel.Clear();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    ComponentButton CreateButton(Vector3 pos, string name, Sprite inSprite = null)
    {
        GameObject obj = Instantiate(Resources.Load("MapTool/ComponentButton")) as GameObject;
        ComponentButton button = obj.GetComponent(typeof(ComponentButton)) as ComponentButton;
        button.name = name;
        button.transform.SetParent(this.transform);
        if (inSprite != null)
            button.SetSprite(inSprite);
        else
            button.Draw(name);
        button.SetTileListener(mtm);
        obj.transform.localPosition = pos;
        return button;
    }
}
