using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class TabDefault : STZBehaviour
{
    public MapToolManager mtm;

    public Text maxStage;
    public InputField targetStageInput;
    public InputField stageInput;
    public InputField stageTagInput;
    public InputField hSizeInput;
    public InputField vSizeInput;

    public InputField turnCountInput;
    public InputField blockTypeInput;
    public InputField score1Input;
    public InputField score2Input;
    public InputField score3Input;
    public Toggle[] missionT;
    public InputField missionCountInput;

    public PanelSeaRoute panelSeaRoute;

    public LocalStageData writeStageData = null;

    private string _writeStageInputText = "";

    void Start()
    {
        panelSeaRoute.SeaRouteAddButton.onClick.AddListener(panelSeaRoute.AddSeaRoute);
        panelSeaRoute.SeaRouteRemoveButton.onClick.AddListener(panelSeaRoute.RemoveSeaRoute);
    }

    public void Clear()
    {
        panelSeaRoute.Clear();

        maxStage.text = string.Empty;
        stageInput.text = string.Empty;
        stageTagInput.text = string.Empty;
        hSizeInput.text = string.Empty;
        vSizeInput.text = string.Empty;
        //score1Input.text        = string.Empty;
        //score2Input.text        = string.Empty;
        //score3Input.text        = string.Empty;
        //blockTypeInput.text     = string.Empty;
        //turnCountInput.text     = string.Empty;
        //missionCountInput.text  = string.Empty;
    }

    public void Refresh()
    {
        if (StageDataManager.it.GetStageCount() <= 0 || StageDataManager.it.GetStage() == null)
            return;

        // NOTE @minjun 스테이지가 변경 될때만 스테이지 UI 데이터를 초기화
        if (writeStageData == null)
        {
            Clear();
            SaveWriteData();
            panelSeaRoute.Refresh();
        }
    }

    /// <summary>
    /// 스테이지가 변경되었을때
    /// </summary>
    public void SaveWriteData()
    {
        writeStageData = StageDataManager.it.GetStage();

        targetStageInput.text = writeStageData.index.ToString();
        stageInput.text = writeStageData.index.ToString();
        stageTagInput.text = writeStageData.tag.ToString();
        //turnCountInput.text = writeStageData.turnLimit.ToString();

        //string list = string.Empty;
        //foreach (int type in writeStageData.blockKind)
        //{
        //    if (list.Length == 0)
        //        list = type.ToString();
        //    else
        //        list += string.Format(",{0}", type);
        //}
        //blockTypeInput.text = list;

        //score1Input.text = writeStageData.goalInfo.gScore_Star1.ToString();
        //score2Input.text = writeStageData.goalInfo.gScore_Star2.ToString();
        //score3Input.text = writeStageData.goalInfo.gScore_Star3.ToString();

        //missionT[writeStageData.goalInfo.gMainType - 1].isOn = true;
        //missionCountInput.text = writeStageData.goalInfo.gGoalCount.ToString();

        hSizeInput.text = writeStageData.tileSize_h.ToString();
        vSizeInput.text = writeStageData.tileSize_v.ToString();

        maxStage.text = StageDataManager.it.GetStageCount().ToString();
    }

    public void OnInputStageInfo(int index)
    {
        if (mtm.isPlayGame)
        {
            mtm.Log("게임 플레이중에는 변경할수 없습니다. Stop후 변경가능합니다.", LogLevel.WARNING);
            return;
        }

        int result;
        switch (index)
        {
            // 스테이지 선택 변경
            case 0:
                if (int.TryParse(targetStageInput.text, out result))
                {
                    if (StageDataManager.it.GetStageCount() <= 0)
                    {
                        mtm.Log("스테이지 데이터가 선택되지 않았습니다!", LogLevel.ERROR);
                    }
                    else if (result <= 0 || result > StageDataManager.it.GetStageCount())
                    {
                        mtm.Log("유효하지 않은 값을 입력하였습니다!\n빈 스테이지로 설정합니다.", LogLevel.WARNING);

                        //mtm.gameMain.ResetGame();

                        targetStageInput.text = StageDataManager.it.GetStageCount().ToString();
                        stageInput.text = (StageDataManager.it.GetStageCount() + 1).ToString();
                        OnInputStageInfo(1);
                    }
                    else
                    {
                        mtm.Log(StringHelper.Format("{0} 스테이지를 선택했습니다.", result));

                        mtm.selectedStageNum = result;
                        mtm.ShowStep();
                    }
                }
                break;
            case 1:
                if (int.TryParse(stageInput.text, out result))
                    mtm.selectedStageNum = int.Parse(stageInput.text);
                break;
            case 2:
                if (int.TryParse(hSizeInput.text, out result))
                {
                    if (result < 18 || result > 27)
                    {
                        mtm.Log("유효하지 않은 값을 입력하였습니다!", LogLevel.WARNING);
                        return;
                    }

                    int oldHorIndex = BattleOceanController.HORIZONTAL;
                    BattleOceanController.HORIZONTAL = result;
                    //mtm.ReArrangeStage(true, oldHorIndex, BattleWorldController.HORIZONTAL);
                }
                break;
            case 3:
                if (int.TryParse(vSizeInput.text, out result))
                {
                    if (result < 10 || result > 27)
                    {
                        mtm.Log("유효하지 않은 값을 입력하였습니다!", LogLevel.WARNING);
                        return;
                    }

                    int oldVerIndex = BattleOceanController.VERTICAL;
                    BattleOceanController.VERTICAL = result;
                    //mtm.ReArrangeStage(false, oldVerIndex, BattleWorldController.VERTICAL);
                }
                break;
            case 4:
                if (int.TryParse(targetStageInput.text, out result))
                {
                    if (StageDataManager.it.GetStageCount() <= 0)
                    {
                        mtm.Log("스테이지 데이터가 선택되지 않았습니다!", LogLevel.ERROR);
                    }
                    else
                    {
                        result = mtm.isDownCtrl ? result - 10 : result - 1;
                        if (result <= 0)
                        {
                            mtm.Log(StringHelper.Format("유효하지 않은 {0} 스테이지를 선택했습니다.", result));
                            return;
                        }

                        mtm.Log(StringHelper.Format("{0} 스테이지를 선택했습니다.", result));
                        mtm.selectedStageNum = result;
                        mtm.ShowStep();
                    }
                }
                break;
            case 5:
                if (int.TryParse(targetStageInput.text, out result))
                {
                    if (StageDataManager.it.GetStageCount() <= 0)
                    {
                        mtm.Log("스테이지 데이터가 선택되지 않았습니다!", LogLevel.ERROR);
                    }
                    else
                    {
                        result = mtm.isDownCtrl ? result + 10 : result + 1;
                        if (result > StageDataManager.it.GetStageCount())
                        {
                            mtm.Log(StringHelper.Format("유효하지 않은 {0} 스테이지를 선택했습니다.", result));
                            return;
                        }

                        mtm.Log(StringHelper.Format("{0} 스테이지를 선택했습니다.", result));
                        mtm.selectedStageNum = result;
                        mtm.ShowStep();
                    }
                }
                break;
        }
    }

    public void Show()
    {
        Refresh();
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
