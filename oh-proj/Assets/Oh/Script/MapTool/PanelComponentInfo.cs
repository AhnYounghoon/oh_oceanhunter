﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelComponentInfo : STZBehaviour
{
    public int index = -1;
    public Text infoText;

    public void ShowIndexInfo(int inIndex, bool ignorePosX = false)
    {
        this.index = inIndex;
        infoText.text = string.Format("인덱스\n{0}", inIndex);

        /// NOTE @huni0417 스크롤과 화면 영역에서의 위치를 위한 인덱스 재계산
        inIndex -= BattleOceanController.SCROLL_X;
        inIndex -= BattleOceanController.SCROLL_Y * BattleOceanController.HORIZONTAL;

        int horIndex = inIndex % BattleOceanController.HORIZONTAL;
        int verIndex = inIndex / BattleOceanController.HORIZONTAL;

        if (horIndex >= BattleOceanController.REGULAR_SIZE_X)
            inIndex = BattleOceanController.REGULAR_SIZE_X - 1 + verIndex * BattleOceanController.HORIZONTAL;
        ///

        Vector3 position = Coordinate.ToPosition(inIndex);
        if (!ignorePosX)
            position.x += 70;
        position.z = this.transform.position.z;

        this.transform.position = position;
        this.gameObject.SetActive(true);
    }

    public void HideIndexInfo()
    {
        index = -1;
        this.gameObject.SetActive(false);
    }
}
