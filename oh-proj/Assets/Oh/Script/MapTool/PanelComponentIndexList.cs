﻿using UnityEngine;
using System.Collections.Generic;

public class PanelComponentIndexList : MonoBehaviour
{
    private List<PanelComponentInfo> indexList = new List<PanelComponentInfo>();

    bool isShow = false;
    public void Start()
    {
        for (int i = 0; i < BattleOceanController.REGULAR_SIZE_Y; i++)
        {
            for (int j = 0; j < BattleOceanController.REGULAR_SIZE_X; j++)
            {
                GameObject go = Instantiate<GameObject>(Resources.Load("MapTool/Info_Panel") as GameObject);
                go.transform.SetParent(this.transform);
                go.SetActive(false);
                indexList.Add(go.GetComponent<PanelComponentInfo>());
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (isShow)
                Hide();
            else
                Show();

            isShow = !isShow;
        }
        if (Input.GetMouseButtonUp(0) && isShow)
        {
            UpdateText();
        }
    }

    public void Hide()
    {
        for (int i = 0; i < indexList.Count; i++)
        {
            indexList[i].HideIndexInfo();
        }
    }

    public void UpdateText()
    {
        for (int i = 0; i < indexList.Count; i++)
        {
            int selectItemX = (i != 0) ? i % BattleOceanController.REGULAR_SIZE_X : 0;
            int selectItemY = (i != 0) ? i / BattleOceanController.REGULAR_SIZE_X : 0;
            int target_index = Coordinate.ToIdxNumber(selectItemX, selectItemY);
            if (selectItemX >= 0 && selectItemX <= BattleOceanController.HORIZONTAL && selectItemY >= 0 && selectItemY <= BattleOceanController.VERTICAL)
            {
                indexList[i].infoText.text = string.Format("{0}", target_index);
            }
        }
    }

    public void Show()
    {
        for (int i = 0; i< indexList.Count;i++)
        {
            int selectItemX = (i != 0) ? i % BattleOceanController.REGULAR_SIZE_X : 0;
            int selectItemY = (i != 0) ? i / BattleOceanController.REGULAR_SIZE_X : 0;
            int target_index = Coordinate.ToIdxNumber(selectItemX, selectItemY);
            if (selectItemX >= 0 && selectItemX <= BattleOceanController.HORIZONTAL && selectItemY >= 0 && selectItemY <= BattleOceanController.VERTICAL)
            {
                if (target_index != -1 && indexList[i].index != target_index)
                    indexList[i].ShowIndexInfo(target_index, true);
                indexList[i].infoText.text = string.Format("{0}", target_index);
            }
        }
    }

}
