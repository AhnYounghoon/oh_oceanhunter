﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ComponentButton : STZBehaviour
{
    public int type = 0;
    public int special_type = 0;
    public int hp = 0;
    public int shape = 0;
    public int rotate = 0;
    //public EDirection dir = EDirection.DIR_NONE;

    public Image image;
    public Button button;
    public Text txt;

    public void SetTileListener(MapToolManager mtm)
    {
        button.onClick.AddListener(() => mtm.OnSelectTile(this));
    }

    public void SetTokenListener(MapToolManager mtm)
    {
        button.onClick.AddListener(() => mtm.OnSelectToken(this));
    }

    public void Draw(string name)
    {
        Sprite sprite = TPResourceManager.it.GetSprite(name);
        if (sprite == null)
            return;

        Image image = button.GetComponent<Image>() as Image;
        image.sprite = sprite;
    }

    public void SetSprite(Sprite inSprite)
    {
        Image image = button.GetComponent<Image>() as Image;
        image.sprite = inSprite;
        image.Get<RectTransform>().sizeDelta = new Vector2(80, 80);/*inSprite.bounds.size*/;
    }

    Vector3 scale;
    public void Select()
    {
        scale = this.transform.localScale;
        scale.x = 1.3f;
        scale.y = 1.3f;
        this.transform.localScale = scale;
    }

    public void Unselect()
    {
        scale = this.transform.localScale;
        scale.x = 1.0f;
        scale.y = 1.0f;
        this.transform.localScale = scale;

        // 해제시 속성값 초기화
        hp = 0;
        rotate = 0;
        //dir = EDirection.DIR_NONE;
        //MapToolManager.OBJECT_ROTATE = 0;
    }
}