using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections.Generic;
using SimpleJSON;

public class TabPlayer : STZBehaviour
{
    [SerializeField]
    public MapToolManager mtm;
    [SerializeField]
    InputField shipIdInput;
    [SerializeField]
    InputField shipLvInput;

    [SerializeField]
    GameObject weaponTab;
    [SerializeField]
    List<InputField> weaponIdInput;
    [SerializeField]
    List<InputField> weaponLvInput;

    [SerializeField]
    GameObject characterTab;
    [SerializeField]
    List<InputField> characterIdInput;
    [SerializeField]
    List<InputField> characterLvInput;

    [SerializeField]
    ToggleGroup slotTab;
    [SerializeField]
    List<Toggle> slotToggle;

    private PlayerPlayData playerData;
    private int _slotIndex = 1;
    private LocalPlayerData localData;

    public void Init()
    {
        
    }

    public void Refresh()
    {
        if (localData == null)
            return;

        shipIdInput.text = localData.shipId.ToString();
        shipLvInput.text = localData.shipLv.ToString();

        if (localData.weaponId != null)
        {
            for (int i = 0; i < localData.weaponId.Count; i++)
            {
                weaponIdInput[i].text = localData.weaponId[i].ToString();
            }
        }

        if (localData.weaponLv != null)
        {
            for (int i = 0; i < localData.weaponLv.Count; i++)
            {
                weaponLvInput[i].text = localData.weaponLv[i].ToString();
            }
        }

        if (localData.characterId != null)
        {
            for (int i = 0; i < localData.characterId.Count; i++)
            {
                characterIdInput[i].text = localData.characterId[i].ToString();
            }
        }

        if (localData.characterLv != null)
        {
            for (int i = 0; i < localData.characterLv.Count; i++)
            {
                characterLvInput[i].text = localData.characterLv[i].ToString();
            }
        }
    }

    public void OnToggle(int inIndex)
    {
        _slotIndex = inIndex;
    }

    public void OnLoad()
    {
        string filePath;
#if UNITY_EDITOR
        filePath = Application.dataPath + "/Resources/Json/" + StringHelper.Format("PlayerData_{0}.txt", _slotIndex);
#else
        string fileName = string.Format("PlayerData_{0}.txt", _slotIndex);
        filePath = Application.dataPath + "/" + fileName;
#endif   

        if (!File.Exists(filePath))
        {
            mtm.Log(StringHelper.Format("{0} 데이터 로딩 실패!", filePath), LogLevel.ERROR);
            return;
        }

        JSONNode jsonNode = JSONNode.Parse(File.ReadAllText(filePath));

        localData = new LocalPlayerData();
        localData.Bind(jsonNode);

        Refresh();
    }

    public void OnDataApply()
    {
        SetLocalData();
        SetRealData();
    }

    public void SavePlayerData()
    {
        if (localData == null)
            return;

        string filePath;
#if UNITY_EDITOR
        filePath = Application.dataPath + "/Resources/Json/" + StringHelper.Format("PlayerData_{0}.txt", _slotIndex);
#else
        string fileName = string.Format("PlayerData_{0}.txt", _slotIndex);
        filePath = Application.dataPath + "/" + fileName;
#endif        

        JSONClass root = localData.Export();
        JsonFormatter format = new JsonFormatter(root.ToString());

        File.WriteAllText(filePath, format.Format());

        mtm.Log(StringHelper.Format("player 정보가 슬롯({0})에 저장되었습니다.", _slotIndex));
    }

    void SetLocalData()
    {
        localData = new LocalPlayerData();

        // 선박 정보
        {
            if (!shipIdInput.text.IsNullOrEmpty())
                localData.shipId = shipIdInput.text.IntValue();
            if (!shipLvInput.text.IsNullOrEmpty())
                localData.shipLv = shipLvInput.text.IntValue();
        }

        // 무기 정보
        {
            for (int i = 0; i < weaponIdInput.Count; i++)
            {
                if (localData.weaponId == null) localData.weaponId = new List<int>();
                if (!weaponIdInput[i].text.IsNullOrEmpty())
                    localData.weaponId.Add(weaponIdInput[i].text.IntValue());
            }

            for (int i = 0; i < weaponLvInput.Count; i++)
            {
                if (localData.weaponLv == null) localData.weaponLv = new List<int>();
                if (!weaponLvInput[i].text.IsNullOrEmpty())
                    localData.weaponLv.Add(weaponLvInput[i].text.IntValue());
            }
        }

        // 캐릭터 정보
        {
            for (int i = 0; i < characterIdInput.Count; i++)
            {
                if (localData.characterId == null) localData.characterId = new List<int>();
                if (!characterIdInput[i].text.IsNullOrEmpty())
                    localData.characterId.Add(characterIdInput[i].text.IntValue());
            }

            for (int i = 0; i < characterLvInput.Count; i++)
            {
                if (localData.characterLv == null) localData.characterLv = new List<int>();
                if (!characterLvInput[i].text.IsNullOrEmpty())
                    localData.characterLv.Add(characterLvInput[i].text.IntValue());
            }
        }
    }

    void SetRealData()
    {
        //playerData = new PlayerPlayData();

        //if (playerData != null)
        //{
        //    if (!shipIdInput.text.IsNullOrEmpty() && Statics.Instance.battle_object.ContainsKey(shipIdInput.text.IntValue()))
        //        playerData.BaseOn(Statics.Instance.battle_object[shipIdInput.text.IntValue()]);
        //}

        //var chList = playerData.GetCharacterIdList();
        //if (chList != null)
        //{
        //    for (int i = 0; i < chList.Count; i++)
        //    {
        //        if (!characterIdInput[i].text.IsNullOrEmpty() && Statics.Instance.character.ContainsKey(characterIdInput[i].text.IntValue()))
        //            playerData.character_id += characterIdInput[i].text + ",";
        //    }
        //}

        //var weaponList = playerData.GetWeaponIdList();
        //if (weaponList != null)
        //{
        //    for (int i = 0; i < weaponList.Count; i++)
        //    {
        //        if (!weaponIdInput[i].text.IsNullOrEmpty() && Statics.Instance.weapon.ContainsKey(weaponIdInput[i].text.IntValue()))
        //            playerData.weapon_id = weaponIdInput[i].text + ",";
        //    }
        //}
    }

    public void Show()
    {
        Refresh();
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}

[Serializable]
public class LocalPlayerData
{
    public int shipId;
    public int shipLv;

    public List<int> weaponId = null;
    public List<int> weaponLv = null;

    public List<int> characterId = null;
    public List<int> characterLv = null;

    public void Bind(JSONNode inNode)
    {
        shipId = inNode.ContainsKey("shipId") ? inNode["shipId"].AsInt : 0;
        shipLv = inNode.ContainsKey("shipLv") ? inNode["shipLv"].AsInt : 0;

        JSONArray array = null;

        if (inNode.ContainsKey("weaponId"))
        {
            weaponId = new List<int>();

            array = inNode["weaponId"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                weaponId.Add(array[i].AsInt);
            }
        }

        if (inNode.ContainsKey("weaponLv"))
        {
            weaponLv = new List<int>();

            array = inNode["weaponLv"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                weaponLv.Add(array[i].AsInt);
            }
        }

        if (inNode.ContainsKey("characterId"))
        {
            characterId = new List<int>();

            array = inNode["characterId"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                characterId.Add(array[i].AsInt);
            }
        }

        if (inNode.ContainsKey("characterLv"))
        {
            characterLv = new List<int>();

            array = inNode["characterLv"].AsArray;
            for (int i = 0; i < array.Count; i++)
            {
                characterLv.Add(array[i].AsInt);
            }
        }
    }

    public JSONClass Export()
    {
        JSONClass newOne = new JSONClass();
        JSONArray newArraySub = null;

        newOne.Add("shipId", shipId.ToString());
        newOne.Add("shipLv", shipLv.ToString());

        if (weaponId != null)
        {
            newArraySub = new JSONArray();
            for (int i = 0; i < weaponId.Count; i++)
                newArraySub.Add(weaponId[i].ToString());
            newOne.Add("weaponId", newArraySub);
        }

        if (weaponLv != null)
        {
            newArraySub = new JSONArray();
            for (int i = 0; i < weaponLv.Count; i++)
                newArraySub.Add(weaponLv[i].ToString());
            newOne.Add("weaponLv", newArraySub);
        }

        if (characterId != null)
        {
            newArraySub = new JSONArray();
            for (int i = 0; i < characterId.Count; i++)
                newArraySub.Add(characterId[i].ToString());
            newOne.Add("characterId", newArraySub);
        }

        if (characterLv != null)
        {
            newArraySub = new JSONArray();
            for (int i = 0; i < characterLv.Count; i++)
                newArraySub.Add(characterLv[i].ToString());
            newOne.Add("characterLv", newArraySub);
        }

        return newOne;
    }
}
