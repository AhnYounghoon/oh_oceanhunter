using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TabToken : STZBehaviour
{
    private const int TILE_GAP = 90;

    public MapToolManager mtm;
    public PanelToken token_panel;

    Vector3 startPos = new Vector3(-250.0f, 150.0f, 0.0f);

    public void Init()
    {
        Vector3 pos = startPos;

        ComponentButton selector = CreateButton(pos, "hand");
        selector.type = 99;
        pos.x += TILE_GAP;

        ComponentButton token = CreateButton(pos, "token");
        token.type = 1;
        token.txt.text = "토큰";
        pos.x += TILE_GAP;

        //token_panel.tokenMovingRouteAddButton.onClick.AddListener(token_panel.AddMovingRoute);
        //token_panel.tokenMovingRouteRemoveButton.onClick.AddListener(token_panel.RemoveMovingRoute);
    }

    public void Refresh()
    {
        token_panel.Refresh();
    }

    public void SetTokenPanel(bool inEnable)
    {
        token_panel.gameObject.SetActive(inEnable);
        if (!inEnable)
            token_panel.Clear();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    ComponentButton CreateButton(Vector3 pos, string name, Sprite inSprite = null)
    {
        GameObject obj = Instantiate(Resources.Load("MapTool/ComponentButton")) as GameObject;
        ComponentButton button = obj.GetComponent(typeof(ComponentButton)) as ComponentButton;
        button.name = name;
        button.transform.SetParent(this.transform);
        if (inSprite != null)
            button.SetSprite(inSprite);
        else
            button.Draw(name);
        button.SetTileListener(mtm);
        obj.transform.localPosition = pos;
        return button;
    }
}
