﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/// <summary>
/// 테이블 뷰 클래스.
/// </summary>

public class STZHorizontalTableView : STZBaseTableView 
{
	Vector2 _tempVec = Vector2.zero;

	protected override float currentContentLocation { get { return -_contentRect.anchoredPosition.x; } }
	protected override float spaceBetweenCells { get { return _cellSpace.y; } }
	protected override float spaceBetweenLines { get { return _cellSpace.x; } }
	protected override float currentCellsize { get { return _templateCell.width; } }
	protected override float contentSize { get { return _contentRect.sizeDelta.x; } }
	protected override float viewSize { get { return _rectTransform.sizeDelta.x; } }

	protected override void Awake ()
	{
		base.Awake ();

		gameObject.GetComponent<ScrollRect>().vertical = false;
		gameObject.GetComponent<ScrollRect>().horizontal = true;
	}

	public override Vector2 GetPosition(int inLineIdx, int cellIdx)
	{
		float initPosX = _frontBorder;

        float initPosY = _templateCell.height * _cellCountPerLine + spaceBetweenCells * (_cellCountPerLine - 1);
		initPosY = -initPosY * 0.5f;

		_tempVec.x = initPosX + inLineIdx * (_templateCell.width + spaceBetweenLines) + _templateCell.width * 0.5f;
		_tempVec.y = initPosY + cellIdx * (_templateCell.height + spaceBetweenCells) + _templateCell.height * 0.5f;

		return _tempVec;
	}

	protected override void ResetContentSize()
	{
		float width = Mathf.Max(viewSize, _lineCount * _templateCell.width + (_lineCount - 1) * spaceBetweenLines + _frontBorder + _backBorder);
		_contentRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
	}

	protected override void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell) {}

	public override void LocateCell(int inLineIndex, ERangeAnchor standardAnchor = ERangeAnchor.CENTER, float time = 0, System.Action inCallback = null)
	{
        _contentRect.DOKill(true);
        gameObject.GetComponent<ScrollRect>().inertia = true;
        _tempVec = GetPosition(inLineIndex, 0);

		float locationX = 0;
		switch (standardAnchor)
		{
		case ERangeAnchor.FRONT:
            locationX = _tempVec.x - currentCellsize * 0.5f + _frontBorder;
            break;

		case ERangeAnchor.CENTER:
			locationX = viewSize * 0.5f - _tempVec.x;
			break;

		case ERangeAnchor.BACK:
                locationX = viewSize - _tempVec.x + currentCellsize * 0.5f - _backBorder;
			break;
		}

        _tempVec.x = Mathf.Clamp(locationX, viewSize - contentSize, 0);
		_tempVec.y = _contentRect.anchoredPosition.y;

        if (time > 0)
        {
            gameObject.GetComponent<ScrollRect>().inertia = false;
            _contentRect.DOAnchorPosX(_tempVec.x, time).OnComplete(()=> { gameObject.GetComponent<ScrollRect>().inertia = true; if (inCallback != null) inCallback(); });
        }
        else
        {
            _contentRect.anchoredPosition = _tempVec;
            if (inCallback != null) inCallback();
        }
	}

    Color _tempColor;
    protected override void UpdateGradations()
    {
        base.UpdateGradations();

        if (_frontGradation != null)
        {
            _tempColor = _frontGradation.color;
            _tempColor.a = Mathf.Clamp(-currentContentLocation, 0f, 50f) / 50f;
            _frontGradation.color = _tempColor;
        }

        if (_backGradation != null)
        {
            _tempColor = _backGradation.color;
            _tempColor.a = Mathf.Clamp((contentSize - viewSize) + currentContentLocation, 0f, 50f) / 50f;
            _backGradation.color = _tempColor;
        }
    }
}
