﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/// <summary>
/// 테이블 뷰 클래스.
/// </summary>

public abstract class STZBaseTableView : MonoBehaviour 
{
	// 셀 정렬을 위해 anchor의 위치를 설정하기 위한 Enum.
	public enum ERangeAnchor
	{
		FRONT = 0,
		CENTER = 1,
		BACK = 2
	};

	[SerializeField]
	protected STZBaseTableViewCell _templateCell;

	[SerializeField]
	protected int _totalCellCount;

	[SerializeField]
	protected int _cellCountPerLine = 1;

	[SerializeField]
	protected Vector2 _cellSpace = Vector2.zero;

	[SerializeField]
	protected float _frontBorder;

	[SerializeField]
	protected float _backBorder;

    [SerializeField]
    protected Image _frontGradation;

    [SerializeField]
    protected Image _backGradation;

    protected int _lineCount;
	protected RectTransform _rectTransform;
	protected RectTransform _contentRect;
	protected Vector2 _animatedPosition = Vector2.zero;
	protected STZScrollBarExtention _scrollBar;

	protected List<STZBaseTableViewCell> _cellPool = new List<STZBaseTableViewCell>();
	protected Dictionary<int, List<STZBaseTableViewCell>>  _enabledCellDic = new Dictionary<int, List<STZBaseTableViewCell>>();

	protected abstract float currentContentLocation { get; }
	protected abstract float spaceBetweenCells { get; }
	protected abstract float spaceBetweenLines { get; }
	protected abstract float currentCellsize { get; }
	protected abstract float contentSize { get; }
	protected abstract float viewSize { get; }

	protected abstract void ResetContentSize();
	protected abstract void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell);

	public abstract void LocateCell(int targetCellIndex, ERangeAnchor standardAnchor = ERangeAnchor.CENTER, float time = 0, System.Action inCallback = null);

	public abstract Vector2 GetPosition(int inLineIdx, int CellIdx);

	/// <summary>
	/// 초기화.
	/// </summary>

	protected virtual void Awake()
	{
		_rectTransform = gameObject.GetComponent<RectTransform>();
		_contentRect = transform.Find("Viewport/Content").GetComponent<RectTransform>();

        if (_frontGradation != null)
        {
            _frontGradation.raycastTarget = false;
        }

        if (_backGradation != null)
        {
            _backGradation.raycastTarget = false;
        }

        //		_rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        //		_rectTransform.anchorMax = new Vector2(0.5f, 0.5f);

        SetTotalCellCount(Mathf.Max(0, _totalCellCount));
		SetCellCountInBlock(Mathf.Max(1, _cellCountPerLine));

		LocateCell(0);
	}

	public virtual void Refresh()
	{
		foreach (List<STZBaseTableViewCell> cellList in _enabledCellDic.Values)
		{
			for (int i = 0; i < cellList.Count; i++)
			{
				PushToPool(cellList[i]);
			}
		}

		_enabledCellDic.Clear();

		ResetContentSize();
		UpdateCellStatus();

		foreach (var cellList in _enabledCellDic)
		{
			foreach (var cell in cellList.Value)
				cell.OnReload();
		}

		Update();
	}

	/// <summary>
	/// 업데이트.
	/// </summary>
	private Vector2 _contentPosition = Vector2.zero;
	protected virtual void Update()
	{
		UpdateCellStatus();

		if (_contentRect.anchoredPosition != _contentPosition && _scrollBar != null)
		{
			_scrollBar.Show();
		}

		_contentPosition = _contentRect.anchoredPosition;
		DoDisablePool();

        UpdateGradations();
	}

	/// <summary>
	/// 테이블 셀의 전체 개수를 지정하는 함수.
	/// </summary>
	/// <param name="totalCellCount">전체 셀의 개수.</param>

	public void SetTotalCellCount(int totalCellCount)
	{
		_totalCellCount = totalCellCount;

		_lineCount = _totalCellCount / _cellCountPerLine;
		if (_lineCount * _cellCountPerLine < _totalCellCount)
		{
			_lineCount += 1;			
		}

		if (_templateCell == null)
		{
			return;
		}

		ResetContentSize();
	}

	/// <summary>
	/// 블럭당 셀의 개수를 지정하는 함수.
	/// </summary>
	/// <param name="cellCountInBlock">블럭당 셀의 개수</param>

	public void SetCellCountInBlock(int cellCountInBlock)
	{
		_cellCountPerLine = cellCountInBlock;
		if (_templateCell == null)
		{
			return;
		}

		ResetContentSize();
	}

	/// <summary>
	/// 테이블 셀을 생성하는 함수.
	/// </summary>
	/// <param name="idx">테이블 셀이 생성될 위치의 index</param>
	/// <returns></returns>

	protected virtual List<STZBaseTableViewCell> CreateCellList(int idx)
	{
		List<STZBaseTableViewCell> cellList = new List<STZBaseTableViewCell>();

		int count = _cellCountPerLine;
		if (idx == _lineCount - 1)
		{
			count = _totalCellCount % _cellCountPerLine;
			if (count == 0)
				count = _cellCountPerLine;
		}

		Transform tr = _contentRect.transform;
		GameObject cellGameObj = _templateCell.gameObject;

		for (int i = 0; i < count; i++)
		{
			int index = idx * _lineCount + i;

			STZBaseTableViewCell cell = PopFromPool(index);
			if (cell == null)
			{
				cell = (Instantiate(cellGameObj) as GameObject).GetComponent<STZBaseTableViewCell>();
				cell.transform.SetParent(tr, false);
				cell.gameObject.SetActive(true);
			}

			cell.cellIndex = index;
            cell.cellParent = this;
			cellList.Add(cell);
			ResetCell(idx, i, ref cell);
            cell.cellParent = this;
		}

		return cellList;
	}

	private void PushToPool(STZBaseTableViewCell cell)
	{
		cell.OnRecycle();

		for (int i = 0; i < _cellPool.Count; i++)
		{
			if (_cellPool[i] == null)
			{
				_cellPool[i] = cell;
				return;
			}
		}

		_cellPool.Add(cell);
	}

	private STZBaseTableViewCell PopFromPool(int index)
	{
		for (int i = 0; i < _cellPool.Count; i++)
		{
			if (_cellPool[i] != null 
				&& _cellPool[i].cellIndex == index)
			{
				STZBaseTableViewCell cell = _cellPool[i];
				_cellPool[i] = null;

				if (!cell.gameObject.activeSelf)
					cell.gameObject.SetActive(true);

				return cell;
			}
		}

		for (int i = 0; i < _cellPool.Count; i++)
		{
			if (_cellPool[i] != null)
			{
				STZBaseTableViewCell cell = _cellPool[i];
				_cellPool[i] = null;

				if (!cell.gameObject.activeSelf)
					cell.gameObject.SetActive(true);

				return cell;
			}
		}

		return null;
	}

	private void DoDisablePool()
	{
		for (int i = 0; i < _cellPool.Count; i++)
		{
			if (_cellPool[i] != null)
			{
				_cellPool[i].gameObject.SetActive(false);
			}
		}
	}

	protected void UpdateCellStatus()
	{
		for (int i = 0 ; i < _lineCount ; i++)
		{
			float cellFrontPos = currentCellsize * i + (i * spaceBetweenLines) + _frontBorder;
			float cellBackPos = cellFrontPos + currentCellsize;

			if (currentContentLocation < cellBackPos && currentContentLocation  + viewSize > cellFrontPos)
			{
                if (_enabledCellDic.ContainsKey(i))
                {
                    continue;
                }
				List<STZBaseTableViewCell> cellList = CreateCellList(i);
				for (int j = 0; j < cellList.Count; j++)
				{
					RectTransform rt = cellList[j].GetComponent<RectTransform>();
					rt.anchoredPosition = GetPosition(i, j);
                }
                if (!_enabledCellDic.ContainsKey(i))
                {
                    _enabledCellDic.Add(i, cellList);
                }
			}
			else
			{
				if (_enabledCellDic.ContainsKey(i))
				{
					for (int j = 0; j < _enabledCellDic[i].Count; j++)
					{
						PushToPool(_enabledCellDic[i][j]);
					}
					_enabledCellDic.Remove(i);
				}
			}
		}
	}

    protected virtual void UpdateGradations()
    {

    }
}
