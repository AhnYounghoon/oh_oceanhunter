﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class STZScrollBarHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField]
    float _hanledAlphaValuePerSec = 5f;

    [SerializeField]
    float _maxAlpha = 1f;

    [SerializeField]
    float _minAlpha = 0;

    [SerializeField]
    float _waitingTime = 0.2f;

    [SerializeField]
    List<CanvasGroup> _scrollBars;

    float _currentAlpha;
    float _curWaitingTime;

    Vector2 _curContentPos;

    void Awake()
    {
        if (_scrollBars.Count == 0)
        {
            Scrollbar[] list = new Scrollbar[] { GetComponent<ScrollRect>().verticalScrollbar, GetComponent<ScrollRect>().horizontalScrollbar };

            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] == null)
                    continue;

                _scrollBars.Add(list[i].GetOrAddComponent<CanvasGroup>());
                _scrollBars[i].alpha = _minAlpha;
            }
        }
        else
        {
            for (int i = 0, count = _scrollBars.Count; i < count; i++)
            {
                _scrollBars[i].alpha = _minAlpha;
            }
        }

        _currentAlpha = _minAlpha;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _curWaitingTime = _waitingTime;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _curWaitingTime = 0;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _curWaitingTime = _waitingTime;
    }

    void Update()
    {
        UpdateDrag();
    }

    void UpdateDrag()
    {
        if (_curWaitingTime > 0 && _currentAlpha < _maxAlpha)
        {
            _currentAlpha = Mathf.Min(_currentAlpha + _hanledAlphaValuePerSec * Time.smoothDeltaTime, _maxAlpha);
            for (int i = 0; i < _scrollBars.Count; i++)
            {
                if (_scrollBars[i] == null)
                    continue;

                _scrollBars[i].alpha = _currentAlpha;
            }
        }

        if (_currentAlpha == _maxAlpha)
        {
            _curWaitingTime -= Time.smoothDeltaTime;
        }

        if (_curWaitingTime <= 0 && _currentAlpha > _minAlpha)
        {
            _currentAlpha = Mathf.Max(_currentAlpha - _hanledAlphaValuePerSec * Time.smoothDeltaTime, _minAlpha);
            for (int i = 0; i < _scrollBars.Count; i++)
            {
                if (_scrollBars[i] == null)
                    continue;

                _scrollBars[i].alpha = _currentAlpha;
            }
        }
    }
}
