﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 테이블 뷰 셀 클래스.
/// </summary>

public class STZHorizontalTableViewCell : STZBaseTableViewCell 
{
    protected override void ResetAnchor()
    {
        base.ResetAnchor();
        rectTransfrom.anchorMin = new Vector2(0, 0.5f);
        rectTransfrom.anchorMax = new Vector2(0, 0.5f);
        rectTransfrom.pivot = new Vector2(0.5f, 0.5f);
    }
}
