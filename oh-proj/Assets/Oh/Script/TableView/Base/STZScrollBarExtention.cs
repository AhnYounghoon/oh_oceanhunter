﻿using UnityEngine;
using System.Collections;

public class STZScrollBarExtention : STZBehaviour {
    protected float _showTime = 0.1f;

    protected bool          _animated           = false;
    protected float         _currentShowTime    = 0;
    protected CanvasGroup   _canvasGroup;

    // Use this for initialization
    public void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Show()
    {
        _currentShowTime = _showTime;
        _animated = true;
    }

    void Update()
    {
        _currentShowTime -= Time.smoothDeltaTime;

        if (_currentShowTime <= 0 && gameObject.activeSelf)
        {
            _animated = false;
        }

        _canvasGroup.alpha = Mathf.Clamp01(_canvasGroup.alpha + (_animated ? 0.1f : -0.1f));
    }
}
