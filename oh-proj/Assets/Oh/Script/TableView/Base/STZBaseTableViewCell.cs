﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 테이블 뷰 셀 클래스.
/// </summary>

public class STZBaseTableViewCell : STZBehaviour 
{
    public delegate void Callback();
    public Callback Refresh;

	protected RectTransform rectTransfrom { get { return GetComponent<RectTransform>(); } }
    public int cellIndex { get; set; }

	public STZBaseTableView cellParent { get; set; }

	Vector2 _tempVec = Vector2.zero;

	public float width { get { return rectTransfrom.sizeDelta.x * rectTransfrom.localScale.x; } }
	public float height { get { return rectTransfrom.sizeDelta.y * rectTransfrom.localScale.y; } }

    /// <summary>
    /// Awake.
    /// </summary>

    protected virtual void Awake()
    {
        ResetAnchor();
        Initialze();
    }

    protected virtual void ResetAnchor()
    {

    }

    /// <summary>
    /// 초기화.
    /// </summary>

    public virtual void Initialze()
    {
        
    }

    /// <summary>
    /// 리로드 시 호출
    /// </summary>

    public virtual void OnReload()
    {
    }

    /// <summary>
    /// 대기 풀에 들어갈 때 호출
    /// </summary>

    public virtual void OnRecycle()
    {

    }
}
