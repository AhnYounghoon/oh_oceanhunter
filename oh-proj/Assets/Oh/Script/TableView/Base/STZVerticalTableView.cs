﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/// <summary>
/// 테이블 뷰 클래스.
/// </summary>

public class STZVerticalTableView : STZBaseTableView 
{
	Vector2 _tempVec = Vector2.zero;

	protected override float currentContentLocation { get { return _contentRect.anchoredPosition.y; } }
	protected override float spaceBetweenCells { get { return _cellSpace.x; } }
	protected override float spaceBetweenLines { get { return _cellSpace.y; } }
	protected override float currentCellsize { get { return _templateCell.height; } }
	protected override float contentSize { get { return _contentRect.sizeDelta.y; } }
	protected override float viewSize { get { return _rectTransform.sizeDelta.y; } }

	protected override void Awake ()
	{
		base.Awake ();

		gameObject.GetComponent<ScrollRect>().vertical = true;
		gameObject.GetComponent<ScrollRect>().horizontal = false;
	}

	public override Vector2 GetPosition(int inLineIdx, int cellIdx)
	{
		float initPosX = _templateCell.width * _cellCountPerLine + spaceBetweenCells * (_cellCountPerLine - 1);
		initPosX = -initPosX * 0.5f;

		float initPosY = -_frontBorder;

		_tempVec.x = initPosX + cellIdx * (_templateCell.width + spaceBetweenCells) + _templateCell.width * 0.5f;
		_tempVec.y = initPosY - inLineIdx * (_templateCell.height + spaceBetweenLines) - _templateCell.height * 0.5f;

		return _tempVec;
	}

	protected override void ResetContentSize()
	{
		float height = Mathf.Max(viewSize, _lineCount * _templateCell.height + (_lineCount - 1) * spaceBetweenLines + _frontBorder + _backBorder);
		_contentRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
	}

	protected override void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell) {}

	public override void LocateCell(int inLineIndex, ERangeAnchor standardAnchor = ERangeAnchor.CENTER, float time = 0, System.Action inCallback = null)
	{
        _contentRect.DOKill(true);
        gameObject.GetComponent<ScrollRect>().inertia = true;
        _tempVec = GetPosition(inLineIndex, 0);

		float locationY = 0;
		switch (standardAnchor)
		{
		case ERangeAnchor.FRONT:
                locationY = -_tempVec.y - currentCellsize * 0.5f - _frontBorder;
            break;

		case ERangeAnchor.CENTER:
			locationY = - viewSize * 0.5f - _tempVec.y;
			break;

		case ERangeAnchor.BACK:
                locationY = -viewSize - _tempVec.y + currentCellsize * 0.5f + _backBorder;
			break;
		}

		_tempVec.x = _contentRect.anchoredPosition.x;
        _tempVec.y = Mathf.Clamp(locationY, 0, contentSize - viewSize);

		_contentRect.anchoredPosition = _tempVec;

        if (time > 0)
        {
            gameObject.GetComponent<ScrollRect>().inertia = false;
            _contentRect.DOAnchorPosY(_tempVec.y, time).OnComplete(() => { gameObject.GetComponent<ScrollRect>().inertia = true; if (inCallback != null) inCallback(); });
        }
        else
        {
            _contentRect.anchoredPosition = _tempVec;
            if (inCallback != null) inCallback();
        }
    }

    Color _tempColor;
    protected override void UpdateGradations()
    {
        base.UpdateGradations();

        if (_frontGradation != null)
        {
            _tempColor = _frontGradation.color;
            _tempColor.a = Mathf.Clamp(currentContentLocation, 0f, 50f) / 50f;
            _frontGradation.color = _tempColor;
        }

        if (_backGradation != null)
        {
            _tempColor = _backGradation.color;
            _tempColor.a = Mathf.Clamp((contentSize - viewSize) - currentContentLocation, 0f, 50f) / 50f;
            _backGradation.color = _tempColor;
        }
    }
}
