﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponTableView : STZVerticalTableView
{
    //List<ServerStatic.Weapon>   _baseWeaponList;
    //ServerStatic.Weapon[]       _weaponDataArr;

    //public System.Action<ServerStatic.Weapon> SelectCallback { get; set; }

    public int equipmentWeaponId
    {
        set
        {
            _equipmentWeaponId = value;
            Refresh();
        }
    }

    private int _equipmentWeaponId;
    private int _selectedWeaponId;

    //public void Init(List<ServerStatic.Weapon> inWeaponList)
    //{
    //    this.Get<ScrollRect>().movementType = ScrollRect.MovementType.Clamped;
    //    this.Get<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

    //    _baseWeaponList     = inWeaponList;
    //    _selectedWeaponId   = 0;
    //    _equipmentWeaponId  = 0;
    //}

    /// <summary>
    /// 무기를 선택
    /// </summary>
    /// <param name="inData"></param>
    //public void OnSelect(ServerStatic.Weapon inData)
    //{
    //    _selectedWeaponId = inData.id;
    //    Refresh();

    //    //if (SelectCallback != null)
    //    //{
    //    //    SelectCallback(inData);
    //    //}
    //}

    /// <summary>
    /// 셀을 정렬한다
    /// </summary>
    /// <param name="inActionType"></param>
    /// <param name="inSortType"></param>
    public void SetCellsBySortingType(SortController.EActionType inActionType, SortController.ESortType inSortType)
    {
        //Debug.Assert(_baseWeaponList != null);
        ////_weaponDataArr = SortController.SortWeaponList(_baseWeaponList, inActionType, inSortType);
        //Debug.Assert(_weaponDataArr != null);

        //SetTotalCellCount(_weaponDataArr.Length);
        Refresh();
    }

    /// <summary>
    /// 셀을 데이터 기반으로 셋팅
    /// </summary>
    /// <param name="majorIndex"></param>
    /// <param name="minorIndex"></param>
    /// <param name="cell"></param>
    protected override void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell)
    {
        //int index = majorIndex * _cellCountPerLine + minorIndex;
        //if (cell is WeaponTableViewCell && _weaponDataArr != null)
        //{
        //    ServerStatic.Weapon data = _weaponDataArr[index];
        //    if (data != null)
        //    {
        //        WeaponTableViewCell weaponCell = (cell as WeaponTableViewCell);
        //        weaponCell.selectCallback = OnSelect; 
        //        weaponCell.SetEquipmentUI(data.id == _equipmentWeaponId);
        //        weaponCell.SetSelectUI(data.id == _selectedWeaponId);
        //        weaponCell.Reset(data);
        //    }
        //}
    }
}
