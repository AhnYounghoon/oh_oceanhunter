﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WeaponTableViewCell : STZVerticalTableViewCell
{
    [SerializeField]
    WeaponUI _weaponUI;

    [SerializeField]
    GameObject _selectedUI;

    [SerializeField]
    GameObject _equipmentUI;

    //public System.Action<ServerStatic.Weapon> selectCallback { private get; set; }

    //ServerStatic.Weapon _data;

    //public void Reset(ServerStatic.Weapon inData)
    //{
    //    _data = inData;
    //    //_weaponUI.Refresh(_data);
    //}

    public void SetSelectUI(bool b)
    {
        _selectedUI.SetActive(b);
    }

    public void SetEquipmentUI(bool b)
    {
        _equipmentUI.SetActive(b);
    }

    public void OnSelect()
    {
        //if (selectCallback != null)
        //{
        //    selectCallback(_data);
        //}
    }
}
