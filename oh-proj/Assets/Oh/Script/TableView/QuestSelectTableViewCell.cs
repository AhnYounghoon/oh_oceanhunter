﻿using UnityEngine;
using UnityEngine.UI;
using STZFramework;
using System.Collections.Generic;

public class QuestSelectTableViewCell : STZVerticalTableViewCell
{
    [SerializeField]
    Image imgIcon;

    [SerializeField]
    Text txtTitle;

    [SerializeField]
    Text txtDesc;

    [SerializeField]
    Image imgPlay;

    private ServerStatic.Quest data = null;

    public void Reset(ServerStatic.Quest inData)
    {
        data = inData;

        txtTitle.text = inData.title;
        FileMap questFM = GeneralDataManager.it.GetQuestData();

        if (questFM == null)
            return;

        int currCount = questFM.Get<int>(data.id.ToString());

        // 진행중일때는 개수 표시 및 버튼 동작 on
        if (currCount == 0)
        {
            txtDesc.text = StringHelper.Format("{0} ({1}/{2})", inData.desc_2, currCount, inData.count);
            this.Get<Button>().enabled = true;
        }
        // 획득하였을때는 개수 표시x 및 버튼 동작 off
        else
        {
            txtDesc.text = StringHelper.Format("{0}", inData.desc_1);
            this.Get<Button>().enabled = false;
        }
    }

    public void OnSelect()
    {
        PopupQuest.Show(null, data);
    }
}
