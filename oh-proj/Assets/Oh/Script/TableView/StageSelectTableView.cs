﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class StageSelectTableView : STZVerticalTableView
{
    public List<ServerStatic.SailingCatogory> cellDataList = null;

    /// <summary>
    ///
    /// </summary>  
    public void Reset(int inEpisodeId)
    {
        cellDataList = ServerStatic.SailingCatogory.GetListByEpisodeId(inEpisodeId);
        if (cellDataList != null)
            SetTotalCellCount(cellDataList.Count);

        Refresh();
    }

    /// <summary>
    /// 셀 정보 업데이트
    /// </summary>
    /// <param name="majorIndex"></param>
    /// <param name="minorIndex"></param>
    /// <param name="cell"></param>
    protected override void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell)
    {
        int index = majorIndex * _cellCountPerLine + minorIndex;

        StageSelectTableViewCell temp = cell as StageSelectTableViewCell;
        if (cellDataList.Count > 0)
        {
            temp.gameObject.SetActive(true);
            temp.Reset(cellDataList[index]);
        }
    }
}

