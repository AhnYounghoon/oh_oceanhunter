﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class CharacterTableViewCell : STZVerticalTableViewCell, IPointerDownHandler
{
    [SerializeField]
    Sprite[] imgTypeSprite;

    [SerializeField]
    Image imgCharacter;

    [SerializeField]
    Text txtCount;

    [SerializeField]
    Text txtName;

    [SerializeField]
    Text txtLevel;

    [SerializeField]
    Text txtHP;

    [SerializeField]
    Image imgType;

    [SerializeField]
    Button button;

    [SerializeField]
    StarSet starSet;

    [SerializeField]
    GaugeBar lvGauge;

    [SerializeField]
    GaugeBar OverlvGauge;

    [SerializeField]
    GameObject _upgradebuttonList;

    public Action<int> pointerDownCallback { get; set; }

    protected ServerStatic.Character  _data = null;
    protected DragObject              _drag = null;

    bool _isUpgrade = false;

    public virtual void Reset(ServerStatic.Character inData, DragObject inDrag = null, int inHaveCount = 0)
    {
        _data = inData;
        _drag = inDrag;

        if (inData != null)
        {
            if (starSet != null)
                starSet.AwakeStars(_data.star);

            if (imgCharacter != null)
                imgCharacter.sprite = Resources.Load<Sprite>(_data.icon_path);

            if (txtName != null)
                txtName.text = _data.name;

            if (txtHP != null)
                txtHP.text = StringHelper.Format("HP {0}/{0}", _data.hp.ToString());

            if (imgType != null)
            {
                imgType.sprite = imgTypeSprite[(int)Enum.Parse(typeof(ECharacterType), _data.type, true)];
            }

            CharacterLevelInfo personLevelInfo = GeneralDataManager.it.GetPersonLevelInfo(inData.category, inData.id);
            int personExp = personLevelInfo.exp;
            int personLevel = personLevelInfo.level;
            //int currLevel = GeneralDataManager.it.GetPersonLevel(personExp);
            int maxExp = personLevel * 10;
            //int afterUpgradeExp = GeneralDataManager.it.GetExpByLevel(personExp);

            // 레벨 표시
            txtLevel.text = StringHelper.Format("Lv{0}", personLevel);

            // 레벨 최대치보다 더 많은 경우
            if (personExp >= maxExp)
            {
                _isUpgrade = true;

                if (OverlvGauge != null)
                {
                    OverlvGauge.gameObject.SetActive(true);
                    OverlvGauge.SetGauge(1, false, StringHelper.Format("{0}/{1}", personExp, maxExp));
                }

                if (lvGauge != null)
                    lvGauge.gameObject.SetActive(false);
            }
            else
            {
                _isUpgrade = false;

                if (lvGauge != null)
                {
                    lvGauge.gameObject.SetActive(true);
                    lvGauge.SetGauge((float)personExp / (float)maxExp, false, StringHelper.Format("{0}/{1}", personExp, maxExp));
                }

                if (OverlvGauge != null)
                    OverlvGauge.gameObject.SetActive(false);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_upgradebuttonList == null)
            return;

        _upgradebuttonList.gameObject.SetActive(!_upgradebuttonList.activeSelf);

        if (pointerDownCallback != null)
        {
            pointerDownCallback(_data.id);
        }
        if (_drag != null)
        {
            _drag.StartDrag(imgCharacter.sprite, STZCommon.Hash("character_data", _data));
        }
    }

    public void OnUpgrade()
    {
        if (!_isUpgrade)
            return;

        PopupCharacterUpgrade.Show(null, _data, true);
    }

    public void OnConfirm()
    {
        _upgradebuttonList.SetActive(false);
    }
}

[Serializable]
public class CharacterLevelInfo
{
    public int level;
    public int exp;

    public CharacterLevelInfo(int inLevel, int inExp)
    {
        level = inLevel;
        exp = inExp;
    }
}