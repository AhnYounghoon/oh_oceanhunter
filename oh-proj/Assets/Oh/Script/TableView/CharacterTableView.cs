﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharacterTableView : STZVerticalTableView
{
    [SerializeField]
    DragObject dragController;

    public System.Action<int> OnTouchEvent { get; set; }

    private List<ServerStatic.Character> characterList = new List<ServerStatic.Character>();
    private Dictionary<int, CharacterLevelInfo> haveCharacterDic = new Dictionary<int, CharacterLevelInfo>();

    public void Init(List<ServerStatic.Character> inCharacterList)
    {
        this.Get<ScrollRect>().movementType = ScrollRect.MovementType.Clamped;
        this.Get<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        this.characterList = inCharacterList;
        this.haveCharacterDic = GeneralDataManager.it.GetAllPersionDictionary();

        if (characterList != null)
            SetTotalCellCount(characterList.Count);

        Refresh();
    }

    protected override void ResetCell(int majorIndex, int minorIndex, ref STZBaseTableViewCell cell)
    {
        int index = majorIndex * _cellCountPerLine + minorIndex;
        if (cell is CharacterTableViewCell)
        {
            ServerStatic.Character data = characterList.ElementAtOrDefault(index);
            if (data != null)
            {
                CharacterTableViewCell characterCell = (cell as CharacterTableViewCell);
                characterCell.Reset(data, dragController);
                characterCell.pointerDownCallback = OnTouch;
            }
        }
    }

    private void OnTouch(int id)
    {
        if (OnTouchEvent != null)
        {
            OnTouchEvent(id);
        }
    }
}
