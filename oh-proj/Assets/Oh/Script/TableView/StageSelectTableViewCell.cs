﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class StageSelectTableViewCell : STZVerticalTableViewCell
{
    [SerializeField]
    Image imgIcon;

    [SerializeField]
    Text txtStageId;

    [SerializeField]
    Text txtStageTag;

    [SerializeField]
    StampAnimation stamp;

    [SerializeField]
    Image[] rewards;

    private ServerStatic.SailingCatogory data = null;

    public void Reset(ServerStatic.SailingCatogory inData)
    {
        data = inData;

        //txtStageId.text = StringHelper.Format("{0}-{1}", data.episode_id, data.stage_id);
        txtStageId.text = data.episode_desc;
        //imgIcon.sprite = Resources.Load<Sprite>(inData.resource_path);

        if (data.stage_id <= GeneralDataManager.it.GetStageClearData())
            stamp.ShowAsPause();

        RewardData[] parsedReward = RewardData.ParseItemIDs(data.clear_reward);
        for (int i = 0; i < rewards.Length; i++)
        {
            rewards[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < parsedReward.Length; i++)
        {
            rewards[i].gameObject.SetActive(true);
            rewards[i].sprite = Resources.Load<Sprite>(parsedReward[i].iconPath);
        }
    }

    public void OnSelect()
    {
        if (data.stage_id > GeneralDataManager.it.GetStageClearData() + 1)
        {
            return;
        }

        if (StageDataManager.it.SelectStage(data.stage_id) == null)
        {
            SimplePopup.ForceConfirm(StringHelper.Format("해당 ({0})스테이지 데이터가 없습니다.", data.stage_id));
            return;
        }

        PopupStageInfo.Show(null, data);
    }
}
