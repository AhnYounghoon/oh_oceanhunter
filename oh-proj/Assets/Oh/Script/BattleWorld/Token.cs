﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class Token : STZBehaviour
{
    public delegate void Callback(Hashtable inParam);

    [SerializeField]
    public Image objImage;

    public SkeletonAnimation spine_object { get; set; }
    private BoxCollider2D _boxCollider;
    
    public int Type { get; set; }
    public int Index { get; set; }
    public TokenData Data { get; set; }
    private bool isMove = false;

    /// <summary>
    /// 바닥 오브젝트 생성
    /// </summary>
    /// <param name="inData"></param>
    public void DrawTokenObject(TokenData inData)
    {
        Index = inData.index;
        objImage.sprite = TPResourceManager.it.GetSprite("token");
    }

    public bool Move(Callback inCallback)
    {
        if (isMove)
        {
            inCallback(null);
            inCallback = null;
            return false;
        }

        if (Data.moving_path == null)
        {
            inCallback(null);
            inCallback = null;
            return false;
        }

        int currIndex = Data.moving_path.IndexOf(Index);

        isMove = true;
        int targetIndex = currIndex + 1;
        // 패스끝까지 이동하면 처음인덱스로 이동하는 순환 구조
        if (currIndex == Data.moving_path.Count - 1)
            targetIndex = 0;

        int nextPosition = Data.moving_path[targetIndex];
        Vector2 from = this.transform.localPosition;
        Vector2 to = Coordinate.ToPosition(nextPosition);
        this.transform.DOMove(to, 0.75f).OnComplete(() =>
        {
            Index = nextPosition;
            isMove = false;
            if (inCallback != null)
            {
                inCallback(STZCommon.Hash(EMovingEvent.SUCCESS, Index));
                inCallback = null;
                ObserverManager.it.Dispatch(EObserverEventKey.TOKEN_MOVED, null);
            }
        });

        return true;
    }

    public static Token GetObject()
    {
        Token board;
        GameObject boardObj = Instantiate(Resources.Load("Prefabs/BattleOcean/Token")) as GameObject;
        board = boardObj.GetComponent<Token>();

        return board;
    }

    public void Release()
    {
        this.gameObject.SetActive(false);
        objImage.enabled = false;

        //this.transform.parent = null;

        Destroy(this.gameObject);
    }
}