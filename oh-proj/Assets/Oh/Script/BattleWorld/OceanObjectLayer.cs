using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using STZFramework;

public class OceanObjectLayer : STZBehaviour
{
    public BattleOceanController BOController { get; set; }

    public RectTransform seaRouteTransform;
    RectTransform tileTransform;
    RectTransform tokenTransform;

    private List<Tile> _tiles;
    private List<Token> _tokens;
    private List<int> _totalCarpetList;

    public List<Tile> Tiles { get { return _tiles; } }
    public List<Token> Tokens { get { return _tokens; } }

    private List<GameObject> seaRouteBuckets = new List<GameObject>();
    private Dictionary<string, Vector3> _effectDic;
    int tokenMoveCount = 0;

    void Awake()
    {
        if (seaRouteTransform == null)
            seaRouteTransform = this.Get<RectTransform>("SeaRouteArea");

        if (tileTransform == null)
            tileTransform = this.Get<RectTransform>("TileArea");

        if (tokenTransform == null)
            tokenTransform = this.Get<RectTransform>("TokenArea");
    }

    public void Clear()
    {
        if (_tiles != null)
        {
            for (int i = 0; i < _tiles.Count; i++)
            {
                if (_tiles[i] != null)
                    _tiles[i].Release();
            }

            _tiles = null;
        }

        if (_tokens != null)
        {
            for (int i = 0; i < _tokens.Count; i++)
            {
                if (_tokens[i] != null)
                    _tokens[i].Release();
            }

            _tokens = null;
        }

        RemoveSeaRouteConnections();

        if (_effectDic != null)
        {
            _effectDic.Clear();
            _effectDic = null;
        }

        if (_totalCarpetList != null)
        {
            _totalCarpetList.Clear();
            _totalCarpetList = null;
        }
    }

    /// <summary>
    /// 오브젝트들 생성
    /// </summary>
    public void CreateOceanObjects()
    {
        for (int index = 0; index < BattleOceanController.TILE_TOTAL; index++)
        {
            List<TileData> tileDatas = StageDataManager.it.GetTileDatas(index);

            if (tileDatas != null && tileDatas.Count > 0)
            {
                for (int i = 0; i < tileDatas.Count; i++)
                {
                    /* REMOVE @huni0417 static 참조 안하도록 수정
                    if (Statics.Instance.sailing_info != null)
                    {
                        List<ServerStatic.SailingInfo> si = ServerStatic.SailingInfo.GetListById(tileDatas[i].id);
                        ServerStatic.SailingInfo targetInfo = null;

                        // 2개 이상일때 확률에 의해 선택
                        if (si.Count > 1)
                        {
                            int sum = 0;
                            int random = Random.Range(0, 10000) + 1;
                            foreach (ServerStatic.SailingInfo o in si)
                            {
                                sum += o.rate;
                                if (random <= sum)
                                {
                                    targetInfo = o;
                                    break;
                                }
                            }
                        }
                        else
                            targetInfo = si[0];

                        int result;
                        tileDatas[i].event_amount = targetInfo.amount;
                        tileDatas[i].type = targetInfo.tile_type;
                        if (int.TryParse(targetInfo.event_type, out result))
                        {
                            tileDatas[i].event_type = result;
                            tileDatas[i].shape = result;
                        }

                        CreateTileHasData(tileDatas[i]);
                    }
                    else
                    //*/
                        CreateTileHasData(tileDatas[i]);
                }
            }

            List<TokenData> tokenDatas = StageDataManager.it.GetTokenDatas(index);

            if (tokenDatas != null && tokenDatas.Count > 0)
            {
                for (int i = 0; i < tokenDatas.Count; i++)
                {
                    // 인게임 전투에서 이긴 토큰은 생성 제외
                    //if (GeneralDataManager.it.GetLastBattleWin() && tokenDatas[i].index == GeneralDataManager.it.tokenIndex)
                    //    continue;

                    CreateTokenHasData(tokenDatas[i]);
                }
            }
        }

        CreateSeaRouteConnections();
    }

    public void CreateTileHasData(TileData inInfo)
    {
        TileData tileData = inInfo;

        if (_tiles == null)
            _tiles = new List<Tile>(BattleOceanController.TILE_TOTAL);

        Tile tileObj = Tile.GetObject();
        tileObj.Data = tileData;
        tileObj.DrawTileObject(tileData);
#if UNITY_EDITOR
        tileObj.SetTileIdMark(inInfo.id);
#else
        tileObj.idMark.SetActive(false);
#endif

        Vector3 pos = new Vector3(0, 0, 0);
        pos.x = BattleOceanController.START_POS_X + (tileData.index % BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_WIDTH;
        pos.y = BattleOceanController.START_POS_Y + (tileData.index / BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_HEIGHT;

        tileObj.name = StringHelper.Format("Tile({0}_{1})", tileData.index, tileData.type);
        tileObj.transform.SetParent(tileTransform);
        tileObj.transform.localPosition = pos;

        _tiles.Add(tileObj);
    }

    public void CreateTokenHasData(TokenData inInfo)
    {
        TokenData tokenData = inInfo;

        if (_tokens == null)
            _tokens = new List<Token>(BattleOceanController.TILE_TOTAL);

        Token tokenObj = Token.GetObject();
        tokenObj.Data = tokenData;
        tokenObj.DrawTokenObject(tokenData);

        Vector3 pos = new Vector3(0, 0, 0);
        pos.x = BattleOceanController.START_POS_X + (tokenData.index % BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_WIDTH;
        pos.y = BattleOceanController.START_POS_Y + (tokenData.index / BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_HEIGHT;

        tokenObj.name = StringHelper.Format("Token({0}_{1})", tokenData.index, tokenData.type);
        tokenObj.transform.SetParent(tokenTransform);
        tokenObj.transform.localPosition = pos;

        _tokens.Add(tokenObj);
    }

    public IEnumerator Co_MoveTokens()
    {
        bool next = false;
        int doMoveToken = 0;

        foreach (Token o in _tokens)
        {
            if (!o.Data.active)
                continue;

            bool doMove = o.Move((param) =>
            {
                tokenMoveCount++;
                BOController.enabled = true;
                //if (param != null && param.ContainsKey(EMovingEvent.SUCCESS) && (int)param[EMovingEvent.SUCCESS] > -1)
                //{
                //    ObserverManager.it.Dispatch(EObserverEventKey.TOKEN_MOVED, null);
                //}
            });

            if (doMove)
            {
                BOController.enabled = false;
                doMoveToken++;
            }
        }

        if (doMoveToken == 0)
            next = true;

        while (!next)
            yield return null;
    }

    public void MoveTokens()
    {
        foreach (Token o in _tokens)
        {
            if (!o.Data.active)
                continue;

            o.Move((param) =>
            {
                tokenMoveCount++;
                if (param != null && param.ContainsKey(EMovingEvent.SUCCESS) && (int)param[EMovingEvent.SUCCESS] > -1)
                {
                    if (IsTokenMoving())
                    {
                        ObserverManager.it.Dispatch(EObserverEventKey.TOKEN_MOVED, null);
                    }
                }
                else
                {
                    if (IsTokenMoving())
                    {
                        Debug.Log("MoveTokens End");
                    }
                }
            });
        }
    }

    public bool IsTokenMoving()
    {
        if (_tokens == null)
            return false;

        return _tokens.Count != tokenMoveCount;
    }

    /// <summary>
    /// 토큰 출현 확률에 의한 배치
    /// </summary>
    public void SetObjectByAppearRating()
    {
        foreach (Tile o in _tiles)
        {
            if (o.Data.appear_rating == 100)
                continue;

            int rnd = Random.Range(0, 100) + 1;
            if (rnd > o.Data.appear_rating)
            {
                o.Data.active = false;
                o.gameObject.SetActive(false);
            }
        }

        foreach (Token o in _tokens)
        {
            if (o.Data.appear_rating == 100)
                continue;

            int rnd = Random.Range(0, 100) + 1;
            if (rnd > o.Data.appear_rating)
            {
                o.Data.active = false;
                o.gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// 항로 오픈 조건에 따른 셋팅
    /// </summary>
    public List<int> SetSeaRouteSetting()
    {
        List<SeaRoute> seaRoute = StageDataManager.it.GetAutoSeaRoute();
        List<int> seaRouteIndex = null;

        foreach (SeaRoute o in seaRoute)
        {
            if (o.open_type <= 0)
            {
                o.open = true;
                continue;
            }

            if (o.open_type == (int)EOceanChange.TILE_APPEAR)
            {
                if (o.open_value != null && o.open_value.Count > 0)
                {
                    foreach (int tileIndex in o.open_value)
                    {
                        List<TileData> tData = StageDataManager.it.GetTileDatas(tileIndex);
                        if (tData != null)
                        {
                            if (tData[0].active)
                                o.open = true;
                            else
                            {
                                if (seaRouteIndex == null) seaRouteIndex = new List<int>();
                                seaRouteIndex.Add(o.index);
                            }
                        }
                    }
                }
            }
        }

        return seaRouteIndex;
    }

    /// <summary>
    /// 타일간의 연결 항로 표시
    /// </summary>
    void CreateSeaRouteConnections()
    {
        Vector3 pos = Vector3.zero;

        if (_tiles == null)
            return;

        foreach (Tile o in _tiles)
        {
            string sailing_pass = o.Data.sailing_pass;
            if (sailing_pass.IsNullOrEmpty())
                continue;

            string[] passArr = sailing_pass.Split(',');
            for (int i = 0; i < passArr.Length; i++)
            {
                int goToIndex = passArr[i].IntValue();
                // 생성될 객체와 중복되는 대상이 있는지 찾아보자
                string findString = StringHelper.Format("SeaConnection_{0}_{1}", goToIndex, o.Data.index);
                if (seaRouteTransform.Find(findString) != null)
                    continue;

                pos.x = BattleOceanController.START_POS_X + (goToIndex % BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_WIDTH;
                pos.y = BattleOceanController.START_POS_Y + (goToIndex / BattleOceanController.HORIZONTAL) * BattleOceanController.TILE_HEIGHT;

                GameObject hts = Instantiate(Resources.Load("MapTool/SeaConnectionWay")) as GameObject;
                hts.name = StringHelper.Format("SeaConnection_{0}_{1}", o.Data.index, goToIndex);
                SeaRouteWayPoint sr = hts.Get<SeaRouteWayPoint>();

                Vector2 prevPos = Coordinate.ToPosition(o.Data.index);
                Vector2 currPos = Coordinate.ToPosition(goToIndex);

                float bAngle = STZMath.BetweenAngle(prevPos, currPos);
                Quaternion angle = Quaternion.Euler(0, 0, bAngle);

                Vector2 gapPos = currPos - prevPos;
                float lengthGap = Mathf.Sqrt(gapPos.x * gapPos.x + gapPos.y * gapPos.y);

                sr.ShowWayPoint(0, false, angle, lengthGap);

                hts.transform.SetParent(seaRouteTransform.transform);
                hts.transform.localPosition = pos;
                hts.transform.localRotation = angle;
                seaRouteBuckets.Add(hts);
            }
        }
    }

    void RemoveSeaRouteConnections()
    {
        if (seaRouteBuckets == null || seaRouteBuckets.Count <= 0)
            return;

        for (int i = 0; i < seaRouteBuckets.Count; i++)
        {
            GameObject o = seaRouteBuckets[i];
            seaRouteBuckets.Remove(o);
            Destroy(o);
            i--;
        }
    }

    /// <summary>
    /// 해당레이어 오브젝트의 활성화 여부 설정
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inEnable"></param>
    public void SetActiveAtIndex(int inIndex, bool inEnable)
    {
        if (inIndex < 0 || inIndex >= BattleOceanController.TILE_TOTAL)
            return;

        if (_tiles != null)
        {
            foreach (Tile o in _tiles)
            {
                if (o == null || o.Data == null)
                    return;

                if (o.Data.index == inIndex)
                {
                    o.gameObject.SetActive(inEnable);
                    //break;
                }
            }
        }
    }

    /// <summary>
    /// 해당 위치의 해당 타입의 바닥 타일 조회
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    /// <returns></returns>
    Tile GetBoardObject(int inIndex, int inType)
    {
        Tile targetOne = null;

        for (int i = 0; i < _tiles.Count; i++)
        {
            if (_tiles[i].Data.index == inIndex && _tiles[i].Data.type == inType)
            {
                targetOne = _tiles[i];
                break;
            }
        }

        return targetOne;
    }

    /// <summary>
    /// 레이져팡에 의해 z값이 바뀐 오브젝트를 정상황
    /// </summary>
    public void ReturnToNormalState()
    {
        if (_tiles == null)
            return;

        // 벽 장애물 처리
        for (int i = 0; i < _tiles.Count; i++)
        {
            Tile o = _tiles[i];

            if (o == null || o.Data == null)
                continue;

            //if (!BlockBoard.IsVaildArea(o.Data.index))
            //    continue;

            //if (Tile.IsBubbleTile(o.Data.type))
            //    o.transform.localPosition = new Vector3(o.transform.localPosition.x, o.transform.localPosition.y, (int)EBoardDepth.DEPTH_OBOVE_BLOCK);
        }
    }

    /// <summary>
    /// Gets the effect Z order.
    /// </summary>
    /// <returns>The effect Z order.</returns>
    /// <param name="inIndex">In index.</param>
    /// <param name="objectDepth">Object depth.</param>
    private Vector3 GetEffectZOrder(int inIndex/*, EBoardDepth objectDepth*/)
    {
        Vector3 effectPos = Coordinate.ToPosition(inIndex);
        //effectPos.z = (int)EObjectDepth.DEPTH_BOARD + (int)objectDepth;
        //effectPos.z = (int)objectDepth;
        return effectPos;
    }

    #region maptool method
    public void SetAlpha(float inAlpha, int inTabIndex = 0)
    {
        Color newColor = new Color(1, 1, 1, inAlpha);

        if (inTabIndex == 0 || inTabIndex == (int)TabIndex.TOKEN)
        {
            if (_tiles != null)
            {
                foreach (Tile o in _tiles)
                {
                    o.objImage.color = newColor;
                }
            }
        }
    }

    //public Tile GetTile(int inIndex)
    //{
    //    if (_tiles == null)
    //        return null;

    //    Tile resList = null;

    //    for (int k = 0; k < _tiles.Count; k++)
    //    {
    //        if (_tiles[k].Data.index == inIndex)
    //            return resList;
    //    }

    //    return resList;
    //}

    public List<Tile> GetTile(int inIndex)
    {
        if (_tiles == null)
            return null;

        List<Tile> resList = new List<Tile>();

        for (int k = 0; k < _tiles.Count; k++)
        {
            if (_tiles[k].Data.index == inIndex)
                resList.Add(_tiles[k]);
        }

        return resList;
    }

    /// <summary>
    /// 타일 삭제(특정 타일만 삭제)
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    public void RemoveTile(int inIndex, int inType = -1)
    {
        if (_tiles == null)
            return;

        for (int i = 0; i < _tiles.Count; i++)
        {
            if (_tiles[i].Data.index == inIndex)
            {
                if (inType < 0 && !Tile.IsHarbor(_tiles[i].Data.type))
                {
                    StageDataManager.it.RemoveTileData(inIndex, _tiles[i].Data);
                    _tiles[i].Release();
                    _tiles.RemoveAt(i);
                }
                else if (_tiles[i].Data.type == inType)
                {
                    StageDataManager.it.RemoveTileData(inIndex, _tiles[i].Data);
                    _tiles[i].Release();
                    _tiles.RemoveAt(i);
                    break;
                }
            }
        }
    }

    public void RemoveTile(int inIndex)
    {
        foreach (Tile o in _tiles)
        {
            if (o.Data.index == inIndex)
            {
                o.Release();
                break;
            }
        }
    }

    public List<Token> GetToken(int inIndex)
    {
        if (_tokens == null)
            return null;

        List<Token> resList = new List<Token>();

        for (int k = 0; k < _tokens.Count; k++)
        {
            if (_tokens[k]./*Data.index*/Index == inIndex)
                resList.Add(_tokens[k]);
        }

        return resList;
    }

    /// <summary>
    /// 토큰 삭제(특정 토큰만 삭제)
    /// </summary>
    /// <param name="inIndex"></param>
    /// <param name="inType"></param>
    public void RemoveToken(int inIndex, int inType = -1)
    {
        if (_tokens == null)
            return;

        for (int i = 0; i < _tokens.Count; i++)
        {
            if (_tokens[i]./*Data.index*/Index == inIndex)
            {
                //if (inType < 0 && !Tile.IsHarbor(_tokens[i].Data.type))
                //{
                //    StageDataManager.it.RemoveTokenData(inIndex, _tokens[i].Data);
                //    _tokens[i].Release();
                //    _tokens.RemoveAt(i);
                //}
                //else if (_tiles[i].Data.type == inType)
                {
                    StageDataManager.it.RemoveTokenData(inIndex, _tokens[i].Data);
                    _tokens[i].Release();
                    _tokens.RemoveAt(i);
                    break;
                }
            }
        }
    }

    public void RemoveToken(int inIndex)
    {
        foreach (Token o in _tokens)
        {
            if (o.Index == inIndex)
            {
                o.Release();
                _tokens.Remove(o);
                break;
            }
        }
    }
    #endregion
}