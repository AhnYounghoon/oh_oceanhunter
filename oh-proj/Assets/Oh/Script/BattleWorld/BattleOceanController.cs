﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum EBattleOceanShape
{
    LAYER_TILE = 0,
    LAYER_TOKEN = 1,

    COUNT
};

public class BattleOceanController : MonoBehaviour, IHashReceiver
{
    static public readonly int BASE_GAME_WIDTH = 1280;
    static public readonly int BASE_GAME_HEIGHT = 720;

    static public readonly int TILE_WIDTH = 70;
    static public readonly int TILE_HALF_WIDTH = 35;
    static public readonly int TILE_HEIGHT = 70;
    static public readonly int TILE_HALF_HEIGHT = 35;

    static public int TILE_TOTAL = 0;
    static public int REGULAR_SIZE_X = 18;
    static public int REGULAR_SIZE_Y = 10;
    static public int HORIZONTAL_GAP = 0;
    static public int VERTICAL_GAP = 0;
    static public int HORIZONTAL = 0;
    static public int VERTICAL = 0;
    static public int SCROLL_X = 0;
    static public int SCROLL_Y = 0;
    static public int SCROLL_MAX = 0;

    static public int TILE_RANGE_X = (BASE_GAME_WIDTH - TILE_WIDTH * REGULAR_SIZE_X) / 2;
    static public int TILE_RANGE_Y = (BASE_GAME_HEIGHT - TILE_HEIGHT * REGULAR_SIZE_Y) / 2;
    static public int START_POS_X = -(BASE_GAME_WIDTH / 2) + TILE_HALF_WIDTH + TILE_RANGE_X;
    static public int START_POS_Y = -(BASE_GAME_HEIGHT / 2) + TILE_HALF_HEIGHT + TILE_RANGE_Y;

    [Tooltip("월드 배경")]
    [SerializeField]
    Image imgBackground;

    [Tooltip("맵 이동시 움직이는 캐릭터")]
    [SerializeField]
    WorldShip character;

    [SerializeField]
    public OceanObjectLayer oceanObjLayer;

    [SerializeField]
    OceanUI oceanUI;

    MapToolManager maptool = null;
    Coroutine _coroutine = null;
    List<GameObject> seaRouteGuide = new List<GameObject>();

    private void Awake()
    {
        //LoadWorldImage();
        if (SceneManager.GetActiveScene().name != GeneralDataManager.ESceneName.MapTool.ToString())
            return;

        maptool = GameObject.Find("MapToolManager").transform.GetComponent<MapToolManager>();
    }

    public void Start()
    {
        if (GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.LobbyScene)
        {
            Vector3[] locatePosition = new Vector3[]
            {
                 new Vector3(-149, 0, 81),
                 new Vector3(-97, 0, 2),
                 new Vector3(-149, 0, -81),
                 new Vector3(-199, 0 ,1),
            };

            GeneralDataManager.it.playerData = new List<PlayData>();
            int selectIdx = 1;
            Dictionary<int, string> fleetData = GeneralDataManager.it.GetFleetData();
            string[] fleet = fleetData[selectIdx].Split(',');
            for (int i = 0; i < fleet.Length; i++)
            {
                string[] spliter = fleet[i].Split(':');
                int hero = spliter[0].IntValue();
                int sailor = spliter[1].IntValue();

                PlayerPlayData player = new PlayerPlayData(hero, sailor);
                player.startPosition = locatePosition[i];
                GeneralDataManager.it.playerData.Add(player);
            }
        }

        if (GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.LobbyScene ||
            GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.BattleScene)
        {
            StageDataManager.it.slotIndex = GeneralDataManager.it.prevStage;
            StageDataManager.it.LoadOceanData();
            StageDataManager.it.SelectStage(GeneralDataManager.it.prevStage);
            SetOcean();
            StartHunting();
        }

        // GeneralDataManager.ESceneName.BattleOceanScene 으로 실행시
        if (SceneManager.GetActiveScene().name != GeneralDataManager.ESceneName.MapTool.ToString() && 
            GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.None)
        {
            Statics.Instance.Parse(Statics.LoadFromLocal());
            StageDataManager.it.LoadPackagedStageFile(string.Empty);
            StageDataManager.it.SelectStage(1);
            SetOcean();
            StartHunting();
        }
    }

    public void ReceiveEvent(EObserverEventKey inEventKey, Hashtable inParam)
    {
        if (inEventKey == EObserverEventKey.TILE_CLICK)
        {
            MoveCharacterShip((TileData)inParam[EObserverEventKey.TILE_CLICK]);
        }
        else if (inEventKey == EObserverEventKey.TOKEN_MOVED)
        {
            //CheckToken(true);
            if (_coroutine != null) /*StopAllCoroutines();*/ StopCoroutine(_coroutine);
            _coroutine = StartCoroutine(AfterTokenMove());
        }
    }

    public void BackToHorbor()
    {
        ResetGame();
        GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.BattleOceanScene;
        SceneManager.LoadScene(GeneralDataManager.ESceneName.LobbyScene.ToString());
    }

    public void ResetGame()
    {
        ObserverManager.it.RemoveEventListener(this, EObserverEventKey.TILE_CLICK);
        ObserverManager.it.RemoveEventListener(this, EObserverEventKey.TOKEN_MOVED);
        character.Clear();
        oceanObjLayer.Clear();
        //RemoveSeaRouteGuide(-1, 0, true);

        if (_coroutine != null)
            StopCoroutine(_coroutine);
    }

    public void SetOcean()
    {
        oceanObjLayer.CreateOceanObjects();
        oceanObjLayer.BOController = this;

        // 인게임 전투를 하고 오면
        //if (/*SceneManager.GetActiveScene().name == GeneralDataManager.ESceneName.MapTool.ToString() && */GeneralDataManager.it.prevSceneName == GeneralDataManager.ESceneName.BattleScene)
        //{
        //    LoadOceanData();
        //    //GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.None;
        //}

        character.Init(StageDataManager.it.localData);

        // 자동항로 가이드 텍스쳐 생성
        /*
        List<SeaRoute> seaRouteList = StageDataManager.it.GetAutoSeaRoute();
        if (seaRouteList != null)
        {
            for (int i = 0; i < seaRouteList.Count; i++)
            {
                SeaRoute sr = seaRouteList[i];

                for (int j = 0; j < sr.sea_route.Count; j++)
                {
                    if (j == 0)
                        continue;

                    AddSeaRouteGuide(sr.sea_route[j], sr.sea_route[j - 1], i + 1, j == sr.sea_route.Count - 1);
                }
            }
        }
        */
    }

    void AddSeaRouteGuide(int inIndex, int inPrevIndex, int inStep, bool inIsFinal)
    {
        Vector3 pos = new Vector3();

        pos.x = START_POS_X + (inIndex % HORIZONTAL) * TILE_WIDTH;
        pos.y = START_POS_Y + (inIndex / HORIZONTAL) * TILE_HEIGHT;

        GameObject hts = Instantiate(Resources.Load("MapTool/SeaRouteWayPoint")) as GameObject;
        hts.name = string.Format("AutoRouteWayPoint_{0}_{1}", inIndex, inStep);
        SeaRouteWayPoint sr = hts.Get<SeaRouteWayPoint>();

        Vector2 prevPos = Coordinate.ToPosition(inPrevIndex);
        Vector2 currPos = Coordinate.ToPosition(inIndex);

        float bAngle = STZMath.BetweenAngle(prevPos, currPos);
        Quaternion angle = Quaternion.Euler(0, 0, bAngle);

        Vector2 gapPos = currPos - prevPos;
        float lengthGap = Mathf.Sqrt(gapPos.x * gapPos.x + gapPos.y * gapPos.y);

        sr.ShowWayPoint(inStep, inIsFinal, angle, lengthGap);

        hts.transform.SetParent(oceanObjLayer.seaRouteTransform.transform);
        hts.transform.localPosition = pos;
        hts.transform.localRotation = angle;
        seaRouteGuide.Add(hts);
    }

    void RemoveSeaRouteGuide(int inIndex, int inRouteIndex = 0, bool inIsAll = false)
    {
        if (seaRouteGuide == null || seaRouteGuide.Count <= 0)
            return;

        int i = 0;
        while (i < seaRouteGuide.Count)
        {
            GameObject o = seaRouteGuide[i];
            string[] arr = o.name.Split('_');

            if (inRouteIndex > 0)
            {
                if (int.Parse(arr[1]) == inIndex && int.Parse(arr[2]) == inRouteIndex)
                {
                    seaRouteGuide.Remove(o);
                    Destroy(o);
                    break;
                }
            }
            else
            {
                if (int.Parse(arr[1]) == inIndex)
                {
                    seaRouteGuide.Remove(o);
                    Destroy(o);
                    break;
                }
                else if (inIsAll)
                {
                    seaRouteGuide.Remove(o);
                    Destroy(o);
                    i--;

                    if (seaRouteGuide.Count == 0)
                        break;
                }
            }

            i++;
        }
    }

    public void StartHunting()
    {
        ObserverManager.it.AddEventListener(this, EObserverEventKey.TILE_CLICK);
        ObserverManager.it.AddEventListener(this, EObserverEventKey.TOKEN_MOVED);

        oceanObjLayer.SetObjectByAppearRating();
        List<int> closedRouteIndex = oceanObjLayer.SetSeaRouteSetting();

        if (closedRouteIndex != null)
        {
            foreach (int routeIndex in closedRouteIndex)
            {
                SeaRoute seaRoute = StageDataManager.it.GetSeaRoute(routeIndex);

                if (seaRoute == null)
                    continue;

                //foreach (int tileIndex in seaRoute.sea_route)
                //{
                //    RemoveSeaRouteGuide(tileIndex, seaRoute.index);
                //}
            }
        }

        //List<PlayData> _playerDataList = new List<PlayData>();  // 플레이어 데이터
        //PlayData playerData = new PlayData();
        //playerData.BaseOn(Statics.Instance.battle_object[1]);
        //playerData.tag = ETag.Player;
        //_playerDataList.Add(playerData);
        //GeneralDataManager.it.playerData = _playerDataList;

        //SimplePopup.ForceConfirm("StartHunting");
    }

    public void StopHunting()
    {
    }

    #region Scene Flow - 배틀해양씬
    IEnumerator PlaySequence()
    {
        //UnityEngine.EventSystems.EventSystem.current.enabled = false;

        yield return StartCoroutine(Co_CheckToken());

        yield return StartCoroutine(Co_CheckTile());

        yield return StartCoroutine(oceanObjLayer.Co_MoveTokens());

        //UnityEngine.EventSystems.EventSystem.current.enabled = true;
    }

    IEnumerator AfterBattleScene()
    {
        yield return StartCoroutine(Co_CheckTile());
    }

    IEnumerator AfterTokenMove()
    {
        yield return StartCoroutine(Co_CheckToken());
    }
    #endregion

    /// <summary>
    /// 배를 움직인다
    /// </summary>
    /// <returns></returns>
    void MoveCharacterShip(TileData inData)
    {
        // 선박이 움직이고 있는 상태
        if (character.isMove)
            return;

        //if (oceanObjLayer.IsTokenMoving())
        //    return;
        if (!enabled)
            return;

        if (inData == null)
            return;

        EMovingEvent eventResult = character.SM(inData.index, (param) =>
        {
            if (param != null && param.ContainsKey(EMovingEvent.ARRIVED_AT_HARBOR) && (bool)param[EMovingEvent.ARRIVED_AT_HARBOR])
            {
                if (maptool != null)
                    maptool.StopStage();
            }
            else if (param != null && param.ContainsKey(EMovingEvent.SUCCESS) && (bool)param[EMovingEvent.SUCCESS])
            {
                if (_coroutine != null) /*StopAllCoroutines();*/ StopCoroutine(_coroutine);
                _coroutine = StartCoroutine(PlaySequence());
            }
        });

        //EMovingEvent eventResult = character.StartMove(inData.index, (param) =>
        //{
        //    if (param != null && param.ContainsKey(EMovingEvent.ARRIVED_AT_HARBOR) && (bool)param[EMovingEvent.ARRIVED_AT_HARBOR])
        //    {
        //        if (maptool != null)
        //            maptool.StopStage();
        //    }
        //    else if (param != null && param.ContainsKey(EMovingEvent.SUCCESS) && (bool)param[EMovingEvent.SUCCESS])
        //    {
        //        //CheckToken();
        //        if (_coroutine != null) /*StopAllCoroutines();*/ StopCoroutine(_coroutine);
        //        _coroutine = StartCoroutine(PlaySequence());
        //    }
        //});

        switch (eventResult)
        {
            case EMovingEvent.NOT_ENOUGH_FUEL:
                StopHunting();
                StartCoroutine(StageFailOnMapTool());
                break;

            case EMovingEvent.ARRIVED_AT_GOAL:
                StopHunting();
                StartCoroutine(StageClearOnMapTool());
                break;
        }
    }

    /// <summary>
    /// 토큰 확인하여 사냥 및 전투
    /// </summary>
    void CheckToken(bool inIsTokenMoved = false)
    {
        List<Token> targetToken = oceanObjLayer.GetToken(character.currPos);
        if (targetToken == null || targetToken.Count == 0)
        {
            if (!inIsTokenMoved)
                CheckTile();
            return;
        }

        // 배틀씬으로 이동
        foreach (Token o in targetToken)
        {
            //SceneManager.LoadScene("BattleScene");
            SimplePopup.ForceConfirm("Let's Battle - 배틀씬 전환", () =>
            {
                oceanObjLayer.RemoveToken(character.currPos);
                StartCoroutine(PlaySequence());

            }, "테스트");
            return;
        }

        CheckTile();
    }

    IEnumerator Co_CheckToken()
    {
        bool next = false;

        List<Token> targetToken = oceanObjLayer.GetToken(character.currPos);
        if (targetToken == null || targetToken.Count == 0)
        {
            yield break;
        }

        // 배틀씬으로 이동
        foreach (Token o in targetToken)
        {
            if (maptool != null)
            {
                // 일단 무조건 승리하는 걸로..
                oceanObjLayer.RemoveToken(character.currPos);
                maptool.SaveStage(false);
                StageDataManager.it.SetLocalOceanData(character.currPos, character.currFuel);
                StageDataManager.it.SaveOceanData();
                ResetGame();

                // player & enemy 셋팅
                //List<PlayData> _playerDataList = new List<PlayData>();  // 플레이어 데이터
                //PlayData playerData = new PlayData();
                //playerData.BaseOn(Statics.Instance.battle_object[1]);
                //playerData.startPosition.x = -2000;
                //playerData.startScale.x = 1;
                //playerData.limitDistance = 600;
                //playerData.SetPlayer();
                //_playerDataList.Add(playerData);
                //GeneralDataManager.it.playerData = _playerDataList;
                List<PlayData> _playerDataList = GeneralDataManager.it.playerData;
                _playerDataList[0].startPosition.x = -2000;
                _playerDataList[0].startScale.x = 1;

                List<PlayData> _enemyDataList = new List<PlayData>();  // 적 데이터
                PlayData enemyData = new PlayData();
                enemyData.BaseOn(Statics.Instance.battle_object[o.Data.type]);
                enemyData.startPosition.x = 2000;
                enemyData.startScale.x = -1;
                enemyData.tag = ETag.Enermy;
                _enemyDataList.Add(enemyData);
                GeneralDataManager.it.enemyData = _enemyDataList;
                //

                GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.MapTool;
                SceneManager.LoadScene(GeneralDataManager.ESceneName.BattleScene.ToString());
            }
            else
            {
                //SimplePopup.ForceConfirm("Let's Battle - 배틀씬 전환", () =>
                //{
                //    oceanObjLayer.RemoveToken(character.currPos);
                //    if (_coroutine != null) StopCoroutine(_coroutine);
                //    _coroutine = StartCoroutine(AfterBattleScene());
                //    next = true;

                //}, "테스트");

                /// 유닛 세팅
                GeneralDataManager.it.enemyData = new List<PlayData>();

                EnemyPlayData enemy = null;
                if (o.Data.wave_info.IsNullOrEmpty())
                {
                    EnemyPlayData enemyBase = new EnemyPlayData();
                    enemyBase.wave = 1;
                    enemyBase.BaseOn(Statics.Instance.battle_object[o.Data.type]);
                    GeneralDataManager.it.enemyData.Add(enemyBase);
                }
                else
                {
                    string[] enemys = o.Data.wave_info.Split(',');
                    for (int i = 0; i < enemys.Length; i++)
                    {
                        string[] waveInfo = enemys[i].Split(':');
                        for (int j = 0; j < waveInfo[1].IntValue(); j++)
                        {
                            enemy = new EnemyPlayData();
                            enemy.wave = i + 1;
                            enemy.BaseOn(Statics.Instance.battle_object[waveInfo[0].IntValue()]);
                            GeneralDataManager.it.enemyData.Add(enemy);
                        }
                    }
                }
                // 항로 마지막에 있는 토큰을 보스로 간주
                SeaRoute aa = StageDataManager.it.GetSeaRoute(1);
                int goalIndex = aa.sea_route[aa.sea_route.Count - 1];
                GeneralDataManager.it.isBoss = character.currPos == goalIndex;
                if (GeneralDataManager.it.isBoss)
                    enemy.boss = true;

                ///  =====================================================

                GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.BattleOceanScene;
                SceneManager.LoadScene(GeneralDataManager.ESceneName.BattleScene.ToString());
                //oceanObjLayer.RemoveToken(character.currPos);
                GeneralDataManager.it.tokenIndex = character.currPos;
                GeneralDataManager.it.prevStage = StageDataManager.it.GetStageIndex();
                SaveTempStageData();
                StageDataManager.it.SetLocalOceanData(character.currPos, character.currFuel);
                //StageDataManager.it.SaveOceanData();
                ResetGame();

                next = true;
            }
        }

        while (!next)
            yield return null;
    }

    /// <summary>
    /// 타일 확인하여 이벤트 진행
    /// </summary>
    void CheckTile()
    {
        List<Tile> targetTile = oceanObjLayer.GetTile(character.currPos);
        if (targetTile == null)
        {
            MoveToken();
            return;
        }

        foreach (Tile o in targetTile)
        {
            if (Tile.IsEvent(o.Data.type))
            {
                SimplePopup.ForceConfirm(StringHelper.Format("Let's ({0}) Event", o.Data.type), null, "테스트");
                return;
            }
        }

        MoveToken();
    }

    IEnumerator Co_CheckTile()
    {
        bool next = false;
        List<Tile> targetTile = oceanObjLayer.GetTile(character.currPos);
        if (targetTile == null)
        {
            yield break;
        }

        foreach (Tile o in targetTile)
        {
            // NOTE 이벤트 체크 임시 금지
            //if (Tile.IsEvent(o.Data.type) && !o.Data.event_occur)
            //{
            //    o.Data.event_occur = true;
            //    //SimplePopup.ForceConfirm(StringHelper.Format("Let's ({0}) Event", ((ETileEvent)o.Data.event_type).ToString()), () => 
            //    //{
            //    //    next = true;
            //    //    AppearTile(o);

            //    //}, "테스트");
            //    PopupSailingEvent.Show((param) =>
            //    {
            //        next = true;
            //        AppearTile(o);
            //    }, o.Data.event_type, o.Data.event_amount);
            //}
            //else 
            if (Tile.IsGoal(o.Data.type))
            {
                SimplePopup.ForceConfirm(StringHelper.Format("Arrived At ({0}) Goal", o.Data.index), () =>
                {
                    if (_coroutine != null) /*StopAllCoroutines();*/ StopCoroutine(_coroutine);
                    StartCoroutine(StageClearOnMapTool());

                }, "테스트");
            }
            else
                next = true;
        }

        while (!next)
            yield return null;
    }

    /// <summary>
    /// 조우 이벤트 후 타일 생성과 항로 가이드 갱신
    /// </summary>
    /// <param name="inData"></param>
    void AppearTile(Tile inData)
    {
        if (inData.Data.reward_type != (int)ETileEventReward.REWARD_MAP)
            return;

        List<Tile> appearTile = oceanObjLayer.GetTile(inData.Data.reward_value.IntValue());
        if (appearTile == null)
            return;

        appearTile[0].gameObject.SetActive(true);

        // 항로 가이드 텍스쳐 생성
        List<SeaRoute> seaRouteList = StageDataManager.it.GetAutoSeaRoute();
        if (seaRouteList != null)
        {
            for (int i = 0; i < seaRouteList.Count; i++)
            {
                SeaRoute sr = seaRouteList[i];

                if (sr.open_type != (int)EOceanChange.TILE_APPEAR || !sr.open_value.Contains(appearTile[0].Data.index))
                    continue;

                for (int j = 0; j < sr.sea_route.Count; j++)
                {
                    if (j == 0)
                        continue;

                    AddSeaRouteGuide(sr.sea_route[j], sr.sea_route[j - 1], i + 1, j == sr.sea_route.Count - 1);
                }
            }
        }
    }

    /// <summary>
    /// 토큰 이동
    /// </summary>
    void MoveToken()
    {
        oceanObjLayer.MoveTokens();
    }

    /// <summary>
    /// 해당 항로 클리어
    /// </summary>
    private void ClearWorld()
    {
        Debug.Log("World Clear");
    }

    /// <summary>
    /// 월드 이미지를 로드한다
    /// TODO @minjun.ha 에셋 번들에서 받도록 수정해야함
    /// </summary>
    private void LoadWorldImage()
    {
        imgBackground.sprite = Resources.Load<Sprite>("AssetBundle/BattleWorld/ocean_1");
    }

    #region maptool play
    IEnumerator StageClearOnMapTool()
    {
        if (maptool == null)
            yield break;

        maptool.Log("스테이지 성공! 1초후에 기본화면으로 돌아갑니다.");

        yield return new WaitForSeconds(1f);

        //IS_RUNNING = false;
        maptool.StopStage();
    }

    IEnumerator StageFailOnMapTool()
    {
        if (maptool == null)
            yield break;

        maptool.Log("스테이지 실패! 1초후에 기본화면으로 돌아갑니다.");

        yield return new WaitForSeconds(1f);

        //IS_RUNNING = false;
        maptool.StopStage();
    }

    /// <summary>
    /// 변경된 토큰을 상태를 저장
    /// </summary>
    public void SaveTempStageData()
    {
        LocalStageData newStage = new LocalStageData();

        newStage.index = GeneralDataManager.it.prevStage;
        for (int i = 0; i < (int)EBattleOceanShape.COUNT; i++)
        {
            for (int objIndex = 0; objIndex < newStage.tileSize_h * newStage.tileSize_v; objIndex++)
            {
                switch (i)
                {
                    case (int)EBattleOceanShape.LAYER_TILE:
                        List<Tile> tile = oceanObjLayer.GetTile(objIndex);
                        if (tile != null)
                        {
                            if (newStage.tileList == null)
                                newStage.tileList = new List<TileData>();

                            foreach (Tile o in tile)
                            {
                                // 출현 확률에 의해서 사라진 타일은 제외
                                if (!o.gameObject.activeSelf)
                                    continue;

                                newStage.tileList.Add(o.Data);
                            }
                        }
                        break;

                    case (int)EBattleOceanShape.LAYER_TOKEN:
                        List<Token> token = oceanObjLayer.GetToken(objIndex);
                        if (token != null)
                        {
                            if (newStage.tokenList == null)
                                newStage.tokenList = new List<TokenData>();

                            foreach (Token o in token)
                            {
                                // 출현 확률에 의해서 사라진 토큰은 제외
                                if (!o.gameObject.activeSelf)
                                    continue;

                                // 배틀씬 진입 전 저장상황에서 토큰이 이동했다면 저장
                                if (o.Index != o.Data.index)
                                    o.Data.index = o.Index;

                                newStage.tokenList.Add(o.Data);
                            }
                        }
                        break;
                }
            }
        }

        List<SeaRoute> seaRouteList = StageDataManager.it.GetAutoSeaRoute();
        if (seaRouteList != null)
        {
            if (newStage.seaRouteList == null)
                newStage.seaRouteList = new List<SeaRoute>();

            newStage.seaRouteList.AddRange(seaRouteList);
        }

        StageDataManager.it.SaveTempStageData(newStage);
    }
    #endregion
}

[SerializeField]
public class LocalBattleOceanData
{
    public int shipIndex;
    public int shipFuel;

    public LocalStageData stageInfo;

    public void Bind(SimpleJSON.JSONNode inNode)
    {
        shipIndex = inNode.ContainsKey("shipIndex") ? inNode["shipIndex"].AsInt : 0;
        shipFuel = inNode.ContainsKey("shipFuel") ? inNode["shipFuel"].AsInt : 0;

        if (inNode.ContainsKey("stageInfo"))
        {
            stageInfo = new LocalStageData();
            stageInfo.Bind(inNode["stageInfo"]);
        }
    }

    public SimpleJSON.JSONClass Export()
    {
        SimpleJSON.JSONClass newOne = new SimpleJSON.JSONClass();

        newOne.Add("shipIndex", shipIndex.ToString());
        newOne.Add("shipFuel", shipFuel.ToString());
        newOne.Add("stageInfo", stageInfo.Export());

        return newOne;
    }
}