﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Collections;

public class WorldShip : MonoBehaviour
{
    public delegate void Callback(Hashtable param);
    public bool isMove { get; set; }
    public int currPos { get { return _currentPosition; } }
    public int currFuel { get { return _fuel; } }

    private List<SeaRoute> _allSeaRoute = null;
    private int _currentPosition = -1;
    private int _routeIndex = 0;
    private int _fuel = 0;

    public void Init(LocalBattleOceanData inData = null)
    {
        _fuel = 50;
        isMove = false;
        _allSeaRoute = StageDataManager.it.GetAutoSeaRoute();

        if (_allSeaRoute != null)
        {
            _currentPosition = _allSeaRoute[_routeIndex].sea_route[0];
        }

        if (inData != null)
        {
            if (GeneralDataManager.it.GetLastBattleWin())
            {
                _currentPosition = inData.shipIndex;
                _fuel = inData.shipFuel;
            }
        }

        this.transform.localPosition = Coordinate.ToPosition(_currentPosition);
    }

    public void Clear()
    {
        _fuel = 0;
        _currentPosition = -1;
        _routeIndex = 0;

        this.transform.localPosition = new Vector2(-728, 0);
    }

    bool FindRouteIndex(int inIndex)
    {
        int matchCount = 0;
        int matchIndex = 0;

        // 이동비용이 1인 항로 조회
        List<SeaRoute> targetRoute = null;
        foreach (SeaRoute o in _allSeaRoute)
        {
            if (!o.open)
                continue;

            if (o.sea_route.IndexOf(_currentPosition) < 0 || o.sea_route.IndexOf(inIndex) < 0)
                continue;

            if (Mathf.Abs(o.sea_route.IndexOf(_currentPosition) - o.sea_route.IndexOf(inIndex)) != 1)
                continue;

            if (targetRoute == null) targetRoute = new List<SeaRoute>();
            targetRoute.Add(o);
        }

        if (targetRoute == null)
            return false;

        foreach (SeaRoute o in targetRoute)
        {
            if (o.sea_route.IndexOf(inIndex) >= 0)
            {
                matchCount++;
                matchIndex = o.index - 1;
            }
        }

        // 교차되는 항로로 가능 경우는 이전 위치 항로 인덱스 유지, 그렇지 않으면 항로 인덱스 갱신
        if (matchCount == 1)
        {
            _routeIndex = matchIndex;
        }

        return true;
    }

    public EMovingEvent SM(int inTargetIndex, Callback inCallback = null)
    {
        if (_fuel <= 0)
        {
            Debug.Log("<color=#E7C802>Not Enough Fuel\n항구로 회황합니다.</color>");
            return EMovingEvent.NOT_ENOUGH_FUEL;
        }

        List<TileData> currData = StageDataManager.it.GetTileDatas(_currentPosition);
        if (currData == null)
        {
            return EMovingEvent.DO_NOT_FIND_ROUTE;
        }

        foreach (TileData o in currData)
        {
            if (!o.sailing_pass.Contains(inTargetIndex.ToString()))
                return EMovingEvent.DO_NOT_FIND_ROUTE;
        }

        isMove = true;
        int nextPosition = inTargetIndex;
        Vector2 from = this.transform.localPosition;
        Vector2 to = Coordinate.ToPosition(nextPosition);
        this.transform.DORotate(new Vector3(0, 0, STZMath.BetweenAngle(from, to) - 90), 0.3f)
            .OnComplete(() =>
            {
                this.transform.DOMove(to, 0.75f).OnComplete(() =>
                {
                    _currentPosition = nextPosition;
                    ArriveEvent();
                    if (inCallback != null)
                    {
                        //if (_allSeaRoute[_routeIndex].sea_route[0] == _currentPosition)
                        //    inCallback(STZCommon.Hash(EMovingEvent.ARRIVED_AT_HARBOR, true));
                        //else
                            inCallback(STZCommon.Hash(EMovingEvent.SUCCESS, true));
                        inCallback = null;
                    }
                });
            });

        return EMovingEvent.SUCCESS;
    }

    /// <summary>
    /// 배를 이동시킨다
    /// </summary>
    /// <param name="inTargetData"></param>
    public EMovingEvent StartMove(int inTargetIndex, Callback inCallback = null)
    {
        if (_fuel <= 0)
        {
            Debug.Log("<color=#E7C802>Not Enough Fuel\n항구로 회황합니다.</color>");
            return EMovingEvent.NOT_ENOUGH_FUEL;
        }

        if (!FindRouteIndex(inTargetIndex))
            return EMovingEvent.DO_NOT_FIND_ROUTE;

        int currIndex = _allSeaRoute[_routeIndex].sea_route.IndexOf(_currentPosition);
        if (currIndex == _allSeaRoute[_routeIndex].sea_route.Count - 1)
        {
            Debug.Log("<color=#FF00FF>목표에 도달하였습니다.</color>");
            return EMovingEvent.ARRIVED_AT_GOAL;
        }

        int targetIndex = _allSeaRoute[_routeIndex].sea_route.IndexOf(inTargetIndex);
        // 항로를 한칸씩 이동
        if (Mathf.Abs(currIndex - targetIndex) != 1)
            return EMovingEvent.DO_NOT_FIND_ROUTE;

        isMove = true;
        int nextPosition = _allSeaRoute[_routeIndex].sea_route[targetIndex];
        Vector2 from = this.transform.localPosition;
        Vector2 to = Coordinate.ToPosition(nextPosition);
        this.transform.DORotate(new Vector3(0, 0, STZMath.BetweenAngle(from, to) - 90), 0.3f)
            .OnComplete(() =>
            {
                this.transform.DOMove(to, 0.75f).OnComplete(() =>
                {
                    _currentPosition = nextPosition;
                    ArriveEvent();
                    if (inCallback != null)
                    {
                        if (_allSeaRoute[_routeIndex].sea_route[0] == _currentPosition)
                            inCallback(STZCommon.Hash(EMovingEvent.ARRIVED_AT_HARBOR, true));
                        else
                            inCallback(STZCommon.Hash(EMovingEvent.SUCCESS, true));
                        inCallback = null;
                    }
                });
            });

        return EMovingEvent.SUCCESS;
    }

    /// <summary>
    /// 도착 이벤트
    /// </summary>
    private void ArriveEvent()
    {
        _fuel--;
        isMove = false;
        Debug.Log(StringHelper.Format("Arrive At ({0}), Left Fuel ({1})", _currentPosition, _fuel));

        if (_allSeaRoute[_routeIndex].sea_route[0] == _currentPosition)
        {
            Debug.Log("항구도착");
        }        
    }
}