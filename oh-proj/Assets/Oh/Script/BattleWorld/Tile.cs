﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Spine.Unity;

public enum ETile
{
	TILE_NONE = 0,
	TILE_HARBOR,
    TILE_GOAL,
    TILE_NORMAL,
    TILE_EVENT,

    TOTAL
};

public enum ETileShape
{
    SHAPE_NONE = 0,
    SHAPE_FISHING,
    SHAPE_ROCK,
    SHAPE_ISLAND
};

public enum ETileEvent
{
    EVENT_NONE = 0,
    EVENT_BATTLE,
    EVENT_FISHING,
    EVENT_STREAM,
    EVENT_COLLISION,
    EVENT_PASS,
    EVENT_ENCOUNTER,
    EVENT_TREASURE,
    EVENT_HOT_SPRING,
    EVENT_RESCUE
};

public enum ETileEventReward
{
    REWARD_NONE = 0,
    REWARD_GOODS,
    REWARD_SHIP_STATUS,
    REWARD_MAP
};

public class Tile : STZBehaviour
{
    public delegate void Callback(Hashtable inParam);

    [SerializeField]
    public Image objImage;
    [SerializeField]
    public GameObject idMark;
    [SerializeField]
    public Text txtId;

    public SkeletonAnimation spine_object { get; set; }
    private BoxCollider2D _boxCollider;

    public int Type { get; set; }
    public TileData Data { get; set; }

    /// <summary>
    /// 바닥 오브젝트 생성
    /// </summary>
    /// <param name="inData"></param>
    public void DrawTileObject(TileData inData)
    {
        if (IsHarbor(inData.type))
        {
            objImage.sprite = TPResourceManager.it.GetSprite("harbor");
        }
        else if (IsGoal(inData.type))
        {
            objImage.sprite = TPResourceManager.it.GetSprite("goal");
        }
        else if (IsEvent(inData.type))
        {
            switch (inData.shape)
            {
                case (int)ETileShape.SHAPE_FISHING:
                    objImage.sprite = TPResourceManager.it.GetSprite("fishing");
                    break;
                case (int)ETileShape.SHAPE_ISLAND:
                    objImage.sprite = TPResourceManager.it.GetSprite("island");
                    break;
                case (int)ETileShape.SHAPE_ROCK:
                    objImage.sprite = TPResourceManager.it.GetSprite("rock");
                    break;
            }
        }
        else
        {
            objImage.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("normal_{0}", Random.Range(1, 5)));
        }
        //else if (IsGroundWater(inData.type))
        //{
        //    rendererType.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("tileWater_{0:D2}", inData.shape));
        //    rendererType.transform.localRotation = Quaternion.Euler(0, 0, inData.rotate);

        //    if (inData.effect_rotate >= 0)
        //    {
        //        spine_object = (Instantiate(Resources.Load("Prefabs/Ingame/spine_water_road")) as GameObject).GetComponent<SkeletonAnimation>();
        //        spine_object.skeleton.Update(0.001f);
        //        spine_object.skeleton.UpdateWorldTransform();
        //        spine_object.LateUpdate();
        //        spine_object.transform.SetParent(this.transform, false);
        //        spine_object.transform.localPosition = new Vector3(0, 0, (float)(-0.001 * inData.index));
        //        spine_object.transform.localRotation = Quaternion.Euler(0, 0, inData.effect_rotate);
        //        //spine_object.Play("animation", true);
        //    }
        //}        
    }

    public void SetTileIdMark(int inValue)
    {
        txtId.text = inValue.ToString();
    }

    public void OnClick()
    {
        //Debug.Log(Data.index.ToString() + " Tile OnClick");

        ObserverManager.it.Dispatch(EObserverEventKey.TILE_CLICK, STZCommon.Hash(EObserverEventKey.TILE_CLICK, Data));
    }

    //void SetEmptyTile(TileData inData, string themeKey, IngameTileSprite tileSet = null)
    //{
    //    if (inData.type == (int)EBoard.BOARD_HIDDEN_TILE)
    //    {
    //        if (tileSet != null)
    //            rendererType.sprite = tileSet.GetResource(IngameTileSprite.ETileType.TYPE_TILE, inData.shape > (int)EBoard.BOARD_TILE_LOCK ? inData.shape - (int)EBoard.BOARD_TILE_LOCK : inData.shape);
    //        else
    //            rendererType.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("{1}_tileBg_{0:D2}", inData.shape > (int)EBoard.BOARD_TILE_LOCK ? inData.shape - (int)EBoard.BOARD_TILE_LOCK : inData.shape, themeKey));

    //        if (inData.shape > (int)EBoard.BOARD_TILE_LOCK)
    //        {
    //            GameObject unLockObject = new GameObject("hidden_unlock");
    //            SpriteRenderer sr = unLockObject.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;

    //            sr.sprite = TPResourceManager.it.GetSprite("tile_unlock01");
    //            unLockObject.transform.SetParent(transform);
    //            unLockObject.transform.localPosition = new Vector3(0, 0, -2);
    //        }
    //    }
    //    else
    //    {
    //        if (tileSet != null)
    //            rendererType.sprite = tileSet.GetResource(IngameTileSprite.ETileType.TYPE_TILE, inData.shape);
    //        else
    //            rendererType.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("{1}_tileBg_{0:D2}", inData.shape, themeKey));
    //    }

    //    rendererType.transform.localRotation = Quaternion.Euler(0, 0, inData.rotate);

    //    if (inData.shape <= (int)EBoard.BOARD_TILE_LOCK)
    //    {
    //        // 땅타일에 꽃 그래픽 랜덤하게 적용
    //        if (tileSet != null)
    //            etcType.sprite = tileSet.GetResource(IngameTileSprite.ETileType.TYPE_ACC, Random.Range(1, 5));
    //        else
    //            etcType.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("{1}_tileFlower_{0:D2}", Random.Range(1, 5), themeKey));
    //    }

    //    if (inData.shadow_type > 0)
    //    {
    //        GameObject shadowTile = new GameObject("shadow_tile");
    //        SpriteRenderer sr = shadowTile.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;

    //        if (tileSet != null)
    //            sr.sprite = tileSet.GetResource(IngameTileSprite.ETileType.TYPE_SHADOW, inData.shadow_type);
    //        else
    //            sr.sprite = TPResourceManager.it.GetSprite(StringHelper.Format("{1}_tileShadow_{0:D2}", inData.shadow_type, themeKey));
    //        shadowTile.transform.SetParent(transform);
    //        shadowTile.transform.localPosition = new Vector3(0, 0, 1);
    //    }
    //}

    //void PlayBiscuitBrick(Callback inCallback, string inAnimationName, bool inLoop = true)
    //{
    //    spine_object.Play(inAnimationName, inLoop);

    //    if (inCallback != null)
    //    {
    //        spine_object.state.GetCurrent(0).Complete += (state, trackIndex, loopCount) =>
    //        {
    //            inCallback(null);
    //            inCallback = null;
    //        };
    //    }
    //}

    public static Tile GetObject()
    {
        Tile board;
        GameObject boardObj = Instantiate(Resources.Load("Prefabs/BattleOcean/Tile")) as GameObject;
        board = boardObj.GetComponent<Tile>();

        return board;
    }

    public void Release()
    {
        this.gameObject.SetActive(false);
        objImage.enabled = false;

        //this.transform.parent = null;

        Destroy(this.gameObject);
    }

    public static bool IsHarbor(int inType)
    {
        return inType == (int)ETile.TILE_HARBOR;
    }

    public static bool IsGoal(int inType)
    {
        return inType == (int)ETile.TILE_GOAL;
    }

    public static bool IsNormal(int inType)
    {
        return inType == (int)ETile.TILE_NORMAL;
    }

    public static bool IsEvent(int inType)
    {
        return inType == (int)ETile.TILE_EVENT;
    }
}