﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UniLinq;

public class OceanUI : STZBehaviour
{
    [SerializeField]
    Button btnChangeAutoRoute;

    [SerializeField]
    Button btnBackToHarbor;

    public bool bLongAutoRoute { get; private set; }

    void Awake()
    {
        btnChangeAutoRoute.Get<Text>("Text").text = bLongAutoRoute ? "최장 항로" : "최단 항로";
    }

    public void OnChangeRoute()
    {
        bLongAutoRoute = !bLongAutoRoute;
        btnChangeAutoRoute.Get<Text>("Text").text = bLongAutoRoute ? "최장 항로" : "최단 항로";
    }

    public void OnBackHarbor()
    {
        GeneralDataManager.it.prevSceneName = GeneralDataManager.ESceneName.BattleOceanScene;
        UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.LobbyScene.ToString());
    }
}