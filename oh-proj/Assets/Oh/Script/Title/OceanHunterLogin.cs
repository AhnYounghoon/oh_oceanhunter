﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using System;

public class OceanHunterLogin : STZLoginModule
{
    //[SerializeField]
    //LoadingResourceUI _loadingResourceUI;    

    System.DateTime dateTime = STZCommon.Time1970;

    string lastConnectionFilePath { get { return Application.temporaryCachePath + "/" + "last_connection_time".MD5() + ".dat"; } }
    string accessAgreeFilePath { get { return Application.temporaryCachePath + "/" + "access_agree".MD5() + ".dat"; } }

    System.DateTime GetLastConnectionTime()
    {
        string path = lastConnectionFilePath;
        if (System.IO.File.Exists(path))
        {
            return System.DateTime.Parse(FileIOExtension.LoadFromFile<string>(path, path));
        }
        else
        {
            return STZCommon.Time1970;
        }
    }

    void SaveConnectionTime()
    {
        string path = lastConnectionFilePath;
        FileIOExtension.SaveAsFile(System.DateTime.Now.ToString(), path, path);
    }

    bool GetAccessAgreeCache()
    {
        string path = accessAgreeFilePath;
        if (System.IO.File.Exists(path))
        {
            return FileIOExtension.LoadFromFile<bool>(path, path);
        }
        else
        {
            return false;
        }
    }

    void SaveAccessAgreeCache()
    {
        string path = accessAgreeFilePath;
        FileIOExtension.SaveAsFile(true, path, path);
    }

    protected override void Awake()
    {
		base.Awake();

        Input.multiTouchEnabled = false;
        Caching.compressionEnabled = true;
        Caching.maximumAvailableDiskSpace = 524288000;

#if UNITY_IOS
        // NOTE @minjun persistentDataPath는 iCloud/iTunes 백업이 되지 않게 수정
        UnityEngine.iOS.Device.SetNoBackupFlag(Application.persistentDataPath);
#endif

//#if UNITY_ANDROID
//        SingletonController.Get<Anipang3AndroidButtonManager>();
//#endif

        GeneralDataManager.it.Clear();
        //SingletonController.Get<SessionManager>();

        //SingletonController.Get<GameEventManager>().Clear();
        //SingletonController.Get<MagicCarpetRideManager>().Clear();
        //SingletonController.Get<SessionManager>().Clear();
        //SingletonController.Get<GoldenEggEventManager>().Clear();
        //SingletonController.Get<MagicBeanManager>().Clear();
        //InfinityTowerManager.it.Clear();
        //GemTreeManager.RemoveGameEventListener();
    }

    public override void InitBeforeNextScene()
    {
        base.InitBeforeNextScene();
        //SingletonController.Get<HonorDataManager>().Initialize();
        //SingletonController.Get<MagicCarpetRideManager>().Initialize();
        //SingletonController.Get<GuideManager>().Initalize();
        //SingletonController.Get<GoldenEggEventManager>().Initialize();
        //SingletonController.Get<MagicBeanManager>().Initialize();
        //SingletonController.Get<BirthdayManager>().Initialize();
        //GemTreeManager.AddGameEventListener();

        //InfinityTowerManager.it.Initialize();
        //PangtasticParadeManager.it.Initialize();

        // NOTE @minjun 로그인/ 로그아웃시 저장된 하트 무제한 시간 초기화를 위해 사용
        //SingletonController.Get<InfinityHeartManager>().Clear();
        //SingletonController.Get<InfinityHeartManager>().Initialize();
        
        //GameInfo.Init();
        //        AssetBundleManager.it.Clear();
    }

    //protected override IEnumerator SetupAdditionalGameServiceServerModel()
    //{
    //    SingletonController.Get<GameEventManager>().Initialize();
    //    yield return StartCoroutine(Co_InitGameData());
    //    yield return StartCoroutine(Co_PostInitGameData());
    //    yield return StartCoroutine(InitMapData(StageDataManager.EStageDataType.Season_1));      // 시즌 1 맵 데이터
    //    yield return StartCoroutine(InitMapData(StageDataManager.EStageDataType.Season_2));      // 시즌 2 맵 데이터
    //    yield return StartCoroutine(InitMapData(StageDataManager.EStageDataType.InfinityTower)); // 무한의 탑 데이터
    //}

//    protected IEnumerator InitMapData(StageDataManager.EStageDataType inStageDataType)
//    {
//        IEnumerable<ServerStatic.MapData> values = null;

//        // NOTE @huni0417 dev버전에서는 밸런싱테스트를 위해 특정 스테이지 파일이 있으면 로딩한다.
//        //if (Config.Deploy == Config.DEPLOY.DEV)
//        {
//            string stageDataFileName = StageDataManager.it.GetPlayTestPath(inStageDataType);
//#if UNITY_EDITOR
//            string DATA_PATH = Application.dataPath + "/Resources/Json" + stageDataFileName.ToString();
//#else
//            string DATA_PATH = Application.persistentDataPath + stageDataFileName.ToString();
//#endif

//            if (System.IO.File.Exists(DATA_PATH))
//            {
//                Debug.Log(DATA_PATH + " found!");
//                StageDataManager.it.LoadFilePathData(DATA_PATH, false);
//                yield break;
//            }
//        }

//#if UNITY_ANDROID
//        values = Statics.Instance.map_data
//            .Where(x => STZCommon.CheckEnableVersion(Config.CLIENT_VERSION, x.Value.start_android_ver, x.Value.end_android_ver))
//            .Select(x => x.Value);
//#elif UNITY_IOS
//        values = Statics.Instance.map_data
//            .Where(x => STZCommon.CheckEnableVersion(Config.CLIENT_VERSION, x.Value.start_ios_ver, x.Value.end_ios_ver))
//            .Select(x => x.Value);
//#else
//        yield break;
//#endif

//        Debug.Assert(values.Count() <= 1, "맵 데이터에서 조건에 만족하는 열이 1개 이상 존재합니다.");

//        // 맵 데이터는 무한의 탑일때 2개 이상이여야 한다
//        int compareValueCount = (int)inStageDataType;

//        // cdn에서 받을게 없으면 패키징된 데이터 로딩
//        if (values.Count() <= compareValueCount)
//        {
//            StageDataManager.it.LoadPackagedStageFile(StageDataManager.it.GetPackagedStageFilePath(inStageDataType));
//            yield break;
//        }

//        string []seasonType = { "s1", "s2", "witch_tower" };
//        var data = values.Where(x => x.season == seasonType[(int)inStageDataType]).FirstOrDefault();

//        // 해당 맵 데이터가 없다면 패키징된 데이터 로딩
//        if (data == null)
//        {
//            StageDataManager.it.LoadPackagedStageFile(StageDataManager.it.GetPackagedStageFilePath(inStageDataType));
//            yield break;
//        }
//        string url = data.fullResourceUrl;

//        // 맵데이터는 서비스 cdn 사용하도록 통일함
//        //if (Config.IsDev)
//        //{
//        //    url = url.Replace("http://anipang3-cdn.stzapp.com", "https://s3-ap-northeast-1.amazonaws.com/dev.anipang3.static");
//        //}

//        string path = StringHelper.Format("{0}/{1}.dat", Application.temporaryCachePath, url.MD5());
//        // cdn에서 받아서 캐싱해둔 파일이 있으면 로딩
//        if (StageDataManager.it.LoadFilePathData(path, true))
//        {
//            yield break;
//        }

//        WWW www = new WWW(url);
//        float timeout = 15f;
//        while (!www.isDone)
//        {
//            if (timeout <= 0)
//                break;

//            yield return null;
//            timeout -= Time.deltaTime;
//        }

//        if (timeout <= 0 || www.error != null)
//        {
//            SimplePopup.Alert(("E_NETWORK_ERROR").Locale(), () => { GeneralDataManager.it.NextScene(GeneralDataManager.ESceneName.Title); });
//            while (true)
//            {
//                yield return null;
//            }
//        }
//        else
//        {
//            StageDataManager.it.Parse(www.text, true);
//            System.IO.File.WriteAllText(path, www.text);
//        }

//        yield return null;
//    }

    protected override void NextScene()
    {
        //    ServerModel.Instance.heartSimulator.Start();
        //    AppManager.it.Initialize();
        //    SingletonController.Get<GameRewardChecker>().Initialize();
        //    SingletonController.Get<CostumeDataManager>().Initialize();
        //    KakaoMessageLimiter.Initialize();

        //    LoadingLayer.it.Close((param) => 
        //{
        //	InitBeforeNextScene();
        //                GeneralDataManager.it.NextScene(GeneralDataManager.it.signUp ? GeneralDataManager.ESceneName.Intro : (GeneralDataManager.ESceneName)System.Enum.Parse(typeof(GeneralDataManager.ESceneName), _nextScene));
        //            }, (LoadingLayer.EType)((int)GeneralDataManager.it.currentWorldMapType));
        UnityEngine.SceneManagement.SceneManager.LoadScene(GeneralDataManager.ESceneName.Opening.ToString());
    }

    bool IsUpdateByRefreshTime()
    {
        System.DateTime updateCheckTime = GetLastConnectionTime();
        updateCheckTime = System.DateTime.Parse(string.Format("{0}-{1}-{2} 00:00:00", updateCheckTime.Year, updateCheckTime.Month, updateCheckTime.Day));
        System.DateTime now = System.DateTime.Now;

        return !(updateCheckTime.Year == now.Year && updateCheckTime.Month == now.Month && updateCheckTime.Day == now.Day);
    }
    /*
    protected override void GetAdditionalInitServerRequest(SimpleJSON.JSONArray outArr)
    {
        Debug.Log("[INIT_STEP] GetAdditionalInitServerRequest...");

        outArr.Add(Server.Instance.stage_info(null, true).PublishToJsonClass());

        // 어드민 툴에서 무언가가 수정되었는지 여부.
        outArr.Add(Server.Instance.apevent_get_cacheinfo(null, "managed", true).PublishToJsonClass());
        outArr.Add(Server.Instance.apreward2(null, ServerData.DailyBonusType.GetAllTypes(), true).PublishToJsonClass());
        outArr.Add(Server.Instance.apreward2_invite_campaign_status(null, true).PublishToJsonClass());
        outArr.Add(Server.Instance.apattend(null, true).PublishToJsonClass());

        // NOTE @sangmoon 각종 이벤트들로 인하여 메시지 들의 상태가 변경되었을 수 있으므로 마지막에 호출 
        outArr.Add(Server.Instance.message_stat(null, true).PublishToJsonClass());
        outArr.Add(Server.Instance.userflag(null, true).PublishToJsonClass());

        // 접근동의 버튼을 눌렀다면 동의 정보 조회
        if (GeneralDataManager.it.isAccessAgree
#if !UNITY_IOS
            || !GetAccessAgreeCache()
#endif
            )
        {
            outArr.Add(Server.Instance.userAgree(null, true).PublishToJsonClass());
        }
    }

    IEnumerator Co_InitGameData()
    {
        bool next = false;

        SimpleJSON.JSONClass batch = new SimpleJSON.JSONClass();
        bool updateChecker = IsUpdateByRefreshTime();

        bool buying = PaymentManager.Buying();
        PaymentManager.EndBuying();

        bool needUpdate = false;
        if (Statics.Instance.systems.ContainsKey("local_caching_enable") && Statics.Instance.systems["local_caching_enable"].intValue == 0)
            needUpdate = true;
        else if (!Config.IsEditorService && ServerData.UserFlag.GetFlag(ServerData.UserFlag.EFlagKey.DEVICE_ID) == null || ServerData.UserFlag.GetFlag(ServerData.UserFlag.EFlagKey.DEVICE_ID).value != SystemInfo.deviceUniqueIdentifier)
            needUpdate = true;
        else if (ServerModel.Instance.event_info_set.ContainsKey("managed")
                 && ServerModel.Instance.event_info_set["managed"].val != "null"
                 && ServerModel.Instance.event_info_set["managed"].val.IntValue() == 1)
        {
            needUpdate = true;
        }

        bool isUsingLocalData = !needUpdate && !updateChecker && !buying;

        // 아바타 정보
        {
            string key1 = "avatar_set";
            string key2 = "main_avatar";
            if (isUsingLocalData && STZCommon.HasSavedModel(key1) && STZCommon.HasSavedModel(key2))
            {
                ServerModel.Instance.avatar_set = FileIOExtension.LoadFromFile<Dictionary<string, ServerData.Avatar>>(STZCommon.GetModelPath(key1), ServerModel.Instance.user_info.id.ToString());
                ServerModel.Instance.main_avatar = FileIOExtension.LoadFromFile<ServerData.MainAvatar>(STZCommon.GetModelPath(key2), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                batch.Add(Server.Instance.avatar_list(null, Config.CLIENT_VERSION, true, true).PublishToJsonClass());
            }
        }

        // 코스튬 정보
        {
            string key = "costume_set";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.costume_set = FileIOExtension.LoadFromFile<Dictionary<string, ServerData.Costume>>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                batch.Add(Server.Instance.costume_list(null, Config.CLIENT_VERSION, true, true).PublishToJsonClass());
            }
        }

        // 아이템
        {
            string key = "item_set";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.item_set = FileIOExtension.LoadFromFile<Dictionary<string, ServerData.Item>>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                batch.Add(Server.Instance.item(null, true).PublishToJsonClass());
            }
        }

        // 콜렉션
        {
            string key = "user_collection";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.user_collection = FileIOExtension.LoadFromFile<ServerData.UserCollection>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                batch.Add(Server.Instance.costume_list_collection(null, true).PublishToJsonClass());
            }
        }

        // 보물
        {
            string key = GeneralDataManager.IsSeason1() ? "relic_set" : "relic_set_s2";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.relic_set = FileIOExtension.LoadFromFile<Dictionary<string, ServerData.Relic>>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                int topEpisode = Statics.Instance.GetCurrentEpisodeDic().Max(x => x.Value.id);
                batch.Add(Server.Instance.current_relic_all(null, 1, topEpisode, true).PublishToJsonClass());
            }
        }

        // 스테이지 정보
        {
            string key = GeneralDataManager.IsSeason1() ? "stage_set" : "stage_set_s2";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.stage_set = FileIOExtension.LoadFromFile<Dictionary<string, ServerData.Stage>>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                int curEpId = ServerModel.Instance.stage_info.GetCurrentEpisode();
                int stageStart = 1;
                int stageEnd = ServerModel.Instance.stage_info.GetCurrentStageByTopClientVer();
                batch.Add(Server.Instance.current_stage_all(null, stageStart, stageEnd, true).PublishToJsonClass());
            }
        }

        // 유저 프로필
        {
            string key = "user_profile";
            if (isUsingLocalData && STZCommon.HasSavedModel(key))
            {
                ServerModel.Instance.user_profile = FileIOExtension.LoadFromFile<ServerData.UserProfile>(STZCommon.GetModelPath(key), ServerModel.Instance.user_info.id.ToString());
            }
            else
            {
                batch.Add(Server.Instance.userProfile(null, true).PublishToJsonClass());
            }
        }

        // 오퍼 레벨
        {
            // NOTE @jairy 요청 최소화를 위해 이벤트 기간에만 offer_level을 보냄.
            ServerStatic.SpecialOffer offer = ServerStatic.SpecialOffer.HasEnableEvents();
            if (offer != null)
            {
                PopupSpecialOffer.IS_DEFAULT_OFFER_LEVEL_DATA = false;

                string key = "offer_level";
                if (STZCommon.HasSavedModel(key + offer.start_at_google))
                {
                    ServerModel.Instance.offer_level = FileIOExtension.LoadFromFile<ServerData.OfferLevel>(STZCommon.GetModelPath(key + offer.start_at_google), ServerModel.Instance.user_info.id.ToString());
                }
                else
                {
                    batch.Add(Server.Instance.vip_offer_level(null, true).PublishToJsonClass());
                    // pay_count 0일때 서버에 안보내려고 했으나, 서버에서 캐싱을 하지 않아 과금후 바뀌는 문제가 있어 그냥 모두 보내도록 함
                }
            }
            else
            {
                Debug.Log("-----isDefaultData " + true);
                PopupSpecialOffer.IS_DEFAULT_OFFER_LEVEL_DATA = true;
            }
        }

        // 상점에 표시되는 골드카드
        var goldCardList = ServerStatic.Payment.GetListByShopCategory(EShopCategory.GOLD_CARD);
        if ((ServerModel.Instance.user_attr.pay_count.IntValue() > 0 && goldCardList != null && goldCardList.Length > 0) || updateChecker || needUpdate || buying)
        {
            for (int i = 0; i < goldCardList.Length; i++)
            {
                int goldCardPaymentId = goldCardList[i].id;
                if (Statics.Instance.payment.ContainsKey(goldCardPaymentId))
                {
                    int goldCardPackageId = Statics.Instance.payment[goldCardPaymentId].item_package_id;
                    RewardData[] reward = RewardData.ParseItemIDs(Statics.Instance.item_package[goldCardPackageId].item_ids);
                    if (reward.Length > 0)
                    {
                        batch.Add(Server.Instance.goldcard(null, reward[0].reward_sub.ToString(), true).PublishToJsonClass());
                        PlayerPrefs.SetString(EPlayerPrefKey.Latest_GoldCard_Query_Time, STZCommon.ConvertToLocalTime(TimeManager.GetTimeFromUnixTimeStamp(ServerModel.Instance._stm)).ToString());
                    }
                }
            }
        }

        // 하트 패키지 체크
        if (ServerModel.Instance.user_attr.pay_count.IntValue() > 0 || updateChecker || needUpdate || buying)
        {
            var heartPackage = Statics.Instance.special_offer.Values.Where(x => x.shop_category == (int)EShopCategory.SPECIAL_PACKAGE &&
                                                      x.item_ids.Contains("103:2:1")).ToList().FirstOrDefault();
            if (heartPackage != null)
            {
                RewardData[] reward = RewardData.ParseItemIDs(heartPackage.item_ids);
                if (reward.Length > 0)
                {
                    batch.Add(Server.Instance.goldcard(null, reward[0].reward_sub.ToString(), true).PublishToJsonClass());
                    PlayerPrefs.SetString(EPlayerPrefKey.Latest_Heart_Package_Query_Time, STZCommon.ConvertToLocalTime(TimeManager.GetTimeFromUnixTimeStamp(ServerModel.Instance._stm)).ToString());
                }
            }
        }

        // 아바타가 없을 경우 아바타 아이디 1, 2, 3을 생성하고 셋 중에 하나를 메인 아바타로 설정
        if (ServerModel.Instance.registered)
        {
            var avatarIds = Statics.Instance.character
                .Where(x => x.Value.is_default.BoolValue())
                .Select(x => x.Key)
                .ToArray();

            List<int> costumeIds = new List<int>();
            foreach (var avatarId in avatarIds)
            {
                costumeIds.AddRange(Statics.Instance.character_costume
                    .Where(x => x.Value.character_id == avatarId && Statics.Instance.character_costume[x.Key].is_default == 1)
                    .Select(x => x.Key)
                    .ToArray());
            }

            // 최초 메인 캐릭터 및 코스튬 랜덤으로 설정
            int costumeId = costumeIds[UnityEngine.Random.Range(0, costumeIds.Count)];
            batch.Add(Server.Instance.avatar_change_costume(null, Statics.Instance.character_costume[costumeId].character_id, costumeId, true).PublishToJsonClass());
            GeneralDataManager.it.signUp = true;
        }

        // 어드민 관련 정보 갱신
        {
            if (needUpdate)
                batch.Add(Server.Instance.apevent_set_cacheinfo(null, "managed", 0.ToString(), 10, true).PublishToJsonClass());
        }

        // 스테이지에 머물러 있는 친구 정보 요청
        btch.Add(Server.Instance.current_stage_friend_stage(null, true).PublishToJsonClass());

        // 친구의 아바타 정보 요청
        bool comma = false;
        System.Text.StringBuilder ids = new System.Text.StringBuilder();
        foreach (var value in Sns.Instance.appFriends.Values)
        {
            if (ServerModel.Instance.friend_set.ContainsKey(value.userid)
                && !string.IsNullOrEmpty(ServerModel.Instance.friend_set[value.userid].user_id))
            {
                if (comma)
                    ids.Append(",");
                ids.Append(ServerModel.Instance.friend_set[value.userid].user_id);
                comma = true;
            }
        }

        string eventKeys = string.Empty;
        // 뽑기권 지원 이벤트 진행 상태 조회
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.GACHA_TICKET_SUPPORT) &&
            (ServerModel.Instance.event_info_set == null || 
            !ServerModel.Instance.event_info_set.ContainsKey(((int)GameEventManager.EEventType.GACHA_TICKET_SUPPORT).ToString()) || 
            ServerModel.Instance.event_info_set[((int)GameEventManager.EEventType.GACHA_TICKET_SUPPORT).ToString()].val.IsNullOrEmpty()))
        {
            eventKeys = ((int)GameEventManager.EEventType.GACHA_TICKET_SUPPORT).ToString();
        }

        // 추석이벤트 - 신규 블럭 모으기 이벤트
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.NEW_COLLECTING_EVENT))
        {
            int eventKey = SingletonController.Get<GameEventManager>().GetNewCollectingEventKey();
            batch.Add(Server.Instance.apevent_block(null, eventKey, true).PublishToJsonClass());
        }

        // 루시이벤트
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.LUCY_COLLECTION))
        {
            ServerStatic.GameEvent gameEvent = SingletonController.Get<GameEventManager>().GetEvent(GameEventManager.EEventType.LUCY_COLLECTION);
            SimpleJSON.JSONNode eventDataRaw = SimpleJSON.JSONNode.Parse(gameEvent.event_script);
            int eventId = eventDataRaw["event_id"].AsInt;
            batch.Add(Server.Instance.apreward2_event_reward_check(null, (int)GameEventManager.EEventType.LUCY_COLLECTION, eventId, true).PublishToJsonClass());
        }

        // 빼빼로 이벤트 진행 상태 조회
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.COLLECTING_EVENT) &&
            (ServerModel.Instance.event_info_set == null ||
            !ServerModel.Instance.event_info_set.ContainsKey(((int)GameEventManager.EEventType.COLLECTING_EVENT).ToString()) ||
            ServerModel.Instance.event_info_set[((int)GameEventManager.EEventType.COLLECTING_EVENT).ToString()].val.IsNullOrEmpty()))
        {
            string eventKey = SingletonController.Get<GameEventManager>().GetCollectingEventKey();

            if (!eventKey.IsNullOrEmpty())
            {
                if (!eventKeys.IsNullOrEmpty())
                    eventKeys += "," + eventKey;
                else
                    eventKeys = eventKey;
            }
        }

        // 하트박스 이벤트
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.HEART_BOX_EVENT) &&
            (ServerModel.Instance.event_info_set == null ||
            !ServerModel.Instance.event_info_set.ContainsKey(((int)GameEventManager.EEventType.HEART_BOX_EVENT).ToString())))
        {
            string eventKey = ((int)GameEventManager.EEventType.HEART_BOX_EVENT).ToString();

            if (!eventKey.IsNullOrEmpty())
            {
                if (!eventKeys.IsNullOrEmpty())
                    eventKeys += "," + eventKey;
                else
                    eventKeys = eventKey;
            }
        }

        if (!eventKeys.IsNullOrEmpty())
        {
            batch.Add(Server.Instance.apevent_get_eventinfo(null, eventKeys, true).PublishToJsonClass());
        }

        // 휴면 유저 상태 조회
        bool bSleepUserEventEnable = Statics.Instance.systems["sleep_event_start"].intValue == 1;
        CachedUserSleepInfo userSleepInfo = new CachedUserSleepInfo();
        if (bSleepUserEventEnable && userSleepInfo.NeedCheck())
        {
            batch.Add(Server.Instance.apsleep(null, true).PublishToJsonClass());
        }

        if (!string.IsNullOrEmpty(ids.ToString()))
        {
            batch.Add(Server.Instance.avatar_list_friendsMainAvatar(null, ids.ToString(), true).PublishToJsonClass());
        }

        if (!Config.IsEditorService && 
            ServerData.UserFlag.GetFlag(ServerData.UserFlag.EFlagKey.DEVICE_ID) == null || ServerData.UserFlag.GetFlag(ServerData.UserFlag.EFlagKey.DEVICE_ID).value != SystemInfo.deviceUniqueIdentifier)
        {
            batch.Add(Server.Instance.userflag_update(null, ServerData.UserFlag.EFlagKey.DEVICE_ID.ToString(), SystemInfo.deviceUniqueIdentifier, true).PublishToJsonClass());
        }

        // 접근 권한 동의를 했다면 최초 동의 시간을 기록하자(기 등록된 정보(device_id)와 비교)
        if (!Config.IsEditorService)
        {
            if (GeneralDataManager.it.isAccessAgree)
            {
                // 최초 등록이거나 다른 디바이스라면 update
                if (ServerModel.Instance.agree_per_device == null ||
                    (ServerModel.Instance.agree_per_device != null && ServerModel.Instance.agree_per_device.device_id != SystemInfo.deviceUniqueIdentifier))
                    batch.Add(Server.Instance.userAgree_update(null, true).PublishToJsonClass());

                GeneralDataManager.it.isAccessAgree = false;
            }
#if !UNITY_IOS
            else if (!GetAccessAgreeCache())
            {
                bool agree = false;
                // 최초 등록이거나 다른 디바이스라면 update
                if (ServerModel.Instance.agree_per_device == null ||
                    (ServerModel.Instance.agree_per_device != null && ServerModel.Instance.agree_per_device.device_id != SystemInfo.deviceUniqueIdentifier))
                {
                    // 접근 권한 동의 팝업 후에 약관 동의 팝업이 나옴
                    SingletonController.Get<PopupManager>().Show(typeof(PopupAccessAgree), (param) =>
                    {
                        batch.Add(Server.Instance.userAgree_update(null, true).PublishToJsonClass());
                        SaveAccessAgreeCache();
                        agree = true;
                    });

                    while (!agree) yield return null;
                }
            }
#endif
        }

        if (batch.Count > 0)
        {
            Server.Instance.apbatch((isSuccess) => {
                if (isSuccess)
                {
                    next = true;

                    // 휴면 유저로 간주되면 휴면 유저에 대한 데이터를 남김 
                    if (bSleepUserEventEnable)
                    {
                        if (ServerModel.Instance.user_sleep == null || ServerModel.Instance.user_sleep.start.IsNullOrEmpty())
                        {
                            userSleepInfo.SetAsActiveUser();
                        }
                        else
                        {
                            userSleepInfo.RecordSleepUserStatus();
                        }
                    }
                }
                else
                {
                    SimplePopup.ConfirmOneButton(ServerModel.Instance.latestErrorMessage.Locale(), ()=> { GeneralDataManager.it.NextScene(GeneralDataManager.ESceneName.Title); });
                }
            }, batch);
        }
        else
        {
            next = true;
        }

        while (!next) yield return null;
        SaveConnectionTime();
    }

    /// <summary>
    /// 로컬에 캐싱된 스테이지 클리어 정보가 있는지 체크
    /// </summary>
    IEnumerator Co_PostInitGameData()
    {
        SimpleJSON.JSONArray batch = new SimpleJSON.JSONArray();
        bool next = false;

        bool containStageInfoCall = false;
        /// 로컬에 캐싱된 스테이지 클리어 정보가 있는지 체크
        CachedStageClearInfo cachedClearInfo = new CachedStageClearInfo(GeneralDataManager.it.currentWorldMapType);
        bool hasCachedClearInfo = false;
        if (cachedClearInfo.LoadData() && cachedClearInfo.stageID > 0 && cachedClearInfo.score >= 0 && cachedClearInfo.star >= 0)
        {
            containStageInfoCall = true;
            hasCachedClearInfo = true;
            SimpleJSON.JSONClass attr = new SimpleJSON.JSONClass();
            attr.Add("score", cachedClearInfo.score.ToString());
            attr.Add("star_cnt", cachedClearInfo.star.ToString());

            batch.Add(Server.Instance.current_stage_attr_set(null, cachedClearInfo.stageID, attr, string.Empty, Config.CLIENT_VERSION, true).PublishToJsonClass());
            batch.Add(Server.Instance.current_stage_info_set(null, cachedClearInfo.stageID + 1, Statics.Instance.GetCurrentEpisodeId(cachedClearInfo.stageID), true).PublishToJsonClass());
            
            // NOTE @seungwon 유저의 보석길 이벤트 진행 여부는 이 시점에 알 수 없지만 후처리를 위해 스테이지 번호를 저장
            SingletonController.Get<GemRoadManager>().localStageClearID = cachedClearInfo.stageID;
        }

        CachedCharacterGachaInfo cachedGachaInfo = new CachedCharacterGachaInfo();
        if (cachedGachaInfo.LoadData() && cachedGachaInfo.charID > 0)
        {
            GeneralDataManager.it.characterGachaInfo = cachedGachaInfo.charID;
            cachedGachaInfo.RemoveData();
        }

        if (!Config.IsEditorService)
        {
            // 유저 프로필이 갱신이 필요한지 체크
            var userProfile = ServerModel.Instance.user_profile;
            var userInfo = Sns.Instance.userInfo;
            if (userProfile.flag.IntValue() == 0 || userProfile.name != userInfo.nickName || userProfile.img_url != userInfo.profileImageUrl)
            {
                batch.Add(Server.Instance.userProfile_update(null, userInfo.nickName, userInfo.profileImageUrl, null, 1, 0, null, 1, true).PublishToJsonClass());
            }
        }

        /// 유저 프로필 정보가 있는지 체크
        if (batch.Count > 0)
        {
            Server.Instance.apbatch((isSuccess) =>
            {
                if (isSuccess)
                {
                    if (containStageInfoCall)
                        Server.Instance.SaveStageInfo(GeneralDataManager.it.currentWorldMapType);

                    if (hasCachedClearInfo)
                    {
                        GeneralDataManager.it.updateStageClearInfo = true;
                        cachedClearInfo.RemoveData();
                    }
                    next = true;
                }
                else
                {
                    SimplePopup.ConfirmOneButton(ServerModel.Instance.latestErrorMessage.Locale(), () => { GeneralDataManager.it.NextScene(GeneralDataManager.ESceneName.Title); });
                }
            }, batch);

            while (!next) yield return null;
        }
    }

    protected override IEnumerator Co_CheckAssetBundle()
    {
        while (!Caching.ready)
            yield return null;

        Dictionary<string, int> urlList = new Dictionary<string, int>();
        int maxEpisodeId = 1;

        // [시즌 1] 월드맵 리소스 url 획득
        foreach (var value in Statics.Instance.episode.Values)
        {
            if (value.id == 0)
                continue;

            int checkSum = STZCommon.CheckVersion(Config.CLIENT_VERSION, value.version);
            if (checkSum >= 0 && maxEpisodeId <= value.id)
                maxEpisodeId = value.id;

            if (checkSum < 0 || value.resource_version <= 0)
                continue;

            // 월드맵
            string url = StaticUrl.GetWorldMapEpisodeUrl(value.id, StaticUrl.EWorldMapResType.BG, GeneralDataManager.EWorldMapType.S1_WorldMap);
            if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.resource_version))
                urlList.Add(url, value.resource_version);

            // 인게임
            url = StaticUrl.GetIngameThemeUrl(value.id, GeneralDataManager.EWorldMapType.S1_WorldMap);
            if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.ingame_resource_version))
                urlList.Add(url, value.ingame_resource_version);

            // 보스 코스튬
            // 매 에피소드마다 새로운 코스튬을 넣지 않도록 한다(식별값 0)
            //if (value.boss_id > 0)
            {
                url = StaticUrl.GetBossThemeUrl(value.id, GeneralDataManager.EWorldMapType.S1_WorldMap);
                if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.boss_resource_version))
                    urlList.Add(url, value.boss_resource_version);
            }
        }

        // [시즌 2] 월드맵 리소스 url 획득
        foreach (var value in Statics.Instance.episode_s2.Values)
        {
            if (value.id == 0)
                continue;

            int checkSum = STZCommon.CheckVersion(Config.CLIENT_VERSION, value.version);
            if (checkSum >= 0 && maxEpisodeId <= value.id)
                maxEpisodeId = value.id;

            if (checkSum < 0 || value.resource_version <= 0)
                continue;

            // 월드맵
            string url = StaticUrl.GetWorldMapEpisodeUrl(value.id, StaticUrl.EWorldMapResType.BG, GeneralDataManager.EWorldMapType.S2_WorldMap);
            if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.resource_version))
                urlList.Add(url, value.resource_version);

            // 인게임
            url = StaticUrl.GetIngameThemeUrl(value.id, GeneralDataManager.EWorldMapType.S2_WorldMap);
            if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.ingame_resource_version))
                urlList.Add(url, value.ingame_resource_version);

            // 보스 코스튬
            // 매 에피소드마다 새로운 코스튬을 넣지 않도록 한다(식별값 0)
            //if (value.boss_id > 0)
            {
                url = StaticUrl.GetBossThemeUrl(value.id, GeneralDataManager.EWorldMapType.S2_WorldMap);
                if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.boss_resource_version))
                    urlList.Add(url, value.boss_resource_version);
            }
        }

        //보물 리소스 url 획득
        foreach (var value in Statics.Instance.relic.Values)
        {
            if (value.id == 0 || value.resource_version <= 0 || maxEpisodeId < value.id)
                continue;

            string url = StaticUrl.GetTreasureUrl(value.id);
            if (!Caching.IsVersionCached(url, value.resource_version))
                urlList.Add(url, value.resource_version);
        }

        //로딩 레이어 리소스 url 획득
        int loadingLayerVersion = Statics.Instance.systems["loading_resource_version"].intValue;
        if (loadingLayerVersion > 0 && !Caching.IsVersionCached(StaticUrl.GetLoadingLayerUrl(), loadingLayerVersion))
            urlList.Add(StaticUrl.GetLoadingLayerUrl(), Statics.Instance.systems["loading_resource_version"].intValue);

        //코스튬 리스트 획득
        foreach (var value in Statics.Instance.character_costume.Values)
        {
            if (value.id == 0 || !value.CheckEnableVer() || value.resource_version <= 0)
                continue;

            string url = StaticUrl.GetCostumeUrl(value.id);

            if (!urlList.ContainsKey(url) && !Caching.IsVersionCached(url, value.resource_version))
                urlList.Add(url, value.resource_version);
        }

        // 작은 코스튬 이미지 획득
        int smallCosutmeVer = Statics.Instance.systems["costume_sprite_version"].intValue;
        if (smallCosutmeVer > 0 && !Caching.IsVersionCached(StaticUrl.GetCostumeSpriteUrl(), smallCosutmeVer))
            urlList.Add(StaticUrl.GetCostumeSpriteUrl(), smallCosutmeVer);

        // 이벤트 이미지 획득
        if (SingletonController.Get<GameEventManager>().IsUsableEvent(GameEventManager.EEventType.COLLECTING_EVENT))
        {
            ServerStatic.GameEvent gameEvent = SingletonController.Get<GameEventManager>().GetUsableEvents().Where(T => T.event_static_id == (int)GameEventManager.EEventType.COLLECTING_EVENT).First();

            if (gameEvent != null)
            {
                SimpleJSON.JSONNode eventDataRaw = SimpleJSON.JSONNode.Parse(gameEvent.event_script);

                int eventResourceVer = 0;
                eventResourceVer = eventDataRaw["resource_version"].AsInt;
                if (eventResourceVer > 0 && !Caching.IsVersionCached(StaticUrl.GetEventResourcesUrl(eventResourceVer), eventResourceVer))
                    urlList.Add(StaticUrl.GetEventResourcesUrl(eventResourceVer), eventResourceVer);
            }
        }

        // 인트로 획득
        {
            string key = "intro_resource_version";
            string introUrl = StaticUrl.GetIntroUrl(GeneralDataManager.EWorldMapType.S1_WorldMap);
            int version = Statics.Instance.systems.ContainsKey(key) ? Statics.Instance.systems[key].intValue : 1;
            if (version > 0 && !Caching.IsVersionCached(introUrl, version))
                urlList.Add(introUrl, version);

            key = "intro_resource_version_S2";
            string introUrlS2 = StaticUrl.GetIntroUrl(GeneralDataManager.EWorldMapType.S2_WorldMap);
            int versionS2 = Statics.Instance.systems.ContainsKey(key) ? Statics.Instance.systems[key].intValue : 1;
            if (versionS2 > 0 && !Caching.IsVersionCached(introUrlS2, versionS2))
                urlList.Add(introUrlS2, versionS2);
        }

        // 사운드 리소스 url 획득
        foreach (ServerStatic.Sound sound in Statics.Instance.sound.Values)
        {
            if (sound.group_id.IsNullOrEmpty() || sound.resource_version <= 0)
                continue;

#if UNITY_IOS
            if (STZCommon.CheckVersion(Config.CLIENT_VERSION, sound.ios_c_ver) < 0)
                continue;
#else
            if (STZCommon.CheckVersion(Config.CLIENT_VERSION, sound.google_c_ver) < 0)
                continue;
#endif

            string sound_url = StaticUrl.GetSoundResourcesUrl(sound.group_id, sound.resource_version);
            if (!Caching.IsVersionCached(sound_url, sound.resource_version))
                urlList.Add(sound_url, sound.resource_version);
        }

        // 팝업 내의 리소스 url 획득
        foreach (ServerStatic.PopupResource popup_res in Statics.Instance.popup_resource.Values)
        {
            if (popup_res.assetbundle_name.IsNullOrEmpty() || popup_res.resource_version <= 0)
                continue;

#if UNITY_IOS
            if (!STZCommon.CheckEnableVersion(Config.CLIENT_VERSION, popup_res.start_ios_ver, popup_res.end_ios_ver))
                continue;
#else
            if (!STZCommon.CheckEnableVersion(Config.CLIENT_VERSION, popup_res.start_android_ver, popup_res.end_android_ver))
                continue;
#endif

            string popup_res_url = StaticUrl.GetPopupResourcesUrl(popup_res.assetbundle_name, popup_res.resource_version);
            if (!Caching.IsVersionCached(popup_res_url, popup_res.resource_version))
                urlList.Add(popup_res_url, popup_res.resource_version);
        }

        // 마녀의 탑 관련 어셋 번들 획득
        InfinityTowerData data = InfinityTowerManager.it.infinityTowerData;
        if (data != null)
        {
            // 기본 마녀의 탑 테마
            string baseThemeUrl = StaticUrl.GetInfinityTowerThemeUrl(data.theme_id, data.theme_ver);
            if (!urlList.ContainsKey(baseThemeUrl) && !Caching.IsVersionCached(baseThemeUrl, data.theme_ver))
                urlList.Add(baseThemeUrl, data.theme_ver);

            // 마녀의 탑 마오방 테마
            string maoThemeUrl = StaticUrl.GetInfinityTowerThemeUrl(data.mao_theme_id, data.mao_theme_ver);
            if (!urlList.ContainsKey(maoThemeUrl) && !Caching.IsVersionCached(maoThemeUrl, data.mao_theme_ver))
                urlList.Add(maoThemeUrl, data.mao_theme_ver);
        }

        if (urlList.Count > 0)
        {
            _loadingResourceUI.Reset(urlList.Count);

            int count = 0;
            foreach (var pair in urlList)
            {
                yield return StartCoroutine(Co_DownloadAssetBundle(pair.Key, pair.Value));
                _loadingResourceUI.SetValue(++count);
            }

            _loadingResourceUI.gameObject.SetActive(false);
        }
    }

    public IEnumerator Co_DownloadAssetBundle(string inUrl, int inVersion)
    {
        bool isSuccess = false;
        WWW www = null;
        while (!isSuccess)
        {
            www = WWW.LoadFromCacheOrDownload(inUrl, inVersion);

            yield return www;

            float timer = 0;
            while (!www.isDone)
            {
                timer += Time.smoothDeltaTime;
                if (timer > 60)
                    break;

                yield return null;
            }

            if (!string.IsNullOrEmpty(www.error) || timer >= 60)
            {
                bool next = false;
                string errorString = (www.error.Contains("404") ? "NO_DOWNLOAD_RESOURCE" : "RESOURCE_DOWNLOAD_FAILED").Locale();
                if (Config.Deploy != Config.DEPLOY.SERVICE)
                    errorString += "\n" + inUrl;
                SimplePopup.Alert(errorString, () => { next = true; });
                Debug.Log(string.Format("{0} load failed.", www.url));
                if (www != null)
                    www.Dispose();
                while (!next) yield return null;
                continue;
            }
            else
            {
                isSuccess = true;

                if (www.assetBundle != null)
                    www.assetBundle.Unload(true);

                if (www != null)
                    www.Dispose();
            }
        }
    }*/
}
