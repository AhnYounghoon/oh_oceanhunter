﻿using UnityEngine;
using UnityEngine.UI;

using STZFramework;
using SimpleJSON;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UniLinq;

public abstract class STZLoginModule : STZBehaviour
{
    /* 로그인 스텝 개수 */
    protected const int STEP_COUNT = 3;

    static bool passedTitle { get; set; }

    [Tooltip("현재 진행 상태를 표시해 줄 프로그래스 바")]
    [SerializeField]
    GaugeBar _loadingBar;

    [Header("UI")]
    [Tooltip("Sns 로그인 버튼")]
    [SerializeField]
    Button _btnKakao;

    [SerializeField]
    [Tooltip("게스트 로그인 버튼")]
    Button _btnGuest;

    [Header("Data")]
    [Tooltip("사운드 데이터 위치")]
    [SerializeField]
    string _soundDataPath;

    [Tooltip("앱 이름")]
    [SerializeField]
    string _appName;

    [Tooltip("다음 진행할 씬 이름")]
    [SerializeField]
    protected string _nextScene;

    [Tooltip("기본 프레임률")]
    [SerializeField]
    int _targetFrameRate; 

    /* 초기화 여부(중복 초기화가 불필요한 부분에 적용을 위함) */
    private static bool _initializedOnce = false;

    protected bool IsInitializedOnce { get { return _initializedOnce; } }

    /* GS서버에 로그인 됐는지 여부 */
    private bool _isLoginToGameServiceServer;

    /// <summary>
    /// 초기화 처리 목록
    /// </summary>
    private List<System.Func<IEnumerator>> _initProcedures;

    protected virtual void Awake()
    {
#if UNITY_EDITOR
        // NOTE @neec 에디터 모드에서 항상 타이틀씬을 먼저 시작하기 위한 처리
        passedTitle = true;
#endif

        Application.targetFrameRate = 60;

#if UNITY_EDITOR
        Debug.Log(string.Format("[Document] Application.persistentDataPath: {0}", Application.persistentDataPath));
        Debug.Log(string.Format("[Document] Application.temporaryCachePath: {0}", Application.temporaryCachePath));
        Debug.Log(string.Format("[App] Application.dataPath: {0}", Application.dataPath));
#endif
        //AES.Initalize(_appName);
        //SingletonController.Get<SoundManager>().Load(_soundDataPath);
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        // init logger
        //{
        //    bool enableFileLog = false;
        //    bool enableRemoteReport = false;
        //    LogType[] logEnableArray = null;

        //    switch (Config.Deploy)
        //    {
        //        case Config.DEPLOY.DEV:
        //        case Config.DEPLOY.STAGE:

        //            enableRemoteReport = true;

        //            logEnableArray = new LogType[4];

        //            logEnableArray[0] = LogType.Log;
        //            logEnableArray[1] = LogType.Warning;
        //            logEnableArray[2] = LogType.Error;
        //            logEnableArray[3] = LogType.Exception;
        //            break;

        //        case Config.DEPLOY.SERVICE:
        //        default:
        //            break;
        //    }

        //    StzLog.Logger.Init(delegate (bool inSuccess)
        //    {
        //        if (inSuccess)
        //        {
        //            Debug.Log("Success Stz logger");
        //        }
        //        else
        //        {
        //            Debug.LogError("failed to init Stz logger");
        //        }
        //    }, enableFileLog, enableRemoteReport, Config.Instance.getClientVersion() ,logEnableArray);
        //}

        //OptionDataManager.it.LoadData();
        Settings.UnityInfoInstaller.Install();
        StartCoroutine(Co_initializeApp());
    }
    
    /// <summary>
    /// 다음씬으로 전환 전 초기화가 필요할 때.
    /// </summary>
    public virtual void InitBeforeNextScene() { }

    IEnumerator Co_initialize()
    {
        _initProcedures = new List<System.Func<IEnumerator>>();

#if UNITY_EDITOR || UNITY_ANDROID
        //_initProcedures.Add(PROC_STZ_LOGO);
#endif

        //_initProcedures.Add(PROC_SHOW_INTRO);
        //_initProcedures.Add(PROC_SHOW_BI);

        // 최초 실행이라면
        if (!_initializedOnce)
        {
            _initProcedures.Add(PROC_INIT_PLUGINS);
            _initProcedures.Add(PROC_INIT_SYSTEM);
        }

        // 약관 동의
        //_initProcedures.Add(PROC_AGREE_TERMS);

        // 온라인 상태까지 대기
        _initProcedures.Add(WAIT_FOR_ONLINE);

        // 정기점검 체크
        //yield return StartCoroutine(Co_CheckServerRoutineInspection());

        // 소셜 로그인
        //_initProcedures.Add(PROC_LOGIN_SNS);

        // GS 서버 로그인
        //_initProcedures.Add(PROC_LOGIN_GAMESERVER);

        // 최초 실행이라면
        if (!_initializedOnce)
        {
            //if (!AppParam.GetInstallReferer().IsNullOrEmpty())
            //{
            //    _initProcedures.Add(PROC_SEND_INSTALL_REFERER);
            //}

            // 인앱상품 갱신(미처리 결제 내역 처리)
            //_initProcedures.Add(PROC_UPDATE_INAPP_PRODUCTS);
        }

        // 각종 컨트롤러 초기화
        //_initProcedures.Add(PROC_INIT_CONTROLLERS);

        //// locale 최신화 체크
        //_initProcedures.Add(PROC_UPDATE_LOCALE);

        //// 소셜 친구 정보 갱신
        //_initProcedures.Add(PROC_UPDATE_SNS);

        //// 로비 정보 업데이트
        //_initProcedures.Add(PROC_UPDATE_GAMEDATA);

        for (int step = 0; step < _initProcedures.Count; ++step)
        {
            string stepText = _initProcedures[step].Method.Name;

            // NOTE @neec 하단 진행 상태 알림 UI에 반영, 해당 함수명이 키값으로 처리됨
            // 해당 단계별로 함수명이 스태틱의 locale에 명시된 값으로 조회되므로 변경시 주의하자

            UpdateLoadingInfo(step + 1, _initProcedures.Count, stepText.Locale());

            // 각 스텝 처리
            yield return StartCoroutine(_initProcedures[step]());

            //while (!PopupManager.it.HasNoPopup || !PopupManager.it.HasNoLoadedPopup)
            //{
            //    yield return null;
            //}
        }

        _initializedOnce = true;

        // 메인씬으로 이동
        NextScene();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator Co_initializeApp()
    {
        _loadingBar.SetGauge(0.0f, false, string.Empty);

        // 로컬에 저장된 Statics 버전 정보 로드 
        string staticsVersions = Statics.Instance.GetLocalSavedVersions();

        // 로컬에 저장된 언어파일 로드 
        //if (Statics.Instance.locale != null)
        //{
        //    Statics.Instance.locale.LoadFromLocal(Statics.Instance.locale.GetUrl());
        //}

        if (!_initializedOnce)
        {
            _initializedOnce = true;
            //Kochava.Tracker.SendEvent("AppLaunch", null);

            SetLoadingInfo(1, STEP_COUNT);
            yield return StartCoroutine(PROC_INIT_SYSTEM());

            SetLoadingInfo(2, STEP_COUNT);
            yield return StartCoroutine(PROC_INIT_PLUGINS());
        }

        // 온라인 상태까지 대기 
        yield return StartCoroutine(WAIT_FOR_ONLINE());

        // 정기점검 체크
        //yield return StartCoroutine(Co_CheckServerRoutineInspection());

        //SetLoadingInfo(3, STEP_COUNT);
        //yield return StartCoroutine(LoginSNS());

        // 카카오톡 친구 목록 불러오기 
#if !UNITY_EDITOR
        //if (!Sns.Instance.isGuest && Kakao.Me.Instance.isTalkUser)
        //{
        //    SetLoadingInfo(4, STEP_COUNT);
        //    yield return StartCoroutine(Kakao.NativeExtension.Instance.LoadAllFriends());
        //    KakaoService.RefreshFriends();
        //    yield return StartCoroutine(Kakao.NativeExtension.Instance.LoadMultiChatList());
        //}

        //if (Config.IsDev && !Sns.Instance.isGuest)
        //{
        //    // NOTE @neec 카카오 사용자 및 친구 정보를 Android/data/com.sundaytoz.kakao.matgo.dev/files/sns.xml에 저장하며
        //    // 이 파일을 로컬 테스트시 Assets/LocalData/에 위치하면 해당 카카오 사용자로 운용이 가능하다 

        //    JSONClass root = Kakao.Friends.Instance.ExportToJSON() as JSONClass;
        //    Kakao.Me.Instance.ExportToJSON(root);

        //    Debug.Log("[SNS] " + root.ToString());

        //    root.SaveToFile(Application.persistentDataPath + "/sns.xml");
        //}
#endif

        //SetLoadingInfo(5, STEP_COUNT);
        //yield return StartCoroutine(LoginGameServiceServer(staticsVersions));

        // cp 서버 연결
        //yield return StartCoroutine(InitXPromotion());

        // 최신 버전의 언어팩이 로드되지 않았다면 
        //if (!Statics.Instance.locale.IsLatestVersion())
        //{
        //    SetLoadingInfo(6, STEP_COUNT);
        //    yield return StartCoroutine(LoadLocaleFromRemote(Statics.Instance.locale.GetUrl()));
        //}

#if !UNITY_EDITOR
        // 구매 프로덕트 정보 가져오기
        //bool unfinishedPaymentFinished = false;
        //PaymentManager.Instance.GetProductInfo(
        //    (isSuccess) =>
        //    {
        //        if (isSuccess)
        //        {
        //            // 미결제 처리 체크
        //            PaymentManager.Instance.CheckUnfinishedPayment((inSuccess, inPaymentCount) =>
        //            {
        //                GeneralDataManager.it.unfinishedPaymentSuccess = isSuccess && inPaymentCount > 0;
        //                unfinishedPaymentFinished = true;
        //            });
        //        }
        //        else
        //        {
        //            unfinishedPaymentFinished = true;
        //        }
        //    }, ServerStatic.Payment.GetProductIdListByMarket(STZCommon.currentMarketId), null);
#endif

        //SetLoadingInfo(7, STEP_COUNT);
        //yield return StartCoroutine(RegisterEssenstialInfo());

#if !UNITY_EDITOR
        //while (!unfinishedPaymentFinished)
        //    yield return null;
#endif

        //SetLoadingInfo(8, STEP_COUNT);
        //yield return StartCoroutine(SetupGameServiceServerModel());

        //SetLoadingInfo(9, STEP_COUNT);
        //yield return StartCoroutine(Co_CheckAssetBundle());

        // 게이지 바가 다 찰때까지 대기 
        {
            bool _bGaugeComplete = false;
            SetLoadingInfo(3, STEP_COUNT, (ratio) => _bGaugeComplete = true);

            while (!_bGaugeComplete)
            {
                yield return null;
            }
        }
        
        NextScene();
    }

    /// <summary>
    /// 시스템 초기화 
    /// 앱 기동시 한 번 만 호출된다
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator PROC_INIT_SYSTEM()
    {
        AES.Initalize(_appName);

        UrlImageLoader.it.Init();

        yield return null;
    }

    /// <summary>
    /// 플러기인 모듈들 초기화 
    /// </summary>
    protected virtual IEnumerator PROC_INIT_PLUGINS()
    {
        SundaytozNativeExtension.Instance.Initialize(delegate (int inStatus, JSONNode inResult)
        {
            SundaytozNativeExtension.Instance.CancelLocalNotificationAll();

            string appParams = string.Empty;
            string installReferer = string.Empty;

            if (inResult.ContainsKey(StzNativeStringKeys.Params.app_params))
            {
                appParams = inResult[StzNativeStringKeys.Params.app_params].Value;
            }

            if (inResult.ContainsKey(StzNativeStringKeys.Params.install_referer))
            {
                installReferer = (inResult[StzNativeStringKeys.Params.install_referer].Value);
            }

            //AppParam.Parse(appParams, installReferer);
        });

        //PaymentManager.Instance.Init();

//#if (!UNITY_EDITOR && UNITY_ANDROID)
//        SundaytozGcmExtension.Instance.Initialize("com.sundaytoz.kakao.anipang3.MainActivity", delegate(string inStatus, JSONNode inResult) {
//            SundaytozGcmExtension.Instance.GcmResetBadge(0);
//            Debug.Log("[GCM] Initialized GCM Extension!");      
//        });
//        // NOTE @neec iOS는 초기화가 필요없음
//#endif

        yield return null;
    }

    /// <summary>
    /// 네트워크 온라인 상태까지 대기 
    /// </summary>
    /// <returns></returns>
    private IEnumerator WAIT_FOR_ONLINE()
    {
        bool showPopup = false;
        const float networkWaitTime = 15.0f;
        float elapsedTime = 0.0f;

        while (!NetworkHelper.IsOnline())
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime > networkWaitTime && !showPopup)
            {
                showPopup = true;
                SimplePopup.SmallAleart("E_NOT_CONNECTED_NETWORK".Locale(), () => { showPopup = false; elapsedTime = 0.0f; });
            }

            yield return null;
        }
    }

  //  IEnumerator Co_CheckServerRoutineInspection()
  //  {
  //      const int TIME_OUT = 3;

  //      bool showPopupNoticeEnd = false;

  //      /*if (Config.Deploy != Config.DEPLOY.SERVICE)
		//{
		//	showPopupNoticeEnd = false;
		//	PopupCommonNotice.Show("", "Start Co_CheckServerState", "OK", null, null, () => { showPopupNoticeEnd = true; });

		//	while (!showPopupNoticeEnd)
		//	{
		//		yield return null;
		//	}
		//}*/

  //      string status_url = "https://s3-ap-northeast-1.amazonaws.com/dev.anipang3.static/status.json";
        
  //      if (Config.Deploy == Config.DEPLOY.SERVICE)
  //      {
  //          status_url = "http://anipang3-cdn.stzapp.com/status.json";

  //          if (Sundaytoz.StzPaymentSettings.AndroidMarket == Sundaytoz.StzPaymentSettings.ANDROID_MARKET.ONE_STORE)
  //              status_url = "http://anipang3-onestore-cdn.stzapp.com/status.json";
  //      }

  //      WWW www = new WWW(string.Format("{0}?time={1}", status_url, TimeManager.GetUnixTimeStampNow().ToString()));

  //      float duration = 0.0f;

  //      while (!www.isDone)
  //      {
  //          if (duration >= TIME_OUT)
  //          {
  //              break;
  //          }
  //          else
  //          {
  //              duration += Time.deltaTime;
  //          }

  //          yield return null;
  //      }

  //      ServerConfig serverConfig = null;
  //      bool errorOccurs = false;

  //      if (duration < TIME_OUT && www.isDone && string.IsNullOrEmpty(www.error) && www.bytes.Length > 0)
  //      {
  //          string configText = www.text;

  //          serverConfig = ServerConfig.Parse(configText);

  //          /*if (serverConfig != null && Config.Deploy != Config.DEPLOY.SERVICE)
		//	{
		//		showPopupNoticeEnd = false;
		//		PopupCommonNotice.Show("", serverConfig.status + ", " + serverConfig.enable_play, "OK", null, null, () => { showPopupNoticeEnd = true; });

		//		while (!showPopupNoticeEnd)
		//		{
		//			yield return null;
		//		}
		//	}*/

  //          if (serverConfig != null)
  //          {
  //              serverConfig.CheckServerStatus();

  //              while (serverConfig.IsCheckingStatus)
  //              {
  //                  yield return null;
  //              }
  //          }
  //      }
  //      else
  //      {
  //          errorOccurs = true;
  //      }

  //      if (errorOccurs || serverConfig == null)
  //      {
  //          if (Config.Deploy != Config.DEPLOY.SERVICE)
  //          {
  //              string debugStr = "URL : " + status_url + "\n" +
  //                  "duration : " + duration.ToString() + "\n" +
  //                  "www.isDone : " + www.isDone.ToString() + "\n" +
  //                  "www.bytes.Length : " + (www.bytes == null ? 0 : www.bytes.Length).ToString() + "\n" +
  //                  "www.error : " + (www.error == null ? string.Empty : www.error) + "\n" +
  //                  "serverConfig is null : " + (serverConfig == null).ToString();

  //              showPopupNoticeEnd = false;
  //              SimplePopup.Alert(debugStr, () => { showPopupNoticeEnd = true; });

  //              while (!showPopupNoticeEnd)
  //              {
  //                  yield return null;
  //              }
  //          }
  //      }

  //      yield break;
  //  }

    /// <summary>
    /// 원격지로부터 최신 언어팩을 다운로드 
    /// </summary>
    /// <param name="inNewLocaleURL"></param>
    /// <returns></returns>
    private IEnumerator LoadLocaleFromRemote(string inNewLocaleURL)
    {
        UnityEngine.Debug.Log("[LOCALE] Downloading " + inNewLocaleURL);

        WWW www = new WWW(inNewLocaleURL);
        yield return www;

        // 로드에 성공했다면 
        if (string.IsNullOrEmpty(www.error))
        {
            if (Statics.Instance == null || Statics.Instance.locale == null)
            {
                yield break;
            }

            Statics.Instance.locale.SetLocale(inNewLocaleURL, www.text);
        }

        yield return null;
    }

    //private IEnumerator InitXPromotion()
    //{
    //    UnityEngine.Debug.Log("[INIT_STEP] InitXPromotion...");

    //    CrossPromotion.DEPLOY deploy = CrossPromotion.DEPLOY.SERVICE;
    //    CrossPromotion.ANDROID_MARKET market = CrossPromotion.ANDROID_MARKET.GOOGLE;

    //    switch (Config.Deploy)
    //    {
    //        case Config.DEPLOY.DEV:
    //            deploy = CrossPromotion.DEPLOY.DEV;
    //            break;
    //        case Config.DEPLOY.STAGE:
    //            deploy = CrossPromotion.DEPLOY.STAGE;
    //            break;
    //    }

    //    switch (Sundaytoz.StzPaymentSettings.AndroidMarket)
    //    {
    //        case Sundaytoz.StzPaymentSettings.ANDROID_MARKET.ONE_STORE:
    //            market = CrossPromotion.ANDROID_MARKET.ONE_STORE;
    //            break;
    //    }

    //    CrossPromotion.Manager.Init((bool inIsSuccess) =>
    //    {
    //        GeneralDataManager.it.isXPromoInit = inIsSuccess;
    //    },
    //    deploy,
    //    Sns.Instance.userInfo.userId,
    //    Config.XPROMOTION_APP_ID,
    //    Config.CLIENT_VERSION,
    //    SystemInfo.deviceUniqueIdentifier,
    //    market,
    //    this.CheckError);

    //    yield return null;
    //}

    //bool CheckError(CrossPromotion.Server.Request.RequestData req)
    //{
    //    return true;
    //}

    /// <summary>
    /// SNS 로그인 처리 
    /// </summary>
    /// <returns></returns>
//    private IEnumerator LoginSNS()
//    {
//        Debug.Log("[INIT_STEP] Login SNS...");

//#if UNITY_EDITOR
//        //Kakao.NativeExtension.Instance.Initialize(null);
//        //LoginAsTester();
//        yield return null;
//#else
//        ShowLoginButton(false);

//        bool bReceived = false;
//        KakaoService.Initialize(delegate (KakaoService.ELoginState inStatus)
//        {
//            bReceived = true;

//            if (inStatus != KakaoService.ELoginState.LOGIN)
//            {
//                Debug.Log(string.Format("[KAKAO] Need KakaoLogin : {0}({1})", KakaoService.lastLoginErrorMessage, KakaoService.lastLoginErrorCode));

//#if UNITY_IOS
//                SingletonController.Get<PopupManager>().Show(typeof(PopupTerms), (param) => ShowLoginButton(true));
//#else
//                // 접근 권한 동의 팝업 후에 약관 동의 팝업이 나옴
//                SingletonController.Get<PopupManager>().Show(typeof(PopupAccessAgree), (param) =>
//                {
//                    SingletonController.Get<PopupManager>().Show(typeof(PopupTerms), (param2) => ShowLoginButton(true));
//                });
//#endif
//            }
//            else
//            {
//                Sns.SetPlatform(Sns.Platform.KAKAO);
//            }
//        });

//        // 로그인 완료까지 대기 
//        while (!Sns.IsLogined || !bReceived)
//        {
//            yield return null;
//        }
//#endif

//        //Kochava.Tracker.SendEvent("Kakao_Login", null);
//        Debug.Log("[INIT_STEP] Login SNS Complete!");
//    }

    //public void OnLoginButton()
    //{
    //    Debug.Log("[KAKAO_LOGIN] Pushed Login Button!");
    //    Sns.SetPlatform(Sns.Platform.KAKAO);
    //    ShowLoginButton(false);

    //    KakaoService.Login(delegate (KakaoService.ELoginState inStatus)
    //    {
    //        OutputKakaoServiceResponse(inStatus);
           
    //        if (inStatus != KakaoService.ELoginState.LOGIN)
    //        {
    //            Debug.LogWarning(string.Format("failed to login at kakao. reason : {0}", inStatus));
    //            ShowLoginButton(true);
    //        }
    //        else
    //        {
    //            ShowLoginButton(false);
    //             // 카카오 로그인에 성공했으면 게스트 데이터 정보 삭제 
    //            SnsDataGuest.ClearCachedLogin();
    //        }
    //    });
    //}

    public void OnGuestLoginButton()
    {
        Debug.Log("[GUEST_LOGIN] Pushed login button!");

        // 게스트 아이디로 로그인 할 경우 게임 삭제, 디바이스 변경, 탈퇴 시 데이터가 삭제될 수 있습니다 
        SimplePopup.Confirm("게스트 아이디로 로그인 할 경우 게임 삭제, 디바이스 변경, 탈퇴 시 데이터가 삭제될 수 있습니다.", () =>
        {
            ShowLoginButton(false);
            //LoginAsGuest();
        }, null, "경고", "예", "아니요");
    }

    /// <summary>
    /// SNS 로그인 버튼 노출 여부 
    /// </summary>
    /// <param name="inShow"></param>
    private void ShowLoginButton(bool inShow = true)
    {
        _btnKakao.gameObject.SetActive(inShow);
        _loadingBar.gameObject.SetActive(!inShow);

#if UNITY_IOS
        _btnGuest.gameObject.SetActive(inShow);
#else
        _btnGuest.gameObject.SetActive(false);
#endif
    }

//#if UNITY_EDITOR
//    /// <summary>
//    /// 테스터로 로그인 
//    /// </summary>
//    private void LoginAsTester()
//    {
//        ShowLoginButton(false);

//        Sns.SetPlatform(Sns.Platform.STANDALONE);
//        Sns.Instance.Init(delegate (bool inSuccess) { });
//        Sns.Instance.Login(null);
//        Sns.Instance.LoadLocalUser();
//        Sns.Instance.LoadFriendsList();
//    }
//#endif

//    /// <summary>
//    /// 게스트로 로그인 
//    /// </summary>
//    private void LoginAsGuest()
//    {
//        ShowLoginButton(false);

//        Sns.SetPlatform(Sns.Platform.GUEST);
//        Sns.Instance.Init(delegate (bool inSuccess) { });
//        Sns.Instance.Login(null);
//        Sns.Instance.LoadLocalUser();
//        Sns.Instance.LoadFriendsList();
//    }

//    /// <summary>
//    /// GS 서버 로그인 
//    /// </summary>
//    /// <param name="inStaticsVersions">로컬에 캐싱된 Statics의 각 시트별 버전 정보</param>
//    /// <param name="reInit"></param>
//    /// <returns></returns>
//    private IEnumerator LoginGameServiceServer(string inStaticsVersions, bool reInit = false)
//    {
//        UnityEngine.Debug.Log("[INIT_STEP] LoginGameServiceServer...");

//        _isLoginToGameServiceServer = false;
//        Server.Instance.init(OnInitializedServer, inStaticsVersions, reInit);

//        while (!_isLoginToGameServiceServer)
//        {
//            yield return null;
//        }

//        if (ServerModel.Instance.user_info.status != EUserStatus.NORMAL.ToString())
//        {
//            bool next = false;

//            // 블록된 유저 처리.
//            if (ServerModel.Instance.user_info.status == EUserStatus.BLOCKED.ToString())
//            {
//                SimplePopup.Alert(("E_USER_BANNED").Locale(), delegate ()
//                {
//                    Application.Quit();
//                });

//                while (!next)
//                {
//                    yield return null;
//                }
//            }
//            // 탈퇴한 유저 처리.
//            else if (ServerModel.Instance.user_info.status == EUserStatus.UNREGISTERED.ToString())
//            {
//                SimplePopup.Confirm("WITHDRAWALING_EXPLAIN_MESSAGE".Locale(), 
//                () =>   // 탈퇴 취소 
//                {
//                    next = true;

//                    _isLoginToGameServiceServer = false;
//                    Server.Instance.init_re(OnReinitializedServer);
//                },
//                () =>   // 탈퇴 취소 안함 
//                {
//                    Debug.Assert(KakaoService.state == KakaoService.ELoginState.LOGIN);
//                    KakaoService.Logout(delegate (KakaoService.ELoginState inState)
//                    {
//                        if (inState != KakaoService.ELoginState.LOGOUT)
//                        {
//                            Debug.LogError("[STZLOGIN_MODULE] failed to logout KAKAO");
//                        }

//                        GeneralDataManager.it.NextScene(GeneralDataManager.ESceneName.Title);
//                    });
//                }, "TEXT_WITHDRAWALING".Locale(), "BT_CANCEL_WITHDRAWAL".Locale());

//                while (!next)
//                {
//                    yield return null;
//                }

//                yield return StartCoroutine(LoginGameServiceServer(inStaticsVersions, true));
//            }
//        }

//        // 탈퇴 철회자를 위한 대기 
//        while (!_isLoginToGameServiceServer)
//        {
//            yield return null;
//        }
//    }

//    /// <summary>
//    /// 유저서버의 응답 처리 
//    /// </summary>
//    /// <param name="isSuccess"></param>
//    private void OnInitializedServer(bool isSuccess)
//    {
//        if (!isSuccess)
//        {
//            string message = string.Format("사용자 정보를 가져오는데 실패 했습니다.\n네트워크 상태를 확인하고 다시 시도해 주세요.\n[{0}]", ServerModel.Instance.latestErrorCode);

//            Debug.LogError(message);

//#if UNITY_EDITOR
//            Application.Quit();
//#endif // UNITY_EDITOR

//            SimplePopup.Alert(message, () => Application.Quit(), null, true);
//            return;
//        }

//        Kochava.Tracker.SendEvent("GameServer_Login", null);
//        // 기존 접속했던 디바이스가 아니라면 || playerID가 변경되었다면 
//        if (!Config.IsEditorService && (string.Compare(SystemInfo.deviceUniqueIdentifier, ServerModel.Instance.user_info.device_id) != 0
//            || string.Compare(ServerModel.Instance.user_info.player_id, Sns.Instance.userInfo.playerId) != 0))
//        {
//            // NOTE @neec 사용자에게 이전 디바이스에 저장된 내용이 유실될 수 있다는 경고를 노출해 동의를 확인한 이후 재호출 
//            Server.Instance.init_re(OnReinitializedServer);
//            return;
//        }

//        // 디버깅을 쉽게 하기 위해 유저 아이디 추가
//        StzLog.Logger.SetupAddtionalData("User ID", ServerModel.Instance.user_info.id.ToString());

//        ServerModel.Instance.timeSimulator.Start();
//        _isLoginToGameServiceServer = true;
//    }

    /// <summary>
    /// 변경된 디바이스로 유저서버 응답 처리 
    /// </summary>
    /// <param name="isSuccess"></param>
    private void OnReinitializedServer(bool isSuccess)
    {
        if (isSuccess)
        {
            //OnInitializedServer(true);
        }
        else
        {
            ShowLoginButton(true);
        }
    }

	/// <summary>
	/// 기본 데이터 파일 패스
	/// </summary>
	/// <value>The essenstial file path.</value>
	//string essenstialFilePath { get { return string.Format("{0}/{1}.dat", Application.temporaryCachePath, StringHelper.Format("CachedEssenstialInfo_{0}", ServerModel.Instance.user_info.id).MD5()); } }

	/// <summary>
	/// 기본 데이터 시크릿 키
	/// </summary>
	/// <value>The essenstial secret key.</value>
	string essenstialSecretKey { get { return  "ESSENSTIAL-INFO"; } }

    /// <summary>
    /// 기본 데이터 로드
    /// </summary>
    /// <returns>The cashed essenstial info.</returns>
    //CachedEssenstialInfo LoadCashedEssenstialInfo()
    //{
    //	return FileIOExtension.LoadFromFile<CachedEssenstialInfo>(essenstialFilePath, essenstialSecretKey);
    //}

    ///// <summary>
    ///// 기본 데이터 저장
    ///// </summary>
    ///// <param name="inInfo">In info.</param>
    //void SaveCashedEssenstialInfo(CachedEssenstialInfo inInfo)
    //{
    //       UnityEngine.Debug.LogFormat("SavePushTokonInfo : ver({0}) token({1})", inInfo.pushTokenVersion, inInfo.pushToken);
    //	FileIOExtension.SaveAsFile(inInfo, essenstialFilePath, essenstialSecretKey);
    //}

    /// <summary>
    /// 마지막으로 입장한 스테이지를 불러온다
    /// <see cref="WorldMapScene.SaveEnterSeason()"/>
    /// </summary>
    /// <returns></returns>
    //    GeneralDataManager.EWorldMapType LoadLastStage()
    //    {
    //        GeneralDataManager.EWorldMapType type = GeneralDataManager.EWorldMapType.S1_WorldMap;
    //        ServerData.UserFlag flag = ServerModel.Instance.user_flag_list.Where(x => x.key == "TUTORIAL_CLEAR_IDS" && x.value.Contains(((int)TutorialManager.ETutorialType.SEASON2).ToString())).FirstOrDefault();
    //        if (flag != null)
    //        {
    //            type = GeneralDataManager.EWorldMapType.S2_WorldMap;
    //        }

    //        string path = STZCommon.GetModelPath("lastSeason");
    //        if (ServerModel.Instance.registered)
    //        {
    //            type = GeneralDataManager.EWorldMapType.S2_WorldMap;
    //        }
    //        else if (System.IO.File.Exists(path))
    //        {
    //            return FileIOExtension.LoadFromFile<bool>(path, path) ? GeneralDataManager.EWorldMapType.S1_WorldMap : GeneralDataManager.EWorldMapType.S2_WorldMap;
    //        }
    //        return type;
    //    }

    //    /// <summary>
    //    /// 필수적으로 등록해야 할 정보들 등록 처리 
    //    /// I/O를 줄이기 위해 배치 호출을 이용함 
    //    /// </summary>
    //    private IEnumerator RegisterEssenstialInfo()
    //    {
    //        UnityEngine.Debug.Log("[INIT_STEP] RegisterEssenstialInfo...");

    //        bool isDone = false;
    //        SimpleJSON.JSONArray calls = new SimpleJSON.JSONArray();
    //		CachedEssenstialInfo cached = LoadCashedEssenstialInfo();
    //        if (cached == null)
    //        {
    //            cached = new CachedEssenstialInfo();
    //        }

    //        GetAdditionalInitServerRequest(calls);

    //        // 토큰 업데이트를 위해 static에서 버전을 가져옴
    //        string key = "push_token_newest_version";
    //        int newest_pushTokenVer = 1;
    //        if (Statics.Instance.systems.ContainsKey(key))
    //        {
    //            newest_pushTokenVer = Statics.Instance.systems[key].intValue;
    //        }

    //        // 푸시토큰이 등록된 기록이 없다면 또는 저장된 버전이 static에 정의된 최신버전보다 낮으면
    //        if (cached == null || cached.pushToken == null || cached.pushToken.Length == 0 || cached.pushTokenVersion < newest_pushTokenVer)
    //        {
    //            string pushToken = null;
    //            string os = "a";
    //            isDone = false;
    //#if (!UNITY_EDITOR && UNITY_ANDROID)
    //		    SundaytozGcmExtension.Instance.GcmRegister(Config.GCM_SENDER_ID, delegate(string inStatus, JSONNode inResult)
    //            {
    //			    pushToken = inResult[StzGcmStringKeys.Params.registration_id];
    //                Debug.Log("PushToken: " + pushToken);
    //                isDone = true;
    //		    });
    //#elif (!UNITY_EDITOR && UNITY_IOS)
    //		    SundaytozNativeExtension.Instance.GetPushId(delegate (int inStatus, JSONNode inResult)
    //            {
    //			    pushToken = inResult[StzGcmStringKeys.Params.registration_id];
    //                Debug.Log("PushToken: " + pushToken);
    //                os = "i";
    //                isDone = true;
    //		    }, 
    //		    delegate(int inStatus, string error)
    //            {
    //			    Debug.Log("PushToken: " + error);
    //                pushToken = null;
    //                isDone = true;
    //		    });
    //#else // UNITY_EDITOR
    //            // 로컬 테스트 
    //            pushToken = null;
    //            isDone = true;
    //#endif

    //            float time = 0;
    //            while (!isDone)
    //            {
    //                // 0.5초 동안 응답이 없다면 
    //                if (time > 0.5f)
    //                {
    //                    break;
    //                }

    //                time += Time.deltaTime;

    //                yield return null;
    //            }

    //            if (pushToken != null)
    //            {
    //                cached.pushToken = pushToken;
    //                cached.pushTokenVersion = newest_pushTokenVer;
    //                calls.Add(Server.Instance.register_pushtoken(null, os, pushToken, true).PublishToJsonClass());
    //            }
    //        }

    //        if (calls.Count > 0)
    //        {
    //            isDone = false;

    //            Server.Instance.apbatch(delegate (bool inSuccess)
    //            {
    //                // Unity Info Log를 활성화
    //                switch (Config.Deploy)
    //                {
    //                    case Config.DEPLOY.DEV:
    //                    case Config.DEPLOY.STAGE:
    //                        Settings.Installer.Install();
    //                        break;

    //                    case Config.DEPLOY.SERVICE:
    //                    default:
    //                        break;
    //                }

    //                // 접속 이전 스테이지로 접속
    //                {
    //                    GeneralDataManager.it.currentWorldMapType = LoadLastStage();
    //                }

    //                // 시즌 1
    //                ServerData.StageInfo s1StageInfo = FileIOExtension.Copy<ServerData.StageInfo>(ServerModel.Instance.stage_info);
    //                ServerModel.Instance.stage_info_dic.Add(GeneralDataManager.EWorldMapType.S1_WorldMap.ToString(), s1StageInfo);

    //                // 시즌 2
    //                Server.Instance.stage2_info(_isSuccess =>
    //                {
    //                    ServerModel.Instance.stage_info_dic.Add(GeneralDataManager.EWorldMapType.S2_WorldMap.ToString(), FileIOExtension.Copy<ServerData.StageInfo>(ServerModel.Instance.stage_info));
    //                    if (GeneralDataManager.IsSeason1())
    //                    {
    //                        ServerModel.Instance.stage_info = s1StageInfo;
    //                    }
    //                });

    //                if (!Sns.Instance.isGuest && Statics.Instance.systems.ContainsKey("local_cache_remove_on_off"))
    //                {
    //                    // NOTE @minjun 해당 플래그가 없거나 Statics 시트에 번호보다 낮으면 모든 캐시를 지운다
    //                    int cacheValue = Statics.Instance.systems["local_cache_remove_on_off"].intValue;
    //                    ServerData.UserFlag flag = ServerData.UserFlag.GetFlag(ServerData.UserFlag.EFlagKey.REMOVE_ALL_CACHE);
    //                    if (flag != null && flag.value.IntValue() >= cacheValue)
    //                    {
    //                        isDone = true;
    //                    }
    //                    else
    //                    {
    //                        Server.Instance.userflag_update(_success =>
    //                        {
    //                            isDone = true;
    //                        }, ServerData.UserFlag.EFlagKey.REMOVE_ALL_CACHE.ToString(), cacheValue.ToString());

    //                        // 삭제 모드 설정
    //                        try
    //                        {
    //                            if (Statics.Instance.systems.ContainsKey("remove_cache_mode"))
    //                            {
    //                                FileIOExtension.RemoveAllFiles(Application.temporaryCachePath, (FileIOExtension.ERemoveMode)Statics.Instance.systems["remove_cache_mode"].intValue);
    //                            }
    //                            else
    //                            {
    //                                FileIOExtension.RemoveAllFiles(Application.temporaryCachePath);
    //                            }
    //                        }
    //                        catch (UnauthorizedAccessException e)
    //                        {
    //                            Debug.Log(StringHelper.Format("UnauthorizedAccessException Data : {0}", e.Data));
    //                        }
    //                        catch (Exception) { }
    //                    }
    //                }
    //                else
    //                {
    //                    isDone = true;
    //                }
    //            }, calls);

    //            while (!isDone)
    //            { yield return null; }

    //            SaveCashedEssenstialInfo(cached);
    //        }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <returns></returns>
    //    private bool _isUpdatedFriendSet;
    //    private IEnumerator SetupGameServiceServerModel()
    //    {
    //        UnityEngine.Debug.Log("[INIT_STEP] SetupGameServiceServerModel...");

    //        _isUpdatedFriendSet = false;

    //        if (!Config.IsEditorService)
    //        {
    //            UpdateFriendSet((isSuccess) =>
    //            {
    //                _isUpdatedFriendSet = true;
    //            });
    //        }
    //        else
    //            _isUpdatedFriendSet = true;

    //        while (!_isUpdatedFriendSet)
    //        {
    //            yield return new WaitForEndOfFrame();
    //        }

    //        yield return StartCoroutine(SetupAdditionalGameServiceServerModel());
    //    }

    //    /// <summary>
    //    /// 친구 목록 갱신 
    //    /// </summary>
    //    /// <param name="inOnComplete"></param>
    //    private void UpdateFriendSet(ServerRequestManager.Callback inOnComplete)
    //    {
    //        // 캐싱된 친구목록을 로드하지 못했다면
    //        if (!ServerModel.Instance.LoadFriendSetFromFile())
    //        {
    //            // 카카오 친구목록 전체를 서버에 업데이트 
    //            ServerModel.Instance.AllowUpdateTime();
    //            Server.Instance.friend_list_update(inOnComplete, Sns.Instance.allFriendsIds, null, "all");
    //            return;
    //        }

    //        // NOTE @sangmoon 카카오에서부터 친구목록을 제대로 업데이트 받지 못했다면 서버에 업데이트하지 않고 바로 종료 
    //        // NOTE2 @sangmmon 서버에 등록된 friend key값이 모두 numeric일 경우 kakao Appfriend로 가정한다 
    //        int serverAppFriendCnt = ServerModel.Instance.friend_set.Keys.Where(T => Regex.IsMatch(T, @"^\d+$")).Count();
    //        int serverNoAppFriendCnt = ServerModel.Instance.friend_set.Count - serverAppFriendCnt;
    //        bool invalidFriendData = (serverAppFriendCnt > 0 && Sns.Instance.appFriends.Count == 0)
    //                                 || (serverNoAppFriendCnt > 0 && Sns.Instance.noAppFriends.Count == 0);
    //        if (invalidFriendData)
    //        {
    //            inOnComplete(true);
    //            return;
    //        }

    //        // 최신 SNS 친구 목록
    //        List<string> snsFriends = new List<string>(Sns.Instance.allFriendsIds);

    //        // 로컬에서 관리중이 SNS 친구 목록
    //        List<string> cachedFriends = new List<string>(ServerModel.Instance.friend_set.Keys);

    //        // 새로 추가된 친구 목록
    //        List<string> added = snsFriends.FindAll((platformId) => !ServerModel.Instance.friend_set.ContainsKey(platformId) 
    //                                                                || (ServerModel.Instance.friend_set.ContainsKey(platformId) 
    //                                                                    && Sns.Instance.appFriends.ContainsKey(platformId) 
    //                                                                    && ServerModel.Instance.friend_set[platformId].user_id.IsNullOrEmpty()));

    //        // 제거된 친구 목록
    //        List<string> removed = cachedFriends.FindAll((platformId) => (snsFriends.IndexOf(platformId) == -1));

    //        /* TEST @Inkyu.jang dev 상태라면 친구 테스트를 위해 v@stz.com, vt@stz.com을 친구에 추가 */
    //        if (Config.IsDev)
    //        {
    //            const string v_stz = "88187390944877504";       // v@stz.com
    //            const string vt_stz = "89018117094393489";      // vt@stz.com

    //            bool unregisted_v = !added.Contains(v_stz) && !ServerModel.Instance.friend_set.ContainsKey(v_stz);
    //            if (unregisted_v)
    //                added.Add(v_stz);

    //            bool unregisted_vt = !added.Contains(vt_stz) && !ServerModel.Instance.friend_set.ContainsKey(vt_stz);
    //            if (unregisted_vt)
    //                added.Add(vt_stz);

    //            removed.Remove(v_stz);
    //            removed.Remove(vt_stz);
    //        }

    //        // 추가 혹은 제거된 SNS 친구가 있다면 
    //        if (added.Count > 0 || removed.Count > 0)
    //        {
    //            Debug.Log("[FRIEND] Added SNS friend(s) : " + added.ToArray().Join());
    //            Debug.Log("[FRIEND] Removed SNS friend(s) : " + removed.ToArray().Join());

    //            ServerModel.Instance.friend_set.Clear();
    //            Server.Instance.friend_list_update(inOnComplete, added.ToArray(), removed.ToArray(), "all");
    //        }
    //        else
    //        {
    //            inOnComplete(true);
    //        }
    //    }

    private void UpdateLoadingInfo(int current, int limit, string inMessage = null)
    {
        float gaugeRatio = 0.0f;
        gaugeRatio = current / (float)limit;

        //string msg = string.Format("{0} ({1}/{2})", ("LOADING_MSG_" + current).Locale(), current, limit);
        _loadingBar.SetGauge(gaugeRatio, true, inMessage);
    }

    private void SetLoadingInfo(int current, int limit, GaugeBar.Callback inCallback = null)
    {
        float gaugeRatio = 0.0f;
        gaugeRatio = current / (float)limit;

        string msg = string.Format("{0} ({1}/{2})", ("LOADING_MSG_" + current).Locale(), current, limit);
        _loadingBar.SetGauge(gaugeRatio, true, msg, inCallback);
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void StartFromTitle()
    {
        if (!passedTitle)
            GeneralDataManager.it.NextScene(GeneralDataManager.ESceneName.Title);
    }

    /*
    /// <summary>
    /// 응답내용 출력 
    /// </summary>
    /// <param name="inStatus"></param>
    private void OutputKakaoServiceResponse(KakaoService.ELoginState inStatus)
    {
        switch (inStatus)
        {
            case KakaoService.ELoginState.LOGIN:
                {
                    Debug.Log("LOGINED!");
                    break;
                }

            case KakaoService.ELoginState.LOGOUT:
                {
                    Debug.Log(string.Format("Kakao SDK {0}\n", Kakao.Key.version));
                    Debug.Log("LOGOUTED");
                    break;
                }

            case KakaoService.ELoginState.UNREGISTERED:
                {
                    Debug.Log("UNREGISTERED");
                    break;
                }

            case KakaoService.ELoginState.ERROR:
                {
                    switch (KakaoService.lastLoginErrorCode)
                    {
                        case Kakao.ELoginError.CANCELED_OPERATION:
                            {
                                Debug.Log("CANCELEED");
                                break;
                            }
                        case Kakao.ELoginError.ILLEGAL_ARGUMENT:
                        case Kakao.ELoginError.MISS_CONFIGURATION:
                            {
                                // 실패했지만 팝업으로 보여주진 않는다 
                                UnityEngine.Debug.LogWarningFormat("{0}: {1}", KakaoService.lastLoginErrorCode, KakaoService.lastLoginErrorMessage);
                                break;
                            }

                        case Kakao.ELoginError.AUTHORIZATION_FAILED:
                            {
                                SimplePopup.Alert("네트워크 상태가 불안정하거나 시스템 및 시간 설정으로 인해 로그인 하실 수 없습니다.\n[환경설정>날짜 및 시간]에서 현재 시간으로 변경해주세요.");
                                break;
                            }

                        case Kakao.ELoginError.UNSPECIFIED_ERROR:
                            {
                                SimplePopup.Alert("절전모드를 끄고 네트워크 상태를\n확인한 후 다시 로그인해주세요.");
                                break;
                            }

                        default:
                            {
                                SimplePopup.Alert(StringHelper.Format("{0}: {1}", KakaoService.lastLoginErrorCode, KakaoService.lastLoginErrorMessage));
                                break;
                            }
                    }
                    break;
                }

            default:
                {
                    SimplePopup.Alert("UNKNOWN STATE: " + KakaoService.state.ToString());
                    break;
                }
        }
    }*/

    protected abstract void NextScene();

    //protected abstract IEnumerator SetupAdditionalGameServiceServerModel();

    //protected abstract IEnumerator Co_CheckAssetBundle();

    protected virtual void GetAdditionalInitServerRequest(SimpleJSON.JSONArray outArr)
    {

    }
}

