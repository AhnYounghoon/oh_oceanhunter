﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarSet : MonoBehaviour {
	[SerializeField]
	float _space;

	[SerializeField]
	Image[] _stars;

	// Use this for initialization
	void Awake () 
	{
		AwakeStars(0);
	}

	public void AwakeStars(int inCount)
	{
		int starCount = Mathf.Clamp(inCount, 0, _stars.Length);
		float initX = -(_space / 2) * (starCount - 1);
		for (int i = 0; i < _stars.Length; i++)
		{
			_stars[i].gameObject.SetActive(i < starCount);
			RectTransform rect = _stars[i].Get<RectTransform>();
			Vector2 pos = rect.anchoredPosition;
			pos.x = initX + (i * _space);
			rect.anchoredPosition = pos;
		}
	}

	public void SetColor(Color inColor)
	{
		for (int i = 0; i < _stars.Length; i++)
		{
			_stars[i].color = inColor;
		}
	}
}
