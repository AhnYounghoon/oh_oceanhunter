﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using STZFramework;

/// <summary>
/// 공용 게이지 클래스.
/// </summary>

public class GaugeBar : STZBehaviour
{
    public delegate void Callback(float ratio);
    protected Callback _callback;
    public Callback frameCallback { set; get; }

    public enum EScrollDirection
    {
        Vertical,
        Horizontal,
    }

	[SerializeField]
    Image _gaugeInner;                                           // 내부 게이지.
   
	[SerializeField]
	EScrollDirection _scrollDir = EScrollDirection.Horizontal;   // 가로, 세로 모드 여부.
    
	[SerializeField]
	float _gaugeSpeed = 2f;	     					            // 게이지가 차오르는 스피드.
    
	[SerializeField]
	Text _gaugeBarText = null;                                   // 게이지 내부 텍스트.

	[SerializeField]
	STZBitmapFont _gaugeBarBitmapText = null;

    protected float _innerGaugeMinWidth;                                // 내부 게이지가 가장 작을 때 크기.
    protected float _innerGaugeMaxWidth;                                // 내부 게이지가 가장 클 때 크기.
    protected bool _animating = false;                                  // 게이지 애니메이션 여부.
    protected float _currentRatio = 0;                                  // 게이지 애니메이션 모드시 현재 값.
    protected float _resultRatio = 0;                                   // 게이지 애니메이션 모드시 결과 값.
    protected RectTransform _innerGaugeRectTransform;                   // 내부 게이지 Transform.

    /// <summary>
    /// 초기화.
    /// </summary>

    protected virtual void Awake()
    {
        //Rect rect = GetComponent<RectTransform>().rect;
        _innerGaugeRectTransform = _gaugeInner.GetComponent<RectTransform>();

        var rect = GetComponent<RectTransform>().rect;
        switch (_scrollDir)
        {
            case EScrollDirection.Horizontal:
                _innerGaugeMinWidth = -rect.width + _gaugeInner.sprite.textureRect.width + _innerGaugeRectTransform.offsetMin.x;
                _innerGaugeMaxWidth = _innerGaugeRectTransform.offsetMax.x;
                break;

            case EScrollDirection.Vertical:
                _innerGaugeMinWidth = -rect.height + _gaugeInner.sprite.textureRect.height + _innerGaugeRectTransform.offsetMin.y;
                _innerGaugeMaxWidth = _innerGaugeRectTransform.offsetMax.y;
                break;
        }

        Reset();
    }

    /// <summary>
    /// 리셋. 게이지 크기를 1로 맞춤.
    /// </summary>

    public void Reset()
    {
        SetGauge(1f);
    }

    /// <summary>
    /// 업데이트
    /// </summary>

    void Update()
    {
        // 게이지의 길이를 업데이트를 이용해 늘이거나 줄임.
        if (_animating)
        {
            UpdateGauge(_currentRatio + (_resultRatio - _currentRatio) * Time.smoothDeltaTime * _gaugeSpeed);
            if (Mathf.Abs(_currentRatio - _resultRatio) < 0.001f)
            {
                _animating = false;

                if (_callback != null)
                {
                    _callback(_currentRatio);
                }
            }
        }
    }

    protected Vector2 _gaugePos = Vector2.zero;

    /// <summary>
    /// 게이지 크기 설정.
    /// </summary>
    /// <param name="ratio">게이지 크기 (0 ~ 1 범위)</param>

    protected virtual void UpdateGauge(float ratio)
    {
        if (_innerGaugeRectTransform == null)
            return;

        _currentRatio = Mathf.Clamp(ratio, 0, 1);
        _gaugeInner.gameObject.SetActive(_currentRatio > 0);

        switch (_scrollDir)
        {
            case EScrollDirection.Horizontal:
                _gaugePos.x = _innerGaugeMinWidth + (_innerGaugeMaxWidth - _innerGaugeMinWidth) * _currentRatio;
                _gaugePos.y = _innerGaugeRectTransform.offsetMax.y;
                _innerGaugeRectTransform.offsetMax = _gaugePos;
                break;

            case EScrollDirection.Vertical:
                _gaugePos.x = _innerGaugeRectTransform.offsetMax.x;
                _gaugePos.y = _innerGaugeMinWidth + (_innerGaugeMaxWidth - _innerGaugeMinWidth) * _currentRatio;
                _innerGaugeRectTransform.offsetMax = _gaugePos;
                break;
        }

        if (frameCallback != null)
            frameCallback(_currentRatio);
    }

    /// <summary>
    /// 게이지 크기 설정.
    /// </summary>
    /// <param name="ratio">게이지 크기</param>
    /// <param name="animated">게이지 애니메이션 여부</param>
    /// <param name="text">게이지 내부 텍스트</param>
    /// <param name="callback">콜백함수</param>

    public void SetGauge(float ratio, bool animated = false, string text = null, Callback callback = null)
    {
        _animating = animated;
        _callback = callback;

        if (_animating)
        {
            _resultRatio = ratio;
            if (frameCallback != null)
                frameCallback(_currentRatio);
        }
        else
        {
            UpdateGauge(ratio);
            if (_callback != null)
            {
                _callback(_resultRatio);
            }
        }

        if (_gaugeBarText != null)
        {
            _gaugeBarText.text = (text == null) ? string.Empty : text;
        }

        if (_gaugeBarBitmapText != null)
		{
			_gaugeBarBitmapText.text = (text == null) ? string.Empty : text;
		}
    }

    public bool TextEnabled 
    {
        set
        {
            if (_gaugeBarText != null)
            {
                _gaugeBarText.gameObject.SetActive(value);
            }

            if (_gaugeBarBitmapText != null)
            {
                _gaugeBarBitmapText.gameObject.SetActive(value);
            }
        }

        get
        {
            if (_gaugeBarText != null)
            {
                return _gaugeBarText.gameObject.activeSelf;
            }

            if (_gaugeBarBitmapText != null)
            {
                return _gaugeBarBitmapText.gameObject.activeSelf;
            }

            return false;
        }
    }
}
