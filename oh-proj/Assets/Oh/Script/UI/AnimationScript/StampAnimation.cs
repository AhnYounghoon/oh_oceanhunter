﻿using UnityEngine;

using System;

using STZFramework;

/// <summary>
/// 스탬프 도장에 대한 애니메이션을 담당하는 behavior
/// </summary>
[RequireComponent(typeof(Animator))]
public class StampAnimation : MonoBehaviour
{
    GameObject stamp;
    GameObject stampLight;

    Action _endEvent;
    Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();

        stamp = transform.Find("stamp_1").gameObject;
        stampLight = transform.Find("stamp_light_1").gameObject;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCallback"></param>
    public void PlayAnimation(Action inCallback)
    {
        SingletonController.Get<SoundManager>().Play("UI_Stamp", "sfx_ui");
        _animator.enabled = true;
        Debug.Assert(_endEvent == null);
        _endEvent = inCallback;
    }

    /// <summary>
    /// 정지된 상태로 보여줌 
    /// </summary>
    public void ShowAsPause()
    {
        _animator.enabled = false;

        stamp.SetActive(true);
        stamp.transform.localScale = Vector3.one;

        stampLight.SetActive(false);
    }

    /// <summary>
    /// 
    /// </summary>
    public void OnEndAnimation()
    {
        if (_endEvent != null)
        {
            _endEvent();
            _endEvent = null;
        }

        _animator.enabled = false;
    }
}
