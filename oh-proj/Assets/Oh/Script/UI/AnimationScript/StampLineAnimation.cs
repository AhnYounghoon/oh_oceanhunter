﻿using UnityEngine;

using System;

using STZFramework;

/// <summary>
/// 출석부에 쓰이는 스탬부 도장에 대한 애니메이션을 담당하는 behavior 
/// </summary>
[RequireComponent(typeof(Animator))]
public class StampLineAnimation : MonoBehaviour
{
    GameObject  _stampLine;

    Action      _endEvent;
    Animator    _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();

        _stampLine = transform.Find("stamp_line_1").gameObject;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCallback"></param>
    public void PlayAnimation(Action inCallback)
    {
        _animator.enabled = true;
        Debug.Assert(_endEvent == null);
        _endEvent = inCallback;
    }

    /// <summary>
    /// 정지된 상태로 보여줌 
    /// </summary>
    public void ShowAsPause()
    {
        _animator.enabled = false;

        _stampLine.SetActive(true);
        _stampLine.transform.localScale = Vector3.one;
    }

    /// <summary>
    /// 
    /// </summary>
    public void Hide()
    {
        _animator.enabled = false;

        _stampLine.SetActive(false);
    }

    /// <summary>
    /// 
    /// </summary>
    public void OnEndAnimation()
    {
        if (_endEvent != null)
        {
            _endEvent();
            _endEvent = null;
        }

        _animator.enabled = false;
    }
}

