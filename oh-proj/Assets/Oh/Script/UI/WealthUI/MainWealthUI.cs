﻿using UnityEngine;
using System.Collections.Generic;

using STZFramework;


/// <summary>
/// 재화 UI관리 클래
/// 팝업을 기준으로 over나 under로 위치함 
/// </summary>
public class MainWealthUI : MonoBehaviour, IMainWealthUI
{
    private static IMainWealthUI _instance = null;

    public static IMainWealthUI it
    {
        get
        {
            if (_instance == null)
            {
                GameObject mainWealthGO = Instantiate(Resources.Load<GameObject>("Prefabs/MainWealthUI"));
                mainWealthGO.name = "MainWealthUI";
                DontDestroyOnLoad(mainWealthGO);
                _instance = mainWealthGO.GetComponent<MainWealthUI>();
            }

            return _instance;
        }
    }

    private const string UNDER_LAYER_NAME = "UNDER_POPUP";
    private const string OVER_LAYER_NAME = "OVER_POPUP";

    private Canvas      _canvas;
    private GameObject  _settingBtnRoot;

    private StateData _currentState = new StateData() { bActive = false, bIsOverPopup = false };

    private Stack<StateData> _stateDataStack = new Stack<StateData>();

    private bool _bDestoryed = false;

    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        _canvas = GetComponent<Canvas>();

        //_settingBtnRoot = transform.Find("Btn_Setting").gameObject;
    }

    void OnDestroy()
    {
        _bDestoryed = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inCamera"></param>
    public void SetUpCamera(Camera inCamera)
    {
        _canvas.worldCamera = inCamera;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inIsOverPopup"></param>
    /// <param name="inSettingsButton"></param>
    public void Show(bool inIsOverPopup = false, bool inSettingsButton = true)
    {
        _stateDataStack.Push(new StateData() { bActive = _currentState.bActive, bIsOverPopup = _currentState.bIsOverPopup, bActiveSettingBtn = _currentState.bActiveSettingBtn });

        gameObject.SetActive(true);
        _canvas.sortingLayerName = inIsOverPopup ? OVER_LAYER_NAME : UNDER_LAYER_NAME;
        //_settingBtnRoot.SetActive(inSettingsButton);

        _currentState.bActive = true;
        _currentState.bIsOverPopup = inIsOverPopup;
        _currentState.bActiveSettingBtn = inSettingsButton;
    }

    /// <summary>
    /// 
    /// </summary>
    public void Hide()
    {
        if (_bDestoryed)
        {
            return;
        }

        _stateDataStack.Clear();
        _stateDataStack.Push(new StateData() { bActive = _currentState.bActive, bIsOverPopup = _currentState.bIsOverPopup, bActiveSettingBtn = _currentState.bActiveSettingBtn });

        gameObject.SetActive(false);
        _currentState.bActive = false;
    }

    /// <summary>
    /// 
    /// </summary>
    public void Rewind()
    {
        if (_stateDataStack.Count == 0)
        {
            Debug.LogWarning("[MAIN_WEALTH_UI] _pre state data count is 0");
            return;
        }
        
        // apply 
        _currentState = _stateDataStack.Pop();
        gameObject.SetActive(_currentState.bActive);
        _canvas.sortingLayerName = _currentState.bIsOverPopup ? OVER_LAYER_NAME : UNDER_LAYER_NAME;
        //_settingBtnRoot.SetActive(_currentState.bActiveSettingBtn);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Vector3 GetHeartWealthPosition()
    {
        return Vector3.zero;
    }

    /// <summary>
    /// 
    /// </summary>
    private struct StateData
    {
        public bool bActive;
        public bool bIsOverPopup;
        public bool bActiveSettingBtn;
    }
}

