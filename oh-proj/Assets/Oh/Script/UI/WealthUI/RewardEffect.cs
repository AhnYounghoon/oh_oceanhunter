﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class RewardEffect : MonoBehaviour
{
    [SerializeField]
    STZBitmapFont _font;

    [SerializeField]
    Image _icon;

    [SerializeField]
    CanvasGroup _board;

    float _space = 10;

    Sequence _seq;

    public void Show(Sprite inIcon, int inValue, string inSoundKey, bool isOriginalSize = true, bool isFadeOut = true)
    {
        gameObject.SetActive(true);

        if (inIcon != null)
        {
            _icon.sprite = inIcon;
            if (isOriginalSize)
                _icon.Get<RectTransform>().sizeDelta = inIcon.bounds.size;
        }

        _font.text = StringHelper.Format("+{0:N0}", inValue);

        float iconWidth = _icon.Get<RectTransform>().sizeDelta.x * _icon.transform.localScale.x;
        float size = (iconWidth + _space + _font.width);

        Vector2 v = _board.gameObject.Get<RectTransform>().sizeDelta;
        v.x = size;
        _board.gameObject.Get<RectTransform>().sizeDelta = v;

        _board.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        if (_seq != null && _seq.IsActive())
            _seq.Kill(true);

        _board.alpha = 1;
        _seq = DOTween.Sequence();
        _seq.Append(_board.GetComponent<RectTransform>().DOAnchorPosY(30, 2f));
        if (isFadeOut)
            _seq.Join(_board.DOFade(0, 1f).SetDelay(1));
        _seq.AppendCallback(() => {
            if (isFadeOut)
                gameObject.SetActive(false);
        });

        if (inSoundKey != null)
        {
            SingletonController.Get<SoundManager>().Play(inSoundKey, "sfx_ui");
        }
    }

    void OnDestroy()
    {
        if (_seq != null && _seq.IsActive())
            _seq.Kill(true);
    }
}
