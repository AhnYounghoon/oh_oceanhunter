﻿using UnityEngine;


public interface IMainWealthUI
{
    /// <summary>
    /// 
    /// </summary>
    void SetUpCamera(Camera inCamera);

    /// <summary>
    /// 활성화 여부 
    /// </summary>
    /// <param name="inIsOverPopup"></param>
    /// <param name="inSettingButton"></param>
    void Show(bool inIsOverPopup = false, bool inSettingButton = true);

    /// <summary>
    /// 
    /// </summary>
    void Hide();

    /// <summary>
    /// 변화되기 직전의 상태로 되돌림 
    /// </summary>
    void Rewind();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Vector3 GetHeartWealthPosition();

}

