﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class BaseWealthUI : STZBehaviour, IHashReceiver
{
    protected Text          _text;
    protected Button        _btnShop;
    protected Image         _icon;

    protected float _currentValue = 0;
    protected float _resultValue = 0;
    protected bool _isShowPopup = false;
    protected RewardEffect _rewardEffect;

    protected System.DateTime _lastUpdateTime = STZCommon.Time1970;

    public    string    text { get { return _text.text; } set { if (!_text) { Debug.Assert(false, "[WealthUI] Text가 없습니다."); return; } _text.text = value; } }

    protected virtual void Awake()
    {
        _text = this.Get<Text>("Text");
        _icon = this.Get<Image>("Icon");
        _btnShop = this.Get<Button>();

        if (_btnShop != null)
            _btnShop.onClick.AddListener(OnClickButton);

        _rewardEffect = this.Get<RewardEffect>("Reward");
        //ServerModel.Instance.AddEventListener("user_attr", UpdateServerModel);
        ObserverManager.it.AddEventListener(this, EObserverEventKey.GOODS_UPDATE);
    }

    public void ReceiveEvent(EObserverEventKey inEventKey, Hashtable inParam)
    {
        if (inEventKey == EObserverEventKey.GOODS_UPDATE)
        {
            CheckData();
        }
    }

    protected virtual void OnDestroy()
    {
        //ServerModel.Instance.RemoveEventListener("user_attr", UpdateServerModel);
        ObserverManager.it.RemoveEventListener(this, EObserverEventKey.GOODS_UPDATE);
    }

    public void SetTextColor(Color color)
    {
        _text.color = color;
    }

    public abstract void OnClickButton();

    void Update()
    {
        if (Mathf.Abs(_resultValue - _currentValue) > 0.01f)
        {
            _currentValue += (_resultValue - _currentValue) * 0.3f;
            UpdateData(_currentValue);
        }
    }

    protected void UpdateServerModel(object server, STZFramework.EventParameters args)
    {
        CheckData();
    }

    protected abstract void CheckData();

    protected void ResetValue(int inResultValue)
    {
        _resultValue = inResultValue;
    }

    protected abstract void UpdateData(float inValue);

    void OnDisable()
    {
        if (_rewardEffect != null)
            _rewardEffect.gameObject.SetActive(false);
    }
}