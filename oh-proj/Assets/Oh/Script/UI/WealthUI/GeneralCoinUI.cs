﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GeneralCoinUI : BaseWealthUI
{
    protected override void Awake()
    {
        base.Awake();
        ResetValue(GeneralDataManager.it.GetGoodsData(ERewardType.SILVER));
    }

    public override void OnClickButton()
    {
        //string enteranceKey = null;
        //switch (GeneralDataManager.it.currentSceneName)
        //{
        //    case GeneralDataManager.ESceneName.WorldMap:
        //        enteranceKey = ShopManager.EnteranceKey.WORLD_MAP_TOP_GEM;
        //        break;

        //    case GeneralDataManager.ESceneName.InGame:
        //        enteranceKey = ShopManager.EnteranceKey.INGAME_TOP_GEM;
        //        break;
        //}
        //SingletonController.Get<PopupManager>().Show(typeof(PopupStore), null);
    }

    protected override void CheckData()
    {
        //var rewards = RewardData.ParseMergedItemIds(SingletonController.Get<GameRewardChecker>().lastGotRewards);
        //for (int i = 0, length = rewards.Count; i < length; i++)
        //{
        //    RewardData reward = rewards[i];
        //    if (reward.reward_type == (int)ERewardType.COIN)
        //    {
        //        _icon.transform.DOScale(1.5f, 0.4f).OnComplete(() => { _icon.transform.DOScale(1.0f, 0.4f); });
        //        _rewardEffect.Show(null, reward.reward_count, null);
        //    }
        //}

        //ResetValue(ServerModel.Instance.user_attr.coin.IntValue());
        //_lastUpdateTime = SingletonController.Get<GameRewardChecker>().lastUpdateTime;
        ResetValue(GeneralDataManager.it.GetGoodsData(ERewardType.SILVER));
    }

    protected override void UpdateData(float inUpdateValue)
    {
        text = StringHelper.Format("{0:N0}", inUpdateValue);
    }

    void OnEnable()
    {
        //if (_lastUpdateTime < SingletonController.Get<GameRewardChecker>().lastUpdateTime)
        //    CheckData();
    }
}
