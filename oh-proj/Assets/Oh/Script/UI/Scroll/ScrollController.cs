﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ScrollController : MonoBehaviour
{
    [Tooltip("스크롤 되는 속도")]
    [SerializeField]
    float scrollSpeed;

    /* content의 RectTransform */
    RectTransform _rect;

    /* 스크롤이 완료되는 목적지 */
    float _destPosY = 0;

    /* 스크롤 현재 위치 벡터를 갱신하기 위한 임시 함수 */
    Vector3 _tempCurPosition = Vector3.zero;

    /* 이전 스크롤 위치 */
    float _prevPosY = 0;

    public RectTransform rectTransform { get { if (_rect == null) _rect = GetComponent<RectTransform>(); return _rect; } }

    /// <summary>
    /// 현재 스크롤 위치
    /// </summary>
    public float currentPosY { get { return _rect.localPosition.y; } }

    /// <summary>
    /// 스크롤이 멈춰 있는지 여부
    /// </summary>
    public bool isScrollStable { get { return _prevPosY == _rect.transform.localPosition.y; } }

    /// <summary>
    /// 초기화
    /// </summary>
    void Awake()
    {
        _rect = transform.GetComponent<RectTransform>();
        Debug.Assert(_rect != null, "\"RectTransform\"가 존재하지 않습니다.");
    }

    /// <summary>
    /// 스크롤 시작
    /// </summary>
    /// <param name="destPosY">스크롤이 멈추는 위치</param>
    /// <param name="isAnimatied">스크롤시 애니메이션 여부</param>
    public void PlayScroll(float inDestPosY, bool isAnimatied = false, bool isBounce = false, TweenCallback callback = null)
    {
        transform.DOKill(true);
        if (isAnimatied)
        {
			transform.DOLocalMoveY(inDestPosY, Mathf.Abs((inDestPosY - transform.localPosition.y) / scrollSpeed), false).OnComplete(callback).SetEase(isBounce ? Ease.OutBack : Ease.Linear).SetId(this);
        }
        else
        {
            _tempCurPosition.y = inDestPosY;
            _rect.localPosition = _tempCurPosition;

			if (callback != null)
	            callback();
        }
    }

    /// <summary>
    /// 스크롤 정지
    /// </summary>
    public void StopScroll()
    {
        transform.DOKill(true);
    }

    /// <summary>
    /// 이후 업데이트
    /// </summary>

    void Update()
    {
        LateUpdatePrevPosY();
    }

    /// <summary>
    /// 현재 포지션을 이전 포지션에 기록
    /// </summary>

    void LateUpdatePrevPosY()
    {
        _prevPosY = _rect.transform.localPosition.y;
    }
}
