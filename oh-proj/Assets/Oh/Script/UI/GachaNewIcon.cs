﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GachaNewIcon : MonoBehaviour
{
    [SerializeField]
    Image _icon;

    [SerializeField]
    ERewardType _type;

    void Awake()
    {
        CheckNewIcon();
    }

    public void CheckNewIcon()
    {
        int currGoldBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.GOLD_BOX);
        int currSilverBoxCount = GeneralDataManager.it.GetGoodsData(ERewardType.SILVER_BOX);

        if (_type == ERewardType.NONE)
        {
            _icon.gameObject.SetActive(currGoldBoxCount + currSilverBoxCount > 0);
        }
        else if (_type == ERewardType.GOLD_BOX)
        {
            _icon.gameObject.SetActive(currGoldBoxCount > 0);
        }
        else if (_type == ERewardType.SILVER_BOX)
        {
            _icon.gameObject.SetActive(currSilverBoxCount > 0);
        }
    }
}
