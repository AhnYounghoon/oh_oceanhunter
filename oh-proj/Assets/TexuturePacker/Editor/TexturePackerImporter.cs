﻿/*
 *  TexturePacker Importer
 *  (c) CodeAndWeb GmbH, Saalbaustraße 61, 89233 Neu-Ulm, Germany
 * 
 *  Use this script to import sprite sheets generated with TexturePacker.
 *  For more information see http://www.codeandweb.com/texturepacker/unity
 * 
 *  Thanks to Brendan Campbell for providing a first version of this script!
 *
 */

using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexturePackerImporter : AssetPostprocessor
{
	const string IMPORTER_VERSION = "3.5.0";

	static string[] textureExtensions = {
		".png",
		".jpg",
		".jpeg",
		".tiff",
		".tga",
		".bmp"
	};

	static bool importPivotPoints = EditorPrefs.GetBool("TPImporter.ImportPivotPoints", true);

	/*
	 *  Pivot point import can be disabled in the Preferences dialog (menu item Unity->Preferences, TexturePacker sheet)
	 */
	[PreferenceItem("TexturePacker")]
	static void PreferencesGUI()
	{
		importPivotPoints = EditorGUILayout.Toggle("Always import pivot points", importPivotPoints);
		if (GUI.changed)
			EditorPrefs.SetBool("TPImporter.ImportPivotPoints", importPivotPoints);
	}


	/*
	 *  Trigger a texture file re-import each time the .tpsheet file changes (or is manually re-imported)
	 */
	static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (var asset in importedAssets) {
			if (!Path.GetExtension (asset).Equals (".tpsheet"))
				continue;
			foreach (string ext in textureExtensions) {
				string pathToTexture = Path.ChangeExtension (asset, ext);
				if (File.Exists (pathToTexture)) {
					AssetDatabase.ImportAsset (pathToTexture, ImportAssetOptions.ForceUpdate);
					break;
				}
			}
		}
	}


	/*
	 *  Trigger a sprite sheet update each time the texture file changes (or is manually re-imported)
	 */
	void OnPreprocessTexture ()
	{
		TextureImporter importer = assetImporter as TextureImporter;

		string pathToData = Path.ChangeExtension (assetPath, ".tpsheet");
		if (File.Exists (pathToData)) {
			updateSpriteMetaData (importer, pathToData);
		}
	}

	static void updateSpriteMetaData (TextureImporter importer, string pathToData)
	{
		if (importer.textureType != TextureImporterType.Default) {
			importer.textureType = TextureImporterType.Sprite;
		}
		importer.maxTextureSize = 2048;
        importer.textureCompression = TextureImporterCompression.Uncompressed;

        string[] dataFileContent = File.ReadAllLines(pathToData);
		int format = 30302;

		foreach (string row in dataFileContent)
		{
			if (row.StartsWith(":format=")) {
				format = int.Parse(row.Remove(0,8));
			}
		}

        if (format == 30302)
        {
            //EditorUtility.DisplayDialog("Please update TexturePacker Importer", "Your TexturePacker Importer is too old, \nplease load a new version from the asset store!", "Ok");
            //return;
            importer.spriteImportMode = SpriteImportMode.Multiple;
        }
        // TexturePackerImporter 포맷 버전 대응
        else if (format == 40300)
        {
            importer.spriteImportMode = SpriteImportMode.Polygon;
        }

        Dictionary<string, SpriteMetaData> existingSprites = new Dictionary<string, SpriteMetaData>();
		foreach (SpriteMetaData sprite in importer.spritesheet)
		{
			existingSprites.Add(sprite.name, sprite);
		}

		List<SpriteMetaData> metaData = new List<SpriteMetaData> ();
		foreach (string row in dataFileContent) {
			if (string.IsNullOrEmpty (row) || row.StartsWith ("#") || row.StartsWith (":"))
				continue; // comment lines start with #, additional atlas properties with :

			string [] cols = row.Split (';');
			if (cols.Length < 7)
				return; // format error

			SpriteMetaData smd = new SpriteMetaData ();

			float x = float.Parse (cols [1]);
			float y = float.Parse (cols [2]);
			float w = float.Parse (cols [3]);
			float h = float.Parse (cols [4]);

			smd.rect = new UnityEngine.Rect (x, y, w, h);
            smd.pivot = new UnityEngine.Vector2(float.Parse(cols[5]), float.Parse(cols[6]));
            smd.border = new Vector4(0, 0, 0, 0);

            /* pivot과 border 설정 */
            string[] strArr = cols[0].Split('&');
            smd.name = strArr[0].Replace("/", "-");  // unity has problems with "/" in sprite names...

            if (strArr.Length > 1)
            {
                for (int i = 1; i < strArr.Length; i++)
                {
                    string param = strArr[i];
                    param.Replace(" ", string.Empty);

                    /* 피봇 설정 */
                    if (param.Contains("p="))
                    {
                        string[] parts1 = param.Split('=');
                        string[] parts2 = parts1[1].Split(',');

                        float px = float.Parse(parts2[0]);
                        float py = float.Parse(parts2[1]);

                        smd.pivot = new UnityEngine.Vector2(px/ w, 1f - py / h);
                    }
                    else if (param.Contains("b="))
                    {
                        string[] parts1 = param.Split('=');
                        string[] parts2 = parts1[1].Split(',');

                        float bl = float.Parse(parts2[0]);
                        float bt = float.Parse(parts2[2]);
                        float br = w - float.Parse(parts2[1]);
                        float bb = h - float.Parse(parts2[3]);

                        smd.border = new Vector4(bl, bt, br, bb);
                    }
                }
            }

// 기존에 작성된 내용이 있을 때 다시 임포트 하면 덮어쓰기가 안되게 하는 코드. 필요 없으므로 주석처리.
//            if (existingSprites.ContainsKey(smd.name))
//            {
//                SpriteMetaData sprite = existingSprites[smd.name];
//                smd.pivot = sprite.pivot;
//                smd.alignment = sprite.alignment;
//#if !UNITY_4_3 // border attribute was introduced with 4.5 (versions <4.3 are not supported at all)
//                smd.border = sprite.border;
//#endif
//            }

            if (smd.pivot.x == 0 && smd.pivot.y == 0)
                smd.alignment = (int)UnityEngine.SpriteAlignment.BottomLeft;
            else if (smd.pivot.x == 0.5 && smd.pivot.y == 0)
                smd.alignment = (int)UnityEngine.SpriteAlignment.BottomCenter;
            else if (smd.pivot.x == 1 && smd.pivot.y == 0)
                smd.alignment = (int)UnityEngine.SpriteAlignment.BottomRight;
            else if (smd.pivot.x == 0 && smd.pivot.y == 0.5)
                smd.alignment = (int)UnityEngine.SpriteAlignment.LeftCenter;
            else if (smd.pivot.x == 0.5 && smd.pivot.y == 0.5)
                smd.alignment = (int)UnityEngine.SpriteAlignment.Center;
            else if (smd.pivot.x == 1 && smd.pivot.y == 0.5)
                smd.alignment = (int)UnityEngine.SpriteAlignment.RightCenter;
            else if (smd.pivot.x == 0 && smd.pivot.y == 1)
                smd.alignment = (int)UnityEngine.SpriteAlignment.TopLeft;
            else if (smd.pivot.x == 0.5 && smd.pivot.y == 1)
                smd.alignment = (int)UnityEngine.SpriteAlignment.TopCenter;
            else if (smd.pivot.x == 1 && smd.pivot.y == 1)
                smd.alignment = (int)UnityEngine.SpriteAlignment.TopRight;
            else
                smd.alignment = (int)UnityEngine.SpriteAlignment.Custom;

            metaData.Add (smd);
		}

		// NOTE @neec tpsheet에서 스프라이트가 추가/삭제 되지 않고 영역만 변경된 경우 
		// png.meta에 반영되지 않는 문제로 png.meta의 갱신을 유도하기 위해 anisoLevel을 변경한다 
		
		importer.anisoLevel += 1;
		importer.spritesheet = metaData.ToArray();
		importer.anisoLevel -= 1;
	}
}
