﻿using System;
using SimpleJSON;
using System.Reflection;
using System.Collections.Generic;

namespace GameService
{
    public class BaseEntity
    {
        public BaseEntity(JSONNode inData = null)
        {
            Parse(inData);
        }

        /// <summary>
        /// 초기화 
        /// </summary>
        public virtual void Clear()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inData"></param>
        public virtual void Parse(JSONNode inData)
        {
            if (inData == null)
            {
                return;
            }

            // 데이터 갱신을 위한 초기화
            Clear();

            FieldInfo[] properties = GetType().GetFields();

            for (int i = 0; i < properties.Length; ++i)
            {
                FieldInfo property = properties[i];
                TypeCode code = Type.GetTypeCode(property.FieldType);

                switch (code)
                {
                    case TypeCode.String: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, (string)inData[property.Name]); } break; }
                    case TypeCode.UInt32: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, (uint)inData[property.Name].AsInt); } break; }
                    case TypeCode.Int32: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, inData[property.Name].AsInt); } break; }
                    case TypeCode.Single: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, inData[property.Name].AsFloat); } break; }
                    case TypeCode.Boolean: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, inData[property.Name].AsBool); } break; }
                    case TypeCode.Int64: { if (inData.ContainsKey(property.Name)) { property.SetValue(this, (long)inData[property.Name].AsDouble); } break; }

                    case TypeCode.Object:
                    {
                        // 배열이라면
                        if (property.FieldType.IsArray)
                        {
                            JSONArray array = inData[property.Name].AsArray;

                            if (array == null)
                            {
                                property.SetValue(this, null);
                                break;
                            }

                            string fullname = property.FieldType.FullName;

                            if (fullname == "System.String[]")
                            {
                                string[] strArray = new string[array.Count];

                                for (int j = 0; j < array.Count; ++j)
                                {
                                    strArray[j] = array[j];
                                }

                                property.SetValue(this, strArray);
                            }
                            else if (fullname == "System.Int32[]")
                            {
                                int[] intArray = new int[array.Count];

                                for (int j = 0; j < array.Count; ++j)
                                {
                                    intArray[j] = array[j].AsInt;
                                }

                                property.SetValue(this, intArray);
                            }
                        }
                        else if (property.FieldType.IsGenericType)
                        {
                            // NOTE @neec 제너릭 타입은 List와 Dictionary만을 제공하며 커스텀 클래스는 지원하지 않는  
                            // 이유는 특정할 수 없기 때문이다 

                            // TODO @neec List와 Dictionary에 BaseEntity의 파생클래스를 지원할 수 있도록 하자 

                            var type = property.FieldType.GetGenericTypeDefinition();

                            if (type == typeof(List<>))
                            {
                                JSONArray array = inData[property.Name].AsArray;
                                Type genericArgument = property.FieldType.GetGenericArguments()[0];

                                if (genericArgument.Name == "String")
                                {
                                    List<string> list = property.GetValue(this) as List<string>;
                                    foreach (JSONNode n in array)
                                    {
                                        list.Add(n.Value);
                                    }
                                }
                                else if (genericArgument.Name == "Int32")
                                {
                                    List<int> list = property.GetValue(this) as List<int>;
                                    foreach (JSONNode n in array)
                                    {
                                        list.Add(n.AsInt);
                                    }
                                }
                                else if (genericArgument.Name == "Int64")
                                {
                                    List<long> list = property.GetValue(this) as List<long>;
                                    foreach (JSONNode n in array)
                                    {
                                        list.Add(n.AsLong);
                                    }
                                }
                                else
                                {
                                    // not support
                                }
                            }
                            else if (type.Name == typeof(Dictionary<int, int>).Name)
                            {
                                Type[] args = property.FieldType.GetGenericArguments();

                                if (args[0].Name == "String" && args[1].Name == "String")
                                {
                                    Dictionary<string, string> map = property.GetValue(this) as Dictionary<string, string>;
                                    foreach (string key in inData[property.Name].Keys)
                                    {
                                        if (map.ContainsKey(key))
                                        {
                                            map[key] = inData[property.Name][key].Value;
                                        }
                                        else
                                        {
                                            map.Add(key, inData[property.Name][key].Value);
                                        }
                                    }
                                }
                                else if (args[0].Name == "String" && args[1].Name == "Int32")
                                {
                                    Dictionary<string, int> map = property.GetValue(this) as Dictionary<string, int>;
                                    foreach (string key in inData[property.Name].Keys)
                                    {
                                        if (map.ContainsKey(key))
                                        {
                                            map[key] = inData[property.Name][key].AsInt;
                                        }
                                        else
                                        {
                                            map.Add(key, inData[property.Name][key].AsInt);
                                        }
                                    }
                                }
                                else if (args[0].Name == "String" && args[1].Name == "Int64")
                                {
                                    Dictionary<string, long> map = property.GetValue(this) as Dictionary<string, long>;
                                    foreach (string key in inData[property.Name].Keys)
                                    {
                                        if (map.ContainsKey(key))
                                        {
                                            map[key] = inData[property.Name][key].AsLong;
                                        }
                                        else
                                        {
                                            map.Add(key, inData[property.Name][key].AsLong);
                                        }
                                    }
                                }
                                else
                                {
                                    // not support
                                }
                            }
                        }
                        // BaseEntity 파생 클래스라면
                        else if (property.GetValue(this) is BaseEntity)
                        {
                            BaseEntity entity = (BaseEntity)property.GetValue(this);

                            if (entity != null)
                            {
                                entity.Parse(inData[property.Name]);
                            }
                            else
                            {
                                entity = (BaseEntity)Activator.CreateInstance(property.FieldType, inData[property.Name]);
                                property.SetValue(this, entity);
                            }
                        }

                        break;
                    }
                }
            }
        }
    }
}
