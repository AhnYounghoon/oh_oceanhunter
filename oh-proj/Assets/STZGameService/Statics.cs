using System.Text;
using System.Collections.Generic;

public class Statics:STZServerStatic
{
	static Statics _instance=null;
	public static Statics Instance{
		get{
			if(_instance==null)
				_instance = new Statics();
			return _instance;
		}
	}

    /// <summary>
    /// 전체 버전 정보 반환
    /// </summary>
    /// <returns></returns>
    public string GetVersions()
    {
        if(versions == null)
        {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        sb.Append("{");

        foreach(string key in versions.Keys)
        {
            sb.AppendFormat(",\"{0}\":{1}", key, versions[key]);
        }

        if(sb.Length > 1)
        {
            sb.Remove(1, 1);
        }

        sb.Append("}");

        return sb.Length == 2 ? null : sb.ToString();
    }
}