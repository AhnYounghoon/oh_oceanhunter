﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;


public class JsonParseObject{
	/*
	public static object Parse(SimpleJSON.JSONClass jsonNode, object o)
	{
 		foreach (FieldInfo prop in o.GetType().GetFields())
		{
 			switch(System.Type.GetTypeCode( prop.FieldType ))
			{
			case System.TypeCode.String:
				prop.SetValue(o,jsonNode[prop.Name].ToString());
				break;
			case System.TypeCode.Single:
				prop.SetValue(o,jsonNode[prop.Name].AsFloat);
				break;
			case System.TypeCode.Int32:
				prop.SetValue(o,jsonNode[prop.Name].AsInt);
				break;
			case System.TypeCode.Boolean:
				prop.SetValue(o,jsonNode[prop.Name].AsBool);
 				break;
			}
		}
		return o;
	}
	*/
	public static JSONClass EncodeToJsonClass(object o)
	{
		JSONClass jsonClass = new JSONClass();
		string s;
 		foreach (FieldInfo prop in o.GetType().GetFields())
		{
 			switch(System.Type.GetTypeCode( prop.FieldType ))
			{
			case System.TypeCode.String:
				s = prop.GetValue(o) as string;
				if(s==null)
					jsonClass[prop.Name] = string.Empty;
				else 
					jsonClass[prop.Name] = s;
				break;
			case System.TypeCode.Single:
				jsonClass[prop.Name].AsFloat = (float)prop.GetValue(o);
				break;
			case System.TypeCode.Int32:
				jsonClass[prop.Name].AsInt = (int)prop.GetValue(o);
				break;
			case System.TypeCode.Boolean:
				jsonClass[prop.Name].AsBool = (bool)prop.GetValue(o);
				break;
			}
		}
		return jsonClass;
	}

	public static Dictionary<string,string> EncodeToDictionary(object o)
	{
		Dictionary<string,string> result = new Dictionary<string,string>();
		string stringValue;
		foreach (FieldInfo prop in o.GetType().GetFields())
		{
			stringValue = prop.GetValue(o) as string;
			result.Add(prop.Name,(stringValue==null?string.Empty:stringValue));
		}
		return result;
	}
	
//	override public string ToString()
//	{
//		System.Text.StringBuilder sb = new 
//		foreach (FieldInfo prop in this.GetType().GetFields())
//		{
//			sb.Append(prop.Name);
//			sb.Append(" : ");
//			sb.Append(prop.GetValue(this));
//			sb.Append("\n");
// 		}
//		return sb.ToString();
//	}

	static public string ToStringTab(object o)
	{
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		ArrayList values = new ArrayList();
		foreach (FieldInfo prop in o.GetType().GetFields())
		{
			sb.Append(prop.Name);
			sb.Append("\t");
			values.Add(prop.GetValue(o));
		}
		sb.Append("\n");
		for(int i=0;i<values.Count;i++)
		{
			sb.Append(values[i]);
			sb.Append("\t");
		}
		sb.Append("\n");
		return sb.ToString();
	}

	public static object ParseFromTsv(string[] textLine,object o)
	{
		string[] keyStrings 	= textLine[0].Split('\t');
		string[] valueStrings 	= textLine[1].Split('\t');
		Dictionary<string,string> dic = new Dictionary<string, string>();
		for(int i=0;i<keyStrings.Length;i++)
		{
			dic.Add(keyStrings[i],valueStrings[i]);
		}
		string s;
		foreach (FieldInfo prop in o.GetType().GetFields())
		{
			s = dic[prop.Name] as string;

			switch(System.Type.GetTypeCode( prop.FieldType ))
			{
			case System.TypeCode.String:
				prop.SetValue(o,s);
				break;
			case System.TypeCode.Single:
				prop.SetValue(o,float.Parse(s));
				break;
			case System.TypeCode.Int32:
				prop.SetValue(o,int.Parse(s));
				break;
			case System.TypeCode.Boolean:
				prop.SetValue(o,bool.Parse(s));
				break;
			}
		}
		return 0;
 	}
	/*
	static public bool Update(object fromData,object toData)
	{
		foreach (FieldInfo prop in toData.GetType().GetFields())
		{
			prop.SetValue(toData,fromData.GetType().GetField(prop.Name).GetValue(fromData) );
		}
		return true;
	}
	*/
}
