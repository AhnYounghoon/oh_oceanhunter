﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using SimpleJSON;
using System;
using STZFramework;

public static class ExtensionMethod
{
    public enum MESH_PIVOT
    {
        Top
        , BOTTOM
        , CENTER
    }

    /// <summary>
    /// ANSI 코드로 변환 
    /// </summary>
    /// <param name="inSource"></param>
    /// <returns></returns>
    public static string Escape(this string inSource)
    {
        return System.Uri.EscapeDataString(inSource);
    }

    /// <summary>
    /// 원래 코드로 변환 
    /// </summary>
    /// <param name="inSource"></param>
    /// <returns></returns>
    public static string Unescape(this string inSource)
    {
        return System.Uri.UnescapeDataString(inSource);
    }

    public static float FloatValue(this string o)
    {
        float result = 0;
        if (float.TryParse(o, out result))
            return result;
        return 0;
    }

    public static double DoubleValue(this string o)
    {
        double result = 0;
        if (double.TryParse(o, out result))
            return result;
        return 0;
    }

    public static int IntValue(this string o)
    {
        int result = 0;
        if (int.TryParse(o, out result))
            return result;
        return 0;
    }

    public static bool BoolValue(this string o)
    {
        bool result = false;
        if (bool.TryParse(o, out result))
            return result;
        return false;
    }

    public static T EnumValue<T>(this string o)
    {
        T result = default(T);
        try
        {
            result = (T)Enum.Parse(typeof(T), o);
        }
        catch
        {
        }
        return result;
    }

    public static string Short(this string o, int length = 7)
    {
        if (o != null && o.Length > length)
        {
            return o.Substring(0, length - 2) + "...";
        }
        return o;
    }

    public static string Unicode(this string o)
    {

        byte[] defaultBytes = Encoding.Default.GetBytes(o);
        byte[] unicodeBytes = Encoding.Convert(Encoding.Default, Encoding.Unicode, defaultBytes);

        //  UTF8 bytes를 ansi로 변환해서 리턴한다.
        return Encoding.Unicode.GetString(unicodeBytes);
    }


    public static bool IsNullOrEmpty(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return true;
        }
        if (str == "null")
            return true;
        return false;
    }

    public static string UTF8(this string str)
    {
        StringBuilder builder = new StringBuilder();

        char[] charArray = str.ToCharArray();
        foreach (var c in charArray)
        {
            int codepoint = System.Convert.ToInt32(c);
            if ((codepoint >= 32) && (codepoint <= 126))
            {
                builder.Append(c);
            }
            else
            {
                builder.Append("\\u");
                builder.Append(codepoint.ToString("x4"));
            }
        }
        return builder.ToString();
    }

    public static bool DecodeTo<T>(this SimpleJSON.JSONClass jsonNode, ref T o)
    {
        if (jsonNode == null)
        {
            Debug.Log("Not JsonClass!!!");
            return false;
        }

        foreach (FieldInfo prop in o.GetType().GetFields())
        {
            // the property value is written at compile time and cannot be changed.
            if (prop.IsLiteral)
                continue;

            switch (System.Type.GetTypeCode(prop.FieldType))
            {
                case System.TypeCode.String:
                prop.SetValue(o, jsonNode[prop.Name].Value);
                break;
                case System.TypeCode.Single:
                prop.SetValue(o, jsonNode[prop.Name].AsFloat);
                break;
                case System.TypeCode.Double:
                prop.SetValue(o, jsonNode[prop.Name].AsDouble);
                break;
                case System.TypeCode.Int32:
                prop.SetValue(o, jsonNode[prop.Name].AsInt);
                break;
                case System.TypeCode.Int64:
                prop.SetValue(o, jsonNode[prop.Name].AsLong);
                break;
                case System.TypeCode.Boolean:
                prop.SetValue(o, jsonNode[prop.Name].AsBool);
                break;
                case System.TypeCode.Object:
                string className = ConvertClassString(prop.Name);
                if (className == "UserData" && jsonNode[prop.Name].Count > 0)
                {
                    //ServerData.UserData uData = new ServerData.UserData();
                    //(JSONClass.Parse(jsonNode[prop.Name].ToString()) as JSONClass).DecodeTo<ServerData.UserData>(ref uData);
                    //prop.SetValue(o, uData);
                }
                break;
            }

            GameService.BaseEntity entity = o as GameService.BaseEntity;

            if (entity != null)
            {
                entity.Parse(jsonNode);
            }
        }
        return true;
    }

    public static string ConvertClassString(string inValue)
    {
        string result = string.Empty;
        for (int i = 0; i < inValue.Length; i++)
        {
            if (i == 0)
            {
                result += inValue[i].ToString().ToUpper();
                continue;
            }
            if (inValue[i] == '_')
            {
                continue;
            }
            else if (inValue[i - 1] == '_')
            {
                result += inValue[i].ToString().ToUpper();
            }
            else
            {
                result += inValue[i];
            }
        }
        return result;
    }

    public static string MD5(this string strToEncrypt)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    public static string Obj2String(this object o)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendFormat("{0}\n", o.ToString());
        foreach (FieldInfo prop in o.GetType().GetFields())
        {
            sb.AppendFormat("{0}:{1}\n", prop.Name, prop.GetValue(o));
        }

        return sb.ToString();
    }

    public static JSONClass EncodeToJsonClass(this object o)
    {
        JSONClass jsonClass = new JSONClass();
        string s;
        foreach (FieldInfo prop in o.GetType().GetFields())
        {
            switch (System.Type.GetTypeCode(prop.FieldType))
            {
                case System.TypeCode.String:
                s = prop.GetValue(o) as string;
                if (s == null)
                    jsonClass[prop.Name] = string.Empty;
                else
                    jsonClass[prop.Name] = s;
                break;
                case System.TypeCode.Double:
                jsonClass[prop.Name].AsDouble = (double)prop.GetValue(o);
                break;
                case System.TypeCode.Single:
                jsonClass[prop.Name].AsFloat = (float)prop.GetValue(o);
                break;
                case System.TypeCode.Int64:
                jsonClass[prop.Name].AsLong = (long)prop.GetValue(o);
                break;
                case System.TypeCode.Int32:
                jsonClass[prop.Name].AsInt = (int)prop.GetValue(o);
                break;
                case System.TypeCode.Boolean:
                jsonClass[prop.Name].AsBool = (bool)prop.GetValue(o);
                break;
            }
        }
        return jsonClass;
    }

    static public string ToStringTab(this object o)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        ArrayList values = new ArrayList();
        foreach (FieldInfo prop in o.GetType().GetFields())
        {
            sb.Append(prop.Name);
            sb.Append("\t");
            values.Add(prop.GetValue(o));
        }
        sb.Append("\n");
        for (int i = 0; i < values.Count; i++)
        {
            sb.Append(values[i]);
            sb.Append("\t");
        }
        sb.Append("\n");
        return sb.ToString();
    }

    public static string Join(this ArrayList list, string delemiter = ",")
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(list[0].ToString());
        for (int i = 1; i < list.Count; i++)
        {
            sb.Append(delemiter);
            sb.Append(list[i].ToString());
        }
        return sb.ToString();
    }

    public static string Join(this string[] list, string delemiter = ",")
    {
        if (list == null)
            return string.Empty;
        return System.String.Join(delemiter, list);
    }

    public static void ShuffleList<T>(this object mono, ref List<T> list)
    {
        int halfIndex = list.Count / 2;
        T tmp;
        int rndIdx;
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < halfIndex; j++)
            {
                rndIdx = UnityEngine.Random.Range(0, halfIndex) + halfIndex;
                tmp = list[j];
                list[j] = list[rndIdx];
                list[rndIdx] = tmp;
            }
        }
    }
    public static void ShuffleIntList(this object mono, ref List<int> list)
    {
        int halfIndex = list.Count / 2;
        int tmp;
        int rndIdx;
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < halfIndex; j++)
            {
                rndIdx = UnityEngine.Random.Range(0, halfIndex) + halfIndex;
                tmp = list[j];
                list[j] = list[rndIdx];
                list[rndIdx] = tmp;
            }
        }
    }
    public static void ShuffleIntArray(this object mono, ref int[] list)
    {
        int halfIndex = list.Length / 2;
        int tmp;
        int rndIdx;
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < halfIndex; j++)
            {
                rndIdx = UnityEngine.Random.Range(0, halfIndex) + halfIndex;
                tmp = list[j];
                list[j] = list[rndIdx];
                list[rndIdx] = tmp;
            }
        }
    }

    public static Transform GetChildWith(this Transform gobj, string name)
    {
        return ShearchChildWith(gobj.transform, name);
    }

    public static Transform ShearchChildWith(Transform gT, string name)
    {
        if (gT.name == name) return gT;
        foreach (Transform t in gT)
        {
            if (t.name == name) return t;
            Transform ut = ShearchChildWith(t, name);
            if (ut != null) return ut;
        }
        return null;
    }

    public static string[] GetChildNames(this Transform gobj, string name)
    {
        ArrayList result = new ArrayList();
        ShearchChildNamesWith(ref result, gobj.transform, name);
        return result.ToArray(typeof(string)) as string[];
    }

    public static void ShearchChildNamesWith(ref ArrayList list, Transform gT, string name)
    {
        if (gT.name.Contains(name))
        {
            string key = gT.name.Replace(name, "");
            if (!list.Contains(key))
                list.Add(key);
        }
        foreach (Transform t in gT)
        {
            ShearchChildNamesWith(ref list, t, name);
        }
    }

    public static void SetTextureWithNewMesh(this Transform t, Texture2D texture, int w, int h)
    {
        t.GetComponent<Renderer>().material.mainTexture = texture;
        MeshFilter mf = t.GetComponent<MeshFilter>() as MeshFilter;
        if (mf == null) { Debug.LogWarning("[ Warning ] MeshFilter is Null"); return; }
        mf.mesh = texture.CreateMesh(MESH_PIVOT.CENTER, w, h, 1);
    }

    public static Mesh CreateMesh(this Texture2D tex, MESH_PIVOT pivot, float scale)
    {
        return CreateMeshN(tex, tex.width, tex.height, pivot, scale);
    }

    public static Mesh CreateMesh(this Texture2D tex, MESH_PIVOT pivot, float width, float height, float scale)
    {
        return CreateMeshN(tex, width, height, pivot, scale);
    }

    public static Mesh CreateMeshN(Texture2D tex, float width, float height, MESH_PIVOT pivot, float scale)
    {
        Mesh mesh = new Mesh();
        int[] triangles = { 0, 1, 2, 2, 1, 3 };
        float widthHalf = width / 2f;
        float heightHalf = height / 2f;
        switch (pivot)
        {
            case MESH_PIVOT.Top:
            Vector3[] verticesT = {
                new Vector3(-widthHalf, 0,0)*scale
                ,   new Vector3( widthHalf, 0,0)*scale
                ,   new Vector3(-widthHalf,-height,0)*scale
                ,   new Vector3( widthHalf,-height,0)*scale
            };
            mesh.vertices = verticesT;
            break;
            case MESH_PIVOT.BOTTOM:
            Vector3[] verticesB = {
                new Vector3(-widthHalf, height,0)*scale
                ,   new Vector3( widthHalf, height,0)*scale
                ,   new Vector3(-widthHalf,0,0)*scale
                ,   new Vector3( widthHalf,0,0)*scale
            };
            mesh.vertices = verticesB;
            break;
            case MESH_PIVOT.CENTER:
            Vector3[] verticesC = {
                new Vector3(-widthHalf, heightHalf,0)*scale
                ,   new Vector3( widthHalf, heightHalf,0)*scale
                ,   new Vector3(-widthHalf,-heightHalf,0)*scale
                ,   new Vector3( widthHalf,-heightHalf,0)*scale
            };
            mesh.vertices = verticesC;
            break;
        }

        Vector2[] uv = {
            new Vector2(    0, 1)
            ,   new Vector2(    1, 1)
            ,   new Vector2(    0, 0)
            ,   new Vector2(    1, 0)
        };

        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        ;
        return mesh;
    }

    public static void AddOrAssign<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue value)
    {
        if (dict.ContainsKey(key))
            dict[key] = value;
        else
            dict.Add(key, value);
    }

	static public T GetOrAddComponent<T>(this Component child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
        {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

    public static string Decode(this string o)
    {
        if (!o.IsNullOrEmpty())
        {
            o = o.Replace(' ', '+');
            if (!string.IsNullOrEmpty(o))
                return AES.Decrypt(o);
        }
        return "";
    }

    public static string Encode(this string o)
    {
        if (!o.IsNullOrEmpty())
        {
            o = o.Trim();
            if (!string.IsNullOrEmpty(o))
                return AES.Encrypt(o);
        }
        return "";
    }

    public static int ToInt(this string o)
    {
        if (!o.IsNullOrEmpty())
        {
            int integer;
            if (int.TryParse(o, out integer))
                return integer;
        }
        return 0;
    }

    public static long ToLong(this string o)
    {
        if (!o.IsNullOrEmpty())
        {
            long num;
            if (long.TryParse(o, out num))
                return num;
        }
        return 0;
    }

    public static double ToDouble(this string o)
    {
        if (!o.IsNullOrEmpty())
        {
            double dbl;
            if (double.TryParse(o, out dbl))
                return dbl;
        }
        return 0.0;
    }

    /// <summary>
    /// 2바이트 해쉬 코드 생성 
    /// </summary>
    /// <param name="inString"></param>
    /// <returns></returns>
    //public static ushort GetHash16Code(this string inString)
    //{
    //    ushort hashCode = 0;
    //    if (!string.IsNullOrEmpty(inString))
    //    {
    //        //Unicode Encode Covering all characterset
    //        byte[] byteContents = Encoding.Unicode.GetBytes(inString);
    //        System.Security.Cryptography.SHA256 hash =
    //        new System.Security.Cryptography.SHA256CryptoServiceProvider();
    //        byte[] hashText = hash.ComputeHash(byteContents);

    //        for (int i = 0; i < 16; ++i)
    //        {
    //            hashCode ^= BitConverter.ToUInt16(hashText, i * 2);
    //        }
    //    }

    //    return (hashCode);
    //}
}