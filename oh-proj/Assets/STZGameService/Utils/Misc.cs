﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace GameService
{
    public class Misc
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static System.DateTime GetTimeFromUnixTimeStamp(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp);
            return dtDateTime;
        }

        /// <summary>
        /// 현재 날짜 문자열 
        /// </summary>
        /// <returns></returns>
        public static string Now()
        {
            return System.DateTime.Now.ToString("u").Remove(19);
        }

        /// <summary>
        /// 주어진 경로에 텍스트 파일 저장 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="contents"></param>
        public static void FileWriteAllText(string path, string contents)
        {
            File.WriteAllText(path, contents);
        }

        /// <summary>
        /// 주어진 경로의 텍스트 파일 로드 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FileReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }
}