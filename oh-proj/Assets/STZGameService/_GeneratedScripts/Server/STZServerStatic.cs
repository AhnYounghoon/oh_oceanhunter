﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using ServerStatic;
using System.Text;
using System.IO;
using UniLinq;

public class STZServerStatic
{
    public Dictionary<string, Systems> systems;
    public Dictionary<int, GameEvent> game_event;
    public Dictionary<int, ServerStatic.BattleObject> battle_object;
    public Dictionary<int, ServerStatic.ShipSkin> ship_skin;
    public Dictionary<int, ServerStatic.Character> character;
    public Dictionary<int, ServerStatic.InGameBackground> ingame_background;
    public Dictionary<int, ServerStatic.Skill> skill;
    public Dictionary<int, ServerStatic.SkillCondition> skill_condition;
    public Dictionary<int, ServerStatic.SkillEffect> skill_effect;
    public Dictionary<int, ServerStatic.SkillTarget> skill_target;
    public Dictionary<int, ServerStatic.SailingCatogory> sailing_category;
    public Dictionary<int, ServerStatic.SailingInfo> sailing_info;
    public Dictionary<int, ServerStatic.SailingEvent> sailing_event;
    public Dictionary<int, ServerStatic.SailingEventReward> sailing_event_reward;
    public Dictionary<int, ServerStatic.Goods> goods;
    public Dictionary<int, ServerStatic.Quest> quest;

    public LocaleUrl locale;

    public Dictionary<string, int> versions { get; private set; }

    string[] _typeList = new string[] { "int", "bool", "float", "double", "string" };

    public STZServerStatic()
    {
        systems = new Dictionary<string, Systems>();
        game_event = new Dictionary<int, GameEvent>();
        battle_object = new Dictionary<int, ServerStatic.BattleObject>();
        ship_skin = new Dictionary<int, ShipSkin>();
        character = new Dictionary<int, ServerStatic.Character>();
        ingame_background = new Dictionary<int, InGameBackground>();
        skill = new Dictionary<int, Skill>();
        skill_condition = new Dictionary<int, SkillCondition>();
        skill_effect = new Dictionary<int, SkillEffect>();
        skill_target = new Dictionary<int, SkillTarget>();
        sailing_category = new Dictionary<int, SailingCatogory>();
        sailing_info = new Dictionary<int, SailingInfo>();
        sailing_event = new Dictionary<int, SailingEvent>();
        sailing_event_reward = new Dictionary<int, SailingEventReward>();
        goods = new Dictionary<int, Goods>();
        quest = new Dictionary<int, Quest>();
    }

    virtual public void Parse(JSONClass inJsonClass, bool isSave = true)
    {
        if (inJsonClass == null)
            return;

        if (versions == null)
            versions = new Dictionary<string, int>();

        int idx = 0;
        JSONClass jClass = null;
        JSONArray jArray = null;
        foreach (string key in inJsonClass.Keys)
        {
            jClass = inJsonClass[key] as JSONClass;

            if (jClass == null) continue;

            if (versions.ContainsKey(key))
                versions[key] = jClass["version"].AsInt;
            else
                versions.Add(key, jClass["version"].AsInt);

            jArray = jClass["data"].AsArray;
            if (jArray == null)
            {
                if (key == "locale")
                {
                    locale = new LocaleUrl();
                    (jClass["data"] as JSONClass).DecodeTo<LocaleUrl>(ref locale);
                }
                else {
                    continue;
                }
                if (_jsonClass != null) _jsonClass[key] = inJsonClass[key];
            }

            if (_jsonClass != null) _jsonClass[key] = inJsonClass[key];

            if (jArray == null || jArray.Count == 0)
                continue;

            idx = 0;
            foreach (var fieldKey in jArray[0].Keys)
            {
                string typeKey = jArray[0][fieldKey].Value;
                for (int i = 0; i < _typeList.Length; i++)
                {
                    if (!typeKey.Equals(_typeList[i]))
                        continue;

                    idx = 1;
                    break;
                }
                break;
            }

            switch (key)
            {
                case "locale":
                    locale = new LocaleUrl();
                    locale.SetLocale(jArray.ToString());
                    break;

                case "systems":
                    systems.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        Systems tmp = new Systems();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<Systems>(ref tmp);
                        if (systems.ContainsKey(tmp.key))
                            systems[tmp.key] = tmp;
                        else
                            systems.Add(tmp.key, tmp);
                    }
                    break;
                case "game_event":
                    game_event.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        GameEvent tmp = new GameEvent();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<GameEvent>(ref tmp);
                        if (game_event.ContainsKey(tmp.id))
                            game_event[tmp.id] = tmp;
                        else
                            game_event.Add(tmp.id, tmp);
                    }
                    break;
                case "battle_object":
                    battle_object.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.BattleObject tmp = new ServerStatic.BattleObject();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.BattleObject>(ref tmp);
                        if (battle_object.ContainsKey(tmp.id))
                            battle_object[tmp.id] = tmp;
                        else
                            battle_object.Add(tmp.id, tmp);
                    }
                    break;
                case "ship_skin":
                    ship_skin.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.ShipSkin tmp = new ServerStatic.ShipSkin();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.ShipSkin>(ref tmp);
                        if (ship_skin.ContainsKey(tmp.id))
                            ship_skin[tmp.id] = tmp;
                        else
                            ship_skin.Add(tmp.id, tmp);
                    }
                    break;
                case "character":
                    character.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.Character tmp = new ServerStatic.Character();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.Character>(ref tmp);
                        if (character.ContainsKey(tmp.id))
                            character[tmp.id] = tmp;
                        else
                            character.Add(tmp.id, tmp);
                    }
                    break;
                case "ingame_background":
                    ingame_background.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.InGameBackground tmp = new ServerStatic.InGameBackground();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.InGameBackground>(ref tmp);
                        if (ingame_background.ContainsKey(tmp.id))
                            ingame_background[tmp.id] = tmp;
                        else
                            ingame_background.Add(tmp.id, tmp);
                    }
                    break;
                case "skill":
                    skill.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.Skill tmp = new ServerStatic.Skill();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.Skill>(ref tmp);
                        if (skill.ContainsKey(tmp.id))
                            skill[tmp.id] = tmp;
                        else
                            skill.Add(tmp.id, tmp);
                    }
                    break;
                case "skill_condition":
                    skill_condition.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SkillCondition tmp = new ServerStatic.SkillCondition();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SkillCondition>(ref tmp);
                        if (skill_condition.ContainsKey(tmp.id))
                            skill_condition[tmp.id] = tmp;
                        else
                            skill_condition.Add(tmp.id, tmp);
                    }
                    break;
                case "skill_target":
                    skill_target.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SkillTarget tmp = new ServerStatic.SkillTarget();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SkillTarget>(ref tmp);
                        if (skill_target.ContainsKey(tmp.id))
                            skill_target[tmp.id] = tmp;
                        else
                            skill_target.Add(tmp.id, tmp);
                    }
                    break;
                case "skill_effect":
                    skill_effect.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SkillEffect tmp = new ServerStatic.SkillEffect();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SkillEffect>(ref tmp);
                        if (skill_effect.ContainsKey(tmp.id))
                            skill_effect[tmp.id] = tmp;
                        else
                            skill_effect.Add(tmp.id, tmp);
                    }
                    break;
                case "sailing_category":
                    sailing_category.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SailingCatogory tmp = new ServerStatic.SailingCatogory();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SailingCatogory>(ref tmp);
                        if (sailing_category.ContainsKey(tmp.id))
                            sailing_category[tmp.id] = tmp;
                        else
                            sailing_category.Add(tmp.id, tmp);
                    }
                    break;
                case "sailing_info":
                    sailing_info.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SailingInfo tmp = new ServerStatic.SailingInfo();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SailingInfo>(ref tmp);
                        if (sailing_info.ContainsKey(tmp.id))
                            sailing_info[tmp.id] = tmp;
                        else
                            sailing_info.Add(tmp.id, tmp);
                    }
                    break;
                case "sailing_event":
                    sailing_event.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SailingEvent tmp = new ServerStatic.SailingEvent();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SailingEvent>(ref tmp);
                        if (sailing_event.ContainsKey(tmp.id))
                            sailing_event[tmp.id] = tmp;
                        else
                            sailing_event.Add(tmp.id, tmp);
                    }
                    break;
                case "sailing_event_reward":
                    sailing_event_reward.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.SailingEventReward tmp = new ServerStatic.SailingEventReward();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.SailingEventReward>(ref tmp);
                        if (sailing_event_reward.ContainsKey(tmp.id))
                            sailing_event_reward[tmp.id] = tmp;
                        else
                            sailing_event_reward.Add(tmp.id, tmp);
                    }
                    break;
                case "goods":
                    goods.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.Goods tmp = new ServerStatic.Goods();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.Goods>(ref tmp);
                        if (goods.ContainsKey(tmp.id))
                            goods[tmp.id] = tmp;
                        else
                            goods.Add(tmp.id, tmp);
                    }
                    break;
                case "quest":
                    quest.Clear();

                    for (; idx < jArray.Count; idx++)
                    {
                        ServerStatic.Quest tmp = new ServerStatic.Quest();
                        (jArray[idx].AsObject as JSONClass).DecodeTo<ServerStatic.Quest>(ref tmp);
                        if (quest.ContainsKey(tmp.id))
                            quest[tmp.id] = tmp;
                        else
                            quest.Add(tmp.id, tmp);
                    }
                    break;
            }
        }

        if (isSave && inJsonClass.Count > 0)
        {
            UnityEngine.Debug.Log("Static SaveToLocal - Count : " + inJsonClass.Count);
            if (_jsonClass == null || _jsonClass.Count <= 1)
            {
                _jsonClass = inJsonClass;
            }
            else {
                Debug.Log(_jsonClass.Count);
            }
            SaveToLocal();
        }

        return;
    }

    void SaveToLocal()
    {
        JSONClass saveJson = LoadFromLocal();
        if (saveJson == null)
        {
            saveJson = new JSONClass();
        }

        foreach (string currentKey in _jsonClass.Keys)
        {
            if (saveJson.ContainsKey(currentKey))
                saveJson[currentKey] = _jsonClass[currentKey];
            else
                saveJson.Add(currentKey, _jsonClass[currentKey]);
        }

        List<string> underbarKeys = saveJson.Keys.Where(T => T.First().Equals('_')).ToList();
        foreach (string key in underbarKeys)
        {
            saveJson.Remove(key);
        }

        saveJson.SaveToFile(string.Format("{0}/{1}", Application.temporaryCachePath, ("Statics").MD5()));
#if UNITY_EDITOR
        GameService.Misc.FileWriteAllText(string.Format("{0}/Resources/Statics/{1}.txt", Application.dataPath, "Statics"), saveJson.ToString());
#endif
        _jsonClass = null;
        versions = null;
    }

    /// <summary>
    /// 캐시된 스태틱 파일 로딩
    /// </summary>
    /// <returns></returns>
    public static JSONClass LoadFromLocal()
    {
        //string path = string.Format("{0}/{1}", Application.temporaryCachePath, ("Statics").MD5());
        //if (!FileExists(path))
        {
            TextAsset txtAsset = Resources.Load<TextAsset>(string.Format("Statics/{0}", "Statics"));
            if (txtAsset == null)
                return null;

            /* TEST 압축된 built-in 스태틱을 압축하지 않은 버전으로 저장하기 위함 확인용
            JSONClass tt = JSONNode.LoadFromCompressedBase64(txtAsset.text) as JSONClass;
            GameService.Misc.FileWriteAllText(string.Format("{0}/Resources/Statics/{1}.txt", Application.dataPath, "StaticsNonCompress"), tt.ToString());
            //*/

            return JSONNode.Parse(txtAsset.text) as JSONClass;
        }

        //return JSONClass.LoadFromFile(path) as JSONClass;
    }

    JSONClass _jsonClass;

    /// <summary>
    /// 캐싱된 Statics.txt에서 시트별 버전 반환 
    /// </summary>
    /// <returns></returns>
    public string GetLocalSavedVersions()
    {
        JSONClass jsonClass = LoadFromLocal();
        if (jsonClass == null)
        {
            _jsonClass = new JSONClass();
            return "{\"0\":0}";
        }

        Parse(jsonClass, false);
        _jsonClass = jsonClass;

        JSONClass json = new JSONClass();

        foreach (string key in versions.Keys)
        {
            json[key] = versions[key].ToString();
        }

        return json.ToString();
    }

    /// <summary>
    /// 파일 존재 여부 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    static bool FileExists(string path)
    {
#if !UNITY_WEBPLAYER
        return File.Exists(path);
#else
		return false;
#endif
    }
}