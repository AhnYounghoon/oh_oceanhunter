﻿namespace ServerStatic
{
    public class SkillTarget
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public string target;             // ETarget
        public string target_id;        
        public string property;           // EProperty
        public string split_property;     // ESplitProperty
        public string value_type;         // EValueType
        public int value;
        public float range;
        //[Auto generate code end]

        public BattleEnum.ETarget GetTarget()
        {
            return target.EnumValue<BattleEnum.ETarget>();
        }

        public BattleEnum.EProperty GetProperty()
        {
            return property.EnumValue<BattleEnum.EProperty>();
        }

        public BattleEnum.ESplitProperty GetSplitProperty()
        {
            return split_property.EnumValue<BattleEnum.ESplitProperty>();
        }

        public BattleEnum.EValueType GetValueType()
        {
            return value_type.EnumValue<BattleEnum.EValueType>();
        }
    }
}