﻿using System.Linq;
using System.Collections.Generic;

namespace ServerStatic
{
    public class SailingCatogory
    {
        //[Auto generate code begin]
        public int id;
        public int episode_id;
        public string episode_desc;
        public int stage_id;
        public string stage_desc;
        public string clear_reward;
        //[Auto generate code end]

        //static public List<SailingCatogory> GetListById(int inId)
        //{
        //    return Statics.Instance.sailing_category.Values.Where(x => x.tile_id == inId).ToList();
        //}

        static public List<SailingCatogory> GetListByEpisodeId(int inEpisodeId)
        {
            return Statics.Instance.sailing_category.Values.Where(x => x.episode_id == inEpisodeId).Distinct().ToList();
        }
    }
}