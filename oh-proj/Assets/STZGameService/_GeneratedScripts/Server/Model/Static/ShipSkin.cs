﻿namespace ServerStatic
{
    public class ShipSkin
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public int max_level;
        public float size;
        public float durability;
        public float defense;
        public float fuel;
        public float speed;
        public int weapon_slot;
        public int character_slot;
        //[Auto generate code end]
    }
}