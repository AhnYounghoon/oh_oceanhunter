﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ServerStatic
{
    public class BattleObject
    {
        /// <summary>
        /// data json 데이터를 변수화 시켜놓은 클래스
        /// </summary>
        public class ParseData
        {
            // NOTE @minjun.ha 외부 스파인 데이터를 사용해 애니메이션 키가 정형화 되지 않아
            //                 각각 수정 가능하도록 개발해놓음
            /// Animation 
            public string idleAnimationName;
            public string summonAnimationName;
            public string damageAnimationName;
            public string dieAnimationName;
            public List<string> attackAnimationList = new List<string>();
            public List<string> skillAnimationList  = new List<string>();

            public List<string> GetAllAnimationName()
            {
                List<string> list = new List<string>();
                list = list.Union(attackAnimationList).ToList();
                list = list.Union(skillAnimationList).ToList();
                list.Add(idleAnimationName);
                list.Add(summonAnimationName);
                list.Add(damageAnimationName);
                list.Add(dieAnimationName);
                return list;
            }
        }

        //[Auto generate code begin]
        public int id;
        public string name;
        public string action_type;
        public string prefab_path;
        public string resource_path;
        public int skin_id;
        public int level;
        public int max_level;
        public float hp;
        public float damage;
        public float defense;
        public float speed;
        public float fuel;
        public int inventory;
        public string ability;
        public float size;
        public float avoid_percent;
        public string offset;
        public float attack_range;
        public string attack_id;
        public string skill_id;
        public string data;
        //[Auto generate code end]
      
        private ParseData parseData = null;

        public BattleEnum.EBattleType GetBattleType()
        {
            return action_type.EnumValue<BattleEnum.EBattleType>();
        }

        public Vector3 GetOffset()
        {
            Vector3 vec3 = new Vector3();
            string[] split = offset.Split(',');
            vec3.x = float.Parse( split[0] );
            vec3.y = float.Parse( split[1] );
            vec3.z = float.Parse( split[2] );
            return vec3;
        }

        public void Parse()
        {
            if (parseData != null || string.IsNullOrEmpty(data) || data == "null")
            {
                return;
            }

            try
            {
                parseData = new ParseData();
                SimpleJSON.JSONNode inNode = SimpleJSON.JSONNode.LoadFromCompressedBase64(data);
                if (inNode.ContainsKey("attack_animation"))
                {
                    SimpleJSON.JSONArray array = inNode["attack_animation"].AsArray;
                    for (int i = 0; i < array.Count; i++)
                    {
                        parseData.attackAnimationList.Add(array[i]);
                    }
                }
                if (inNode.ContainsKey("skill_animation"))
                {
                    SimpleJSON.JSONArray array = inNode["skill_animation"].AsArray;
                    for (int i = 0; i < array.Count; i++)
                    {
                        parseData.skillAnimationList.Add(array[i]);
                    }
                }
                if (inNode.ContainsKey("idle_animation"))
                {
                    parseData.idleAnimationName = inNode["idle_animation"];
                }
                if (inNode.ContainsKey("summon_animation"))
                {
                    parseData.summonAnimationName = inNode["summon_animation"];
                }
                if (inNode.ContainsKey("damage_animation"))
                {
                    parseData.damageAnimationName = inNode["damage_animation"];
                }
                if (inNode.ContainsKey("die_animation"))
                {
                    parseData.dieAnimationName = inNode["die_animation"];
                }
            }
            catch
            {
                parseData = null;
                Debug.LogError("Parsing Error Data : " + data);
            }
        }

        public ParseData GetParseData()
        {
            // 파싱 데이터가 없다면 즉시 파싱
            if (parseData == null)
            {
                Parse();
            }
            return parseData;
        }
    }
}