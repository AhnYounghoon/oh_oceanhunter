﻿namespace ServerStatic
{
    public class SkillEffect
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public string effect_type;      // EEfectType
        public string split_property;   // ESplitProperty
        public float value;
        public float correct_value;
        public float count;
        public float time;
        public string resource_path;
        //[Auto generate code end]

        public BattleEnum.EEffectType GetEffectType()
        {
            return effect_type.EnumValue<BattleEnum.EEffectType>();
        }

        public BattleEnum.ESplitProperty GetSplitProperty()
        {
            return split_property.EnumValue<BattleEnum.ESplitProperty>();
        }
    }
}