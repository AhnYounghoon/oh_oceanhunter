﻿using System.Collections.Generic;
using UniLinq;

namespace ServerStatic
{
    public class SailingEventReward
    {
        //[Auto generate code begin]
        public int id;
        public string category;
        public string value;
        public string plus_minus;
        public int min;
        public int max;
        //[Auto generate code end]

        static public List<SailingEventReward> GetListById(int inId)
        {
            return Statics.Instance.sailing_event_reward.Values.Where(x => x.id == inId).ToList();
        }
    }
}