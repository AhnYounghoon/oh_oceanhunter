﻿namespace ServerStatic
{
    public class SkillCondition
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public string condition_type;   // EConditionType
        public string target;           // ETarget
        public string property;         // EProperty
        public string value_type;       // EValueType
        public int value;
        public float excute_percent;
        //[Auto generate code end]

        public BattleEnum.EConditionType GetConditionType()
        {
            return condition_type.EnumValue<BattleEnum.EConditionType>();
        }

        public BattleEnum.ETarget GetTarget()
        {
            return target.EnumValue<BattleEnum.ETarget>();
        }

        public BattleEnum.EProperty GetProperty()
        {
            return property.EnumValue<BattleEnum.EProperty>();
        }

        public BattleEnum.EValueType GetValueType()
        {
            return value_type.EnumValue<BattleEnum.EValueType>();
        }

    }
}