﻿using System.Linq;
using System.Collections.Generic;

namespace ServerStatic
{
    public class SailingInfo
    {
        //[Auto generate code begin]
        public int id;
        public int sailing_id;
        public int tile_id;
        public int tile_type;
        public int rate;
        public int amount;
        public string event_type;
        //[Auto generate code end]

        static public List<SailingInfo> GetListById(int inId)
        {
            return Statics.Instance.sailing_info.Values.Where(x => x.tile_id == inId).ToList();
        }

        static public List<SailingInfo> GetUniqueById()
        {
            return Statics.Instance.sailing_info.Values.Distinct().ToList();
        }
    }
}