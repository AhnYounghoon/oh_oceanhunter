﻿using UniLinq;

namespace ServerStatic
{
    public class SailingEvent
    {
        //[Auto generate code begin]
        public int id;
        public int type;
        public string cut_scene;
        public string text;
        public int rate;
        public string event_reward;
        //[Auto generate code end]

        static public ServerStatic.SailingEvent[] GetListByType(int inType)
        {
            return Statics.Instance.sailing_event
                .Where(x => x.Value.type == inType)
                .Select(x => x.Value)
                .ToArray();
        }
    }
}