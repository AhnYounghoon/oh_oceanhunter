﻿using System.Linq;
using System.Collections.Generic;

namespace ServerStatic
{
    public class Quest
    {
        //[Auto generate code begin]
        public int id;
        public int type;
        public int count;
        public int battle_object_id;
        public string reward;
        public string title;
        public string desc_1;
        public string desc_2;
        //[Auto generate code end]

        static public Quest GetQuestById(int inId)
        {
            return Statics.Instance.quest.Values.Where(x => x.id == inId).FirstOrDefault();
        }

        static public List<Quest> GetListByIds(List<int> inIds)
        {
            return Statics.Instance.quest.Values.Where(x => inIds.Contains(x.id)).ToList();
        }

        static public List<Quest> GetListByBattleObjectId(int inId)
        {
            return Statics.Instance.quest.Values.Where(x => x.battle_object_id == inId).ToList();
        }
    }
}