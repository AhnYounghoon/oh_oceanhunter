﻿using System.Linq;
using System.Collections.Generic;

namespace ServerStatic
{
    public class Character
    {
        //[Auto generate code begin]
        public int id;
        public string battle_object_id;
        public string name;
        public int star;
        public int category;
        public string type;
        public int cost;
        public float hp;
        public float damage;
        public float defense;
        public float attack_range;
        public float speed;
        public int leadership;
        public float hit_percent;
        public float critical_percent;
        public float critical_damage;
        public string attack_id;
        public string skill_id;
        public string icon_path;
        public string illust_path;
        //[Auto generate code end]

        public enum ECatetory
        {
            HERO = 1,
            SAILOR = 2,
        }

        static public List<Character> GetListByCategory(int inValue)
        {
            return Statics.Instance.character.Values.Where(x => x.category == inValue).ToList();
        }

        static public Character GetById(int inValue)
        {
            return Statics.Instance.character.Values.Where(x => x.id == inValue).FirstOrDefault();
        }
    }
}