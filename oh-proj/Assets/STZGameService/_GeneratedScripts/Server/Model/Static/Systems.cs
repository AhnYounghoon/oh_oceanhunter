﻿namespace ServerStatic{
	public class Systems{
//[Auto generate code begin]
		public string key;
		public string value;
//[Auto generate code end]

        public double doubleValue
        {
			get
            {
				return value.DoubleValue();
			}
		}

		public int intValue
        {
			get
            {
				return value.IntValue();
			}
		}

		public float floatValue
        {
			get
            {
				return value.FloatValue();
			}
		}

        public static string goldCardSwitchVer
        {
            get
            {
#if UNITY_IOS
                return Statics.Instance.systems["ios_goldcard_ver"].value;
#else
                return Statics.Instance.systems["aos_goldcard_ver"].value;
#endif
            }
        }

        public static string topClientVerAmongMarkets
        {
            get
            {
                string iosVer = Statics.Instance.systems["ios_market_version"].value;
                string aosVer = Statics.Instance.systems["aos_market_version"].value;

                return STZCommon.CheckVersion(iosVer, aosVer) >= 0 ? iosVer : aosVer;
            }
        }

        public static string serviceClientVer
        {
            get
            {
#if UNITY_IOS
                return Statics.Instance.systems["ios_market_version"].value;
#else
                return Statics.Instance.systems["aos_market_version"].value;
#endif
            }
        }

        public static bool enableActionLog
        {
            get
            {
                return Statics.Instance.systems.ContainsKey("action_log") ? Statics.Instance.systems["action_log"].value.IntValue() != 0 : false;
            }
        }

        public static string supportBlockEnableVer
        {
            get
            {
#if UNITY_IOS
                return Statics.Instance.systems.ContainsKey("ios_support_block_enable_client_ver") ? Statics.Instance.systems["ios_support_block_enable_client_ver"].value : "3.0.0";
#else                
                return Statics.Instance.systems.ContainsKey("aos_support_block_enable_client_ver") ? Statics.Instance.systems["aos_support_block_enable_client_ver"].value : "3.0.0";
#endif
            }
        }
    }
}