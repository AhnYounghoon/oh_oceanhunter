﻿using System.Collections.Generic;
using UniLinq;

namespace ServerStatic
{
    public class Goods
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public string icon_path;
        //[Auto generate code end]

        static public Goods GetItemById(int inId)
        {
            return Statics.Instance.goods.Values.Where(x => x.id == inId).FirstOrDefault();
        }
    }
}