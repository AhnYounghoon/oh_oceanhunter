﻿namespace ServerStatic
{
    public class Skill
    {
        //[Auto generate code begin]
        public int id;
        public string name;
        public string desc;
        public int grade;
        public string type;
        public float range;
        public float cool_time;
        public int excute_condition_id;
        public int excute_target_id;
        public int excute_effect_id;
        //[Auto generate code end]
    }
}