﻿namespace ServerStatic{
	public class GameEvent{
//[Auto generate code begin]
		public int id;
		public int event_static_id;
		public string event_name;
		public string country;
		public int condition;
		public int condition_value;
		public string start_at_google;
		public string end_at_google;
		public string start_at_ios;
		public string end_at_ios;
		public string google_c_ver;
		public int google_chk;
		public string ios_c_ver;
		public int ios_chk;
		public string event_script;
//[Auto generate code end]

        public System.DateTime GetStartTime()
        {
            System.DateTime startTime;

#if UNITY_IOS
            if (!System.DateTime.TryParse(start_at_ios, out startTime))
#else
            if (!System.DateTime.TryParse(start_at_google, out startTime))
#endif
                return System.DateTime.MaxValue;
            else
                return startTime;
        }

        public System.DateTime GetEndTime()
        {
            System.DateTime endTime;

#if UNITY_IOS
            if (!System.DateTime.TryParse(end_at_ios, out endTime))
#else
            if (!System.DateTime.TryParse(end_at_google, out endTime))
#endif
                return System.DateTime.MinValue;
            else
                return endTime;
        }

        public string GetClientVer()
        {
#if UNITY_IOS
            return ios_c_ver;
#else
            return google_c_ver;
#endif
        }

        public int GetVersionChecker()
        {
#if UNITY_IOS
            return ios_chk;
#else
            return google_chk;
#endif
        }
    }
}