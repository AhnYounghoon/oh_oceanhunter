﻿using UnityEngine;
using System.Collections.Generic;

public enum ERewardType
{
    NONE = 0,
    GOLD = 1,
    SILVER = 2,
    ITEM = 3,
    HEART = 5,
    EXP = 8,
    CHARACTER = 102,
    GOLD_BOX = 103,
    SILVER_BOX = 104,
    EMOTICON = 104,
    COUPON = 105,
    INFINITY_HEART = 106,
    MESSAGE = 107,
    GEM_ROAD_REWARD = 108,
    GEM_TREE_REWARD = 109,
    CHARACTER_COUPON = 110, // 캐릭터 교환가능한 쿠폰
    DUMMY_REWARD = 999 // 클라이언트만에 보여질 더미 이미지용 (리소스는 http://anipang3-cdn.stzapp.com/pds/DummyItemReward/DummyItemReward_{0:D2}.png)

    //SERVER_ROULETTE_REWARD = 1001 // NOTE @seungwon 시트 전용. 보상은 서버에서 _roulette_reward 시트를 참조하여 확률 아이템을 전송해준다
}

public class RewardData
{
    public int reward_type;
    public int reward_count;
    public int reward_sub;
    public int reward_eventMsg;

    // 무제한 하트 시간 체크를 위한 변수
    public float reward_time;

    // 해당 타입 인덱스 갯수 적합 여부
    public bool isSuitableIndex;

    public string name;
    public string description;
    // 코스튬으로 획득할 때 아이콘 패스 //
    public string costumeRewardIconPath;
    // 행운 상자 보상 아이콘 패스 //
    public string iconPath;
    // 메시지 타입으로 전송한것인지 체크 //
    public bool messageType;

    public string itemId { get { return StringHelper.Format("{0}:{1}:{2}", reward_type, reward_sub, reward_count); } }

    public RewardData(string obj)
    {
        // 기본 [type][itemId][count]
        isSuitableIndex = true;
        messageType = false;
        ParseRewardData(obj);
    }

    /// <summary>
    /// 리워드 데이터를 리워드 타입에 맞게 파싱한다.
    /// </summary>
    /// <param name="obj"></param>
    private void ParseRewardData(string obj)
    {
        string[] item = obj.Split(':');
        if (item.Length < 3)
        {
            reward_type = 0;
            reward_sub = 0;
            reward_count = 0;
        }
        else
        {
            reward_type = item[0].IntValue();
            reward_sub = item[1].IntValue();
            reward_count = item[2].IntValue();
            if (item.Length > 3)
            {
                isSuitableIndex = false;
            }
        }

        switch ((ERewardType)reward_type)
        {
            //case ERewardType.HEART:
            case ERewardType.SILVER:
            case ERewardType.GOLD:
            case ERewardType.EXP:
            case ERewardType.GOLD_BOX:
            case ERewardType.SILVER_BOX:
                //    if (Statics.Instance.payment_data.ContainsKey(reward_type))
                //    {
                //        ServerStatic.PaymentData value = null;
                //        // RewardType과는 연관없이 payment_data에서 사용하기 위해 104으로 사용한다
                //        // 하트 패키지는 표기때문에 5번을 사용해야한다 (분류는 골드카드와 동일함)
                //        if (reward_sub == 2)
                //        {
                //            value = Statics.Instance.payment_data[5];
                //            reward_count = 10;
                //        }
                //        else
                //        {
                //            value = Statics.Instance.payment_data[reward_type];
                //        }
                //        name = value.name.Locale();
                //        description = value.description.Locale();
                //        costumeRewardIconPath = value.costume_icon_path;
                //        iconPath = value.reward_icon_path;
                //    }
                if (Statics.Instance.goods.ContainsKey(reward_type))
                {
                    ServerStatic.Goods value = Statics.Instance.goods[reward_type];
                    name = value.name.Locale();
                    iconPath = value.icon_path;
                }
                break;

            //case ERewardType.ITEM:
            //    if (Statics.Instance.item.ContainsKey(reward_sub))
            //    {
            //        var value = Statics.Instance.item[reward_sub];
            //        name = value.name.Locale();
            //        description = value.desc.Locale();
            //        costumeRewardIconPath = value.costume_icon_path;
            //        iconPath = value.reward_icon_path;
            //    }
            //    break;

            case ERewardType.CHARACTER:
                if (Statics.Instance.character.ContainsKey(reward_sub))
                {
                    var value = Statics.Instance.character[reward_sub];
                    name = value.name.Locale();
                    //description = value.resource_path.Locale();
                    iconPath = value.icon_path;
                }
                break;

            //case ERewardType.EMOTICON:
            //    if (Statics.Instance.emoticon_reward.ContainsKey(reward_sub))
            //    {
            //        var value = Statics.Instance.emoticon_reward[reward_sub];
            //        name = value.name;
            //        description = string.Empty;
            //        iconPath = value.reward_icon_path;
            //    }
            //    break;

            //case ERewardType.COUPON:
            //    if (Statics.Instance.coupon_msg.ContainsKey(reward_sub))
            //    {
            //        var value = Statics.Instance.coupon_msg[reward_sub];
            //        name = value.name;
            //        description = string.Empty;
            //        iconPath = value.fullIconUrl;
            //    }
            //    break;

            case ERewardType.INFINITY_HEART:
                // 무제한 하트는 float 단위 처리가 가능해야하기 때문에 따로 변수를 생성
                if (item.Length >= 3)
                    reward_time = item[2].FloatValue();

                // 무제한 하트는 1개만 리워드 지정가능하다.
                reward_count = 1;
                name = StringHelper.Format("INFINITY_HEART_ITEM_NAME".Locale(), reward_time);
                description = StringHelper.Format("INFINITY_HEART_ITEM_DESC".Locale(), reward_time);
                iconPath = "Image/ItemIcon/item_infinity_heart";
                break;

            case ERewardType.MESSAGE:
                // Message Reward 구성은 기존 구성과 다르기 때문에 분류 이후 변경
                // [RewardType][EventMsg][RewardType][ItemId][Count]
                reward_type = 0;
                reward_sub = 0;
                reward_count = 0;
                reward_eventMsg = 0;
                isSuitableIndex = true;
                messageType = true;

                if (item.Length >= 5)
                {
                    reward_eventMsg = item[1].IntValue();
                    reward_type = item[2].IntValue();
                    reward_sub = item[3].IntValue();
                    reward_count = item[4].IntValue();
                    if (item.Length > 5)
                    {
                        isSuitableIndex = false;
                    }
                }
                if (reward_type == ((int)ERewardType.MESSAGE))
                {
                    reward_type = 0;
                    reward_sub = 0;
                    reward_count = 0;
                }
                else
                {
                    // 하위 리워드 형식은 기존의 리워드 방식과 같기 때문에 재귀
                    ParseRewardData(StringHelper.Format("{0}:{1}:{2}", reward_type, reward_sub, reward_count));
                }
                break;
            case ERewardType.DUMMY_REWARD:
                iconPath = StringHelper.Format("http://anipang3-cdn.stzapp.com/pds/DummyItemReward/DummyItemReward_{0:D2}.png", reward_sub);
                break;

        }
    }

    /// <summary>
    /// 여러개의 아이템 아이디를 파싱한다.
    /// </summary>
    /// <param name="item_ids"></param>
    /// <returns></returns>
    public static RewardData[] ParseItemIDs(string item_ids)
    {
        string[] rwdString = item_ids.Split(',');
        RewardData[] rewardDataList = new RewardData[rwdString.Length];
        for (int i = 0; i < rewardDataList.Length; i++)
        {
            rewardDataList[i] = new RewardData(rwdString[i]);
        }
        return rewardDataList;
    }

    /// <summary>
    /// reward의 type과 sub가 같으면 합친다.
    /// </summary>
    /// <param name="item_ids"></param>
    /// <returns></returns>
    public static List<RewardData> ParseMergedItemIds(string item_ids)
    {
        string[] rwdString = item_ids.Split(',');
        List<RewardData> rewardDataList = new List<RewardData>();
        for (int i = 0; i < rwdString.Length; i++)
        {
            var reward = new RewardData(rwdString[i]);

            bool isMerged = false;
            for (int j = 0; j < rewardDataList.Count; j++)
            {
                if (rewardDataList[j].reward_type == reward.reward_type
                    && rewardDataList[j].reward_sub == reward.reward_sub)
                {
                    rewardDataList[j].reward_count += reward.reward_count;
                    isMerged = true;
                }
            }

            if (!isMerged)
                rewardDataList.Add(reward);
        }

        return rewardDataList;
    }
}