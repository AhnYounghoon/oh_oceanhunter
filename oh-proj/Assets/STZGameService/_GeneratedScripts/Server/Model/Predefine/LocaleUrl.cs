﻿using UnityEngine;
using SimpleJSON;
using System.IO;

namespace ServerStatic
{
    public class LocaleUrl
    {
        public string ko;
        public string ja;
        public string en;
        public string zh;
        public string th;

        static private SimpleJSON.JSONClass s_locale;

        const string __LANG__ = "__LANG__";
        const string __VERSION__ = "__VERSION__";

        public void SetLocale(string url, string jsonText)
        {
            if (jsonText[0] == '[')
            {
                s_locale = new JSONClass();
                jsonText = "{ \"data\": " + jsonText + " }";
                SimpleJSON.JSONClass jc = SimpleJSON.JSONClass.Parse(jsonText) as SimpleJSON.JSONClass;
                JSONArray list = jc["data"].AsArray;
                for (int i = 0; i < list.Count; i++)
                {
                    s_locale.Add((list[i].AsObject as JSONClass)["key"], (list[i].AsObject as JSONClass)["data"]);
                }
            }
            else
            {
                SimpleJSON.JSONClass js = SimpleJSON.JSONClass.Parse(jsonText) as SimpleJSON.JSONClass;
                if (js != null)
                {
                    if (s_locale != null)
                        Debug.LogFormat("pre locale data => {0}", s_locale.ToString());
                    s_locale = js;
                    if (s_locale != null)
                        Debug.LogFormat("post locale data => {0}", s_locale.ToString());
                }
            }

            // REMOVED ME @sangmoon ?? 의미없는 코드 
            //foreach (string key in s_locale.Keys)
            //{
            //    string v = s_locale[key];
            //    if (v == null)
            //        continue;
            //}

            SaveToLocal(url);
        }

        /// <summary>
        /// TEST 로케일 셋팅
        /// </summary>
        /// <param name="jsonText"></param>
        public void SetLocale(string jsonText)
        {
            s_locale = new JSONClass();
            jsonText = "{ \"data\": " + jsonText + " }";
            JSONClass jc = JSONClass.Parse(jsonText) as JSONClass;
            JSONArray list = jc["data"].AsArray;
            for (int i = 0; i < list.Count; i++)
            {
                s_locale.Add(list[i]["id"].Value, list[i]["ko"].Value);
            }
        }

        public void SaveToLocal(string url)
        {
            string fn = GetFileName(url);
            string path = string.Format("{0}/{1}.dat", Application.temporaryCachePath, fn.MD5());
            s_locale.SaveToFile(path);

#if UNITY_EDITOR
			GameService.Misc.FileWriteAllText(string.Format("{0}/Resources/Statics/ko.txt", Application.dataPath, fn), s_locale.SaveToCompressedBase64());
#endif
        }

        /// <summary>
        /// 캐시된 로케일 파일 로딩
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool LoadFromLocal(string url)
        {
            string fn = GetFileName(url);
            string path = string.Format("{0}/{1}.dat", Application.temporaryCachePath, fn.MD5());

            // 최신 버전의 캐싱된 언어팩이 있다면 
            if (File.Exists(path))
            {
                s_locale = JSONClass.LoadFromFile(path) as JSONClass;
                return true;
            }

            // NOTE @neec 현재 설정된 언어팩 로드에 모두 실패했다면 기본 언어팩을 사용하도록 한다
            string resourcePath = string.Format("Statics/{0}", fn);
            TextAsset txtAsset = Resources.Load<TextAsset>(resourcePath);
            if (txtAsset == null)
            {
                resourcePath = string.Format("Statics/{0}", "ko");
                txtAsset = Resources.Load<TextAsset>(resourcePath);
                if (txtAsset != null)
                    s_locale = JSONNode.LoadFromCompressedBase64(txtAsset.text) as JSONClass;
                return false;
            }

            s_locale = JSONClass.Parse(txtAsset.text) as JSONClass;

            return (s_locale != null);
        }

        /// <summary>
        /// 현재 버전의 locale파일이 최신인지 검사, GS init을 한 후에 호출해줘야 유효함 
        /// </summary>
        /// <returns></returns>
        public bool IsLatestVersion()
        {
            string url = GetUrl();

            string fn = GetFileName(url);
            string path = string.Format("{0}/{1}.dat", Application.temporaryCachePath, fn.MD5());

            return File.Exists(path);
        }

        string GetFileName(string url)
        {
            return url.Remove(0, url.LastIndexOf("locale/") + 7).Replace(".", "_");
        }

        public string GetUrl()
        {
            string url = ko;
            // FIXME @sangmoon 16.09.28 추후 글로벌 세팅 언어 설정값 읽어오도록 변경 
            //switch (Application.systemLanguage)
            //{
            //    case SystemLanguage.Korean:
            //        url = ko;
            //        break;

            //    case SystemLanguage.Japanese:
            //        url = ja;
            //        break;

            //    case SystemLanguage.Chinese:
            //        url = zh;
            //        break;

            //    default:
            //        url = en;
            //    break;
            //}

            //if (Config.IsDev)
            {
                string[] urlArr = url.Split('/');
                url = string.Format("https://s3-ap-northeast-1.amazonaws.com/dev.anipang3.static/locale/{0}", urlArr[urlArr.Length - 1]);
            }

            return url;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetText_Static(string key)
        {
            if (key == null)
            {
                return string.Empty;
            }

            if (s_locale == null)
            {
                return key;
            }

            if (!s_locale.ContainsKey(key))
            {
                return key;
            }

            return s_locale[key];
        }

        public string GetText(string key)
        {
            if (key == null)
                return string.Empty;
            if (s_locale == null)
            {
                return key;
            }
            if (!s_locale.ContainsKey(key))
            {
                return key;
            }
            return s_locale[key];
        }

        public bool ContainKey(string key)
        {
            if (key.IsNullOrEmpty())
            {
                return false;
            }

            return s_locale.ContainsKey(key);
        }
    }
}