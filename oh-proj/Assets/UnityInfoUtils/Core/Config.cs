using System.Collections.Generic;
using UnityEngine;

namespace Settings
{
    public class SettingsConfig
    {
        private const string _prefKey = "UnitySettings_Config";
        public string StartView;

        public static SettingsConfig LoadFromPrefs()
        {
            var json = PlayerPrefs.GetString(_prefKey);
            if (string.IsNullOrEmpty(json)) return new SettingsConfig();
            return JsonUtility.FromJson<SettingsConfig>(json);
        }

        public void SaveToPrefs()
        {
            var json = JsonUtility.ToJson(this);
            PlayerPrefs.SetString(_prefKey, json);
        }
    }
}
