using UnityEngine;
using SystemInfo = Settings.Extension.SystemInfo;
using Log = Settings.Extension.Log;

namespace Settings
{
    public class UnityInfoInstaller : MonoBehaviour
    {
        private static Settings _instance;
        public static Settings Instance
        {
            get
            {
                Install();
                return _instance;
            }
        }
        private static Log.Watch watch;

        private static bool InstallWithoutCreateInstaller()
        {
            if (_instance != null)
                return false;

            _instance = Object.FindObjectOfType<Settings>();
            var shouldInstantiate = _instance == null;
            if (shouldInstantiate)
            {
                var singleton = new GameObject("Settings (Singleton)");
                _instance = singleton.AddComponent<Settings>();
                DontDestroyOnLoad(singleton);
            }

            InjectDependency(_instance);
            return shouldInstantiate;
        }

        public static void Install()
        {
            if (!Application.isPlaying) return;
            if (!InstallWithoutCreateInstaller()) return;
            new GameObject("Installer (For Recompile)").AddComponent<UnityInfoInstaller>();

            Debug.Log("Success Unity Info Utils");
        }

        private void Awake()
        {
            Install();
        }

        private void OnEnable()
        {
        }

        private static void InjectDependency(Settings settings)
        {
            // inject SystemInfo
            {
                var view = new SystemInfo.View();
                settings.AddView(view);
            }

            // inject Log
            {
                var provider = new Log.Provider();
                var sampler = new Log.Sampler();
                watch = new Log.Watch(provider, sampler);
                _instance.logList = watch.Stash.logs;

                settings.AddBehaviourListener(watch);

                var viewConfig = new Log.View.Config();
                var stash = watch.Stash;
                var organizer = stash.Organizer;
                var view = new Log.View(viewConfig, organizer);
                view.OnClickClear += () => stash.Clear();
                settings.AddView(view);
            }
        }
    }
}
