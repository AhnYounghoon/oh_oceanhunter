using UnityEngine;
using BehaviourListeners = System.Collections.Generic.List<Settings.IBehaviourListener>;
using Log = Settings.Extension.Log;

namespace Settings
{
    public class Settings : MonoBehaviour
    {
        public static readonly Vector2 RESOLUTION_SIZE = new Vector2(1920, 1080);

        private bool _isInited { get { return _guiDrawer != null; } }

        // private Config _config;
        private GUI.Drawer _guiDrawer;
        private bool _isShowingGUI;
        private GameObject eventSystemGameObject = null;
        private Vector3 resolutionSize;
        public System.Collections.Generic.List<Extension.Log.Log> logList;

        private bool showGUI
        {
            set
            {
                // NOTE @minjun GUI 표시 중에 인게임 터치를 막기 위함
                if (value && UnityEngine.EventSystems.EventSystem.current != null)
                {
                    eventSystemGameObject = UnityEngine.EventSystems.EventSystem.current.gameObject;
                }
                if (eventSystemGameObject != null)
                {
                    eventSystemGameObject.SetActive(!value);
                }
                _isShowingGUI = value;
            }
        }
        private IGesture _gesture;
        private readonly BehaviourListeners _behaviourListeners = new BehaviourListeners(8);

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            if (_isInited) return;
            // _config = Config.LoadFromPrefs();
            _guiDrawer = new GUI.Drawer();

            // 로그 창 닫기
            _guiDrawer.OnClose += () => showGUI = false;

            // 최대 100개의 일반 로그를 Slack으로 전송
            _guiDrawer.OnSendLog += () => 
            {
                string text = string.Empty;
                int startValue = (logList.Count - 100 >= 0) ? logList.Count - 100 : 0;
                for (int i = startValue; i < logList.Count;i++)
                {
                    // [ , ] 포함은 json Data가 많아 제외한다
                    if (logList[i].Type != LogType.Log || logList[i].Message.Contains("[") || logList[i].Message.Contains("]"))
                        continue;

                    text += StringHelper.Format("{0}\n", logList[i].Message);
                }

                //StzLog.Logger.SendToRemote(text);
            };

            var gestureLeastLength = (Screen.width + Screen.height) / 4;
            _gesture = new CircleGesture(new TouchProvider(), gestureLeastLength);

            //calculate the rescale ratio
            float guiRatioX = (float)Screen.width  / RESOLUTION_SIZE.x;
            float guiRatioY = (float)Screen.height / RESOLUTION_SIZE.y;
            //create a rescale Vector3 with the above ratio
            resolutionSize = new Vector3(guiRatioX, guiRatioY, 1);
        }

        private void OnEnable()
        {
            Init();
            foreach (var l in _behaviourListeners)
                l.OnEnable();
        }

        private void OnDestroy()
        {
            foreach (var l in _behaviourListeners)
                l.OnDisable();
            // _config.SaveToPrefs();
        }

        private void Update()
        {
            Init();
            if (Input.GetKeyDown(KeyCode.Escape))
                showGUI = false;
            if (_isShowingGUI)
            {
                Util.Mouse.RefreshPos();
                _guiDrawer.Update();
            }
            foreach (var l in _behaviourListeners)
                l.Update(_isShowingGUI);
            DetectGesture();
        }

        /// <summary>
        /// 제스쳐 등록
        /// </summary>
        private void DetectGesture()
        {
            if (_isShowingGUI) return;
            _gesture.SampleOrCancel();
            if (_gesture.CheckAndClear())
                showGUI = true;
        }

        private void OnGUI()
        {
            UnityEngine.GUI.skin.verticalScrollbar.fixedWidth = 40;
            UnityEngine.GUI.skin.verticalScrollbarThumb.fixedWidth = 40;

            Init();
            if (!_isShowingGUI) return;
            var orgDepth = UnityEngine.GUI.depth;
            UnityEngine.GUI.matrix = Matrix4x4.Scale(resolutionSize);

            UnityEngine.GUI.depth = -1000;
            _guiDrawer.OnGUI();

            UnityEngine.GUI.depth = orgDepth;
        }

        public void AddBehaviourListener(IBehaviourListener behaviourListener)
        {
            Init();
            _behaviourListeners.Add(behaviourListener);
            if (enabled) behaviourListener.OnEnable();
        }

        public void AddView(GUI.IView view)
        {
            Init();
            _guiDrawer.Add(view);
        }
    }
}
