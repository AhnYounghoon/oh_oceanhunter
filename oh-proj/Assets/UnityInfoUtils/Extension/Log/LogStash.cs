using Logs = System.Collections.Generic.List<Settings.Extension.Log.Log>;
using ReadOnlyLogs = System.Collections.ObjectModel.ReadOnlyCollection<Settings.Extension.Log.Log>;

namespace Settings.Extension.Log
{
    internal class Stash
    {
        public readonly Logs logs;
        private readonly ReadOnlyLogs _logsReadOnly;
        private readonly Util.StringCache _strCache;

        public readonly Organizer Organizer;

        public Stash()
        {
            logs = new Logs(256);
            _logsReadOnly = logs.AsReadOnly();
            _strCache = new Util.StringCache();
            Organizer = new Organizer(this);
        }

        public void Clear()
        {
            logs.Clear();
            _strCache.Clear();
            Organizer.Clear();
        }

        /// <summary>
        /// 로그를 등록한다
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="sample"></param>
        public void Add(RawLog raw, Sample sample)
        {
            var msg = _strCache.Cache(raw.Message);
            var stacktrace = _strCache.Cache(raw.Stacktrace);
            var newLog = new Log(raw.Type, msg, stacktrace, sample);
            logs.Add(newLog);
            Organizer.Internal_AddToCache(newLog);
        }

        public ReadOnlyLogs All()
        {
            return _logsReadOnly;
        }
    }
}