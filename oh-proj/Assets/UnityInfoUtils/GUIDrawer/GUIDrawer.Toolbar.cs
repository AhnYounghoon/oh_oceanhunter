using UnityEngine;

namespace Settings.GUI
{
    internal partial class Drawer
    {
        private static Rect _toolbarIconRect = new Rect(0, 0, _toolbarH, _toolbarH);

        private bool DrawToolbarToggle(GUIContent icon, bool value)
        {
            var style = value ? Styles.ToolbarButtonOn : Styles.ToolbarButton;
            var clicked = UnityEngine.GUI.Button(_toolbarIconRect, icon, style);
            if (clicked) value = !value;
            return value;
        }

        /// <summary>
        /// ��� ���� Draw
        /// </summary>
        /// <param name="area"></param>
        private void OnGUIToolbar(Rect area)
        {
            // draw views
            var viewArea = new Rect(area); viewArea.width -= _toolbarH;
            _toolbarIconRect = new Rect(0, 0, _toolbarH, _toolbarH);
            _toolbarTable.OnGUI(viewArea, _views.Count, OnGUIToolbarIcon);

            // draw close
            var closeRect = new Rect(area.xMax - _toolbarH, area.y, _toolbarH, area.height);
            if (UnityEngine.GUI.Button(closeRect, Icons.Close, Styles.ToolbarButton))
                OnClose();

            GUIStyle style = Styles.ToolbarButton;
            style.fontSize = 28;

            // send all logs
            //var removeRect = new Rect(area.xMax - _toolbarH * 4, area.y, _toolbarH * 3, area.height);
            //if (UnityEngine.GUI.Button(removeRect, "Send Log", style))
            //    OnSendLog();
        }

        private void OnGUIToolbarIcon(int i)
        {
            var view = _views[i];
            var icon = view.ToolbarIcon;
            if (DrawToolbarToggle(icon, view == _curView))
                _curView = view;
        }
    }
}