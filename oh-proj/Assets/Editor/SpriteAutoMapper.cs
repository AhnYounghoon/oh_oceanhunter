﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.Reflection;
using System.IO;

[CustomEditor(typeof(MonoBehaviour), true), CanEditMultipleObjects]
public class SpriteAutoMapper : Editor
{
    //Reflection Test
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//        EditorGUILayout.LabelField("Write Atlas image name");
//        atlasName = EditorGUILayout.TextField("AtlasName : ", atlasName);
//
//        if(GUILayout.Button("Bind Sprite"))
//        {
//            string path = "";
//            string[] pathList = null;
//            string[] dirPaths = Directory.GetFiles(Application.dataPath + "/Resources", atlasName+".png" ,SearchOption.AllDirectories);
//            foreach(string currentPath in dirPaths)
//            {
//                string currentChanged = currentPath.Replace('\\', '/');
//                pathList = currentChanged.Split('/');
//            }
//            if(pathList == null) return;
//            int resCount = 9999;
//            for(int i = 0 ; i < pathList.Length; i++)
//            {
//                if(pathList[i] == "Resources")
//                {
//                    resCount = i;
//                }
//
//                if(i > resCount)
//                {
//                    path = path + "/" + pathList[i];
//                }
//            }
//            path = path.Substring(1, path.Length-5); 
//
//            Debug.Log("path : " + path);
//            Debug.Log("atlasName = "+atlasName);
//
//            Type targetType = target.GetType();
//            foreach (FieldInfo fieldInfo in targetType.GetFields())
//            {
//                if(fieldInfo.FieldType.ToString() == "UnityEngine.Sprite")
//                {
//                    Sprite[] blockSprites;
//                    blockSprites = Resources.LoadAll<Sprite>(path);
//                    foreach (Sprite sprite in blockSprites)
//                    {
//                        Debug.Log("spriteName : " + sprite.name);
//                        if(sprite.name == fieldInfo.Name)
//                        {
//                            fieldInfo.SetValue(target,sprite);
//                            break;
//                        }
//                    }
//
//                }
//            }
//        }
//        this.Repaint();
//    }

    const string AUTO_BEGIN = "//[Sprite-Auto generate code begin]";
    const string AUTO_END     = "//[Sprite-Auto generate code end]";
    [MenuItem("STZ/Add Sprite Atlas Script")]
    static void addSpriteListAtlas()
    {
        string reference = FileManager.FileReadAllText(    string.Format("{0}/StzFrameWork/Sprite_Reference.txt",Application.dataPath) );

        Component[] components = Selection.activeTransform.GetComponents<Component>();
        string fileName = "";
        foreach(Component currentComponent in components)
        {
            Debug.Log("toSTR : " + currentComponent.GetType().IsSubclassOf(typeof(MonoBehaviour)));
            Debug.Log("fullname : " + currentComponent.GetType().FullName);
            if(currentComponent.GetType().IsSubclassOf(typeof(MonoBehaviour)))
            {
                fileName = currentComponent.GetType().FullName;
            }
        }

        string result ="";
        string atlasPath = EditorUtility.OpenFilePanel("Open Atlas Image", Application.dataPath, "png");
        Debug.Log("atlasPath : " + atlasPath);
        string[] pathList = null;
        atlasPath = atlasPath.Replace('\\', '/');
        pathList = atlasPath.Split('/');
        atlasPath = "";
        int resCount = 9999;
        for(int i = 0 ; i < pathList.Length; i++)
        {
            if(pathList[i] == "Resources")
            {
                resCount = i;
            }

            if(i > resCount)
            {
                atlasPath = atlasPath + "/" + pathList[i];
            }
        }
        atlasPath = atlasPath.Substring(1, atlasPath.Length-5); 
        string atlasName = pathList[pathList.Length-1];
        Debug.Log("atlasName = "+ atlasName);

        string sprites = "\n";

        Sprite[] blockSprites;
        blockSprites = Resources.LoadAll<Sprite>(atlasPath);
        for(int i=0;i<blockSprites.Length;i++)
        {    
            sprites += ("\t\t"+blockSprites[i].name + ",\n");    
        }
        sprites += "\t\tMax\n";


        if(reference.LastIndexOf("[SPR_Names]")!=-1 ){    reference = reference.Replace("[SPR_Names]"    ,    sprites);    }
        if(reference.LastIndexOf("[Atlas_Name]")!=-1 ){    reference = reference.Replace("[Atlas_Name]"    ,    atlasName );}

        string saveFilePath = EditorUtility.SaveFilePanel("Save Script", Application.dataPath,fileName, "cs");
        if(FileManager.FileExists(saveFilePath))
        {
            string origin = FileManager.FileReadAllText(saveFilePath);
            int start = origin.LastIndexOf(AUTO_BEGIN);
            int end = origin.LastIndexOf(AUTO_END);

            if(origin.LastIndexOf("using System.Collections;") == -1) origin = "using System.Collections;\n" + origin;
            if(origin.LastIndexOf("using System.IO;") == -1) origin = "using System.IO;\n" + origin;

            if(start == -1 && end == -1)
            {
                int classIdx = origin.IndexOf("{");
                while(origin[classIdx] != '\n')
                {
                    classIdx++;
                }
                Debug.Log("origin[classIdx]" + origin[classIdx]);

                int insertedTagStart = classIdx;
                int insertedTagEnd = classIdx+1;

                string first = origin.Substring(0, insertedTagStart);
                Debug.Log("first : " + first);
                string second = origin.Substring(insertedTagEnd, origin.Length - insertedTagEnd);
                Debug.Log("second : " + second);
                origin = first + "\n" + AUTO_BEGIN + "\n" + AUTO_END + "\n" + second;
                Debug.Log("origin" + origin);
            }

            start = origin.LastIndexOf(AUTO_BEGIN);
            end = origin.LastIndexOf(AUTO_END);

            Debug.Log("start" + start);
            Debug.Log("end" + end);

            string firstString = origin.Substring(0,start);
            string secondString = origin.Substring(end + AUTO_END.Length,origin.Length - (end+AUTO_END.Length));
            
            result = firstString +reference+ secondString;
        }else{
            result = reference;
        }
        FileManager.FileWriteAllText( saveFilePath, result);
        
        AssetDatabase.Refresh();
    }
}