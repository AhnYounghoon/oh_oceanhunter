﻿
using UnityEditor;
using UnityEditor.SceneManagement;


class SceneStartEditor : EditorWindow
{
    const string savePath = "Assets/LocalData/.lastScene";

    /// <summary>
    /// 다른 scene에서 작업을 하더라도, init을 위해 titlescene을 강제적으로 경유 
    /// </summary>
    [MenuItem("Play/Execute starting scene _%h")]
    public static void RunMainScene()
    {
        string currentSceneName = EditorSceneManager.GetActiveScene().name;
        System.IO.File.WriteAllText(savePath, currentSceneName);
        EditorSceneManager.OpenScene("Assets/Anipang3/Scenes/Title.unity");
        EditorApplication.isPlaying = true;
    }

    /// <summary>
    /// 작업 중이던 scene을 로드 
    /// </summary>
    [MenuItem("Play/Reload editing scene _%g")]
    public static void ReturnToLastScene()
    {
        string lastSceneName = System.IO.File.ReadAllText(savePath);
        EditorSceneManager.OpenScene(string.Format("Assets/Anipang3/Scenes/{0}.unity", lastSceneName));
    }
}
