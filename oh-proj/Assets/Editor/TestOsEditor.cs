﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestOsEditor : EditorWindow
{
    static string path = "Assets/LocalData/TestOsData.txt";

    public SimpleJSON.JSONNode userData;

    bool init = false;

    public enum eTargetPlatform
    {
        win,
        aos,
        ios,
        osx
    }

    void OnGUI()
    {
        if (!init)
        {
            init = true;

            var text = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            if (text == null)
            {
                userData = SimpleJSON.JSONNode.Parse("{}");
                userData["os"] = "win";
            }
            else
            {
                userData = SimpleJSON.JSONNode.Parse(text.text);
            }
        }

        if (userData == null)
        {
            init = false;
            return;
        }

        userData["os"] = EditorGUILayout.TextField("Target OS", userData["os"]);

        if (GUILayout.Button("저장하기"))
        {
            System.IO.File.WriteAllText(path, userData.ToString());
            AssetDatabase.Refresh();
            Debug.Log("OS 테스트 파일 저장이 완료되었습니다.");
        }

        if (GUILayout.Button("데이터 지우기"))
        {
            System.IO.File.Delete(path);
            AssetDatabase.Refresh();
            Debug.Log("OS 테스트 파일 삭제가 완료되었습니다.");
        }
    }

    [MenuItem("SundaytozDebug/EditTestOS")]
    static void Open()
    {
        TestOsEditor window = (TestOsEditor)EditorWindow.GetWindow(typeof(TestOsEditor));
        window.Show();
    }
}
