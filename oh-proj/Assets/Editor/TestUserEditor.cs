﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestUserEditor : EditorWindow
{
    static string path = "Assets/LocalData/TestUserData.txt";

    public SimpleJSON.JSONNode userData;

    bool init = false;

    void OnGUI()
    {
        if (!init)
        {
            init = true;

            var text = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            if (text == null)
            {
                userData = SimpleJSON.JSONNode.Parse("{}");
                userData["user_id"] = SystemInfo.graphicsDeviceID + SystemInfo.deviceUniqueIdentifier;
                //userData["player_id"] = SystemInfo.deviceUniqueIdentifier.GetHash16Code().ToString();
                userData["nickname"] = "STAND_ALONE";
                userData["profile_image_url"] = "";
                userData["hashed_talk_user_id"] = "GUEST";
                userData["message_blocked"].AsBool = true;
                userData["verified"].AsBool = true;
                userData["country_iso"] = "KR";
            }
            else
            {
                userData = SimpleJSON.JSONNode.Parse(text.text);
            }
        }

        if (userData == null)
        {
            init = false;
            return;
        }

        string[] checkList = new string[] { "user_id", "player_id", "nickname", "profile_image_url", "hashed_talk_user_id", "message_blocked", "verified", "country_iso"};
        string[] TitleList = new string[] { "플렛폼 ID(string)", "플레이어 ID(string)", "닉네임(string)", "프로필 URL(string)", "hashed_talk_user_id(string)", "메시지 제한(bool)", "verified(bool)", "country_iso(string)" };

        for (int i = 0; i < checkList.Length; i++)
        {
            string value = checkList[i];
            userData[value].Value = EditorGUILayout.TextField(TitleList[i], userData[value].Value);
        }

        if (GUILayout.Button("저장하기"))
        {
            System.IO.File.WriteAllText(path, userData.ToString());
            AssetDatabase.Refresh();
            Debug.Log("테스트 유저 저장이 완료되었습니다. : " + path);
        }

        if (GUILayout.Button("데이터 지우기"))
        {
            System.IO.File.Delete(path);
            AssetDatabase.Refresh();
            Debug.Log("테스트 유저 삭제가 완료되었습니다. : " + path);
        }
    }

    [MenuItem("SundaytozDebug/EditTestUser")]
    static void Open()
    {
        TestUserEditor window = (TestUserEditor)EditorWindow.GetWindow(typeof(TestUserEditor));
        window.Show();
    }
}
