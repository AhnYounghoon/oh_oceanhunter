﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class FileEditor : MonoBehaviour {
	/// <summary>
	/// Removes the temporary cache folder.
	/// </summary>
	[MenuItem("FileEditor/RemoveTemporaryCacheFolder")]
	public static void RemoveTemporaryCacheFolder()
	{
		System.IO.Directory.Delete(Application.temporaryCachePath, true);
		Debug.Log("[CLEAR]" + Application.temporaryCachePath);
	}

	/// <summary>
	/// Removes the persistent data folder.
	/// </summary>
	[MenuItem("FileEditor/RemovePersistentDataFolder")]
	public static void RemovePersistentDataFolder()
	{
		System.IO.Directory.Delete(Application.persistentDataPath, true);
		Debug.Log("[CLEAR]" + Application.persistentDataPath);
	}
}
