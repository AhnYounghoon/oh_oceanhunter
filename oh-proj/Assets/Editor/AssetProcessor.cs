/*-------------------------------------------------------------------------------
Copyright 2013-2014 Dreamset Studios, Mario Rodriguez Garcia
	All Rights Reserved.
		
This is PUBLISHED PROPRIETARY SOURCE CODE of Dreamset Studios and Mario Rodriguez
García.
the contents of this file may not be disclosed to third parties, copied or
duplicated in any form, in whole or in part, without the prior written
permission of Dreamset Studios and Mario Rodríguez García.
---------------------------------------------------------------------------------*/

using System;
using UnityEngine;
using UnityEditor;
using System.Collections;

// This class is an Asset postprocessor. It allows to import assets with our own Import settings
class ProjectAssetPostprocessor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        TextureImporter importer = (TextureImporter)assetImporter;
        if (importer.assetPath.Contains("CrossPromotion/Assets"))
            return;

        importer.spritePixelsPerUnit = 1;
        importer.maxTextureSize = 2048;
        importer.mipmapEnabled = false;
        importer.filterMode = FilterMode.Bilinear;

        // NOTE @minjun.ha 기본 텍스처 포멧의 변경에 따라 Obsolete 레벨 처리로 더이상 사용할 수 없는 값으로 변경됨
        //importer.textureFormat = TextureImporterFormat.RGBA32;
        importer.textureCompression = TextureImporterCompression.Uncompressed;
    }
}