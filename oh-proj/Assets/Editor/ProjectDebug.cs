﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ProjectDebug
{
#if UNITY_EDITOR_WIN
    [MenuItem("SundaytozDebug/AndroidDebugConsole")]
    public static void ShowAndroidDebugConsole()
    {
        string batFilePath = Application.dataPath + "/androidDebug.bat";
        System.IO.File.WriteAllText(batFilePath, "adb logcat -s Unity");
        System.IO.File.AppendAllText(batFilePath, "\n@pause");
        Application.OpenURL(batFilePath);
    }

    [MenuItem("SundaytozDebug/Restart adb")]
    public static void RestartAdb()
    {
        string batFilePath = Application.dataPath + "/restartAdb.bat";
        System.IO.File.WriteAllText(batFilePath, "adb kill-server");
        System.IO.File.AppendAllText(batFilePath, "\nadb start-server");
        System.IO.File.AppendAllText(batFilePath, "\n@pause");
        Application.OpenURL(batFilePath);
    }
#endif
}
