﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestUserInitDataEditor : EditorWindow
{
    static string path = "Assets/LocalData/TestUserInitData.txt";

    public SimpleJSON.JSONNode userData;

    bool init = false;

    void OnGUI()
    {
        if (!init)
        {
            init = true;

            var text = AssetDatabase.LoadAssetAtPath<TextAsset>(path);
            if (text == null)
            {
                userData = SimpleJSON.JSONNode.Parse("{}");

                userData["platform_id"] = "platform_id";
                userData["player_id"] = "player_id";
                userData["uuid"] = "uuid";
                userData["os"] = "a";
                userData["device_id"] = "device_id";
                userData["device_name"] = "device_name";
                userData["access_token"] = "access_token";
                userData["lang"] = "Korean";
                userData["country"] = "KR";
                userData["push_agree"].AsBool = true;
            }
            else
            {
                userData = SimpleJSON.JSONNode.Parse(text.text);
            }
        }

        if (userData == null)
        {
            init = false;
            return;
        }

        string[] checkList = new string[] { "platform_id", "player_id", "uuid", "os", "lang", "country", "device_id", "device_name", "access_token", "push_agree" };
        string[] TitleList = new string[] { "플렛폼 ID(string)", "플레이어 ID(string)", "uuid(string)", "os(string)", "lang(string)", "country(string)", "device_id(string)", "device_name(string)", "access_token(string)", "push_agree(bool)" };

        for (int i = 0; i < checkList.Length; i++)
        {
            string value = checkList[i];
            userData[value].Value = EditorGUILayout.TextField(TitleList[i], userData[value].Value);
        }

        if (GUILayout.Button("저장하기"))
        {
            System.IO.File.WriteAllText(path, userData.ToString());
            AssetDatabase.Refresh();
            Debug.Log("테스트 유저 저장이 완료되었습니다. : " + path);
        }

        if (GUILayout.Button("데이터 지우기"))
        {
            System.IO.File.Delete(path);
            AssetDatabase.Refresh();
            Debug.Log("테스트 유저 삭제가 완료되었습니다. : " + path);
        }
    }

    [MenuItem("SundaytozDebug/EditTestUserInitData")]
    static void Open()
    {
        TestUserInitDataEditor window = (TestUserInitDataEditor)EditorWindow.GetWindow(typeof(TestUserInitDataEditor));
        window.Show();
    }
}
