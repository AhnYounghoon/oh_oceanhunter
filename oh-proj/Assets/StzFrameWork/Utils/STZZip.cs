﻿using UnityEngine;

using System.IO;
using ICSharpCode.SharpZipLib.BZip2;

namespace STZFramework
{
    /// <summary>
    /// 압축 관련 클래스
    /// SharpZipLib를 필요한 부분만 래핑함
    /// </summary>

    public class STZZip
    {
        /// <summary>
        /// 압축하기
        /// </summary>
        /// <param name="inByteArr">압축할 바이트 배열</param>
        /// <param name="inOffset">바이트 배열에서 압축을 할 시작점</param>
        /// <param name="inSize">압축할 바이트 배열의 사이즈</param>

        static public byte[] Compress(byte[] inByteArr, int inOffset, int inSize)
        {
            MemoryStream inStream = null;
            MemoryStream outStream = null;
            byte[] result = null;

            try
            {
                inStream = new MemoryStream(inByteArr, inOffset, inSize);
                outStream = new MemoryStream();
                BZip2.Compress(inStream, outStream, true, inSize);

                result = outStream.ToArray();
            }
            catch (IOException e)
            {
                Debug.LogError(e);
            }

            finally
            {
                if (inStream != null)
                    inStream.Close();

                if (outStream != null)
                    outStream.Close();
            }

            return result;
        }

        /// <summary>
        /// 압축 해제
        /// </summary>
        /// <param name="inData">압축 해제할 바이트 배열</param>
        /// <param name="inOffset">바이트 배열에서 압축 해제할 시작점</param>
        /// <param name="inSize">압축 해제할 바이트 배열의 사이즈</param>

        static public byte[] Decompress(byte[] inData, int inOffset, int inSize)
        {
            MemoryStream inStream = null;
            MemoryStream outStream = null;
            byte[] result = null;

            try
            {
                inStream = new MemoryStream(inData, inOffset, inSize);
                outStream = new MemoryStream();
                BZip2.Decompress(inStream, outStream, true);

                result = outStream.ToArray();
            }
            catch (IOException e)
            {
                Debug.LogError(e);
            }

            finally
            {
                if (inStream != null)
                    inStream.Close();

                if (outStream != null)
                    outStream.Close();
            }

            return result;
        }
    }
}

