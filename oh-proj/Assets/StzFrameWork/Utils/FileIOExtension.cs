﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using STZFramework;
using System.Linq;
using UniRx;

public class FileIOExtension
{
    public enum ERemoveMode
    {
        AllRemove,                  // 모든 캐시 파일을 지움
        RemoveSpecific,             // 특정 캐시 파일을 지움 (remove_cache_list)
        RemoveSpecificAndUnknown,   // 특정 캐시 파일을 지우고 정의되지 않은 파일을 모두 지움 
    }

    public static byte[] Serialize(object inObject)
    {
        MemoryStream ms = new MemoryStream();
        BinaryFormatter f = new BinaryFormatter();
        f.Serialize(ms, inObject);

        byte[] arr = ms.ToArray();
        ms.Close();
        return arr;
    }

    public static T Deserialize<T>(byte[] inByte)
    {
        MemoryStream ms = new MemoryStream(inByte);
        BinaryFormatter f = new BinaryFormatter();
        T output = (T)f.Deserialize(ms);
        ms.Close();
        return output;
    }

    public static T Copy<T>(T inObject)
    {
        return Deserialize<T>(Serialize(inObject));
    }

    /// <summary>
    /// 파일 저장하기.
    /// </summary>
    /// <param name="inObject">타겟 오브젝트</param>
    /// <param name="inFilePath">저장할 위치</param>
    /// <param name="inSecretKey">암호화키</param>
    /// <returns></returns>
    public static bool SaveAsFile(object inObject, string inFilePath, string inSecretKey)
    {
        if (inObject == null)
        {
            Debug.Log("저장할 데이터가 없습니다.");
            return false;
        }

        string folderPath = inFilePath.Substring(0, inFilePath.LastIndexOf("/"));
        if (!System.IO.Directory.Exists(folderPath))
        {
            System.IO.Directory.CreateDirectory(folderPath);
        }

        try
        {
            MemoryStream ms = new MemoryStream();

            BinaryFormatter f = new BinaryFormatter();
            f.Serialize(ms, inObject);

            byte[] byteArr = AES.EncryptFromStream(ms.ToArray(), inSecretKey);

            ms.Close();

            FileStream fs = new FileStream(inFilePath, FileMode.Create);
            fs.Write(byteArr, 0, byteArr.Length);
            fs.Close();
        }
        catch (System.IO.IOException e)
        {
            Debug.Log(string.Format("{0} 파일 저장에 실패하였습니다.({1})", inFilePath, e.ToString()));
            return false;
        }

        Debug.Log(inFilePath + " 파일 저장을 완료하였습니다.");

        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    private static Dictionary<string, IDisposable> _openedFileStreams = new Dictionary<string, IDisposable>();

    /// <summary>
    /// 새로운 thread를 생성하여 파일 저장 
    /// </summary>
    /// <param name="inObject"></param>
    /// <param name="inFilePath"></param>
    /// <param name="inSecretKey"></param>
    public static void AsyncSaveAsFile(object inObject, string inFilePath, string inSecretKey)
    {
        if (_openedFileStreams.ContainsKey(inFilePath))
        {
            UnityEngine.Debug.LogWarningFormat("[FILE_IO_EXTENTION] {0} is saving now please try later", inFilePath);
            return;
        }

        IDisposable stream = Observable.Start(() => Unit.Default)
                                .Do(T =>
                                {
                                    if (inObject == null)
                                    {
                                        return;
                                    }

                                    string folderPath = inFilePath.Substring(0, inFilePath.LastIndexOf("/"));
                                    if (!System.IO.Directory.Exists(folderPath))
                                    {
                                        System.IO.Directory.CreateDirectory(folderPath);
                                    }
                                    MemoryStream ms = new MemoryStream();

                                    BinaryFormatter f = new BinaryFormatter();
                                    f.Serialize(ms, inObject);

                                    byte[] byteArr = AES.EncryptFromStream(ms.ToArray(), inSecretKey);

                                    ms.Close();

                                    FileStream fs = new FileStream(inFilePath, FileMode.Create);
                                    fs.Write(byteArr, 0, byteArr.Length);
                                    fs.Close();

                                })
                                .ObserveOnMainThread()
                                .Subscribe(
                                (T) =>
                                {
                                    Debug.LogFormat("[ASYNC_FILE_IO_EXTENTION] Async save thread is done; {0}", inFilePath);
                                },
                                (E) =>
                                {
                                    Debug.LogFormat("[ASYNC_FILE_IO_EXTENTION] Async save thread is done with error; {0}, {1}", inFilePath, E.Message);
                                },
                                () =>
                                {
                                    // close file IO
                                    if (_openedFileStreams.ContainsKey(inFilePath))
                                    {
                                        _openedFileStreams[inFilePath].Dispose();
                                        _openedFileStreams.Remove(inFilePath);
                                    }
                                });

        _openedFileStreams.Add(inFilePath, stream);
    }

    /// <summary>
    /// 파일 불러오기.
    /// </summary>
    /// <param name="inObject">타겟 오브젝트</param>
    /// <param name="inFilePath">저장할 위치</param>
    /// <param name="inSecretKey">암호화키</param>
    /// <returns>성공 여부</returns>
    public static T LoadFromFile<T>(string inFilePath, string inSecretKey)
    {
        if (!System.IO.File.Exists(inFilePath))
        {
            UnityEngine.Debug.Log(inFilePath + "의 파일이 존재하지 않습니다.");
            return default(T);
        }

        try
        {
            FileStream fs = new FileStream(inFilePath, FileMode.Open);
            byte[] byteArr = new byte[fs.Length];
            fs.Read(byteArr, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();

            byte[] result = AES.DecryptFromStream(byteArr, inSecretKey);
            MemoryStream ms = new MemoryStream(result);
            BinaryFormatter f = new BinaryFormatter();
            T output = (T)f.Deserialize(ms);
            ms.Close();
            return output;
        }
        catch (System.IO.IOException e)
        {
            UnityEngine.Debug.Log(e.ToString());
            return default(T);
        }
    }

    /// <summary>
    /// 파일 삭제하기.
    /// </summary>
    /// <param name="inFilePath"></param>
    /// <returns></returns>
    public static bool RemoveFile(string inFilePath, bool isChangeAttributeNormal = false)
    {
        if (!System.IO.File.Exists(inFilePath))
        {
            UnityEngine.Debug.Log(inFilePath + "의 파일이 존재하지 않습니다.");
            return false;
        }

        if (isChangeAttributeNormal)
        {
            File.SetAttributes(inFilePath, FileAttributes.Normal);
        }
        File.Delete(inFilePath);
        return true;
    }

    /// <summary>
    /// 해당 디렉토리 내부 모든 파일 삭제
    /// </summary>
    /// <param name="inDirectoryPath">디렉토리 경로</param>
    /// <param name="inMode">파일 삭제 모드 설정</param>
    /// <returns></returns>
    /* TODO
    public static bool RemoveAllFiles(string inDirectoryPath, ERemoveMode inMode = ERemoveMode.AllRemove)
    {
        if (!Directory.Exists(inDirectoryPath))
        {
            UnityEngine.Debug.Log(inDirectoryPath + " 디렉토리가 존재하지 않습니다.");
            return false;
        }
        string[] files = Directory.GetFiles(inDirectoryPath);
        string[] dirs  = Directory.GetDirectories(inDirectoryPath);

        // 캐시의 경로로 해싱 및 변환
        Dictionary<int, ServerStatic.RemoveCacheList> dic = new Dictionary<int, ServerStatic.RemoveCacheList>();
        if (inMode != ERemoveMode.AllRemove)
        {
            dic = ServerStatic.RemoveCacheList.GetRealPath(Statics.Instance.remove_cache_list);
        }
        foreach (string file in files)
        {
            if (inMode == ERemoveMode.AllRemove)
            {
                RemoveFile(file, true);
            }
            else
            {
                ServerStatic.RemoveCacheList target = dic.Where(x => x.Value.name == file).FirstOrDefault().Value;
                if (target != null)
                {
                    if (target.active == "on")
                    {
                        RemoveFile(file, true);
                    }
                }
                else if (inMode == ERemoveMode.RemoveSpecificAndUnknown)
                {
                    RemoveFile(file, true);
                }
            }
        }

        // 디렉토리 내부 디렉토리가 있다면 재귀
        foreach (string dir in dirs)
        {
            RemoveAllFiles(dir);
        }
        return true;
    }*/
}
