﻿
using System.Diagnostics;


namespace STZFramework
{
    /// <summary>
    /// profiler을 세분화해서 분석할때 사용 
    /// ** begin 후 end를 꼭 호출해줄것 **
    /// </summary>
    public static class STZProfiler
    {
        [Conditional("PROFILER")]
        public static void Begin(string msg)
        {
            UnityEngine.Profiling.Profiler.BeginSample(msg);
        }

        [Conditional("PROFILER")]
        public static void End()
        {
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}
