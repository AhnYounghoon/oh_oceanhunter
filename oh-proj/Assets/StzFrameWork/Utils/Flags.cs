﻿
namespace STZFramework
{
    /// <summary>
    /// flag값을 관리하는 클래스, 독립적으로 사용할려면 2의 승수 값으로 사용해야함 
    /// ex : )
    // * enum {
    // *     flag1 = 1 << 0,
    // *     flag2 = 1 << 1,
    // *     flag3 = 1 << 2,
    // *     ...
    // * }
    /// </summary>
    public class Flags
    {
        private uint _data;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inInitailFlags"></param>
        public Flags(uint inInitailFlags = 0)
        {
            Reset(inInitailFlags);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            Reset(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inInitailFlags"></param>
        public void Reset(uint inInitailFlags = 0)
        {
            _data = inInitailFlags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inFlags"></param>
        public void Add(uint inFlags)
        {
            _data |= inFlags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inFlags"></param>
        public void Remove(uint inFlags)
        {
            _data = (_data & ~inFlags);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inFlags"></param>
        /// <returns></returns>
        public bool Has(uint inFlags)
        {
            return (_data & inFlags) == inFlags;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint value { get { return _data; } }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmpty { get { return _data == 0; } }
    };
}
