﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace STZFramework
{
    public class MonoJuggler : STZBehaviour
    {
        public enum EUpdateType
        {
            PREVIOUS,
            NOW,
            POST
        }

        private const string GOName = "MonoJuggler";

        /// <summary>
        /// singleton
        /// </summary>
		private static MonoJuggler _instance;

        /// <summary>
        /// 모노 저글러 생성
        /// *주의: 모노저글러는 생성 후 어플리케이션이 종료될 때까지 파괴시키지 않음을 원칙으로 한다. 한 번 파괴되면 재생성 할 수 없다.
        /// </summary>
        /// <value>It.</value>

        public static MonoJuggler it
        {
            get
            {
                if (_instance == null)
                {
					GameObject go = GameObject.Find(GOName);
					if (go != null)
						_instance = go.GetComponent<MonoJuggler>();
					
					if (_instance == null)
					{
						_instance = CreateInstance();
						DontDestroyOnLoad(_instance.gameObject);
					}
                }

                return _instance;
            }
        }

		void OnDestroy()
		{
			if (_instance != null)
				Destroy(_instance.gameObject);
		}

        /* 대상 객체들 */
        private List<IAnimatable> _objects = new List<IAnimatable>();
        private List<IAnimatable> _prevObjects = new List<IAnimatable>();
        private List<IAnimatable> _postObjects = new List<IAnimatable>();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static MonoJuggler CreateInstance()
        {
            GameObject go = new GameObject();
            go.name = GOName;

            return go.AddComponent<MonoJuggler>();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inObject"></param>
        public void Add(object inObject, EUpdateType type = EUpdateType.NOW)
        {
            if (inObject is IAnimatable)
            {
                switch (type)
                {
                    case EUpdateType.PREVIOUS:
                        if (!_prevObjects.Contains(inObject as IAnimatable))
                            _prevObjects.Add(inObject as IAnimatable);
                        break;

                    case EUpdateType.NOW:
                        if (!_objects.Contains(inObject as IAnimatable))
                            _objects.Add(inObject as IAnimatable);
                        break;

                    case EUpdateType.POST:
                        if (!_postObjects.Contains(inObject as IAnimatable))
                            _postObjects.Add(inObject as IAnimatable);
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inObject"></param>
        public void Remove(object inObject, EUpdateType type = EUpdateType.NOW)
        {
            if (inObject is IAnimatable)
            {
                switch (type)
                {
                    case EUpdateType.PREVIOUS:
                        _prevObjects.Remove(inObject as IAnimatable);
                        break;

                    case EUpdateType.NOW:
                        _objects.Remove(inObject as IAnimatable);
                        break;

                    case EUpdateType.POST:
                        _postObjects.Remove(inObject as IAnimatable);
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inObject"></param>
        /// <returns></returns>
        public bool Contains(object inObject, EUpdateType type = EUpdateType.NOW)
        {
            if (inObject is IAnimatable)
            {
                switch (type)
                {
                    case EUpdateType.PREVIOUS:
                        return _prevObjects.Contains(inObject as IAnimatable);

                    case EUpdateType.NOW:
                        return _objects.Contains(inObject as IAnimatable);

                    case EUpdateType.POST:
                        return _postObjects.Contains(inObject as IAnimatable);
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Count(EUpdateType type = EUpdateType.NOW)
        {
            switch (type)
            {
                case EUpdateType.PREVIOUS:
                    return _prevObjects.Count;

                case EUpdateType.NOW:
                    return _objects.Count;

                case EUpdateType.POST:
                    return _postObjects.Count;
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Update()
        {
            float deltaTime = Time.deltaTime;

            for (int i = 0; i < _prevObjects.Count; ++i)
            {
                _prevObjects[i].AdvanceTime(deltaTime);
            }

            for (int i = 0; i < _objects.Count; ++i)
            {
                _objects[i].AdvanceTime(deltaTime);
            }

            for (int i = 0; i < _postObjects.Count; ++i)
            {
                _postObjects[i].AdvanceTime(deltaTime);
            }
        }
    }
}
