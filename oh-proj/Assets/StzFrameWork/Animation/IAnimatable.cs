﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STZFramework
{
    interface IAnimatable
    {
        void AdvanceTime(float deltaTime);
    }
}
