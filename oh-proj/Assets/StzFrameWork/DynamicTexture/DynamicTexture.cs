﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DynamicTexture 
{
	class TextureNode
	{
		public Rect rect { get; set; }
		public TextureNode leftNode { get; set; }
		public TextureNode rightNode { get; set; }
		public bool filled { get; set; }

		public TextureNode(){}

		public TextureNode(int inWidth, int inHeight)
		{
			rect = new Rect(0, 0, inWidth, inHeight);
		}

		public TextureNode InsertRect(Rect inRect)
		{
			//자식노드가 있따면 자식노드부터 검사.
			//상대적으로 작은 영역 (left) 부터 검사하고 null 이라면 상대적으로 큰 영역 (right)을 다시 검사 
			if(leftNode != null)
			{
				TextureNode texNode = leftNode.InsertRect(inRect);
				if (texNode == null)
				{
					texNode = rightNode.InsertRect(inRect);
				}

				if (texNode != null)
					return texNode;

			}
			//영역이 채워져 있음
			if(filled){
				return null;
			}
			//자신의 영역보다 대상의 영역이 큼
			if(!FitsIn(rect, inRect))
			{
				return null;
			}
			//영역 채워짐
			if(SameSizeAs(rect, inRect))
			{
				filled = true;
				return this;                
			}

			//자식노드 생성
			leftNode = new TextureNode();
			rightNode = new TextureNode();         

			float remainWidth = rect.width - inRect.width;
			float remainHeight = rect.height - inRect.height;       

			//좌우분할
			if(remainWidth > remainHeight)  {
				leftNode.rect = new Rect(rect.x, rect.y, inRect.width, rect.height);
				rightNode.rect = new Rect(rect.x + inRect.width, rect.y, remainWidth, rect.height);
			}
			//상하분할
			else {
				leftNode.rect = new Rect(rect.x, rect.y, rect.width, inRect.height);
				rightNode.rect = new Rect(rect.x, rect.y + inRect.height, rect.width, remainHeight);
			}

			return leftNode.InsertRect(inRect);            
		}

		public bool FitsIn(Rect inRect1, Rect inRect2)
		{
			return inRect1.width >= inRect2.width && inRect1.height >= inRect2.height;
		}
		//자신의 영역과 포함되어야 하는 영역의 크기가 같은지 검사
		public bool SameSizeAs(Rect inRect1, Rect inRect2)
		{
			return inRect1.width == inRect2.width && inRect1.height == inRect2.height;
		}
	}

	TextureNode _textureNode;
	Texture2D _texture;
	Color[] _textureArr;
	Sprite _sheet;
	int _border;
	Dictionary<string, Sprite> _dic = new Dictionary<string, Sprite>();

	public Sprite sheet { get { return _sheet; } }

	public DynamicTexture(int inWidth, int inHeight, int inBorder = 0)
	{
		_border = inBorder;
		_textureNode = new TextureNode(inWidth, inHeight);
		_texture = new Texture2D(inWidth, inHeight, TextureFormat.ARGB32, false);
		_textureArr = _texture.GetPixels();
		_sheet = Sprite.Create(_texture, new Rect(0, 0, _texture.width, _texture.height), new Vector2(0.5f, 0.5f), 1);
	}

	public void SetTexture(Texture2D inTexture)
	{
		if (_dic.ContainsKey(inTexture.name))
		{
			return;
		}

		int texWidth = inTexture.width + _border * 2;
		int texHeight = inTexture.height + _border * 2;

		TextureNode node = _textureNode.InsertRect(new Rect(0, 0, texWidth, texHeight));

		if (node == null)
			return;

		int x = (int)node.rect.x + _border;
		int y = (int)node.rect.y + _border;

		Color[] inTextureArr = inTexture.GetPixels();

		for (int i = 0; i < inTextureArr.Length; i++)
		{
			_textureArr[(i % inTexture.width) + _texture.width * (i / inTexture.width) + x + y * inTexture.width] = inTextureArr[i];
		}

		_texture.SetPixels(_textureArr);
		_texture.Apply();

		Sprite sprite = Sprite.Create(_texture, new Rect(node.rect.x, node.rect.y, node.rect.width, node.rect.height), new Vector2(0.5f, 0.5f), 1);
		sprite.name = inTexture.name;

		_dic.Add(inTexture.name, sprite);
	}

	public Sprite GetSprite(string inTextureName)
	{
		if (_dic.ContainsKey(inTextureName))
			return _dic[inTextureName];

		return null;
	}
}
