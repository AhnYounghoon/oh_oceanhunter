﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;


[CustomEditor(typeof(UIMono),true)]
public class UIMonoEditor : Editor {

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI ();
        UIMono uiMono = (UIMono)target;
        string nameList = null;

        if(uiMono.m_Transform==null)
            uiMono.m_Transform = uiMono.transform as RectTransform;

        if(GUILayout.Button("AsignButtonEvent"))
        {
            nameList = "";
            Button[] childButtons = uiMono.gameObject.GetComponentsInChildren<Button>();
            int idx=0;
            foreach(Button unit in childButtons)
            {
                while(unit.onClick.GetPersistentEventCount()>0){
                    UnityEditor.Events.UnityEventTools.RemovePersistentListener(unit.onClick,0);
                }
                UnityEditor.Events.UnityEventTools.AddIntPersistentListener(unit.onClick,uiMono.OnButtonEvent,idx++);
                Navigation nav = new Navigation();
                nav.mode = Navigation.Mode.None;
                unit.navigation = nav;
                nameList += unit.name + "\n";
            }
        }

        if(GUILayout.Button("AsignInputFieldEvent"))
        {
            nameList = "";
            InputField[] childInputField = uiMono.gameObject.GetComponentsInChildren<InputField>();
             foreach(InputField unit in childInputField)
            {
                while(unit.onEndEdit.GetPersistentEventCount()>0){
                    UnityEditor.Events.UnityEventTools.RemovePersistentListener(unit.onEndEdit,0);
                }
                UnityEditor.Events.UnityEventTools.AddObjectPersistentListener(unit.onEndEdit,uiMono.OnInputFieldEnd,unit);
                
                nameList += unit.name + "\n";
            }
            //StzDebug.Log(nameList);
            uiMono.m_If = uiMono.gameObject.GetComponentsInChildren<InputField>();
        }

        if(GUILayout.Button("AsignToggleEvent"))
        {
            nameList = "";
            Toggle[] childToggle = uiMono.gameObject.GetComponentsInChildren<Toggle>();
            foreach(Toggle unit in childToggle)
            {
                while(unit.onValueChanged.GetPersistentEventCount()>0){
                    UnityEditor.Events.UnityEventTools.RemovePersistentListener(unit.onValueChanged,0);
                }
                UnityEditor.Events.UnityEventTools.AddObjectPersistentListener(unit.onValueChanged,uiMono.OnToggleEvent,unit);
                
                nameList += unit.name + "\n";
            }
            //StzDebug.Log(nameList);
        }
    }
    const string AUTO_BEGIN = "//[Auto generate code begin]";
    const string AUTO_END     = "//[Auto generate code end]";
    [MenuItem("STZ/Creat UIMono Script")]
    static void CreatScript () {
        string reference = FileManager.FileReadAllText(    string.Format("{0}/StzFrameWork/UI/UIMonoReference.txt",Application.dataPath) );
        string fileName = Selection.activeTransform.name.Replace("_",string.Empty);
        string result ="";
        
        string[] imgList     = Selection.activeTransform.GetChildNames("IMG_");
        string[] btnList     = Selection.activeTransform.GetChildNames("BTN_");
        string[] txtList     = Selection.activeTransform.GetChildNames("TXT_");
        string[] rtfList     = Selection.activeTransform.GetChildNames("RT_");
        string[] ifList     = Selection.activeTransform.GetChildNames("IF_");
        
        string imgS = "\n";
        string btnS = "\n";
        string txtS = "\n";
        string rtfS = "\n";
        string ifS    = "\n";        
        
        for(int i=0;i<imgList.Length;i++){    
            imgS += ("\t\t"+imgList[i] + ",\n");
        }
         imgS += "\t\tMax\n";

        for(int i=0;i<btnList.Length;i++){    
            btnS += ("\t\t"+btnList[i] + ",\n");    
        }
         btnS += "\t\tMax\n";
        
        for(int i=0;i<txtList.Length;i++){    
            txtS += ("\t\t"+txtList[i] + ",\n");    
         }
         txtS += "\t\tMax\n";
        
        for(int i=0;i<rtfList.Length;i++){    
            rtfS += ("\t\t"+rtfList[i] + ",\n");    
        }
         rtfS += "\t\tMax\n";

        for(int i=0;i<ifList.Length;i++){    
            ifS += ("\t\t"+ifList[i] + ",\n");    
        }
         ifS += "\t\tMax\n";

        if(reference.LastIndexOf("[ClassName]")!=-1 ){    reference = reference.Replace("[ClassName]"    ,    fileName);    }
        if(reference.LastIndexOf("[IMG_Names]")!=-1 ){    reference = reference.Replace("[IMG_Names]"    ,    imgS    );    }
        if(reference.LastIndexOf("[BTN_Names]")!=-1 ){    reference = reference.Replace("[BTN_Names]"    ,    btnS    );    }
        if(reference.LastIndexOf("[TXT_Names]")!=-1 ){    reference = reference.Replace("[TXT_Names]"    ,    txtS    );    }
        if(reference.LastIndexOf("[RT_Names]")!=-1  ){    reference = reference.Replace("[RT_Names]"    ,    rtfS    );    }
        if(reference.LastIndexOf("[IF_Names]")!=-1  ){    reference = reference.Replace("[IF_Names]"    ,    ifS        );    }
         
        string path = EditorUtility.SaveFilePanel("Save Script", Application.dataPath,fileName, "cs");
        if(FileManager.FileExists(path)){
            string origin = FileManager.FileReadAllText(path);
            int s = origin.LastIndexOf(AUTO_BEGIN);
            int e = origin.LastIndexOf(AUTO_END);
            string f = origin.Substring(0,s);
            string b = origin.Substring(e,origin.Length-e);

            s = reference.LastIndexOf(AUTO_BEGIN);
            e = reference.LastIndexOf(AUTO_END);

            result = f +reference.Substring(s,e-s)+b;
        }else{
            result = reference;
        }
        FileManager.FileWriteAllText( path, result);

        AssetDatabase.Refresh();
//        UIMonoEditorWindow.window = EditorWindow.GetWindowWithRect(typeof(UIMonoEditorWindow),new Rect(0,0,100,100)) as UIMonoEditorWindow;
//        UIMonoEditorWindow.window.m_FileName = fileName;
//        UIMonoEditorWindow.window.Show();  
    }
}
#endif