using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FpsCounter : MonoBehaviour
{
    float deltaTime = 0.0f;
    float min = 60;
    public Text fpsLabel;
    void Start()
    {

    }
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        if(fps<min)
            min = fps;
        fpsLabel.text = string.Format("{0:0.0} ms\n({1:0.} fps)\nmin {2:0.}", msec, fps,min);
    }

    public void ResetMin()
    {
        min = 60;
    }
}