using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIMono : MonoBehaviour {

//[Auto generate code begin]
//[Auto generate code end]
    public Text[]            m_Txt    ;
    public Image[]             m_Img    ;
    public Button[]            m_Btn    ;
    public RectTransform[]    m_Rt    ;
    public InputField[]        m_If    ;

    public RectTransform     m_Transform;

    public virtual void OnButtonEvent(int buttonId)
    {
        // SoundManager.PlaySoundOneshot(ESoundID.common_bt);
     }

    protected void PlayOpenPopup()
    {
        //SoundManager.PlaySoundOneshot (ESoundID.common_popup_open);
    }
    protected void PlayClosePopup()
    {
        //SoundManager.PlaySoundOneshot (ESoundID.common_close_popup);
    }
    protected void PlayOpenPopup_small()
    {
        //SoundManager.PlaySoundOneshot (ESoundID.common_alert_open);
    }




    public virtual void OnInputFieldEnd(InputField inputField)
    {
    }

    public virtual void OnToggleEvent(Toggle toggle)
    {
        //SoundManager.PlaySoundOneshot(ESoundID.common_bt);
    }

    public delegate void Event_UIVisible();
    public void NULL_Event_UIVisible(){    Debug.Log("null????");}
    
    public delegate void Event_UISetWithInt(int v);
    public void NULL_Event_UISetWithInt(int v){}

    public delegate void Event_UISetWithInt2(int v1, int v2);
    public void NULL_Event_UISetWithInt2(int v1, int v2){Debug.Log ("null int2");}

    public void NULL_Event_ButtonClickWithIB(int type, bool bFlag){;}
    public void NULL_Event_ButtonClickWithString(string str){}
    //public void NULL_Event_RewardData(RewardData data){}
    
    
    public delegate void Event_UISetWithBool(bool v);
    public void NULL_Event_UISetWithBool(bool v){}

    public delegate void Event_ButtonClick();
    public delegate void Event_ButtonClickWithBool(bool bFlag);
    public delegate void Event_ButtonClickWithInt(int type);
    public delegate void Event_ButtonClickWithInt2(int type1, int type2);
    public delegate void Event_ButtonClickWithIB(int type, bool bFlag);
    public delegate void Event_ButtonClickWithString(string str);
    public delegate void Event_ButtonClickWithString2(string str,string str2);
    //public delegate void Event_RewardData(RewardData data);
}
