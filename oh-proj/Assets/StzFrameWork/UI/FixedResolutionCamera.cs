﻿using UnityEngine;
using System.Collections;

public class FixedResolutionCamera : MonoBehaviour
{
    [Tooltip("기준 해상도")]
    [SerializeField]
    Vector2 _standardResolution = default(Vector2);

	void Awake ()
    {
        Camera camera = this.Get<Camera>();
        if (camera != null)
        {
            // 현재 해상도를 기준 해상도의 width기반 ratio로 변경한다
            // ex)
            // 기준 해상도 640 x 1136
            // 실제 디바이스 해상도 1200 x 1920 => ratio == 1.6
            // 실제 게임내 해상도 (1200 * 0.5) * 1.6
            float stdHalfWidth =  0.5f * _standardResolution.x;
            camera.orthographicSize = stdHalfWidth / Screen.width * Screen.height;
        }	
	}
}
