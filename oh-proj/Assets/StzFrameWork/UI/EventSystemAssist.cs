﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class EventSystemAssist : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        EventSystem eventSystem = GetComponent<EventSystem>();
        eventSystem.pixelDragThreshold = CalcPixelDragThreshold();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private int CalcPixelDragThreshold()
    {
        const float INCH_TO_CM = 2.54f;
        const float DRAG_THRESHOLD_CM = 0.5f;

        float dragThreshold = DRAG_THRESHOLD_CM * Screen.dpi / INCH_TO_CM;
        Debug.Log("dragThreshold: " + dragThreshold);

        return (int)dragThreshold;
    }
}