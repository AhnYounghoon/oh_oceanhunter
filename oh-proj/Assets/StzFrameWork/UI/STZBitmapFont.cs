﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class STZBitmapFont : STZBehaviour
{
    public enum EBitmapFontType
    {
        SPRITE = 0,
        IMAGE
    }

    [SerializeField]
    List<Sprite> _spriteList = null;

    [SerializeField]
    string _textSpritePath;

    [SerializeField]
    string _prefix;

    [SerializeField]
    string _text = string.Empty;

    [SerializeField]
    string _sortingLayerName = string.Empty;

    [SerializeField]
    int _sortingOrder = 0;

    [SerializeField]
    float _spaceBarSpace = 10;

    [SerializeField]
    float _letterSpacing = 1;

    [SerializeField]
    TextAlignment _alignment = TextAlignment.Right;

    [SerializeField]
    Color _tintColor = Color.white;

    [SerializeField]
    Material _material = null;

    float _leftPosX;
    float _rightPosX;

    [SerializeField]
    float _firstCharPosX;

    const float GEM_ROAD_REWARD_OFFSET = 15.0f;

    protected Dictionary<string, Sprite> _bitmapTextDic = new Dictionary<string, Sprite>();
    protected Stack<GameObject> _characterPool = new Stack<GameObject>();
    protected List<GameObject> _characterList = new List<GameObject>();
    
    public string text { get { return _text; } set { if (_text.Equals(value)) return; _text = value; ProcessText(); } }

    public string prefix { get { return _prefix; } set { if (_prefix.Equals(value)) return; _prefix = value; ProcessText(); } }

    public float spaceBarSpace { get { return _spaceBarSpace; } set { _spaceBarSpace = value; ProcessText(); } }
    
    public float letterSpacing { get { return _letterSpacing; } set { _letterSpacing = value; ProcessText(); } }

    public TextAlignment alignment { get { return _alignment; } set { _alignment = value; ProcessText(); } }
    
    public Color tintColor { get { return _tintColor; } set { _tintColor = value; ProcessText(); } }

    public float leftPosX { get { return transform.TransformPoint(new Vector3(_leftPosX, 0, 0)).x; } }

    public float rightPosX { get { return transform.TransformPoint(new Vector3(_rightPosX, 0, 0)).x; } }

    public float width { get { return Mathf.Abs(rightPosX - leftPosX); } }

    public float startingPositionX
    {
        get
        {
            return _firstCharPosX;
        }
    }

    protected float _textWidth = 0;

    public EBitmapFontType _bitmapFontType;

	void Awake() 
    {
        foreach (Transform tf in transform)
        {
            _characterPool.Push(tf.gameObject);
            tf.gameObject.SetActive(false);
        }

        if (_spriteList.Count > 0)
        {
            for (int i = 0; i < _spriteList.Count; i++)
            {
                if (_spriteList[i] == null)
                    continue;

                _bitmapTextDic.Add(_spriteList[i].name, _spriteList[i]);
            }
        }
        else
        {
            SetTextResources(_textSpritePath);
        }

        ProcessText();
	}

    public void SetTextResources(string resourcesFolderPath)
    {
        _textSpritePath = resourcesFolderPath;
        _bitmapTextDic.Clear();
        Sprite[] sprites = Resources.LoadAll<Sprite>(_textSpritePath);
        for (int i = 0 ; i < sprites.Length ; i++)
        {
            _bitmapTextDic.Add(sprites[i].name, sprites[i]);
        }

        ProcessText();
    }

    System.Text.StringBuilder _strBuilder = new System.Text.StringBuilder();
    protected void ProcessText()
    {
        for (int i = 0 ; i < _characterList.Count ; i++)
        {
            _characterPool.Push(_characterList[i]);
            _characterList[i].SetActive(false);
        }
        _characterList.Clear();

        if (_text.IsNullOrEmpty())
        {
            return;
        }

        float curLength = 0;

        _textWidth = 0;
        foreach (char c in _text)
        {
            GameObject charSprite = null;
            _strBuilder.Length = 0;
            _strBuilder.Append(_prefix);
            _strBuilder.Append(c);
            if (_bitmapTextDic.ContainsKey(_strBuilder.ToString()))
            {
                if (_characterPool.Count == 0)
                {
                    charSprite = new GameObject();

                    switch(_bitmapFontType)
                    {
                        case EBitmapFontType.SPRITE:
                            charSprite.AddComponent<SpriteRenderer>();
                            break;

                        case EBitmapFontType.IMAGE:
                            charSprite.AddComponent<Image>();
                            break;
                    }
                    

                    charSprite.transform.SetParent(transform, false);
                }
                else
                {
                    charSprite = _characterPool.Pop();
                    charSprite.SetActive(true);
                }

                Sprite s = _bitmapTextDic[_strBuilder.ToString()];
                charSprite.name = _strBuilder.ToString();

                switch (_bitmapFontType)
                {
                    case EBitmapFontType.SPRITE:
                        {
                            var spriteRenderer = charSprite.GetComponent<SpriteRenderer>();
                            spriteRenderer.sprite = s;
                            spriteRenderer.color = tintColor;
                            spriteRenderer.sortingLayerName = _sortingLayerName;
                            spriteRenderer.sortingOrder = _sortingOrder;
                            if (_material != null)
                                spriteRenderer.material = _material;
                            break;
                        }

                    case EBitmapFontType.IMAGE:
                        {
                            var image = charSprite.GetComponent<Image>();
                            image.sprite = s;
                            image.color = tintColor;
                            charSprite.GetComponent<RectTransform>().sizeDelta = s.bounds.size;

                            if (_material != null)
                                image.material = _material;
                            break;
                        }
                        
                }
                

                Vector3 v = Vector3.zero;
                //v.x = s.bounds.size.x * 0.5f + _textWidth + (textCount == 0 ? 0 : _letterSpacing);
                v.x = s.bounds.size.x * 0.5f + _textWidth + _letterSpacing;
                charSprite.transform.localPosition = v;

                _characterList.Add(charSprite);

                curLength = s.bounds.size.x;
            }
            else
            {
                curLength = _spaceBarSpace;
            }

            //_textWidth += curLength + (textCount == 0 ? 0 : _letterSpacing);
            _textWidth += curLength + _letterSpacing;
        }

        ProcessAlignment();
    }

    protected void ProcessAlignment()
    {
        _leftPosX = 0;
        _rightPosX = 0;

        float space = 0;
        switch (_alignment)
        {
        case TextAlignment.Right:
            space = -_textWidth;
            break;

        case TextAlignment.Center:
            space = -_textWidth * 0.5f;
            break;

        case TextAlignment.Left:
            space = 0;
            break;

        default:
            break;
        }

        for (int i = 0 ; i < _characterList.Count ; i++)
        {
            Vector3 v = _characterList[i].transform.localPosition;
            v.x += space;
            _characterList[i].transform.localPosition = v;

            if (i == 0)
                _firstCharPosX = _characterList[i].transform.position.x - GEM_ROAD_REWARD_OFFSET;

            switch (_bitmapFontType)
            {
                case EBitmapFontType.IMAGE:
                    if (i == 0)
                    {
                        _leftPosX = _characterList[i].transform.localPosition.x - _characterList[i].GetComponent<RectTransform>().sizeDelta.x * 0.5f;
                    }

                    if (i == Mathf.Max(_characterList.Count - 1, 0))
                    {
                        _rightPosX = _characterList[i].transform.localPosition.x + _characterList[i].GetComponent<RectTransform>().sizeDelta.x * 0.5f;
                    }
                    break;

                case EBitmapFontType.SPRITE:
                    if (i == 0)
                    {
                        _leftPosX = _characterList[i].transform.localPosition.x - _characterList[i].GetComponent<SpriteRenderer>().sprite.rect.size.x * 0.5f;
                    }

                    if (i == Mathf.Max(_characterList.Count - 1, 0))
                    {
                        _rightPosX = _characterList[i].transform.localPosition.x + _characterList[i].GetComponent<SpriteRenderer>().sprite.rect.size.x * 0.5f;
                    }
                    break;
            }
        }
    }

#if UNITY_EDITOR
    [Header(" -- 에디터용 --")]
    [SerializeField]
    string _textTexturePath = null;

    [ContextMenu("Import Text Texture")]
    public void ImportTextTexture()
    {
        _spriteList.Clear();
        Object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(_textTexturePath);
        foreach (var sprite in sprites)
        {
            if (sprite.GetType() == typeof(Sprite)
                && sprite.name.Contains(_prefix))
            {
                _spriteList.Add((Sprite)sprite);
            }
        }
    }

    [ContextMenu("Generate")]
    public void UpdateEditor()
    {
        if (Application.isPlaying)
            return;

        while (transform.childCount > 0)
        {
            foreach (Transform tf in transform)
            {
                DestroyImmediate(tf.gameObject);
            }
        }

        _bitmapTextDic.Clear();
        _characterPool.Clear();
        _characterList.Clear();

        if (_spriteList.Count > 0)
        {
            for (int i = 0; i < _spriteList.Count; i++)
            {
                _bitmapTextDic.Add(_spriteList[i].name, _spriteList[i]);
                ProcessText();
            }
        }
        else
        {
            SetTextResources(_textSpritePath);
        }

        text = _text;
        ProcessText();
    }
#endif
}
