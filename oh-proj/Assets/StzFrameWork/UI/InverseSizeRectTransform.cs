﻿using UnityEngine;
using System.Collections;

public class InverseSizeRectTransform : MonoBehaviour {
    [Tooltip("기준 해상도")]
    [SerializeField]
    float _height = 0;

    // Use this for initialization
    void Awake ()
    {
        RectTransform rt = this.Get<RectTransform>();
        if (rt == null)
        {
            return;
        }

        rt.localScale = new Vector3(_height / (Camera.main.orthographicSize * 2f), _height / (Camera.main.orthographicSize * 2f), 1);
    }
}
