﻿using UnityEngine;
using System.Collections;

using STZFramework;

public class FixedResolutionRectTransform : MonoBehaviour
{
    public enum EFixedAxis
    {
        X,
        Y
    }

    [Tooltip("기준 축")]
    [SerializeField]
    EFixedAxis _axis = EFixedAxis.X;    

    [Tooltip("기준 해상도")]
    [SerializeField]
    Vector2 _standardResolution = default(Vector2);

    void Awake()
    {
        RectTransform rt = this.Get<RectTransform>();
        if (rt == null)
        {
            return;
        }

        switch (_axis)
        {
            case EFixedAxis.X:
                rt.sizeDelta = new Vector2(_standardResolution.x, (0.5f * _standardResolution.x) / Screen.width * Screen.height * 2);
                
                break;

            case EFixedAxis.Y:
                // NOTE @sangmoon Y축을 기준으로 UI 사이즈를 정할때에는 FixedResolutionCameara의 속성을 가지고 있는 Camera를 기준으로 scale해야한다 
                // 그러한 카메라가 없을때는 경고 메시지를 띄우고 main Camera를 사용한다 
                Camera[] cameras = Camera.allCameras;
                Camera resolutionCamera = null;
                foreach (Camera camera in cameras)
                {
                    var temp = camera.GetComponent<FixedResolutionCamera>();

                    bool bFound = temp != null;
                    if (bFound)
                    {
                        resolutionCamera = camera;
                        break;
                    }
                }

                if (resolutionCamera == null)
                {
                    Debug.LogWarning("[FIXED_RESOLUTION_RECTTRANSFORM] can't find resolution Camera");
                    resolutionCamera = Camera.main;
                    return;
                }

                Debug.Assert(resolutionCamera != null);

                rt.localScale = new Vector3((resolutionCamera.orthographicSize * 2f) / _standardResolution.y, (resolutionCamera.orthographicSize * 2f) / _standardResolution.y, 1);
                break;
        }
    }
}
