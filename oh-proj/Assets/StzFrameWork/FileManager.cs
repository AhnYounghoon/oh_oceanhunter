﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;


public class FileManager{
    
    static public bool DirectoryExists(string path){
        
        #if !UNITY_WEBPLAYER         
        return Directory.Exists(path);
        #else
        return false;
        #endif        
    }
    
    static public void DirectoryCreateDirectory(string path){
        #if !UNITY_WEBPLAYER        
        Directory.CreateDirectory(path);
        #endif
    }
    
    static public void FileWriteAllBytes(string path, byte[] contents){
        #if !UNITY_WEBPLAYER        
        File.WriteAllBytes( path, contents );
        #endif
    }    
    
    static public bool FileExists(string path){
        #if !UNITY_WEBPLAYER        
        return File.Exists(path);
        #else        
        return false;
        #endif
    }        
    
    static public void FileWriteAllText(string path, string contents){
        #if !UNITY_WEBPLAYER        
        File.WriteAllText(path,contents);    
        #endif
    }

    static public string FileReadAllText(string path){
        #if !UNITY_WEBPLAYER        
        return File.ReadAllText(path);    
        #endif
    }
    
    static public string[] FileReadAllLine(string path){
        #if !UNITY_WEBPLAYER        
        return File.ReadAllLines(path);    
        #endif
    }
    
    static public byte[] FileReadAllBytes(string path){
        #if !UNITY_WEBPLAYER        
        return File.ReadAllBytes( path );
        #else
        return null;
        #endif        
    }                
    
    static public byte[] FileOpenRead(string path){
        byte[] buffer = null;
        #if !UNITY_WEBPLAYER
        FileStream fileStream = File.Open( path, FileMode.Open );
        buffer = new byte[fileStream.Length];
        fileStream.Read(buffer, 0, buffer.Length);    
        fileStream.Close();
        #endif        
        return buffer;
        
    }    
    static public BinaryWriter GetBinaryWriter(string path){
        BinaryWriter result = null;
#if !UNITY_WEBPLAYER        
        result = new BinaryWriter( File.Open( path, FileMode.Create), Encoding.UTF8 );
#endif        
        return result;        
    }
    
    static public BinaryReader GetBinaryReader(string path){
        BinaryReader result = null;
#if !UNITY_WEBPLAYER
        result = new BinaryReader( File.Open(path, FileMode.Open) );
#endif
        return result;
    }
    
    static public TextWriter GetStreamWriter(string path){
        TextWriter result = null;
#if !UNITY_WEBPLAYER            
        result = new StreamWriter( File.Open( path, FileMode.Create), Encoding.UTF8 );
#endif
        return result;
    }    
    
    static public TextReader GetStreamReader(string path){
        TextReader result = null;
#if !UNITY_WEBPLAYER            
        result = new StreamReader( File.Open(path, FileMode.Open) );
#endif
        return result;
    }    

    static public void WriteBinaryByteArray(string path, ArrayList byteArray_List){
        #if !UNITY_WEBPLAYER        
        BinaryWriter BW = FileManager.GetBinaryWriter(path);
        foreach(byte[] unit in byteArray_List){
            BW.Write( unit );
        }
        BW.Close();
        #endif
    }
    
    static public void WriteBinaryByteArray(string path, byte[] byteArray ){
        #if !UNITY_WEBPLAYER        
        BinaryWriter BW = FileManager.GetBinaryWriter(path);
        BW.Write( byteArray );
        BW.Close();
        #endif
    }    
    
    static public void WriteTextObject(string path, object obj){
        #if !UNITY_WEBPLAYER        
        TextWriter textWriter = FileManager.GetStreamWriter( path );
        textWriter.Write( obj );
        textWriter.Close();
        #endif
    }
    
    static public void DeleteFile(string path){
        #if !UNITY_WEBPLAYER        
        if( FileExists(path) )
            File.Delete(path);
        #endif
    }

    static public void Copy(string pathA, string pathB)
    {
        #if !UNITY_WEBPLAYER        
        if( FileExists(pathB) )
            File.Delete(pathB);
        File.Copy(pathA,pathB);
        #endif
    }
}

//Application.OpenURL("mailto:yesyoucan@hotmail.com?subject=Email&body=from Unity");
