﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// URL thumbnail loader.
/// </summary>
public class UrlThumbnailLoader
{
    /// <summary>
    /// E texture size.
    /// </summary>
    public enum ETextureSize
    {
        _2 = 2,
        _4 = 4,
        _8 = 8,
        _16 = 16,
        _32 = 32,
        _64 = 64,
        _128 = 128,
        _256 = 256,
        _512 = 512,
        _1024 = 1024,
        _2048 = 2048
    }

    /// <summary>
    /// URL tumbnail texture.
    /// </summary>
    public class UrlThumbnailTexture
    {
        /// <summary>
        /// I vector2.
        /// </summary>
        struct IVector2
        {
            public int x;
            public int y;

            /// <summary>
            /// Initializes a new instance of the <see cref="UrlThumbnailLoader+UrlTumbnailTexture+IVector2"/> struct.
            /// </summary>
            /// <param name="inX">In x.</param>
            /// <param name="inY">In y.</param>
            public IVector2(int inX, int inY)
            {
                x = inX;
                y = inY;
            }

            /// <summary>
            /// Returns a <see cref="System.String"/> that represents the current <see cref="UrlThumbnailLoader+UrlTumbnailTexture+IVector2"/>.
            /// </summary>
            /// <returns>A <see cref="System.String"/> that represents the current <see cref="UrlThumbnailLoader+UrlTumbnailTexture+IVector2"/>.</returns>
            public override string ToString()
            {
                return (string.Format("({0},{1})", x, y));
            }
        };

        /**/
        Texture2D _texture;
        Sprite _sprite;

        /**/
        int _maxThumbnailWidth;
        /**/
        int _maxThumbnailHeight;

        /**/
        int _maxCount;

        /**/
        int _widthCount;
        /**/
        int _heightCount;

        int _border;

        /**/
        IVector2[] _indexPositions;

        /**/
        Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();
        /**/
        Dictionary<Sprite, int> _spriteIndexes = new Dictionary<Sprite, int>();
        /**/
        Dictionary<Sprite, int> _spriteRefCount = new Dictionary<Sprite, int>();

        /**/
        Color[] _textureArr;


        /// <summary>
        /// 
        /// </summary>
        public bool isFull { get { return _maxCount == _sprites.Count; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlThumbnailLoader+UrlTumbnailTexture"/> class.
        /// </summary>
        /// <param name="inSize">In size.</param>
        /// <param name="inThumbnailWidth">In thumbnail width.</param>
        /// <param name="inThumbnailHeight">In thumbnail height.</param>
        /// <param name="inBorder">In border.</param>
        public UrlThumbnailTexture(ETextureSize inSize, int inMaxThumbnailWidth = 110, int inMaxThumbnailHeight = 110, int inBorder = 1)
        {
            _border = inBorder;

            int size = (int)inSize;
            _texture = new Texture2D(size, size, TextureFormat.RGBA32, false);

            _textureArr = new Color[size * size];

            _maxThumbnailWidth = inMaxThumbnailWidth;
            _maxThumbnailHeight = inMaxThumbnailHeight;

            _widthCount = size / (inMaxThumbnailWidth + inBorder * 2);
            _heightCount = size / (inMaxThumbnailHeight + inBorder * 2);

            _maxCount = _widthCount * _heightCount;

            _indexPositions = new IVector2[_maxCount];
            for (int i = 0; i < _maxCount; i++)
            {
                int row = i / _widthCount * (_maxThumbnailHeight + inBorder * 2);
                int col = i % _widthCount * (_maxThumbnailWidth + inBorder * 2);
                _indexPositions[i] = new IVector2(col, row);
            }

            _sprite = Sprite.Create(_texture, new Rect(0, 0, _texture.width, _texture.height), new Vector2(0.5f, 0.5f), 1);

            SingletonController.Get<CoroutineManager>().StartCoroutine(Update());
        }

        /// <summary>
        /// Set the specified inKey and inTexture.
        /// </summary>
        /// <param name="inKey">In key.</param>
        /// <param name="inTexture">In texture.</param>
		public void Set(string inKey, Texture2D inTexture, Texture2D inMask, bool inApplyTexture = true)
        {
            if (_maxThumbnailWidth < inTexture.width || _maxThumbnailHeight < inTexture.height)
            {
                Debug.Log("텍스쳐의 크기가 너무 큽니다.");
                return;
            }

            int curIndex = 0;
            if (_sprites.ContainsKey(inKey))
            {
                curIndex = _spriteIndexes[_sprites[inKey]];
            }
            else
            {
                if (_sprites.Count == _maxCount)
                {
                    Debug.LogError("텍스쳐 저장 한도를 초과하였습니다.");
                    return;
                }


                for (int i = 0; i < _maxCount; i++)
                {
                    if (!_spriteIndexes.ContainsValue(i))
                    {
                        curIndex = i;
                        break;
                    }
                }
            }

            Color[] textureColors = inTexture.GetPixels();
            if (inMask != null)
            {
                if (inMask.width != inTexture.width || inMask.height != inTexture.height)
                {
                    inMask.Resize(inTexture.width, inTexture.height);
                }

                Color[] maskColors = inMask.GetPixels();
                for (int i = 0; i < maskColors.Length; i++)
                {
                    textureColors[i].a *= maskColors[i].a;
                }
            }

            IVector2 curPosition = _indexPositions[curIndex];
            int startIdx = (curPosition.x + _border) + (curPosition.y + _border) * _texture.width;
            for (int i = 0; i < textureColors.Length; i++)
            {
                _textureArr[startIdx + _texture.width * (i / inTexture.width) + (i % inTexture.width)] = textureColors[i];
            }

            Sprite sprite = Sprite.Create(_texture, new Rect(curPosition.x, curPosition.y, _maxThumbnailWidth, _maxThumbnailHeight), new Vector2(0.5f, 0.5f), 1);
            sprite.name = inKey;

            if (_sprites.ContainsKey(inKey))
                _sprites[inKey] = sprite;
            else
                _sprites.Add(inKey, sprite);

            _spriteIndexes.Remove(sprite);
            _spriteIndexes.Add(sprite, curIndex);

            _spriteRefCount.Remove(sprite);
            _spriteRefCount.Add(sprite, 0);

            _dirty = true;
        }

        bool _dirty;
        public void ApplyTexture()
        {
            _texture.SetPixels(_textureArr);
            _texture.Apply();
        }

        IEnumerator Update()
        {
            while (true)
            {
                if (_dirty)
                {
                    ApplyTexture();
                    _dirty = false;
                }
                yield return new WaitForSeconds(1);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inKey"></param>
        /// <returns></returns>
        public Sprite GetSprite(string inKey)
        {
            if (!_sprites.ContainsKey(inKey))
                return null;

            Sprite sprite = _sprites[inKey];
            if (_spriteRefCount.ContainsKey(sprite))
            {
                _spriteRefCount[sprite]++;
            }
            else
            {
                _spriteRefCount[sprite] = 1;
            }

            return sprite;
        }

        public Sprite cachingSprite { get { return _sprite; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inSprite"></param>
        public bool Release(Sprite inSprite)
        {
            if (_spriteRefCount.ContainsKey(inSprite))
            {
                _spriteRefCount[inSprite] = Mathf.Max(0, _spriteRefCount[inSprite] - 1);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inkey"></param>
        public bool Release(string inkey)
        {
            if (_sprites.ContainsKey(inkey))
            {
                _spriteRefCount[_sprites[inkey]] = Mathf.Max(0, _spriteRefCount[_sprites[inkey]] - 1);
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearUnusingSprites()
        {
            List<Sprite> removingSprites = new List<Sprite>();
            List<string> spriteKeys = new List<string>();
            foreach (var keyValue in _sprites)
            {
                if (_spriteRefCount[keyValue.Value] > 0)
                {
                    removingSprites.Add(keyValue.Value);
                    spriteKeys.Add(keyValue.Key);
                }
            }

            for (int i = 0; i < removingSprites.Count; i++)
            {
                IVector2 curPosition = _indexPositions[_spriteIndexes[removingSprites[i]]];
                int startIdx = (curPosition.x + _border) + (curPosition.y + _border) * _texture.width;
                for (int j = 0; j < _maxThumbnailWidth * _maxThumbnailHeight; j++)
                {
                    int idx = startIdx + _texture.width * (i / _maxThumbnailWidth) + (i % _maxThumbnailHeight);
                    _textureArr[idx].r = 0;
                    _textureArr[idx].g = 0;
                    _textureArr[idx].b = 0;
                    _textureArr[idx].a = 0;
                }

                _spriteRefCount.Remove(removingSprites[i]);
                _spriteIndexes.Remove(removingSprites[i]);
                _sprites.Remove(spriteKeys[i]);
            }

            _texture.SetPixels(_textureArr);
        }

        /// <summary>
        /// Containses the key.
        /// </summary>
        /// <returns><c>true</c>, if key was containsed, <c>false</c> otherwise.</returns>
        /// <param name="key">Key.</param>
        public bool ContainsKey(string key)
        {
            return _sprites.ContainsKey(key);
        }
    }

    /// <summary>
    /// URL thmbnail info.
    /// </summary>
    public class UrlThumbnailInfo
    {
        /// <summary>
        /// The base texture count.
        /// </summary>
        public int cachingTextureCount = 1;

        /// <summary>
        /// The width of the thumbnail.
        /// </summary>
        public int thumbnailWidth = 110;

        /// <summary>
        /// The height of the thumbnail.
        /// </summary>
        public int thumbnailHeight = 110;

        /// <summary>
        /// The size of the texture.
        /// </summary>
        public ETextureSize textureSize = ETextureSize._512;

        /// <summary>
        /// The border.
        /// </summary>
        public int border = 1;

        /// <summary>
        /// The time out sec.
        /// </summary>
        public float timeOutSec = 10;

        /// <summary>
        /// The maxfile count.
        /// </summary>
        public int maxfileCount = 100;

        /// <summary>
        /// The storage hours.
        /// </summary>
        public int storageHours = 1;
    };

    /// <summary>
    /// URL request info.
    /// </summary>
    class UrlRequestInfo
    {
        /**/
        string _url;
        /**/
        Callback _callback;
        /**/
        Texture2D _mask;

        /// <summary>
        /// Gets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string url { get { return _url; } }

        /// <summary>
        /// Gets the callback.
        /// </summary>
        /// <value>The callback.</value>
        public Callback callback { get { return _callback; } }

        /// <summary>
        /// Gets the mask.
        /// </summary>
        /// <value>The mask.</value>
        public Texture2D mask { get { return _mask; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlThumbnailLoader+UrlRequestInfo"/> class.
        /// </summary>
        /// <param name="inCallback">In callback.</param>
        /// <param name="inUrl">In URL.</param>
        public UrlRequestInfo(Callback inCallback, string inUrl, Texture2D inMask)
        {
            _url = inUrl;
            _callback = inCallback;
            _mask = inMask;
        }
    }

    /// <summary>
    /// Thumbnail data base.
    /// </summary>
    [System.Serializable()]
    class ThumbnailDataBase
    {
        /// <summary>
        /// Thumbnail data.
        /// </summary>
        [System.Serializable()]
        class ThumbnailData
        {
            /**/
            string _expiredTime;
            /**/
            string _filePath;
            /**/
            int _width;
            /**/
            int _height;

            /// <summary>
            /// Gets the expired time.
            /// </summary>
            /// <value>The expired time.</value>
            public string expiredTime { get { return _expiredTime; } }

            /// <summary>
            /// Gets the file path.
            /// </summary>
            /// <value>The file path.</value>
            public string filePath { get { return _filePath; } }

            /// <summary>
            /// Gets the width.
            /// </summary>
            /// <value>The width.</value>
            public int width { get { return _width; } }

            /// <summary>
            /// Gets the height.
            /// </summary>
            /// <value>The height.</value>
            public int height { get { return _height; } }

            [System.NonSerialized()]
            Texture2D _texture;

            public Texture2D texture
            {
                get
                {
                    if (_texture == null && _textureData != null)
                    {
                        _texture = new Texture2D(_width, _height);
                        _texture.LoadImage(_textureData);
                    }

                    return _texture;
                }

            }

            protected byte[] _textureData;

            /// <summary>
            /// Initializes a new instance of the <see cref="UrlThumbnailLoader+ThumbnailDataBase+ThumbnailData"/> class.
            /// </summary>
            /// <param name="inExpiredTime">In expired time.</param>
            /// <param name="inFilePath">In file path.</param>
            /// <param name="inWidth">In width.</param>
            /// <param name="inHeight">In height.</param>
            public ThumbnailData(string inExpiredTime, string inFilePath, int inWidth, int inHeight, byte[] inTexArr)
            {
                _expiredTime = inExpiredTime;
                _filePath = inFilePath;
                _width = inWidth;
                _height = inHeight;
                _textureData = inTexArr;
            }

            ~ThumbnailData()
            {
                if (texture != null)
                    MonoBehaviour.Destroy(texture);
            }
        }
        /// <summary>
        /// Gets or sets the max data count.
        /// </summary>
        /// <value>The max data count.</value>
        public int maxDataCount { get; set; }

        /// <summary>
        /// Gets or sets the storage hours.
        /// </summary>
        /// <value>The storage hours.</value>
        public int storageHours { get; set; }

        List<ThumbnailData> _thumbnailData = new List<ThumbnailData>();

        System.Text.StringBuilder _strBuilder = new System.Text.StringBuilder();

        /// <summary>
        /// Gets the full path.
        /// </summary>
        /// <returns>The full path.</returns>
        /// <param name="inUrl">In URL.</param>
        string GetFullPath(string inUrl)
        {
            _strBuilder.Length = 0;
            _strBuilder.Append(Application.temporaryCachePath);
            _strBuilder.Append("/");
            _strBuilder.Append(inUrl.MD5());
#if UNITY_EDITOR
            _strBuilder.Append(".png");
#else
			_strBuilder.Append(".dat");
#endif
            return _strBuilder.ToString();
        }

        /// <summary>
        /// Saves the texture as file.
        /// </summary>	
        /// <param name="inUrl">In URL.</param>
        /// <param name="inTexture">In texture.</param>
        public void SaveTextureAsFile(string inUrl, Texture2D inTexture)
        {
            string fullPath = GetFullPath(inUrl);

            ThumbnailData data = _thumbnailData.Find(x => x.filePath == fullPath);
            if (data != null)
            {
                _thumbnailData.Remove(data);
            }

            DestroyOldestData(_thumbnailData.Count - maxDataCount + 1);

            _thumbnailData.Add(new ThumbnailData(System.DateTime.UtcNow.AddHours(storageHours).ToString(), fullPath, inTexture.width, inTexture.height, inTexture.EncodeToPNG()));
        }

        /// <summary>
        /// Loads the texture from file.
        /// </summary>
        /// <returns>The texture from file.</returns>
        /// <param name="inUrl">In URL.</param>
        public Texture2D LoadTextureFromFile(string inKey)
        {
            string fullPath = GetFullPath(inKey);
            ThumbnailData data = _thumbnailData.Find(x => x.filePath == fullPath);
            if (data != null)
            {
                return data.texture;
            }

            return null;
        }

        /// <summary>
        /// Destroies the expired data.
        /// </summary>
        public void DestroyExpiredData()
        {
            System.DateTime curTime = System.DateTime.UtcNow;

            for (int i = 0; i < _thumbnailData.Count;)
            {
                ThumbnailData data = _thumbnailData[i];
                System.DateTime expiredTime = System.DateTime.Parse(data.expiredTime);
                if (expiredTime < curTime)
                {
                    _thumbnailData.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// Destroies the overflow data.
        /// </summary>
        public void DestroyOverflowData()
        {
            if (_thumbnailData.Count > maxDataCount)
            {
                DestroyOldestData(_thumbnailData.Count - maxDataCount);
            }
        }

        /// <summary>
        /// Destroies the oldest data.
        /// </summary>
        /// <param name="inDataCount">In data count.</param>
        public void DestroyOldestData(int inDataCount)
        {
            for (int i = 0; i < inDataCount; i++)
            {
                _thumbnailData.RemoveAt(0);
            }
        }
    }

    static UrlThumbnailLoader _instance;
    public static UrlThumbnailLoader it { get { if (_instance == null) _instance = new UrlThumbnailLoader(); return _instance; } }

    public delegate void Callback(string inUrl, Sprite inSprite);
    bool _initialized;
    UrlThumbnailInfo _info;
    WWW _www;
    UrlThumbnailTexture _cachingTexture;
    Queue<UrlRequestInfo> _urlRequestQueue = new Queue<UrlRequestInfo>();

    ThumbnailDataBase _database;

    string databaseFilePath { get { return Application.temporaryCachePath + "/" + ("thumbnailData").MD5() + ".dat"; } }
    string databaseSecretKey = "thumbnailData";

    /// <summary>
    /// Loads the database.
    /// </summary>
    /// <param name="inInfo">In info.</param>
    void LoadDatabase(UrlThumbnailInfo inInfo)
    {
        _database = FileIOExtension.LoadFromFile<ThumbnailDataBase>(databaseFilePath, databaseSecretKey);
        if (_database == null)
        {
            _database = new ThumbnailDataBase();
        }

        _database.maxDataCount = inInfo.maxfileCount;
        _database.storageHours = inInfo.storageHours;
    }

    /// <summary>
    /// Saves the database.
    /// </summary>
    public void SaveDatabase()
    {
        if (_database == null)
            return;

        _database.DestroyExpiredData();
        _database.DestroyOverflowData();

        FileIOExtension.SaveAsFile(_database, databaseFilePath, databaseSecretKey);
    }

    /// <summary>
    /// Init the specified inInfo.
    /// </summary>
    /// <param name="inInfo">In info.</param>
    public void Init(UrlThumbnailInfo inInfo)
    {
        _initialized = true;

        _info = inInfo;

        _cachingTexture = new UrlThumbnailTexture(inInfo.textureSize, inInfo.thumbnailWidth, inInfo.thumbnailHeight, inInfo.border);

        LoadDatabase(inInfo);
        SaveDatabase();
    }

    /// <summary>
    /// Gets the sprite.
    /// </summary>
    /// <param name="inCallback">In callback.</param>
    /// <param name="inUrl">In URL.</param>
    public void GetSprite(Callback inCallback, string inUrl, Texture2D inMask)
    {
        if (!_initialized)
        {
            return;
        }

        string texKey = GetTexKey(inUrl, inMask);

        Sprite sprite = GetSprite(texKey, inMask);

        /* 텍스쳐에 저장된 이미지가 있으면 그것을 이용한다. */
        if (sprite != null)
        {
            inCallback(inUrl, sprite);
            return;
        }

        Texture2D tex = _database.LoadTextureFromFile(inUrl);
        if (tex != null)
        {
            SetTexture(texKey, tex, inMask);
            inCallback(inUrl, GetSprite(texKey, inMask));
            return;
        }

        /* 텍스쳐에 저장된 이미지가 없으면 웹에서 다운로드하기 위해 예약해 둔다. */
        _urlRequestQueue.Enqueue(new UrlRequestInfo(inCallback, inUrl, inMask));
        DownloadNext();
    }

    /// <summary>
    /// Downloads the next.
    /// </summary>
    void DownloadNext()
    {
        if (_www != null
           || _urlRequestQueue == null
           || _urlRequestQueue.Count == 0)
        {
            return;
        }
        SingletonController.Get<CoroutineManager>().StartCoroutine(this.Co_NextDownload(_urlRequestQueue.Dequeue()));
    }

    /// <summary>
    /// 웹에서 이미지 불러오기.
    /// </summary>
    /// <param name="url">이미지가 있는 웹주소</param>
    /// <returns></returns>
    IEnumerator Co_NextDownload(UrlRequestInfo info)
    {
        string texKey = GetTexKey(info.url, info.mask);

        string path = info.url;
        float timer = 0;
        bool isTimeout = false;

        _www = new WWW(info.url);
        while (!_www.isDone)
        {
            timer += Time.deltaTime;
            if (timer > _info.timeOutSec)
            {
                isTimeout = true;
                break;
            }
            yield return null;
        }

        bool isSuccess = false;
        Texture2D tex = null;
        if (!isTimeout
               && _www.isDone
               && _www.error == null)
        {
            tex = _www.texture;
            isSuccess = _info.thumbnailWidth >= tex.width && _info.thumbnailHeight >= tex.height;
        }

        if (tex != null && isSuccess)
        {
            _database.SaveTextureAsFile(info.url, tex);
            SaveDatabase();
            SetTexture(texKey, tex, info.mask);
            info.callback(info.url, GetSprite(texKey, info.mask));
        }
        else
        {
            info.callback(info.url, null);
        }

        if (tex != null)
        {
            MonoBehaviour.DestroyImmediate(tex);
        }

        _www = null;

        DownloadNext();
    }

    /// <summary>
    /// Sets the texture.
    /// </summary>
    /// <param name="inUrl">In URL.</param>
    /// <param name="inTexture">In texture.</param>
    public void SetTexture(string inKey, Texture2D inTexture, Texture2D inMask)
    {
        if (_cachingTexture.ContainsKey(inKey))
        {
            _cachingTexture.Set(inKey, inTexture, inMask);
            return;
        }

        /* 빈 텍스쳐를 찾아서 기록 */
        if (!_cachingTexture.isFull)
        {
            _cachingTexture.Set(inKey, inTexture, inMask);
            return;
        }

        /* 빈 텍스쳐가 없다면 사용하지 않는 텍스쳐 제거 후 기록 */
        _cachingTexture.ClearUnusingSprites();
        if (!_cachingTexture.isFull)
        {
            _cachingTexture.Set(inKey, inTexture, inMask);
            return;
        }

        Debug.Log("텍스쳐 저장 한도를 초과하여 이미지를 더이상 저장할 수 없습니다.");
    }

    /**/
    System.Text.StringBuilder _keyStr = new System.Text.StringBuilder();

    /// <summary>
    /// Gets the tex key.
    /// </summary>
    /// <returns>The tex key.</returns>
    /// <param name="inUrl">In URL.</param>
    /// <param name="inMask">In mask.</param>
    public string GetTexKey(string inUrl, Texture2D inMask)
    {
        _keyStr.Length = 0;
        _keyStr.Append(inUrl);
        if (inMask != null)
            _keyStr.Append(inMask.name);
        return _keyStr.ToString().MD5();
    }

    /// <summary>
    /// Gets the sprite.
    /// </summary>
    /// <returns>The sprite.</returns>
    /// <param name="inUrl">In URL.</param>
    Sprite GetSprite(string inKey, Texture2D inMask)
    {
        if (_cachingTexture.ContainsKey(inKey))
        {
            return _cachingTexture.GetSprite(inKey);
        }

        return null;
    }

    /// <summary>
    /// Release the specified inUrl.
    /// </summary>
    /// <param name="inUrl">In URL.</param>
    public void Release(string inKey, Texture2D inMask)
    {
        if (!_initialized)
        {
            return;
        }

        bool b = _cachingTexture.Release(inKey);

        if (b)
            return;
    }

    /// <summary>
    /// Release the specified inSprite.
    /// </summary>
    /// <param name="inSprite">In sprite.</param>
    public void Release(Sprite inSprite)
    {
        if (!_initialized)
        {
            return;
        }

        bool b = _cachingTexture.Release(inSprite);

        if (b)
            return;
    }

    public Sprite cachingSprite { get { return _cachingTexture.cachingSprite; } }
}
