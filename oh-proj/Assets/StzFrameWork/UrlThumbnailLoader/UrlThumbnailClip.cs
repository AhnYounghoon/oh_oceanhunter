﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class UrlThumbnailClip : MonoBehaviour
{
    public enum EType
    {
        IMAGE,
        SPRITE_RENDERER
    }

    [SerializeField]
    EType _type = EType.IMAGE;

    [SerializeField]
    Sprite _mask = null;

    [SerializeField]
    Sprite _defaultSprite = null;

    void Update()
    {
        UpdateInEditor();
    }

    void Awake()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        targetSprite = _defaultSprite;
    }

    public Sprite targetSprite
    {
        set
        {
            switch (_type)
            {
                case EType.IMAGE:
                    GetComponent<Image>().sprite = value;
                    break;

                case EType.SPRITE_RENDERER:
                    GetComponent<SpriteRenderer>().sprite = value;
                    break;
            }
        }
        get
        {
            switch (_type)
            {
                case EType.IMAGE:
                    return GetComponent<Image>().sprite;

                case EType.SPRITE_RENDERER:
                    return GetComponent<SpriteRenderer>().sprite;
            }

            return null;
        }
    }

    public void SetSprite(string inUrl)
    {
        UrlThumbnailLoader.it.Release(targetSprite);
        UrlThumbnailLoader.it.GetSprite((url, sprite) =>
        {
            if (sprite == null)
            {
                targetSprite = _defaultSprite;
            }
            else
            {
                targetSprite = sprite;
            }
        }, inUrl, maskTexture);
    }

    void OnDestroy()
    {
        UrlThumbnailLoader.it.Release(targetSprite);
    }

    Texture2D maskTexture
    {
        get { return _mask == null ? null : _mask.texture; }
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void UpdateInEditor()
    {
        if (Application.isPlaying)
            return;

        targetSprite = _defaultSprite;
    }
}
