﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;

public class UIMonoEditorWindow : EditorWindow {

     public static UIMonoEditorWindow window;
    public string m_FileName = null;
    void Update(){
        if(m_FileName==null)    return;
        if(EditorApplication.isCompiling)    return;
        if(m_FileName.Length < 2 ) return;
        if(Selection.activeTransform==null)return;
        Component c = UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(Selection.activeTransform.gameObject, "Assets/StzFrameWork/UIMonoEditorWindow.cs (15,17)", m_FileName);
        if(c==null) return;
        c.SendMessage("AssignObj");
        m_FileName = null;
        this.Close();
    }
}
#endif