﻿using System;
using System.Text;

namespace STZFramework
{
    public class SNSEvent
    {
        /** 메세지 등의 처리가 완료됨 */
        public const string COMPLETE                = "sns_complete";

        /** 로그인 성공 */
        public const string LOGIN                   = "sns_login";

        /** 연령인증 성공 */
        public const string COMPLETED_AGE_AUTH      = "sns_completed_age_auth";

        /** 로그아웃 */
        public const string LOGOUT                  = "sns_logout";
        
        /** 탈퇴됨 */
        public const string UNREGISTERED            = "sns_unregistered";
        
        /** 게스트 로그인 */
        public const string GUEST_LOGIN             = "sns_guest_login";

        /** 오류 */
        public const string ERROR                   = "sns_error";
    };
}
