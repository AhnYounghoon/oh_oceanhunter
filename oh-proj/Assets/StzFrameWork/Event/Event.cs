﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STZFramework
{
    public class EventParameters : EventArgs
    {
        /* 전달 데이터 */
        public object[] data;

        /* 즉시 해당 이벤트 처리를 중단 */
        public bool stopImmediatePropagation = false;

        public EventParameters(params object[] inData)
        {
            data = inData;
        }
    };
}
