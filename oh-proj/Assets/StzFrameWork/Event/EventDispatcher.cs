﻿using System;
using System.Collections.Generic;
using System.Text;

namespace STZFramework
{
    public class EventDispatcher
    {
        public delegate void STZEventHandler(object sender, EventParameters args);

        private Dictionary<string, List<STZEventHandler>> _handlers;

        public EventDispatcher()
        {
            _handlers = new Dictionary<string, List<STZEventHandler>>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inType"></param>
        /// <param name="inHandler">void func(object sender, EventParameters args)</param>
        public void AddEventListener(object inType, STZEventHandler inHandler)
        {
            string type = (inType is string ? (string)inType : inType.ToString());
            List<STZEventHandler> handlers = null;

            if(_handlers.ContainsKey(type))
            {
                handlers = _handlers[type];
            }
            else
            {
                handlers = new List<STZEventHandler>();
                _handlers[type] = handlers;
            }

            // 동일한 주소를 가진 콜백의 중복을 허용하지 않음 
            if(!handlers.Contains(inHandler))
            {
                handlers.Add(inHandler);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inType"></param>
        /// <param name="inHandler"></param>
        public void RemoveEventListener(object inType, STZEventHandler inHandler)
        {
            string type = (inType is string ? (string)inType : inType.ToString());

            if(!_handlers.ContainsKey(type))
            {
                return;
            }

            List<STZEventHandler> handlers = _handlers[type];

            if(handlers.Contains(inHandler))
            {
                handlers.Remove(inHandler);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void RemoveEventListeners()
        {
            foreach(string key in _handlers.Keys)
            {
                _handlers[key].Clear();
            }

            _handlers.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inType"></param>
        /// <param name="inEventArgs"></param>
        public void DispatchEvent(object inType, EventParameters inEventArgs = null)
        {
            string type = (inType is string ? (string)inType : inType.ToString());

            if(!_handlers.ContainsKey(type))
            {
                return;
            }

            List<STZEventHandler> handlers = _handlers[type];

            for(int i = 0; i < handlers.Count; ++i)
            {
                handlers[i](this, inEventArgs);

				if(inEventArgs != null && inEventArgs.stopImmediatePropagation)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inType"></param>
        /// <returns></returns>
        public bool HasEventListener(object inType)
        {
            string type = (inType is string ? (string)inType : inType.ToString());
            return _handlers.ContainsKey(type);
        }
    };
}
