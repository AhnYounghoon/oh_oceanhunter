using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace STZFramework{

	public class UrlAssetLoader
	{
		public delegate void CompleteCallback(string url,string path);

		private string _relPath;
 		private string _currentRequestUrl;
		private CompleteCallback _completeCallback;
		private Queue<string> _urlRequestStack;
		private MonoBehaviour	_mono;

		public UrlAssetLoader(string inRelPath,MonoBehaviour inMono,CompleteCallback inCompleteCallback)
		{
			_currentRequestUrl = null;
			_urlRequestStack = new Queue<string>();

			_completeCallback = inCompleteCallback;
			_relPath = inRelPath;
			_mono = inMono;
 		}
		
		public void RegistDownloadUrl(string inUrl)
		{
			if(	_urlRequestStack.Contains(inUrl) )
				return;

			_urlRequestStack.Enqueue(inUrl);
 		}

		public void StartDownload()
		{
			if(_mono!=null
			   && _urlRequestStack.Count > 0)
				SingletonController.Get<CoroutineManager>().StartCoroutine(Co_NextDownload());
			else{
				if(_completeCallback!=null)
					_completeCallback(null,null);
				else
					Debug.LogError("[UrlAssetLoader] not asigned completeCallback.");
			}
		}

		private IEnumerator Co_NextDownload()
		{

			string url 	= _currentRequestUrl = _urlRequestStack.Dequeue();
			string[] split = _currentRequestUrl.Split('/');
			string path = string.Format("{0}/{1}",_relPath,split[split.Length-1]);
			WWW www = new WWW(url);
			yield return www;

			if(www.error==null
			   && www.texture !=null){
 				File.WriteAllBytes(path,www.texture.EncodeToPNG());
				UnityEngine.GameObject.Destroy(www.texture);
			}else{
				path = null;
			}

			_currentRequestUrl = null;

			if(_completeCallback!=null)
				_completeCallback(url,path);
		}

		public void Dispose()
		{
			_urlRequestStack.Clear();
			_urlRequestStack = null;
		}
	}
}