using UnityEngine;
using System.Collections;
using SimpleJSON;

namespace STZFramework.STZ_UrlImageLoader{

	public class UrlAssetModelUrlCacheInfo
	{
		public string url;
		public string accessTime;
	}
}