﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using STZFramework;
using System;

/// <summary>
/// UrlImageLoader에서 텍스쳐를 가지고 와서 사용할 시 사용을 완료한 텍스쳐를 로더로 돌려줘야 한다.
/// 본 클래스는 위 내용을 자동화하기 위해 제작한 클래스이며, 
/// Image나 SpriteRenderer 컴포넌트와 함께 추가하여 사용한다.
/// </summary>

[RequireComponent(typeof(SpriteRenderer))]
public class UrlImageSprite : BaseUrlImage 
{
    SpriteRenderer _image;

    /// <summary>
    /// 초기화
    /// </summary>
    protected override void Init()
    {
        if (defaultSprite == null)
        {
            _image = gameObject.GetComponent<SpriteRenderer>();
            if (_image != null)
                defaultSprite = _image.sprite;
        }
    }

    /// <summary>
    /// sprite를 Image 혹은 SpriteRenderer와 연결.
    /// </summary>
    /// <param name="sprite">연결할 스프라이트.</param>
    protected override void LinkSprite(Sprite sprite)
    {
        if (_image != null)
        {
            _image.sprite = sprite;
        }
    }

    /// <summary>
    /// Image나 Sprite Renderer가 현재 가지고 있는 이미지를 UrlImageLoader로 반환한다.
    /// </summary>

    protected override void ClearTexture()
    {
        bool loadedSprite = _image != null && _image.sprite && _bDownloaded;
        if (loadedSprite)
        {
            UrlImageLoader.it.ReturnToPool(_image.sprite.name);
            _image.sprite = defaultSprite;
        }
    }

    protected override void SetImageName(string inURL)
    {
        if (_image.sprite != null)
            _image.sprite.name = inURL;
    }

    /// <summary>
    /// Reset this instance.
    /// </summary>
    /// 
    public override void Reset()
    {
        if (_image != null)
        {
            _image.sprite = defaultSprite;
        }
    }
}
