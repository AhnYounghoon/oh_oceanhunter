using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

namespace STZFramework.STZ_UrlImageLoader{
	
	public class UrlImageDataBase{
		
		const string URL_ASSET_DATAS 		= "URL_ASSET_DATAS";
 
		/**	version histroy
		 * <1.0>
		 * CAHS_INFO
		 * 	|-	_url:String;
		 * 	|-	_accessTime:Number;
		 * 
		 * RESOURCE_INFO
		 * 	|-	_url:String;
		 * 	|-	_filePath:String;		
 		 * 	|-	_created:Number;
		 */
 		
		double 	_expiredTime = 0;
		int 	_cacheCapacity = 0;
		string SAVE_PATH;
		
		List<UrlAssetModelUrlCacheInfo> _cacheList;
		Dictionary<string,UrlAssetModelUrlCacheInfo> _cacheDic;
		Dictionary<string,UrlAssetModelResourceInfo> _resourceDic;
		
		
		public UrlImageDataBase(	string 	inSavePath,
		                        	double	inExpiredTime,
		                        	int		inCacheCapacity)
		{

			SAVE_PATH 		= string.Format("{0}/{1}", inSavePath, URL_ASSET_DATAS.MD5() );
 			_expiredTime 	= inExpiredTime;
			_cacheCapacity 	= inCacheCapacity;

			Init();
		}

		void Init()
		{
			_resourceDic 	= new Dictionary<string,UrlAssetModelResourceInfo>();
			_cacheDic	 	= new Dictionary<string,UrlAssetModelUrlCacheInfo>();
			
			LoadFromFile();
			ComposeCacheList();
		}

        public void Save()
        {
            ComposeCacheList();
            SaveAsFile();
        }

        void LoadFromFile()
		{
			if(!File.Exists(SAVE_PATH))
				return;

			Parse(	JSONClass.LoadFromFile(SAVE_PATH) as JSONClass );
		}

		public bool Parse( JSONClass jsonObject )
		{
 			JSONArray list = jsonObject["resourceDic"].AsArray;
			UrlAssetModelResourceInfo resourceInfo;
			for(int resourceInfoIndex = 0; resourceInfoIndex < list.Count; resourceInfoIndex++)
			{
				resourceInfo = new UrlAssetModelResourceInfo();
				list[resourceInfoIndex].AsObject.DecodeTo<UrlAssetModelResourceInfo>(ref resourceInfo);
				_resourceDic[resourceInfo.url] = resourceInfo;
			}
			
			list = jsonObject["cacheList"].AsArray;
			UrlAssetModelUrlCacheInfo cacheInfo;
			for(int cacheInfoIndex = 0; cacheInfoIndex < list.Count; cacheInfoIndex++)
			{ 
				cacheInfo = new UrlAssetModelUrlCacheInfo();
				list[cacheInfoIndex].AsObject.DecodeTo<UrlAssetModelUrlCacheInfo>(ref cacheInfo);
				_cacheDic.Add(cacheInfo.url,cacheInfo);
			}
			
			return true;
		}


		
		public void RegistResource(	string	url,
									string fileName,
		                           	double timeStamp)
		{
 			UrlAssetModelResourceInfo o = new UrlAssetModelResourceInfo(){
				url = url, fileName = fileName, created = timeStamp.ToString()
			};

            bool bNeedUpdate = false;

            if (!_resourceDic.ContainsKey(url))
            {
                bNeedUpdate = true;
                _resourceDic.Add(url, o);
            }
            else
                _resourceDic[url] = o;

			UrlAssetModelUrlCacheInfo c = new UrlAssetModelUrlCacheInfo(){
				url = url, accessTime = timeStamp.ToString()
			};

            if (!_cacheDic.ContainsKey(url))
            {
                bNeedUpdate = true;
                _cacheDic.Add(url, c);
            }
            else
                _cacheDic[url] = c;

            if (bNeedUpdate)
            {
                ComposeCacheList();
                //SaveAsFile(); // 종료시 Save에서 한번만 호출한다.
            }
		}
		
		public string GetResourceFileName(string url, double today)
		{
			string resultPath = null;
			if(_resourceDic!=null
			   && _resourceDic.ContainsKey(url)){
				UrlAssetModelResourceInfo info = _resourceDic[url] as UrlAssetModelResourceInfo;
				if( info != null
					&& !info.IsExpired(today,_expiredTime) )
					resultPath = info.fileName;
			}
 
			UrlAssetModelUrlCacheInfo c = new UrlAssetModelUrlCacheInfo(){
				url = url, accessTime = today.ToString()
			};

			if(_cacheDic.ContainsKey(url))
				_cacheDic[url] = c;
			else
				_cacheDic.Add(url,c);
			ComposeCacheList();
			return resultPath;
		}		

		public void DeleteResource(List<string> urlList)
		{
			foreach(string url in urlList){
				if( _cacheDic.ContainsKey(url) )
					_cacheDic.Remove(url);
				if( _resourceDic.ContainsKey(url) )
					_resourceDic.Remove(url);
			}
			ComposeCacheList();
            //SaveAsFile(); // 종료시 Save에서 한번만 호출한다.
        }

        public List<string> GetWillDestroyList(double timeNow)
		{
			List<string> resultUrl = new List<string>();
			AddExpiredFilePathListBy(ref resultUrl,timeNow);
			AddOverflowFilePathListBy(ref resultUrl);
			return resultUrl;
		}

		void AddExpiredFilePathListBy(ref List<string> outUrlList,double timeNow)
		{
			foreach(string url in _resourceDic.Keys){
				if( _resourceDic[url].IsExpired(timeNow,_expiredTime))
					outUrlList.Add(_resourceDic[url].url);
			}
 		}
		

		void AddOverflowFilePathListBy(ref List<string> outUrlList)
		{
			UrlAssetModelUrlCacheInfo unit;
			UrlAssetModelResourceInfo resource;

            _cacheList.Sort(CompareCacheList);

            for (int i=_cacheCapacity;i<_cacheList.Count;i++){
				unit = _cacheList[i];
				if(_resourceDic.ContainsKey(unit.url)){
					resource = _resourceDic[unit.url] as UrlAssetModelResourceInfo;
					if(resource == null)
						continue;
					outUrlList.Add(resource.url);
				}
			}
 		}

		void ComposeCacheList()
		{
			if(_cacheList!=null)
				_cacheList.Clear();
			_cacheList = new List<UrlAssetModelUrlCacheInfo>();
 			foreach(string url in _cacheDic.Keys){
				_cacheList.Add(_cacheDic[url]);
			}
 		}

		int CompareCacheList(UrlAssetModelUrlCacheInfo l, UrlAssetModelUrlCacheInfo r)
		{
            //if( l.accessTime.DoubleValue() > r.accessTime.DoubleValue() )
            //	return -1;
            //else if( l.accessTime.DoubleValue() < r.accessTime.DoubleValue() )
            //	return 1;
            //return 0;

            // 단순히 시간의 크기를 비교하는 부분이므로 단순하게 비교해도 된다.
            return r.accessTime.CompareTo(l.accessTime);
		}

		void SaveAsFile()
		{
 			ToJsonClass().SaveToFile(SAVE_PATH);
		}

		public JSONClass ToJsonClass(uint version=1)
		{
			JSONClass jsonObject = new JSONClass();

			JSONArray list = new JSONArray();
			foreach(string url in _resourceDic.Keys){
				list.Add(_resourceDic[url].EncodeToJsonClass() );
			}
			jsonObject["resourceDic"] = list;

			list = new JSONArray();
			for(int i=0;i<_cacheList.Count;i++){
				list.Add(_cacheList[i].EncodeToJsonClass());
 			}
			jsonObject["cacheList"] = list;
			return jsonObject;
		}
	}
}