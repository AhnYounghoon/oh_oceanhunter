using UnityEngine;
using System.Collections;
using SimpleJSON;

namespace STZFramework.STZ_UrlImageLoader{

	public class UrlAssetModelResourceInfo
	{
		public string url;
		public string fileName;
		public string created;

		public bool IsExpired(double today,double limitTime)
		{	
 			return (created.DoubleValue() + limitTime < today);
		}
	}
}
