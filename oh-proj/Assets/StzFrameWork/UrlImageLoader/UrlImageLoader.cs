/**
 * @file    urlImageLoader.as
 * @author  donggun.kim@sundaytoz.com
 * 
 * 이미지를 로드할 URL과 콜벡받을 Function을 인자로 받는다. 
 * 로컬에 저장된 이미지가 있으면 콜벡 Function을 호출한다. 인자로 URL, Texture2D를 넘겨준다.
 * 로컬에 저장된 이미지가 없으면 해당하는 URL의 이미지를 로컬에 다운로드한다.
 * path는 url file name 을 사용하도록 한다.
 * 다운로드한 url,path,created를 저장한다.
 * 로컬에 저장된 이미지의 유효기간이 지났으면 로컬에 저장된 이미지를 삭제한다.
 * 로컬에 캐싱한 이미지의 개수가 일정 개수를 넘으면 가장 오래동안 사용되지 않았던 이미지부터 삭제한다.
 * 로컬에 저장된 이미지를 리턴해줄때 마지막 접근 날짜를 기록한다.
 */
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using STZFramework.STZ_UrlImageLoader;

namespace STZFramework{

	public class UrlImageLoader {

        /* 싱글톤 관련 */
		static UrlImageLoader _instance;
        public static bool hasInstance { get { return _instance != null; } }

		static public UrlImageLoader it
		{	
			get
			{
				if(_instance == null){
                    _instance = new UrlImageLoader();
				}

				return _instance;
			}
		}

        /* 메모리에 캐시되어 있는 텍스쳐 */
		Dictionary<string,Texture2D> _memoryCachePool;

        /* 메모리에 캐시되어 있는 텍스쳐의 레퍼런스 카운트 */
		Dictionary<string,int> _memoryCachePoolRefCnt;

        /* 메무리에 캐시되는 텍스쳐의 수 */
 		int _memoryCacheCapacity;

        /* 불러올 수 없거나 존재하지 않는 텍스쳐 정보 */
        Dictionary<string, System.DateTime> _missingCachePool;

        /* 불러올 수 없거나 존재하지 않는 텍스쳐를 다시 한 번 업데이트 하는 시간 */
        float _missingCacheUpdateMilliSec;

        /* 불러온 텍스쳐 정보를 기록하는 저장소 */
		UrlImageDataBase _db;

        /* 초기화를 진행한 시간 */
 		double _timeNow;

        /* WWW를 통해 텍스쳐 호출 후 불러올 콜백 */
		public delegate void CompleteCallback(string url,Texture2D tex);

        /* 콜백을 받을 클래스의 유효성을 판단하기 위한 클래스 */
        public class CompleteCallbackSet
        {
            public object           target;
            public CompleteCallback callback;
        }

        /* 요청된 콜백의 리스트 */
        Dictionary<string, List<CompleteCallbackSet>> _listenerList;

        /* 실질적으로 데이터를 저장할 path */
        string _relPath;

        /* WWW에서 이미지를 받아오는 것이 늦어질 경우 대기하는 최대 시간(sec) */
		int _timeoutSec = 15;

        /* URL Image Loader의 초기화 여부 */
        bool _isInitialized = false;

        /// <summary>
        /// 꼭 한번만 실행할 것( 싱글턴 객체 )
        /// </summary>
        /// <param name="localSavePath">기본이 되는 저장 경로 Application.temporaryCachePath </param>
        /// <param name="localCacheExpiredTime_sec"> 이미지의 유효 시간 밀리세컨 (1주일) 7*24*60*60*1000 = 604800000 </param>
        /// <param name="missingCacheUpdateMilliSec">불러오지 못한(혹은 존재하지 않는) 이미지를 캐싱하고 있을 시간(캐싱중에는 다시 불러오기를 시도하지 않음)(밀리세컨)</param>
        /// <param name="memoryCacheCapacity">메모리 캐싱할 최대 이미지 개수(50개)</param>
        /// <param name="imageCacheCapacity">로컬에 저장할 최대 이미지 개수(300개)</param>

        public void Init(string  localSavePath              = null,
			             double  localCacheExpiredTime_sec  = 604800000,
                         float   missingCacheUpdateMilliSec = 6000000,
                         int     memoryCacheCapacity		= 50,
			             int     imageCacheCapacity 		= 300)
		{
            /* 초기화는 앱 실행 후 최초 한 번만 하도록 강제함 */
            if (_isInitialized)
                return;

			if (_db != null)
				return;

            _isInitialized = true;

            _listenerList = new Dictionary<string, List<CompleteCallbackSet>>();

            _missingCacheUpdateMilliSec = missingCacheUpdateMilliSec;

            _relPath = (localSavePath == null) ? Application.temporaryCachePath : localSavePath;

			_memoryCacheCapacity = memoryCacheCapacity;

			System.TimeSpan span = (System.DateTime.UtcNow - new System.DateTime(1970, 1, 1, 0, 0, 0, 0,System.DateTimeKind.Utc));
			_timeNow = span.TotalMilliseconds;

 			_db = new UrlImageDataBase(	_relPath, localCacheExpiredTime_sec, imageCacheCapacity);

			_memoryCachePool 		= new Dictionary<string,Texture2D>();
			_memoryCachePoolRefCnt 	= new Dictionary<string, int>();
            _missingCachePool       = new Dictionary<string, System.DateTime>();

            CleanResource();

            SingletonController.Get<CoroutineManager>().StartCoroutine(CheckMissingCachePool());
        }

        /// <summary>
        /// 앱 종료시 호출되는 함수.
        /// </summary>
        void OnApplicationQuit()
        {
            _db.Save(); // 종료시 한번 호출해준다.
        }

        /**
		 * 이미지를 로드할 URL과 콜벡받을 Function을 인자로 받는다.
		 * @param url : "http://www.image.com/profile.png"
		 * @param callback : function(path:String):void{}
		 * 
		 */

        /// <summary>
        /// 이미지를 불러오는 함수, 콜벡에서 url, texture null 검증을 할것.
        /// 호출하자마자 일단 defualt로 지정된 이미지를 리턴한다.
        /// 사용이 끝난 텍스쳐는 ReturnToPool을 호출이 필요함. ( 미 호출시 메모리가 계속 증가하므로 주의!!!)
        /// 1. 웹으로 부터 불러오지 못했거나 없는 이미지가 캐싱된 정보,
        /// 2. 메모리 캐싱된 이미지,
        /// 3. 로컬에 저장된 이미지
        /// 4. url download
        /// 순으로 진행
        /// </summary>
        /// <param name="inUrl">호출할 url</param>
        /// <param name="inCallback">호출한 url과 결과 텍스쳐를 인자로 받는 함수.</param>
        public void GetImage(string inUrl, GameObject inTarget, CompleteCallback inCallback)
		{
            /* 초기화가 이루어지지 않았다면 해당 함수를 실행하지 않는다. */
            if (!_isInitialized)
            {
                Debug.Assert(_isInitialized, "The UrlImageLoader is not initialzed!");
                return;
            }

			if(string.IsNullOrEmpty(inUrl))
            {
 				return;
			}

			RegistListener(inUrl, inTarget, inCallback);

            /* 1. 웹으로 부터 불러오지 못했거나 없는 이미지가 캐싱된 정보 체크. */
            if (_missingCachePool.ContainsKey(inUrl))
            {
                if (inCallback != null)
                    inCallback(inUrl, null);
                return;
            }

			/*	2. 메모리상에 해당 텍스쳐가 있는지 체크. */
			Texture2D result = GetTextureFromPool(inUrl);
			if(result != null)
            {
				ReturnImagePath(inUrl, GetFileNameFromUrl(inUrl), result);
 				return;
			}

			/*	3. 저장소에 해당 텍스쳐가 있는지 체크. */
			string fileName = _db.GetResourceFileName(inUrl, _timeNow);
			if (!string.IsNullOrEmpty(fileName))
            {
				result = GetTextureFromStorage(inUrl, GetPath(fileName));
                if (result != null)
                {
                    ReturnImagePath(inUrl, fileName, result);
                    return;
                }
            }

			/*	4. 웹으로부터 다운로드. */
            if (NetworkHelper.IsOnline())
            {
                RegistDownloadUrl(inUrl);
                DownloadNext();
                Debug.LogWarning("Download " + inUrl);
            }
            else
            {
                if (inCallback != null)
                    inCallback(inUrl, null);
            }
		}

		/// <summary>
		/// 리소스의 저장 시간이 지났거나, 최대 캐싱 허용치를 넘은 리소스들을 삭제한다. 오래걸리는 함수이므로 사용에 주의할것.
		/// </summary>
		public void CleanResource()
		{
            /* 초기화가 이루어지지 않았다면 해당 함수를 실행하지 않는다. */
            if (!_isInitialized)
            {
                Debug.Assert(false, "The UrlImageLoader is not initialzed!");
                return;
            }

            List<string> list = _db.GetWillDestroyList(_timeNow);
			string path = null;
			foreach (string url in list) 
			{
				path = GetPathFromUrl(url);
				if (File.Exists(path))
					File.Delete(path);
			}
			_db.DeleteResource(list);
		}	

		/// <summary>
		/// 사용이 끝난 텍스쳐는 이 함수를 불러줄 것.
		/// 레퍼런스 카운팅을 통하여 메모리 캐싱을 관리함.
		/// 레퍼런스 카운터가 0이되어도 바로 메모리 해제는 하지 않는다.
		/// ( 0이 되어도 곧 사용할 가능성은 있음 )
		/// 메모리 캐싱 허용치가 넘었을때 레퍼런스 카운터가 0은 Texture를 Destroy한다.
		/// 레퍼런스 카운터가 0이 아닌 텍스쳐들이 쌓이면 메모리가 계속 증가하므로 주의!!!
		/// </summary>
		/// <param name="inUrl">In URL.</param>
		public void ReturnToPool(string inUrl)
		{
            /* 초기화가 이루어지지 않았다면 해당 함수를 실행하지 않는다. */
            if (!_isInitialized)
            {
                Debug.Assert(false, "The UrlImageLoader is not initialzed!");
                return;
            }

            if ( string.IsNullOrEmpty(inUrl))
            {
				return;
			}

			if(!_memoryCachePoolRefCnt.ContainsKey(inUrl)
			   || !_memoryCachePool.ContainsKey(inUrl)){
				Debug.LogWarning("[UrlImageLoader] not contained " + inUrl);
				return;
			}

			_memoryCachePoolRefCnt[inUrl]--;

			if( _memoryCachePoolRefCnt[inUrl] < 1)
            {
				_memoryCachePoolRefCnt[inUrl] = 0;
			}

			CheckCachePool();
		}

        /// <summary>
        /// 
        /// 
        /// 
        /// </summary>
        /// <param name="inUrl"></param>

		void DestroyTexture(string inUrl)
		{
			_memoryCachePoolRefCnt.Remove(inUrl);

			if (!_memoryCachePool.ContainsKey(inUrl))
				return;

			if (_memoryCachePool[inUrl]!=null)
				MonoBehaviour.Destroy(_memoryCachePool[inUrl]);

			_memoryCachePool.Remove(inUrl);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUrl"></param>
        /// <param name="inTex"></param>

		void RegistTextureToPool(string inUrl,Texture2D inTex)
		{
 			if (!_memoryCachePool.ContainsKey(inUrl))
				_memoryCachePool.Add(inUrl,inTex);

			if (!_memoryCachePoolRefCnt.ContainsKey(inUrl))
				_memoryCachePoolRefCnt.Add(inUrl,0);
 		}

        /// <summary>
        /// 
        /// </summary>

		void CheckCachePool()
		{
			if(_memoryCachePool.Count <= _memoryCacheCapacity)
				return;
			List<string> keys = new List<string>(_memoryCachePoolRefCnt.Keys);
			foreach(string key in keys){
				if(_memoryCachePoolRefCnt[key]>0)
					continue;
				DestroyTexture(key);
			}
			keys.Clear();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUrl"></param>
        /// <returns></returns>

		Texture2D GetTextureFromPool(string inUrl)
		{
			if (!_memoryCachePool.ContainsKey(inUrl))
				return null;

			if(_memoryCachePool[inUrl] == null)
            {
 				_memoryCachePool.Remove(inUrl);
				_memoryCachePoolRefCnt.Remove(inUrl);
				return null;
			}

			_memoryCachePoolRefCnt[inUrl]++;
			return _memoryCachePool[inUrl];
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUrl"></param>
        /// <param name="path"></param>
        /// <returns></returns>

		Texture2D GetTextureFromStorage(string inUrl,string path)
		{
			if (! File.Exists(path))
            {
 				return null;
			}

            Texture2D result = new Texture2D(1,1,TextureFormat.ARGB32,false,true);
			result.LoadImage(File.ReadAllBytes(path));
 			RegistTextureToPool(inUrl,result);
			var texture = GetTextureFromPool(inUrl);

            return texture;
		}			

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUrl"></param>
        /// <param name="inTarget"></param>
        /// <param name="inCallback"></param>

		void RegistListener(string inUrl, object inTarget, CompleteCallback inCallback)
		{
			if(!_listenerList.ContainsKey(inUrl))
            {
				_listenerList[inUrl] = new List<CompleteCallbackSet>();
			}
            (_listenerList[inUrl] as List<CompleteCallbackSet>).Add(new CompleteCallbackSet() { target = inTarget, callback = inCallback });
		}

 		Queue<string> _urlRequestQueue = new Queue<string>();
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inUrl"></param>

		void RegistDownloadUrl(string inUrl)
		{
			if(_urlRequestQueue.Contains(inUrl))
				return;
			_urlRequestQueue.Enqueue(inUrl);
		}

		WWW _www = null;
		void DownloadNext()
		{
			if( _www!=null 
			   || _urlRequestQueue ==null
			   || _urlRequestQueue.Count==0)
            {
                _db.Save();
                return;
            }


            SingletonController.Get<CoroutineManager>().StartCoroutine(this.Co_NextDownload(_urlRequestQueue.Dequeue()));
		}

        /// <summary>
        /// 웹에서 이미지 불러오기.
        /// </summary>
        /// <param name="url">이미지가 있는 웹주소</param>
        /// <returns></returns>
		IEnumerator Co_NextDownload(string url)
		{
 			string path = GetPathFromUrl(url);
			float timer = 0;
			bool isTimeout = false;

            _www = new WWW(url);
            while (!_www.isDone)
            {
                timer += Time.deltaTime;
                if (timer > _timeoutSec)
                {
                    isTimeout = true;
                    break;
                }
                yield return null;
            }

            Texture2D tex = null;
            if (!isTimeout
                   && _www.isDone
                   && _www.error == null)
            {
                tex = _www.texture;
                File.WriteAllBytes(path, tex.EncodeToPNG());
            }
            else
            {
                path = null;
            }

            if (tex != null && path != null)
            {
                RegistTextureToPool(url, tex);
                ReturnImagePath(url, GetFileNameFromUrl(url), GetTextureFromPool(url));
            }

            if (tex == null)
            {
                // TODO
                //if (_missingCachePool.ContainsKey(url))
                //{
                //    _missingCachePool[url] = ServerModel.Instance.localServerTime;
                //}
                //else
                //{
                //    _missingCachePool.Add(url, ServerModel.Instance.localServerTime);
                //}
            }

            _www = null;
			
			DownloadNext();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fn"></param>
        /// <param name="tex"></param>
		void ReturnImagePath(string url,string fn,Texture2D tex)
		{
			string path = GetPath(fn);
			if (path != null)
            {
				_db.RegistResource(url,fn,_timeNow);
			}
			
			if (!_listenerList.ContainsKey(url))
            {
				ReturnToPool(url);
				return;
			}
			List<CompleteCallbackSet> callbackList = _listenerList[url] as List<CompleteCallbackSet>; 
			if (callbackList == null)
            {
				ReturnToPool(url);
				return;
			}

 			foreach (CompleteCallbackSet f in callbackList)
            {
				if (f==null || f.target == null || f.callback == null)
                {
					continue;
				}
				f.callback(url,tex);
			}
			_listenerList[url].Clear();
			_listenerList.Remove(url);
		}

        /// <summary>
        /// 데이터가 저장된 path 가져오기.
        /// </summary>
        /// <param name="fn"></param>
        /// <returns></returns>
		string GetPath(string fn)
		{
			return string.Format("{0}/{1}",_relPath,fn);
		}

        /// <summary>
        /// Url Path 가져오기
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
		string GetPathFromUrl(string url)
		{
			return GetPath(GetFileNameFromUrl(url));
		}

        /// <summary>
        /// 파일 이름 가져오기
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
		string GetFileNameFromUrl(string url)
		{
			string[] split = url.Split('/');

            if (split.Length > 2)
            {
#if UNITY_EDITOR
                return split[split.Length - 2] + split[split.Length - 1] + ".png";
#else
 			    return split [split.Length - 2] + split [split.Length - 1] + ".bytes";
#endif
            }
            else
            {
#if UNITY_EDITOR
                return split[split.Length - 1] + ".png";
#else
 				return split[split.Length-1] + ".bytes";
#endif
            }
        }

        /// <summary>
        /// 웹에 없는 이미지일 경우에 이미지 없음으로 처리된 것을 
        /// 일정 시간이 지난 후에 다시 웹에서 재호출할 수 있도록 해주는 함수.
        /// </summary>
        /// <returns></returns>
        IEnumerator CheckMissingCachePool()
        {
            List<string> keyList = new List<string>();

            while(true)
            {
                yield return new WaitForSeconds(_missingCacheUpdateMilliSec);
                keyList.Clear();
                // TODO
                //System.DateTime time = ServerModel.Instance.localServerTime;
                //foreach (var missing in _missingCachePool)
                //{
                //    if (missing.Value.AddMilliseconds(_missingCacheUpdateMilliSec) < time)
                //    {
                //        keyList.Add(missing.Key);
                //    }
                //}

                for (int i = 0; i < keyList.Count; i++)
                {
                    _missingCachePool.Remove(keyList[i]);
                }
            }
        }
    }
}
