﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using STZFramework;

/// <summary>
/// UrlImageLoader에서 텍스쳐를 가지고 와서 사용할 시 사용을 완료한 텍스쳐를 로더로 돌려줘야 한다.
/// 본 클래스는 위 내용을 자동화하기 위해 제작한 클래스이며, 
/// Image나 SpriteRenderer 컴포넌트와 함께 추가하여 사용한다.
/// </summary>

public abstract class BaseUrlImage : STZBehaviour 
{
    public  Sprite mask;                  // 가져온 텍스쳐를 마스킹 하고 싶을 때 텍스쳐 사이즈와 같은 이미지 추가.
    public  System.Action<bool> finishedCallback;   // 완료시 성공 실패 여부를 호출한다. (실패시 기본 이미지 표시 등의 작업을 위해)
	public  Sprite defaultSprite;

    Texture2D _linkedTexture = null;
    protected bool _bDownloaded = false;
    private string _url;

    void Awake()
    {
        // SpriteRenderer나 UI.Image의 sprite를 defaultSprite에 할당 
        Init();
    }

    void Start()
    {
        if (defaultSprite != null && !_bDownloaded)
        {
            defaultSprite = GetMaskedSprite(defaultSprite.texture, true);
        }
    }

    /// <summary>
    /// url을 이용해서 이미지를 설정한다.
    /// </summary>
    /// <param name="url">이미지 url</param>

    public void SetImage(string url, System.Action<bool> finishedCallback = null)
    {
        _url = url;

        ClearTexture();
        if (string.IsNullOrEmpty(url))
        {
            LinkSprite(defaultSprite);
        }
        else if (url.IndexOf("http") == 0 || url.IndexOf("https") == 0)
        {
            this.finishedCallback = finishedCallback;

            UrlImageLoader.it.GetImage(url, this.gameObject, (outUrl, outTex) => 
            {
                if (this != null && _url == outUrl)
                {
                    UpdateUrlImage(outUrl, outTex);
                    _bDownloaded = true;
                }
            });
        }
        else
        {
            LinkSprite(GetMaskedSprite(Resources.Load<Texture2D>(url)));
        }
    }

    /// <summary>
    /// 텍스쳐를 마스킹하여 스프라이트로 만들어 주는 함수.
    /// </summary>
    /// <param name="tex">인풋 텍스쳐.</param>
    /// <returns>마스킹이 완료된 스프라이트.</returns>

    Sprite GetMaskedSprite(Texture2D tex, bool isDefault = false)
    {
        if (_linkedTexture != null)
        {
            Destroy(_linkedTexture);
        }

        if (tex == null)
            return defaultSprite;

        if (mask == null)
        {
            return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 1);
        }
        else 
        {
            if (tex.width != mask.texture.width || tex.height != mask.texture.height)
            {
                Debug.LogWarning("[UrlImage]텍스쳐 사이즈와 마스크 사이즈가 같지 않습니다!");
                tex = ResizeTexture(tex, ImageFilterMode.Average, mask.texture.width, mask.texture.height);
            }

            Texture2D tex2D = new Texture2D(tex.width, tex.height, TextureFormat.ARGB32, false);
            Color32[] texArr = tex.GetPixels32();

            Color32[] maskArr = mask.texture.GetPixels32();

            for (int i = 0; i < texArr.Length; i++)
            {
                texArr[i].a = maskArr[i].a;
            }

            tex2D.SetPixels32(texArr);
            tex2D.Apply();

            if (!isDefault)
                _linkedTexture = tex2D;

            return Sprite.Create(tex2D, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 1);
        }
    }

    /// <summary>
    /// UrlImageLoader에서 받은 텍스쳐를 가공하여 Image나 Sprite Renderer에 적용한다.
    /// Image나 Sprite Renderer가 현재 가지고 있는 이미지를 UrlImageLoader로 반환한다.
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="tex">Tex.</param>

    private void UpdateUrlImage(string url, Texture2D tex)
    {
        Sprite sprite = GetMaskedSprite(tex);
        LinkSprite(sprite);
        SetImageName(url);
        if (finishedCallback != null)
            finishedCallback(tex != null);
    }

    /// <summary>
    /// 초기화
    /// </summary>
    protected abstract void Init();


    /// <summary>
    /// sprite를 Image 혹은 SpriteRenderer와 연결.
    /// </summary>
    /// <param name="sprite">연결할 스프라이트.</param>
    protected abstract void LinkSprite(Sprite sprite);

    /// <summary>
    /// Image나 Sprite Renderer가 현재 가지고 있는 이미지를 UrlImageLoader로 반환한다.
    /// </summary>

    protected abstract void ClearTexture();

    /// <summary>
    /// 해당 클래스가 파괴될 때 Image나 Sprite Renderer가 현재 가지고 있는 이미지를 UrlImageLoader로 반환한다.
    /// </summary>

    void OnDestroy()
    {
        _url = null;
        ClearTexture();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inURL"></param>
    protected abstract void SetImageName(string inURL);

    /// <summary>
    /// Reset this instance.
    /// </summary>
    /// 
    public abstract void Reset();
    //{
    //       if (_image != null)
    //       {
    //           _image.sprite = _defaultSprite;
    //       }
    //       else if (_spriteRenderer != null)
    //       {
    //           _spriteRenderer.sprite = _defaultSprite;
    //       }
    //   }

    public enum ImageFilterMode : int
    {
        Nearest = 0,
        Biliner = 1,
        Average = 2
    }
    public Texture2D ResizeTexture(Texture2D pSource, ImageFilterMode pFilterMode, float inWidth, float inHeight)
    {
        //*** Variables
        int i;

        //*** Get All the source pixels
        Color[] aSourceColor = pSource.GetPixels(0);
        Vector2 vSourceSize = new Vector2(pSource.width, pSource.height);

        //*** Calculate New Size
        float xWidth = Mathf.RoundToInt(inWidth);
        float xHeight = Mathf.RoundToInt(inHeight);

        //*** Make New
        Texture2D oNewTex = new Texture2D((int)xWidth, (int)xHeight, TextureFormat.RGBA32, false);

        //*** Make destination array
        int xLength = (int)xWidth * (int)xHeight;
        Color[] aColor = new Color[xLength];

        Vector2 vPixelSize = new Vector2(vSourceSize.x / xWidth, vSourceSize.y / xHeight);

        //*** Loop through destination pixels and process
        Vector2 vCenter = new Vector2();
        for (i = 0; i < xLength; i++)
        {

            //*** Figure out x&y
            float xX = (float)i % xWidth;
            float xY = Mathf.Floor((float)i / xWidth);

            //*** Calculate Center
            vCenter.x = (xX / xWidth) * vSourceSize.x;
            vCenter.y = (xY / xHeight) * vSourceSize.y;

            //*** Do Based on mode
            //*** Nearest neighbour (testing)
            if (pFilterMode == ImageFilterMode.Nearest)
            {

                //*** Nearest neighbour (testing)
                vCenter.x = Mathf.Round(vCenter.x);
                vCenter.y = Mathf.Round(vCenter.y);

                //*** Calculate source index
                int xSourceIndex = (int)((vCenter.y * vSourceSize.x) + vCenter.x);

                //*** Copy Pixel
                aColor[i] = aSourceColor[xSourceIndex];
            }

            //*** Bilinear
            else if (pFilterMode == ImageFilterMode.Biliner)
            {

                //*** Get Ratios
                float xRatioX = vCenter.x - Mathf.Floor(vCenter.x);
                float xRatioY = vCenter.y - Mathf.Floor(vCenter.y);

                //*** Get Pixel index's
                int xIndexTL = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
                int xIndexTR = (int)((Mathf.Floor(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));
                int xIndexBL = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Floor(vCenter.x));
                int xIndexBR = (int)((Mathf.Ceil(vCenter.y) * vSourceSize.x) + Mathf.Ceil(vCenter.x));

                //*** Calculate Color
                aColor[i] = Color.Lerp(
                    Color.Lerp(aSourceColor[xIndexTL], aSourceColor[xIndexTR], xRatioX),
                    Color.Lerp(aSourceColor[xIndexBL], aSourceColor[xIndexBR], xRatioX),
                    xRatioY
                );
            }

            //*** Average
            else if (pFilterMode == ImageFilterMode.Average)
            {

                //*** Calculate grid around point
                int xXFrom = (int)Mathf.Max(Mathf.Floor(vCenter.x - (vPixelSize.x * 0.5f)), 0);
                int xXTo = (int)Mathf.Min(Mathf.Ceil(vCenter.x + (vPixelSize.x * 0.5f)), vSourceSize.x);
                int xYFrom = (int)Mathf.Max(Mathf.Floor(vCenter.y - (vPixelSize.y * 0.5f)), 0);
                int xYTo = (int)Mathf.Min(Mathf.Ceil(vCenter.y + (vPixelSize.y * 0.5f)), vSourceSize.y);

                //*** Loop and accumulate
                Color oColorTemp = new Color();
                float xGridCount = 0;
                for (int iy = xYFrom; iy < xYTo; iy++)
                {
                    for (int ix = xXFrom; ix < xXTo; ix++)
                    {

                        //*** Get Color
                        oColorTemp += aSourceColor[(int)(((float)iy * vSourceSize.x) + ix)];

                        //*** Sum
                        xGridCount++;
                    }
                }

                //*** Average Color
                aColor[i] = oColorTemp / (float)xGridCount;
            }
        }

        //*** Set Pixels
        oNewTex.SetPixels(aColor);
        oNewTex.Apply();

        //*** Return
        return oNewTex;
    }
}
