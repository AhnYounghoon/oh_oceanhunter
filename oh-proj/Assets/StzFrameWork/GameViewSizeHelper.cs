﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System;
using UnityEditor;

// 소스 참고 : https://github.com/anchan828/unity-GameViewSizeHelper/blob/master/Editor/GameViewSizeHelper.cs
public class GameViewSizeHelper
{
    
    [MenuItem("STZ/Add Resolution Preset Horizontal")]
    public static void AddResolutionPreset_Horizontal()
    {
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 480,    height = 320,     baseText = "1.5 iPhone 3GS"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 960,    height = 640,     baseText = "1.5 iPhone 4/4S"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1136,    height = 640,     baseText = "1.775 iPhone 5"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 320,    height = 240,     baseText = "1.333 HTC(Wildfire)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 480,    height = 320,     baseText = "1.5 HTC(Aria/Dream/Driod Eris/Gratia/Legend/Wildfire S)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 800,    height = 480,     baseText = "1.666 Galaxy(S/S2)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 768,     baseText = "1.666 Google Nexus 4"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 854,    height = 480,     baseText = "1.779 Motorola(Defy+/Droid/Droid 2/Droid X)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 960,    height = 540,     baseText = "1.777 HTC(Evo 3D/Sensation) Motorola(Droid 3)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 720,     baseText = "1.777 Galaxy(S3)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 800,     baseText = "1.6 Galaxy Note"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1920,    height = 1080,     baseText = "1.777 Galaxy(S4)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1024,    height = 600,     baseText = "1.71 HTC(Evo View)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1024,    height = 640,     baseText = "1.6"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1024,    height = 768,     baseText = "LG Optimus View/LG Optimus View 2"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 960,     baseText = "LG Optimus View 3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1920,    height = 1200,     baseText = "Google Nexus 7 (2013)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2560,    height = 1600,     baseText = "1.6 Google Nexus 10"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2560,    height = 1440,     baseText = "LG G3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2048,    height = 1536,     baseText = "Google Nexus 9"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2560,    height = 1532,     baseText = "Galaxy Note Edge"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440,    height = 810,     baseText = "Galaxy A7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440,    height = 1440,     baseText = "BlackBerry Passport"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2464,    height = 1600,     baseText = "1.666 Google Nexus 10"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 752,     baseText = "1.702 GalaxyTab 10.1"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1280,    height = 736,     baseText = "1.739 Google Nexus 7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1205,    height = 800,     baseText = "1.739 Google Nexus 7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1824,    height = 1200,     baseText = "Google Nexus 7 (2013)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1232,    height = 720,     baseText = "Vega Iron(소프트키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1184,    height = 720,     baseText = "Vega Iron(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1800,    height = 1080,     baseText = "Vega N6(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1848,    height = 1080,     baseText = "Vega N6(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1803,    height = 1080,     baseText = "LG G2(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1834,    height = 1080,     baseText = "LG V500(G_Pad)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1776,    height = 1080,     baseText = "1.777 Nexus 5"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2392,    height = 1440,     baseText = "LG G3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2960,   height = 1440,  baseText = "2.055 Galaxy(S8)" });
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 2880,   height = 1440,  baseText = "2 LG G6" });
    }
    
    [MenuItem("STZ/Add Resolution Preset Vertical")]
    public static void AddResolutionPreset_Vertical()
    {
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 320, height = 480, baseText = "1.5 iPhone 3GS"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 640, height = 960, baseText = "1.5 iPhone 4/4S"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 640, height = 1136, baseText = "1.775 iPhone 5"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 240, height = 320, baseText = "1.333 HTC(Wildfire)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 320, height = 480, baseText = "1.5 HTC(Aria/Dream/Driod Eris/Gratia/Legend/Wildfire S)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 480, height = 800, baseText = "1.666 Galaxy(S/S2)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 768, height = 1280, baseText = "1.666 Google Nexus 4"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 480, height = 854, baseText = "1.779 Motorola(Defy+/Droid/Droid 2/Droid X)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 540, height = 960, baseText = "1.777 HTC(Evo 3D/Sensation) Motorola(Droid 3)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 720, height = 1280, baseText = "1.777 Galaxy(S3)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 800, height = 1280, baseText = "1.6 Galaxy Note"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1920, baseText = "1.777 Galaxy(S4)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 600, height = 1024, baseText = "1.71 HTC(Evo View)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 640, height = 1024, baseText = "1.6"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 768, height = 1024, baseText = "LG Optimus View/LG Optimus View 2"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 960, height = 1280, baseText = "LG Optimus View 3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1200, height = 1920, baseText = "Google Nexus 7 (2013)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1600, height = 2560, baseText = "1.6 Google Nexus 10"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440, height = 2560, baseText = "LG G3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1536, height = 2048, baseText = "Google Nexus 9"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1532, height = 2560, baseText = "Galaxy Note Edge"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 810, height = 1440, baseText = "Galaxy A7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440, height = 1440, baseText = "BlackBerry Passport"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1600, height = 2464, baseText = "1.666 Google Nexus 10"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 752, height = 1280, baseText = "1.702 GalaxyTab 10.1"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 736, height = 1280, baseText = "1.739 Google Nexus 7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 800, height = 1205, baseText = "1.739 Google Nexus 7"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1200, height = 1824, baseText = "Google Nexus 7 (2013)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 720, height = 1232, baseText = "Vega Iron(소프트키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 720, height = 1184, baseText = "Vega Iron(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1800, baseText = "Vega N6(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1848, baseText = "Vega N6(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1803, baseText = "LG G2(소프트 키)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1834, baseText = "LG V500(G_Pad)"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1080, height = 1776, baseText = "1.777 Nexus 5"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440, height = 2392, baseText = "LG G3"});
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440, height = 2960, baseText = "2.055 Galaxy(S8)" });
        AddCustomSize(GameViewSizeGroupType.Android,new GameViewSize(){ type = GameViewSizeType.FixedResolution,width = 1440, height = 2880, baseText = "2 LG G6" });
    }
    #region public enum
    
    public enum GameViewSizeType
    {
        FixedResolution,
        AspectRatio
    }

    #endregion public enum

    #region private Fiald

    static string assemblyName = "UnityEditor.dll";
    static Type gameViewSizeType = Assembly.Load("UnityEditor.GameViewSizeType").GetType(assemblyName);
    static Type gameViewSize = Assembly.Load("UnityEditor.GameViewSize").GetType(assemblyName);
    static Type gameViewSizes = Assembly.Load("UnityEditor.ScriptableSingleton`1").GetType(assemblyName).MakeGenericType(Assembly.Load("UnityEditor.GameViewSizes").GetType(assemblyName));
    static BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

    #endregion private Fiald

    #region private Class

    private static GameViewSize _gameViewSize;
    public class GameViewSize
    {
        public GameViewSizeType type;
        public int width;
        public int height;
        public string baseText;
    }
    
    #endregion private Class
    
    #region public Method
    
    public static void AddCustomSize(GameViewSizeGroupType groupType, GameViewSize gameViewSize)
    {
        _gameViewSize = gameViewSize;
        object sizeType = Enum.Parse(gameViewSizeType, gameViewSize.type.ToString());
        
        ConstructorInfo ctor = GameViewSizeHelper.gameViewSize.GetConstructor(new Type[]
                                                                              {
            gameViewSizeType,
            typeof(int),
            typeof(int),
            typeof(string)
        });
        
        object instance_gameViewSize = ctor.Invoke(new object[]
                                                   {
            sizeType,
            gameViewSize.width,
            gameViewSize.height,
            gameViewSize.baseText
        });
        
        object instance_gameViewSizeGroup = GetGroup(groupType, instance);
        
        if (!Contains(instance_gameViewSizeGroup))
        {
            AddCustomSize(instance_gameViewSizeGroup, instance_gameViewSize);
        }
    }
    
    public static void AddCustomSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
    {
        AddCustomSize(groupType, new GameViewSize{ type = type, width = width, height = height, baseText = baseText });
    }
    
    public static bool RemoveCustomSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
    {
        _gameViewSize = new GameViewSize{ type = type, width = width, height = height, baseText = baseText };
        return Remove(GetGroup(groupType, instance));
    }
    public static bool RemoveCustomSize(GameViewSizeGroupType groupType, GameViewSize gameViewSize)
    {
        _gameViewSize = gameViewSize;
        return Remove(GetGroup(groupType, instance));
    }
    public static bool Contains(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
    {
        _gameViewSize = new GameViewSize{ type = type, width = width, height = height, baseText = baseText };
        return Contains(GetGroup(groupType, instance));
    }
    public static bool Contains(GameViewSizeGroupType groupType, GameViewSize gameViewSize)
    {
        _gameViewSize = gameViewSize;
        return Contains(GetGroup(groupType, instance));
    }
    
    
    public static void ChangeGameViewSize(GameViewSizeGroupType groupType, GameViewSizeType type, int width, int height, string baseText)
    {
        ChangeGameViewSize(groupType, new GameViewSize{ type = type, width = width, height = height, baseText = baseText });
    }
    
    public static void ChangeGameViewSize(GameViewSizeGroupType groupType, GameViewSize gameViewSize)
    {
        System.Type targetType = System.Reflection.Assembly.Load("UnityEditor.GameView").GetType(assemblyName);

        EditorWindow gameView = EditorWindow.GetWindow(targetType);
        PropertyInfo currentSizeGroupType = targetType.GetProperty("currentSizeGroupType", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static);
        GameViewSizeGroupType currentType = (GameViewSizeGroupType)currentSizeGroupType.GetValue(gameView, null);
        if (groupType != currentType)
        {
            Debug.LogError(string.Format("GameViewSizeGroupType is {0}. but Current GameViewSizeGroupType is {1}.", groupType, currentType));
            return;
        }
        object group = GetGroup(groupType, instance);
        int totalCount = GetTotalCount(group);
        int gameViewSizeLength = GetCustomCount(group);
        int index = -1;
        for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
        {
            object other_gameViewSize = GetGameViewSize(group, i);
            if (GameViewSize_Equals(_gameViewSize, other_gameViewSize))
            {
                index = i;
                break;
            }
        }
        if (index != -1)
        {
            PropertyInfo selectedSizeIndex = targetType.GetProperty("selectedSizeIndex", BindingFlags.Instance | BindingFlags.NonPublic);
            selectedSizeIndex.SetValue(gameView, index, null);
        }
    }
    
    #endregion public Method
    
    #region private Method
    
    static bool Remove(object instance_gameViewSizeGroup)
    {
        int gameViewSizeLength = GetCustomCount(instance_gameViewSizeGroup);
        int totalCount = GetTotalCount(instance_gameViewSizeGroup);
        for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
        {
            object other_gameViewSize = GetGameViewSize(instance_gameViewSizeGroup, i);
            if (GameViewSize_Equals(_gameViewSize, other_gameViewSize))
            {
                RemoveCustomSize(instance_gameViewSizeGroup, i);
                return true;
            }
        }
        return false;
    }
    
    static bool Contains(object instance_gameViewSizeGroup)
    {
        int gameViewSizeLength = GetCustomCount(instance_gameViewSizeGroup);
        int totalCount = GetTotalCount(instance_gameViewSizeGroup);
        for (int i = totalCount - gameViewSizeLength; i < totalCount; i++)
        {
            if (GameViewSize_Equals(_gameViewSize, GetGameViewSize(instance_gameViewSizeGroup, i)))
            {
                return true;
            }
        }
        return false;
    }
    
    private static bool GameViewSize_Equals(GameViewSize a, object b)
    {
        int b_width = (int)GetGameSizeProperty(b, "width");
        int b_height = (int)GetGameSizeProperty(b, "height");
        string b_baseText = (string)GetGameSizeProperty(b, "baseText");
        GameViewSizeType b_sizeType = (GameViewSizeType)Enum.Parse(typeof(GameViewSizeType), GetGameSizeProperty(b, "sizeType").ToString());
        
        return a.type == b_sizeType && a.width == b_width && a.height == b_height && a.baseText == b_baseText;
    }
    
    static object GetGameSizeProperty(object instance, string name)
    {
        return instance.GetType().GetProperty(name).GetValue(instance, new object[0]);
    }
    
    static object m_instance;
    
    static object instance
    {
        get
        {
            if (m_instance == null)
            {
                PropertyInfo propertyInfo_gameViewSizes = gameViewSizes.GetProperty("instance");
                m_instance = propertyInfo_gameViewSizes.GetValue(null, new object[0]);
            }
            return m_instance;
        }
    }
    
    static object GetGroup(GameViewSizeGroupType groupType, object instance_gameViewSizes)
    {
        Type[] returnTypes = new Type[] { groupType.GetType() };
        object[] parameters = new object[] { groupType };
        return instance_gameViewSizes.GetType().GetMethod("GetGroup", 
                                                          bindingFlags,
                                                          null,
                                                          returnTypes,
                                                          null).Invoke(instance_gameViewSizes, parameters);
    }
    
    static object GetGameViewSize(object instance_gameViewSizeGroup, int i)
    {
        Type[] returnTypes = new Type[] { typeof(int) };
        object[] parameters = new object[] { i };
        return instance_gameViewSizeGroup.GetType().GetMethod("GetGameViewSize",
                                                              bindingFlags,
                                                              null,
                                                              returnTypes, 
                                                              null).Invoke(instance_gameViewSizeGroup, parameters);
    }
    
    static int GetCustomCount(object instance_gameViewSizeGroup)
    {
        return (int)instance_gameViewSizeGroup.GetType().GetMethod("GetCustomCount",
                                                                   bindingFlags,
                                                                   null, 
                                                                   new Type[0],
                                                                   null).Invoke(instance_gameViewSizeGroup, new object[0]);
    }
    
    static int GetTotalCount(object instance_gameViewSizeGroup)
    {
        return (int)instance_gameViewSizeGroup.GetType().GetMethod("GetTotalCount",
                                                                   bindingFlags,
                                                                   null,
                                                                   new Type[0],
                                                                   null).Invoke(instance_gameViewSizeGroup, new object[0]);
    }
    
    static void AddCustomSize(object instance_gameViewSizeGroup, object instance_gameViewSize)
    {
        Type[] returnTypes = new Type[] { gameViewSize };
        object[] parameters = new object[] { instance_gameViewSize };
        instance_gameViewSizeGroup.GetType().GetMethod("AddCustomSize",
                                                       bindingFlags,
                                                       null, 
                                                       returnTypes,
                                                       null).Invoke(instance_gameViewSizeGroup, parameters);
    }
    
    static void RemoveCustomSize(object instance_gameViewSizeGroup, int index)
    {
        Type[] returnTypes = new Type[]{ typeof(int) };
        object[] parameters = new object[] { index };
        instance_gameViewSizeGroup.GetType().GetMethod("RemoveCustomSize", 
                                                       bindingFlags, 
                                                       null, 
                                                       returnTypes,
                                                       null).Invoke(instance_gameViewSizeGroup, parameters);
    }
    
    #endregion private Method
}
#endif