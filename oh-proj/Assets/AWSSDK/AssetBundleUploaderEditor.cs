﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using AWSUploader;
using UnityEditor;
using UnityEditor.SceneManagement;

public class AssetBundleUploaderEditor : EditorWindow
{
    public Scene tempScene;
    public string beforeScenePath = string.Empty;
    public bool bQuit = false;
    public bool awsSetting = false;

    // 로컬 에셋 번들 경로
    public string localPath = "AssetBundles";
    // 선택한 탭의 종류
    public int selectIdx = 0;

    GameObject uploaderGameObject;

    AWSSettings dev = new AWSSettings
    {
        enumRegionEndPoint  = ERegionEndpoint.APNortheast1,
        accessKeyId         = "AKIAIMXFGLT3WKNQZESA",
        secretAccessKey     = "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",
        bucketName          = "dev.anipang3.static",
        remotePath          = "pds/resources"
    };

    AWSSettings live = new AWSSettings
    {
        enumRegionEndPoint  = ERegionEndpoint.APNortheast2,
        accessKeyId         = "AKIAIMXFGLT3WKNQZESA",
        secretAccessKey     = "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",
        bucketName          = "anipang3.static",
        remotePath          = "pds/resources"
    };

    [MenuItem("Bundles/Upload Asset Bundles AWS S3")]
    public static void BuildAssetBundles()
    {
        AssetBundleUploaderEditor window = (AssetBundleUploaderEditor)EditorWindow.GetWindow(typeof(AssetBundleUploaderEditor));
    }

    private void TabS3Settings()
    {
        // Dev Settings
        EditorGUILayout.Space();
        dev.accessKeyId = EditorGUILayout.TextField("[DEV] AccessKeyId", dev.accessKeyId);
        dev.secretAccessKey = EditorGUILayout.TextField("[DEV] AccessSecretKey", dev.secretAccessKey);
        dev.remotePath = EditorGUILayout.TextField("[DEV] RemotePath", dev.remotePath);
        dev.enumRegionEndPoint = (ERegionEndpoint)EditorGUILayout.EnumPopup("[DEV] RegionEndPoint", dev.enumRegionEndPoint);
        EditorGUILayout.Space();

        // Live Settings
        live.accessKeyId = EditorGUILayout.TextField("[LIVE] AccessKeyId", live.accessKeyId);
        live.secretAccessKey = EditorGUILayout.TextField("[LIVE] AccessSecretKey", live.secretAccessKey);
        live.remotePath = EditorGUILayout.TextField("[LIVE] RemotePath", live.remotePath);
        live.enumRegionEndPoint = (ERegionEndpoint)EditorGUILayout.EnumPopup("[LIVE] RegionEndPoint", live.enumRegionEndPoint);
        EditorGUILayout.Space();

        localPath = EditorGUILayout.TextField("Local Asset Bundle Path", localPath);

        if (GUILayout.Button("Active S3 Uploader"))
        {
            if (EditorApplication.isPlaying)
            {
                if (EditorSceneManager.GetActiveScene().name == string.Empty)
                {
                    Debug.Log("<color=#FF0000>이미 S3 Uploader가 실행 중입니다!</color>");
                }
                else
                {
                    Debug.Log("<color=#FF0000>실행중인 씬을 중지하고 실행해주세요!</color>");
                }
            }
            else if (dev.CheckEmptyValue("DEV") && live.CheckEmptyValue("LIVE"))
            {
                // NOTE @minjun S3 SDK는 씬 메인 쓰레드를 사용해 빈씬 생성 후 Uploader를 작동 시킨다.
                beforeScenePath = EditorSceneManager.GetActiveScene().path;
                tempScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
                EditorApplication.isPlaying = true;

                uploaderGameObject = new GameObject("S3 Uploader");
                uploaderGameObject.AddComponent<AssetBundleUploader>();
                awsSetting = false;

                Debug.Log("<color=#FCDC79FF>S3 Uploader 로드중...</color>");
            }
        }
        else if (GUILayout.Button("Stop S3 Uploader"))
        {
            if (EditorApplication.isPlaying && EditorSceneManager.GetActiveScene().name == string.Empty)
            {
                EditorApplication.isPlaying = false;
                bQuit = true;
            }
            else
            {
                Debug.Log("<color=#FF0000>S3 Uploader가 작동중이지 않습니다!</color>");
            }
        }

        // S3 Uploader 관련 셋팅 초기화
        if (EditorApplication.isPlaying && !awsSetting)
        {
            awsSetting = true;
            uploaderGameObject.GetComponent<AssetBundleUploader>().InitSettings(dev, live, localPath);
        }
    }

    private void TabUploadAssetBundles()
    {
        string deploy   = string.Empty;
        string platform = string.Empty;
        bool isPress    = false;

        GUILayout.BeginVertical();

        GUIStyle devStyle = new GUIStyle(GUI.skin.label);
        devStyle.normal.textColor = Color.yellow;
        devStyle.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("[DEV]", devStyle);
        if (GUILayout.Button("Upload Android Dev"))
        {
            deploy = "DEV"; platform = "aos"; isPress = true;
        }
        else if (GUILayout.Button("Upload IOS Dev"))
        {
            deploy = "DEV"; platform = "ios"; isPress = true;
        }
        else if (GUILayout.Button("Upload Windows Dev"))
        {
            deploy = "DEV"; platform = "win"; isPress = true;
        }
        else if (GUILayout.Button("Upload OSX Dev"))
        {
            deploy = "DEV"; platform = "osx"; isPress = true;
        }
        GUIStyle liveStyle = new GUIStyle(GUI.skin.label);
        liveStyle.normal.textColor = Color.red;
        liveStyle.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("[LIVE]", liveStyle);
        if (GUILayout.Button("Upload Android Live"))
        {
            deploy = "LIVE"; platform = "aos"; isPress = true;
        }
        else if (GUILayout.Button("Upload IOS Live"))
        {
            deploy = "LIVE"; platform = "ios"; isPress = true;
        }
        else if (GUILayout.Button("Upload Windows Live"))
        {
            deploy = "LIVE"; platform = "win"; isPress = true;
        }
        else if (GUILayout.Button("Upload OSX Live"))
        {
            deploy = "LIVE"; platform = "osx"; isPress = true;
        }
        GUILayout.Label("※ S3 Uploader가 실행중일때만 업로드 할 수 있습니다.", EditorStyles.helpBox);
        GUILayout.EndVertical();

        // S3 에셋 번들 업로드
        if (isPress)
        {
            isPress = false;
            if (awsSetting && EditorSceneManager.GetActiveScene().name == string.Empty)
            {
                uploaderGameObject.GetComponent<AssetBundleUploader>().Sync(platform, deploy);
            }
            else
            {
                Debug.Log("<color=#FF0000>S3 Uploader가 작동중이지 않습니다!</color>");
            }
        }
    }

    void OnGUI()
    {
        selectIdx = GUILayout.Toolbar(selectIdx, new string[] { "S3 Settings", "Upload Asset Bundles" });
        if (selectIdx == 0)
        {
            TabS3Settings();
        }
        else if (selectIdx == 1)
        {
            TabUploadAssetBundles();
        }

        if (bQuit && !EditorApplication.isPlaying && beforeScenePath != string.Empty &&
            EditorSceneManager.GetActiveScene().path != beforeScenePath)
        {
            EditorSceneManager.OpenScene(beforeScenePath, OpenSceneMode.Additive);
            EditorSceneManager.CloseScene(EditorSceneManager.GetActiveScene(), true);
            bQuit       = false;
        }
    }
}
#endif
