﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AWSUploader
{
    public enum ERegionEndpoint
    {
        APNortheast1,
        APNortheast2,
        APSouth1,
        APSoutheast1,
        APSoutheast2,
        CNNorth1,
        EUCentral1,
        EUWest1,
        SAEast1,
        USEast1,
        USEast2,
        USGovCloudWest1,
        USWest1,
        USWest2,
    }

    public struct AWSSettings
    {
        public ERegionEndpoint enumRegionEndPoint;
        public string secretAccessKey;
        public string accessKeyId;
        public string bucketName;
        public string remotePath;

        /// <summary>
        /// 관련 셋팅이 작성되었는지 판별
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public bool CheckEmptyValue(string typeName)
        {
            bool success = true;
            if (secretAccessKey == string.Empty)
                Debug.LogError(string.Format("[{0}] SecretAccessKey를 작성해주세요!", typeName));
            if (accessKeyId == string.Empty)
                Debug.LogError(string.Format("[{0}] AccessKeyId를 작성해주세요!", typeName));
            if (bucketName == string.Empty)
                Debug.LogError(string.Format("[{0}] BucketName을 작성해주세요!", typeName));
            if (remotePath == string.Empty)
                Debug.LogError(string.Format("[{0}] RemotePath를 작성해주세요!", typeName));

            if (secretAccessKey == string.Empty || accessKeyId == string.Empty || bucketName == string.Empty || remotePath == string.Empty)
                success = false;
            return success;
        }

        public RegionEndpoint regionEndPoint
        {
            get
            {
                RegionEndpoint point = RegionEndpoint.APNortheast1;
                switch (enumRegionEndPoint)
                {
                    case ERegionEndpoint.APNortheast1: point = RegionEndpoint.APNortheast1; break;
                    case ERegionEndpoint.APNortheast2: point = RegionEndpoint.APNortheast2; break;
                    case ERegionEndpoint.APSouth1: point = RegionEndpoint.APSouth1; break;
                    case ERegionEndpoint.APSoutheast1: point = RegionEndpoint.APSoutheast1; break;
                    case ERegionEndpoint.APSoutheast2: point = RegionEndpoint.APSoutheast2; break;
                    case ERegionEndpoint.CNNorth1: point = RegionEndpoint.CNNorth1; break;
                    case ERegionEndpoint.EUCentral1: point = RegionEndpoint.EUCentral1; break;
                    case ERegionEndpoint.EUWest1: point = RegionEndpoint.EUWest1; break;
                    case ERegionEndpoint.SAEast1: point = RegionEndpoint.SAEast1; break;
                    case ERegionEndpoint.USEast1: point = RegionEndpoint.USEast1; break;
                    case ERegionEndpoint.USEast2: point = RegionEndpoint.USEast2; break;
                    case ERegionEndpoint.USGovCloudWest1: point = RegionEndpoint.USGovCloudWest1; break;
                    case ERegionEndpoint.USWest1: point = RegionEndpoint.USWest1; break;
                    case ERegionEndpoint.USWest2: point = RegionEndpoint.USWest2; break;
                    default: break;
                }
                return point;
            }
        }
    }

    public class AssetBundleUploader : MonoBehaviour
    {
        void Start()
        {
            UnityInitializer.AttachToGameObject(this.gameObject);
            Debug.Log("<color=#FCDC79FF>S3 Uploader 로드완료!</color>");
        }

        public void InitSettings(AWSSettings dev, AWSSettings live, string localPath)
        {
            _devSettings    = dev;
            _liveSettings   = live;
            _baseLocalPath  = localPath;

            AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
            _s3ClientDev    = new AmazonS3Client(_devSettings.accessKeyId, _devSettings.secretAccessKey, _devSettings.regionEndPoint);
            _s3ClientLive   = new AmazonS3Client(_liveSettings.accessKeyId, _liveSettings.secretAccessKey, _liveSettings.regionEndPoint);
        }

        #region private members
        private AWSSettings _devSettings;
        private AWSSettings _liveSettings;

        private IAmazonS3 _s3ClientDev;
        private IAmazonS3 _s3ClientLive;

        private IAmazonS3 Client
        {
            get
            {
                if (isLive)
                    return _s3ClientLive;
                else
                    return _s3ClientDev;
            }
        }
        #endregion
        private List<string> _remoteKeyList = new List<string>();
        private Dictionary<string, string> _remoteCRCList = new Dictionary<string, string>();
        private Dictionary<string, string> _localCRCList = new Dictionary<string, string>();
        public string remotePath;
        public string localPath;

        private List<string> _sameKeyList = new List<string>();
        private List<string> _addKeyList = new List<string>();
        private List<string> _modifyKeyList = new List<string>();
        private List<string> _deleteKeyList = new List<string>();
        private bool _loading = false;
        private string _baseLocalPath = string.Empty;
        public bool isLive = false;

        AWSSettings applySetting;
        private string syncDeploy = string.Empty;
        private string syncPlatform = string.Empty;

        /// <summary>
        /// 서버와 로컬 에셋번들을 서로 동기화 시킨다
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="deploy"></param>
        public void Sync(string platform, string deploy)
        {
            if (_loading)
            {
                Debug.Log("<color=#FCDC79FF>S3 Uploader가 이미 로드중입니다!</color>");
                return;
            }
            applySetting = (deploy == "DEV") ? _devSettings : _liveSettings;
            isLive       = (deploy == "LIVE") ? true : false;
            syncPlatform = platform;
            syncDeploy   = deploy;

            remotePath = string.Format("{0}/{1}/", applySetting.remotePath, platform);
            localPath = string.Empty;
            string[] localPaths = Application.dataPath.Split('/');
            for (int i = 0; i < localPaths.Length - 1; i++)
                localPath += string.Format("{0}/", localPaths[i]);
            localPath += string.Format("{0}/{1}/", _baseLocalPath ,platform);

            SyncAssetBundles();
        }

        private void SyncAssetBundles()
        {
            _loading = true;

            _remoteKeyList.Clear();
            _remoteCRCList.Clear();
            _localCRCList.Clear();

            _sameKeyList.Clear();
            _addKeyList.Clear();
            _modifyKeyList.Clear();
            _deleteKeyList.Clear();

            string key = null;
            Client.ListObjectsAsync(applySetting.bucketName, remotePath, (responseObj) =>
            {
                for (int i = 0; i < responseObj.Response.S3Objects.Count; i++)
                {
                    key = responseObj.Response.S3Objects[i].Key;
                    if (key.Contains(".manifest"))
                    {
                        _remoteKeyList.Add(key);
                    }
                }
                LoadRemoteManifest();
            });
        }

        private void LoadRemoteManifest()
        {
            Debug.Log(string.Format("[{0}] DownLoading : {1} Remain", syncDeploy, _remoteKeyList.Count));

            if (_remoteKeyList.Count == 0)
            {
                LoadLocalManifest();
                return;
            }

            for (int i = 0; i < _remoteKeyList.Count; i++)
            {
                string key = _remoteKeyList[i];
                //_remoteKeyList.RemoveAt(0);
                Client.GetObjectAsync(applySetting.bucketName, key, (responseObj) =>
                {
                    StreamReader reader = new StreamReader(responseObj.Response.ResponseStream);
                    string crc = GetCRC(reader);
                    if (crc != null)
                        _remoteCRCList.Add(responseObj.Response.Key.Replace(remotePath, "").Replace(".manifest", ""), crc);

                    //LoadRemoteManifest();
                    _remoteKeyList.Remove(responseObj.Response.Key);
                    Debug.Log(string.Format("[{0}] DownLoading : {1} Remain", syncDeploy, _remoteKeyList.Count));
                    if (_remoteKeyList.Count == 0)
                        LoadLocalManifest();
                });
            }
        }

        private void LoadLocalManifest()
        {
            if (!Directory.Exists(localPath))
            {
                Debug.Log("<color=#FF0000>에셋 번들 로컬 경로가 없습니다!</color>");
                return;
            }

            Debug.Log("Load Local Resources");
            var info = new DirectoryInfo(localPath);
            FileInfo[] fileInfos = info.GetFiles("*.manifest", SearchOption.AllDirectories);
            string crc = null;
            for (int i = 0; i < fileInfos.Length; i++)
            {
                StreamReader reader = new StreamReader(fileInfos[i].FullName);
                crc = GetCRC(reader);

                if (crc != null)
                    _localCRCList.Add(fileInfos[i].FullName.Replace("\\", "/").Replace(localPath, "").Replace(".manifest", ""), crc);
            }

            CompareCRCBetweenLocalAndRemote();
        }

        private string GetCRC(StreamReader reader)
        {
            string crcStr = null;
            while (!reader.EndOfStream)
            {
                crcStr = reader.ReadLine();
                if (crcStr.Contains("CRC"))
                {
                    crcStr = crcStr.Replace("CRC:", "").Trim();
                    break;
                }
            }
            return crcStr;
        }

        private void CompareCRCBetweenLocalAndRemote()
        {
            Debug.Log("Compare CRC");
            List<string> keyList = new List<string>(_localCRCList.Keys);

            foreach (string key in keyList)
            {
                if (_remoteCRCList.ContainsKey(key))
                {
                    if (_remoteCRCList[key] == _localCRCList[key])
                    {
                        _sameKeyList.Add(key);
                    }
                    else
                    {
                        _modifyKeyList.Add(key);
                    }


                    _localCRCList.Remove(key);
                    _remoteCRCList.Remove(key);
                }
                else
                {
                    _addKeyList.Add(key);
                }
            }

            foreach (string key in _remoteCRCList.Keys)
                _deleteKeyList.Add(key);

            //int i = 0;
            //for (i = 0; i < _sameKeyList.Count; i++) Debug.LogFormat("same : {0}", _sameKeyList[i]);
            //for (i = 0; i < _modifiedKeyList.Count; i++) Debug.LogFormat("modify : {0}", _modifiedKeyList[i]);
            //for (i = 0; i < _addKeyList.Count; i++) Debug.LogFormat("add : {0}", _addKeyList[i]);
            //for (i = 0; i < _deleteKeyList.Count; i++) Debug.LogFormat("delete : {0}", _deleteKeyList[i]);

#if UNITY_EDITOR
            SelectEditorWindow window = EditorWindow.GetWindowWithRect<SelectEditorWindow>
                (new Rect(0, 0, 500, 750), true,string.Format("[{0} {1}] Send to S3", syncDeploy,syncPlatform.ToUpper()), true);
            window.SetData(_addKeyList, _modifyKeyList, _deleteKeyList, this);
#endif

            _loading = false;

            Debug.Log("Choice assets");
        }

        public void ApplyModifiedAssets(List<string> addKeyList, List<string> modifyKeyList, List<string> deleteKeyList)
        {
            if (_loading)
                return;

            Debug.Log("<color=#FCDC79FF>서버 동기화 성공</color>");
            Debug.Log("Apply choose assets");

            _addKeyList = addKeyList;
            _modifyKeyList = modifyKeyList;
            _deleteKeyList = deleteKeyList;

            int i = 0;
            int total = _addKeyList.Count * 2 + _modifyKeyList.Count * 2 + _deleteKeyList.Count * 2;


            // add
            for (i = 0; i < _addKeyList.Count; i++)
            {
                var add = new PutObjectRequest()
                {
                    BucketName = applySetting.bucketName,
                    Key = remotePath + _addKeyList[i],
                    InputStream = new FileStream(localPath + _addKeyList[i], FileMode.Open, FileAccess.Read, FileShare.Read),
                    CannedACL = S3CannedACL.PublicRead
                };
                Client.PutObjectAsync(add, (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });

                var addManifest = new PutObjectRequest()
                {
                    BucketName = applySetting.bucketName,
                    Key = remotePath + _addKeyList[i] + ".manifest",
                    InputStream = new FileStream(localPath + _addKeyList[i] + ".manifest", FileMode.Open, FileAccess.Read, FileShare.Read),
                    CannedACL = S3CannedACL.PublicRead
                };
                Client.PutObjectAsync(addManifest, (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });
            }


            // modify
            for (i = 0; i < _modifyKeyList.Count; i++)
            {
                var modify = new PutObjectRequest()
                {
                    BucketName = applySetting.bucketName,
                    Key = remotePath + _modifyKeyList[i],
                    CannedACL = S3CannedACL.PublicRead
                };
                modify.InputStream = new FileStream(localPath + _modifyKeyList[i], FileMode.Open, FileAccess.Read, FileShare.Read);
                Client.PutObjectAsync(modify, (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });

                var modifyManifest = new PutObjectRequest()
                {
                    BucketName = applySetting.bucketName,
                    Key = remotePath + _modifyKeyList[i] + ".manifest",
                    CannedACL = S3CannedACL.PublicRead
                };
                modifyManifest.InputStream = new FileStream(localPath + _modifyKeyList[i] + ".manifest", FileMode.Open, FileAccess.Read, FileShare.Read);
                Client.PutObjectAsync(modifyManifest, (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });
            }


            // delete
            for (i = 0; i < _deleteKeyList.Count; i++)
            {
                Client.DeleteObjectAsync(applySetting.bucketName, remotePath + _deleteKeyList[i], (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });
                Client.DeleteObjectAsync(applySetting.bucketName, remotePath + _deleteKeyList[i] + ".manifest", (response) =>
                {
                    Debug.Log(string.Format("Apply : {0} Remain", --total));
                    if (total == 0) Debug.Log("Apply Complete!");
                });
            }
        }

    } // end of class

#if UNITY_EDITOR
    public class SelectEditorWindow : EditorWindow
    {
        private List<string> _addKeyList;
        private List<string> _modifyKeyList;
        private List<string> _deleteKeyList;
        private AssetBundleUploader _delegate;
        Vector2 scrollAdd = Vector2.one;
        Vector2 scrollModify = Vector2.one;
        Vector2 scrollDelete = Vector2.one;
        private List<bool> _addToggleList = new List<bool>();
        private List<bool> _modifyToggleList = new List<bool>();
        private List<bool> _deleteToggleList = new List<bool>();

        public void SetData(List<string> addKeyList, List<string> modifiedKeyList, List<string> deleteKeyList, AssetBundleUploader inDelegate)
        {
            _addKeyList = addKeyList;
            _modifyKeyList = modifiedKeyList;
            _deleteKeyList = deleteKeyList;
            _delegate = inDelegate;

            int i = 0;
            for (i = 0; i < _addKeyList.Count; i++) _addToggleList.Add(false);
            for (i = 0; i < _modifyKeyList.Count; i++) _modifyToggleList.Add(false);
            for (i = 0; i < _deleteKeyList.Count; i++) _deleteToggleList.Add(false);
        }

        void OnGUI()
        {
            int i = 0;
            EditorGUILayout.LabelField("ADD", EditorStyles.boldLabel);
            scrollAdd = EditorGUILayout.BeginScrollView(scrollAdd, GUILayout.Height(200));
            if (_addKeyList != null)
                for (i = 0; i < _addKeyList.Count; i++) _addToggleList[i] = EditorGUILayout.ToggleLeft(_addKeyList[i], _addToggleList[i]);
            EditorGUILayout.EndScrollView();



            EditorGUILayout.Space(); EditorGUILayout.Space();
            EditorGUILayout.LabelField("MODIFY", EditorStyles.boldLabel);
            scrollModify = EditorGUILayout.BeginScrollView(scrollModify, GUILayout.Height(200));
            if (_modifyKeyList != null)
                for (i = 0; i < _modifyKeyList.Count; i++) _modifyToggleList[i] = EditorGUILayout.ToggleLeft(_modifyKeyList[i], _modifyToggleList[i]);
            EditorGUILayout.EndScrollView();



            EditorGUILayout.Space(); EditorGUILayout.Space();
            EditorGUILayout.LabelField("DELETED", EditorStyles.boldLabel);
            scrollDelete = EditorGUILayout.BeginScrollView(scrollDelete, GUILayout.Height(200));
            if (_deleteKeyList != null)
                for (i = 0; i < _deleteKeyList.Count; i++) _deleteToggleList[i] = EditorGUILayout.ToggleLeft(_deleteKeyList[i], _deleteToggleList[i]);
            EditorGUILayout.EndScrollView();



            EditorGUILayout.Space(); EditorGUILayout.Space();
            if (GUILayout.Button("Apply"))
            {
                if (_delegate.isLive)
                    if (!EditorUtility.DisplayDialog("Live update!!", "It's live update. are you sure?", "Upload", "Cancel"))
                    {
                        return;
                    }

                for (i = _addKeyList.Count - 1; i >= 0; i--)
                    if (!_addToggleList[i]) _addKeyList.RemoveAt(i);

                for (i = _modifyKeyList.Count - 1; i >= 0; i--)
                    if (!_modifyToggleList[i]) _modifyKeyList.RemoveAt(i);

                for (i = _deleteKeyList.Count - 1; i >= 0; i--)
                    if (!_deleteToggleList[i]) _deleteKeyList.RemoveAt(i);

                //for (i = 0; i < _addKeyList.Count; i++) Debug.LogFormat("ADD : {0}", _addKeyList[i]);
                //for (i = 0; i < _modifyKeyList.Count; i++) Debug.LogFormat("MODIFIED : {0}", _modifyKeyList[i]);
                //for (i = 0; i < _deleteKeyList.Count; i++) Debug.LogFormat("DELETE : {0}", _deleteKeyList[i]);

                _delegate.ApplyModifiedAssets(_addKeyList, _modifyKeyList, _deleteKeyList);

                this.Close();

            }
        }
    }
#endif

}
