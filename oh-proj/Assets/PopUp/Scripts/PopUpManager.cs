﻿using UnityEngine;
using System.Collections;

using STZFramework;

public class PopupManager : BasePopupManager
{
}

/// <summary>
/// 간단한 팝업으로 이므로 편하게 사용하도록 interface 래핑 
/// </summary>
public static class SimplePopup
{
    /// <summary>
    /// 작은사이즈의 [확인]버튼 알림 팝업 (너비 - 한글 기준 15글자)
    /// </summary>
    /// <param name="inText"></param>
    /// <param name="inCallback"></param>
    /// <param name="inTitleText"></param>
    public static void SmallAleart(string inText, System.Action inCallback = null, string inTitleText = null)
    {
        Debug.Log("[SIMPLE_POPUP_SMALL_Aleart]" + inText);
        SingletonController.Get<PopupManager>().Show(typeof(PopupSimpleSmall), (param) => { if (inCallback != null) inCallback(); }, STZCommon.Hash("text", inText,
                                                                                                                            "titleText", inTitleText));
    }

    /// <summary>
    /// 일반적인 [확인]버튼 같은 알림 팝업 
    /// </summary>
    /// <param name="inText"></param>
    /// <param name="inCallback"></param>
    /// <param name="inTitleText"></param>
    public static void Alert(string inText, System.Action inCallback = null, string inTitleText = null, bool inIsLoadingLayer = false)
    {
        Debug.Log("[SIMPLE_POPUP_Alert]" + inText);
        BasePopUp popup = SingletonController.Get<PopupManager>().Show(typeof(PopupSimple), (param) => { if (inCallback != null) inCallback(); }, STZCommon.Hash("text", inText,
                                                                                                                       "buttonType", PopupSimple.EButtonType.OK,
                                                                                                                       "titleText", inTitleText));

        if (inIsLoadingLayer)
        {
            popup.Get<Canvas>().sortingLayerName = "LoadingLayer";
        }
    }

    /// <summary>
    /// [확인]버튼만 있는 알림 팝업 
    /// </summary>
    /// <param name="inText"></param>
    /// <param name="inCallback"></param>
    /// <param name="inTitleText"></param>
    public static void ForceConfirm(string inText, System.Action inCallback = null, string inTitleText = null, bool inIsLoadingLayer = false)
    {
        Debug.Log("[SIMPLE_POPUP_Alert]" + inText);
        BasePopUp popup = SingletonController.Get<PopupManager>().Show(typeof(PopupSimple), (param) => { if (inCallback != null) inCallback(); }, STZCommon.Hash("text", inText,
                                                                                                                       "buttonType", PopupSimple.EButtonType.Only_OK,
                                                                                                                       "titleText", inTitleText));

        if (inIsLoadingLayer)
        {
            popup.Get<Canvas>().sortingLayerName = "LoadingLayer";
        }
    }

    /// <summary>
    /// OK 버튼과 Exit버튼만 있는 선택 팝업 
    /// </summary>
    /// <param name="inText"></param>
    /// <param name="inYesCallback"></param>
    /// <param name="inNoCallback"></param>
    /// <param name="inTitleText"></param>
    public static void ConfirmOneButton(string inText, System.Action inYesCallback = null, System.Action inNoCallback = null, string inTitleText = null, bool inUseBackButton = true, Sprite inItemSprite = null)
    {
        Debug.Log("[SIMPLE_POPUP_Confirm]" + inText);

        SingletonController.Get<PopupManager>().Show(typeof(PopupSimple), (inParam) =>
        {
            bool result = (bool)inParam["result"];

            if (result)
            {
                if (inYesCallback != null)
                {
                    inYesCallback();
                }
            }
            else
            {
                if (inNoCallback != null)
                {
                    inNoCallback();
                }
            }

        }, STZCommon.Hash("text", inText, "buttonType", PopupSimple.EButtonType.OK_Exit, "titleText", inTitleText, "useBackButton", inUseBackButton, "itemImage", inItemSprite));
    }

    /// <summary>
    /// 공용적으로 사용되는 선택 팝업 
    /// </summary>
    /// <param name="inText"></param>
    /// <param name="inYesCallback"></param>
    /// <param name="inNoCallback"></param>
    /// <param name="inTitleText"></param>
    /// <param name="inYesButtonText"></param>
    /// <param name="inNobuttonText"></param>
    /// <param name="isLoadingLayer"></param>
    /// <param name="inItemSprite"></param>
    public static void Confirm(string inText,
                                   System.Action inYesCallback = null, System.Action inNoCallback = null, string inTitleText = null,
                                   string inYesButtonText = null, string inNobuttonText = null, bool isLoadingLayer = false, Sprite inItemSprite = null)
    {
        BasePopUp popup = SingletonController.Get<PopupManager>().Show(typeof(PopupSimple), (inParam) =>
        {
            bool result = (bool)inParam["result"];
            if (result)
            {
                if (inYesCallback != null)
                {
                    inYesCallback();
                }
            }
            else
            {
                if (inNoCallback != null)
                {
                    inNoCallback();
                }
            }

        }, STZCommon.Hash("text", inText,
                          "buttonType", PopupSimple.EButtonType.Yes_No,
                          "titleText", inTitleText,
                          "yesButtonText", inYesButtonText,
                          "noButtonText", inNobuttonText,
                          "itemImage", inItemSprite));

        if (isLoadingLayer)
        {
            popup.Get<Canvas>().sortingLayerName = "LoadingLayer";
        }
    }

    public static void ConfirmReserved(string inText,
                                   System.Action inYesCallback = null, System.Action inNoCallback = null, string inTitleText = null,
                                   string inYesButtonText = null, string inNobuttonText = null)
    {
        SingletonController.Get<PopupManager>().Reserve(typeof(PopupSimple), (inParam) =>
        {
            bool result = (bool)inParam["result"];
            if (result)
            {
                if (inYesCallback != null)
                {
                    inYesCallback();
                }
            }
            else
            {
                if (inNoCallback != null)
                {
                    inNoCallback();
                }
            }

        }, STZCommon.Hash("text", inText,
                          "buttonType", PopupSimple.EButtonType.Yes_No,
                          "titleText", inTitleText,
                          "yesButtonText", inYesButtonText,
                          "noButtonText", inNobuttonText));
    }
};