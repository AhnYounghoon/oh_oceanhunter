﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BasePopUp : MonoBehaviour
{
    [System.Serializable]
    public class SoundData
    {
        public SoundData(string inClipName, string inGroupName)
        {
            clipName = inClipName;
            groupName = inGroupName;
        }

        public string groupName;
        public string clipName;
    }

    /* 하나의 팝업에서 사용 가능한 뎁스의 범위 */
    public const int SORTING_ORDER_DEPTH = 50;

    /// <summary>
    /// 팝업이 종료될 때 사용할 콜백
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="param">팝업 파라메터</param>
    public delegate void Callback(Hashtable param);

    /// <summary>
    /// 팝업 액션 설정을 위한 델리게이트
    /// </summary>
    protected delegate void ActionDelegate();

    [Tooltip("딤드 사용 여부")]
	[SerializeField]
	protected bool _isDimmed;

    [Tooltip("팝업에서 설정한 파라메터 키 리스트")]
	[SerializeField]
	protected string[] _parameterKeyList;

    [Tooltip("팝업이 나타날 때 사운드 정보(clip@group)")]
    [SerializeField]
    protected string _openSound = "popup_open@sfx_ui";

    [Tooltip("팝업이 사라질 때 사운드 정보(clip@group)")]
    [SerializeField]
    protected string _closeSound = "popup_close@sfx_ui";

    [Tooltip("어셋번들 사용 여부")]
    [SerializeField]
    protected bool _isUseAssetBundle;
    public bool isUseAssetBundle { get{ return _isUseAssetBundle; } }

    /* 팝업 기본 배경 */
    protected Transform _bg;

    /* 딤드 */
    protected Transform _dimmed;

    /* 팝업 콜백 */
    protected Callback _callback;

    /* 팝업 파라메터 */
    protected Hashtable _param;

    /* 백그라운드 애니메이터 */
    protected Animator _animator;

    /* 딤드 애니메이터 */
    protected Animator _dimmedAnimator;

    /* 캔버스 */
    protected Canvas _canvas;

    /* 캔버스 그룹 */
    protected CanvasGroup _canvasGroup;

    /* 팝업 오픈 여부 */
    protected bool _isOpend = false;

    /* 팝업 클로즈 여부 */
    protected bool _isClosed = false;

    protected bool _animating = true;

    /// <summary>
    /// 현재 팝업의 기본 sorting order
    /// </summary>
    public int sortingOrder { get { return _canvas.sortingOrder; } set { _canvas.sortingOrder = value; } }

    /// <summary>
    /// 팝업을 비추는 카메라
    /// </summary>
    public Camera viewCamera { set { _canvas.worldCamera = value; } }

    /// <summary>
    /// 어셋 번들을 로드 중인지 체크
    /// </summary>
    public bool loadAssetBundle = false;

    /// <summary>
    /// 최초 생성시 초기화
    /// </summary>
    protected virtual void Awake()
    {
        _bg = transform.Find("Background");

        _dimmed = transform.Find("Dimmed");
        _dimmed.gameObject.SetActive(_dimmed);

        _animator = GetComponent<Animator>();
        _dimmedAnimator = _dimmed.GetComponent<Animator>();

        _canvas = GetComponent<Canvas>();

        _canvasGroup = _bg.GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// 팝업 등장
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="callback">팝업 콜백</param>
    /// <param name="param">팝업 파라메터</param>
    public void Open(Callback callback = null, Hashtable param = null)
    {
        /* 팝업이 열려 있으면 해당 함수를 수행하지 않는다. */
        if (_isOpend)
            return;

        _isOpend = true;
        if (_canvasGroup != null)
        {
            _canvasGroup.interactable = false;
        }

        _callback = callback;
        _param = param == null ? new Hashtable() : param;

        if (_parameterKeyList != null)
        {
            for (int i = 0; i < _parameterKeyList.Length; i++)
            {
                if (!_param.ContainsKey(_parameterKeyList[i]))
                {
                    Debug.LogWarning(string.Format("파라메터 \"{0}\"가 없습니다.", _parameterKeyList[i]));
                }
            }
        }

        PrevOpen();


        if(_isUseAssetBundle)
        {
            LoadAssetBundle();
        }
        else
        {
            AnimateOpen();
        }
    }

    private void LoadAssetBundle()
    {
        //string className = this.GetType().Name;
        //var popupResource = ServerStatic.PopupResource.GetStatic(className, null);
        //if(popupResource == null)
        //{
        //    AnimateOpen();
        //    return;
        //}

        //loadAssetBundle = true;

        //SimpleLoadingPopup.Show();
        //this.gameObject.SetActive(false);
        //int version = popupResource.resource_version;
        //string url = StaticUrl.GetPopupResourcesUrl(popupResource.assetbundle_name, version);
        //PopupAssetBundleImageSet imageSet = null;
        //SingletonController.Get<ABCacheManager>().Get<GameObject>((obj) =>
        //{
        //    loadAssetBundle = false;
        //    SimpleLoadingPopup.Hide();
        //    this.gameObject.SetActive(true);

        //    if (obj == null || gameObject.Equals(null))
        //    {
        //        UnityEngine.Debug.Log(StringHelper.Format("popup_images 리소스 로딩 실패."));
        //        AnimateOpen();
        //        return;
        //    }

        //    imageSet = ((GameObject)obj).Get<PopupAssetBundleImageSet>();
        //    BindImagesFromAssetBundle(imageSet);
            
        //    AnimateOpen();
        //}, ABCacheManager.EType.POPUP_RESOURCE, url, version, gameObject, "popup_images");

        // static 체크
        // ABCacheManager 로드

        //var list = GetComponentsInChildren<Image>(true);
        //for(Image img in list)
        //{
        //    img.name
        //}
    }

    //private void BindImagesFromAssetBundle(PopupAssetBundleImageSet imageSet)
    //{
    //    var list = this.GetComponentsInChildren<Image>(true);
    //    for(int i = 0; i < list.Length; i++)
    //    {
    //        if(imageSet.bindingName.Contains(list[i].name))
    //        {
    //            list[i].sprite = imageSet.sprites[imageSet.bindingName.IndexOf(list[i].name)];
    //        }
    //    }
    //}

    protected virtual void AnimateOpen()
    {
        if (_animator == null || !_animator.enabled)
        {
            Invoke("LastOpen", 0.033f);
        }
        else
        {
            _animator.Play("Open");
            _animating = true;
        }

        if (_dimmedAnimator == null || !_dimmedAnimator.enabled)
        {
            _dimmed.gameObject.SetActive(_isDimmed);
        }
        else
        {
            if (_isDimmed)
            {
                _dimmedAnimator.Play("Open");
            }
            else
            {
                _dimmed.gameObject.SetActive(false);
            }
        }

        PlaySound(_openSound);
    }

    protected virtual void AnimateClose()
    {
        _animator.Play("Close");
        if (_isDimmed && _dimmedAnimator != null && _dimmedAnimator.enabled)
        {
            _dimmedAnimator.Play("Close");
        }
    }

    /// <summary>
    /// 팝업 퇴장
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="callback">팝업 콜백</param>
    /// <param name="param">팝업 파라메터</param>
    public virtual void Close(bool isCallbackEnabled = true, bool isAnimationEnabled = true, bool isCanceledNext = false)
    {
        /* 팝업이 닫혀 있으면 해당 함수를 수행하지 않는다. */
        if (_isClosed)
            return;

        if (_canvasGroup != null)
        {
            _canvasGroup.interactable = false;
        }
        bool activeEnabled = (_animator != null && _animator.enabled) && isActiveAndEnabled;

        /* 콜백을 사용하지 않는다면 등록되어 있는 콜백 제거 */
        if (!isCallbackEnabled)
            _callback = null;

        _isClosed = true;
        _animating = true;

        if (activeEnabled)
        {
            PrevClose();
            AnimateClose();
        }
        else
        {
            PrevClose();

            if (_callback != null)
                _callback(_param);
            SingletonController.Get<PopupManager>().Remove(this, isCanceledNext);
        }

        PlaySound(_closeSound);
    }

    /// <summary>
    /// 팝업 오픈 시작시 호출 함수
    /// 필요하다면 상속 받은 클래스에서 오버라이드 하여 사용
    /// </summary>
    protected virtual void PrevOpen()
    {
        //STZFramework.STZDebug.Log("PrevOpen");
    }

    /// <summary>
    /// 팝업 오픈 종료시 호출 함수
    /// 필요하다면 상속 받은 클래스에서 오버라이드 하여 사용
    /// </summary>
    protected virtual void PostOpen()
    {
        //STZFramework.STZDebug.Log("PostOpen");
    }

    /// <summary>
    /// 팝업 오픈 애니메이션 종료시 호출 함수
    /// </summary>
    protected virtual void LastOpen()
    {
        if (_canvasGroup != null)
        {
            _canvasGroup.interactable = true;
        }
        PostOpen();
        _animating = false;
    }

    /// <summary>
    /// 팝업 클로즈 시작시 호출 함수
    /// 필요하다면 상속 받은 클래스에서 오버라이드 하여 사용
    /// </summary>
    protected virtual void PrevClose()
    {
        //STZFramework.STZDebug.Log("PrevClose");
    }

    /// <summary>
    /// 팝업 클로즈 종료시 호출 함수
    /// 필요하다면 상속 받은 클래스에서 오버라이드 하여 사용
    /// </summary>
    protected virtual void PostClose()
    {
        //STZFramework.STZDebug.Log("PostClose");
    }

    /// <summary>
    /// 팝업 클로즈 애니메이션 종료시 호출 함수
    /// </summary>
    protected void LastClose()
    {
        PostClose();

        SingletonController.Get<PopupManager>().Remove(this, false);

        if (_callback != null)
            _callback(_param);
    }

    /// <summary>
    /// 팝업 정상 종료
    /// </summary>
    public virtual void Exit()
    {
        Close(true, true, false);
    }

    /// <summary>
    /// 안드로이드에서 백버튼이 눌러졌을 때 호출되는 함수.
    /// 백 버튼으로 종료시에만 특정 이벤트가 필요하다면 여기에 오버라이드 한다.
    /// </summary>
    public virtual void OnAndroidBackbutton()
    {
        if (!_animating)
            Exit();
    }

    /// <summary>
    /// 사운드 플레이
    /// </summary>
    /// <param name="clipNameAtGroupName">[사운드 클립 이름]@[사운드 그룹 이름]</param>
    public void PlaySound(string clipNameAtGroupName)
    {
        if (string.IsNullOrEmpty(clipNameAtGroupName))
            return;

        string[] strArr = clipNameAtGroupName.Split('@');
        if (strArr.Length == 2 
            && !string.IsNullOrEmpty(strArr[0]) 
            && !string.IsNullOrEmpty(strArr[1]))
        {
            //SingletonController.Get<SoundManager>().Play(strArr[0], strArr[1]);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(BasePopUp), true)]
public class BasePopUpEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BasePopUp popup = (BasePopUp)target;

        if (GUILayout.Button("assign images", GUILayout.Height(30)))
        {
            if (!popup.isUseAssetBundle)
                return;

            string className = popup.GetType().Name;
            string path = StringHelper.Format("Assets/AssetBundles/PopupResource/{0}/popup_images.prefab", className);
            var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (obj == null)
                return;

            //var imageSet = obj.GetComponent<PopupAssetBundleImageSet>();

            //var list = popup.GetComponentsInChildren<Image>(true);
            //for(int i = 0; i < list.Length; i++)
            //{
            //    if (imageSet.bindingName.Contains(list[i].name))
            //    {
            //        list[i].sprite = imageSet.sprites[imageSet.bindingName.IndexOf(list[i].name)];
            //    }
            //}
            popup.gameObject.SetActive(false);
            popup.gameObject.SetActive(true);
        }

        if (GUILayout.Button("remove images", GUILayout.Height(30)))
        {
            if (!popup.isUseAssetBundle)
                return;

            string className = popup.GetType().Name;
            string path = StringHelper.Format("Assets/AssetBundles/PopupResource/{0}/popup_images.prefab", className);
            var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (obj == null)
                return;

            //var imageSet = obj.GetComponent<PopupAssetBundleImageSet>();

            //var list = popup.GetComponentsInChildren<Image>(true);
            //for (int i = 0; i < list.Length; i++)
            //{
            //    if (imageSet.bindingName.Contains(list[i].name))
            //    {
            //        list[i].sprite = null;
            //    }
            //}

            popup.gameObject.SetActive(false);
            popup.gameObject.SetActive(true);
        }
    }
}
#endif
