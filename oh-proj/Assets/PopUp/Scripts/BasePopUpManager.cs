﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 팝업 기본 정보 Attribute
/// </summary>

public class PopupAttribute : System.Attribute
{
    public string prefab_path = null;
}

/// <summary>
/// 팝업 매니저 기본
/// </summary>

public class BasePopupManager
{
    /// <summary>
    /// 예약된 팝업 정보
    /// </summary>
    protected class ReservedPopUpData
    {
        /* 팝업 종류 */
        private System.Type _popUpType;

        /* 팝업 콜백 */
        private BasePopUp.Callback _callback;

        /* 팝업 파라메터 */
        private Hashtable _param;

        /// <summary>
        /// 팝업 종류
        /// </summary>
        public System.Type popUpType { get { return _popUpType; } }

        /// <summary>
        /// 팝업 콜백
        /// </summary>
        public BasePopUp.Callback callback { get { return _callback; } }

        /// <summary>
        /// 팝업 파라메터
        /// </summary>
        public Hashtable param { get { return _param; } }

        /// <summary>
        /// 팝업 예약 정보 생성
        /// </summary>
        /// <param name="inPopUpType">팝업 종류</param>
        /// <param name="inCallback">팝업 콜백</param>
        /// <param name="inParam">팝업 파라메터</param>

        public ReservedPopUpData(System.Type inPopUpType, BasePopUp.Callback inCallback, Hashtable inParam)
        {
            _popUpType = inPopUpType;
            _callback = inCallback;
            _param = inParam;
        }
    }

    /* 현재 화면상에 나타나 있는 팝업들 */
    protected List<BasePopUp> _popUpList = new List<BasePopUp>();

    /* 예약된 팝업들 */
    protected Dictionary<int, List<ReservedPopUpData>> _reservedPopUpDataDic = new Dictionary<int, List<ReservedPopUpData>>();

    /// <summary>
    /// 화면에 표시되어 있는 팝업 개수
    /// </summary>
    public int displayedPopUpCount { get { return _popUpList.Count; } }

    /// <summary>
    /// 예약 되어 있는 팝업 개수
    /// </summary>
    public int reservedPopUpCount { get { return _popUpList.Count; } }

    /// <summary>
    /// 화면에 표시된 팝업이 있는지에 대한 여부
    /// </summary>
    public bool isEmptyDisplayedPopUp { get { return displayedPopUpCount == 0; } }

    /// <summary>
    /// 최상단 팝업
    /// </summary>
    public BasePopUp frontPopUp { get { if (_popUpList.Count == 0) return null; return _popUpList[_popUpList.Count - 1]; } }

    /// <summary>
    /// 팝업을 비추는 카메라
    /// </summary>
    public Camera viewCamera { get; set; }

    /// <summary>
    /// 팝업 생성
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="callback">팝업 콜백</param>
    /// <param name="param">팝업 파라메터</param>
    /// <returns>생성된 팝업</returns>
    protected BasePopUp CreatePopUp(System.Type popUpType, BasePopUp.Callback callback, Hashtable param)
    {
        BasePopUp popUp = null;

        /* 팝업 종류에 따라 해당 Attribute에서 기본 정보를 가져와서 팝업을 생성한다. */
        foreach (System.Attribute attr in popUpType.GetCustomAttributes(true))
        {
            PopupAttribute popUpData = attr as PopupAttribute;
            if (popUpData != null)
            {
                var obj = Resources.Load(popUpData.prefab_path);
                var gameObj = MonoBehaviour.Instantiate(obj) as GameObject;
                gameObj.SetActive(true);
                popUp = gameObj.GetComponent(popUpType.ToString()) as BasePopUp;
                popUp.transform.SetAsLastSibling();
                popUp.sortingOrder = _popUpList.Count * BasePopUp.SORTING_ORDER_DEPTH;
                _popUpList.Add(popUp);

                if (viewCamera != null)
                    popUp.viewCamera = viewCamera;
                else
                    popUp.viewCamera = Camera.main;

                break;
            }
        }

        return popUp;
    }

    /// <summary>
    /// 팝업 보여주기
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="callback">팝업 콜백</param>
    /// <param name="param">팝업 파라메터</param>
    /// <returns>생성되어 보이는 팝업</returns>
    public BasePopUp Show(System.Type popUpType, BasePopUp.Callback callback = null, Hashtable param = null)
    {
        BasePopUp popUp = CreatePopUp(popUpType, callback, param);
        popUp.Open(callback, param);
        return popUp;
    }

    public BasePopUp CreatePopup(System.Type popUpType, BasePopUp.Callback callback = null, Hashtable param = null)
    {
        BasePopUp popUp = CreatePopUp(popUpType, callback, param);
        return popUp;
    }

    /// <summary>
    /// 팝업 예약
    /// 현재 화면에 보여지는 팝업이 사라진 후 나타날 팝업 예약
    /// </summary>
    /// <param name="popUpType">팝업 종류</param>
    /// <param name="callback">팝업 콜백</param>
    /// <param name="param">팝업 파라메터</param>
    public void Reserve(System.Type popUpType, BasePopUp.Callback callback = null, Hashtable param = null)
    {
        /* 현재 화면에 표시된 팝업이 없다면 예약된 팝업이 즉시 화면에 나타난다. */
        if (_popUpList.Count == 0)
        {
            Show(popUpType, callback = null, param = null);
            return;
        }

        int idx = _popUpList.Count - 1;
        if (!_reservedPopUpDataDic.ContainsKey(idx))
        {
            _reservedPopUpDataDic.Add(idx, new List<ReservedPopUpData>());
        }

        _reservedPopUpDataDic[idx].Add(new ReservedPopUpData(popUpType, callback, param));
    }

    /// <summary>
    /// 팝업 제거
    /// </summary>
    /// <param name="popUp">제거할 팝업</param>
    /// <param name="isCancelledNext">현재 팝업이 제거된 후 나타나도록 예약된 팝업을 동작하게할지 여부</param>
    public void Remove(BasePopUp popUp, bool isCancelledNext = false)
    {
        int idx = _popUpList.LastIndexOf(popUp);

        /* 현재 팝업이 제거될 때 현제 팝업보다 상위에 있는 팝업을 함께 제거한다. */
        for (int i = idx; i < _popUpList.Count; i++)
        {
            _popUpList[i].Close(true, false, false);
            
            if (i > idx)
            {
                _reservedPopUpDataDic.Remove(i);
            }
        }

        /* 예약된 팝업을 나타나게 할지 결정 */
        if (isCancelledNext)
        {
            _reservedPopUpDataDic.Remove(idx);
        }
        else
        {
            if (_reservedPopUpDataDic.ContainsKey(idx) && _reservedPopUpDataDic[idx].Count > 0)
            {
                ReservedPopUpData data = _reservedPopUpDataDic[idx][0];
                Show(data.popUpType, data.callback, data.param);
                _reservedPopUpDataDic[idx].RemoveAt(0);
            }
        }

        if (_reservedPopUpDataDic.ContainsKey(idx) && _reservedPopUpDataDic[idx].Count == 0)
            _reservedPopUpDataDic.Remove(idx);

        MonoBehaviour.Destroy(popUp.gameObject);
        _popUpList.Remove(popUp);
        Resources.UnloadUnusedAssets();
    }

    /// <summary>
    /// 최상단 팝업 제거
    /// </summary>
    /// <param name="isCancelledNext">현재 팝업이 제거된 후 나타나도록 예약된 팝업을 동작하게할지 여부</param>
    public void RemoveFront(bool isCancelledNext = false)
    {
        if (_popUpList.Count == 0)
            return;

        _popUpList[_popUpList.Count - 1].Close(true, true, isCancelledNext);
    }

    /// <summary>
    /// 화면상의 모든 팝업 제거
    /// </summary>
    public void DestroyAll()
    {
        for (int i = 0; i < _popUpList.Count; i++)
        {
            if (!_popUpList[i].Equals(null))
                MonoBehaviour.Destroy(_popUpList[i].gameObject);
        }

        _popUpList.Clear();
        _reservedPopUpDataDic.Clear();
    }
}
