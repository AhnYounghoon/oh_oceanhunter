﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Button), typeof(Animator))]
public class ButtonEventPlayer : STZBehaviour, IPointerUpHandler, IPointerDownHandler
{
    [System.Serializable]
    public struct SoundData
    {
        public string groupName;
        public string clipName;
    }

    [Header("사운드 정보")]
    [SerializeField]
    SoundData _buttonSoundData = new SoundData { clipName = "button", groupName = "sfx_ui" };

    Animator    _animator;
    Button      _button;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _button   = GetComponent<Button>();
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (_button != null)
        {
            if (!_button.interactable || !_button.enabled)
            {
                return;
            }
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (_button != null)
        {
            if (!_button.interactable || !_button.enabled)
            {
                return;
            }
        }

        if (_animator != null)
        {
            _animator.SetTrigger("Pressed");
        }
            
        SingletonController.Get<SoundManager>().Play(_buttonSoundData.clipName, _buttonSoundData.groupName);
    }

    void OnDisable()
    {
        this.transform.localScale = new Vector3(1, 1, 1);
    }
}
