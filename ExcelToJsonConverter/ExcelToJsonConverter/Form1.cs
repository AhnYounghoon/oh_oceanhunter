﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelToJsonConverter
{
    public partial class Form1 : Form
    {
        private Winform.ExcelToJsonConverter converter = new Winform.ExcelToJsonConverter();

        public Form1()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog opd = new OpenFileDialog())
            {
                opd.DefaultExt = "All files";                               // 기본 파일타입 설정
                opd.Filter = "All files (*.*)|*.*"; // 파일타입
                opd.Multiselect = false;            // 다중선택되지 않도록.               
                string strAppDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                opd.InitialDirectory = strAppDir;   // 파일불러오기를 했을 때 제일 처음에 열리는 디렉토리 설정

                if (opd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string fileName = opd.FileName;
                        textBox1.Text = fileName;

                        // 선택한 파일을 Open
                        //rList = ReadTextFileToList(fileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog opd = new FolderBrowserDialog())
            {
                if (opd.ShowDialog() == DialogResult.OK)
                {
                    textBox2.Text = opd.SelectedPath;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            converter.ConvertExcelFileToJson(textBox1.Text, textBox2.Text);
            MessageBox.Show("변환 성공 너굴!","알림");
            System.Diagnostics.Process.Start(textBox2.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            textBox2.Text = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] file = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string str in file)
                {
                    textBox1.Text = str;
                    button3_Click(null, null);
                }
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy | DragDropEffects.Scroll;
            }
        }
    }
}
